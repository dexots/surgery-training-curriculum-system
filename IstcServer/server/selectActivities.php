<?php
function getActivitiesTable($c) {
    $stmt = $c->prepare("SELECT activityId, title, registrationDeadlineDate, DATE_FORMAT(registrationDeadlineTime, '%H:%i') AS registrationDeadlineTime, eventDate, DATE_FORMAT(eventStartTime, '%H:%i') AS eventStartTime, DATE_FORMAT(eventEndTime, '%H:%i') AS eventEndTime, maxVacancy, curVacancy, location, description, link, important FROM activities;");
	$stmt->execute();
	$activitiesUpdated = $stmt->fetchAll(PDO::FETCH_ASSOC);
	return $activitiesUpdated;
	}
?>