<?php
function getTraineeExamsTable($c) {
    $stmt = $c->prepare("SELECT * FROM traineeExams;");
	$stmt->execute();
	$traineeExamsUpdated = $stmt->fetchAll(PDO::FETCH_ASSOC);
	return $traineeExamsUpdated;
	}
?>