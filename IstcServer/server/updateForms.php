<?php

include('details.php');

try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    echo "Connected successfully"; 

    // prepare sql and bind parameters
    $stmt = $conn->prepare("UPDATE forms SET name = :name, dateReleased = :dateReleased, deadline = :deadline, url = :url, type = :type WHERE formsId = :formsId;");
    $stmt->bindParam(':formsId', $formsId);
    $stmt->bindParam(':name', $name);
    $stmt->bindParam(':dateReleased', $dateReleased);
    $stmt->bindParam(':deadline', $deadline);
    $stmt->bindParam(':url', $url);
	$stmt->bindParam(':type', $type);

    // TO IMPLEMENT: Getting params values from JSON file and executing query
	/*
    $formsId = "";
	$name = ""; 
	$dateReleased = ""; 
	$deadline = ""; 
	$url = "";
	$type = "";
    $stmt->execute();
	echo "New records created successfully";
	*/
	}
catch(PDOException $e)
    {
    echo "Connection failed: " . $e->getMessage();
    }

$conn = null;
?>
