<?php

    // prepare sql and bind parameters
    $stmt = $conn->prepare("INSERT INTO notes (noteId, noteTitle, date, content, userId, role) VALUES(:noteId, :noteTitle, :date, :content, :userId, :role);");
    $stmt->bindParam(':noteId', $_SESSION["noteId"]);
    $stmt->bindParam(':noteTitle', $_SESSION["noteTitle"]);
    $stmt->bindParam(':date', $_SESSION["date"]);
    $stmt->bindParam(':content', $_SESSION["content"]);
    $stmt->bindParam(':userId', $userId);
	$stmt->bindParam(':role', $role);
	$stmt->execute();
	echo "New records created successfully";

    // TO IMPLEMENT: Getting params values from JSON file and executing query
	/*
    $noteId = "";
	$noteTitle = ""; 
	$date = ""; 
	$content = ""; 
	$userId = "";
	$role = "";
	*/

/* TO BE DELETED ONCE ABOVE CODE WORKING
include('details.php');

try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    echo "Connected successfully"; 

    // prepare sql and bind parameters
    $stmt = $conn->prepare("INSERT INTO notes (noteId, noteTitle, date, content, userId, role) VALUES(:noteId, :noteTitle, :date, :content, :userId, :role);");
    $stmt->bindParam(':noteId', $noteId);
    $stmt->bindParam(':noteTitle', $noteTitle);
    $stmt->bindParam(':date', $date);
    $stmt->bindParam(':content', $content);
    $stmt->bindParam(':userId', $userId);
	$stmt->bindParam(':role', $role);

    // TO IMPLEMENT: Getting params values from JSON file and executing query
    $noteId = "";
	$noteTitle = ""; 
	$date = ""; 
	$content = ""; 
	$userId = "";
	$role = "";
    $stmt->execute();
	echo "New records created successfully";
	}
catch(PDOException $e)
    {
    echo "Connection failed: " . $e->getMessage();
    }

$conn = null; */
?>