<?php
    $stmt = $conn->prepare("INSERT INTO activities (activityId, title, registrationDeadlineTime, registrationDeadlineDate, eventDate, eventStartTime, eventEndTime, maxVacancy, curVacancy, location, description, link, important) VALUES(:activityId, :title, :registrationDeadlineTime, :registrationDeadlineDate, :eventDate, :eventStartTime, :eventEndTime, :maxVacancy, :curVacancy, :location, :description, :link, :important) ON DUPLICATE KEY UPDATE title=:title, registrationDeadlineTime=:registrationDeadlineTime, registrationDeadlineDate=:registrationDeadlineDate, eventDate=:eventDate, eventStartTime=:eventStartTime, eventEndTime=:eventEndTime, maxVacancy=:maxVacancy, curVacancy=:curVacancy, location=:location, description=:description, link=:link, important=:important;");
    $stmt->bindParam(':activityId', $activityId);
    $stmt->bindParam(':title', $title);
    $stmt->bindParam(':registrationDeadlineTime', $registrationDeadlineTime);
    $stmt->bindParam(':registrationDeadlineDate', $registrationDeadlineDate);
    $stmt->bindParam(':eventDate', $eventDate);
    $stmt->bindParam(':eventStartTime', $eventStartTime);
    $stmt->bindParam(':eventEndTime', $eventEndTime);
    $stmt->bindParam(':maxVacancy', $maxVacancy);
    $stmt->bindParam(':curVacancy', $curVacancy);
    $stmt->bindParam(':location', $location);
	$stmt->bindParam(':description', $description);
	$stmt->bindParam(':link', $link);
	$stmt->bindParam(':important', $important);
	$stmt->execute();
?>
