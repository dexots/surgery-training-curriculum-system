<?php
    $stmt = $conn->prepare("INSERT INTO patient (patientId, dob) VALUES(:patientId, :dob) ON DUPLICATE KEY UPDATE dob=:dob;");
    $stmt->bindParam(':patientId', $patientId);
    $stmt->bindParam(':dob', $dob);
	$stmt->execute();
?>
