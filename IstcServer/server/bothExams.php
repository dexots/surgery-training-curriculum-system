<?php
    $stmt = $conn->prepare("INSERT INTO exams (moduleId, moduleName, date, time, location) VALUES(:moduleId, :moduleName, :date, :time, :location) ON DUPLICATE KEY UPDATE moduleName=:moduleName, date=:date, time=:time, location=:location;");
    $stmt->bindParam(':moduleId', $moduleId);
    $stmt->bindParam(':moduleName', $moduleName);
    $stmt->bindParam(':date', $date);
    $stmt->bindParam(':time', $time);
    $stmt->bindParam(':location', $location);
    $stmt->execute();
?>
