<?php
    $stmt = $conn->prepare("INSERT INTO userProcedure (userId, role, procedureId) VALUES(:userId, :role, :procedureId) ON DUPLICATE KEY UPDATE role=:role;");
    $stmt->bindParam(':userId', $userId);
    $stmt->bindParam(':role', $role);
    $stmt->bindParam(':procedureId', $procedureId);
	$stmt->execute();
?>
