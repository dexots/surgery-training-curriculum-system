<?php

include('details.php');

try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    echo "Connected successfully"; 

    // prepare sql and bind parameters
    $stmt = $conn->prepare("DELETE FROM activities WHERE activityId = :activityId;");
    $stmt->bindParam(':activityId', $activityId);

    // TO IMPLEMENT: Getting params values from JSON file and executing query
	/*
    $activityId = "";
	$title = ""; 
	$registrationDeadlineTime = ""; 
	$registrationDeadlineDate = ""; 
	$eventDate = "";
	$eventStartTime = "";
	$eventEndTime = "";
	$maxVacancy = "";
	$curVacancy = "";
	$location = "";
	$description = "";
	$link = "";
	$important = "";
    $stmt->execute();
	echo "New records created successfully";
	*/
	}
catch(PDOException $e)
    {
    echo "Connection failed: " . $e->getMessage();
    }

$conn = null;
?>
