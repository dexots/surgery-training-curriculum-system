<?php
    $stmt = $conn->prepare("INSERT INTO leaveApp (leaveAppId, startDate, endDate, userId, role, status) VALUES(:leaveAppId, :startDate, :endDate, :userId, :role, :status) ON DUPLICATE KEY UPDATE startDate=:startDate, endDate=:endDate, status=:status;");
    $stmt->bindParam(':leaveAppId', $leaveAppId);
    $stmt->bindParam(':startDate', $startDate);
    $stmt->bindParam(':endDate', $endDate);
    $stmt->bindParam(':userId', $userId);
    $stmt->bindParam(':role', $role);
	$stmt->bindParam(':status', $status);
    $stmt->execute();
?>
