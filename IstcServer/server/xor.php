<?php
class XorClass {
	public function xor_this($string, $key) {
		$text = $string;
		$outText = '';
		
		for($j = 0, $i = 0; $j < strlen ( $text ); $j ++, $i ++) {
			$outText .= $text {$i} ^ $key {$j % strlen ( $key )};
			// echo 'i='.$i.', '.'j='.$j.', '.$outText[$i].'<br />'; //for debugging
		}
		return $outText;
	}
}

$xorClass = new XorClass();

function xor_this($string, $key) {
	return $xorClass->xor_this($string, $key);
}

function xor_db($string, $key) {

	$text =$string;
	$outText = '';

	for($j=0, $i=0;$j<strlen($text);$j++,$i++) {
		$outText .= chr(ord($text[$i]) ^ $key);
		//echo 'i='.$i.', '.'j='.$j.', '.$outText[$i].'<br />'; //for debugging
	}
	return $outText;
}
?>
