<?php

include('details.php');

try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    echo "Connected successfully"; 

    // prepare sql and bind parameters
    $stmt = $conn->prepare("UPDATE user SET  name = :name, personalEmail = :personalEmail, nuhEmail = :nuhEmail, address = :address, homePhone = :homePhone, handPhone = :handPhone, rotation = :rotation, rosterMonster = :rosterMonster WHERE matricNo = :matricNo AND role = :role;");
    $stmt->bindParam(':matricNo', $matricNo);
    $stmt->bindParam(':name', $name);
    $stmt->bindParam(':personalEmail', $personalEmail);
    $stmt->bindParam(':nuhEmail', $nuhEmail);
    $stmt->bindParam(':address', $address);
    $stmt->bindParam(':homePhone', $homePhone);
    $stmt->bindParam(':handPhone', $handPhone);
    $stmt->bindParam(':rotation', $rotation);
    $stmt->bindParam(':role', $role);
    $stmt->bindParam(':rosterMonster', $rosterMonster);

    // TO IMPLEMENT: Getting params values from JSON file and executing query
	/*
    $matricNo = "";
	$name = ""; 
	$personalEmail = ""; 
	$nuhEmail = ""; 
	$address = "";
	$homePhone = "";
	$handPhone = "";
	$rotation = "";
	$role = "";
	$rosterMonster = "";
    $stmt->execute();
	echo "New records created successfully";
	*/
	}
catch(PDOException $e)
    {
    echo "Connection failed: " . $e->getMessage();
    }

$conn = null;
?>
