<?php
    $stmt = $conn->prepare("INSERT INTO forms (formsId, name, dateReleased, deadline, url, type) VALUES(:formsId, :name, :dateReleased, :deadline, :url, :type) ON DUPLICATE KEY UPDATE name=:name, dateReleased=:dateReleased, deadline=:deadline, url=:url, type=:type;");
    $stmt->bindParam(':formsId', $formsId);
    $stmt->bindParam(':name', $name);
    $stmt->bindParam(':dateReleased', $dateReleased);
    $stmt->bindParam(':deadline', $deadline);
    $stmt->bindParam(':url', $url);
	$stmt->bindParam(':type', $type);
    $stmt->execute();
?>
