<?php
    $stmt = $conn->prepare("INSERT INTO user (matricNo, name, personalEmail , nuhEmail, address, homePhone, handPhone, rotation, role, rosterMonster) VALUES(:matricNo, :name, :personalEmail, :nuhEmail, :address, :homePhone, :handPhone, :rotation, :role, :rosterMonster) ON DUPLICATE KEY UPDATE matricNo=:matricNo, name=:name, personalEmail=:personalEmail, nuhEmail=:nuhEmail, address=:address, homePhone=:homePhone, handPhone=:handPhone, rotation=:rotation, role=:role, rosterMonster=:rosterMonster;");
    $stmt->bindParam(':matricNo', $matricNo);
    $stmt->bindParam(':name', $name);
    $stmt->bindParam(':personalEmail', $personalEmail);
    $stmt->bindParam(':nuhEmail', $nuhEmail);
    $stmt->bindParam(':address', $address);
    $stmt->bindParam(':homePhone', $homePhone);
    $stmt->bindParam(':handPhone', $handPhone);
    $stmt->bindParam(':rotation', $rotation);
    $stmt->bindParam(':role', $role);
    $stmt->bindParam(':rosterMonster', $rosterMonster);
    $stmt->execute();
?>
