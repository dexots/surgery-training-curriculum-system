<?php
function getProcedureTable($c) {
    $stmt = $c->prepare("SELECT procedureId, name, patientId, date, DATE_FORMAT(time, '%H:%i') AS time, remarks, outcome FROM procedureTable;");
	$stmt->execute();
	$procedureTableUpdated = $stmt->fetchAll(PDO::FETCH_ASSOC);
	return $procedureTableUpdated;
	}
?>
