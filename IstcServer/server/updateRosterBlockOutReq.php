<?php

include('details.php');

try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    echo "Connected successfully"; 

    // prepare sql and bind parameters
    $stmt = $conn->prepare("UPDATE rosterBlockOutReq SET submitDate = :submitDate, reqDate = :reqDate, reason = :reason, role = :role, status = :status WHERE reqId = :reqId AND userId = :userId;");
    $stmt->bindParam(':reqId', $reqId);
    $stmt->bindParam(':submitDate', $submitDate);
    $stmt->bindParam(':reqDate', $reqDate);
    $stmt->bindParam(':reason', $reason);
    $stmt->bindParam(':userId', $userId);
	$stmt->bindParam(':role', $role);
	$stmt->bindParam(':status', $status);

    // TO IMPLEMENT: Getting params values from JSON file and executing query
	/*
    $reqId = "";
	$submitDate = ""; 
	$reqDate = ""; 
	$reason = ""; 
	$userId = "";
	$role = "";
	$status = "";
    $stmt->execute();
	echo "New records created successfully";
	*/
	}
catch(PDOException $e)
    {
    echo "Connection failed: " . $e->getMessage();
    }

$conn = null;
?>
