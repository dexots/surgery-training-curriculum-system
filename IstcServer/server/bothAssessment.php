<?php
    $stmt = $conn->prepare("INSERT INTO assessment (assessmentId, userId, role, title, date, grade, feedback) VALUES(:assessmentId, :userId, :role, :title, :date, :grade, :feedback) ON DUPLICATE KEY UPDATE userId=:userId, role=:role, title=:title, date=:date, grade=:grade, feedback=:feedback;");
    $stmt->bindParam(':assessmentId', $assessmentId);
    $stmt->bindParam(':userId', $userId);
    $stmt->bindParam(':role', $role);
    $stmt->bindParam(':title', $title);
    $stmt->bindParam(':date', $date);
    $stmt->bindParam(':grade', $grade);
    $stmt->bindParam(':feedback', $feedback);
    $stmt->execute();
?>
