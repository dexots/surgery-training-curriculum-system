<?php
    $stmt = $conn->prepare("INSERT INTO notes (noteId, noteTitle, date, content, userId, role) VALUES(:noteId, :noteTitle, :date, :content, :userId, :role) ON DUPLICATE KEY UPDATE noteTitle=:noteTitle, date=:date, content=:content;");
    $stmt->bindParam(':noteId', $noteId);
    $stmt->bindParam(':noteTitle', $noteTitle);
    $stmt->bindParam(':date', $date);
    $stmt->bindParam(':content', $content);
    $stmt->bindParam(':userId', $userId);
	$stmt->bindParam(':role', $role);
	$stmt->execute();
?>