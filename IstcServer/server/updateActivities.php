<?php

include('details.php');

try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    echo "Connected successfully"; 

    // prepare sql and bind parameters
    $stmt = $conn->prepare("UPDATE activities SET title = :title, registrationDeadlineTime = :registrationDeadlineTime, registrationDeadlineDate = :registrationDeadlineDate, eventDate = :eventDate, eventStartTime = :eventStartTime, eventEndTime = :eventEndTime, maxVacancy = :maxVacancy, curVacancy = :curVacancy, location = :location, description = :description, link = :link, important = :important WHERE activityId = :activityId;");
    $stmt->bindParam(':activityId', $activityId);
    $stmt->bindParam(':title', $title);
    $stmt->bindParam(':registrationDeadlineTime', $registrationDeadlineTime);
    $stmt->bindParam(':registrationDeadlineDate', $registrationDeadlineDate);
    $stmt->bindParam(':eventDate', $eventDate);
    $stmt->bindParam(':eventStartTime', $eventStartTime);
    $stmt->bindParam(':eventEndTime', $eventEndTime);
    $stmt->bindParam(':maxVacancy', $maxVacancy);
    $stmt->bindParam(':curVacancy', $curVacancy);
    $stmt->bindParam(':location', $location);
	$stmt->bindParam(':description', $description);
	$stmt->bindParam(':link', $link);
	$stmt->bindParam(':important', $important);

    // TO IMPLEMENT: Getting params values from JSON file and executing query
	/*
    $activityId = "";
	$title = ""; 
	$registrationDeadlineTime = ""; 
	$registrationDeadlineDate = ""; 
	$eventDate = "";
	$eventStartTime = "";
	$eventEndTime = "";
	$maxVacancy = "";
	$curVacancy = "";
	$location = "";
	$description = "";
	$link = "";
	$important = "";
    $stmt->execute();
	echo "New records created successfully";
	*/
	}
catch(PDOException $e)
    {
    echo "Connection failed: " . $e->getMessage();
    }

$conn = null;
?>
