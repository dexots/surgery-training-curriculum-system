<?php

include('details.php');

try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    echo "Connected successfully"; 

    // prepare sql and bind parameters
    $stmt = $conn->prepare("INSERT INTO assessment (assessmentId, userId, role, title, date, grade, feedback) VALUES(:assessmentId, :userId, :role, :title, :date, :grade, :feedback);");
    $stmt->bindParam(':assessmentId', $assessmentId);
    $stmt->bindParam(':userId', $userId);
    $stmt->bindParam(':role', $role);
    $stmt->bindParam(':title', $title);
    $stmt->bindParam(':date', $date);
    $stmt->bindParam(':grade', $grade);
    $stmt->bindParam(':feedback', $feedback);

    // TO IMPLEMENT: Getting params values from JSON file and executing query
	/*
    $assessmentId = "";
	$userId = ""; 
	$role = ""; 
	$title = ""; 
	$date = "";
	$grade = "";
	$feedback = "";
    $stmt->execute();
	echo "New records created successfully";
	*/
	}
catch(PDOException $e)
    {
    echo "Connection failed: " . $e->getMessage();
    }

$conn = null;
?>
