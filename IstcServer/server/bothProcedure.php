<?php
    $stmt = $conn->prepare("INSERT INTO procedureTable (procedureId, name, patientId, date, time, remarks, outcome) VALUES(:procedureId, :name, :patientId, :date, :time, :remarks, :outcome) ON DUPLICATE KEY UPDATE name=:name, date=:date, time=:time, remarks=:remarks, outcome=:outcome;");
    $stmt->bindParam(':procedureId', $procedureId);
    $stmt->bindParam(':name', $name);
    $stmt->bindParam(':patientId', $patientId);
    $stmt->bindParam(':date', $date);
    $stmt->bindParam(':time', $time);
	$stmt->bindParam(':remarks', $remarks);
	$stmt->bindParam(':outcome', $outcome);
    $stmt->execute();
?>
