<?php
    $stmt = $conn->prepare("INSERT INTO userForms (userId, role, formsId, status) VALUES(:userId, :role, :formsId, :status) ON DUPLICATE KEY UPDATE status=:status");
    $stmt->bindParam(':userId', $userId);
    $stmt->bindParam(':role', $role);
    $stmt->bindParam(':formsId', $formsId);
    $stmt->bindParam(':status', $status);
    $stmt->execute();
?>
