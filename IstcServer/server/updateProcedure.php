<?php

include('details.php');

try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    echo "Connected successfully"; 

    // prepare sql and bind parameters
    $stmt = $conn->prepare("UPDATE procedureTable SET name = :name, patientId = :patientId, date = :date, time = :time, remarks = :remarks, outcome = :outcome WHERE procedureId = :procedureId;");
    $stmt->bindParam(':procedureId', $procedureId);
    $stmt->bindParam(':name', $name);
    $stmt->bindParam(':patientId', $patientId);
    $stmt->bindParam(':date', $date);
    $stmt->bindParam(':time', $time);
	$stmt->bindParam(':remarks', $remarks);
	$stmt->bindParam(':outcome', $outcome);

    // TO IMPLEMENT: Getting params values from JSON file and executing query
	/*
    $procedureId = "";
	$name = ""; 
	$patientId = ""; 
	$date = ""; 
	$time = "";
	$remarks = "";
	$outcome = "";
    $stmt->execute();
	echo "New records created successfully";
	*/
	}
catch(PDOException $e)
    {
    echo "Connection failed: " . $e->getMessage();
    }

$conn = null;
?>
