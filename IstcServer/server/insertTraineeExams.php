<?php

include('details.php');

try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    echo "Connected successfully"; 

    // prepare sql and bind parameters
    $stmt = $conn->prepare("INSERT INTO traineeExams (traineeId, role, moduleId, seatNo) VALUES(:traineeId, :role, :moduleId, :seatNo);");
    $stmt->bindParam(':traineeId', $traineeId);
    $stmt->bindParam(':role', $role);
    $stmt->bindParam(':moduleId', $moduleId);
    $stmt->bindParam(':seatNo', $seatNo);

    // TO IMPLEMENT: Getting params values from JSON file and executing query
	/*
    $traineeId = "";
	$role = ""; 
	$moduleId = ""; 
	$seatNo = ""; 
    $stmt->execute();
	echo "New records created successfully";
	*/
	}
catch(PDOException $e)
    {
    echo "Connection failed: " . $e->getMessage();
    }

$conn = null;
?>
