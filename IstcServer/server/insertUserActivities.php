<?php

include('details.php');

try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    echo "Connected successfully"; 

    // prepare sql and bind parameters
    $stmt = $conn->prepare("INSERT INTO userActivities (userId, role, activityId, status) VALUES(:userId, :role, :activityId, :status);");
    $stmt->bindParam(':userId', $userId);
    $stmt->bindParam(':role', $role);
    $stmt->bindParam(':activityId', $activityId);
    $stmt->bindParam(':status', $status);

    // TO IMPLEMENT: Getting params values from JSON file and executing query
	/*
    $userId = "";
	$role = ""; 
	$activityId = ""; 
	$status = ""; 
    $stmt->execute();
	echo "New records created successfully";
	*/
	}
catch(PDOException $e)
    {
    echo "Connection failed: " . $e->getMessage();
    }

$conn = null;
?>
