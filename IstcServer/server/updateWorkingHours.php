<?php

include('details.php');

try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    echo "Connected successfully"; 

    // prepare sql and bind parameters
    $stmt = $conn->prepare("UPDATE workingHours SET dayIn = :dayIn, timeIn = :timeIn, dayOut = :dayOut, timeOut = :timeOut WHERE userId = :userId AND role = :role AND workingHoursId = :workingHoursId;");
    $stmt->bindParam(':userId', $userId);
    $stmt->bindParam(':role', $role);
    $stmt->bindParam(':workingHoursId', $workingHoursId);
    $stmt->bindParam(':dayIn', $dayIn);
	$stmt->bindParam(':timeIn', $timeIn);
	$stmt->bindParam(':dayOut', $dayOut);
	$stmt->bindParam(':timeOut', $timeOut);

    // TO IMPLEMENT: Getting params values from JSON file and executing query
	/*
    $userId = "";
	$role = ""; 
	$workingHoursId = ""; 
	$dayIn = ""; 
	$timeIn = ""; 
	$dayOut = ""; 
	$timeOut = ""; 
    $stmt->execute();
	echo "New records created successfully";
	*/
	}
catch(PDOException $e)
    {
    echo "Connection failed: " . $e->getMessage();
    }

$conn = null;
?>
