<?php
function getFormsTable($c) {
    $stmt = $c->prepare("SELECT * FROM forms;");
	$stmt->execute();
	$formsUpdated = $stmt->fetchAll(PDO::FETCH_ASSOC);
	return $formsUpdated;
	}
?>