<?php
    $stmt = $conn->prepare("INSERT INTO traineeExams (traineeId, role, moduleId, seatNo) VALUES(:traineeId, :role, :moduleId, :seatNo) ON DUPLICATE KEY UPDATE seatNo=:seatNo;");
    $stmt->bindParam(':traineeId', $traineeId);
    $stmt->bindParam(':role', $role);
    $stmt->bindParam(':moduleId', $moduleId);
    $stmt->bindParam(':seatNo', $seatNo);
    $stmt->execute();
?>
