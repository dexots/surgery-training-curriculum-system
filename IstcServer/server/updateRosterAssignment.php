<?php

include('details.php');

try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    echo "Connected successfully"; 

    // prepare sql and bind parameters
    $stmt = $conn->prepare("UPDATE rosterAssignment SET date = :date WHERE rosterId = :rosterId;");
    $stmt->bindParam(':rosterId', $rosterId);
    $stmt->bindParam(':date', $date);

    // TO IMPLEMENT: Getting params values from JSON file and executing query
	/*
    $rosterId = "";
	$date = ""; 
    $stmt->execute();
	echo "New records created successfully";
	*/
	}
catch(PDOException $e)
    {
    echo "Connection failed: " . $e->getMessage();
    }

$conn = null;
?>
