<?php
    $stmt = $conn->prepare("INSERT INTO userActivities (userId, role, activityId, status) VALUES(:userId, :role, :activityId, :status) ON DUPLICATE KEY UPDATE status=:status;");
    $stmt->bindParam(':userId', $userId);
    $stmt->bindParam(':role', $role);
    $stmt->bindParam(':activityId', $activityId);
    $stmt->bindParam(':status', $status);
    $stmt->execute();
?>
