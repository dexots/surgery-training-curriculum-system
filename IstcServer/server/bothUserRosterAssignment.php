<?php
    $stmt = $conn->prepare("INSERT INTO userRosterAssignment (userId, role, rosterId, status) VALUES(:userId, :role, :rosterId, :status) ON DUPLICATE KEY UPDATE status=:status;");
    $stmt->bindParam(':userId', $userId);
    $stmt->bindParam(':role', $role);
    $stmt->bindParam(':rosterId', $rosterId);
    $stmt->bindParam(':status', $status);
    $stmt->execute();
?>
