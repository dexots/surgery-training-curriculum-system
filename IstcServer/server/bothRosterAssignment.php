<?php
    $stmt = $conn->prepare("INSERT INTO rosterAssignment (rosterId, date) VALUES(:rosterId, :date) ON DUPLICATE KEY UPDATE date=:date;");
    $stmt->bindParam(':rosterId', $rosterId);
    $stmt->bindParam(':date', $date);
    $stmt->execute();
?>
