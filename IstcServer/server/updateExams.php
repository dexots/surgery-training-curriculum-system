<?php

include('details.php');

try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    echo "Connected successfully"; 

    // prepare sql and bind parameters
    $stmt = $conn->prepare("UPDATE exams SET moduleId = :moduleId, moduleName = :moduleName, date = :date, time = :time, location = :location WHERE moduleId = :moduleId;");
    $stmt->bindParam(':moduleId', $moduleId);
    $stmt->bindParam(':moduleName', $moduleName);
    $stmt->bindParam(':date', $date);
    $stmt->bindParam(':time', $time);
    $stmt->bindParam(':location', $location);

    // TO IMPLEMENT: Getting params values from JSON file and executing query
	/*
    $moduleId = "";
	$moduleName = ""; 
	$date = ""; 
	$time = ""; 
	$location = "";
    $stmt->execute();
	echo "New records created successfully";
	*/
	}
catch(PDOException $e)
    {
    echo "Connection failed: " . $e->getMessage();
    }

$conn = null;
?>
