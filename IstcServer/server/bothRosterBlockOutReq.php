<?php
    $stmt = $conn->prepare("INSERT INTO rosterBlockOutReq (reqId, submitDate, reqDate, reason, userId, role, status) VALUES(:reqId, :submitDate, :reqDate, :reason, :userId, :role, :status) ON DUPLICATE KEY UPDATE submitDate=:submitDate, reqDate=:reqDate, reason=:reason, status=:status;");
    $stmt->bindParam(':reqId', $reqId);
    $stmt->bindParam(':submitDate', $submitDate);
    $stmt->bindParam(':reqDate', $reqDate);
    $stmt->bindParam(':reason', $reason);
    $stmt->bindParam(':userId', $userId);
	$stmt->bindParam(':role', $role);
	$stmt->bindParam(':status', $status);
    $stmt->execute();
?>
