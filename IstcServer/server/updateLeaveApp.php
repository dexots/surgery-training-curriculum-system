<?php

include('details.php');

try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    echo "Connected successfully"; 

    // prepare sql and bind parameters
    $stmt = $conn->prepare("UPDATE leaveApp SET startDate = :startDate, endDate = :endDate, userId = :userId, role = :role, status = :status WHERE leaveAppId = :leaveAppId;");
    $stmt->bindParam(':leaveAppId', $leaveAppId);
    $stmt->bindParam(':startDate', $name);
    $stmt->bindParam(':endDate', $endDate);
    $stmt->bindParam(':userId', $userId);
    $stmt->bindParam(':role', $role);
	$stmt->bindParam(':status', $status);

    // TO IMPLEMENT: Getting params values from JSON file and executing query
	/*
    $leaveAppId = "";
	$startDate = ""; 
	$endDate = ""; 
	$userId = ""; 
	$role = "";
	$status = "";
    $stmt->execute();
	echo "New records created successfully";
	*/
	}
catch(PDOException $e)
    {
    echo "Connection failed: " . $e->getMessage();
    }

$conn = null;
?>
