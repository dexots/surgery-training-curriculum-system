<?php
function getWorkingHoursTable($c) {
    $stmt = $c->prepare("SELECT userId, role, workingHoursId, dayIn, DATE_FORMAT(timeIn, '%H:%i') AS timeIn, dayOut,  DATE_FORMAT(timeOut, '%H:%i') AS timeOut FROM workingHours;");
	$stmt->execute();
	$workingHoursUpdated = $stmt->fetchAll(PDO::FETCH_ASSOC);
	return $workingHoursUpdated;
	}
?>