<?php
function getUserActivitiesTable($c) {
    $stmt = $c->prepare("SELECT * FROM userActivities;");
	$stmt->execute();
	$userActivitiesUpdated = $stmt->fetchAll(PDO::FETCH_ASSOC);
	return $userActivitiesUpdated;
	}
?>
