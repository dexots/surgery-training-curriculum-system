<?php
    $stmt = $conn->prepare("INSERT INTO trainerAssessment (trainerId, assessmentId, role) VALUES(:trainerId, :assessmentId, :role)ON DUPLICATE KEY UPDATE role=:role;");
    $stmt->bindParam(':trainerId', $trainerId);
    $stmt->bindParam(':assessmentId', $assessmentId);
    $stmt->bindParam(':role', $role);
    $stmt->execute();
?>
