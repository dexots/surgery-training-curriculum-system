<?php
// NOTE: This is just a test script that should be good enough to show some simple server-client interaction, will implement fully

error_reporting(E_ALL);

//Header lines for file output
$file = 'results.json';
header('Content-Description: File Transfer');
header('Content-Type: application/json');
header('Content-Disposition: attachment; filename='.basename($file));
header('Expires: 0');
header('Cache-Control: must-revalidate');
header('Pragma: public');
//header('Content-Length: ' . filesize($file));

function endsWith($haystack, $needle) {
	// search forward starting from end minus needle length characters
	return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== FALSE);
}

// Start the session, needed so that can pass vars to other PHP files for processing
session_start();

include_once('../details.php');

try {
	$conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
	// set the PDO error mode to exception
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	//echo "Connected successfully";

	//Read JSON file
	// copy file content into a string var
	//For now, a MODIFIED VERSION of it is being used.
	$json_file = file_get_contents('php://input');
	//$json_file = file_get_contents('test.json');
	// convert the string to a json object
	$jfo = json_decode($json_file, true);

	if ($json_file === NULL) {
		echo "FAIL!\n";
		die;
	}

	//Split JSON file into params
	$table = $jfo['tableNames'];
	$userMatricNo = $jfo['userMatricNo'];
	$userRole = $jfo['userRole'];

	$clientPW = $jfo['clientPassword'];
	$clientPW = xor_this($clientPW, "InteGrateD");

	date_default_timezone_set('Asia/Singapore');
	$today = date(dYm);

	if (endsWith($clientPW, $today)) {

		$serverPw = "\\e-2WE\"|w";
		$serverPw = xor_this($serverPw, "iSTc");

		$clientPW = str_replace($today, "", $clientPW);
		$clientPW = xor_this($clientPW, "iSTc");
			
		if($clientPW !== $serverPw) {
			echo "Password not matched!!!\n";
			die;
		}
	} else {
		echo "Password not matched!!!!\n";
		die;
	}

	/*
	 //TO IMPLEMENT: See if necessary params there
	 //For time being, will assume that the params for insertNotes are already there
	 $_SESSION["noteId"] = $jfo->{'noteId'};
	 $_SESSION["noteTitle"] = $jfo->{'noteTitle'};
	 $_SESSION["date"] = $jfo->{'date'};
	 $_SESSION["content"] = $jfo->{'content'};
	 */

	//Check if needed array exists in $table (cannot loop for this cos the check can only done with exact name), include the needed PHP file, loop through each entry and pass their elements to the included PHP file, which will then execute the SQL queries
	if(isset($table["user"])) {
		foreach ($table["user"] as $entry) {
			$matricNo = $entry["matricNo"];
			$name = $entry["name"];
			$personalEmail = $entry["personalEmail"];
			$nuhEmail = $entry["nuhEmail"];
			$address = $entry["address"];
			$homePhone = $entry["homePhone"];
			$handPhone = $entry["handPhone"];
			$rotation = $entry["rotation"];
			$role = $entry["role"];
			$rosterMonster = $entry["rosterMonster"];
			include('../bothUser.php');
		}
		include('../selectUser.php');
		$tableNames['user'] = getUserTable($conn);
	}
	if(isset($table["patient"])) {
		foreach ($table["patient"] as $entry) {
			$patientId = $entry["patientId"];
			$dob = $entry["dob"];
			include('../bothPatient.php');
		}
		include('../selectPatient.php');
		$tableNames['patient'] = getPatientTable($conn);
	}
	if(isset($table["activities"])) {
		foreach ($table["activities"] as $entry) {
			$activityId = $entry["activityId"];
			$title = $entry["title"];
			$registrationDeadlineTime = $entry["registrationDeadlineTime"];
			$registrationDeadlineDate = $entry["registrationDeadlineDate"];
			$eventDate = $entry["eventDate"];
			$eventStartTime = $entry["eventStartTime"];
			$eventEndTime = $entry["eventEndTime"];
			$maxVacancy = $entry["maxVacancy"];
			$curVacancy = $entry["curVacancy"];
			$location = $entry["location"];
			$description = $entry["description"];
			$link = $entry["link"];
			$important = $entry["important"];
			include('../bothActivities.php');
		}
		include('../selectActivities.php');
		$tableNames['activities'] = getActivitiesTable($conn);
	}
	if(isset($table["notes"])) {
		foreach ($table["notes"] as $entry) {
			$noteId = $entry["noteId"];
			$noteTitle = $entry["noteTitle"];
			$date = $entry["date"];
			$content = $entry["content"];
			$userId = $entry["userId"];
			$role = $entry["role"];
			include('../bothNotes.php');
		}
		include('../selectNotes.php');
		$tableNames['notes'] = getNotesTable($conn);
	}
	if(isset($table["procedureTable"])) {
		foreach ($table["procedureTable"] as $entry) {
			$procedureId = $entry["procedureId"];
			$name = $entry["name"];
			$patientId = $entry["patientId"];
			$date = $entry["date"];
			$time = $entry["time"];
			$remarks = $entry["remarks"];
			$outcome = $entry["outcome"];
			include('../bothProcedure.php');
		}
		include('../selectProcedure.php');
		$tableNames['procedureTable'] = getProcedureTable($conn);
	}
	if(isset($table["rosterAssignment"])) {
		foreach ($table["rosterAssignment"] as $entry) {
			$rosterId = $entry["rosterId"];
			$date = $entry["date"];
			include('../bothRosterAssignment.php');
		}
		include('../selectRosterAssignment.php');
		$tableNames['rosterAssignment'] = getRosterAssignmentTable($conn);
	}
	if(isset($table["workingHours"])) {
		foreach ($table["workingHours"] as $entry) {
			$workingHoursId = $entry["workingHoursId"];
			$dayIn = $entry["dayIn"];
			$timeIn = $entry["timeIn"];
			$dayOut = $entry["dayOut"];
			$timeOut = $entry["timeOut"];
			$userId = $entry["userId"];
			$role = $entry["role"];
			include('../bothWorkingHours.php');
		}
		include('../selectWorkingHours.php');
		$tableNames['workingHours'] = getWorkingHoursTable($conn);
	}
	if(isset($table["assessment"])) {
		foreach ($table["assessment"] as $entry) {
			$assessmentId = $entry["assessmentId"];
			$title = $entry["title"];
			$date = $entry["date"];
			$grade = $entry["grade"];
			$feedback = $entry["feedback"];
			$userId = $entry["userId"];
			$role = $entry["role"];
			include('../bothAssessment.php');
		}
		include('../selectAssessment.php');
		$tableNames['assessment'] = getAssessmentTable($conn);
	}
	if(isset($table["exams"])) {
		foreach ($table["exams"] as $entry) {
			$moduleId = $entry["moduleId"];
			$moduleName = $entry["moduleName"];
			$date = $entry["date"];
			$time = $entry["time"];
			$location = $entry["location"];
			include('../bothExams.php');
		}
		include('../selectExams.php');
		$tableNames['exams'] = getExamsTable($conn);
	}
	if(isset($table["forms"])) {
		foreach ($table["forms"] as $entry) {
			$formsId = $entry["formsId"];
			$name = $entry["name"];
			$dateReleased = $entry["dateReleased"];
			$deadline = $entry["deadline"];
			$url = $entry["url"];
			$type = $entry["type"];
			include('../bothForms.php');
		}
		include('../selectForms.php');
		$tableNames['forms'] = getFormsTable($conn);
	}
	if(isset($table["leaveApp"])) {
		foreach ($table["leaveApp"] as $entry) {
			$leaveAppId = $entry["leaveAppId"];
			$startDate = $entry["startDate"];
			$endDate = $entry["endDate"];
			$status = $entry["status"];
			$userId = $entry["userId"];
			$role = $entry["role"];
			include('../bothLeaveApp.php');
		}
		include('../selectLeaveApp.php');
		$tableNames['leaveApp'] = getLeaveAppTable($conn);
	}
	if(isset($table["rosterBlockOutReq"])) {
		foreach ($table["rosterBlockOutReq"] as $entry) {
			$reqId = $entry["reqId"];
			$submitDate = $entry["submitDate"];
			$reqDate = $entry["reqDate"];
			$reason = $entry["reason"];
			$status = $entry["status"];
			$userId = $entry["userId"];
			$role = $entry["role"];
			include('../bothRosterBlockOutReq.php');
		}
		include('../selectRosterBlockOutReq.php');
		$tableNames['rosterBlockOutReq'] = getRosterBlockOutReq($conn);
	}
	if(isset($table["traineeExams"])) {
		foreach ($table["traineeExams"] as $entry) {
			$moduleId = $entry["moduleId"];
			$seatNo = $entry["seatNo"];
			$traineeId = $entry["traineeId"];
			$role = $entry["role"];
			include('../bothTraineeExams.php');
		}
		include('../selectTraineeExams.php');
		$tableNames['traineeExams'] = getTraineeExamsTable($conn);
	}
	if(isset($table["userActivities"])) {
		foreach ($table["userActivities"] as $entry) {
			$userId = $entry["userId"];
			$role = $entry["role"];
			$activityId = $entry["activityId"];
			$status = $entry["status"];
			include('../bothUserActivities.php');
		}
		include('../selectUserActivities.php');
		$tableNames['userActivities'] = getUserActivitiesTable($conn);
	}
	if(isset($table["trainerAssessment"])) {
		foreach ($table["trainerAssessment"] as $entry) {
			$trainerId = $entry["trainerId"];
			$assessmentId = $entry["assessmentId"];
			$role = $entry["role"];
			include('../bothTrainerAssessment.php');
		}
		include('../selectTrainerAssessment.php');
		$tableNames['trainerAssessment'] = getTrainerAssessmentTable($conn);
	}
	if(isset($table["userForms"])) {
		foreach ($table["userForms"] as $entry) {
			$formsId = $entry["formsId"];
			$status = $entry["status"];
			$userId = $entry["userId"];
			$role = $entry["role"];
			include('../bothUserForms.php');
		}
		include('../selectUserForms.php');
		$tableNames['userForms'] = getUserFormsTable($conn);
	}
	if(isset($table["userProcedure"])) {
		foreach ($table["userProcedure"] as $entry) {
			$procedureId = $entry["procedureId"];
			$userId = $entry["userId"];
			$role = $entry["role"];
			include('../bothUserProcedure.php');
		}
		include('../selectUserProcedure.php');
		$tableNames['userProcedure'] = getUserProcedureTable($conn);
	}
	if(isset($table["userRosterAssignment"])) {
		foreach ($table["userRosterAssignment"] as $entry) {
			$rosterId = $entry["rosterId"];
			$status = $entry["status"];
			$userId = $entry["userId"];
			$role = $entry["role"];
			include('../bothUserRosterAssignment.php');
		}
		include('../selectUserRosterAssignment.php');
		$tableNames['userRosterAssignment'] = getUserRosterAssignmentTable($conn);
	}

	//Alternate idea: Echo json arrays, see if can receive via QT
	// See this: http://www.qtcentre.org/threads/41006-Send-Qt-HTTP-Post-and-read-back-the-response
	// Also see this: http://stackoverflow.com/questions/4064444/returning-json-from-a-php-script
	//TO IMPLEMENT: Put all tables eventually...
	/*
	 include('../selectUser.php');
	 $tableNames['user'] = getUserTable($conn);

	 include('../selectProcedure.php');
	 $tableNames['procedure'] = getProcedureTable($conn);

	 include('../selectUserActivities.php');
	 $tableNames['userActivities'] = getUserActivitiesTable($conn);

	 include('../selectActivities.php');
	 $tableNames['activities'] = getActivitiesTable($conn);

	 include('../selectRosterAssignment.php');
	 $tableNames['rosterassignment'] = getRosterAssignmentTable($conn);

	 include('../selectRosterBlockOutReq.php');
	 $tableNames['rosterblockoutreg'] = getRosterBlockOutReq($conn);

	 include('../selectTraineeExams.php');
	 $tableNames['traineeexams'] = getTraineeExamsTable($conn);

	 include('../selectTrainerAssessment.php');
	 $tableNames['trainerassessment'] = getTrainerAssessmentTable($conn);

	 include('../selectUserProcedure.php');
	 $tableNames['userprocedure'] = getUserProcedureTable($conn);

	 include('../selectUserForms.php');
	 $tableNames['userforms'] = getUserFormsTable($conn);

	 include('../selectUserRosterAssignment.php');
	 $tableNames['userrosterassignment'] = getUserRosterAssignmentTable($conn);

	 include('../selectWorkingHours.php');
	 $tableNames['workinghours'] = getWorkingHoursTable($conn);

	 include('../selectAssessment.php');
	 $tableNames['assessment'] = getAssessmentTable($conn);

	 include('../selectExams.php');
	 $tableNames['exams'] = getExamsTable($conn);

	 include('../selectForms.php');
	 $tableNames['forms'] = getFormsTable($conn);

	 include('../selectLeaveApp.php');
	 $tableNames['leaveapplication'] = getLeaveAppTable($conn);

	 include('../selectNotes.php');
	 $tableNames['notes'] = getNotesTable($conn);

	 include('../selectPatient.php');
	 $tableNames['patient'] = getPatientTable($conn);
	 */


	if (isset($tableNames)) {
		$jsonfile['tableNames'] = $tableNames;

		$jsonClient = json_encode($jsonfile);
		echo $jsonClient;
	} else {
		echo 'failed';
	}

	/*
	 //Output entire DB
	 //Pass output into a JSON file and return it
	 $fp = fopen('results.json', 'w+') or die("Error opening output file");

	 include('selectProcedure.php');
	 fwrite($fp, "{\n \"procedure\": ");
	 fwrite($fp, json_encode($_SESSION["tablerows"]));

	 include('selectRosterAssignment.php');
	 fwrite($fp, ",\n \"rosterassignment\": ");
	 fwrite($fp, json_encode($_SESSION["tablerows"]));

	 include('selectRosterBlockOutReg.php');
	 fwrite($fp, ",\n \"rosterblockoutreg\": ");
	 fwrite($fp, json_encode($_SESSION["tablerows"]));

	 include('selectTraineeExams.php');
	 fwrite($fp, ",\n \"traineeexams\": ");
	 fwrite($fp, json_encode($_SESSION["tablerows"]));

	 include('selectTrainerAssessment.php');
	 fwrite($fp, ",\n \"trainerassessment\": ");
	 fwrite($fp, json_encode($_SESSION["tablerows"]));

	 include('selectUser.php');
	 fwrite($fp, ",\n \"user\": ");
	 fwrite($fp, json_encode($_SESSION["tablerows"]));

	 include('selectUserActivities.php');
	 fwrite($fp, ",\n \"useractivities\": ");
	 fwrite($fp, json_encode($_SESSION["tablerows"]));

	 include('selectUserForms.php');
	 fwrite($fp, ",\n \"userforms\": ");
	 fwrite($fp, json_encode($_SESSION["tablerows"]));

	 include('selectUserProcedure.php');
	 fwrite($fp, ",\n \"userprocedure\": ");
	 fwrite($fp, json_encode($_SESSION["tablerows"]));

	 include('selectUserRosterAssignment.php');
	 fwrite($fp, ",\n \"userrosterassignment\": ");
	 fwrite($fp, json_encode($_SESSION["tablerows"]));

	 include('selectWorkingHours.php');
	 fwrite($fp, ",\n \"workinghours\": ");
	 fwrite($fp, json_encode($_SESSION["tablerows"]));

	 include('selectActivities.php');
	 fwrite($fp, ",\n \"activities\": ");
	 fwrite($fp, json_encode($_SESSION["tablerows"]));

	 include('selectAssessment.php');
	 fwrite($fp, ",\n \"assessment\": ");
	 fwrite($fp, json_encode($_SESSION["tablerows"]));

	 include('selectExams.php');
	 fwrite($fp, ",\n \"exams\": ");
	 fwrite($fp, json_encode($_SESSION["tablerows"]));

	 include('selectForms.php');
	 fwrite($fp, ",\n \"forms\": ");
	 fwrite($fp, json_encode($_SESSION["tablerows"]));

	 include('selectLeaveApp.php');
	 fwrite($fp, ",\n \"leaveapplication\": ");
	 fwrite($fp, json_encode($_SESSION["tablerows"]));

	 include('selectNotes.php');
	 fwrite($fp, ",\n \"notes\": ");
	 fwrite($fp, json_encode($_SESSION["tablerows"]));

	 include('selectPatient.php');
	 fwrite($fp, ",\n \"patient\": ");
	 fwrite($fp, json_encode($_SESSION["tablerows"]));

	 fwrite($fp, "\n}");
	 fclose($fp);

	 readfile($file);
	 */
} catch(PDOException $e) {
	echo "Connection failed: " . $e->getMessage();
}

//Cleaning up
// remove all session variables
session_unset();

// destroy the session
session_destroy();

$conn = null;
?>
