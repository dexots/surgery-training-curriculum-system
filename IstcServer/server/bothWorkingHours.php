<?php
    $stmt = $conn->prepare("INSERT INTO workingHours (userId, role, workingHoursId, dayIn, timeIn, dayOut, timeOut) VALUES(:userId, :role, :workingHoursId, :dayIn, :timeIn, :dayOut, :timeOut) ON DUPLICATE KEY UPDATE dayIn=:dayIn, timeIn=:timeIn, dayOut=:dayOut, timeOut=:timeOut;");
    $stmt->bindParam(':userId', $userId);
    $stmt->bindParam(':role', $role);
    $stmt->bindParam(':workingHoursId', $workingHoursId);
    $stmt->bindParam(':dayIn', $dayIn);
	$stmt->bindParam(':timeIn', $timeIn);
	$stmt->bindParam(':dayOut', $dayOut);
	$stmt->bindParam(':timeOut', $timeOut);
    $stmt->execute();
?>
