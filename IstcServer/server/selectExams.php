<?php
function getExamsTable($c) {
    $stmt = $c->prepare("SELECT moduleId, moduleName, date, DATE_FORMAT(time, '%H:%i') AS time, location FROM exams;");
	$stmt->execute();
	$examsUpdated = $stmt->fetchAll(PDO::FETCH_ASSOC);
	return $examsUpdated;
	}
?>