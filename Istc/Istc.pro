#-------------------------------------------------
#
# Project created by QtCreator 2014-09-07T14:10:44
#
#-------------------------------------------------

QT       += core gui
QT       += core sql
QT       += printsupport
QT       += core network


#temporarily include this back to do nus open id login (fast method)
QT       += webkit webkitwidgets


greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Istc
TEMPLATE = app

INCLUDEPATH +=  $$_PRO_FILE_PWD_/src \
                $$_PRO_FILE_PWD_/include

SOURCES += main.cpp\
    src/gui/mainwindow.cpp \
    src/gui/catebox.cpp \
    src/gui/mycalendarwidget.cpp \
    src/gui/botrightmain.cpp \
    src/gui/loginui.cpp \
    src/gui/subcate.cpp \
    src/gui/resourcewidget.cpp \
    src/gui/topwidget.cpp \
    src/gui/botleftwidget.cpp \
    src/gui/catetable.cpp \
    src/gui/clickablewidget.cpp \
    src/gui/logwidget.cpp \
    src/gui/trainingwidget.cpp \
    src/gui/profilewidget.cpp \
    src/gui/rosterwidget.cpp \
    src/gui/researchwidget.cpp \
    src/gui/popupmsgbox.cpp \
    src/gui/yourrosterwidget.cpp \
    src/gui/rosreq.cpp \
    src/gui/progressbar.cpp \
    src/gui/workingwidget.cpp \
    src/gui/patientbox.cpp \
    src/gui/milestonewidget.cpp \
    src/gui/inputbox.cpp \
    src/gui/activitywidget.cpp \
    src/gui/examinationwidget.cpp \
    src/gui/formswidget.cpp \
    src/gui/personalwidget.cpp \
    src/gui/curriculumwidget.cpp \
    src/gui/leavewidget.cpp \
    src/gui/assessmentwidget.cpp \
    src/gui/noteswidget.cpp \
    src/gui/researchinstance.cpp \
    src/gui/notesbox.cpp \
    src/gui/procedurecodewidget.cpp \
    src/gui/logoutbox.cpp \
    src/gui/hyperlinkwidget.cpp \
    src/gui/errorbox.cpp \
    src/gui/popupbox.cpp \
    src/gui/browser.cpp \
    src/gui/generalpopupbox.cpp \
    src/gui/syncbox.cpp \
    src/gui/settingsbox.cpp \
    src/gui/profilebox.cpp \
    src/gui/searchpopupbox.cpp \
    src/gui/researchbox.cpp \
    src/gui/rosreqbox.cpp \
    src/gui/rostermonsterwidget.cpp \
    src/gui/eventbox.cpp \
    src/gui/activitybox.cpp \
    src/gui/tablewidget.cpp \
    src/model/trainingmodel.cpp \
    src/model/logbookmodel.cpp \
    src/dbclient/localdatabaseclient.cpp \
    src/dbclient/networkdatabaseclient.cpp \
    src/objects/notes.cpp \
    src/objects/userdetails.cpp \
    src/objects/form.cpp \
    src/objects/exam.cpp \
    src/objects/proceduralcode.cpp \
    src/objects/eventitem.cpp \
    src/objects/rosterassignobj.cpp \
    src/objects/rosterrequestobj.cpp \
    src/objects/workinghoursobject.cpp \
    src/objects/leaveobj.cpp \
    src/objects/assessmentobj.cpp \
    src/model/rostermodel.cpp \
    src/gui/addrosterbox.cpp \
    src/model/personalmodel.cpp \
    src/objects/serverrequestobj.cpp \
    src/gui/assessmentbox.cpp \
    src/gui/resourcesbox.cpp \
    src/gui/researchaddbox.cpp \
    src/gui/milestonebox.cpp \
    src/gui/login.cpp \
    src/gui/loginbrowser.cpp \
    src/gui/clockinbox.cpp



HEADERS  += include/gui/mainwindow.h \
    include/gui/catebox.h \
    include/gui/mycalendarwidget.h \
    include/gui/botrightmain.h \
    include/gui/loginui.h \
    include/gui/subcate.h \
    include/gui/resourcewidget.h \
    include/gui/topwidget.h \
    include/gui/botleftwidget.h \
    include/gui/catetable.h \
    include/gui/clickablewidget.h \
    include/gui/logwidget.h \
    include/gui/trainingwidget.h \
    include/gui/profilewidget.h \
    include/gui/rosterwidget.h \
    include/gui/researchwidget.h \
    include/gui/popupmsgbox.h \
    include/gui/yourrosterwidget.h \
    include/gui/rosreq.h \
    include/gui/progressbar.h \
    include/gui/workingwidget.h \
    include/gui/patientbox.h \
    include/gui/milestonewidget.h \
    include/gui/inputbox.h \
    include/gui/activitywidget.h \
    include/gui/examinationwidget.h \
    include/gui/formswidget.h \
    include/gui/personalwidget.h \
    include/gui/curriculumwidget.h \
    include/gui/leavewidget.h \
    include/gui/assessmentwidget.h \
    include/gui/noteswidget.h \
    include/gui/researchinstance.h \
    include/gui/notesbox.h \
    include/gui/procedurecodewidget.h \
    include/gui/logoutbox.h \
    include/gui/popupboxincludes.h \
    include/gui/popupbox.h \
    include/gui/hyperlinkwidget.h \
    include/gui/errorbox.h \
    include/gui/browser.h \
    include/gui/generalpopupbox.h \
    include/gui/syncbox.h \
    include/gui/settingsbox.h \
    include/gui/profilebox.h \
    include/gui/searchpopupbox.h \
    include/gui/researchbox.h \
    include/gui/rosreqbox.h \
    include/gui/rostermonsterwidget.h \
    include/gui/eventbox.h \
    include/gui/activitybox.h \
    include/gui/tablewidget.h \
    include/logging/messagehandler.h \
    include/model/trainingmodel.h \
    include/model/logbookmodel.h \
    include/dbclient/localdatabaseclient.h \
    include/dbclient/networkdatabaseclient.h \
    include/objects/notes.h \
    include/objects/userdetails.h \
    include/objects/form.h \
    include/objects/exam.h \
    include/objects/proceduralcode.h \
    include/objects/eventitem.h \
    include/objects/rosterassignobj.h \
    include/objects/rosterrequestobj.h \
    include/objects/workinghoursobject.h \
    include/objects/leaveobj.h \
    include/objects/assessmentobj.h \
    include/model/rostermodel.h \
    include/gui/addrosterbox.h \
    include/model/personalmodel.h \
    include/objects/serverrequestobj.h \
    include/gui/assessmentbox.h \
    include/gui/resourcesbox.h \
    include/gui/researchaddbox.h \
    include/gui/milestonebox.h \
    include/gui/login.h \
    include/gui/loginbrowser.h \
    include/gui/clockinbox.h


RESOURCES += \
    IconResources.qrc

OTHER_FILES += \
    mainwindow.temp \
    procedurecode.txt


CONFIG(release, debug|release) {
    DEFINES += _RELEASE
    #message(Release)
}

CONFIG(debug, debug|release) {
    DEFINES += _DEBUG
    #message(Debug)
}

testlocal {
    #message(Test build)
    DEFINES += _TEST

    QT += testlib
    TARGET = UnitTests

    HEADERS += include/testlocal/testlogbookmodel.h \
               include/testlocal/testprofilebox.h \
               include/testlocal/testpatientbox.h \
               include/testlocal/testlocaldb.h \
               include/testlocal/testactivitywidget.h \
               include/testlocal/generatestring.h

    SOURCES += src/testlocal/testlogbookmodel.cpp \
               src/testlocal/testprofilebox.cpp \
               src/testlocal/testpatientbox.cpp \
               src/testlocal/testlocaldb.cpp \
               src/testlocal/testactivitywidget.cpp \
               src/testlocal/generatestring.cpp


    #QMAKE_CXXFLAGS += -g -Wall -fprofile-arcs -ftest-coverage -O0
    #QMAKE_LFLAGS += -g -Wall -fprofile-arcs -ftest-coverage  -O0


    #LIBS += \
    #    -lgcov
}

DISTFILES += \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/AndroidManifest.xml \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew \
    android/gradlew.bat

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android

TARGET.CAPABILITY += NetworkServices
