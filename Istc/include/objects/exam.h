#ifndef EXAM_H
#define EXAM_H

#include <cassert>
#include <QString>
#include <QDate>
#include <QTime>

class QString;
class QDate;
class QTime;

namespace Gui {
    class Exam;
}

class Exam {
public:
    explicit Exam();
    ~Exam();

public:
    void setModuleCode(QString moduleCode);
    void setModuleName(QString moduleName);
    void setExamDate(QDate examDate);
    void setExamTime(QTime examTime);
    void setExamLocation(QString examLocation);
    void setSeatNo(QString seatNo);
    QString getModuleCode();
    QString getModuleName();
    QDate getExamDate();
    QTime getExamTime();
    QString getExamLocation();
    QString getSeatNo();

private:
    QString _moduleCode;
    QString _moduleName;
    QString _examLocation;
    QString _seatNo;

    QDate _examDate;
    QTime _examTime;
};

#endif // EXAM_H
