#ifndef ASSESSMENTOBJ
#define ASSESSMENTOBJ

#include <cassert>

#include <QDate>
#include <QString>
#include <QVector>

class QDate;
class QString;

namespace model {
    class AssessmentObj;
}

class AssessmentObj {
public:
    explicit AssessmentObj();
    ~AssessmentObj();

public:
    QDate getAssessDate();
    QString getAssessTitle();
    QString getGrade();
    QString getFeedback();
    QString getAssessId();
    QString getTraineeId();
    QString getGradersString();
    QVector <QString> *getGraders();
    void setAssessDate(QDate assessDate);
    void setAssessTitle(QString asseQssTitle);
    void setGrade(QString grade);
    void setFeedback(QString feedback);
    void addGrader(QString grader);
    void setAssessId(QString assessId);
    void setTraineeId(QString traineeId);
    void setGraders(QVector <QString>* graders);

private:
    QString _assessId;
    QDate _assessDate;
    QString _assessTitle;
    QString _grade;
    QString _feedback;
    QString _traineeId;
    QVector <QString> * _graders;

};

#endif // ASSESSMENTOBJ

