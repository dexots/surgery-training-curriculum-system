#ifndef WORKINGHOURSOBJECT_H
#define WORKINGHOURSOBJECT_H

#include <cassert>

#include <QDateTime>
#include <QString>

class QDateTime;
class QString;

namespace model {
    class WorkingHoursObject;
}

class WorkingHoursObject {
public:
    explicit WorkingHoursObject();
    ~WorkingHoursObject();

public:
    QDateTime getCheckInTime();
    QDateTime getCheckOutTime();
    qint64 getNoOfSec();
    qint64 getNoOfHours();
    QString getWorkingHourId();
    void setCheckInTime(QDateTime checkInTime);
    void setCheckOutTime(QDateTime checkOutTime);
    void setNoOfSec(qint64 s);
    void setWorkingHourId(QString workingHourId);
    bool isEmpty();
    bool isEqual(WorkingHoursObject other);

private:
    QDateTime _checkInTime;
    QDateTime _checkOutTime;
    QString _workingHourId;
    qint64 _noOfSec;

public:
    static const int MIN_TO_SEC;
    static const int HOUR_TO_MIN;

private:
    static const int ERROR_NO_OF_SEC;
};


#endif // WORKINGHOURSOBJECT_H
