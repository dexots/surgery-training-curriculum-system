#ifndef ROSTERASSIGNOBJ_H
#define ROSTERASSIGNOBJ_H

#include <cassert>

#include <QString>
#include <QDate>

class QString;
class QDate;

namespace model {
    class RosterAssignObj;
}

class RosterAssignObj {
public:
    explicit RosterAssignObj();
    explicit RosterAssignObj(QDate date,
                             QString status,
                             QString rosterAssignId,
                             QString userId,
                             QString userRole);
    ~RosterAssignObj();

public:
    QDate getAssignedDate ();
    QString getStatus ();
    QString getRosterAssignId();
    QString getUserId();
    QString getRole();
    void setAssignedDate(QDate date);
    void setStatus(QString status);
    void setRosterAssignId(QString rosterAssignId);
    void setUserId(QString userId);
    void setUserRole(QString userRole);

private:
    void initializeClass(QDate date,
                         QString status,
                         QString rosterAssignId,
                         QString userId,
                         QString role);

private:
    QDate _assignedDate;
    QString _status;
    QString _rosterAssignId;
    QString _userId;
    QString _userRole;

public:
    static const QString STATUS_COMPLETED;
    static const QString STATUS_NOT_COMPLETED;
    static const QString STATUS_WAIT;
};



#endif // ROSTERASSIGNOBJ_H
