#ifndef FORM_H
#define FORM_H

#include <cassert>
#include <QWidget>
#include <QDate>
#include <QString>
#include <QLabel>

#include "gui/clickablewidget.h"

class QWidget;
class QDate;
class QString;
class QLabel;
class ClickableWidget;

namespace Gui {
    class Form;
}

class Form : public QWidget {
    Q_OBJECT

public:
    explicit Form();
    ~Form();

public:
    void setFormName(QString formName);
    void setReleaseDate(QDate releaseDate);
    void setDeadline(QDate deadline);
    void setFormStatus(QString formStatus);
    void setFormUrl(QString formUrl);
    void setType(QString type);
    QString getFormName();
    QDate getReleaseDate();
    QDate getDeadline();
    QString getOnlineFormStatus();
    QString getUrlAddress();
    QString getType();
    ClickableWidget* getDownloadIcon();
    ClickableWidget* getClickableTitle();

signals:
    void loadWebpage(QString url);

private slots:
    void handleTitleClicked();

private:
    QString _formName;
    QString _formStatus;
    QDate _releaseDate;
    QDate _deadline;
    QString _urlAddress;
    QString _type;
    ClickableWidget* _clickableDownloadIcon;
    ClickableWidget* _clickableTitle;

private:
    static const QString FORM_TYPE_ONLINE;
    static const QString FORM_TYPE_SOFTCOPY;

};

#endif // FORM_H
