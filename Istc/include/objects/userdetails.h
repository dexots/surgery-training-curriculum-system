#ifndef USERDETAILS
#define USERDETAILS

#include <cassert>

#include <QString>
#include <QObject>

namespace Gui {
    class UserDetails;
}

class UserDetails : public QObject {
private:
    explicit UserDetails();
    ~UserDetails();

public:
    static UserDetails* getObject();
    static void deleteObject();

public:
    QString getUserName();
    QString getMatricNo();
    QString getUserRole();
    QString getUserEmail();
    QString getPersonalEmail();
    QString getRotation();
    QString getAddress();
    QString getHomePhone();
    QString getHandPhone();
    bool getIsRosterMonster();
    void setUserName(QString username);
    void setMatricNo(QString matricNo);
    void setUserRole(QString userRole);
    void setUserEmail(QString userEmail);
    void setPersonalEmail(QString personalEmail);
    void setRotation(QString rotation);
    void setAddress(QString address);
    void setHomePhone(QString homePhone);
    void setHandPhone(QString handPhone);
    void setIsRosterMonster(bool isRosterMonster);
    bool isNotInitialized();

private:
    void initiateClass();
    void clearGarbage();

private:
    QString _userName;
    QString _userMatricNo;
    QString _userRole;
    QString _userEmail;
    QString _personalEmail;
    QString _rotation;
    QString _address;
    QString _homePhone;
    QString _handPhone;
    bool _isRosterMonster;

public:
    static const QString ROLE_TRAINER;
    static const QString ROLE_TRAINEE;
    static const QString ROLE_ADMIN;
    static const QString STRING_EMPTY;

private:
    static UserDetails* _userDetails;

};

#endif // USERDETAILS

