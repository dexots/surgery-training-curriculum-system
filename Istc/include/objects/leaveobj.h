#ifndef LEAVEOBJ
#define LEAVEOBJ

#include <cassert>

#include <QDate>
#include <QString>

class QDate;
class QString;

namespace model {
    class LeaveObj;
}

class LeaveObj {
public:
    explicit LeaveObj();
    explicit LeaveObj(QDate startDate,
                      QDate endDate,
                      QString applicationStatus,
                      QString leaveId);
    ~LeaveObj();

public:
    void setStartDate(QDate startDate);
    void setEndDate(QDate endDate);
    void setApplicationStatus(QString applicationStatus);
    void setLeaveId(QString leaveId);
    QDate getStartDate();
    QDate getEndDate();
    QString getApplicationStatus();
    QString getLeaveId();
    qint64 getNumOfDates();

private:
    QDate _startDate;
    QDate _endDate;
    QString _applicationStatus;
    QString _leaveId;

};


#endif // LEAVEOBJ

