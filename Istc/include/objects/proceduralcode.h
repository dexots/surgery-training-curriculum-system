#ifndef PROCEDURALCODE_H
#define PROCEDURALCODE_H

#include <cassert>

#include <QString>
#include <QStringList>
#include <QPixmap>
#include <QDate>
#include <QTime>
#include <QLabel>
#include <QVector>
#include <QHash>
#include "gui/clickablewidget.h"
#include "gui/patientbox.h"

class QString;
class QPixmap;
class QDate;
class QTime;
class QLabel;
class ClickableWidget;
class PatientBox;

namespace Gui {
    class ProceduralCode;
}

class ProceduralCode : public QWidget {
    Q_OBJECT

public:
    explicit ProceduralCode();
    explicit ProceduralCode(QString procedureDate, QString procedureTime, QString patientID,
                            QString patientDOB, int procedureCodeIndex, QString outcome,
                            QString procedureRemarks);
    ~ProceduralCode();

public:
    QDate getProcedureDate();
    QDate getPatientDOB();
    QTime getProcedureTime();
    QString getPatientID();
    QString getProcedureCode();
    QString getProcedureRemarks();
    QString getProcedureOutcome();
    QString getProcedureID();
    void setProcedureDate(QString procedureDate);
    void setProcedureDate(QDate procedureDate);
    void setProcedureTime(QString procedureTime);
    void setProcedureTime(QTime procedureTime);
    void setProcedureID(QString procedureID);
    void setPatientID(QString patientID);
    void setPatientDOB(QString patientDOB);
    void setPatientDOB(QDate patientDOB);
    bool setProcedureCodeName(QString procedureName);
    void setProcedureCodeIndex(int procedureCodeIndex);
    void setProcedureOutcome(QString outome);
    void setProcedureRemarks(QString procedureRemarks);

    ClickableWidget* getClickableEditIcon();

private:
    void initializeQString();
    void setupProcedureCodeData();
    void importProcedureCodeFile();
    void setupProcedureCodeMap(QVector <QString> *codes);
    void constructClickableEditIcon();
    void initializeClass(QString procedureDate, QString procedureTime, QString patientID,
                         QString patientDOB, int procedureCodeIndex,
                         QString outcome, QString procedureRemarks);
private:
    ClickableWidget *_clickableEditIcon;

    QDate _procedureDate;
    QDate _patientDOB;

    QTime _procedureTime;
    QString _patientID;
    QString _procedureRemarks;
    QString _procedureID;
    QString _outcome;

    int _procedureCodeIndex;

    QVector<QString> *_procedureCode;
    QHash<QString, int> _hash;

private:
    static const QString ICON_EDIT;
    static const QString STRING_SPACE;
    static const QString STRING_COLON;

signals:
    void clickedUpdateButton();

private slots:
    void popUpWindow();
    void updateProceduralCode(QVector<QString>);
};

#endif // PROCEDURALCODE_H

