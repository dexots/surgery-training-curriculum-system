#ifndef EVENTITEM_H
#define EVENTITEM_H

#include <cassert>
#include <QPixmap>
#include <QLabel>
#include <QVBoxLayout>
#include <QFrame>
#include <QStringList>
#include <QDate>
#include <QTime>

#include "gui/clickablewidget.h"
#include "gui/activitybox.h"
#include "gui/eventbox.h"
#include "objects/userdetails.h"

class QPixmap;
class QLabel;
class QVBoxLayout;
class QFrame;
class QStringList;
class QDate;
class QTime;

class ClickableWidget;
class ActivityBox;
class EventBox;
class UserDetails;

namespace Gui{
    class EventItem;
}

class EventItem:public QWidget {
    Q_OBJECT

public:
    explicit EventItem(QDate eventDate, QTime eventStartTime,QTime eventEndTime,
                       QString eventName, QString eventLocation,
                       QString eventTrainer, int curVacancy, int maxVacancy,
                       QString eventDescription, QString eventLink,
                       bool isImportant, bool isClosed = false);
    ~EventItem();

public:
    int getCurrentVacancy();
    int getMaxVacancy();
    QString getActivityId();
    QString getDescription();
    QString getLink();
    QString getLocation();
    QString getName();
    QString getTrainer();
    QString getVacancy();
    QDate getDate();
    QTime getEndTime();
    QTime getStartTime();
    QDateTime getRegistrationDeadline();
    bool getIsImportant();
    bool getIsRegistered();
    bool getIsClosed();
    ClickableWidget *getClickableEditIcon();
    ClickableWidget *getClickableSummary();
    ClickableWidget *getClickableTitle();
    void setActivityId(QString activityId);
    void setIsUserRegistered(bool isRegistered);
    void setIsClosed(bool isClosed);
    void setRegistrationDeadline(QDateTime registrationDeadline);
    void constructClickableSummary();
    void constructClickableTitle();
    void constructClickableEditIcon();

private:
    void initializeQString();
    void addLabelsToLayout();
    void setClickableLayoutAndFrame();
    void setDate(QDate eventDate);
    void setStartTime(QTime eventStartTime);
    void setEndTime(QTime eventEndTime);
    void setName(QString eventName);
    void setLocation(QString eventLocation);
    void setTrainer(QString eventTrainer);
    void setVacancy(int curVacancy, int maxVacancy);
    void setDescription(QString eventDescription);
    void setLink(QString eventLink);
    void setIsImportant(bool isImportant);

//class variables
private:
    ClickableWidget *_clickableSummary;
    ClickableWidget *_clickableTitle;
    ClickableWidget *_clickableEditIcon;

    QDate _eventDate;

    QTime _eventStartTime;
    QTime _eventEndTime;

    QDateTime _registrationDeadline;

    QString _eventName;
    QString _eventLocation;
    QString _eventTrainer;
    QString _eventDescription;
    QString _eventLink;
    QString _eventVacancy;

    QString _activityId;
    int _curVacancy;
    int _maxVacancy;

    bool _isRegistered;
    bool _isImportant;
    bool _isClosed;

signals:
    void clickedEventItem();
    void clickedRegisterButton(QString activityId, bool isWantedToRegister);
    void isUpdated(QString activityId);

private slots:
    void popUpWindow();
    void popUpRegisterWindow();
    void popUpActivityBox();
    void updateActivityInfo(QVector<QString> info, QDate date, QDateTime registrationDeadline,
                              QVector<QTime> time, int maxVacancy, bool isImportant);
    void handleUserClickRegister(bool isRegister);

//constants
private:
    static const QString FORMAT_TIME;
    static const QString FORMAT_VACANCYSTRING;
    static const QString ICON_EDIT;
    static const QString NEWLINE;
    static const QString LABEL_DATE;
    static const QString LABEL_TIME;
    static const QString LABEL_LOCATION;
    static const QString LABEL_TRAINER;
    static const QString LABEL_VACANCY;
    static const QString LABEL_DESCRIPTION;
    static const QString STRING_EMPTY;
    static const QString STRING_SPACE;
    static const QString STRING_COLON;

    static const int SIZE_FONT;
};

#endif // EVENTITEM_H
