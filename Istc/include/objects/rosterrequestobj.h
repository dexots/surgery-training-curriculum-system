#ifndef ROSTERREQUESTOBJ_H
#define ROSTERREQUESTOBJ_H

#include <cassert>
#include <QWidget>
#include <QDebug>
#include <QString>
#include <QDate>
#include <QLabel>
#include "gui/clickablewidget.h"

class QString;
class QDate;
class QDebug;
class QLabel;
class ClickableWidget;

namespace model {
    class RosterRequestObj;
}

class RosterRequestObj:public QWidget {
    Q_OBJECT

public:
    explicit RosterRequestObj();
    ~RosterRequestObj();

public:
    QDate getDateSubmit();
    QDate getDateRequest();
    QString getReason();
    QString getStatus();
    QString getUserId();
    QString getRosterRequestId();
    void setDateSubmit(QDate dateSubmit);
    void setDateRequest(QDate dateRequest);
    void setReason(QString reason);
    void setStatus(QString status);
    void setUserId(QString userId);
    void setRosterRequestId(QString rosterRequestId);

private:
    QString _userId;
    QDate _dateSubmit;
    QDate _dateRequest;
    QString _reason;
    QString _status;
    QString _rosterRequestId;

//constants
private:
    static const QString STATUS_APPROVED;
    static const QString STATUS_DISAPPROVED;
    static const QString STATUS_PENDINGS;

signals:
    void statusUpdated(QString rosterRequestId);

private slots:
    void approveRoster();
    void disapproveRoster();
};

#endif // ROSTERREQUESTOBJ_H
