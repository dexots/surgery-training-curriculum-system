#ifndef SERVERREQUESTOBJ
#define SERVERREQUESTOBJ

#include <cassert>

#include <QString>
#include <QJsonArray>
#include <QJsonObject>
#include <QVector>
#include <QMap>
#include <QSqlRecord>
#include <QVariant>

namespace dbclient {
    class ServerRequestObj;
}

class ServerRequestObj {
public:
    explicit ServerRequestObj();
    ~ServerRequestObj();

public:
    void setMatricNo(QString matricNo);
    void setRole(QString role);
    void setPassword(QString password);
    void addTableName(QString table);
    void addFileName(QString file);

    QJsonObject toJsonObj();

    void addTableContent(QString tablename, QSqlRecord record,
                         QVector<QString> columns);

private:
    QJsonObject convertRecordToJson(QSqlRecord record, QVector<QString> params);

    void addJsonToMap(QString name, QJsonObject object);

    QJsonObject convertVectorToJson(QVector<QString> strVector);
    QJsonObject constructTableJson(QVector<QString> strVector,
                                   QMap< QString, QVector<QJsonObject> > contents);

private:
    QString _userMatricNo;
    QString _userRole;
    QString _clientPassword;
    QVector <QString> _tableNames;
    QVector <QString> _fileNames;
    QMap < QString, QVector<QJsonObject> > _tablesContents;
};



#endif // SERVERREQUESTOBJ

