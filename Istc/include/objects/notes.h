#ifndef NOTES_H
#define NOTES_H

#include <cassert>

#include <QString>
#include <QDate>
#include <QWidget>
#include <QDebug>
#include <QLabel>
#include <QVector>

#include "gui/clickablewidget.h"
#include "gui/notesbox.h"

class QString;
class QDate;
class QWidget;
class QLabel;

class ClickableWidget;
class NotesBox;

namespace Gui {
    class Notes;
}

class Notes:public QWidget {
    Q_OBJECT

public:
    explicit Notes();
    explicit Notes(QString notesDate,
                   QString notesTitle,
                   QString notesContent);
    ~Notes();

public:
    void setNotesId(QString notesId);
    void setNotesTitle(QString notesTitle);
    void setNotesContent(QString notesContent);
    void setNotesDate(QString notesDate);
    void setNotesDate(QDate notesDate);

    QDate getNotesDate();
    QString getNotesId();
    QString getNotesTitle();
    QString getNotesContent();
    ClickableWidget* getClickableEditIcon();

private:
    void constructClickableEditIcon();

private:
    QDate _notesDate;
    QString _noteId;
    QString _notesTitle;
    QString _notesContent;

    ClickableWidget *_clickableEditIcon;

private:
    static const QString ICON_EDIT;
    static const QString STRING_SPACE;

signals:
    void clickedUpdateButton(QString notesId);

private slots:
    void popUpWindow();
    void updateNotes(QVector<QString>);
};

#endif // NOTES_H
