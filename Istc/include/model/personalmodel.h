#ifndef PERSONALMODEL
#define PERSONALMODEL

#include <cassert>

#include <QVector>
#include <QString>

#include "dbclient/localdatabaseclient.h"
#include "objects/notes.h"
#include "objects/assessmentobj.h"


class LocalDatabaseClient;

namespace model {
    class PersonalModel;
}

class PersonalModel : public QWidget{
    Q_OBJECT

public:
    static PersonalModel* getObject();
    static void deleteObject();

private:
    explicit PersonalModel();
    ~PersonalModel();

private:
    static PersonalModel* _rosterModel;

public:
    QVector <AssessmentObj> *getAssessments();
    QVector <Notes*> getNotes();

    bool addAssessment(AssessmentObj assessmentObj);
    bool addNote(Notes* notesObj);
    bool updateNote(Notes* notesObj);
    void clearNotes();

    QString generateAssessmentId();
    QString generateNotesId();

private:
    void retrieveNotesFromDb();

private:
    LocalDatabaseClient *_localDb;
    QVector <Notes*> _notes;


};

#endif // PERSONALMODEL

