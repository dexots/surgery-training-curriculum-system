#ifndef ROSTERMODEL
#define ROSTERMODEL

#include <cassert>
#include <QDate>
#include <QVector>
#include <QString>

#include "dbclient/localdatabaseclient.h"
#include "objects/rosterassignobj.h"
#include "objects/rosterrequestobj.h"
#include "objects/leaveobj.h"


class LocalDatabaseClient;

namespace model {
    class RosterModel;
}

class RosterModel : public QWidget{
    Q_OBJECT

public:
    static RosterModel* getObject();
    static void deleteObject();

private:
    explicit RosterModel();
    ~RosterModel();

private:
    static RosterModel* _rosterModel;

private:
    LocalDatabaseClient *_localDb;

public:
    QVector <RosterAssignObj> getYourRoster(QDate date);
    QVector <RosterRequestObj*> *getBlockOutReq();
    QVector <LeaveObj> getLeaveApplications();
    QVector <RosterRequestObj*> *getTeamRequest();

    bool saveBlockOutReq (RosterRequestObj *blockOutReq);
    bool updateBlockOutReq (RosterRequestObj *blockOutReq);
    bool addLeaveApplication(LeaveObj leaveObj);
    bool updateLeaveApplication(LeaveObj leaveObj);
    bool saveRosterAssignment (RosterAssignObj assignObj);

    QString generateBlockRequestId();
    QString generateLeaveId();
    int getCompletedRosterCount();
    int getTotalRosterCount();
    int getNumberOfLeaveApplications();

private:
    void retrieveLeaveFromDb();
    void retrieveRosterAssignFromDb();

private:
    QVector <RosterAssignObj> _yourRoster;
    QVector <LeaveObj> _leaveObjects;
};



#endif // ROSTERMODEL

