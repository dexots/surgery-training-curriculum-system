#ifndef LOGBOOKMODEL_H
#define LOGBOOKMODEL_H

#include <cassert>

#include <QWidget>
#include <QVector>
#include <QString>
#include <QDateTime>
#include <QDate>
#include <QTime>

#include "dbclient/localdatabaseclient.h"
#include "objects/workinghoursobject.h"
#include "objects/proceduralcode.h"

class LocalDatabaseClient;
class WorkingHoursObject;

namespace model {
    class LogbookModel;
}

class LogbookModel:public QWidget{
    Q_OBJECT

public:
    static LogbookModel* getObject();
    static void deleteObject();

private:
    explicit LogbookModel();
    ~LogbookModel();

public:
    QVector <WorkingHoursObject> getWorkingHoursData();
    qint64 calculateCurWeekHour();
    qint64 calculateConHour();
    bool setLogin(QDateTime dateTime);
    bool activateWorkingHoursObject(WorkingHoursObject oldObj);
    bool setLogout(QDateTime dateTime);
    void clearWorkingHoursDatas();

    QVector <ProceduralCode*> *getProcedures();
    bool addProcedure(ProceduralCode* newCode);
    bool updateProcedure(ProceduralCode* oldCode);
    QString generateNewProcedureId();

private:
    void initializeModel();
    void retrieveWorkHourFromDb(QDate todayDate,
                                QDate requestDate);
    qint64 calNoOfSecs(QDateTime from, QDateTime to);
    qint64 calNoOfHours(QDateTime from, QDateTime to);

private:
    static LogbookModel *_logObject;
    LocalDatabaseClient *_localDbClient;
    QVector <WorkingHoursObject> _workingHours;
    QDateTime _logoutDateTime;
    QDateTime _loginDateTime;
    qint64 _conHour;
    qint64 _prevWeekSecs;

private:
    static const int MAX_DELAY_MIN;
    static const int ERROR_CODE;

};

#endif // LOGBOOKMODEL_H
