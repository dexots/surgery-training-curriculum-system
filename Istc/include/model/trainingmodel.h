#ifndef TRAININGMODEL_H
#define TRAININGMODEL_H

#include <QVector>
#include <QDebug>
#include <cassert>
#include "objects/eventitem.h"
#include "objects/exam.h"
#include "objects/form.h"
#include "dbclient/localdatabaseclient.h"

class EventItem;
class QDebug;
class LocalDatabaseClient;

namespace model {
    class TrainingModel;
}

class TrainingModel:public QWidget{
    Q_OBJECT

public:
    static TrainingModel* getObject();
    static void deleteObject();

public:
    QVector <EventItem*> getTrainingActivities();
    QVector <EventItem*> getFutureTrainingActivities();
    QVector <Exam> getExamInfo();
    QVector <Form*> getOnlineForms();
    QVector <Form*> getSoftcopyForms();
    QString generateActivityId();

    bool updateActivity(EventItem* oldEvent);
    bool addNewActivity(EventItem* newEvent);

    void getTrainingActivitiesFromLocalDB();

signals:
    void activitiesInfoRefreshed();

private:
    explicit TrainingModel();
    ~TrainingModel();

private:
    void initializeModel();
    void getExamInfoFromLocalDB();
    void getOnlineFormsFromLocalDB();
    void getSoftcopyFormsFromLocalDB();

private:
    static TrainingModel *_trainingObject;
    LocalDatabaseClient *_localDbClient;

    QVector <EventItem*> _events;
    QVector <Exam> _exams;
    QVector <Form*> _onlineForms;
    QVector <Form*> _softcopyForms;

    bool _isActivitiesRetrieved;

};

#endif // TRAININGMODEL_H
