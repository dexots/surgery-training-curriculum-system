#ifndef ROSTERMONSTERWIDGET_H
#define ROSTERMONSTERWIDGET_H

#include <QDate>
#include <QLabel>
#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QFont>
#include <QPalette>
#include <QString>
#include <QVector>
#include <QSizePolicy>

#include "gui/tablewidget.h"
#include "gui/clickablewidget.h"
#include "objects/rosterrequestobj.h"
#include "model/rostermodel.h"

namespace gui {
    class RosterMonsterWidget;
}

class RosterMonsterWidget:public QWidget {
    Q_OBJECT

public:
    RosterMonsterWidget();
    ~RosterMonsterWidget();

private:
    void setUpTopLabel();
    void setUpTable();
    void setUpTableData();
    void setUpMainLayout();
    void colorWidget(QColor font, QColor background, QWidget *widget);
    void addAllTeamRequests();
    ClickableWidget* createTickClickableWidget();
    ClickableWidget* createCrossClickableWidget();

public slots:
    void updateTable();

private slots:
    void handleResize(int changedSize);
    void approveRoster(QString rosterRequestId);
    void disapproveRoster(QString rosterRequestId);

private:
    QLabel *_topLabel;

    TableWidget *_rosterMonsterTable;

    QVBoxLayout *_rosMonLayout;
    QWidget *_dummyWidget;

    int _appContRow;

    RosterModel *_rosterModel;

    QVector <RosterRequestObj*> *_teamRequests;

private:
    static const QColor HEADER_BACK;
    static const QColor HEADER_FONT;
    static const QColor CONT_BACK;
    static const QColor CONT_FONT;
    static const QColor WIDGET_COLOR;
    static const QString COLOR_APPLYBUTTON;
    static const QString ICON_CROSS;
    static const QString ICON_TICK;
    static const QString STATUS_APPROVED;
    static const QString STATUS_DISAPPROVED;
    static const QString STATUS_PENDINGS;
    static const QString TAG_TOP_LABEL;

    static const int CONTENT_MARGINS;
};

#endif // ROSTERMONSTERWIDGET_H
