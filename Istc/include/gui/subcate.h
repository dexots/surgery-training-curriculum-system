#ifndef SUBCATE_H
#define SUBCATE_H

#include <QLabel>
#include <QString>
#include <QMouseEvent>
#include <QLayout>
#include <QVBoxLayout>
#include <QSizePolicy>

#include "gui/catebox.h"

namespace gui {
    class SubCate;
}

class QLabel;
class QString;
class QLayout;
class QVBoxLayout;
class QMouseEvent;

class CateBox;

class SubCate:public CateBox {
    Q_OBJECT

public:
    explicit SubCate(QString name,
                     QString desc,
                     const char* icon,
                     QColor descColor,
                     QColor defaultColor,
                     QColor selectedColor,
                     QWidget *corWidget);
    ~SubCate();

public:
    QWidget *getCorWidget();
    void showDefaultBackground();
    void showSelectedBackground();


protected:
    void handleTouch();
    virtual void mousePressEvent(QMouseEvent *event);
    void createMyself();
    QLayout *setupSubCateLayout();

private:
    void initializeLabel();
    void setupDescLabel(QString descString, QColor descColor);
    void insDesc(QString descString);
    void setDescFontColor(QColor fontColor);
    void setDefaultDescColor(QColor defaultColor);
    void setSelectedColor(QColor selectedColor);
    void setCorWidget(QWidget *corWidget);

private:
    QLabel *_descriptionLabel;
    QWidget *_corWidget;
    QColor _colorDefault;
    QColor _colorSelected;
};

#endif // SUBCATE_H
