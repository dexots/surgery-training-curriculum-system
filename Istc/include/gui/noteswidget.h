#ifndef NOTESWIDGET_H
#define NOTESWIDGET_H

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QString>
#include <QPixmap>
#include <QFrame>
#include <QVector>
#include <QWidget>

#include "gui/tablewidget.h"
#include "gui/clickablewidget.h"
#include "gui/notesbox.h"
#include "objects/notes.h"
#include "model/personalmodel.h"

class QVBoxLayout;
class QHBoxLayout;
class QLabel;
class QString;
class QPixmap;
class QFrame;
class QWidget;

class ClickableWidget;
class TableWidget;
class Notes;

namespace Gui {
    class NotesWidget;
}

class NotesWidget:public QWidget {
    Q_OBJECT

public:
    explicit NotesWidget();
    ~NotesWidget();

private:
    void setUpNotesLayout();
    void setUpAddNotesIcon();
    void setUpNotesTitleAndDateTime();
    void setUpAlignWidget();
    void setWidgetStyleSheet();
    void addRowsToTable();

public slots:
    void updateTable();

private slots:
    void addNote();
    void updateNotesTable(QString notesId);
    void haveNoteDetails(QVector<QString> info);
    void handleResize(int changedSize);

//class variables
private:
    QVBoxLayout *_notesLayout;
    TableWidget *_notesTable;

    QWidget *_alignWidget;

    int _contRow;

    QVector <Notes*> _notes;
    PersonalModel* _personalModel;

//constants
private:
    static const QString ICON_ADD;
    static const QString SIZE_FONT;

    static const QVector<int> WIDTH_COLUMN;
    static const int HEIGHT_ROW;
    static const int NUMBER_COLUMN;
    static const int WIDTH_TABLE;

    static const QVector<QString> HEADER_TABLE;

    static const QColor COLOR_CONTENTBACKGROUND;
    static const QColor COLOR_CONTENTFONT;
    static const QColor COLOR_HEADERBACKGROUND;
    static const QColor COLOR_HEADERTEXT;

};

#endif // NOTESWIDGET_H
