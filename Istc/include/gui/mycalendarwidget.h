#ifndef MYCALENDARWIDGET_H
#define MYCALENDARWIDGET_H

#include <iostream>
#include <algorithm>

#include <QCalendarWidget>
#include <QPainter>
#include <QDate>
#include <QColor>
#include <QFont>
#include <QTextFormat>
#include <QBrush>
#include <QString>

class QCalendarWidget;
class QPainter;
class QDate;
class QColor;
class QFont;
class QTextFormat;
class QBrush;
class QString;

namespace Gui{
    class MyCalendarWidget;
}

class MyCalendarWidget:public QCalendarWidget {
    Q_OBJECT

public:
    explicit MyCalendarWidget();
    ~MyCalendarWidget();

protected:
    void paintCell(QPainter *painter, const QRect &rect, const QDate &date) const;

private:
    void setPainterFontAndPen(const QDate &date, QPainter *painter) const;
    void setHorizontalHeaderAppearance();
    void setNavigationBarAppearance();
    void setNavigationBarBackgroundColour();
    void setNavigationBarTextColor();
    void setNextMonthButton();
    void setPrevMonthButton();
    int countWeekNumber();

signals:
    void darkGreyWeekOfYearChanged(int darkGreyWeekOfYear) const;
    void lightGreyWeekOfYearChanged(int lightGreyWeekOfYear) const;

private slots:
    void setDarkGreyWeekOfYear(int darkGreyWeekOfYear);
    void setLightGreyWeekOfYear(int lightGreyWeekOfYear);

//class variables
private:
    int _darkGreyWeekOfYear[3];
    int _lightGreyWeekOfYear[3];

//constants
private:
    static const char *ICON_PREVMONTH;
    static const char *ICON_NEXTMONTH;

    static const int HEIGHT_NAVBAR;
    static const int HEIGHT_PREVNEXTMONTHICON;
    static const int SIZE_CURRENTDATEFONT;
    static const int SIZE_HORIZONTALHEADERFONT;
    static const int SIZE_NAVBARFONT;
    static const int SIZE_ORIPREVNEXTMONTHICON;
    static const int WIDTH_PREVNEXTMONTHICON;

    static const QColor COLOR_HORIZONTALHEADERBACKGROUND;
    static const QColor COLOR_HORIZONTALHEADERTEXT;
    static const QColor COLOR_CALENDARVIEWEVENROW;
    static const QColor COLOR_CALENDARVIEWODDROW;
    static const QColor COLOR_CALENDARVIEWOTHERTEXT;
    static const QColor COLOR_CALENDARVIEWTHISMONTHTEXT;

    static const QString COLOR_NAVBARBACKGROUND;
};



#endif // MYCALENDARWIDGET_H
