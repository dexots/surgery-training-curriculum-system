#ifndef LOGINUI_H
#define LOGINUI_H

#include <iostream>

#include <QWidget>
#include <QVBoxLayout>
#include <QVector>
#include <QString>
#include <QSizePolicy>
#include <QLabel>
#include <QPixmap>
#include <QComboBox>
#include <QCheckBox>
#include <QLineEdit>
#include <QDebug>

//temporarily include nus open id classes
#include "gui/loginbrowser.h"
#include "gui/login.h"


#include "gui/clickablewidget.h"
#include "objects/userdetails.h"

class QWidget;
class QVBoxLayout;
class QString;
class QSizePolicy;
class QLabel;
class QPixmap;
class QLineEdit;

class Login;
class ClickableWidget;
class UserDetails;

namespace gui {
    class LoginUi;
}

class LoginUi:public QWidget {
    Q_OBJECT

public:
    explicit LoginUi();
    ~LoginUi();

public:
    void showErrorMessage(QString errorMessage);

    void backToLoginUi();
    void passInfoToMainWindow();

private:
    void initializeVariables();
    void initializeClass();
    void initializeLogo();
    void initializeRole();
    void initializeMatricNoInput();
    void initializeFullnameInput();
    void initializeEmailInput();
    void initializeRotationInput();
    void initializeButton();
    void initializeErrorMsg();


    void initializeLoginBrowser();

    void clearGarbage();
    void colorButton(QColor background, QWidget *widget);
    void changeFontColor (QColor font, QWidget *widget);
    void reinitiateUi();
    void createView();

    void hideDataFields();

    bool checkDataCompleteness();


private slots:
    void handleNotOpenIdButton();
    void handleOpenIdButton();
    void handleRosterMonster();

    //wrote for nus open id login
    void handleShowWidget();
    void handleUrlChanged(QUrl url);


signals:
    void loginSuccess();

private:
    ClickableWidget *_loginButton;
    ClickableWidget *_loginOpenIdButton;
    QVBoxLayout *_layout;

    QLabel *_appLogo;
    QLabel *_errorMessage;

    QString _role;
    QString _matricNo;
    QString _fullname;
    QString _email;
    QString _rotation;

    bool _isRosterMonster;

    QComboBox *_roleDropDown;
    QCheckBox *_rosterMonsterCheck;
    QVBoxLayout *_roleLayout;

    QLineEdit *_matricNoInput;
    QLineEdit *_fullnameInput;
    QLineEdit *_emailInput;
    QLineEdit *_rotationInput;

    //temporarily support nus open id
    LoginBrowser *_loginBrowser;
    Login *_urlParser;


private:
    static const char* ICON_LOGO;
    static const char* BUTTON_OPENID_NAME;
    static const char* BUTTON_WITHOUT_OPENID_NAME;
    static const char* FONT_NAME;
    static const int FONT_BUTTON_SIZE;
    static const QColor GREEN;

    static const QString LOGIN_FAIL;
    static const QString LOGIN_CANCEL;

    static const QString ROSTER_MONSTER;
    static const QString STRING_EMPTY;
};

#endif // LOGINUI_H
