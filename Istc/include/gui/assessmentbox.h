#ifndef ASSESSMENTBOX_H
#define ASSESSMENTBOX_H

#include <QComboBox>

#include "gui/popupboxincludes.h"
#include "dbclient/localdatabaseclient.h"

namespace Gui {
    class AssessmentBox;
}

class AssessmentBox:public QDialog {
    Q_OBJECT

public:
    AssessmentBox();
    ~AssessmentBox();

public:
    void hideButton();

protected:
    void paintEvent(QPaintEvent *event);
    void setButtonStyleSheet(QPushButton *button);

private:
    void initialiseVariables();
    void setUpLayout();
    void setUpMatricLayout();
    void setUButtonLayout();
    void setUpDateLayout();
    void setUpGradeLayout();
    void setUpCommentsLayout();
    void setUpErrorLabel();
    void setUpAddCancelButton();
    void showMessageIfEmptyInput(QString input, QString errorMessage);
    void showMessageIfInvalidMatricNo();
    void changeColor(QColor font, QColor back, QWidget *widget);

private:
    QFont _boldFont;

    QLabel *_dateLabel;
    QLabel *_errorLabel;
    QLabel *_matricLabel;
    QLabel *_moduleLabel;
    QLabel *_commentsLabel;
    QLabel *_gradeLabel;

    InputBox *_inputComments;
    InputBox *_inputMatric;
    InputBox *_inputModule;

    QString _dateString;
    QString _commentsString;
    QString _moduleString;
    QString _matricString;

    QDate _date;

    QComboBox *_gradesBox;

    QPushButton *_addButton;
    QPushButton *_cancelButton;

    QHBoxLayout *_matricLayout;
    QVBoxLayout *_gradeLayout;
    QVBoxLayout *_commentsLayout;
    QHBoxLayout *_buttonLayout;
    QVBoxLayout *_mainLayout;


private slots:
    void handleAddOrUpdate();
    void handleCancel();
    void handleMatric(QString inputString);
    void handleModule(QString inputString);
    void handleComments(QString inputString);

signals:
    void passInfo(QDate date, QVector<QString> info);

private:
    static const QString COLOR_BUTTONBACKGROUND;
    static const QString STYLE_BUTTONBORDER;
    static const QString WIDTH_BUTTONBORDER;
    static const QString RADIUS_BUTTONBORDER;
    static const QString COLOR_BUTTONBORDER;
    static const QString FONT_BUTTON;
    static const QString COLOR_BUTTONFONT;
    static const QString WIDTH_BUTTON;
    static const QString PADDING_BUTTON;
    static const int WIDTH_MINIMUM;
    static const int HEIGHT_MINIMUM;

    static const QString MESSAGE_INVALIDMATRICNO;
    static const QString MESSAGE_MISSINGMATRICNO;
    static const QString MESSAGE_MISSINGMODULETITLE;

    static const QString QSTRING_EMPTY;
    static const QString QSTRING_NEWLINE;
    static const QString HTML_ALERT;
    static const QString HTML_NORMAL;
};

#endif // ASSESSMENTBOX_H
