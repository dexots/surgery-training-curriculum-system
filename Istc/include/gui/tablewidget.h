//TableWidget is a widget that is self-created to create a nicer
//table.

//FixedRowHeight has deprecated. Please do not use it

#ifndef TABLEWIDGET_H
#define TABLEWIDGET_H

#include <iostream>
#include <cassert>


#include <QGridLayout>
#include <QVBoxLayout>
#include <QScrollArea>
#include <QWidget>
#include <QVector>
#include <QString>
#include <QColor>
#include <QLayoutItem>
#include <QLabel>
#include <QFont>
#include <QSize>
#include <QResizeEvent>
#include <QDebug>

class QGridLayout;
class QVBoxLayout;
class QScrollArea;
class QWidget;
class QString;
class QColor;
class QLayoutItem;
class QLabel;
class QFont;

namespace gui {
    class TableWidget;
}

class TableWidget:public QWidget {
    Q_OBJECT

public:   
    TableWidget(bool hasHeader,
                int noOfCol,
                QVector <int> widthOfCol);
    TableWidget(bool hasHeader,
                int noOfCol,
                QVector <int> widthOfCol,
                int rowHeight);
    TableWidget(bool hasHeader,
                int noOfCol,
                QVector <int> widthOfCol,
                QWidget *parent);
    TableWidget(bool hasHeader,
                int noOfCol,
                QVector <int> widthOfCol,
                int rowHeight,
                QWidget *parent);
    ~TableWidget();

public:
    void setHeader(QVector <QString> headerNames,
                   QColor font,
                   QColor background);
    void setHeader(QVector <QString> headerNames,
                   QColor font,
                   QColor background,
                   int headerHeight);
    void setContBackground(QColor defaultBackground);
    void setContFont(QColor defaultFont);
    void colorCell(int row, int col, QColor font, QColor background);
    void underlineCell(int row, int col); //setFontUnderline
    void removeRow(int row);
    void addRow(QVector<QString> contentOfARow);
    void addRow(QVector<QString> contentOfARow, int row);
    void addRow(QVector<QWidget*> contentOfARow);
    void addRow(QVector<QWidget*> contentOfARow, int row);
    void addCell(QString content, int row, int col);
    void addCell(QWidget *content, int row, int col);
    void deleteRow(int row);
    void deleteRow();
    void deleteAll();

    //this method is provided if u addRow only using QString or QLabel
    //and it is not advisable to use it, as it takes O(n) runtime efficiency
    void addRowFromTop(QVector<QString> contentOfARow);

    void resizeHeaders();

    void showWidget(int row, int col); //for testing purpose


    void setNoGrid();
    int getNumOfRow();

 private:
    void deleteCell(int row, int col);
    void deleteChildWidgets(QLayoutItem *item);
    void addCell(QString content,
                 int row,
                 int col,
                 QGridLayout *layout);
    void addCell(QWidget *content,
                 int row,
                 int col,
                 QGridLayout *layout);
    void colorCell(int row,
                   int col,
                   QColor font,
                   QColor background,
                   QGridLayout *layout);

    void initializeClass(bool hasHeader,
                         int noOfCol,
                         QVector <int> widthOfCol);
    void setupGridLayout();
    void setupScrollArea();
    void setupLayout();
    void setupYourself();
    void clearGarbage();
    void setupHeader(QVector <QString> headerNames,
                     QColor font,
                     QColor background,
                     int headerHeight);
    void colorWidget(QColor font, QColor background, QWidget *widget);
    void initializeLabel(QLabel *label,
                         QString txt,
                         QColor font,
                         QColor background);




    //@deprecated: it is not a good idea to have a fixed row height
    //as you need to have "word wrap" for your labels
    void setFixedRowHeight(int height);


signals:
    void resizedTable(int minimumHeight);

protected:
    void resizeEvent(QResizeEvent * event);

private:
    int _curRow;
    int _rowCount;
    int _noOfCol;
    bool _hasHeader;
    QGridLayout *_gridLayout;
    QGridLayout *_headerLayout;
    QVBoxLayout *_layout;
    QScrollArea *_area;
    QVector <int> _stretchOfCol;
    QColor _deContBack;
    QColor _deContFont;

    QVector <QLabel*> _headerLabels;
    QVector < QVector <QWidget*> > _contents;
    QVector <int> _widthOfHeaders;

    QVector <int> _heightOfContents;


    //@deprecated
    int _fixRowHeight;


private:
    static const int DEFAULT_HEADER_HEIGHT;
    static const double ENLARGE_FACTOR;
};

#endif // TABLEWIDGET_H
