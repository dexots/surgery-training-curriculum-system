#ifndef CURRICULUMWIDGET_H
#define CURRICULUMWIDGET_H

#include <QLabel>
#include <QCheckBox>
#include <QDateEdit>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QString>
#include <QFrame>
#include <QPushButton>
#include <QPrinter>
#include <QPainter>
#include <QTextDocument>    //Testing QPrinter
#include <QDir>
#include <QStandardPaths>
#include <QFileDialog>

#include "gui/mycalendarwidget.h"
#include "gui/generalpopupbox.h"

class QLabel;
class QCheckBox;
class QDateEdit;
class QVBoxlayout;
class QHBoxLayout;
class QString;
class QFrame;
class QPushButton;
class QPrinter;
class QPainter;
class QTextDocument;
class QDir;
class QStandardPaths;
class QFileDialog;

class MyCalendarWidget;

namespace Gui {
    class CurriculumWidget;
}

class CurriculumWidget:public QWidget{
    Q_OBJECT

public:
    explicit CurriculumWidget();
    ~CurriculumWidget();

private:
    void setGenerateButton();
    void singleGenerateFormat();
    void setGenerateFormat();
    void setGenerateType();
    void setWidgetStyleSheet();
    void setUpCurriculumLayout();
    void cvPopUp(QString saveFileName);

private slots:
    bool generateCV();

//class variables
private:
    QVBoxLayout *_curriculumLayout;
    QPrinter _printer;

//constants
private:
    static const QString BORDER_DATEEDIT;
    static const QString BOTTOMBORDER_CHECKBOXINDICATOR;
    static const QString COLOR_BUTTON;
    static const QString COLOR_BUTTONTEXT;
    static const QString COLOR_DATEEDITBACKGROUND;
    static const QString HEIGHT_BUTTON;
    static const QString ICON_CHECK;
    static const QString LABEL_TO;
    static const QString LEFTBORDER_CHECKBOXINDICATOR;
    static const QString LEFTMARGIN_BUTTON;
    static const QString LEFTPADDING_CHECKBOX;
    static const QString MESSAGE_GENERATECV;
    static const QString NEWLINE;
    static const QString PADDING_CHECKBOX;
    static const QString PADDING_LABEL;
    static const QString RADIUS_BUTTONBORDER;
    static const QString RIGHTBORDER_CHECKBOXINDICATOR;
    static const QString SIZE_FONT;
    static const QString TAG_DOCX;
    static const QString TAG_EDUCATION;
    static const QString TAG_FORMAT;
    static const QString TAG_GENERATE;
    static const QString TAG_PDF;
    static const QString TAG_RESEARCH;
    static const QString TAG_TRAINING;
    static const QString TOPBORDER_CHECKBOXINDICATOR;
    static const QString WIDTH_BUTTON;
};

#endif // CURRICULUMWIDGET_H
