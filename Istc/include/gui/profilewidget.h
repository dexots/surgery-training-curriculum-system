#ifndef PROFILEWIDGET_H
#define PROFILEWIDGET_H

#include <QLabel>
#include <QLayout>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QWidget>
#include <QVector>

#include "gui/catebox.h"
#include "gui/subcate.h"
#include "gui/catetable.h"
#include "gui/personalwidget.h"
#include "gui/curriculumwidget.h"
#include "gui/assessmentwidget.h"
#include "gui/noteswidget.h"
#include "objects/userdetails.h"

class QLabel;
class QLayout;
class QVBoxLayout;
class QHBoxLayout;
class QWidget;

class CateBox;
class SubCate;
class CateTable;
class PersonalWidget;
class CurriculumWidget;
class AssessmentWidget;
class NotesWidget;

namespace Gui {
    class ProfileWidget;
}

class ProfileWidget:public QWidget {
    Q_OBJECT

public:
    explicit ProfileWidget();
    ~ProfileWidget();

private:
    void createLayout();
    void colorWidget(QColor color, QWidget *widget);

private:
    QVBoxLayout *_widgetLayout;

public slots:
    void updateAllTables();

private slots:
    void handleHomeSignal();

signals:
    void backToHome();
    void updateAssessmentTable();
    void updateNotesTable();
};

#endif // PROFILEWIDGET_H
