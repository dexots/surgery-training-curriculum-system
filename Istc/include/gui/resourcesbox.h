#ifndef RESOURCESBOX_H
#define RESOURCESBOX_H

#include "gui/popupboxincludes.h"

namespace gui {
    class ResourcesBox;
}

class ResourcesBox:public QDialog {
    Q_OBJECT

public:
    ResourcesBox(QString resourceType);
    ~ResourcesBox();

private:
    void initializeVariable();
    void setUpText();
    void setUpLink();
    void setUpErrorLabel();
    void setUpLayout();
    void setUpAddAndCancelButton();
    void changeColor(QColor font, QColor back, QWidget* widget);
    void setButtonStyleSheet(QPushButton *button);
    void paintEvent(QPaintEvent *);

private slots:
    void handleAdd();
    void handleCancel();

signals:
    void passInfo(QVector<QString> info);

private:
    QLabel *_errorLabel;
    QLabel *_textLabel;
    QLabel *_linkLabel;

    QTextEdit *_inputTitle;
    QTextEdit *_inputLink;
    QPushButton *_addButton;
    QPushButton *_cancelButton;

    QString _titleString;
    QString _linkString;
    QString _resourceType;

    QFont _bold;

    QHBoxLayout *_linkLayout;
    QHBoxLayout *_titleLayout;
    QHBoxLayout *_buttonLayout;
    QVBoxLayout *_mainLayout;

// private constants
private:
    static const QString COLOR_BUTTONBACKGROUND;
    static const QString STYLE_BUTTONBORDER;
    static const QString WIDTH_BUTTONBORDER;
    static const QString RADIUS_BUTTONBORDER;
    static const QString COLOR_BUTTONBORDER;
    static const QString FONT_BUTTON;
    static const QString COLOR_BUTTONFONT;
    static const QString WIDTH_BUTTON;
    static const QString PADDING_BUTTON;
    static const QString STRING_SPACE;

    static const int WIDTH_MINIMUM;
    static const int HEIGHT_MINIMUM;

    static const QString QSTRING_EMPTY;

};


#endif // RESOURCESBOX_H
