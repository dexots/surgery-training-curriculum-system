//The second layout
#ifndef BOTLEFTWIDGET_H
#define BOTLEFTWIDGET_H

#include <iostream>

#include <QVBoxLayout>
#include <QLabel>
#include <QScrollArea>
#include <QScrollBar>
#include <QFrame>
#include <QDate>
#include <QWidget>
#include <QString>
#include <QPixmap>
#include <QDebug>
#include <QColor>

#include "gui/mycalendarwidget.h"
#include "gui/tablewidget.h"
#include "objects/eventitem.h"
#include "model/trainingmodel.h"

class QVBoxLayout;
class QLabel;
class QScrollArea;
class QScrollBar;
class QFrame;
class QDate;
class QWidget;
class QString;
class QPixmap;
class QDebug;
class QColor;
class TrainingModel;
class TableWidget;

namespace Gui {
    class BotLeftWidget;
}

class BotLeftWidget:public QWidget {
    Q_OBJECT

public:
    explicit BotLeftWidget();
    ~BotLeftWidget();

private:
    void setUpCalendarToLayout();
    void setUpEventListHeaderToLayout();
    void setUpEventContentToEventListLayout();
    void setUpBotLeftWidgetLayout();
    void setUpDummyWidget();
    void getUserDetails();
    void connectQtSignalSlot();
    void addEventListContent();

private slots:
    void updateEventListHeader();
    void updateEventList();

private:
    QVBoxLayout *_eventListLayout;
    QVBoxLayout *_widgetLayout;

    MyCalendarWidget *_calendar;

    QLabel *_eventListHeader;

    TableWidget *_eventListTable;

    TrainingModel *_trainingModel;

    QVector<EventItem*> _events;

    QString _userRole;
    QString _userName;

//constants
private:
    static const int HEIGHT_EVENTITEM;
    static const int NUMBER_COLUMN;

    static const QString ALIGN_EVENTLISTHEADER;
    static const QString FONTSIZE_EVENTLISTHEADER;
    static const QString FONTSTYLE_EVENTLISTHEADER;
    static const QString FORMAT_DATE;
    static const QString ICON_CALENDAR;
    static const QString PADDING_EVENTLISTHEADER;
    static const QString TAG_EVENTLISTHEADER;

    static const QColor COLOR_CONTENTBACKGROUND;
    static const QColor COLOR_CONTENTFONT;
};

#endif // BOTLEFTWIDGET_H
