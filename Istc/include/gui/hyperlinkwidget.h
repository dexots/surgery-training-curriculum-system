#ifndef HYPERLINKWIDGET_H
#define HYPERLINKWIDGET_H

#include <QWidget>
#include <QLabel>
#include <QVector>
#include <QString>
#include <QScrollArea>
#include <QFrame>
#include <cassert>

#include "gui/clickablewidget.h"
#include "gui/resourcesbox.h"
#include "gui/resourcewidget.h"
#include "gui/generalpopupbox.h"
#include "objects/userdetails.h"

class QWidget;
class QLabel;
class QScrollArea;
class QFrame;
class QString;

class ClickableWidget;
class ResourcesBox;
class ResourceWidget;
class GeneralPopupBox;
class UserDetails;

namespace Gui {
    class HyperlinkWidget;
}

class HyperlinkWidget:public QWidget {
    Q_OBJECT

public:
    explicit HyperlinkWidget(QString resourceType);
    ~HyperlinkWidget();

public:
    void addLinkAndText(QVector<QString> hyperlinks,
                        QVector<QString> texts);
    void addLinkAndText(QString hyperlink, QString text);

private:
    void initiateWidget();
    void setupScrollArea();
    void changeColor(QColor font, QColor back, QWidget *widget);
    void getUserRole();
    void setUpAddButton();
    void showSuccessBox();

private:
    QWidget *_list;
    QVBoxLayout *_listLayout;
    QScrollArea *_area;
    QVBoxLayout *_widgetLayout;
    QVector <QString> _texts;
    QVector <QString> _hyperlinks;
    QVector <ClickableWidget*> _displayTexts;
    QFont _font;

    QString _userRole;
    QString _resourceType;

    static const QString ICON_ADD;
    static const QString DELIMITER;
    static const QString NEWLINE;

private slots:
    void handleClick();
    void handleAdd();
    void writeFileContent(QVector<QString> info);

signals:
    void openLink(QString hyperlink);
};

#endif // HYPERLINKWIDGET_H
