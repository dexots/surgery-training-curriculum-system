#ifndef POPUPBOXINCLUDES_H
#define POPUPBOXINCLUDES_H

#include <iostream>

#include <QDebug>
#include <QDialog>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QWidget>
#include <QFont>
#include <QPainter>
#include <QBrush>
#include <QRect>
#include <QPushButton>
#include <QColor>
#include <QPalette>
#include <QString>
#include <QDate>
#include <QFrame>
#include <QSizePolicy>
#include <QVector>

#include "gui/inputbox.h"

class QDialog;
class QHBoxLayout;
class QVBoxLayout;
class QLabel;
class QWidget;
class QFont;
class QPainter;
class QTextEdit;
class QFrame;
class QSizePolicy;
class QString;
class QPushButton;
class QDate;
class InputBox;

#endif // POPUPBOXINCLUDES_H
