#ifndef BROWSER_H
#define BROWSER_H

//@deprecate webkit
//#include <QWebView>
//#include <QWebSettings>

//things to replace webkit temporarily
#include <QDesktopServices>

#include <QWidget>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QString>
#include <QPixmap>
#include <QUrl>
#include <QNetworkCookieJar>
#include <QNetworkCookie>
#include <QList>
#include <QPalette>
#include <QFont>

#include "gui/clickablewidget.h"

//@deprecated webkit
//class QWebView;

class QWidget;
class QHBoxLayout;
class QVBoxLayout;
class QLabel;
class ClickableWidget;

namespace Gui {
    class Browser;
}

class Browser:public QWidget {
    Q_OBJECT

public:
    explicit Browser();
    ~Browser();

public:
    void load(QUrl url);
    void clearCookie();
    void hideHomeButton();

private:
    void initializeBrowser();
    void initiateBack();
    void initiateForward();
    void initiateHome();
    void initiateLoadingLabel();
    void initiateLoadErrorLabel();
    void initiateButtonsLayout();
    void initiateBrowserLayout();
    void clearGarbage();
    void closeWebView();

private slots:
    void timeToShowWidget();
    void timeToChangeUrl(QUrl url);
    void loadStatus(bool success);
    void loadStart();

signals:
    void showWidget();
    void urlChanged(QUrl url);

private:
    //QWebView *_browserView;

    QHBoxLayout *_buttonLayout;
    QVBoxLayout *_browserLayout;

    QLabel *_backLabel;
    QLabel *_forwardLabel;
    QLabel *_homeLabel;
    QLabel *_loadingLabel;
    QLabel *_loadErrorLabel;

    ClickableWidget *_backClick;
    ClickableWidget *_forwardClick;
    ClickableWidget *_homeClick;

    QFont _font;

private:
    static const int FONT_SIZE;

    static const QString LABEL_ERROR;
    static const QString LABEL_LOADING;

    static const QString LABEL_HOME;
    static const QString LABEL_FORWARD;
    static const QString LABEL_BACK;
};


#endif // BROWSER_H
