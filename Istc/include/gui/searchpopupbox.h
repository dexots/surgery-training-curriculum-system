#ifndef SEARCHPOPUPBOX_H
#define SEARCHPOPUPBOX_H

#include "gui/popupboxincludes.h"

namespace gui {
    class SearchPopupBox;
}

class SearchPopupBox:public QDialog {
    Q_OBJECT

public:
    SearchPopupBox(QString searchRequest);
    ~SearchPopupBox();

protected:
    void paintEvent(QPaintEvent *event);

private:
    void changeColor(QColor font, QColor back, QWidget* widget);
    void setButtonStyleSheet(QPushButton *button);

private slots:
    void handleButton();

private:
    QLabel *_searchText;
    QLabel *_searchRequestText;
    QPushButton *_okButton;
    QVBoxLayout *_mainLayout;
    QHBoxLayout *_buttonLayout;

    QFont _bold;

// private constants
private:
    static const QString COLOR_BUTTONBACKGROUND;
    static const QString STYLE_BUTTONBORDER;
    static const QString WIDTH_BUTTONBORDER;
    static const QString RADIUS_BUTTONBORDER;
    static const QString COLOR_BUTTONBORDER;
    static const QString FONT_BUTTON;
    static const QString COLOR_BUTTONFONT;
    static const QString WIDTH_BUTTON;
    static const QString PADDING_BUTTON;

    static const QString TAG_SEARCHING;
    static const QString TAG_OK;

    static const int WIDTH_MINIMUM;
    static const int HEIGHT_MINIMUM;
};

#endif // SEARCHPOPUPBOX_H
