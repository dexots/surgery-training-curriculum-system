// TopWidget.h
#ifndef TOPWIDGET_H
#define TOPWIDGET_H

#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QFile>
#include <QWidget>
#include <QSpinBox>
#include <QDate>
#include <QLineEdit>
#include <QLabel>


#include "gui/clickablewidget.h"
#include "gui/logoutbox.h"
#include "gui/syncbox.h"
#include "gui/settingsbox.h"
#include "gui/searchpopupbox.h"
#include "objects/userdetails.h"

class QFile;
class QDate;
class QSpinBox;
class QVBoxLayout;
class QHBoxLayout;
class QPushButton;
class QLineEdit;
class QLabel;
class QWidget;
class QPixmap;

class ClickableWidget;
class LogoutBox;
class SyncBox;
class SettingsBox;
class SearchPopupBox;

namespace Gui {
    class TopWidget;
}

class TopWidget:public QWidget {
    Q_OBJECT

public:
    explicit TopWidget();

public:
    void setSearchBarVisibility(bool isVisible);

signals:
    void wantMain();
    void wantProfile();
    void wantSync();

private slots:
    void clickedIstc();
    void clickedProfileLogo();
    void clickedRefresh();
    void clickedSettings();
    void clickedLogout();
    void enteredSearch();
    void handleSuccessSyncSignal();

private:
    QHBoxLayout *_searchLayout;
    QWidget *_searchBar;

    QLineEdit *_searchLine;
};

#endif
