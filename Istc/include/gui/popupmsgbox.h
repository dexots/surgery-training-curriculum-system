#ifndef POPUPMSGBOX_H
#define POPUPMSGBOX_H

#include <QDebug>
#include "gui/popupbox.h"

namespace Gui {
    class PopUpMsgBox;
}

class PopUpMsgBox:public PopupBox {
    Q_OBJECT

public:
    PopUpMsgBox(QString message);
    ~PopUpMsgBox();

protected:
    void paintEvent(QPaintEvent *event);

private:
    void setMessageLabel(QString message);
    void setCloseButton();

//class variables
private:
    QVBoxLayout *_mainLayout;
    QString _message;


//constants
private:
    static const QString LABEL_CLOSEBUTTON;

//private slots
private slots:
    void clickedCloseButton();
};


#endif // POPUPMSGBOX_H
