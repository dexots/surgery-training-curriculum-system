#ifndef NOTESBOX_H
#define NOTESBOX_H

#include <QDialog>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QWidget>
#include <QFont>
#include <QPainter>
#include <QBrush>
#include <QRect>
#include <QPushButton>
#include <QColor>
#include <QPalette>
#include <QString>
#include <QDate>
#include <QFrame>
#include <QSizePolicy>
#include <QVector>
#include <QDebug>

#include "gui/inputbox.h"

class QDialog;
class QHBoxLayout;
class QVBoxLayout;
class QLabel;
class QWidget;
class QFont;
class QPainter;
class QBrush;
class QRect;
class QColor;
class QPalette;
class QTextEdit;
class QFrame;
class QSizePolicy;
class QString;
class QPushButton;
class QDate;
class QDebug;

class InputBox;

namespace gui {
    class NotesBox;
}

class NotesBox:public QDialog {
    Q_OBJECT

public:
    NotesBox();
    NotesBox(QString notesDate, QString notesTitle, QString notesContent);
    ~NotesBox();

protected:
    void paintEvent(QPaintEvent *event);

private:
    void initializeVariable();
    void setUpDateLabel(const QString &notesDate = "");
    void setUpNoteTitleInput(const QString &notesTitle = "");
    void setUpNoteContentInput(const QString &notesContent = "");
    void setUpErrorLabel();
    void setUpAddAndCancelButton();
    void setUpUpdateAndCancelButton();
    void setNotesBoxAttribute();
    void changeColor(QColor font, QColor back, QWidget* widget);
    void setButtonStyleSheet(QPushButton *button);

private slots:
    void handleTitle(QString inputString);
    void handleAddOrUpdate();
    void handleCancel();

signals:
    void passInfo(QVector<QString> info);

private:
    InputBox *_inputTitle;
    QTextEdit *_inputNote;
    QLabel *_errorLabel;
    QLabel *_dateLabel;
    QPushButton *_addButton;
    QPushButton *_cancelButton;
    QDate _date;

    QString _titleString;
    QString _noteString;
    QString _dateString;

    QFont _bold;

    QVBoxLayout *_boxLayout;

// private constants
private:
    static const QString COLOR_BUTTONBACKGROUND;
    static const QString STYLE_BUTTONBORDER;
    static const QString WIDTH_BUTTONBORDER;
    static const QString RADIUS_BUTTONBORDER;
    static const QString COLOR_BUTTONBORDER;
    static const QString FONT_BUTTON;
    static const QString COLOR_BUTTONFONT;
    static const QString WIDTH_BUTTON;
    static const QString PADDING_BUTTON;
    static const QString STRING_SPACE;
    static const int WIDTH_MINIMUM;
    static const int HEIGHT_MINIMUM;

    static const QString QSTRING_EMPTY;

};

#endif // NOTESBOX_H
