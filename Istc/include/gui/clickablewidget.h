#ifndef CLICKABLEWIDGET_H
#define CLICKABLEWIDGET_H

#include <QWidget>
#include <QMouseEvent>
#include <QInputEvent>
#include <QEvent>
#include <QString>
#include <QVBoxLayout>

#include "objects/userdetails.h"

class QWidget;
class QString;
class QMouseEvent;
class QInputEvent;
class QEvent;
class QVBoxLayout;

class UserDetails;

namespace gui {
    class ClickableWidget;
}

class ClickableWidget:public QWidget {
    Q_OBJECT

public:
    ClickableWidget(QWidget *widget);
    ClickableWidget(QWidget* widget, QWidget* parent);
    ~ClickableWidget();

public:
    QString getWidgetName();

protected:
    virtual void mousePressEvent(QMouseEvent *event);
    virtual bool event(QEvent *event);
    void handleTouch();

private:
    void initializeWidget(QWidget* widget);

signals:
    void isClicked();
    void isClicked(QString accessibleName);
    //to trigger slots with bool
    void isClicked(bool isEnabled);

private:
    static const QString STRING_EMPTY;

private:
    QString _widgetName;

};

#endif // CLICKABLEWIDGET_H
