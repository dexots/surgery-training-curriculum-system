#ifndef SETTINGSBOX_H
#define SETTINGSBOX_H

#include <iostream>
#include <cstdio>

#include <QComboBox>
#include <QTextStream>
#include <QDebug>

#include "gui/popupboxincludes.h"
#include "gui/errorbox.h"
#include "gui/generalpopupbox.h"

class QComboBox;
class QTextStream;
class QDebug;
class PopupBoxIncludes;
class ErrorBox;
class GeneralPopupBox;

using namespace std;

namespace gui {
    class SettingsBox;
}

class SettingsBox:public QDialog {
    Q_OBJECT

public:
    SettingsBox();
    ~SettingsBox();

protected:
    void paintEvent(QPaintEvent *event);

private:
    void initialise();

    void readSettingsFile();
    void saveSettingsFile();
    void createSettingsFile();
    void populateData(QString data);

    void changeColor(QColor font, QColor back, QWidget* widget);
    void setButtonStyleSheet(QPushButton *button);

    void setSyncLayout();
    void setUpSyncBox();
    void setColourLayout();
    void setAboutLayout();
    void setButtonLayout();
    void customisePopupBox();
    void showErrorBox();
    void showSuccessMsg();

    void addToMainLayout(QVBoxLayout *layout);
    void addToMainLayout(QHBoxLayout *layout);
    void addToMainLayout(QWidget *widget);

    bool isFileExist(QString filename);

private slots:
    void handleButton();
    void handleSyncChange();
    void handleColourChange();

private:
    QLabel *_syncLabel;
    QLabel *_colourLabel;
    QLabel *_aboutLabel;
    QLabel *_aboutWriteupLabel;
    QComboBox *_syncBox;
    QComboBox *_colourBox;
    QPushButton *_button;
    QVBoxLayout *_mainLayout;
    QVBoxLayout *_syncLayout;
    QVBoxLayout *_colourLayout;
    QVBoxLayout *_aboutLayout;
    QHBoxLayout *_buttonLayout;

    QStringList list;
    QFont _bold;
    QString _aboutWriteup;
    QString _syncInterval;

    bool _settingsChanged;

private:
    static const QString COLOR_BUTTONBACKGROUND;
    static const QString STYLE_BUTTONBORDER;
    static const QString WIDTH_BUTTONBORDER;
    static const QString RADIUS_BUTTONBORDER;
    static const QString COLOR_BUTTONBORDER;
    static const QString FONT_BUTTON;
    static const QString COLOR_BUTTONFONT;
    static const QString WIDTH_BUTTON;
    static const QString PADDING_BUTTON;

    static const QString TAG_ABOUT;
    static const QString TAG_SYNC;
    static const QString TAG_OK;
    static const QString TAG_UPDATE;
    static const QString TAG_COLOUR;

    static const QString SETTINGS_FILE;

    static const int WIDTH_MINIMUM;
    static const int HEIGHT_MINIMUM;

};

#endif // SETTINGSBOX_H
