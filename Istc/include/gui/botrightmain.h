#ifndef BOTRIGHTMAIN_H
#define BOTRIGHTMAIN_H

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QWidget>
#include <QDebug>

#include "gui/catebox.h"

class QVBoxLayout;
class QHBoxLayout;
class QDebug;
class CateBox;

namespace Gui {
    class BotRightMain;
}

class BotRightMain:public QWidget {
    Q_OBJECT

public:
    explicit BotRightMain();
    ~BotRightMain();

private:
    void clearGarbage();
    void initializeBoxes();
    void initializeLayoutTop();
    void initializeLayoutMid();
    void initializeLayoutBot();
    void initializeCateTrain();
    void initializeCateRos();
    void initializeCateLog();
    void initializeCateResearch();
    void initializeCateResources();
    void combineTwoLayouts(CateBox *left, CateBox *right);

private slots:
    void clickedTrain();
    void clickedRos();
    void clickedLog();
    void clickedResearch();
    void clickedResources();

signals:
    void wantTrain();
    void wantRos();
    void wantLog();
    void wantResearch();
    void wantResources();

private:
    CateBox *_cateTrain;
    CateBox *_cateRos;
    CateBox *_cateLog;
    CateBox *_cateResearch;
    CateBox *_cateResources;
    QVBoxLayout *_widgetLayout;

private:
    static const QColor COLOR_GREEN;
    static const QColor COLOR_BLUE;
    static const QColor COLOR_YELLOW;
    static const QColor COLOR_RED;
    static const QColor COLOR_GREY;

    static const QString CATEGORY_ROS;
    static const QString CATEGORY_LOG;
    static const QString CATEGORY_PRO;
    static const QString CATEGORY_RESEARCH;
    static const QString CATEGORY_RESOURCES;
    static const QString CATEGORY_TRAIN;

    static const char* ICON_TRAIN;
    static const char* ICON_LOG;
    static const char* ICON_ROS;
    static const char* ICON_RESEARCH;
    static const char* ICON_RESOURCES;
};


#endif // BOTRIGHTMAIN_H
