#ifndef ERRORBOX_H
#define ERRORBOX_H

#include <iostream>
#include <string>

#include <QDialog>
#include <QVBoxLayout>
#include <QLabel>
#include <QWidget>
#include <QFont>
#include <QPainter>
#include <QBrush>
#include <QRect>
#include <QPushButton>
#include <QColor>
#include <QPalette>
#include <QString>
#include <QDate>
#include <QFrame>
#include <QSizePolicy>
#include <QVector>

#include "gui/inputbox.h"

class QDialog;
class QVBoxLayout;
class QLabel;
class QWidget;
class QFont;
class QPainter;
class QBrush;
class QRect;
class QPushButton;
class QColor;
class QPalette;
class QString;
class QDate;
class QFrame;
class QSizePolicy;

using namespace std;

namespace gui {
    class ErrorBox;
}

class ErrorBox:public QDialog {
    Q_OBJECT

public:
    ErrorBox(string type);
    ~ErrorBox();

protected:
    void paintEvent(QPaintEvent *event);

private:
    void changeColor(QColor font, QColor back, QWidget* widget);
    void setButtonStyleSheet(QPushButton *button);

private slots:
    void handleButton();

private:
    QLabel *_errorLabel;
    QPushButton *_okButton;
    QVBoxLayout *_mainLayout;

    QFont _bold;

// private constants
private:
    static const QString COLOR_BUTTONBACKGROUND;
    static const QString STYLE_BUTTONBORDER;
    static const QString WIDTH_BUTTONBORDER;
    static const QString RADIUS_BUTTONBORDER;
    static const QString COLOR_BUTTONBORDER;
    static const QString FONT_BUTTON;
    static const QString COLOR_BUTTONFONT;
    static const QString WIDTH_BUTTON;
    static const QString PADDING_BUTTON;
    static const QString STRING_DOWNLOAD_ERROR;
    static const QString STRING_SAVE_ERROR;
    static const QString STRING_CONNECTION_ERROR;
    static const int WIDTH_MINIMUM;
    static const int HEIGHT_MINIMUM;

};

#endif // ERRORBOX_H
