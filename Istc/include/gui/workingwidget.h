#ifndef WORKINGWIDGET_H
#define WORKINGWIDGET_H

#include <iostream>

#include <QLabel>
#include <QWidget>
#include <QPixmap>
#include <QColor>
#include <QFont>
#include <QDate>
#include <QDateTime>
#include <QString>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QSizePolicy>
#include <QTimer>

#include "gui/popupmsgbox.h"
#include "gui/tablewidget.h"
#include "gui/clickablewidget.h"
#include "gui/progressbar.h"
#include "gui/clockinbox.h"
#include "objects/workinghoursobject.h"
#include "model/logbookmodel.h"

class QLabel;
class QWidget;
class QPixmap;
class QColor;
class QFont;
class QDate;
class QString;
class QHBoxLayout;
class QVBoxLayout;
class QSizePolicy;
class QTimer;
class TableWidget;
class ClickableWidget;
class ProgressBar;

namespace gui {
    class WorkingWidget;
}

class WorkingWidget:public QWidget {
    Q_OBJECT

public:
    WorkingWidget();
    ~WorkingWidget();

private:
    void colorWidget(QColor color, QWidget *widget);
    void addRowsToTable();

public slots:
    void updateTable();

private slots:
    void clickedClock();
    void changeState(QDateTime time);
    void updateTime();
    void handleTableResize(int minHeight);

private:
    QLabel *_clockIn;
    QLabel *_clockOut;
    QLabel *_clockLogo;
    QHBoxLayout *_clockLayout;
    QVBoxLayout *_workingLayout;
    QDateTime _clockInTime;
    QDateTime _clockOutTime;
    QTimer *_timer;
    QVector <QString> _includedDates;

    LogbookModel *_logbookModel;
    ClickableWidget *_clockWidget;
    TableWidget *_table;
    ProgressBar *_curWeekProgress;
    ProgressBar *_curLimitProgress;

    QWidget *_dummyWidget;

    bool _inState;

private:
    static const int BASE;
    static const int TIMER_INTERVAL;
    static const int SEC_TO_MILLI;

    static const QString ERROR_LOGIN;

};

#endif // WORKINGWIDGET_H
