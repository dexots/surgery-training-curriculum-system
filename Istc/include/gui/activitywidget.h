#ifndef ACTIVITYWIDGET_H
#define ACTIVITYWIDGET_H

#include <iostream>

#include <QVBoxLayout>
#include <QVector>
#include <QtCore>
#include <QDebug>
#include <QString>
#include <QPixmap>
#include <QLabel>
#include <QWidget>

#include "gui/tablewidget.h"
#include "gui/activitybox.h"
#include "gui/popupmsgbox.h"

#include "model/trainingmodel.h"

#include "objects/eventitem.h"
#include "objects/userdetails.h"

class QVBoxLayout;
class TableWidget;
class QDebug;
class QString;
class QPixmap;
class QLabel;
class QWidget;

class EventItem;
class TrainingModel;
class ActivityBox;
class UserDetails;

class PopUpMsgBox;

namespace Gui {
    class ActivityWidget;
}

class ActivityWidget:public QWidget
{
    Q_OBJECT

public:
    explicit ActivityWidget();
    ~ActivityWidget();

private:
    void addRowsToTable();
    void addClickableEditIcon();
    void addClickableTitle();
    void colorRowIfImportantEvent(int i);
    void colorImportantEventsBackground();
    void getUserRole();
    void setUpActivityLayout();
    void setUpActivityTable();
    void setUpAddIconForTrainer();
    void setUpDummyWidget();
    void setUpTableContentFromEventItems();

protected:
    void resizeEvent(QResizeEvent *event);

public slots:
    void updateTable();

private slots:
    void updateStatusColumn(QString activityId, bool isWantedToRegister);
    void addActivity();
    bool processActivityDetails(QVector<QString> info, QDate date, QDateTime registrationDeadline,
                                QVector<QTime> time, int maxVacancy, bool isImportant);
    void updateVacancyColumn(int eventIndex);

    void handleTableResize(int minHeight);
    void updateEventToDatabase(QString activityId);

//class variables
private:
    QVBoxLayout *_activityLayout;
    TableWidget *_activityTable;

    QVector<EventItem*> _events;
    TrainingModel *_trainingModel;

    QWidget *_dummyWidget;

    QString _userRole;
    bool _isTimeToUpdate;
    int _dummyReducedHeight;

//constants
private:
    static const int NUMBER_COLUMN_TRAINER;
    static const int NUMBER_COLUMN_TRAINEE;

    static const QVector<int> WIDTH_COLUMN_TRAINEE;
    static const QVector<int> WIDTH_COLUMN_TRAINER;

    static const QVector<QString> HEADER_TRAINEETABLE;
    static const QVector<QString> HEADER_TRAINERTABLE;

    static const QColor COLOR_CONTENTBACKGROUND;
    static const QColor COLOR_CONTENTFONT;
    static const QColor COLOR_HEADERBACKGROUND;
    static const QColor COLOR_HEADERTEXT;
    static const QColor COLOR_IMPORTANTCONTENTBACKGROUND;

    static const QString FORMAT_DATE;
    static const QString FORMAT_TIME;
    static const QString ICON_ADD;

    static const QString LABEL_ADDACTIVITY;
    static const QString LABEL_CLOSEDEVENT;
    static const QString LABEL_REGISTERD;
    static const QString LABEL_NOTREGISTERD;
    static const QString LABEL_OPENEVENT;
    static const QString MESSAGE_OUTOFVACANCY;
    static const QString STRING_EMPTY;

};

#endif // ACTIVITYWIDGET_H
