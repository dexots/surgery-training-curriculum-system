#ifndef ADDROSTERBOX_H
#define ADDROSTERBOX_H

#include <QComboBox>
#include <QScrollArea>
#include <QFile>
#include <QTextStream>
#include <QDate>

#include "gui/popupboxincludes.h"
#include "objects/rosterassignobj.h"

class QComboBox;
class QScrollArea;
class QFile;
class QTextStream;
class QDate;

class RosterAssignObj;

namespace gui {
    class AddRosterBox;
}

class AddRosterBox:public QDialog{
    Q_OBJECT

public:
    AddRosterBox();
    ~AddRosterBox();

protected:
    void paintEvent(QPaintEvent *event);

private:
    void setUpMainLayout();
    void setUpMonthYearLabel();
    void setUpInputLabels();
    void setUpInputBoxes();
    void setUpSubmitCancelBtn();
    void setBoxAttributes();

    void passToModels();

    void changeColor(QColor font, QColor back, QWidget* widget);
    void setButtonStyleSheet(QPushButton *button);

private slots:
    void handleSubmit();
    void handleCancel();

private:

    QPushButton *_submitButton;
    QPushButton *_cancelButton;

    QComboBox *_monthBox;
    QComboBox *_yearBox;

    QVBoxLayout *_mainLayout;

    QFont _bold;
    QDate _curDate;

    QVector <InputBox*> _inputBoxes;
    QVector <RosterAssignObj*> _rosterAssignObjs;
    QScrollArea *_scrollArea;

// private constants
private:
    static const QString COLOR_BUTTONBACKGROUND;
    static const QString STYLE_BUTTONBORDER;
    static const QString WIDTH_BUTTONBORDER;
    static const QString RADIUS_BUTTONBORDER;
    static const QString COLOR_BUTTONBORDER;

    static const QString FONT_BUTTON;
    static const QString COLOR_BUTTONFONT;
    static const QString WIDTH_BUTTON;
    static const QString PADDING_BUTTON;

    static const int WIDTH_MINIMUM;
    static const int HEIGHT_MINIMUM;

    static const QString QSTRING_EMPTY;
    static const QString QSTRING_SPACE;

    static const QString FILENAME;

};


#endif // ADDROSTERBOX_H
