#ifndef RESEARCHADDBOX_H
#define RESEARCHADDBOX_H

#include <QComboBox>
#include <QTextStream>
#include <cassert>
#include <QApplication>
#include <QProcess>

#include "gui/popupboxincludes.h"
#include "gui/generalpopupbox.h"

class QComboBox;
class GeneralPopupBox;

namespace gui {
    class ResearchAddBox;
}

class ResearchAddBox:public QDialog {
    Q_OBJECT

public:
    ResearchAddBox();
    ResearchAddBox(QString titleString,
                    QString descString, QString collabString,
                     QString folderString, QString statusString,
                     QString initiatedString, QString teamLead);
    ~ResearchAddBox();

protected:
    void paintEvent(QPaintEvent *event);

private:
    void initializeVariable();
    void initializeEmptyStrings();
    void initializePreStrings(QString titleString,
                              QString descString, QString collabString,
                               QString folderString, QString statusString,
                               QString initiatedString, QString teamLead);
    void doBasicSetup();

    void setUpTitleLayout();
    void setUpDescLayout();
    void setUpCurrProjStatus();
    void setUpErrorLabel();
    void setUpTeamLead();
    void setUpInitiatedOnLayout();
    void setUpCollaboratorsLayout();
    void setUpFolderLink();
    void setUpAddAndCancelButton();
    void setBoxAttribute();
    void changeColor(QColor font, QColor back, QWidget* widget);
    void setButtonStyleSheet(QPushButton *button);
    void setUpWarningLabel();

    void writeToFile();
    void showSuccessMsg();

signals:
    void editedInfo(QString titleString,
                  QString descString, QString collabString,
                   QString folderString, QString statusString,
                   QString initiatedString, QString teamLead);


private slots:
    void handleAdd();
    void handleCancel();
    void handleTitle(QString inputString);
    void handleDescription(QString inputString);
    void handleCollaborators(QString inputString);
    void handleFolderLink(QString inputString);
    void handleTeamLead(QString inputString);

private:
    InputBox *_inputTitle;
    InputBox *_inputDesc;
    InputBox *_inputCollab;
    InputBox *_inputFolder;
    InputBox *_inputTeamLead;

    QComboBox *_statusBox;

    QLabel *_errorLabel;
    QLabel *_titleLabel;
    QLabel *_warningLabel;

    QPushButton *_addButton;
    QPushButton *_cancelButton;

    QString _titleString;
    QString _descString;
    QString _collabString;
    QString _folderString;
    QString _statusString;
    QString _initiatedString;
    QString _teamLead;
    QString _warningString;

    QFont _bold;

    QHBoxLayout *_titleLayout;
    QHBoxLayout *_descLayout;
    QHBoxLayout *_teamLeadLayout;
    QHBoxLayout *_collabLayout;
    QHBoxLayout *_statusLayout;
    QHBoxLayout *_initiatedLayout;
    QHBoxLayout *_folderLayout;
    QHBoxLayout *_buttonLayout;
    QVBoxLayout *_boxLayout;

    bool _isEdit;

// private constants
private:
    static const QString COLOR_BUTTONBACKGROUND;
    static const QString STYLE_BUTTONBORDER;
    static const QString WIDTH_BUTTONBORDER;
    static const QString RADIUS_BUTTONBORDER;
    static const QString COLOR_BUTTONBORDER;
    static const QString FONT_BUTTON;
    static const QString COLOR_BUTTONFONT;
    static const QString WIDTH_BUTTON;
    static const QString PADDING_BUTTON;
    static const int WIDTH_MINIMUM;
    static const int HEIGHT_MINIMUM;

    static const QString QSTRING_EMPTY;

    static const QString STATES_CONST[7];
    static const QString RESEARCH_FILE;
};


#endif // RESEARCHADDBOX_H
