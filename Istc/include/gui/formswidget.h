#ifndef FORMWIDGET_H
#define FORMWIDGET_H

#include <sys/stat.h>
#include <QVBoxLayout>
#include <QVector>
#include <QLabel>
#include <QPixmap>
#include <QColor>
#include <QString>
#include <QStandardPaths>
#include <QFile>
#include <QFileDialog>
#include <QDebug>

#include "gui/browser.h"
#include "gui/tablewidget.h"
#include "gui/clickablewidget.h"
#include "gui/errorbox.h"
#include "gui/generalpopupbox.h"
#include "model/trainingmodel.h"

class QVBoxLayout;
class QLabel;
class QPixmap;
class QColor;
class QString;
class QStandardPaths;
class QFile;
class QFileDialog;

class Browser;
class TableWidget;
class ClickableWidget;
class ErrorBox;
class CvBox;
class TrainingModel;

namespace Gui {
    class FormsWidget;
}

class FormsWidget:public QWidget {
    Q_OBJECT

public:
    explicit FormsWidget();
    ~FormsWidget();

    void addRowsToTable();
private:
    void setUpFormsLayout();
    void setUpOnlineFormTable();
    void setUpSoftcopyFormTable();
    void setUpDummyWidget();
    void downloadSuccess(QString saveFileName);
    void downloadFailure();
    bool isFileExists (const std::string& fileName);

public slots:
    void updateTable();

private slots:
    bool downloadFile();
    void handleLoadWebpage(QString urlString);

//class variables
private:
    QVBoxLayout *_formsLayout;
    TableWidget *_formsTable;
    QVector <ClickableWidget*> _forms;

//constants
private:
    static const int HEIGHT_ROW;
    static const int NUMBER_COLUMN;

    static const QVector<int> WIDTH_COLUMN;

    static const QVector<QString> HEADER_TABLE1;
    static const QVector<QString> HEADER_TABLE2;

    static const QColor COLOR_CONTENTBACKGROUND;
    static const QColor COLOR_CONTENTFONT;
    static const QColor COLOR_HEADERBACKGROUND;
    static const QColor COLOR_HEADERTEXT;
    static const QColor COLOR_SUBMITTEDCONTENTFONT;

    static const QString ICON_DOWNLOAD;
    static const QString ICON_DOWNLOADED;
};

#endif // FORMWIDGET_H
