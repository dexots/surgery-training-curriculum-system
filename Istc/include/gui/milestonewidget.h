#ifndef MILESTONEWIDGET_H
#define MILESTONEWIDGET_H

#include <cassert>
#include <QDebug>
#include <QLabel>
#include <QVBoxLayout>
#include <QString>
#include <QVector>
#include <QTextStream>
#include <cassert>
#include <QWidget>

#include "gui/progressbar.h"
#include "gui/clickablewidget.h"
#include "gui/milestonebox.h"
#include "gui/popupmsgbox.h"
#include "gui/tablewidget.h"

class QDebug;
class QLabel;
class QVBoxLayout;
class QString;
class QTextStream;
class QWidget;

class ProgressBar;
class ClickableWidget;
class MilestoneBox;
class TableWidget;

namespace Gui {
    class MilestoneWidget;
}

class MilestoneWidget:public QWidget {
    Q_OBJECT

public:
    explicit MilestoneWidget();
    ~MilestoneWidget();

private:
    void addProcedureMilestones();
    void populateData(QString data);
    void readMilestoneFile();
    void setUpAddLayout();
    void setUpMilestoneLayout();
    void setUpProcedureMilestones();
    void setUpDummyWidget();
    QLabel* setUpBarLabel(QString label, bool bold);
    void addProcedureProgressBar(QString label, int minValue, int maxValue,
                      bool isBadToGood, int progressValue);
    ProgressBar* addProgressBar(QString label, int minValue, int maxValue,
                        bool isBadToGood, int progressValue);
    ProgressBar* setUpProgressBar(int minValue, int maxValue, bool isBadToGood,
                          int progressValue);
    ClickableWidget* createClickableAddWidget(QString accessibleName);
    ClickableWidget* createClickableMinusWidget(QString accessibleName);
    ClickableWidget* createClickableMoreWidget(QString accessibleName);

private:
    //QVBoxLayout *_procedureLayout;
    QVBoxLayout *_milestoneLayout;
    TableWidget *_procedureMilestonesTable;
    int _procSum;
    QVector <QStringList> _procMilestone;
    QVector <ProgressBar*> _procedureProgressBars;
    ProgressBar *_procBar;
    QWidget *_dummyWidget;

private:
    static const int HEIGHT_PROGRESSBAR;

    static const QString ICON_ADD;
    static const QString ICON_MINUS;
    static const QString ICON_MORE;
    static const QString MILESTONE_FILE;
    static const QString DELIMITER;
    static const QString STRING_NEWLINE;
    static const QString STRING_EMPTY;
    static const QString STRING_COMMA;
    static const QString STRING_COLON;
    static const QString STRING_SPACE;
    static const QString FORMAT_DATE;

    static const int NUMBER_COLUMN;
    static const int NUM_PROC;
    static const int SIZE_PROC;

    static const QColor COLOR_CONTENTBACKGROUND;
    static const QColor COLOR_CONTENTFONT;

    static const QVector<int> WIDTH_COLUMN;

private slots:
    void handleAdd();
    void receivedMilestone(QString title, int completedNumber, int requiredNumber);
    void writeMilestoneFile();
    void showProgressBarRecords(QString accessibleName);
    void addRecordedDate(QString accessibleName);
    void removeRecordedDate(QString accessibleName);
};

#endif // MILESTONEWIDGET_H
