#ifndef ROSREQ_H
#define ROSREQ_H

#include <iostream>

#include <QDate>
#include <QLabel>
#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QFont>
#include <QPalette>
#include <QString>
#include <QVector>
#include <QSizePolicy>
#include <QPushButton>

#include "gui/tablewidget.h"
#include "gui/clickablewidget.h"
#include "gui/rosreqbox.h"
#include "model/rostermodel.h"
#include "objects/rosterrequestobj.h"
#include "objects/userdetails.h"

namespace gui {
    class RosReq;
}

class RosReq:public QWidget {
    Q_OBJECT

public:
    RosReq();
    ~RosReq();

private:
    void colorWidget(QColor font, QColor background, QWidget *widget);
    void setButtonStyleSheet(QPushButton *button);
    void addRowsToTable();

signals:
    void rosterRequestAdded();

public slots:
    void updateTable();

private slots:
    void handlePickDate();
    void handleResize(int changedSize);
    void updateInfo(QVector<QString>);

private:
    QLabel *_req;
    QLabel *_appDateLabel;
    QLabel *_period;
    QLabel *_fromDateLabel;
    QLabel *_to;
    QLabel *_toDateLabel;
    QLabel *_reqHistory;

    QHBoxLayout *_reqLayout;

    TableWidget *_hisTable;

    QDate _todayDate;
    QDate _appDate;
    QDate _fromDate;
    QDate _toDate;
    QVBoxLayout *_reqRosLayout;
    QWidget *_dummyWidget;

    int _appContRow;
    int _hisContRow;

    RosterModel* _rosterModel;

private:
    static const QColor HEADER_BACK;
    static const QColor HEADER_FONT;
    static const QColor CONT_BACK;
    static const QColor CONT_FONT;
    static const QColor MY_OWN_COLOR;

    static const QString COLOR_APPLYBUTTON;
    static const QString COLOR_BUTTONBACKGROUND;
    static const QString COLOR_BUTTONFONT;
    static const QString FONT_BUTTON;
    static const QString LEFTMARGIN_BUTTON;
    static const QString PADDING_BUTTON;
    static const QString RADIUS_BUTTONBORDER;
    static const QString STATUS_PENDINGS;
    static const QString WIDTH_BUTTON;
};

#endif // ROSREQ_H
