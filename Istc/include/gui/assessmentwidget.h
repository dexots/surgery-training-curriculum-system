#ifndef ASSESSMENTWIDGET_H
#define ASSESSMENTWIDGET_H

#include <iostream>

#include <QDate>
#include <QString>
#include <QVBoxLayout>
#include <QDebug>
#include <QWidget>

#include "gui/tablewidget.h"
#include "gui/assessmentbox.h"
#include "model/personalmodel.h"
#include "objects/assessmentobj.h"
#include "objects/userdetails.h"

class QVBoxLayout;
class QDebug;
class QDate;
class QString;

class TableWidget;
class AssessmentBox;

namespace Gui {
    class AssessmentWidget;
}

class AssessmentWidget:public QWidget {
    Q_OBJECT

public:
    explicit AssessmentWidget();
    ~AssessmentWidget();

private:
    void getUserDetails();
    void setUpAddIconForTrainer();
    void setUpAssessmentLayout();
    void setUpAssessmentTable();
    void setWidgetStyleSheet();
    void setUpDummyWidget();
    void addAssessmentsToTable();

    void invalidateLayout(QLayout *layout);

//class variables
private:
    TableWidget *_assessmentTable;
    QVBoxLayout *_assessmentLayout;
    QWidget *_dummyWidget;

    PersonalModel *_personalModel;

    QString _role;
    QString _matricNum;

    QVector<AssessmentObj> *_assessments;


//constants
private:
    static const int NUMBER_COLUMN;

    static const QVector<int> WIDTH_COLUMN;

    static const QVector<QString> HEADER_TABLE_TRAINEE;
    static const QVector<QString> HEADER_TABLE_TRAINER;

    static const QColor COLOR_CONTENTBACKGROUND;
    static const QColor COLOR_CONTENTFONT;
    static const QColor COLOR_HEADERBACKGROUND;
    static const QColor COLOR_HEADERTEXT;

    static const QString SIZE_FONT;

    static const QString ADD_ICON;

public slots:
    void updateTable();

private slots:
    void addAssessment();
    void assessmentReceived(QDate date, QVector<QString> info);

    void handleTableResized(int minHeightChanged);
};


#endif // ASSESSMENTWIDGET_H
