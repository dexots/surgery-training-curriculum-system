#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <iostream>

//@deprecate webkit settings
//#include <QWebSettings>


#include <QMainWindow>
#include <QLayout>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QDesktopWidget>
#include <QRect>
#include <QList>
#include <QUrl>
#include <QUrlQuery>
#include <QtGui>
#include <QtCore>
#include <QTextStream>
#include <QStringList>

#include "dbclient/localdatabaseclient.h"
#include "gui/topwidget.h"
#include "gui/botleftwidget.h"
#include "gui/botrightmain.h"
#include "gui/loginui.h"
#include "gui/resourcewidget.h"
#include "gui/rosterwidget.h"
#include "gui/researchwidget.h"
#include "gui/trainingwidget.h"
#include "gui/logwidget.h"
#include "gui/profilewidget.h"
#include "gui/errorbox.h"
#include "gui/browser.h"
#include "objects/userdetails.h"

class QMainWindow;
class QLayout;
class QHBoxLayout;
class QVBoxLayout;
class QDesktopWidget;
class QRect;
class QUrl;
class QUrlQuery;
class QtGui;
class QtCore;

class LocalDatabaseClient;
class TopWidget;
class BotLeftWidget;
class BotRightMain;
class LoginUi;
class ResourceWidget;
class RosterWidget;
class ResearchWidget;
class TrainingWidget;
class LogWidget;
class ProfileWidget;
class ErrorBox;
class Browser;
class UserDetails;

namespace Gui {
    class MainWindow;
}

class MainWindow:public QMainWindow {
    Q_OBJECT


public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    virtual bool eventFilter(QObject *, QEvent *);

private:
    void clearGarbage();
    void initializeClass();
    void initializeWindow();
    void initializeBrowser();
    void initializeTopWidget();
    void initializeDatabase();
    void initializeBotLeftWidget();
    void initializeBotRightMain();
    void initializeProfileWidget();
    void initializeRosterWidget();
    void initializeResourcesWidget();
    void initializeResearchWidget();
    void initializeTrainingWidget();
    void initializeLogWidget();
    void initializeLogin();

    void hideAllWidgets();
    void hideBotRightWidgets();
    void setupPage();
    void showPage();

    bool createUserProfileFile(UserDetails* userDetails);
    bool isUserMatches(QString matricNo);
    bool retrieveDetailsFromFile();
    bool isFileExist(QString fileName);
    void setupLayout();
    void setupWidget();
    void maximizeWindow();
    void checkLoginStat();

    void showErrorBox();

private slots:
    void loadUrl(QString hyperlink, QString sender);
    void hideBrowser();
    void viewProfile();
    void viewTrain();
    void viewRos();
    void viewLog();
    void viewResearch();
    void viewResources();
    void backHome();
    void userHasLogin();
    void syncEveryWidget();

private:
    bool _isLogin;
    int _clientWidth;
    int _clientHeight;
    QString _urlSender;
    QVector<QString> *_userInfo;
    LocalDatabaseClient *_localData;
    LoginUi *_loginView;
    TopWidget *_topWidget;
    BotLeftWidget *_botLeftWidget;
    BotRightMain *_botRightMain;
    QHBoxLayout *_botLayout;
    QVBoxLayout *_windowLayout;
    QWidget *_windowWidget;
    ResourceWidget *_resourceWidget;
    RosterWidget *_rosterWidget;
    ResearchWidget *_researchWidget;
    TrainingWidget *_trainingWidget;
    LogWidget *_logWidget;
    ProfileWidget *_profileWidget;
    Browser *_browser;

private:
    static const QString NAME_FILE_USER;

};

#endif // MAINWINDOW_H
