#ifndef RESEARCHBOX_H
#define RESEARCHBOX_H

#include <QComboBox>
#include <QStringList>
#include <QLineEdit>

#include "gui/popupboxincludes.h"

class QStringList;
class QComboBox;
class QLineEdit;

namespace gui {
    class ResearchBox;
}

class ResearchBox:public QDialog {
    Q_OBJECT

public:
    ResearchBox(QString contentLabel, QString details);
    ~ResearchBox();

protected:
    void paintEvent(QPaintEvent *event);

private:
    void changeColor(QColor font, QColor back, QWidget* widget);
    void setButtonStyleSheet(QPushButton *button);

    void setUpDetailsLayout();
    void setUpAddCancelLayout();
    void setUpMainLayout();

private slots:
    void handleContent();
    void handleStatusChange();
    void handleButton();
    void handleCancel();

signals:
    void passInfo(QString states, QString details);

private:
    InputBox *_detailsBox;
    QLabel *_contentLabel;
    QLabel *_detailsLabel;
    QLabel *_statusLabel;

    QPushButton *_addButton;
    QPushButton *_cancelButton;
    QComboBox *_statusBox;

    QVBoxLayout *_mainLayout;
    QVBoxLayout *_detailsLayout;
    QHBoxLayout *_addCancelLayout;
    QVBoxLayout *_statusLayout;

    QString _details;
    QString _status;
    QString _state;

    QFont _bold;

private:
    static const QString COLOR_BUTTONBACKGROUND;
    static const QString STYLE_BUTTONBORDER;
    static const QString WIDTH_BUTTONBORDER;
    static const QString RADIUS_BUTTONBORDER;
    static const QString COLOR_BUTTONBORDER;
    static const QString FONT_BUTTON;
    static const QString COLOR_BUTTONFONT;
    static const QString WIDTH_BUTTON;
    static const QString PADDING_BUTTON;
    static const int WIDTH_MINIMUM;
    static const int HEIGHT_MINIMUM;

    static const QString QSTRING_EMPTY;
    static const QString QSTRING_UPDATE;

};

#endif // RESEARCHBOX_H
