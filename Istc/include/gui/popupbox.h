#ifndef POPUPBOX_H
#define POPUPBOX_H

#include "gui/popupboxincludes.h"
#include "gui/inputbox.h"

namespace gui{
    class PopupBox;
}

class QDialog;
class QHBoxLayout;
class QVBoxLayout;
class QLabel;
class QWidget;
class QFont;
class QPainter;
class QTextEdit;
class QFrame;
class QSizePolicy;
class QString;
class QPushButton;
class QDate;
class InputBox;

class PopupBox : public QDialog{
    Q_OBJECT

public:
    PopupBox();
    ~PopupBox();

protected:
    void paintEvent(QPaintEvent *event);
    void setButtonStyleSheet(QPushButton *button);

private:
    void changeColor(QColor font, QColor back, QWidget* widget);
    void setWidth(int width);
    void setHeight(int height);

private slots:
    void handleButton();
    void handleCancel();

// private constants
private:
    static const QString COLOR_BUTTONBACKGROUND;
    static const QString STYLE_BUTTONBORDER;
    static const QString WIDTH_BUTTONBORDER;
    static const QString RADIUS_BUTTONBORDER;
    static const QString COLOR_BUTTONBORDER;
    static const QString FONT_BUTTON;
    static const QString COLOR_BUTTONFONT;
    static const QString WIDTH_BUTTON;
    static const QString PADDING_BUTTON;
    static int WIDTH_MINIMUM;
    static int HEIGHT_MINIMUM;
};

#endif // POPUPBOX_H
