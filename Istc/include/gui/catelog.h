#ifndef CATELOG_H
#define CATELOG_H

#include "catebox.h"

namespace Gui{
    class CateLog;
}

class CateTrain : public CateBox {

public:
    explicit CateTrain();

private:
    void initializeCateTrain();
};


#endif // CATELOG_H
