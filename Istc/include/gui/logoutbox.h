#ifndef LOGOUTBOX_H
#define LOGOUTBOX_H

#include <QApplication>

#include "gui/popupboxincludes.h"
#include "gui/inputbox.h"

class QApplication;

class InputBox;

namespace gui{
    class LogoutBox;
}

class LogoutBox:public QDialog{
    Q_OBJECT

public:
    LogoutBox();
    ~LogoutBox();

private:
    void setButtonStyleSheet(QPushButton *button, QString buttonColour);
    void paintEvent(QPaintEvent *event);

private slots:
    void handleCancel();

private:
    QFont _bold;
    QLabel *_exitLabel;
    QPushButton *_okButton;
    QPushButton *_cancelButton;

private:
    static const QString STRING_EXIT;
    static const QString STRING_YES;
    static const QString STRING_NO;

    static const QString COLOR_BUTTONGREENBACKGROUND;
    static const QString COLOR_BUTTONREDBACKGROUND;
    static const QString COLOR_BUTTONBORDER;
    static const QString COLOR_BUTTONFONT;
    static const QString FONT_BUTTON;
    static const QString PADDING_BUTTON;
    static const QString RADIUS_BUTTONBORDER;
    static const QString STYLE_BUTTONBORDER;
    static const QString WIDTH_BUTTON;
    static const QString WIDTH_BUTTONBORDER;
    static const int WIDTH_MINIMUM;
    static const int HEIGHT_MINIMUM;

};

#endif // LOGOUTBOX_H
