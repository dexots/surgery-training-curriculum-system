#ifndef GENERALPOPUPBOX_H
#define GENERALPOPUPBOX_H

#include <iostream>

#include <QDialog>
#include <QVBoxLayout>
#include <QLabel>
#include <QWidget>
#include <QFont>
#include <QPainter>
#include <QBrush>
#include <QRect>
#include <QPushButton>
#include <QColor>
#include <QPalette>
#include <QString>
#include <QDate>
#include <QFrame>
#include <QSizePolicy>
#include <QVector>

#include "gui/inputbox.h"

class QDialog;
class QVBoxLayout;
class QLabel;
class QWidget;
class QFont;
class QPainter;
class QBrush;
class QRect;
class QPushButton;
class QColor;
class QPalette;
class QString;
class QDate;
class QFrame;
class QSizePolicy;

namespace gui {
    class GeneralPopupBox;
}

class GeneralPopupBox:public QDialog {
    Q_OBJECT

public:
    GeneralPopupBox(QString saveFileName, int type);
    ~GeneralPopupBox();

protected:
    void paintEvent(QPaintEvent *event);

private:
    void changeColor(QColor font, QColor back, QWidget* widget);
    void setButtonStyleSheet(QPushButton *button);

    void setSuccessLabel(QString saveFileName, int type);

private slots:
    void handleButton();

private:
    QLabel *_successLabel;
    QPushButton *_okButton;
    QVBoxLayout *_mainLayout;
    QHBoxLayout *_buttonLayout;

    QFont _bold;

// private constants
private:
    static const QString COLOR_BUTTONBACKGROUND;
    static const QString STYLE_BUTTONBORDER;
    static const QString WIDTH_BUTTONBORDER;
    static const QString RADIUS_BUTTONBORDER;
    static const QString COLOR_BUTTONBORDER;
    static const QString FONT_BUTTON;
    static const QString COLOR_BUTTONFONT;
    static const QString WIDTH_BUTTON;
    static const QString PADDING_BUTTON;

    static const QString STRING_CV_SUCCESS;
    static const QString STRING_FORM_SUCCESS;
    static const QString STRING_SETTINGS_SUCCESS;
    static const QString STRING_GENERAL_SUCCESS;

    static const int WIDTH_MINIMUM;
    static const int HEIGHT_MINIMUM;

};

#endif // GENERALPOPUPBOX_H
