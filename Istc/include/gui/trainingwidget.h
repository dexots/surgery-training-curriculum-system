#ifndef TRAININGWIDGET_H
#define TRAININGWIDGET_H

#include <QLabel>
#include <QLayout>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QWidget>
#include <QVector>

#include "gui/catebox.h"
#include "gui/subcate.h"
#include "gui/catetable.h"
#include "gui/milestonewidget.h"
#include "gui/activitywidget.h"
#include "gui/examinationwidget.h"
#include "gui/formswidget.h"

#include "objects/userdetails.h"

class QLabel;
class QLayout;
class QVBoxLayout;
class QHBoxLayout;
class QWidget;

class CateBox;
class SubCate;
class CateTable;
class MilestoneWidget;
class ActivityWidget;
class ExaminationWidget;
class FormsWidget;

namespace Gui {
    class TrainingWidget;
}

class TrainingWidget:public QWidget {
    Q_OBJECT

public:
    explicit TrainingWidget();
    ~TrainingWidget();

private:
    void createLayout();
    void colorWidget(QColor color, QWidget *widget);
    void getUserRole();
    void addMilestoneWidget();
    void addActivityWidget();
    void addExaminationWidget();
    void addFormsWidget();

private:
    QVBoxLayout *_widgetLayout;
    SubCate *_milestoneCate;
    SubCate *_activityCate;
    SubCate *_examCate;
    SubCate *_formCate;

    QString _userRole;
    QVector <SubCate*> _trainingSubCategories;

public slots:
    void updateAllTables();

private slots:
    void handleHomeSignal();

signals:
    void backToHome();
    void updateActivityTable();
    void updateExamTable();
    void updateFormsTable();
};

#endif // TRAININGWIDGET_H
