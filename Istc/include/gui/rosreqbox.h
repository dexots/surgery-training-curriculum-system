#ifndef ROSREQBOX_H
#define ROSREQBOX_H

#include <QDate>
#include <QDateEdit>

#include "gui/inputbox.h"
#include "gui/popupboxincludes.h"

class QDate;
class QDateEdit;

class InputBox;
class PopupBoxIncludes;

namespace gui {
    class RosReqBox;
}

class RosReqBox:public QDialog {
    Q_OBJECT

public:
    RosReqBox();
    ~RosReqBox();

protected:
    void paintEvent(QPaintEvent *event);

private:
    void initialise();
    void makeTitle();
    void makeStartDateLayout();
    void makeEndDateLayout();
    void makeButtonLayout();
    void makeErrorLabel();
    void makeReasonLayout();
    void makeMainLayout();

    void changeColor(QColor font, QColor back, QWidget* widget);
    void setButtonStyleSheet(QPushButton *button);

    bool dateLaterThanCurrent();

private slots:
    void handleButton();
    void handleCancel();
    void handleRemarks(QString string);

    void handleStartDate(QDate startDate);
    void handleEndDate(QDate endDate);

signals:
    void passInfo(QVector<QString> info);

private:
    QFont _bold;

    QLabel *_titleLabel;
    QLabel *_monthLabel;
    QLabel *_startDateLabel;
    QLabel *_endDateLabel;
    QLabel *_reasonLabel;
    QLabel *_errorLabel;

    QDateEdit *_startDate;
    QDateEdit *_endDate;
    QString _startDateString;
    QString _endDateString;

    InputBox *_reasonBox;
    QString _reasonString;

    QPushButton *_submitBtn;
    QPushButton *_cancelBtn;

    QHBoxLayout *_startDateLayout;
    QHBoxLayout *_endDateLayout;
    QHBoxLayout *_buttonLayout;
    QVBoxLayout *_reasonLayout;
    QVBoxLayout *_mainLayout;

    QVector <QString> _request;
    QDate _currentDate;


private:
    static const QString COLOR_BUTTONBACKGROUND;
    static const QString STYLE_BUTTONBORDER;
    static const QString WIDTH_BUTTONBORDER;
    static const QString RADIUS_BUTTONBORDER;
    static const QString COLOR_BUTTONBORDER;
    static const QString FONT_BUTTON;
    static const QString COLOR_BUTTONFONT;
    static const QString WIDTH_BUTTON;
    static const QString PADDING_BUTTON;

    static const QString TAG_TITLE;
    static const QString TAG_START_DATE;
    static const QString TAG_END_DATE;
    static const QString TAG_REASON;
    static const QString TAG_SUBMIT;
    static const QString TAG_CANCEL;
    static const QString TAG_ERROR;
    static const QString TAG_DATE_ERROR;

    static const QString SETTINGS_FILE;

    static const int WIDTH_MINIMUM;
    static const int HEIGHT_MINIMUM;
};

#endif // ROSREQBOX_H
