#ifndef CATETABLE_H
#define CATETABLE_H

#include <iostream>

#include <QWidget>
#include <QString>
#include <QColor>
#include <QPixmap>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QSizePolicy>
#include <QFont>
#include <QPalette>
#include <QVector>

#include "gui/subcate.h"
#include "gui/clickablewidget.h"

class QWidget;
class QString;
class QColor;
class QHBoxLayout;
class QVBoxLayout;
class QLabel;
class QSizePolicy;
class QFont;
class QPalette;

class SubCate;
class ClickableWidget;

namespace gui {
    class CateTable;
}

class CateTable : public QWidget {
    Q_OBJECT

public:
    CateTable(QString headerName,
              QString headerDesc,
              QString headerIcon,
              QColor headerColor,
              QVector <SubCate*> subCategories);
    ~CateTable();

private:
    void initializeClass();
    void createHeader(QString headerName,
                      QString headerDesc,
                      QString headerIcon,
                      QColor headerColor);
    void insHeaderTitle(QString headerName);
    void insHeaderDesc(QString headerDesc);
    void insHeaderLogo(QString headerIcon);
    void insHeaderLogo();
    void setupHomeButton();
    void createHeaderLayout();
    void createHeaderWidget(QColor headerColor);
    void colorWidget(QColor headerColor, QWidget *widget);
    void createSubCatBar(QVector <SubCate*> subCategories);
    void addInCatCorWidget(QVector <SubCate*> subCategories);
    void setupTable();
    QPixmap getPixmapBy(const char *pixmap);

private:
    QWidget *_headerWidget;

    QVector<SubCate*> _subCatWidget;
    QLabel *_headIconLabel;
    QLabel *_headNameLabel;
    QLabel *_headDescLabel;

    QHBoxLayout *_headerLayout;
    QVBoxLayout *_headNameAndDesc;
    QVBoxLayout *_subCateLayout;
    QHBoxLayout *_botLayout;
    QVBoxLayout *_tableLayout;

    ClickableWidget *_homeLabel;

    int _prevCorWidgetIdx;

private:
    static const char *HOME_BUTTON;
    static const char *HEAD_FONT_NAME;
    static const int HEAD_NAME_SIZE;
    static const int HEAD_DESC_SIZE;
    static const int HEAD_HEIGHT;
    static const int SUB_CAT_COL_SIZE;
    static const int LAYOUT_SPACE;

private slots:
    void handleHomeButton();
    void changeView();

signals:
    void backToHome();

};

#endif // CATETABLE_H
