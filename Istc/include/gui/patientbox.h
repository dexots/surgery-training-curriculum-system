#ifndef PATIENTBOX_H
#define PATIENTBOX_H

#include <iostream>
#include <cassert>

#include <QDialog>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QWidget>
#include <QFont>
#include <QPainter>
#include <QBrush>
#include <QRect>
#include <QPushButton>
#include <QColor>
#include <QPalette>
#include <QTextEdit>
#include <QString>
#include <QDateTime>
#include <QFrame>
#include <QSizePolicy>
#include <QVector>
#include <QComboBox>
#include <QDateEdit>
#include <QTimeEdit>
#include <QFile>
#include <QTextStream>
#include <QDebug>

#include "gui/inputbox.h"

class QDialog;
class QHBoxLayout;
class QVBoxLayout;
class QLabel;
class QWidget;
class QFont;
class QPainter;
class QBrush;
class QRect;
class QPushButton;
class QColor;
class QPalette;
class QTextEdit;
class QString;
class QDateTime;
class QFrame;
class QSizePolicy;
class QDateEdit;
class QTimeEdit;
class QFile;
class QDebug;

class InputBox;

namespace gui {
    class PatientBox;
}

class PatientBox:public QDialog {
    Q_OBJECT

public:
    PatientBox();
    PatientBox(QString patientID, QString procedureDate, QString patientDOB,
               QString procedureTime, int procedureCodeIndex,
               QString procedureRemarks, QString outcome);
    ~PatientBox();

public:
    void setEditMode();

protected:
    void paintEvent(QPaintEvent *event);

private:
    void initializeBoxLayout();
    void initializeQString();

    void setUpPatientDetailsLabel();
    void setUpIdAndDateInput(const QString &patientID = "", const QString &procedureDate = "");
    void setUpDobAndTimeInput(const QString &patientDOB = "", const QString &procedureTime = "");
    void setUpProcedureCodeInput(int procedureCodeIndex = 0);
    void setUpOutcomeInput();
    void setUpOutcomeInput(QString outcome);
    void setUpRemarksInput(const QString &procedureRemarks = "");
    void setUpErrorLabel();
    void setUpAddAndCancelButton();
    void setUpUpdateAndCancelButton();
    void setPatientBoxAttribute();

    void addToBoxLayout(QHBoxLayout *toBeAddedLayout);
    void addToBoxLayout(QWidget *toBeAddedWidget);
    void updateInputDate(QDateEdit *dateEdit, QString newDate);
    void updateInputTime(QTimeEdit *timeEdit, QString newTime);
    void changeColor(QColor font, QColor back, QWidget* widget);
    void setButtonStyleSheet(QPushButton *button);
    void createCodeItems();
    void readCodeFile();

private slots:
    void handleLoginId();
    void handleDob(QDate inputString);
    void handleDate(QDate inputString);
    void handleTime(QTime inputString);
    void handleCode(QString inputString);
    void handleRemarks(QString inputString);
    void handleOutcome(QString inputString);
    void handleAddOrUpdate();
    void handleCancel();

signals:
    void passInfo(QVector<QString> info);

private:
    QLabel *_patientDetails;
    QLabel *_id;
    QLabel *_date;
    QLabel *_dob;
    QLabel *_time;
    QLabel *_codeLabel;
    QLabel *_outcomeLabel;
    QLabel *_remarks;
    QLabel *_errorLabel;

    QFont _boldAndUnderline;
    QFont _bold;

    QTextEdit *_inputId;
    InputBox *_inputDob;
    InputBox *_inputRemarks;

    QDate _currentDate;

    QTime _currentTime;

    QDateEdit *_actualDate;
    QDateEdit *_inputDobEdit;

    QTimeEdit *_actualTime;

    QComboBox *_codeBox;
    QComboBox *_outcomeBox;

    QPushButton *_addButton;
    QPushButton *_updateButton;
    QPushButton *_cancelButton;

    QHBoxLayout *_buttonLayout;
    QHBoxLayout *_outcomeLayout;
    QVBoxLayout *_boxLayout;

    QString _idString;
    QString _dateString;
    QString _dobString;
    QString _timeString;
    QString _codeString;
    QString _remarksString;
    QString _outcomeString;

    QVector <QString> _codes;

private:
    static const QString COLOR_BUTTONBACKGROUND;
    static const QString STYLE_BUTTONBORDER;
    static const QString WIDTH_BUTTONBORDER;
    static const QString RADIUS_BUTTONBORDER;
    static const QString COLOR_BUTTONBORDER;
    static const QString FONT_BUTTON;
    static const QString COLOR_BUTTONFONT;
    static const QString WIDTH_BUTTON;
    static const QString PADDING_BUTTON;
    static const QString STRING_COLON;
    static const QString STRING_EMPTY;
    static const QString STRING_SPACE;
    static const QString INPUT_FILE;
};


#endif // PATIENTBOX_H
