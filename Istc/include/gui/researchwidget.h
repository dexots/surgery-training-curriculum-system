#ifndef RESEARCHWIDGET_H
#define RESEARCHWIDGET_H

#include <QLabel>
#include <QLayout>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QWidget>
#include <QVector>
#include <QTextStream>

#include "gui/researchinstance.h"
#include "gui/catebox.h"
#include "gui/subcate.h"
#include "gui/catetable.h"
#include "gui/popupmsgbox.h"

class QLabel;
class QLayout;
class QVBoxLayout;
class QHBoxLayout;
class QWidget;
class QTextStream;

class CateBox;
class SubCate;
class CateTable;

namespace Gui {
    class ResearchWidget;
}

class ResearchWidget: public QWidget {
    Q_OBJECT

public:
    explicit ResearchWidget();
    ~ResearchWidget();

    QString getClassName();

private:
    void createLayout();
    void colorWidget(QColor color, QWidget *widget);

    void readResearchFile();
    void createResearchTable();
    void populateData (QString data);
    void createSubCate(QString cateDetails);

private:
    QVBoxLayout *_widgetLayout;
    QVector <SubCate*> _researchSubCategories;
    bool _hasProjects;

private slots:
    void handleHomeSignal();
    void handleHyperlink(QString hyperlink, QString sender);

signals:
    void backToHome();
    void load(QString hyperlink, QString sender);
};

#endif // RESEARCHWIDGET_H
