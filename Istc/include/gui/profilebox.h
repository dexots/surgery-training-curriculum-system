#ifndef PROFILEBOX_H
#define PROFILEBOX_H

#include <QIntValidator>
#include <QRegExpValidator>
#include <QLineEdit>

#include "gui/popupboxincludes.h"

class QIntValidator;
class QRegExpValidator;
class QLineEdit;

namespace gui {
    class ProfileBox;
}

class ProfileBox:public QDialog{
    Q_OBJECT

public:
    ProfileBox(QString contentLabel, QString inputString);
    ~ProfileBox();

protected:
    void paintEvent(QPaintEvent *event);

private:
    void changeColor(QColor font, QColor back, QWidget* widget);
    void setButtonStyleSheet(QPushButton *button);

    void setUpContentLabel();
    void setUpInputBox(QString type, QString inputString);
    void checkTypeAndSetRestrictions(QString type);
    void setUpUpdateLabel();
    void setUpAddCancelBtn();
    void setUpMainLayout();

    bool isNotEmailType();
    bool isValidEmailContent(QString content);

private slots:
    void handleContent(QString content);
    void handleButton();
    void handleCancel();

signals:
    void passInfo(QString type, QString info);

private:
    QLineEdit *_inputContent;
    QLabel *_contentLabel;
    QPushButton *_addButton;
    QPushButton *_cancelButton;

    QVBoxLayout *_mainLayout;
    QHBoxLayout *_addCancelLayout;

    QString _inputString;
    QString _type;

    QFont _bold;

// private constants
private:
    static const QString COLOR_BUTTONBACKGROUND;
    static const QString STYLE_BUTTONBORDER;
    static const QString WIDTH_BUTTONBORDER;
    static const QString RADIUS_BUTTONBORDER;
    static const QString COLOR_BUTTONBORDER;
    static const QString FONT_BUTTON;
    static const QString COLOR_BUTTONFONT;
    static const QString WIDTH_BUTTON;
    static const QString PADDING_BUTTON;
    static const int WIDTH_MINIMUM;
    static const int HEIGHT_MINIMUM;

    static const QString QSTRING_EMPTY;
    static const QString QSTRING_UPDATE;

};


#endif // PROFILEBOX_H
