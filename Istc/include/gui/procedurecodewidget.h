#ifndef CODEWIDGET_H
#define CODEWIDGET_H

#include <cassert>
#include <QWidget>
#include <QLabel>
#include <QPixmap>
#include <QString>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QSizePolicy>
#include <QPalette>
#include <QColor>
#include <QDateTime>
#include <QVector>
#include <QVectorIterator>

#include "gui/clickablewidget.h"
#include "gui/tablewidget.h"
#include "gui/patientbox.h"
#include "objects/proceduralcode.h"
#include "model/logbookmodel.h"

class QWidget;
class QLabel;
class QPixmap;
class QString;
class QHBoxLayout;
class QVBoxLayout;
class QSizePolicy;
class QPalette;
class QColor;
class QDateTime;

class ClickableWidget;
class TableWidget;
class PatientBox;
class ProceduralCode;

namespace gui {
    class CodeWidget;
}

class CodeWidget:public QWidget {
    Q_OBJECT

public:
    CodeWidget();
    ~CodeWidget();

public slots:
    void updateTable();

private slots:
    void addPatient();
    void havePatientDetails(QVector<QString> info);
    void updatePatientDetails();
    void handleResize(int changedSize);

private:
    void colorWidget(QColor background, QWidget *widget);
    void addData();

private:
    LogbookModel *_logbookModel;
    TableWidget *_table;
    QWidget *_dummyWidget;
    QVector <ClickableWidget*> _clicks;
    QVector <ProceduralCode*> *_proceduralCode;
    int _contRow;

};

#endif // CODEWIDGET_H
