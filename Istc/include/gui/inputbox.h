#ifndef INPUTBOX_H
#define INPUTBOX_H

#include <QTextEdit>
#include <QKeyEvent>
#include <QFocusEvent>
#include <iostream>

namespace gui {
    class InputBox;
}

class QTextEdit;
class QKeyEvent;
class QFocusEvent;

class InputBox:public QTextEdit {
    Q_OBJECT

public:
    InputBox();
    ~InputBox();

public:
    void keyPressEvent( QKeyEvent *e );
    void focusOutEvent( QFocusEvent *e );

signals:
    void stringReady(QString inputString);
};

#endif // INPUTBOX_H
