#ifndef EVENTBOX_H
#define EVENTBOX_H

#include "gui/popupboxincludes.h"

namespace Gui {
    class EventBox;
}

class EventBox:public QDialog {
    Q_OBJECT

public:
    EventBox(QString title, QString description, QString link);
    EventBox(QString title, QString description,
             QString link, bool isRegistered, bool canRegister = true);
    ~EventBox();

public:
    void hideButton();

protected:
    void paintEvent(QPaintEvent *event);
    void setButtonStyleSheet(QPushButton *button);
    void setCloseButton();
    void setRegisterButton();

private:
    void setupLayout();
    void setButtonLayout();
    void setRegisteredBtnText();
    void setRegisteredStatusText();

private:
    QFont _boldFont;

    QLabel *_titleLbl;
    QLabel *_descLbl;
    QLabel *_statusLbl;
    QLabel *_linkLbl;

    bool _isRegistered;

    QPushButton *_registerBtn;
    QPushButton *_closeBtn;

    QHBoxLayout *_buttonLayout;
    QVBoxLayout *_mainLayout;

private slots:
    void clickedCloseBtn();
    void clickedRegisteredBtn();

signals:
    void userHasClickedRegister(bool isRegister);

};

#endif // EVENTBOX_H
