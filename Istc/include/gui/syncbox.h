#ifndef SYNCBOX_H
#define SYNCBOX_H

#include <QMovie>
#include <QDebug>

#include "gui/popupboxincludes.h"
#include "dbclient/localdatabaseclient.h"

class QMovie;

namespace gui {
    class SyncBox;
}

class SyncBox:public QDialog {
    Q_OBJECT

public:
    SyncBox();
    ~SyncBox();

protected:
    void paintEvent(QPaintEvent *event);

private:
    void changeColor(QColor font, QColor back, QWidget* widget);
    void setButtonStyleSheet(QPushButton *button);

signals:
    void receivedSuccessSyncSignal();

private slots:
    void handleButton();
    void handleSyncSignal(bool isSuccess);

private:
    QLabel *_syncLabel;
    QLabel *_spinnerLabel;
    QPushButton *_okButton;
    QVBoxLayout *_mainLayout;
    QHBoxLayout *_spinnerLayout;
    QHBoxLayout *_buttonLayout;
    QLabel *_statusLabel;

    QFont _bold;
    QMovie *_spinner;

    LocalDatabaseClient *_localDbClient;

// private constants
private:
    static const QString COLOR_BUTTONBACKGROUND;
    static const QString STYLE_BUTTONBORDER;
    static const QString WIDTH_BUTTONBORDER;
    static const QString RADIUS_BUTTONBORDER;
    static const QString COLOR_BUTTONBORDER;
    static const QString FONT_BUTTON;
    static const QString COLOR_BUTTONFONT;
    static const QString WIDTH_BUTTON;
    static const QString PADDING_BUTTON;
    static const QString SYNC_TEXT;
    static const QString SYNC_FAIL;
    static const QString SYNC_SUCCESS;
    static const QString SPINNER_LOCATION;

    static const int WIDTH_MINIMUM;
    static const int HEIGHT_MINIMUM;

};

#endif // SYNCBOX_H
