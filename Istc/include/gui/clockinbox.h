#ifndef CLOCKINBOX
#define CLOCKINBOX

#include <QDateTimeEdit>

#include "gui/popupboxincludes.h"

#endif // CLOCKINBOX

namespace gui {
    class ClockInBox;
}

class ClockInBox:public QDialog {
    Q_OBJECT

public:
    ClockInBox(bool clockInOrOut);
    ~ClockInBox();

protected:
    void paintEvent(QPaintEvent *event);

private:
    void initializeVariables();
    void initializeTime();
    void setBoxLayout();
    void initializeButtons();
    void setButtonStyleSheet(QPushButton *button);

private slots:
    void handleAdd();
    void handleCancel();

signals:
    void passInfo(QDateTime time);

private:
    QLabel *_header;

    QDateTimeEdit *_actualTime;

    QPushButton *_clockButton;
    QPushButton *_cancelButton;

    QVBoxLayout *_boxLayout;
    QVBoxLayout *_timeLayout;
    QHBoxLayout *_buttonLayout;

    QString _clockInOrOut;

private:
    static const QString COLOR_BUTTONBACKGROUND;
    static const QString STYLE_BUTTONBORDER;
    static const QString WIDTH_BUTTONBORDER;
    static const QString RADIUS_BUTTONBORDER;
    static const QString COLOR_BUTTONBORDER;
    static const QString FONT_BUTTON;
    static const QString COLOR_BUTTONFONT;
    static const QString WIDTH_BUTTON;
    static const QString PADDING_BUTTON;

    static const QString QSTRING_EMPTY;
    static const QString QSTRING_CLOCKIN;
    static const QString QSTRING_CLOCKOUT;
    static const QString QSTRING_CANCEL;
};
