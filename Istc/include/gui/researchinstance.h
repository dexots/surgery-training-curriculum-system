#ifndef RESEARCHINSTANCE_H
#define RESEARCHINSTANCE_H

#include <iostream>
#include <cassert>

#include <QWidget>
#include <QLabel>
#include <QPalette>
#include <QColor>
#include <QFont>
#include <QPixmap>
#include <QString>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QSizePolicy>
#include <QVector>
#include <QFrame>
#include <QFile>
#include <QTextStream>

#include "gui/clickablewidget.h"
#include "gui/researchbox.h"
#include "gui/researchaddbox.h"
#include "gui/errorbox.h"
#include "objects/userdetails.h"

class QWidget;
class QLabel;
class QPalette;
class QColor;
class QFont;
class QPixmap;
class QString;
class QHBoxLayout;
class QVBoxLayout;
class QFrame;

class ClickableWidget;
class ResearchBox;
class ResearchAddBox;
class ErrorBox;
class UserDetails;

namespace gui {
    class ResearchInstance;
}

class ResearchInstance:public QWidget {
    Q_OBJECT

public:
    ResearchInstance(QString fileName);
    ResearchInstance();
    ~ResearchInstance();

public:
    void setTitle(QString title);
    void setDesc(QString desc);
    void setState(int state);
    void setIniDate(QString date);
    void setTeamLead(QString name);
    void setColla(QString names);

private:
    void colorWidget(QColor font, QColor back, QWidget *widget);
    void clearGarbage();
    void initiateClass();
    void doBasicSetup();
    void getUserRole();
    void setUpAddButton();

    void initialiseFonts();
    void setStatusLayout();
    void addInstanceLayout();
    void addProjectProgress();
    void addDateLayout();
    void addLeadLayout();
    void addColLayout();
    void addFolderLayout();

    // Widgets for Project Progress
    void addPlanningWidget();
    void addDSRBWidget();
    void addDataWidget();
    void addAnalysisWidget();
    void addSubmissionWidget();
    void addDraftWidget();
    void addAcceptedWidget();
    void makeClickableWidgets();

    void readResearchFile();
    void saveResearchFile();
    void showErrorBox();

    // Populate Data and helper functions
    void populateData(QString data);
    void setPlanning(QString planning);
    void setDSRB(QString dsrb);
    void setData(QString data);
    void setAnalysis(QString analysis);
    void setSubmission(QString submission);
    void setDrafting(QString drafting);
    void setAccepted(QString accepted);

    void setHyperLink(QString hyperlink);

signals:
    void load(QString hyperlink, QString sender);

private slots:
    void handleHyperlink();

    void clickedPlan();
    void clickedDsrb();
    void clickedData();
    void clickedAnalysis();
    void clickedDraft();
    void clickedSubmit();
    void clickedAccept();

    void handleAdd();

    void haveInfo(QString state, QString details);
    void statusChanged();

    void handleEdit();
    void editReceived(QString titleString,
                  QString descString, QString collabString,
                   QString folderString, QString statusString,
                   QString initiatedString, QString teamLead);
private:
    QFont boldFont;
    QFont nonBold;
    QLabel *_titleLabel;
    QLabel *_descLabel;
    QVector <QLabel*> _projectWidgets;
    QHBoxLayout *_projectProgress;
    QLabel *_plan;
    QLabel *_dsrb;
    QLabel *_data;
    QLabel *_analysis;
    QLabel *_submit;
    QLabel *_draft;
    QLabel *_accept;
    QHBoxLayout *_dateLayout;
    QLabel *_dateStatic;
    QLabel *_dateLabel;
    QHBoxLayout *_leadLayout;
    QLabel *_leadStatic;
    QLabel *_leadLabel;
    QHBoxLayout *_colLayout;
    QLabel *_colStatic;
    QLabel *_colLabel;
    QPixmap _folderLogo;
    QLabel *_folderLabel;
    QLabel *_viewLabel;
    QLabel *_statusLabel;
    QComboBox *_statusBox;

    QVBoxLayout *_folderLayout;
    QVBoxLayout *_instanceLayout;
    QHBoxLayout *_statusLayout;

    QString _hyperlink;
    QString _fileName;

    QString _title;
    QString _desc;
    QString _initDate;
    QString _teamLead;
    QString _colla;
    QString _state;
    QString _folderLink;
    QString _planningDetails;
    QString _dsrbDetails;
    QString _dataDetails;
    QString _analysisDetails;
    QString _submissionDetails;
    QString _draftingDetails;
    QString _acceptedDetails;

    QString _userRole;

private:
    static const QString STATES_CONST[7];
    static const QString PROGRESS_CONST[3];
    static const char* FOLDER_PATH;
    static const QColor PAST;
    static const QColor ONGOING;
    static const QColor PENDING;
    static const QColor MY_COLOR;
    static const int PROJECT_PROGRESS_HEIGHT;

    static const QString ICON_ADD;
    static const QString ICON_EDIT;
};

#endif // RESEARCHINSTANCE_H
