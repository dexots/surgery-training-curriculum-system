#ifndef RESOURCEWIDGET_H
#define RESOURCEWIDGET_H

#include <iostream>

#include <QLabel>
#include <QLayout>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QWidget>
#include <QVector>
#include <QFile>
#include <QIODevice>
#include <QTextStream>
#include <QStringList>

#include "gui/catebox.h"
#include "gui/subcate.h"
#include "gui/catetable.h"
#include "gui/clickablewidget.h"
#include "gui/hyperlinkwidget.h"
#include "objects/userdetails.h"

class QLabel;
class QLayout;
class QVBoxLayout;
class QHBoxLayout;
class QWidget;
class QFile;
class QIODevice;
class QTextStream;
class QStringList;

class CateBox;
class SubCate;
class CateTable;
class ClickableWidget;
class HyperlinkWidget;
class UserDetails;

namespace Gui {
    class ResourceWidget;
}

class ResourceWidget:public QWidget {
    Q_OBJECT

public:
    explicit ResourceWidget();
    ~ResourceWidget();

public:
    QString getClassName();

    static const QString TEXTBOOK_LINK;
    static const QString JOURNAL_LINK;
    static const QString QUESTION_LINK;

    static const QString TYPE_TEXTBOOK;
    static const QString TYPE_JOURNAL;
    static const QString TYPE_QUESTION;

private:
    void createLayout();
    void colorWidget(QColor color, QWidget *widget);
    void generateTextbooks();
    void generateJournals();
    void generateQuestionBanks();
    void getFileContent(QString fileLink);

private:
    QVBoxLayout *_widgetLayout;

signals:
    void backToHome();
    void load(QString hyperlink, QString sender);

private slots:
    void handleHomeSignal();
    void handleHyperlink(QString hyperlink);

private:
    QVector <QString> _texts;
    QVector <QString> _hyperlinks;

    CateTable *_resourcesTable;

    HyperlinkWidget *_textbookWidget;
    HyperlinkWidget *_journalWidget;
    HyperlinkWidget *_questionWidget;

// Private constants
    static const QString DELIMITER;
    static const QString CLASS_NAME;

};


#endif // RESOURCELAYOUT_H
