#ifndef CATEROS_H
#define CATEROS_H

#include "catebox.h"


namespace Gui{
    class CateRos;
}

class CateRos : public CateBox {

public:
    explicit CateRos();

private:
    void initializeCateRos();
};

#endif // CATEROS_H
