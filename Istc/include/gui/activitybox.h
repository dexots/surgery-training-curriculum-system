#ifndef ACTIVITYBOX_H
#define ACTIVITYBOX_H

#include <QDialog>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QWidget>
#include <QFont>
#include <QPainter>
#include <QBrush>
#include <QRect>
#include <QPushButton>
#include <QColor>
#include <QPalette>
#include <QTextEdit>
#include <QString>
#include <QDateTime>
#include <QFrame>
#include <QSizePolicy>
#include <QVector>
#include <QComboBox>
#include <QDateEdit>
#include <QTimeEdit>
#include <QTextStream>
#include <QCheckBox>
#include <QDebug>
#include <QUrl>

#include "gui/inputbox.h"
#include "gui/mycalendarwidget.h"
#include "objects/userdetails.h"

class QDialog;
class QHBoxLayout;
class QVBoxLayout;
class QLabel;
class QWidget;
class QFont;
class QPainter;
class QBrush;
class QRect;
class QPushButton;
class QColor;
class QPalette;
class QTextEdit;
class QString;
class QDateTime;
class QFrame;
class QSizePolicy;
class QDateEdit;
class QTimeEdit;

class QFile;
class QDebug;
class QCheckBox;
class QUrl;

class InputBox;
class UserDetails;

namespace gui {
    class ActivityBox;
}

class ActivityBox:public QDialog{
    Q_OBJECT

public:
    ActivityBox();
    ActivityBox(QString title, QString trainer, QDate date,
                QDateTime registrationDeadline, QTime startTime,
                QTime endTime, int maxVacancy, QString location,
                QString description, bool isImportant, QString eventLink = "");
    ~ActivityBox();

protected:
    void paintEvent(QPaintEvent *event);

private:
    void addToBoxLayout(QHBoxLayout *toBeAddedLayout);
    void addToBoxLayout(QWidget *toBeAddedWidget);
    void changeColor(QColor font, QColor back, QWidget* widget);
    void initializeBoxLayout();
    void setActivityBoxAttribute();
    void setButtonStyleSheet(QPushButton *button);
    void setUpActivityDetailsLabel();
    void setUpAddAndCancelButton();
    void setUpDateAndRegistrationDeadlineInput(QDate activityDate = QDate::currentDate().addDays(1),
                                               QDateTime registrationDeadline = QDateTime::currentDateTime());
    void setUpDescriptionInput(const QString &description = "");
    void setUpErrorLabel();
    void setUpEventLinkInput(const QString &eventLink = "");
    void setUpIsImportantCheckbox(bool isImportant = false);
    void setUpMaxVacancyAndLocationInput(int maxVacancy = 0, const QString &location = "");
    void setUpStartAndEndTimeInput(QTime startTime = QTime::currentTime(),
                                   QTime endTime = QTime::currentTime());
    void setUpTitleAndTrainerInput(const QString &title = "", const QString &trainer = "");
    void setUpUpdateAndCancelButton();
    void showMessageIfDeadlineLogicError();
    void showMessageIfEmptyInput(QString title, QString errorMessage);
    void showMessageIfInvalidEventLink();
    void showMessageIfTimeLogicError();

    QPushButton* setUpAddButton();
    QPushButton* setUpCancelButton();
    QPushButton* setUpUpdateButton();


private slots:
    void handleAddOrUpdate();
    void handleCancel();
    void handleDescription(QString inputString);
    void handleEventLink(QString inputString);
    void handleLocation(QString inputString);
    void handleMaxVacancy(QString inputString);
    void handleTitle(QString inputString);
    void handleTrainer(QString inputString);

signals:
    void passInfo(QVector<QString> info, QDate date, QDateTime registrationDeadline,
                  QVector<QTime> time, int maxVacancy, bool isImportant);

private:
    QFont _boldFont;

    QVBoxLayout *_boxLayout;

    InputBox *_descriptionInput;
    InputBox *_eventLinkInput;
    InputBox *_locationInput;
    InputBox *_maxVacancyInput;
    InputBox *_titleInput;
    InputBox *_trainerInput;

    QDateTimeEdit *_registrationDeadlineInput;

    QDateEdit *_dateInput;

    QTimeEdit *_startTimeInput;
    QTimeEdit *_endTimeInput;

    QCheckBox *_isImportantCheckBox;

    QLabel *_errorLabel;

    int _maxVacancy;
    bool isImportant;

private:
    static const QString COLOR_BUTTONBACKGROUND;
    static const QString COLOR_BUTTONBORDER;
    static const QString COLOR_BUTTONFONT;
    static const QString FONT_BUTTON;

    static const QString FORMAT_DATE;
    static const QString FORMAT_DATETIME;
    static const QString FORMAT_TIME;

    static const QString LABEL_ACTIVITYDETAILS;
    static const QString LABEL_ADDBUTTON;
    static const QString LABEL_CANCELBUTTON;
    static const QString LABEL_DATE;
    static const QString LABEL_DESCRIPTION;
    static const QString LABEL_ENDTIME;
    static const QString LABEL_EVENTLINK;
    static const QString LABEL_IMPORTANT;
    static const QString LABEL_LOCATION;
    static const QString LABEL_MAXVACANCY;
    static const QString LABEL_REGISTRATIONDEADLINE;
    static const QString LABEL_STARTTIME;
    static const QString LABEL_TITLE;
    static const QString LABEL_TRAINER;
    static const QString LABEL_UPDATEBUTTON;

    static const QString MESSAGE_DEADLINELOGICERROR;
    static const QString MESSAGE_INVALIDEVENTLINK;
    static const QString MESSAGE_MISSINGTITLE;
    static const QString MESSAGE_MISSINGTRAINER;
    static const QString MESSAGE_MISSINGMAXVACANCY;
    static const QString MESSAGE_MISSINGLOCATION;
    static const QString MESSAGE_MISSINGDESCRIPTION;
    static const QString MESSAGE_TIMELOGICERROR;

    static const QString PADDING_BUTTON;

    static const QString PLACEHOLDER_DESCRIPTION;
    static const QString PLACEHOLDER_EVENTLINK;
    static const QString PLACEHOLDER_LOCATION;
    static const QString PLACEHOLDER_MAXVACANCY;
    static const QString PLACEHOLDER_TITLE;
    static const QString PLACEHOLDER_TRAINER;

    static const QString RADIUS_BUTTONBORDER;

    static const QString STRING_COLON;
    static const QString STRING_EMPTY;
    static const QString STRING_NEWLINE;
    static const QString STRING_SPACE;

    static const QString STYLE_BUTTONBORDER;
    static const QString WIDTH_BUTTON;
    static const QString WIDTH_BUTTONBORDER;

    static const int HEIGHT_NORMALINPUTBOX;
    static const int HEIGHT_LARGEINPUTBOX;

    static const QTime DEFAULTTIME_DEADLINE;
};

#endif // ACTIVITYBOX_H
