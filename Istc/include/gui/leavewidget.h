#ifndef LEAVEWIDGET_H
#define LEAVEWIDGET_H

#include <QLabel>
#include <QString>
#include <QPushButton>
#include <QDateEdit>
#include <QVector>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QColor>
#include <QDebug>

#include "gui/mycalendarwidget.h"
#include "gui/progressbar.h"
#include "gui/tablewidget.h"
#include "gui/generalpopupbox.h"
#include "model/rostermodel.h"
#include "objects/leaveobj.h"

class QLabel;
class QString;
class QPushButton;
class QDateEdit;
class QVBoxLayout;
class QHBoxLayout;

class MyCalendarWidget;
class ProgressBar;
class TableWidget;
class RosterModel;
class LeaveObj;

namespace Gui {
    class LeaveWidget;
}

class LeaveWidget:public QWidget {
    Q_OBJECT

public:
    explicit LeaveWidget();
    ~LeaveWidget();

private:
    void setUpLeaveLayout();
    void setUpLeaveApplicationLayout();
    void setUpLeaveHistoryLayout();
    void setUpRemaingDayBar();
    void setUpDatePickLayout();
    void setUpDummyWidget();
    void addRowsToTable();

    bool sendToServer(LeaveObj leave);

    void setWidgetStyleSheet();
    void colorWidget(QColor color, QWidget *widget);

public slots:
    void updateTable();

private slots:
    void updateTotalDays();
    void sendLeaveApplication();
    void resetLeaveApplication();
    void handleTableResized(int minHeightChanged);

private:
    QVBoxLayout *_leaveLayout;
    QVBoxLayout *_leaveApplicationLayout;

    QLabel *_daysLeftLabel;
    QLabel *_totalDaysLabel;

    ProgressBar *_remainingDayBar;

    QDateEdit *_startDate;
    QDateEdit *_endDate;

    int _daysLeft;

    TableWidget *_leaveHistoryTable;

    int _currentIndex;

    RosterModel *_rosterModel;

    QWidget *_dummyWidget;

//constants
private:
    static const QString BORDER_DATEEDIT;
    static const QString COLOR_APPLYBUTTON;
    static const QString COLOR_BUTTONTEXT;
    static const QString COLOR_DATEEDITBACKGROUND;
    static const QString COLOR_RESETBUTTON;
    static const QString HEIGHT_BUTTON;
    static const QString LEFTMARGIN_BUTTON;
    static const QString MESSAGE_DAYSLEFT;
    static const QString RADIUS_BUTTONBORDER;
    static const QString SIZE_FONT;
    static const QString TAG_ENDDATE;
    static const QString TAG_LEAVEAPPLICATION;
    static const QString TAG_LEAVEHISTORY;
    static const QString TAG_STARTDATE;
    static const QString TAG_TOTALDAYS;
    static const QString WIDTH_BUTTON;

    static const QVector<int> WIDTH_COLUMN;

    static const QVector<QString> HEADER_TABLE;

    static const int DAY_LEAVE;
    static const int NUMBER_COLUMN;

    static const QColor COLOR_CONTENTBACKGROUND;
    static const QColor COLOR_CONTENTFONT;
    static const QColor COLOR_HEADERBACKGROUND;
    static const QColor COLOR_HEADERTEXT;
    static const QColor COLOR_BACK;
};

#endif // LEAVEWIDGET_H
