#ifndef MILESTONEBOX_H
#define MILESTONEBOX_H

#include "gui/popupboxincludes.h"

namespace gui {
    class MilestoneBox;
}

class MilestoneBox:public QDialog {
    Q_OBJECT

public:
    MilestoneBox();
    ~MilestoneBox();

protected:
    void paintEvent(QPaintEvent *event);

private:
    void initializeVariable();
    void setUpTitleLayout();
    void setUpMaxLayout();
    void setUpCompletedLayout();
    void setUpErrorLabel();
    void setUpAddAndCancelButton();
    void setBoxAttribute();
    void changeColor(QColor font, QColor back, QWidget* widget);
    void setButtonStyleSheet(QPushButton *button);

private slots:
    void handleAdd();
    void handleCancel();
    void handleTitle(QString inputString);
    void handleCompletedNumber(QString inputString);
    void handleRequiredNumber(QString inputString);

signals:
    void passInfo(QString title, int completedNumber, int requiredNumber);

private:
    InputBox *_inputTitle;
    InputBox *_inputMax;
    InputBox *_inputCompleted;

    QLabel *_errorLabel;
    QLabel *_titleLabel;

    QPushButton *_addButton;
    QPushButton *_cancelButton;

    QString _titleString;
    QString _maxString;
    QString _completedString;

    QFont _bold;

    QHBoxLayout *_titleLayout;
    QHBoxLayout *_maxLayout;
    QHBoxLayout *_completedLayout;
    QHBoxLayout *_buttonLayout;
    QVBoxLayout *_boxLayout;

// private constants
private:
    static const QString COLOR_BUTTONBACKGROUND;
    static const QString STYLE_BUTTONBORDER;
    static const QString WIDTH_BUTTONBORDER;
    static const QString RADIUS_BUTTONBORDER;
    static const QString COLOR_BUTTONBORDER;
    static const QString FONT_BUTTON;
    static const QString COLOR_BUTTONFONT;
    static const QString WIDTH_BUTTON;
    static const QString PADDING_BUTTON;
    static const QString MESSAGE_MISSINGTITLE;
    static const QString MESSAGE_NUMBERLOGICERROR;
    static const int WIDTH_MINIMUM;
    static const int HEIGHT_MINIMUM;

    static const QString QSTRING_EMPTY;

};


#endif // MILESTONEBOX_H
