#ifndef EXAMINATIONWIDGET_H
#define EXAMINATIONWIDGET_H

#include <QVBoxLayout>
#include <QLabel>
#include <QVector>
#include <QString>
#include <QColor>

#include "objects/exam.h"
#include "gui/tablewidget.h"
#include "model/trainingmodel.h"

class QVBoxLayout;
class QLabel;
class QString;
class QColor;

class Exam;
class TableWidget;
class TrainingModel;

namespace Gui {
    class ExaminationWidget;
}

class ExaminationWidget:public QWidget {
    Q_OBJECT

public:
    explicit ExaminationWidget();
    ~ExaminationWidget();


private:
    void setUpExaminationLayout();
    void addRowsToTable();

//class variables
private:
    QVBoxLayout *_examinationLayout;
    TableWidget *_examinationTable;
    QWidget *_dummyWidget;

//constants
private:
    static const int HEIGHT_ROW;
    static const int NUMBER_COLUMN;
    static const QVector<int> WIDTH_COLUMN;

    static const QVector<QString> HEADER_TABLE;

    static const QColor COLOR_CONTENTBACKGROUND;
    static const QColor COLOR_CONTENTFONT;
    static const QColor COLOR_HEADERBACKGROUND;
    static const QColor COLOR_HEADERTEXT;

    static const QString MESSAGE_NOTICE;

public slots:
    void updateTable();
};

#endif // EXAMINATIONWIDGET_H
