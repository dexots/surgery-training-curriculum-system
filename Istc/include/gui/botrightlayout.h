#ifndef BOTRIGHTLAYOUT_H
#define BOTRIGHTLAYOUT_H

#include <cassert> //assertion package, add in "#define NDEBUG" to disable it
#include "botrightmain.h"

class BotRightMain;

namespace Gui {
    class BotRightLayout;
}

//BotRightLayout is a layout on the bottom right of the GUI
class BotRightLayout: public QVBoxLayout {
private:
    struct PAGE {
        enum Type {
            MAIN,
            PRO,
            TRAIN,
            ROS,
            LOG,
            RESEARCH,
            RESOURCES
        };
        Type state;
    };

public:
    explicit BotRightLayout();
    ~BotRightLayout();

private:
    void initializeLayout();
    void clearGarbage();
    void createLayout(PAGE reqState);
    void deleteLayout(PAGE reqState);

private:
    BotRightMain *_mainLayout;
    PAGE _cur;
};

#endif // BOTRIGHTLAYOUT_H
