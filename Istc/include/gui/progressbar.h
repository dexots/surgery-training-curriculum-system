#ifndef PROGRESSBAR_H
#define PROGRESSBAR_H

#include <iostream>

#include <QProgressBar>
#include <QPalette>
#include <math.h>

namespace gui {
    class ProgressBar;
}

class ProgressBar:public QProgressBar {
    Q_OBJECT

public:
    ProgressBar();
    ~ProgressBar();

public:
    void setBadToGood(bool isBadToGood);
    void setValue(int value);

private:
    void initializeProgressBar();
    void changeProgressBarColor(int value);
    void colorProgressBar(QString style);
    void setTextInBar(int value);

private:
    bool _isBadToGood;
    QString _styleBad;
    QString _styleOk;
    QString _styleGood;

signals:
    //bool value to trigger slots with bool
    void reachMaxValue(bool isTrue);
    void reachMinValue(bool isTrue);
    void isUpdated();

private slots:
    void addValue();
    void minusValue();

};

#endif // PROGRESSBAR_H
