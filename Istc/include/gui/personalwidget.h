#ifndef PERSONALWIDGET_H
#define PERSONALWIDGET_H

#include <iostream>
#include <cassert>

#include <QLabel>
#include <QPixmap>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QString>
#include <QFrame>
#include <QTextStream>

#include "gui/clickablewidget.h"
#include "gui/errorbox.h"
#include "gui/generalpopupbox.h"
#include "gui/profilebox.h"

class QLabel;
class QPixmap;
class QVBoxLayout;
class QHBoxLayout;
class QString;
class QFrame;

class ClickableWidget;
class ErrorBox;
class GeneralPopupBox;
class ProfileBox;

namespace Gui {
    class PersonalWidget;
}

class PersonalWidget:public QWidget{
    Q_OBJECT

public:
    explicit PersonalWidget(QString userName, QString userCurrentRotation, QString userStudentID,
                            QString userAddress, QString userHomeContact, QString userMobileContact,
                            QString userSchoolEmail, QString userPersonalEmail);
    explicit PersonalWidget();
    ~PersonalWidget();

    void setUserName(QString userName);
    void setUserStudentID(QString userStudentID);
    void setUserAddress(QString userAddress);
    void setUserCurrentRotation(QString userCurrentRotation);
    void setUserHomeContact(QString userHomeContact);
    void setUserMobileContact(QString userMobileContact);
    void setUserSchoolEmail(QString userSchoolEmail);
    void setUserPersonalEmail(QString userPersonalEmail);

private slots:
   void editPersonalEmail();
   void editSchoolEmail();
   void editUserAddress();
   void editHomeContact();
   void editMobileContact();

   void haveInfo(QString type, QString info);

private:
    void initialiseVariables();
    void populateData(QString data);
    void setUpPersonalLayout();
    void setUpUpperLayout();
    void setUpLowerLayout();

    void setPersonalInfo();
    void setUpPersonalEmail();
    void setUpSchoolEmail();
    void setUpUserAddress();
    void setUpHomeContact();
    void setUpMobileContact();

    void readSettingsFile();
    void saveSettingsFile();
    void updateInfo(QString type, QString info);

    void showErrorBox();
    void showSuccessMsg();
    QString formatData();

private:
    QVBoxLayout *_personalLayout;
    ProfileBox *_profileBox;

    QPixmap _userProfilePicture;
    QString _userName;
    QString _userStudentID;
    QString _userAddress;
    QString _userHomeContact;
    QString _userMobileContact;
    QString _userSchoolEmail;
    QString _userPersonalEmail;
    QString _userCurrentRotation;

    QLabel *_personalEmailLbl;
    QLabel *_schoolEmailLbl;
    QLabel *_userAddLbl;
    QLabel *_homeContactLbl;
    QLabel *_mobileContactLbl;

private:
    static const QString ICON_ADDRESS;
    static const QString ICON_EDIT;
    static const QString ICON_HOMECONTACT;
    static const QString ICON_MOBILECONTACT;
    static const QString ICON_PERSONALEMAIL;
    static const QString ICON_STUDENTID;
    static const QString ICON_SCHOOLEMAIL;
    static const QString LEFTPADDING_LABEL;
    static const QString SIZE_FONT;
    static const QString TAG_BOLD;
    static const QString TAG_CURRENTROTATION;
    static const QString PICTURE_PROFILE;
    static const QString PROFILE_FILE;

    static const QString QSTRING_EMPTY;
    static const string STRING_EMPTY;
};

#endif // PERSONALWIDGET_H
