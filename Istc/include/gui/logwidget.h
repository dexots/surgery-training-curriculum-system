#ifndef LOGWIDGET_H
#define LOGWIDGET_H

#include <QLabel>
#include <QLayout>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QWidget>
#include <QVector>
#include <QDebug>

#include "gui/catebox.h"
#include "gui/subcate.h"
#include "gui/catetable.h"
#include "gui/workingwidget.h"
#include "gui/procedurecodewidget.h"

class QLabel;
class QLayout;
class QVBoxLayout;
class QHBoxLayout;
class QWidget;

class CateBox;
class SubCate;
class CateTable;
class WorkingWidget;
class CodeWidget;

namespace Gui {
    class LogWidget;
}

class LogWidget:public QWidget {
    Q_OBJECT

public:
    explicit LogWidget();
    ~LogWidget();

private:
    void createLayout();
    void colorWidget(QColor color, QWidget *widget);

private:
    QVBoxLayout *_widgetLayout;

public slots:
    void updateAllTables();

private slots:
    void handleHomeSignal();

signals:
    void backToHome();
    void updateProcedureCodeTable();
    void updateWorkingHourTable();
};


#endif // LOGWIDGET_H
