#ifndef CATEBOX_H
#define CATEBOX_H

#include <iostream>

#include <QWidget>
#include <QPalette>
#include <QColor>
#include <QLabel>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPixmap>
#include <QString>
#include <QFrame>
#include <QMouseEvent>
#include <QEvent>
#include <QTextOption>

class QWidget;
class QPalette;
class QColor;
class QLabel;
class QVBoxLayout;
class QHBoxLayout;
class QPixmap;
class QString;
class QFrame;
class QFont;
class QMouseEvent;
class QEvent;
class QTextOption;

namespace Gui {
    class CateBox;
}

class CateBox:public QWidget {
    Q_OBJECT

public:
    explicit CateBox(QString name, const char* icon, QColor color);
    ~CateBox();

protected:
    explicit CateBox();

protected:
    void preSetupCateBox(QString name, const char* icon, QColor color);
    void setInputAs(QString originalInput);
    virtual void mousePressEvent(QMouseEvent * event);
    virtual bool event(QEvent *event);
    void handleTouch();
    void colorWidget(QColor background);
    QLayout *setupCateBoxLayout();
    void createMyself();

private:
    void initializeClass();
    void setCategoryName(QString nameOfCategory);
    void setPixmap(const char* nameOfIcon);
    void setColor(QColor colorOfWidget);
    void setupInputArea();
    void setupButton();
    void setupIcon();
    void initializeWidget();
    QLayout *createWidgetLayout();


signals:
   void isClicked();

private:
    QHBoxLayout *_hLayout;
    QVBoxLayout *_vLayout;
    QLabel *_button;
    QLabel *_buttonIcon;
    QLabel *_inputArea;
    QString _nameOfCategory;
    QPixmap _pixmap;
    QColor _colorOfWidget;

 protected:
    static const char* FONT_NAME;
    static const int FONT_BUTTON_SIZE;
    static const int FONT_INPUT_AREA_SIZE;

};



#endif // CATEBOX_H
