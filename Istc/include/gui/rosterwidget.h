#ifndef ROSTERWIDGET_H
#define ROSTERWIDGET_H

#include <QLabel>
#include <QLayout>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QWidget>
#include <QVector>

#include "gui/catebox.h"
#include "gui/subcate.h"
#include "gui/catetable.h"
#include "gui/yourrosterwidget.h"
#include "gui/leavewidget.h"
#include "gui/rosreq.h"
#include "gui/rostermonsterwidget.h"

#include "objects/userdetails.h"

class QLabel;
class QLayout;
class QVBoxLayout;
class QHBoxLayout;
class QWidget;
class CateBox;
class SubCate;
class CateTable;
class YourRosterWidget;
class LeaveWidget;
class RosReq;
class RosterMonsterWidget;

namespace Gui {
    class RosterWidget;
}

class RosterWidget:public QWidget {
    Q_OBJECT

public:
    explicit RosterWidget();
    ~RosterWidget();

private:
    void createLayout();
    void colorWidget(QColor color, QWidget *widget);
    bool isRosterMonster();

private:
    QVBoxLayout *_widgetLayout;
    QString _userRole;

public slots:
    void updateAllTables();

private slots:
    void handleHomeSignal();

signals:
    void backToHome();
    void updateYourRosterTable();
    void updateLeaveTable();
    void updateRosterMonsterTable();
    void updateRosterRequestTable();
};


#endif // ROSTERWIDGET_H
