#ifndef YOURROSTERWIDGET_H
#define YOURROSTERWIDGET_H

#include <iostream>

#include <QLabel>
#include <QPixmap>
#include <QDate>
#include <QString>
#include <QWidget>
#include <QVector>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QSizePolicy>

#include "gui/tablewidget.h"
#include "gui/clickablewidget.h"
#include "gui/addrosterbox.h"

#include "objects/rosterassignobj.h"
#include "objects/userdetails.h"

#include "model/rostermodel.h"

class QLabel;
class QPixmap;
class QDate;
class QString;
class QWidget;

class AddRosterBox;
class TableWidget;
class QHBoxLayout;
class ClickableWidget;

class RosterAssignObj;
class UserDetails;
class RosterModel;

namespace gui {
    class YourRosterWidget;
}

class YourRosterWidget:public QWidget {
    Q_OBJECT

public:
    YourRosterWidget();
    ~YourRosterWidget();

private:
    void initializeClass();
    void clearGarbage();
    bool getIsRosterMonster();

    void getEarliestAndLatestDate();
    void setupTable();
    void initializeTableHeader();
    void setupMonthBar();
    void initializeMonthBarHeader();
    void setHeaderText();

    void getContent();
    void addContents();
    void addContentRow(QVector <QString> contentRow, int rowId);

    void initializeMonthBarPrev();
    void initializeMonthBarNext();
    void initializeMonthBarLayout();
    void initializeMonthBar();
    void initializeAddLayout();
    void initializeLayout();
    void initializeDummyWidget();

    void createYourself();
    void colorWidget(QColor font, QColor background, QWidget *widget);

    void restoreDummyWidget();

protected:
    void resizeEvent(QResizeEvent * event);

public slots:
    void updateTable();

private slots:
    void handlePrev();
    void handleNext();
    void addRoster();

    void handleTableResized(int minHeight);

private:
    QVector< QVector <QString> > _contents;
    QWidget *_monthBar;
    QLabel *_monthBarHeader;

    ClickableWidget *_monthBarPrev;
    ClickableWidget *_monthBarNext;
    QHBoxLayout *_monthBarLayout;
    TableWidget *_table;

    QDate _latestDate;
    QDate _earliestDate;
    QDate _curDate;
    QDate _todayDate;

    QVBoxLayout *_rosterLayout;
    QHBoxLayout *_addLayout;
    RosterModel *_rosterModel;

    QWidget *_dummyWidget;
    int _dummyMaxHeight;

    bool _isRosterMonster;

private:
    static const QString COL0;
    static const QString COL1;

    static const char *ICON_LEFT;
    static const char *ICON_RIGHT;
    static const char *ICON_ADD;

    static const bool HAS_HEADER;
    static const int NO_OF_COL;
    static const int COL0_WIDTH;
    static const int COL1_WIDTH;

    static const QColor MONTH_BAR_BACK;
    static const QColor MONTH_BAR_FONT;
    static const QColor HEADER_FONT;
    static const QColor HEADER_BACK;
    static const QColor CONT_BACK;
    static const QColor CONT_FONT;
    static const QColor MY_OWN_COLOR;

    static const QString LABEL_ADDROSTER;

};



#endif // YOURROSTERWIDGET_H
