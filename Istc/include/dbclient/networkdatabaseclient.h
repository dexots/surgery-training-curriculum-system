#ifndef NETWORKDATABASECLIENT_H
#define NETWORKDATABASECLIENT_H

#include <iostream>
#include <cassert>
#include <string>
#include <iterator>

#include <QObject>
#include <QString>
#include <QDateTime>
#include <QJsonObject>
#include <QJsonDocument>
#include <QFile>
#include <QDebug>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QByteArray>
#include <QSslCertificate>
#include <QSslSocket>
#include <QSslError>
#include <QUrl>
#include <QNetworkProxy>

#include "objects/serverrequestobj.h"
#include "objects/userdetails.h"


class QObject;
class QString;

class ServerRequestObj;


namespace DatabaseClient {
    class NetworkDatabaseClient;
}

class NetworkDatabaseClient:public QObject {
    Q_OBJECT

private:
  enum FileFormat {
      JSON,
      BINARY
  };

public:
    static NetworkDatabaseClient* getObject();
    static void deleteObject();

public:
    NetworkDatabaseClient();
    ~NetworkDatabaseClient();

public:
    bool requestSyncToServer(ServerRequestObj serverObj);
    void getUserDetails();

    QJsonObject loadJsonFile(QString filename);

private:
    QString encryptDecrypt(QString toEncrypt, std::string key);

    QString generatePassword();
    QString generateFilename(NetworkDatabaseClient::FileFormat fileFormat);

    void initiateClass();


    bool saveJsonFile(QJsonObject jsonObject,
                      QString filename,
                      NetworkDatabaseClient::FileFormat fileFormat);

    bool sendFileToServer(QString filename,
                          QString siteUrl,
                          QString host);
    bool setupCertificate();

private slots:
    void handleReplyAfterSendingFile();
    void handleReplyError(QNetworkReply::NetworkError error);
    void showHandshakeSuccessful();
    void handleSslErrors(QList<QSslError> errors);

signals:
    void fileIsReady(QString fileName);
    void syncFailed();

private:
    static NetworkDatabaseClient *_dbClient;

private:
    QString _matricNo;
    QNetworkReply* _reply;

private:
    static const std::string KEY2;
    static const QString PASSWORD;
    static const QString FORMAT_DATE;
    static const QString EXT_JSON;
    static const QString EXT_BINARY;
    static const QString SERVER_CERTIFICATE;
    static const int SINGAPORE_OFFSET;


};





#endif // NETWORKDATABASECLIENT_H
