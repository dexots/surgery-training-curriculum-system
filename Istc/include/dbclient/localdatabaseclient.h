#ifndef LOCALDATABASECLIENT_H
#define LOCALDATABASECLIENT_H

#include <iostream>
#include <cassert>

#include <QWidget>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QSqlError>
#include <QString>
#include <QFile>
#include <QDir>
#include <QIODevice>
#include <QVector>
#include <QVariant>
#include <QDate>
#include <QTime>
#include <QDateTime>
#include <QHash>
#include <QDebug>

#include "dbclient/networkdatabaseclient.h"
#include "objects/eventitem.h"
#include "objects/rosterassignobj.h"
#include "objects/rosterrequestobj.h"
#include "objects/workinghoursobject.h"
#include "objects/leaveobj.h"
#include "objects/assessmentobj.h"
#include "objects/proceduralcode.h"
#include "objects/exam.h"
#include "objects/form.h"
#include "objects/userdetails.h"
#include "objects/notes.h"
#include "objects/serverrequestobj.h"

class QWidget;
class QSqlDatabase;
class QSqlQuery;
class QString;
class QFile;
class QDir;
class QIODevice;

namespace DatabaseClient {
    class LocalDatabaseClient;
}

class LocalDatabaseClient:public QWidget {
    Q_OBJECT

public:
    static LocalDatabaseClient* getObject();
    static void deleteObject();

private:
    explicit LocalDatabaseClient();
    ~LocalDatabaseClient();

public:
    void getUserDetails();

    bool isCurrentUserExist();
    bool addCurrentUser();
    bool deleteCurrentUser(QString matricNo);
    bool updateCurrentUser();


    QVector <WorkingHoursObject> *getWorkingHours(QDate date);
    QVector <ProceduralCode*> *getProcedures();
    QVector <Exam> *getExamInfo();
    QVector <Form*> *getOnlineForms();
    QVector <Form*> *getSoftcopyForms();
    QVector <QString> *getPatientData(QString patientId);//id, name, dob
    QVector <EventItem*> *getActivitiesInfo();
    QVector <RosterAssignObj> *getYourRoster();
    QVector <RosterRequestObj*> *getBlockOutReq();
    QVector <LeaveObj> *getLeaveApplications();
    QVector <RosterRequestObj*> *getTeamRequest();
    QVector <AssessmentObj> *getAssessments();
    QVector <Notes*> *getNotes();

    bool deleteWorkingHours (QString workingHoursId);
    bool updateWorkingHours (WorkingHoursObject oldObj);
    bool addWorkingHours(WorkingHoursObject newObj);

    bool updateProceduralCodes(ProceduralCode* code);
    bool addNewProceduralCodes(ProceduralCode* code);

    bool saveBlockOutReq (RosterRequestObj* blockOutReq);
    bool updateBlockOutReq(RosterRequestObj *blockOutReq);

    bool saveRosterAssignment (RosterAssignObj assignObj);

    bool addNewActivity(EventItem* event);
    bool updateActivity(EventItem* activity);
    bool updateActivityCurVacancy(int curVacancy, QString activityId);

    bool updateFormStatus(Form edittedForm);

    bool addLeaveApplication(LeaveObj leaveObj);
    bool updateLeaveApplication(LeaveObj leaveObj);

    bool addAssessment(AssessmentObj assessmentObj);

    bool addNote(Notes* notesObj);
    bool updateNote(Notes* notesObj);

    bool isValidUser(QString matricNo, QString role);

    bool syncAllTables();

    QString generateActivityId();
    QString generateLeaveApplicationId();
    QString generateAssessmentId();
    QString generateNotesId();
    QString generateWorkingHoursId();
    QString generateProcedureId();
    QString generateBlockRequestId();
    QString generateLeaveId();

    QString getUserName(QString matricNo);

private:
    bool syncTable(ServerRequestObj& obj, QString tableName);

    QVector <AssessmentObj> *getTrainerAssessments();
    QVector <AssessmentObj> *getTraineeAssessments();
    QVector <QString> *getGraders(QString assessmentId);

    QVector <Form*> *getForm(QString type);
    void initiateClass();
    void setMatricNo(QString matricNo);
    void setUserRole(QString userRole);
    void setIsRosterMonster(bool isRosterMonster);
    bool createConnection();
    bool createFile(QString name);
    bool removeFile(QString name);
    bool createTables();
    bool startupTables();
    bool insertIntoActivities(QString activityId,
                              QString title,
                              QString registrationDeadlineTime,
                              QString registrationDeadlineDate,
                              QString eventDate,
                              QString startTime,
                              QString endTime,
                              int maxVacancy,
                              int curVacancy,
                              QString location,
                              QString description,
                              QString eventLink,
                              int important);
    bool insertIntoUserActivities(QString status,
                                  QString role,
                                  QString userId,
                                  QString activityId);
    bool insertIntoAssessment(QString assessmentId,
                              QString traineeId,
                              QString role,
                              QString title,
                              QString date,
                              QString grade,
                              QString feedback);
    bool insertIntoTrainerAssessment(QString trainerId,
                                     QString role,
                                     QString assessmentId);
    bool insertIntoUser(QString matricNo,
                        QString name,
                        QString role,
                        int rosterMonster,
                        QString personalEmail,
                        QString nuhEmail,
                        QString address,
                        QString homePhone,
                        QString handPhone,
                        QString rotation);

    //todo: convert all insert into to this:
    bool insertIntoAnyTable(QString tableName, QVariantMap data);


    bool isMatricNoUnique(QString matricNo);



    QVector <QString> getAllTrainees();

    int getTableSize(QString tableName);

    QString generateId(QString tableName);

    bool clearAllTables();

    bool clearTable(QString tableName);

    void initializeHashTables();

    bool isValidTime(QString time);
    bool isValidDate(QString date);
    void readReplyFile(QString filename);
    bool isJsonValueExist(QJsonValue jsonValue);
    bool isJsonValueExist(QJsonObject json, QString param);
    bool retrieveJsonData(QJsonObject jsonFile,
                          QString param,
                          QVector<QString> names);
    bool extractDataFrom(QString table, QJsonObject data);
    bool extractDataFromActivity(QJsonObject data);
    bool extractDataFromUserActivity(QJsonObject data);
    bool extractDataFromUser(QJsonObject data);
    bool extractDataFromAnyTable(QString tableName, QJsonObject data);


    bool updateUserActivityStatus(EventItem* activity);
    bool updateActivityTableOnly(QString activityId,
                                 QString title,
                                 QString registrationDeadlineTime,
                                 QString registrationDeadlineDate,
                                 QString eventDate,
                                 QString startTime,
                                 QString endTime,
                                 int maxVacancy,
                                 int curVacancy,
                                 QString location,
                                 QString description,
                                 QString eventLink,
                                 int important);
    bool updateUserActivityStatusOnly(QString userId,
                                      QString role,
                                      QString activityStatus,
                                      QString activityId);
    bool updateUserOnly(QString matricNo,
                        QString name,
                        QString role,
                        int rosterMonster,
                        QString personalEmail,
                        QString nuhEmail,
                        QString address,
                        QString homePhone,
                        QString handPhone,
                        QString rotation);

    //change all update table to this:
    bool updateAnyTable(QString tableName, QVariantMap data);

signals:
    void hasSynced(bool status);

private slots:
    void handleFileIsReady(QString filename);
    void handleSyncFailed();


private:
    static LocalDatabaseClient *_dbClient;
    NetworkDatabaseClient *_networkClient;
    QSqlDatabase _db;
    QString _userMatricNo;
    QString _userRole;
    QString _fileName;
    bool _isRosterMonster;
    QHash <QString, QVector <QString> > _schema;
    QHash <QString, QHash <QString, bool> > _isPrimaryKey;
    QHash <QString, int> _numOfPrimaryKey;

//constants
private:
    static const QString DATABASE_NAME;
    static const QString DATABASE_TYPE;
    static const QString CONNECTION_NAME;
    static const QString FORM_TYPE_ONLINE;
    static const QString FORM_TYPE_SOFTCOPY;
    static const QString FORMAT_DATE;
    static const QString FORMAT_TIME;
    static const int NUM_OF_DAY_IN_WEEK;

    static const QVector<QString> TABLE_NAMES;
    static const QVector<QString> FILE_NAMES;
};

#endif // LOCALDATABASECLIENT_H
