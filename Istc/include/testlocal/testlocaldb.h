#ifndef TESTLOCALDB
#define TESTLOCALDB

#ifdef _TEST
    #define private public
#endif

#include <QtTest>

#include "dbclient/localdatabaseclient.h"
#include "objects/assessmentobj.h"

#include "testlocal/generatestring.h"

namespace testlocal {
    class TestLocalDb;
}

class TestLocalDb : public QObject {
    Q_OBJECT

private slots:
    // functions executed by QtTest before and after test suite
    void initTestCase();
    void cleanupTestCase();

    // functions executed by QtTest before and after each test
    void cleanup();

    /*
     * Test Suites
     */

    // Test Adding Users
    void testValidUserByRole();
    void testValidUserByRosterMon();
    void testValidUserByName();

    // Test Adding Activity
    void testAddActivityName0();
    void testAddActivityName1();
    void testAddActivityName2();
    void testAddActivityName3();
    void testAddActivityName4();
    void testAddActivityName5();
    void testAddActivityName6();
    void testAddActivityName7();
    void testAddActivityName8();
    void testAddActivityName9();


    // Test Adding Assessments
    void testGetAssessment();

    void testAddAssessmentFeedback();

    void testAddAssessmentTitle();

    void testValidTableSizes();
    void testValidEmptyTables();
    void testInvalidTableNames();

private:
    LocalDatabaseClient* _localDbClient;
    UserDetails* _user;

private:
    void testInvalidUserRole();  //Actually a unit test but a lot of console output

    void addValidUser(QString name, QString matricNo,
                      QString role, bool isRosterMonster);
    void addInvalidUser(QVector<QString> details, bool isRosterMonster);
    bool addUserToDb(QString name, QString matricNo,
                     QString role, bool isRosterMonster);
    void deleteUser(QString matricNo, QString role);
    void checkIfUserExists(QString matricNo, QString role);

    void addActivityNames(QString name, QString trainerName, bool isPositiveCase);
    void addActivityPlace(QString place, QString trainerName, bool isPositiveCase);
    void addActivityVacancies(int vacancy, QString trainerName, bool isPositiveCase);
    void addActivityDescription(QString description, QString trainerName, bool isPositiveCase);
    void addActivityWebLink(QString webLink, QString trainerName, bool isPositiveCase);
    void addActivityDateAndTime(QDate activityDate, QDateTime registrationDeadline,
                                QTime startTime, QTime endTime,
                                QString trainerName, bool isPositiveCase);

    void addAssessmentFeedback(QString feedback, QString traineeId, QString trainerId);
    void addAssessmentTitle(QString assessTitle, QString traineeId, QString trainerId);
    void addAssessment(QDate assessDate, QString assessId, QString assessTitle,
                       QString feedback, QString grade, QString traineeId, QString trainerId);
    void addAssessmentToDb(AssessmentObj assess);
    AssessmentObj createAssessmentObj(QDate assessDate, QString assessId, QString assessTitle,
                                      QString feedback, QString grade, QString traineeId, QString trainerId);
    void checkAssessmentAddition(QVector<AssessmentObj>*  test,
                                 QVector<AssessmentObj>* fromDb);

    void invalidTableSizes(QString tableName);
    void validTableSizes(QString tableName, int tableSize);

    void validUserDetails(QVector<QString> details, bool isRosterMonster);

    void compareVectorSizes(int size1, int size2);

    void addTestAddActivityNameData();

    void clearAllTables();
};


#endif // TESTLOCALDB

