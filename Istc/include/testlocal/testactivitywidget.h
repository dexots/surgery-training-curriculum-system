#ifndef TESTACTIVITYWIDGET
#define TESTACTIVITYWIDGET

#ifdef _TEST
    #define private public
#endif

#include <limits.h>

#include <QtTest>

#include "gui/activitywidget.h"
#include "objects/userdetails.h"
#include "objects/eventitem.h"
#include "dbclient/localdatabaseclient.h"

#include "testlocal/generatestring.h"

namespace testlocal {
    class TestActivityWidget;
}

class TestActivityWidget : public QObject {
    Q_OBJECT

private slots:
    // functions executed by QtTest before and after test suite
    void initTestCase();
    void cleanupTestCase();

    /*
     * Unit tests
     */
    void testIntegration();

    void testAddActivityNames();
    void testAddActivityPlace();
    void testAddActivityVacancies();
    void testAddActivityDescription();
    void testAddActivityDateAndTime();
    void testAddActivityWebLink();
    void testInvalidAddActivityVacancies();

private:
    void setTrainer(QString trainerId, QString trainerName);
    void deleteUser(QString matricNo);

    void addActivityNames(QString name, QString trainerName, bool isPositiveCase);
    void addActivityPlace(QString place, QString trainerName, bool isPositiveCase);
    void addActivityVacancies(int vacancy, QString trainerName, bool isPositiveCase);
    void addActivityDescription(QString description, QString trainerName, bool isPositiveCase);
    void addActivityWebLink(QString webLink, QString trainerName, bool isPositiveCase);
    void addActivityDateAndTime(QDate activityDate, QDateTime registrationDeadline,
                                QTime startTime, QTime endTime,
                                QString trainerName, bool isPositiveCase);

    void verify(bool variable);

private:
    LocalDatabaseClient* _localDb;
    UserDetails* _userDetails;
    ActivityWidget* _activityWidget;
    void clearAllTables();
};


#endif // TESTACTIVITYWIDGET

