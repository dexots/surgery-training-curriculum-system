#ifndef TESTPROFILEBOX
#define TESTPROFILEBOX


#ifdef _TEST
    #define private public
#endif

#include <QtTest>
#include <QDateTime>
#include <QObject>

#include "gui/profilebox.h"

namespace testlocal {
    class TestProfileBox;
}

class TestProfileBox : public QObject {
    Q_OBJECT

private slots:
    // functions executed by QtTest before and after test suite
    //void initTestCase();
    //void cleanupTestCase();

    // functions executed by QtTest before and after each test
    //void init();
    void cleanup();

    // test functions - all functions prefixed with "test" will be ran as tests
    void testInput_data();
    void testInput();

private:
    ProfileBox *_profileBox;
};


#endif // TESTPROFILEBOX

