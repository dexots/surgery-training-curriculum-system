#ifndef TESTLOGBOOKMODEL_H
#define TESTLOGBOOKMODEL_H


#ifdef _TEST
    #define private public
#endif

#include <QtTest>
#include <QDateTime>
#include <QObject>

#include "model/logbookmodel.h"

namespace testlocal {
    class TestLogbookModel;
}

class TestLogbookModel : public QObject {
    Q_OBJECT

private slots:
    // functions executed by QtTest before and after test suite
    void initTestCase();
    void cleanupTestCase();

    void testCalNoOfHours();
    void testCalNoOfSecs();

private:
    void compareResult(qint64 hours, qint64 ans);
    void verify(bool variable);

private:
    LogbookModel *_logbookModel;
    LocalDatabaseClient *_localDbClient;
    UserDetails *_user;
};

#endif // TESTLOGBOOKMODEL_H
