#ifndef GENERATESTRING
#define GENERATESTRING

#include <iostream>
#include <string>

#include <QObject>

namespace testlocal {
    class GenerateString;
}

class GenerateString : public QObject {
    Q_OBJECT

    static void FillWithAlphaNumeric(std::string& string, unsigned int StringLength);
    static void FillWithNumeric(std::string& string, unsigned int StringLength);
    static void FillWithAlpha(std::string& string, unsigned int StringLength);
    static QString generateString(unsigned int StringLength,
                               bool wantNumber, bool wantAlpha);

};


#endif // GENERATESTRING

