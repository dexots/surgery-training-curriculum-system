#ifndef TESTPATIENTBOX
#define TESTPATIENTBOX

#ifdef _TEST
    #define private public
#endif

#include <QtTest>
#include <QObject>

#include "gui/patientbox.h"

namespace testlocal {
    class TestPatientBox;
}

class TestPatientBox : public QObject {
    Q_OBJECT

private slots:
    // functions executed by QtTest before and after each test
    void init();
    void cleanup();

    // Test functions
    void testInputId_data();
    void testInputId();

private:
    PatientBox *_patientBox;
};

#endif // TESTPATIENTBOX

