//Test build
#ifdef _TEST

    #include <QtTest>

    #include "testlocal/testlogbookmodel.h"
    #include "testlocal/testactivitywidget.h"
    #include "testlocal/testprofilebox.h"
    #include "testlocal/testpatientbox.h"
    #include "testlocal/testlocaldb.h"

    int testModel(int argc, char** argv){
        TestLogbookModel testlogbookModel;

        int testModelValue = QTest::qExec(&testlogbookModel, argc, argv);

        return testModelValue;
    }

    int testGui(int argc, char** argv){
        TestProfileBox testProfileBox;
        TestPatientBox testPatientBox;
        TestActivityWidget testActivityWidget;

        int testGuiValue = QTest::qExec(&testProfileBox, argc, argv);
        testGuiValue |= QTest::qExec(&testActivityWidget, argc, argv);
        testGuiValue |= QTest::qExec(&testPatientBox, argc, argv);

        return testGuiValue;
    }

    int testDbClient(int argc, char** argv){
        TestLocalDb testLocalDb;

        int testDbClientValue = QTest::qExec(&testLocalDb, argc, argv);
        return testDbClientValue;
    }


    int main(int argc, char** argv) {
        //must create QApplication to run QWidget
        QApplication app(argc, argv);
        Q_UNUSED(app);

        // multiple test suites can be ran like this

        return testGui(argc, argv) |
                testModel(argc, argv) |
                testDbClient(argc, argv);
    }

//release/debug build
#else

    #include <stdio.h>
    #include <stdlib.h>

    #include <iostream>
    #include <QStringList>

    #include <QApplication>
    #include <QIcon>
    #include <QtPlugin>
    #include <QFile>
    #include <QFileInfo>

    #include "gui/mainwindow.h"
    #include "logging/messagehandler.h"


    int main(int argc, char *argv[]) {
        qInstallMessageHandler(customMessageHandler);

        QString logFile = "LogFile.log";
        QFileInfo checkFile(logFile);
        // check if file exists and it is a file, remove it
        if (checkFile.exists() && checkFile.isFile()) {
            QFile::remove(logFile);
            qDebug() << "Main -> removeExistingLogFile : Removed Log File";
        } else {
            // do nothing
        }

        QApplication a(argc, argv);
        MainWindow w;

        QIcon icon(":/icon/ButtonIcon/istc.ico");
        w.setWindowIcon(icon);

        w.show();

        return a.exec();
    }

#endif
