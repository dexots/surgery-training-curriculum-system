#include "model/rostermodel.h"

RosterModel* RosterModel::_rosterModel = NULL;

RosterModel* RosterModel::getObject() {
    if (_rosterModel == NULL) {
        _rosterModel = new RosterModel;
    }
    return _rosterModel;
}

void RosterModel::deleteObject() {
    if (_rosterModel != NULL) {
        delete _rosterModel;
    }
}

RosterModel::RosterModel() {
    _localDb = LocalDatabaseClient::getObject();
}

RosterModel::~RosterModel() {
}

//public methods:
QVector <RosterAssignObj> RosterModel::getYourRoster(QDate date) {
    retrieveRosterAssignFromDb();

    QVector <RosterAssignObj> requestedObjs;
    for (int i = 0; i < _yourRoster.size(); i++) {
        RosterAssignObj curObj = _yourRoster.value(i);
        if (curObj.getAssignedDate().month() == date.month()) {
            requestedObjs.append(curObj);
        }
    }
    return requestedObjs;
}

QVector <RosterRequestObj*> *RosterModel::getBlockOutReq() {
    return _localDb->getBlockOutReq();
}

QVector <LeaveObj> RosterModel::getLeaveApplications() {
    retrieveLeaveFromDb();
    return _leaveObjects;
}

QVector <RosterRequestObj*> *RosterModel::getTeamRequest() {
    return _localDb->getTeamRequest();
}

bool RosterModel::saveBlockOutReq (RosterRequestObj *blockOutReq) {
    return _localDb->saveBlockOutReq(blockOutReq);
}

bool RosterModel::updateBlockOutReq(RosterRequestObj *blockOutReq) {
    return _localDb->updateBlockOutReq(blockOutReq);
}

bool RosterModel::addLeaveApplication(LeaveObj leaveObj) {
    return _localDb->addLeaveApplication(leaveObj);
}

bool RosterModel::updateLeaveApplication(LeaveObj leaveObj) {
    return _localDb->updateLeaveApplication(leaveObj);
}

bool RosterModel::saveRosterAssignment (RosterAssignObj assignObj) {
    _yourRoster.append(assignObj);
    return _localDb->saveRosterAssignment(assignObj);
}

QString RosterModel::generateBlockRequestId() {
    return _localDb->generateBlockRequestId();
}

QString RosterModel::generateLeaveId() {
    return _localDb->generateLeaveId();
}

int RosterModel::getCompletedRosterCount() {
    retrieveRosterAssignFromDb();
    int completeRosterCount = 0;
    for (int i = 0; i < _yourRoster.size(); i++) {
        QString rosterStatus = _yourRoster.value(i).getStatus();
        rosterStatus = rosterStatus.toLower();
        if (rosterStatus == RosterAssignObj::STATUS_COMPLETED) {
            completeRosterCount++;
        }
    }
    return completeRosterCount;
}

int RosterModel::getTotalRosterCount() {
    retrieveRosterAssignFromDb();
    return _yourRoster.size();
}

int RosterModel::getNumberOfLeaveApplications() {
    retrieveLeaveFromDb();
    return _leaveObjects.size();
}

//private methods:
void RosterModel::retrieveLeaveFromDb() {
    if (_leaveObjects.isEmpty()) {
        QVector <LeaveObj> *objects = _localDb->getLeaveApplications();
        for (int i = 0; i < objects->size(); i++) {
            _leaveObjects.append(objects->value(i));
        }
    }
}

void RosterModel::retrieveRosterAssignFromDb() {
    if (_yourRoster.isEmpty()) {
        QVector <RosterAssignObj> * dbRoster = _localDb->getYourRoster();
        for (int i = 0; i < dbRoster->size(); i++) {
            _yourRoster.append(dbRoster->value(i));
        }
    }
}
