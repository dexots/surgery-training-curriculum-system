#include "model/personalmodel.h"

PersonalModel* PersonalModel::_rosterModel = NULL;

PersonalModel* PersonalModel::getObject() {
    if (_rosterModel == NULL) {
        _rosterModel = new PersonalModel;
    }
    return _rosterModel;
}

void PersonalModel::deleteObject() {
    delete _rosterModel;
}

PersonalModel::PersonalModel() {
    _localDb = LocalDatabaseClient::getObject();
}

PersonalModel::~PersonalModel() {
}

//public methods
QVector <AssessmentObj>* PersonalModel::getAssessments() {
    return _localDb->getAssessments();
}

QVector <Notes*> PersonalModel::getNotes() {
    retrieveNotesFromDb();
    return _notes;
}

bool PersonalModel::addAssessment(AssessmentObj assessmentObj) {
    return _localDb->addAssessment(assessmentObj);
}

bool PersonalModel::addNote(Notes* notesObj) {
    _notes.append(notesObj);
    return _localDb->addNote(notesObj);
}

bool PersonalModel::updateNote(Notes *notesObj) {
    for (int i = 0; i < _notes.size(); i++) {
        Notes* curNote = _notes.value(i);
        if (curNote->getNotesId() == notesObj->getNotesId()) {
            _notes[i] = notesObj;
            //delete curNote;
            break;
        }
        //delete curNote;
    }
    return _localDb->updateNote(notesObj);
}

QString PersonalModel::generateAssessmentId() {
    return _localDb->generateAssessmentId();
}

QString PersonalModel::generateNotesId() {
    return _localDb->generateNotesId();
}

void PersonalModel::clearNotes() {
    _notes.clear();
}

//private methods
void PersonalModel::retrieveNotesFromDb() {
    if (_notes.isEmpty()) {
        QVector <Notes*> *notes = _localDb->getNotes();
        for (int i = 0; i < notes->size(); i++) {
            _notes.append(notes->value(i));
        }
    }
}
