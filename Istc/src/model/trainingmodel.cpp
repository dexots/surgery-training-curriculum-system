#include "model/trainingmodel.h"

TrainingModel* TrainingModel::_trainingObject = NULL;

TrainingModel::TrainingModel() {
    TrainingModel::initializeModel();
}

TrainingModel::~TrainingModel() {
}

//public static
TrainingModel* TrainingModel::getObject() {
    if (_trainingObject == NULL) {
        _trainingObject = new TrainingModel();
        _trainingObject->getTrainingActivitiesFromLocalDB();
    }
    return _trainingObject;
}

void TrainingModel::deleteObject() {
    delete _trainingObject;
}

//public
void TrainingModel::getTrainingActivitiesFromLocalDB() {
    QVector <EventItem*> *events = _localDbClient->getActivitiesInfo();

    _events.clear();
    for (int i = 0; i < events->size(); i ++) {
        _events.append(events->at(i));
    }
    emit activitiesInfoRefreshed();
    _isActivitiesRetrieved = true;
}


QVector <EventItem*> TrainingModel::getTrainingActivities() {
    if (!_isActivitiesRetrieved) {
        getTrainingActivitiesFromLocalDB();
    } else {
        //do nothing
    }

    return _events;
}

QVector <EventItem*> TrainingModel::getFutureTrainingActivities() {
    if (!_isActivitiesRetrieved) {
        getTrainingActivitiesFromLocalDB();
    } else {
        //do nothing
    }

    QVector <EventItem*> futureEvents = QVector <EventItem*>(_events);
    //remove past events
    for (int i = futureEvents.size() - 1; i >= 0 ; i --) {
        if (futureEvents.at(i)->getDate() < QDate::currentDate()) {
            //futureEvents.at(i)->deleteLater();
            futureEvents.removeAt(i);
        } else {
            // do nothing
        }
    }
    return futureEvents;
}

QVector <Exam> TrainingModel::getExamInfo() {
    getExamInfoFromLocalDB();
    return _exams;
}

QVector <Form*> TrainingModel::getOnlineForms() {
    getOnlineFormsFromLocalDB();
    return _onlineForms;
}

QVector <Form*> TrainingModel::getSoftcopyForms() {
    getSoftcopyFormsFromLocalDB();
    return _softcopyForms;
}

QString TrainingModel::generateActivityId() {
    return _localDbClient->generateActivityId();
}

bool TrainingModel::updateActivity(EventItem* oldEvent) {
    if(_localDbClient->updateActivity(oldEvent)) {
        qDebug() << "TrainingModel -> updateActivityStatus: Update activity status successfully";
        return true;
    } else {
        qDebug() << "TrainingModel -> updateActivityStatus: Fail to update activity status";
        return false;
    }
}

bool TrainingModel::addNewActivity(EventItem* newEvent) {
    assert(newEvent);

    qDebug() << "newEvent description: " + newEvent->getDescription();

    QDateTime registrationDeadline = newEvent->getRegistrationDeadline();
    QDateTime currentDateTime = QDateTime::currentDateTime();
    QDate activityDate = newEvent->getDate();
    QTime startTime = newEvent->getStartTime();
    QTime endTime = newEvent->getEndTime();
    if ((registrationDeadline.date() > activityDate) || (startTime >= endTime) ||
        (registrationDeadline.date() ==  activityDate && startTime <= registrationDeadline.time()) ||
        (registrationDeadline.date() < currentDateTime.date()) ||
        (activityDate < currentDateTime.date()) || (registrationDeadline <= currentDateTime) ||
        (activityDate == currentDateTime.date() && startTime <= currentDateTime.time())) {
        //TODO: change to assert false if able to find a way to detect assertion in QtTest
        qDebug() <<(registrationDeadline.date() > activityDate) << (startTime >= endTime) <<
                   (registrationDeadline.date() ==  activityDate && startTime <= registrationDeadline.time()) <<
                   (registrationDeadline.date() < currentDateTime.date()) <<
                   (activityDate < currentDateTime.date()) << (registrationDeadline <= currentDateTime) <<
                   (activityDate == currentDateTime.date() && startTime <= currentDateTime.time());
        qDebug() << registrationDeadline.date().toString();
        qDebug() << registrationDeadline.time().toString();
        qDebug() << activityDate.toString();
        qDebug() << startTime.toString();
        qDebug() << endTime.toString();
        return false;
    } else {
        // do nothing;
    }

    if(_localDbClient->addNewActivity(newEvent)) {
        qDebug() << "TrainingModel -> updateActivityStatus: Added new activity successfully";
        return true;
    } else {
        qDebug() << "TrainingModel -> updateActivityStatus: Fail to add new activity";
        return false;
    }
}

//private
void TrainingModel::initializeModel() {
    _localDbClient = LocalDatabaseClient::getObject();
}

void TrainingModel::getExamInfoFromLocalDB() {
    QVector <Exam> *exams = _localDbClient->getExamInfo();

    _exams.clear();
    for (int i = 0; i < exams->size(); i ++) {
        _exams.append(exams->at(i));
    }
}
void TrainingModel::getOnlineFormsFromLocalDB() {
    QVector <Form*> *onlineForms = _localDbClient->getOnlineForms();

    for (int i = 0; i < onlineForms->size(); i ++) {
        _onlineForms.append(onlineForms->at(i));
    }
}

void TrainingModel::getSoftcopyFormsFromLocalDB() {
    QVector <Form*> *softcopyForms = _localDbClient->getSoftcopyForms();

    for (int i = 0; i < softcopyForms->size(); i ++) {
        _softcopyForms.append(softcopyForms->at(i));
    }
}
