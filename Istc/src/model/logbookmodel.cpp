#include "model/logbookmodel.h"
#include <iostream>

const int LogbookModel::MAX_DELAY_MIN = 5;
const int LogbookModel::ERROR_CODE = -1;

LogbookModel* LogbookModel::_logObject = NULL;

LogbookModel::LogbookModel() {
    LogbookModel::initializeModel();
}

LogbookModel::~LogbookModel() {
}

//public static
LogbookModel* LogbookModel::getObject() {
    if (_logObject == NULL) {
        _logObject = new LogbookModel;
    }
    return _logObject;
}

void LogbookModel::deleteObject() {
    if (_logObject != NULL) {
        delete _logObject;
    }
}

//public methods
//for working hours object
QVector <WorkingHoursObject> LogbookModel::getWorkingHoursData() {
    if (_workingHours.isEmpty()) {
        QDate todayDate = QDate::currentDate();

        QDate firstDateOfWeek = todayDate.addDays(1 - todayDate.dayOfWeek());

        QDate requestDate = firstDateOfWeek;
        retrieveWorkHourFromDb(todayDate, requestDate);
    }

    return _workingHours;
}

qint64 LogbookModel::calculateCurWeekHour() {
    if (_prevWeekSecs == ERROR_CODE) {
        _prevWeekSecs = 0;
        for (int i = 0; i < _workingHours.size(); i++) {
            WorkingHoursObject work = _workingHours.at(i);
            qint64 sec = work.getNoOfSec();
            _prevWeekSecs += sec;
        }
    }

    qint64 weekSec = _prevWeekSecs;
    if (!_loginDateTime.isNull()) {
        QDateTime now = QDateTime::currentDateTime();
        weekSec += calNoOfSecs(_loginDateTime, now);
    }

    qint64 weekHour = weekSec / (WorkingHoursObject::HOUR_TO_MIN *
                                 WorkingHoursObject::MIN_TO_SEC);

    return weekHour;
}

qint64 LogbookModel::calculateConHour() {
    if (_conHour > 0) {
        return _conHour;
    } else if (_loginDateTime.isNull()) {
        return 0;
    }

    qint64 hour;
    QDateTime current = QDateTime::currentDateTime();
    hour = calNoOfHours(_loginDateTime, current);

    return hour;
}

bool LogbookModel::setLogin(QDateTime dateTime) {
    _loginDateTime = dateTime;
    _conHour = 0;

    QString workingHoursId = _localDbClient->generateWorkingHoursId();
    WorkingHoursObject newObj;
    newObj.setCheckInTime(_loginDateTime);
    newObj.setWorkingHourId(workingHoursId);

    _workingHours.append(newObj);

    return _localDbClient->addWorkingHours(newObj);
}

bool LogbookModel::setLogout(QDateTime dateTime) {
    _logoutDateTime = dateTime;
    qint64 noOfSecs = calNoOfSecs(_loginDateTime,
                                  _logoutDateTime);

    int index = _workingHours.size() - 1;
    WorkingHoursObject oldObj = _workingHours.at(index);
    oldObj.setCheckOutTime(_logoutDateTime);
    oldObj.setNoOfSec(noOfSecs);

    _conHour = noOfSecs / (WorkingHoursObject::HOUR_TO_MIN *
                           WorkingHoursObject::MIN_TO_SEC);
    _prevWeekSecs += noOfSecs;

    _loginDateTime = QDateTime();
    _logoutDateTime = QDateTime();

    return _localDbClient->updateWorkingHours(oldObj);

}

bool LogbookModel::activateWorkingHoursObject(WorkingHoursObject oldObj) {
    QDateTime checkInTime = oldObj.getCheckInTime();
    QDateTime checkOutTime = oldObj.getCheckOutTime();

    assert (checkOutTime.isNull());


    QString workingHoursId = oldObj.getWorkingHourId();

    bool hasDeleted = _localDbClient->deleteWorkingHours(workingHoursId);
    bool isLogin = setLogin(checkInTime);


    for (int i = 0; i < _workingHours.size(); i++) {
        WorkingHoursObject work = _workingHours.at(i);
        if (work.isEqual(oldObj)) {
            _workingHours.remove(i);
            break;
        }
    }

    return hasDeleted && isLogin;
}

void LogbookModel::clearWorkingHoursDatas() {
    _workingHours.clear();
}

//for procedure codes
QVector <ProceduralCode*> *LogbookModel::getProcedures() {
    return _localDbClient->getProcedures();
}

bool LogbookModel::addProcedure(ProceduralCode *newCode) {
    return _localDbClient->addNewProceduralCodes(newCode);
}

bool LogbookModel::updateProcedure(ProceduralCode *oldCode) {
    return _localDbClient->updateProceduralCodes(oldCode);
}

QString LogbookModel::generateNewProcedureId() {
    return _localDbClient->generateProcedureId();
}

//private methods
void LogbookModel::initializeModel() {
    _localDbClient = LocalDatabaseClient::getObject();

    _conHour = ERROR_CODE;
    _prevWeekSecs = ERROR_CODE;
}

void LogbookModel::retrieveWorkHourFromDb(QDate todayDate,
                                          QDate requestDate) {

    while (requestDate.daysTo(todayDate) >= 0) {
        QVector <WorkingHoursObject> *workHours = _localDbClient->
                                                  getWorkingHours(requestDate);
        for (int i = 0; i < workHours->size(); i++) {
            WorkingHoursObject workHour = workHours->at(i);

            QDateTime checkInTime = workHour.getCheckInTime();
            QDateTime checkOutTime = workHour.getCheckOutTime();

            qint64 noOfSecs = calNoOfSecs(checkInTime,
                                          checkOutTime);
            workHour.setNoOfSec(noOfSecs);

            _workingHours.append(workHour);
            std::cout << _workingHours.size();
        }

        requestDate = requestDate.addDays(1);
        delete workHours;
    }
}

qint64 LogbookModel::calNoOfSecs(QDateTime from, QDateTime to) {
    if (to.isNull()) {
        return 0;
    }

    qint64 sec = from.secsTo(to);
    return sec;
}

qint64 LogbookModel::calNoOfHours(QDateTime from, QDateTime to) {
    if (to.isNull()) {
        return 0;
    }

    qint64 sec = calNoOfSecs(from, to);
    qint64 hour = sec / (WorkingHoursObject::HOUR_TO_MIN *
                         WorkingHoursObject::MIN_TO_SEC);
    return hour;
}
