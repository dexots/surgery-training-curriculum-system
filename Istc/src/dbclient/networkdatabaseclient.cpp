#include "dbclient/networkdatabaseclient.h"


const std::string NetworkDatabaseClient::KEY2 = "InteGrateD";
const QString NetworkDatabaseClient::PASSWORD = "\\e-2WE\"|w";
const QString NetworkDatabaseClient::FORMAT_DATE = "ddyyyyMM";
const QString NetworkDatabaseClient::EXT_JSON = ".json";
const QString NetworkDatabaseClient::EXT_BINARY = ".dat";

const QString NetworkDatabaseClient::SERVER_CERTIFICATE = ":/ssl/LocalFile/bluebell.d1.comp.nus.edu.sg.crt";

const int NetworkDatabaseClient::SINGAPORE_OFFSET = 8;


//to do: link with server using ssl

NetworkDatabaseClient* NetworkDatabaseClient::_dbClient = NULL;

NetworkDatabaseClient* NetworkDatabaseClient::getObject() {
    if (_dbClient == NULL){
        _dbClient = new NetworkDatabaseClient;
    }

    return _dbClient;
}

void NetworkDatabaseClient::deleteObject() {
    delete _dbClient;
}

NetworkDatabaseClient::NetworkDatabaseClient() {
    initiateClass();
}

NetworkDatabaseClient::~NetworkDatabaseClient() {
}

//public methods
bool NetworkDatabaseClient::requestSyncToServer(ServerRequestObj serverObj) {
    QString filename = generateFilename(JSON);
    QString password = generatePassword();
    serverObj.setPassword(password);

    QJsonObject jsonObject = serverObj.toJsonObj();
    bool isFileSaved = saveJsonFile(jsonObject, filename, JSON);
    if (!isFileSaved) {
        std::cout << "File is not saved :(\n";
        qDebug() << "NetworkDatabaseClient -> requestSyncToServer : "
                 << "File is not saved :(";
        return false;
    }
    std::cout << "file created! \n";

    //to do: send to server

    bool ret = sendFileToServer(filename,
                                "https://bluebell.d1.comp.nus.edu.sg/~surgery/process.php",
                                "bluebell.d1.comp.nus.edu.sg/~surgery/process.php"
                                //"http://localhost/~surgery/process.php",
                                //"localhost/~surgery/process.php"
                                );


    return ret;
}


void NetworkDatabaseClient::getUserDetails() {
    UserDetails* userDetails = UserDetails::getObject();
    _matricNo = userDetails->getMatricNo();
}


//private methods

//reference : https://forum.qt.io/topic/11086/solved-qnetworkaccessmanager-uploading-files/2
bool NetworkDatabaseClient::sendFileToServer(QString filename,
                                             QString siteUrl,
                                             QString host) {


    qDebug() << "NetworkDatabaseClient -> sendFileToServer: "
                "start initiating connection...";

    QNetworkAccessManager* am = new QNetworkAccessManager(this);

    QUrl url = QUrl::fromPercentEncoding(siteUrl.toLatin1());

    qDebug() << "host is: " << host;
    qDebug() << "url is: " << url.toString();


    am->connectToHostEncrypted(host);

    QFile file(filename);
    if (!file.open(QIODevice::ReadOnly)) {
        return false;
    }

    QByteArray data = file.readAll();

    QNetworkRequest request;

    request.setUrl(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader,
                      QVariant("application/x-www-form-urlencoded"));


    QSslConfiguration config = QSslConfiguration::defaultConfiguration();
    request.setSslConfiguration(config);

    _reply = am->post(request, data);

    connect (_reply, SIGNAL(encrypted()),
             this, SLOT(showHandshakeSuccessful()));
    connect (_reply, SIGNAL(finished()), this, SLOT(handleReplyAfterSendingFile()));
    connect (_reply, SIGNAL(error(QNetworkReply::NetworkError)),
             this, SLOT(handleReplyError(QNetworkReply::NetworkError)));
    connect (_reply, SIGNAL(sslErrors(QList<QSslError>)),
             this, SLOT(handleSslErrors(QList<QSslError>)));

    qDebug() << data.data();

    qDebug() << "NetworkDatabaseClient -> sendFileToServer: "
                "Done sending file to server";

    return true;
}


bool NetworkDatabaseClient::saveJsonFile(QJsonObject jsonObject,
                                         QString filename,
                                         NetworkDatabaseClient::FileFormat fileFormat) {

    QFile saveFile(filename);

    if (!saveFile.open(QIODevice::WriteOnly)) {
        qDebug() << "NetworkDatabaseClient -> saveJsonFile : "
                 << "Couldn't open save file.";
        return false;
    }

    QJsonDocument saveDoc(jsonObject);

    int error = 0;
    if (fileFormat == JSON) {
        error = saveFile.write(saveDoc.toJson());
    } else if (fileFormat == BINARY) {
        error = saveFile.write(saveDoc.toBinaryData());
    } else {
        assert (false);
    }
    return error != -1;
}

QJsonObject NetworkDatabaseClient::loadJsonFile(QString filename) {
    qDebug() << "NetworkDatabaseClient -> saveJsonFile : "
             << "Start loading save file.";

    QJsonObject json;

    QFile loadFile(filename);

    if (!loadFile.open(QIODevice::ReadOnly)) {
        qDebug() << "NetworkDatabaseClient -> saveJsonFile : "
                 << "Couldn't open save file.";
        return json;
    }

    QByteArray saveData = loadFile.readAll();

    QJsonDocument loadDoc(QJsonDocument::fromJson(saveData));
    if (filename.toLower().endsWith(EXT_BINARY)) {
        loadDoc = QJsonDocument::fromBinaryData(saveData);
    } else if (filename.toLower().endsWith(EXT_JSON)) {
    } else {
        assert (false);
    }

    json = loadDoc.object();

    QJsonObject::iterator i;
    for (i = json.begin(); i != json.end(); i++) {
        qDebug() << i.key() << " " << i.value();
    }

    qDebug() << "NetworkDatabaseClient -> saveJsonFile : "
             << "Done loading save file.";

    return json;
}

void NetworkDatabaseClient::initiateClass() {
    setupCertificate();
}

bool NetworkDatabaseClient::setupCertificate() {
    qDebug() << "NetworkDatabaseClient -> setupCertificate: "
                "setuping the certificate...";

    // Read the SSL certificate
    QFile file(SERVER_CERTIFICATE);
    if (!file.open(QIODevice::ReadOnly)) {
        qDebug() << "NetworkDatabaseClient -> setupCertificate: "
                    "can't open .cert file";
        std::cout << "can't open cert file\n";
        return false;
    }
    const QByteArray bytes = file.readAll();

    qDebug() << "setup certificate success!!!\n";

    // Create a certificate object
    const QSslCertificate certificate(bytes);

    qDebug() << certificate.toText();

    QSslSocket::addDefaultCaCertificate(certificate);


    qDebug() << "NetworkDatabaseClient -> setupCertificate: "
                "done setuping the certificate";

    return true;
}


QString NetworkDatabaseClient::generatePassword() {
    QDateTime date = QDateTime::currentDateTime();
    date.setOffsetFromUtc(SINGAPORE_OFFSET);

    QString pass = PASSWORD + date.toString(FORMAT_DATE);
    pass = encryptDecrypt(pass, KEY2);
    return pass;
}

QString NetworkDatabaseClient::generateFilename(NetworkDatabaseClient::FileFormat fileFormat) {
    QString filename = _matricNo;

    if (fileFormat == JSON) {
        filename += EXT_JSON;
    } else if (fileFormat == BINARY) {
        filename += EXT_BINARY;
    } else {

        //should not reach here
        assert (false);
    }
    return filename;
}

//reference : http://kylewbanks.com/blog/Simple-XOR-Encryption-Decryption-in-Cpp
QString NetworkDatabaseClient::encryptDecrypt(QString toEncrypt,
                                              std::string key) {
    std::string originalString = toEncrypt.toStdString();
    std::string encryptedString = originalString;

    for (int i = 0; i < toEncrypt.size(); i++) {
        encryptedString[i] =
                originalString[i] ^ key[i % (key.size() / sizeof(char))];
    }

    QString output = QString::fromStdString(encryptedString);
    return output;
}


//private slots
void NetworkDatabaseClient::handleReplyAfterSendingFile() {
    qDebug() << "NetworkDatabaseClient -> handleReplyAfterSendingFile : "
             << "start handling reply...";

    if (_reply->isFinished()) {
        qDebug() << "reply is finished";
    }

    if (_reply->error()) {
        qDebug() << "error!\n"
                 << _reply->errorString();
        qDebug() << "the HTTP status code : ";
        qDebug() << _reply->attribute( QNetworkRequest::HttpStatusCodeAttribute).toInt();
        qDebug() << "the page returned";
        qDebug() << _reply->readAll();

        emit this->syncFailed();
    } else {
        int v = _reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
        if (v >= 200 && v < 300) { // Success
            qDebug() << "Connected successfully!";
            qDebug() << _reply->header(QNetworkRequest::ContentTypeHeader).toString();
            qDebug() << _reply->header(QNetworkRequest::LastModifiedHeader).toDateTime().toString();
            qDebug() << _reply->header(QNetworkRequest::ContentLengthHeader).toULongLong();
            qDebug() << _reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
            qDebug() << _reply->attribute(QNetworkRequest::HttpReasonPhraseAttribute).toString();

            QByteArray data = _reply->readAll();

            QString fileName = "reply.json";
            QFile file (fileName);
            if (file.open(QFile::WriteOnly)) {
                file.write(data);
                qDebug() << data;
                file.flush();
                file.close();
            } else {
                qDebug() << "NetworkDatabaseClient -> handleReplyAfterSendingFile : "
                            "failed to open file";
                return;
            }

            emit this->fileIsReady(fileName);


        } else if (v >= 300 && v < 400) { // Redirection
            qDebug() << "Website redirected";
            qDebug() << _reply->header(QNetworkRequest::ContentTypeHeader).toString();
            qDebug() << _reply->header(QNetworkRequest::LastModifiedHeader).toDateTime().toString();
            qDebug() << _reply->header(QNetworkRequest::ContentLengthHeader).toULongLong();
            qDebug() << _reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
            qDebug() << _reply->attribute(QNetworkRequest::HttpReasonPhraseAttribute).toString();
            emit this->syncFailed();

        }
    }


    _reply->deleteLater();

    qDebug() << "NetworkDatabaseClient -> handleReplyAfterSendingFile : "
                "done handling reply";

}

void NetworkDatabaseClient::handleReplyError(QNetworkReply::NetworkError error) {
    qDebug() << "NetworkDatabaseClient -> handleReplyError: "
                "start handling error";

    if (error == QNetworkReply::NoError) {
        qDebug() << "No error is detected";
    } else if (error == QNetworkReply::UnknownNetworkError){
        qDebug() << "Unknown Network Error detected";
    } else if (error == QNetworkReply::UnknownServerError){
        qDebug() << "Unknown Server Error detected";
    } else if (error == QNetworkReply::UnknownProxyError){
        qDebug() << "Unknown Proxy Error detected";
    } else if (error == QNetworkReply::UnknownContentError){
        qDebug() << "Unknown Content Error detected";
    } else {
        qDebug() << "Other error is detected!";
        qDebug() << _reply->errorString();
    }

    qDebug() << "NetworkDatabaseClient -> handleReplyError: "
                "done handling error";

}

void  NetworkDatabaseClient::showHandshakeSuccessful() {
    qDebug() << "NetworkDatabaseClient -> showHandshakeSuccessful: "
                "you are good. Handshake successful!";
}

void NetworkDatabaseClient::handleSslErrors(QList<QSslError> errors) {
    qDebug() << "NetworkDatabaseClient -> handleSslErrors: "
                "There are ssl errors: ";
    for (int i = 0; i < errors.size(); i++) {
        qDebug() << errors.at(i).errorString();
    }
    qDebug() << "NetworkDatabaseClient -> handleSslErrors: "
                "Done showing ssl errors... ";
}
