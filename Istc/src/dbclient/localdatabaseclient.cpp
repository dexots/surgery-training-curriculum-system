#include "dbclient/localdatabaseclient.h"

const QString LocalDatabaseClient::DATABASE_NAME = "local.sqlite";
const QString LocalDatabaseClient::DATABASE_TYPE = "QSQLITE";
const QString LocalDatabaseClient::CONNECTION_NAME = "localDb";
const QString LocalDatabaseClient::FORM_TYPE_ONLINE = "online";
const QString LocalDatabaseClient::FORM_TYPE_SOFTCOPY = "softcopy";
const QString LocalDatabaseClient::FORMAT_DATE = "yyyy-MM-dd";
const QString LocalDatabaseClient::FORMAT_TIME = "hh:mm";
const int LocalDatabaseClient::NUM_OF_DAY_IN_WEEK = 7;
LocalDatabaseClient* LocalDatabaseClient::_dbClient = NULL;

const QVector<QString> LocalDatabaseClient::TABLE_NAMES
        = QVector<QString>()
        << "user" << "forms" << "userForms"
        << "exams" << "traineeExams" << "activities"
        << "userActivities" << "assessment"  << "trainerAssessment"
        << "notes" << "patient" << "procedureTable"
        << "userProcedure" << "workingHours" << "rosterBlockOutReq"
        << "rosterAssignment" << "userRosterAssignment"
        << "leaveApp";

const QVector<QString> LocalDatabaseClient::FILE_NAMES
        = QVector<QString>()
        << "milestones.txt";



LocalDatabaseClient* LocalDatabaseClient::getObject(){
    if (_dbClient == NULL){
        _dbClient = new LocalDatabaseClient;
    }

    return _dbClient;
}

void LocalDatabaseClient::deleteObject(){
    delete _dbClient;
}

LocalDatabaseClient::LocalDatabaseClient() {
    LocalDatabaseClient::initiateClass();
    LocalDatabaseClient::setMatricNo("");
}

LocalDatabaseClient::~LocalDatabaseClient() {
    _db.close();
    _networkClient->deleteObject();
}

//public methods
void LocalDatabaseClient::getUserDetails() {
    UserDetails* userDetails = UserDetails::getObject();
    setMatricNo(userDetails->getMatricNo().trimmed());
    setUserRole(userDetails->getUserRole().toLower().trimmed());
    setIsRosterMonster(userDetails->getIsRosterMonster());
}

bool LocalDatabaseClient::isCurrentUserExist() {
    qDebug() << "LocalDatabaseClient -> isCurrentUserExist: "
                "Start checking wheter the user is exist";


    UserDetails* userDetails = UserDetails::getObject();
    QString matricNo = userDetails->getMatricNo().toLower().trimmed();
    QString role = userDetails->getUserRole().toLower().trimmed();

    bool isExist = isValidUser(matricNo, role);

    qDebug() << "LocalDatabaseClient -> isCurrentUserExist: "
                "done checking!";

    return isExist;
}

bool LocalDatabaseClient::addCurrentUser() {
    qDebug() << "LocalDatabaseClient -> addCurrentUser: "
                "start adding user..";

    UserDetails* userDetails = UserDetails::getObject();
    QString matricNo = userDetails->getMatricNo().trimmed().toUpper();
    QString name = userDetails->getUserName().trimmed();
    QString role = userDetails->getUserRole().trimmed();
    bool isRosterMonster = userDetails->getIsRosterMonster();
    QString personalEmail = userDetails->getPersonalEmail().trimmed();
    QString nuhEmail = userDetails->getUserEmail().trimmed();
    QString address = userDetails->getAddress().trimmed();
    QString homePhone = userDetails->getHomePhone().trimmed();
    QString handPhone = userDetails->getHandPhone().trimmed();
    QString rotation = userDetails->getRotation().trimmed();

    if (!isMatricNoUnique(matricNo)) {
        QString errorMsg = "LocalDatabaseClient -> addCurrentUser: "
                           "matric No is not unique!!! ";
        qDebug() << errorMsg;
        return false;
    }


    int rosterMonster = 0;
    if (isRosterMonster) {
        rosterMonster = 1;
    }

    bool isSuccess = insertIntoUser(matricNo,
                                    name,
                                    role,
                                    rosterMonster,
                                    personalEmail,
                                    nuhEmail,
                                    address,
                                    homePhone,
                                    handPhone,
                                    rotation);
    qDebug() << "LocalDatabaseClient -> addCurrentUser: "
                "done adding user.. status: " << isSuccess;
    return isSuccess;
}



bool LocalDatabaseClient::deleteCurrentUser(QString matricNo) {
    qDebug() << "LocalDatabaseClient -> deleteCurrentUser: "
                "start deleting user..";

    matricNo = matricNo.toUpper().trimmed();

    QString queryString = "DELETE FROM user "
                          "WHERE matricNo = :matricNo";
    QSqlQuery query(_db);

    bool isSuccess = query.prepare(queryString);
    query.bindValue(":matricNo", matricNo);

    isSuccess = isSuccess && query.exec();

    if (!isSuccess) {
        QString errorMsg = "LocalDatabaseClient -> deleteCurrentUser: " +
                           query.lastError().text();
        qDebug() << errorMsg;
    }

    qDebug() << "LocalDatabaseClient -> deleteCurrentUser: "
                "done deleting user..";
    return isSuccess;
}



bool LocalDatabaseClient::updateCurrentUser() {
    qDebug() << "LocalDatabaseClient -> updateCurrentUser: "
                "start updating user..";

    UserDetails* userDetails = UserDetails::getObject();
    QString matricNo = userDetails->getMatricNo().trimmed();
    QString name = userDetails->getUserName().trimmed();
    QString role = userDetails->getUserRole().trimmed();
    bool isRosterMonster = userDetails->getIsRosterMonster();
    QString personalEmail = userDetails->getPersonalEmail().trimmed();
    QString nuhEmail = userDetails->getUserEmail().trimmed();
    QString address = userDetails->getAddress().trimmed();
    QString homePhone = userDetails->getHomePhone().trimmed();
    QString handPhone = userDetails->getHandPhone().trimmed();
    QString rotation = userDetails->getRotation().trimmed();

    int rosterMonster = 0;
    if (isRosterMonster) {
        rosterMonster = 1;
    }

    bool isSuccess = updateUserOnly(matricNo,
                                    name,
                                    role,
                                    rosterMonster,
                                    personalEmail,
                                    nuhEmail,
                                    address,
                                    homePhone,
                                    handPhone,
                                    rotation);

    qDebug() << "LocalDatabaseClient -> updateCurrentUser: "
                "done updating user..";
    return isSuccess;
}



QVector <WorkingHoursObject> *
    LocalDatabaseClient::getWorkingHours(QDate date) {
    QSqlQuery query(_db);

    bool ret = true;
    QString queryString;

    QVector <WorkingHoursObject> *works =
            new QVector <WorkingHoursObject>;

    QString userId = _userMatricNo;

    queryString = "SELECT workingHoursId, dayIn, timeIn, dayOut, timeOut "
                  "FROM workingHours "
                  "WHERE dayIn = :dayIn "
                  "AND userId = :userId";
    ret = ret && query.prepare(queryString);

    query.bindValue(":dayIn", date.toString(FORMAT_DATE));
    query.bindValue(":userId", userId);

    ret = ret && query.exec();

    if (!ret) {
        qDebug() << "LocalDatabaseClient -> getWorkingHours: "
                 << query.lastError().text();

        return works;
    }

    while (query.next()) {
        QSqlRecord record = query.record();
        QString workingHoursId = record.value("workingHoursId").toString().trimmed();
        QString curDayIn = record.value("dayIn").toString().trimmed();
        QString curTimeIn = record.value("timeIn").toString().trimmed();
        QString curDayOut = record.value("dayOut").toString().trimmed();
        QString curTimeOut = record.value("timeOut").toString().trimmed();

        QDateTime curCheckInTime = QDateTime::fromString
                                   (curDayIn + " " + curTimeIn,
                                    FORMAT_DATE + " " + FORMAT_TIME);
        QDateTime curCheckOutTime = QDateTime::fromString
                                    (curDayOut + " " + curTimeOut,
                                     FORMAT_DATE + " " + FORMAT_TIME);

        WorkingHoursObject workingHoursObject;
        workingHoursObject.setWorkingHourId(workingHoursId);
        workingHoursObject.setCheckInTime(curCheckInTime);
        workingHoursObject.setCheckOutTime(curCheckOutTime);
        works->append(workingHoursObject);
    }

    return works;
}

bool LocalDatabaseClient::deleteWorkingHours(QString workingHoursId) {
    if (workingHoursId.isNull()) {
        return false;
    }

    workingHoursId = workingHoursId.trimmed();
    if (workingHoursId.isEmpty()) {
        return false;
    }


    QSqlQuery query(_db);
    bool ret;
    QString queryString = "DELETE FROM workingHours "
                          "WHERE workingHoursId = :workingHoursId";
    ret = query.prepare(queryString);
    query.bindValue(":workingHoursId", workingHoursId);
    ret = ret && query.exec();

    if (!ret) {
        qDebug() << "LocalDatabaseClient -> deleteWorkingHours: "
                 << query.lastError().text();
    }
    return ret;
}

bool LocalDatabaseClient::updateWorkingHours(WorkingHoursObject oldObj) {
    if (oldObj.isEmpty()) {
        return false;
    }

    QSqlQuery query(_db);
    bool ret;

    QDateTime checkOutTime = oldObj.getCheckOutTime();
    QString dayOut = checkOutTime.toString(FORMAT_DATE);
    QString timeOut = checkOutTime.toString(FORMAT_TIME);
    QString workingHoursId = oldObj.getWorkingHourId().trimmed();

    QString queryString = "UPDATE workingHours "
                          "SET dayOut = :dayOut, "
                          "timeOut = :timeOut "
                          "WHERE workingHoursId = :workingHoursId";
    ret = query.prepare(queryString);
    query.bindValue(":dayOut", dayOut);
    query.bindValue(":timeOut", timeOut);
    query.bindValue(":workingHoursId", workingHoursId);
    ret = ret && query.exec();

    if (!ret) {
        qDebug() << query.lastError().text();
    }
    return ret;
}

bool LocalDatabaseClient::addWorkingHours(
        WorkingHoursObject newObj) {
    assert (!newObj.isEmpty());

    bool ret = true;

    QString userId = _userMatricNo;
    QString role = _userRole;
    QString id = newObj.getWorkingHourId().trimmed();

    QDateTime checkInTime = newObj.getCheckInTime();

    QString dayIn = checkInTime.toString(FORMAT_DATE);
    QString timeIn = checkInTime.toString(FORMAT_TIME);
    QString dayOut = "";
    QString timeOut = "";

    QSqlQuery query(_db);
    QString queryString = ("INSERT INTO workingHours ("
                           "userId, role, "
                           "workingHoursId, dayIn, "
                           "timeIn, dayOut, timeOut) "
                           "VALUES ( "
                           ":userId, :role, "
                           ":workingHoursId, :dayIn, "
                           ":timeIn, :dayOut, :timeOut)");


    ret = ret &&  query.prepare(queryString);
    query.bindValue(":userId", userId);
    query.bindValue(":role", role);
    query.bindValue(":workingHoursId", id);
    query.bindValue(":dayIn", dayIn);
    query.bindValue(":timeIn", timeIn);
    query.bindValue(":dayOut", dayOut);
    query.bindValue(":timeOut", timeOut);
    ret = ret &&  query.exec();

    if (!ret) {
        qDebug() << "LocalDatabaseClient -> addWorkingHours: "
                 << query.lastError().text();
    }

    return ret;
}

QVector <ProceduralCode*> *LocalDatabaseClient::getProcedures() {
    QSqlQuery query(_db);
    QString queryString;
    QVector <ProceduralCode*> *codes
            = new QVector <ProceduralCode*> ;
    bool ret = true;

    queryString = "SELECT p.procedureId, p.date, "
                  "p.time, p.patientId, p.remarks, "
                  "p.name, p.outcome, b.dob "
                  "FROM procedureTable p, patient b, userProcedure up "
                  "WHERE p.patientId = b.patientId "
                  "AND p.procedureId = up.procedureId "
                  "AND up.userId = :userId "
                  "AND up.role = :role";
    ret = ret &&  query.prepare(queryString);

    QString userId = _userMatricNo;
    QString role = _userRole;
    query.bindValue(":userId", userId);
    query.bindValue(":role", role);

    ret = ret && query.exec();

    while (query.next()) {
        QSqlRecord record = query.record();
        QString procedureId = record.value("procedureId").toString();
        QString procedureDateString = record.value("date").toString();
        QString procedureTimeString = record.value("time").toString();
        QString patientID = record.value("patientId").toString();
        QString patientDOBString = record.value("dob").toString();
        QString procedureRemarks = record.value("remarks").toString();
        QString procedureName = record.value("name").toString();
        QString outcome = record.value("outcome").toString();

        QDate procedureDate = QDate::fromString(procedureDateString,
                                                FORMAT_DATE);
        QTime procedureTime = QTime::fromString(procedureTimeString,
                                                FORMAT_TIME);
        QDate patientDOB = QDate::fromString(patientDOBString,
                                             FORMAT_DATE);

        ProceduralCode* code = new ProceduralCode;
        code->setProcedureID(procedureId);
        code->setProcedureDate(procedureDate);
        code->setProcedureTime(procedureTime);
        code->setPatientID(patientID);
        code->setPatientDOB(patientDOB);
        code->setProcedureRemarks(procedureRemarks);
        code->setProcedureCodeName(procedureName);
        code->setProcedureOutcome(outcome);
        codes->append(code);


    }

    if (!ret) {
        QString errorMsg = "LocalDatabaseClient -> getProcedures: " +
                           query.lastError().text();
        qDebug() << errorMsg;
    }

    return codes;
}

//only trainee can get exam info
QVector <Exam> *LocalDatabaseClient::getExamInfo() {
    assert (_userRole.toLower() == UserDetails::ROLE_TRAINEE);

    QSqlQuery query(_db);
    QString queryString;
    QVector <Exam> *exams
            = new QVector <Exam> ;
    bool ret = true;

    QString traineeId = _userMatricNo;
    queryString = "SELECT e.moduleId, e.moduleName, "
                  "e.date, e.time, e.location, t.seatNo "
                  "FROM exams e, traineeExams t "
                  "WHERE e.moduleId = t.moduleId "
                  "AND t.traineeId = :traineeId "
                  "GROUP BY e.moduleId";
    ret = ret && query.prepare(queryString);
    query.bindValue(":traineeId", traineeId);
    ret = ret && query.exec();

    while (query.next()) {
        QSqlRecord record = query.record();
        QString moduleCode = record.value("moduleId").toString();
        QString moduleName = record.value("moduleName").toString();
        QString examDateString = record.value("date").toString();
        QString examTimeString = record.value("time").toString();
        QString examLocation = record.value("location").toString();
        QString seatNo = record.value("seatNo").toString();

        QDate examDate = QDate::fromString(examDateString,
                                           FORMAT_DATE);
        QTime examTime = QTime::fromString(examTimeString,
                                           FORMAT_TIME);

        Exam curExam;
        curExam.setModuleCode(moduleCode);
        curExam.setModuleName(moduleName);
        curExam.setExamDate(examDate);
        curExam.setExamTime(examTime);
        curExam.setExamLocation(examLocation);
        curExam.setSeatNo(seatNo);
        exams->append(curExam);
    }

    if (!ret) {
        qDebug() << "LocalDatabaseClient -> getExamInfo: "
                 << query.lastError().text();
    }

    return exams;
}

QVector <Form*> *LocalDatabaseClient::getOnlineForms() {
    return getForm(FORM_TYPE_ONLINE);
}

QVector <Form*> *LocalDatabaseClient::getSoftcopyForms() {
    return getForm(FORM_TYPE_SOFTCOPY);
}

QVector <QString> *LocalDatabaseClient::getPatientData(QString patientId) {
    //assume patientId has already validated
    //id, dob

    QSqlQuery query(_db);
    QString queryString;
    QVector <QString> *patientData = new QVector <QString>();

    patientData->append(patientId);

    bool ret = true;

    queryString = "SELECT dob "
                  "FROM patient "
                  "WHERE patientId LIKE (:patientId)";
    ret = ret && query.prepare(queryString);

    query.bindValue(":patientId", patientId);

    ret = ret && query.exec();

    while (query.next()) {
        QSqlRecord record = query.record();
        QString dob = record.value("dob").toString();

        patientData->append(dob);
    }

    if (!ret) {
        patientData->clear();
    }

    return patientData;
}

QVector <Notes*> *LocalDatabaseClient::getNotes() {
    QSqlQuery query(_db);
    QString queryString;
    bool check;
    QVector <Notes*>* notes = new QVector <Notes*>();

    QString userId = _userMatricNo;

    queryString = "SELECT n.noteId, n.noteTitle, n.date, n.content "
                  "FROM notes n "
                  "WHERE n.userId = :userId";
    check = query.prepare(queryString);

    query.bindValue(":userId", userId);

    check = check && query.exec();

    while (query.next()) {
        QSqlRecord record = query.record();
        QString noteId = record.value("noteId").toString();
        QString noteTitle = record.value("noteTitle").toString();
        QString dateString = record.value("date").toString();
        QString content = record.value("content").toString();

        QDate date = QDate::fromString(dateString,
                                       FORMAT_DATE);

        Notes* note = new Notes;
        note->setNotesId(noteId);
        note->setNotesTitle(noteTitle);
        note->setNotesDate(date);
        note->setNotesContent(content);

        notes->append(note);
    }

    if (!check) {
        qDebug() << "LocalDatabaseClient -> getNotes: "
                 << query.lastError().text();
    }

    return notes;
}

bool LocalDatabaseClient::updateProceduralCodes(ProceduralCode *code) {
    QSqlQuery query(_db);
    QString queryString;

    bool ret = true;

    QString procedureId = code->getProcedureID().trimmed();
    queryString = "UPDATE procedureTable "
                  "SET remarks = :remarks "
                  "WHERE procedureId = :procedureId ";

    ret = ret && query.prepare(queryString);
    query.bindValue(":remarks", code->getProcedureRemarks().trimmed());
    query.bindValue(":procedureId", procedureId);
    ret = ret && query.exec();

    if (!ret) {
        qDebug() << "LocalDatabaseClient -> updateProceduralCodes: "
                 << query.lastError().text();
    }

    return ret;
}

bool LocalDatabaseClient::addNewProceduralCodes(ProceduralCode *code) {
    QSqlQuery query(_db);
    QString queryString;

    bool ret = true;

    //patientId can be failed
    bool hasNoPatient;
    QString patientId = code->getPatientID().trimmed();
    QString dob = code->getPatientDOB().toString(FORMAT_DATE);
    queryString = "INSERT INTO patient ( "
                  "patientId, dob ) "
                  "VALUES ( "
                  ":patientId, :dob ) ";
    hasNoPatient = query.prepare(queryString);
    query.bindValue(":patientId", patientId);
    query.bindValue(":dob", dob);
    hasNoPatient = hasNoPatient && query.exec();

    if (!hasNoPatient) {
        queryString = "SELECT * "
                      "FROM patient p "
                      "WHERE p.patientId = :patientId "
                      "AND p.dob = :dob ";
        bool hasNoQueryError = query.prepare(queryString);
        query.bindValue(":patientId", patientId);
        query.bindValue(":dob", dob);
        hasNoQueryError = hasNoQueryError && query.exec();

        if (!hasNoQueryError) {
            qDebug() << "LocalDatabaseClient -> addNewProceduralCodes: "
                     << query.lastError().text();
        }

        bool hasResults = false;
        while (query.next()) {
            hasResults = true;
        }
        if (!hasResults) {
            return false;
        }
    }


    queryString = ("INSERT INTO procedureTable ("
                   "procedureId, name, date, time, "
                   "patientId, remarks, outcome) "
                   "VALUES ("
                   ":procedureId, :name, :date, :time, "
                   ":patientId, :remarks, :outcome"
                   ")");

    ret = ret && query.prepare(queryString);

    QString id = code->getProcedureID().trimmed();
    QString name = code->getProcedureCode().trimmed();
    QString date = code->getProcedureDate().toString(FORMAT_DATE);
    QString time = code->getProcedureTime().toString(FORMAT_TIME);   
    QString remarks = code->getProcedureRemarks().trimmed();
    QString outcome = code->getProcedureOutcome().trimmed();
    query.bindValue(":procedureId", id);
    query.bindValue(":name", name);
    query.bindValue(":date", date);
    query.bindValue(":time", time);
    query.bindValue(":patientId", patientId);
    query.bindValue(":remarks", remarks);
    query.bindValue(":outcome", outcome);

    ret = ret && query.exec();

    if (!ret) {
        qDebug() << "LocalDatabaseClient -> addNewProceduralCodes: "
                 << query.lastError().text();
    }

    queryString = "INSERT INTO userProcedure ("
                  "procedureId, userId, role) "
                  "VALUES ("
                  ":procedureId, :userId, :role"
                  ")";
    ret = ret && query.prepare(queryString);
    QString userId = _userMatricNo;
    QString role = _userRole;
    query.bindValue(":procedureId", id);
    query.bindValue(":userId", userId);
    query.bindValue(":role", role);
    ret = ret && query.exec();

    if (!ret) {
        qDebug() << "LocalDatabaseClient -> addNewProceduralCodes: "
                 << query.lastError().text();
    }

    return ret;
}

QVector <EventItem*> *LocalDatabaseClient::getActivitiesInfo() {
    QSqlQuery query(_db);
    QString queryString;
    QVector <EventItem*> *events
            = new QVector <EventItem*> ;
    bool ret = true;

    QString userRole = _userRole;
    QString userId = _userMatricNo;
    queryString = "SELECT a.activityId, a.title, "
                  "a.registrationDeadlineTime, a.registrationDeadlineDate, "
                  "a.eventDate, a.eventStartTime, "
                  "a.eventEndTime, a.curVacancy, "
                  "a.maxVacancy, a.location, "
                  "a.description, a.important, a.link, "
                  "u.name, ua2.status "
                  "FROM activities a, userActivities ua1, "
                  "userActivities ua2, user u "
                  "WHERE a.activityId = ua1.activityId "
                  "AND a.activityId = ua2.activityId "
                  "AND ua1.userId = u.matricNo "
                  "AND ua2.userId = :userId "
                  "AND LOWER(ua1.role) LIKE ('trainer') "
                  "AND LOWER(ua2.role) = :userRole "
                  "GROUP BY a.activityId";

    ret = ret && query.prepare(queryString);
    query.bindValue(":userId", userId);
    query.bindValue(":userRole", userRole);
    ret = ret && query.exec();

    if (!ret) {
        qDebug() << "LocalDatabaseClient -> getActivitiesInfo: "
                 << query.lastError().text();
        return events;
    }

    while (query.next()) {
        QSqlRecord record = query.record();
        QString activityId = record.value("activityId").toString();
        QString title = record.value("title").toString();
        QString dateString = record.value("eventDate").toString();
        QString startTimeString = record.value("eventStartTime").toString();
        QString endTimeString = record.value("eventEndTime").toString();
        QString registrationDeadlineString = record.value("registrationDeadlineDate").toString();
        registrationDeadlineString += " " + record.value("registrationDeadlineTime").toString();
        int curVacancy = record.value("curVacancy").toInt();
        int maxVacancy = record.value("maxVacancy").toInt();
        QString location = record.value("location").toString();
        QString description = record.value("description").toString();
        QString eventLink = record.value("link").toString();
        QString trainerName = record.value("name").toString();
        bool isImportant = record.value("important").toInt() == 1;
        bool isRegistered = record.value("status").toString().toLower() == "registered";

        QDate date = QDate::fromString(dateString, FORMAT_DATE);
        QTime startTime = QTime::fromString(startTimeString, FORMAT_TIME);
        QTime endTime = QTime::fromString(endTimeString, FORMAT_TIME);
        QDateTime registrationDeadline = QDateTime::fromString(
                    registrationDeadlineString,
                    FORMAT_DATE + " " + FORMAT_TIME);

        EventItem* event =
                new EventItem(date, startTime, endTime, title, location,
                              trainerName, curVacancy, maxVacancy, description,
                              eventLink, isImportant);
        event->setIsUserRegistered(isRegistered);
        event->setActivityId(activityId);
        event->setRegistrationDeadline(registrationDeadline);

        events->append(event);
    }

    return events;
}




bool LocalDatabaseClient::updateActivity(EventItem *activity) {

    if (_userRole.toLower() == UserDetails::ROLE_TRAINEE) {
        return updateUserActivityStatus(activity);
    } else if (_userRole.toLower() != UserDetails::ROLE_TRAINER) {
        return false;
    }

    bool isSuccess = updateUserActivityStatus(activity);

    QString activityId = activity->getActivityId().trimmed();
    QString title = activity->getName().trimmed();

    QDateTime registrationDeadline = activity->getRegistrationDeadline();
    QString registrationDeadlineTime = registrationDeadline.toString(FORMAT_TIME);
    QString registrationDeadlineDate = registrationDeadline.toString(FORMAT_DATE);

    QString eventDate = activity->getDate().toString(FORMAT_DATE);
    QString eventStartTime = activity->getStartTime().toString(FORMAT_TIME);
    QString eventEndTime = activity->getEndTime().toString(FORMAT_TIME);

    int maxVacancy = activity->getMaxVacancy();
    int curVacancy = activity->getCurrentVacancy();

    QString location = activity->getLocation().trimmed();
    QString description = activity->getDescription().trimmed();
    QString link = activity->getLink().trimmed();

    int important = 0;
    bool isImportant = activity->getIsImportant();

    if (isImportant) {
        important = 1;
    }

    isSuccess = isSuccess && updateActivityTableOnly(activityId,
                                                    title,
                                                    registrationDeadlineTime,
                                                    registrationDeadlineDate,
                                                    eventDate,
                                                    eventStartTime,
                                                    eventEndTime,
                                                    maxVacancy,
                                                    curVacancy,
                                                    location,
                                                    description,
                                                    link,
                                                    important);

    return isSuccess;

}


bool LocalDatabaseClient::updateActivityCurVacancy(int curVacancy,
                                                   QString activityId) {
    QSqlQuery query(_db);
    QString queryString;

    bool ret;

    queryString = "UPDATE activities "
                  "SET curVacancy = :curVacancy "
                  "WHERE activityId = :activityId";
    ret = query.prepare(queryString);
    query.bindValue(":curVacancy", curVacancy);
    query.bindValue(":activityId", activityId);
    ret = ret && query.exec();

    return ret;
}


bool LocalDatabaseClient::updateFormStatus(Form edittedForm) {
    QSqlQuery query(_db);
    QString queryString;
    QString urlString = edittedForm.getUrlAddress().trimmed();
    QString status = edittedForm.getOnlineFormStatus().trimmed();

    bool ret = true;

    queryString = "UPDATE userForms"
                  "SET status = :status "
                  "WHERE formId in ("
                  "SELECT f.url "
                  "FROM forms f"
                  "WHERE f.url = :urlString"
                  ")";
    ret = ret && query.prepare(queryString);

    query.bindValue(":status", status);
    query.bindValue(":urlString", urlString);
    ret = ret && query.exec();

    return ret;
}

QVector <RosterAssignObj> *LocalDatabaseClient::getYourRoster() {
    QSqlQuery query(_db);
    QString queryString;

    QVector <RosterAssignObj> *assignedRosters
            = new QVector <RosterAssignObj> ;
    bool ret = true;

    QString userId = _userMatricNo;

    queryString = "SELECT r.date, r.rosterId, "
                  "u.status, u.userId, u.role "
                  "FROM rosterAssignment r, userRosterAssignment u "
                  "WHERE r.rosterId = u.rosterId "
                  "AND u.userId = :userId "
                  "ORDER BY r.date ";
    ret = ret &&  query.prepare(queryString);
    query.bindValue(":userId", userId);
    query.exec();

    if (!ret) {
        qDebug() << "LocalDatabaseClient -> getYourRoster: "
                 << query.lastError().text();
        return assignedRosters;
    }

    while (query.next()) {
        QSqlRecord record = query.record();
        QString date = record.value("date").toString();
        QString rosterAssignId = record.value("rosterId").toString();
        QString status = record.value("status").toString();
        QString userId = record.value("userId").toString();
        QString role = record.value("role").toString();

        QDate assignedDate = QDate::fromString(date,
                                               FORMAT_DATE);

        RosterAssignObj assignedRoster;
        assignedRoster.setAssignedDate(assignedDate);
        assignedRoster.setRosterAssignId(rosterAssignId);
        assignedRoster.setStatus(status);
        assignedRoster.setUserId(userId);
        assignedRoster.setUserRole(role);
        assignedRosters->append(assignedRoster);
    }

    return assignedRosters;
}

QVector <RosterRequestObj*> *LocalDatabaseClient::getBlockOutReq() {
    QSqlQuery query(_db);
    QString queryString;

    QVector <RosterRequestObj*> *requestRosters
            = new QVector <RosterRequestObj*> ;
    bool ret = true;

    queryString = "SELECT r.submitDate, r.reqDate, r.reason, "
                  "r.status, r.reqId "
                  "FROM rosterBlockOutReq r "
                  "WHERE r.userId = :userId";
    query.prepare(queryString);
    query.bindValue(":userId", _userMatricNo);
    ret = ret &&  query.exec();

    if (!ret) {
        //error code
        assert (false);
        return requestRosters;
    }

    while (query.next()) {
        QSqlRecord record = query.record();
        QString submitString = record.value("submitDate").toString();
        QString reqString = record.value("reqDate").toString();
        QString reason = record.value("reason").toString();
        QString status = record.value("status").toString();
        QString rosterRequestId = record.value("reqId").toString();
        QString userId = record.value("userId").toString();

        QDate submitDate = QDate::fromString(submitString,
                                             FORMAT_DATE);
        QDate reqDate = QDate::fromString(reqString,
                                          FORMAT_DATE);

        RosterRequestObj* requestRoster = new RosterRequestObj();
        requestRoster->setDateRequest(reqDate);
        requestRoster->setDateSubmit(submitDate);
        requestRoster->setReason(reason);
        requestRoster->setStatus(status);
        requestRoster->setRosterRequestId(rosterRequestId);
        requestRoster->setUserId(userId);

        requestRosters->append(requestRoster);
    }

    return requestRosters;
}

//can be get only by roster monster
QVector <RosterRequestObj*> *LocalDatabaseClient::getTeamRequest() {
    //assert (_isRosterMonster);
    QVector <RosterRequestObj*> *requestRosters
            = new QVector <RosterRequestObj*> ;

    if (!_isRosterMonster) {
        return requestRosters;
    }

    QSqlQuery query(_db);
    QString queryString;

    bool ret = true;

    queryString = "SELECT * "
                  "FROM rosterBlockOutReq r "
                  "WHERE r.userId IN ( "
                  "SELECT t1.matricNo "
                  "FROM user t1 "
                  "WHERE t1.rotation = ( "
                  "SELECT t2.rotation "
                  "FROM user t2 "
                  "WHERE t2.matricNo = :userId) "
                  ")";
    query.prepare(queryString);
    query.bindValue(":userId", _userMatricNo);
    ret = ret &&  query.exec();

    if (!ret) {
        //error code
        assert (false);
        return requestRosters;
    }

    while (query.next()) {
        QSqlRecord record = query.record();
        QString submitString = record.value("submitDate").toString();
        QString reqString = record.value("reqDate").toString();
        QString reason = record.value("reason").toString();
        QString status = record.value("status").toString();

        QDate submitDate = QDate::fromString(submitString,
                                             FORMAT_DATE);
        QDate reqDate = QDate::fromString(reqString,
                                          FORMAT_DATE);
        QString userId = record.value("userId").toString();
        QString rosterRequestId = record.value("reqId").toString();

        RosterRequestObj* requestRoster = new RosterRequestObj();
        requestRoster->setDateRequest(reqDate);
        requestRoster->setDateSubmit(submitDate);
        requestRoster->setReason(reason);
        requestRoster->setStatus(status);
        requestRoster->setUserId(userId);
        requestRoster->setRosterRequestId(rosterRequestId);

        requestRosters->append(requestRoster);
    }

    return requestRosters;
}

bool LocalDatabaseClient::saveBlockOutReq(RosterRequestObj* blockOutReq) {
    QSqlQuery query(_db);
    QString queryString;

    bool ret = true;

    QString userId = blockOutReq->getUserId().trimmed();
    QString submitDate = blockOutReq->getDateSubmit().toString(FORMAT_DATE);
    QString reqDate = blockOutReq->getDateRequest().toString(FORMAT_DATE);
    QString reason = blockOutReq->getReason().trimmed();
    QString status = blockOutReq->getStatus().trimmed();
    QString reqId = blockOutReq->getRosterRequestId().trimmed();
    QString role = _userRole;

    queryString = ("INSERT INTO rosterBlockOutReq ("
                   "userId, submitDate, reqDate, "
                   "reason, status, reqId, role) "
                   "VALUES ("
                   ":userId, :submitDate, :reqDate, "
                   ":reason, :status, :reqId, :role)");
    query.prepare(queryString);
    query.bindValue(":userId", userId);
    query.bindValue(":submitDate", submitDate);
    query.bindValue(":reqDate", reqDate);
    query.bindValue(":reason", reason);
    query.bindValue(":status", status);
    query.bindValue(":reqId", reqId);
    query.bindValue(":role", role);

    ret = ret && query.exec();

    if (!ret) {
        qDebug() << "LocalDatabaseClient -> saveBlockOutReq: "
                 << query.lastError().text();
    }

    return ret;
}

bool LocalDatabaseClient::updateBlockOutReq(RosterRequestObj *blockOutReq) {
    QSqlQuery query(_db);
    QString queryString;

    bool ret = true;

    QString userId = blockOutReq->getUserId().trimmed();
    QString status = blockOutReq->getStatus().trimmed();
    QString reqId = blockOutReq->getRosterRequestId().trimmed();

    queryString = "UPDATE rosterBlockOutReq "
                  "SET status = :status "
                  "WHERE userId = :userId "
                  "AND reqId = :reqId ";

    query.prepare(queryString);
    query.bindValue(":status", status);
    query.bindValue(":userId", userId);
    query.bindValue(":reqId", reqId);

    ret = ret && query.exec();

    if (!ret) {
        qDebug() << "LocalDatabaseClient -> updateBlockOutReq : "
                 << query.lastError().text();
    }

    return ret;
}

bool LocalDatabaseClient::saveRosterAssignment(RosterAssignObj assignObj) {
    QSqlQuery query(_db);
    QString queryString;
    bool ret = true;

    QDate assignedDate = assignObj.getAssignedDate();
    QString date = assignedDate.toString(FORMAT_DATE);

    QString rosterAssignId = assignObj.getRosterAssignId().trimmed();


    queryString = "INSERT INTO rosterAssignment ( "
                  "rosterId, date "
                  ") VALUES ( "
                  ":rosterId, :date)";
    ret = ret && query.prepare(queryString);
    query.bindValue(":rosterId", rosterAssignId);
    query.bindValue(":date", date);
    ret = ret && query.exec();

    QString userId = assignObj.getUserId().trimmed();
    QString role = assignObj.getRole().trimmed();
    QString status = assignObj.getStatus().trimmed();

    queryString = "INSERT INTO userRosterAssignment ( "
                  "userId, role, rosterId, status "
                  ") VALUES ( "
                  ":userId, :role, :rosterId, :status)";
    ret = ret && query.prepare(queryString);
    query.bindValue(":userId", userId);
    query.bindValue(":role", role);
    query.bindValue(":rosterId", rosterAssignId);
    query.bindValue(":status", status);
    ret = ret && query.exec();

    if (!ret) {
        qDebug() << "LocalDatabaseClient -> saveRosterAssignment : "
                 << query.lastError().text();
    }

    return ret;
}

QVector <LeaveObj> *LocalDatabaseClient::getLeaveApplications() {
    QSqlQuery query(_db);
    QString queryString;

    QVector <LeaveObj> *leaveObjects
            = new QVector <LeaveObj> ;
    bool check ;

    queryString = "SELECT l.leaveAppId, l.startDate, "
                  "l.endDate, l.status "
                  "FROM leaveApp l "
                  "WHERE l.userId = :userId";
    check = query.prepare(queryString);
    query.bindValue(":userId", _userMatricNo);
    check = check &&  query.exec();

    if (!check) {
        //error code
        qDebug() << "LocalDatabaseClient -> getLeaveApplications : "
                 << query.lastError().text();
        return leaveObjects;
    }

    while (query.next()) {
        QSqlRecord record = query.record();
        QString leaveAppId = record.value("leaveAppId").toString();
        QString startDateString = record.value("startDate").toString();
        QString endDateString = record.value("endDate").toString();
        QString status = record.value("status").toString();

        QDate startDate = QDate::fromString(startDateString,
                                            FORMAT_DATE);
        QDate endDate = QDate::fromString(endDateString,
                                          FORMAT_DATE);

        LeaveObj leaveObject(startDate,
                             endDate,
                             status,
                             leaveAppId);

        leaveObjects->append(leaveObject);

    }

    return leaveObjects;
}

QVector <AssessmentObj> *LocalDatabaseClient::getAssessments() {
    QVector <AssessmentObj> * assessmentObjects =
            new QVector <AssessmentObj>();
    if (_userRole.toLower() == UserDetails::ROLE_TRAINEE) {
        assessmentObjects = getTraineeAssessments();
    } else if (_userRole.toLower() == UserDetails::ROLE_TRAINER) {
        assessmentObjects = getTrainerAssessments();
    } else {
        assert (false);
    }
    return assessmentObjects;

}

bool LocalDatabaseClient::addLeaveApplication(LeaveObj leaveObj) {
    QSqlQuery query(_db);
    QString queryString;

    bool ret;

    QString userId = _userMatricNo;
    QString role = _userRole;
    QString leaveAppId = leaveObj.getLeaveId();
    QString startDate = leaveObj.getStartDate().
            toString(FORMAT_DATE);
    QString endDate = leaveObj.getEndDate().
            toString(FORMAT_DATE);
    QString status = leaveObj.getApplicationStatus();
    queryString = ("INSERT INTO leaveApp ("
                   "leaveAppId, startDate, endDate, "
                   "userId, role, status) "
                   "VALUES ("
                   ":leaveAppId, :startDate, :endDate, "
                   ":userId, :role, :status)");
    ret = query.prepare(queryString);
    query.bindValue(":leaveAppId", leaveAppId);
    query.bindValue(":startDate", startDate);
    query.bindValue(":endDate", endDate);
    query.bindValue(":userId", userId);
    query.bindValue(":role", role);
    query.bindValue(":status", status);

    ret = ret && query.exec();

    if (!ret) {
        qDebug() << "LocalDatabaseClient -> addLeaveApplication : "
                 << query.lastError().text();
    }

    return ret;
}

bool LocalDatabaseClient::updateLeaveApplication(LeaveObj leaveObj) {
    QSqlQuery query(_db);
    QString queryString;

    bool ret;

    QString userId = _userMatricNo;
    QString leaveAppId = leaveObj.getLeaveId().trimmed();
    QString status = leaveObj.getApplicationStatus().trimmed();

    queryString = "UPDATE leaveApp "
                  "SET status = :status "
                  "WHERE leaveAppId = :leaveAppId "
                  "AND userId = :userId ";

    ret = query.prepare(queryString);
    query.bindValue(":status", status);
    query.bindValue(":leaveAppId", leaveAppId);
    query.bindValue(":userId", userId);

    ret = ret && query.exec();

    if (!ret) {
        qDebug() << "LocalDatabaseClient -> updateLeaveApplication : "
                 << query.lastError().text();
    }

    return ret;
}

//only trainer can addNewActivity
bool LocalDatabaseClient::addNewActivity(EventItem *event) {
    assert (_userRole.toLower() == UserDetails::ROLE_TRAINER);

    bool ret;

    QString activityId = event->getActivityId().trimmed();
    QString title = event->getName().trimmed();
    QString eventDate = event->getDate().
            toString(FORMAT_DATE);
    QString startTime = event->getStartTime().
            toString(FORMAT_TIME);
    QString endTime = event->getEndTime().
            toString(FORMAT_TIME);
    QDateTime registrationDeadline = event->getRegistrationDeadline();
    QString registrationDeadlineDate = registrationDeadline.
                                            toString(FORMAT_DATE);
    QString registrationDeadlineTime = registrationDeadline.
                                            toString(FORMAT_TIME);
    int curVacancy = event->getCurrentVacancy();
    int maxVacancy = event->getMaxVacancy();
    QString location = event->getLocation().trimmed();
    QString description = event->getDescription().trimmed();
    QString eventLink = event->getLink().trimmed();
    bool isImportant = event->getIsImportant();
    bool isClosed = event->getIsClosed();
    QString userId = _userMatricNo;
    QString role = _userRole;

    if (title.isEmpty() || description.isEmpty() || location.isEmpty()) {
        return false;
    }

    int important = 0;

    if (isImportant) {
        important = 1;
    } else {
        important = 0;
    }

    QString status = "";
    if (isClosed) {
        status = "close";
    } else {
        status = "open";
    }

    ret = insertIntoActivities(activityId, title,
                               registrationDeadlineTime,
                               registrationDeadlineDate, eventDate,
                               startTime, endTime,
                               maxVacancy, curVacancy,
                               location, description,
                               eventLink, important);

    //trainer's activity
    ret = ret && insertIntoUserActivities(status, role, userId,
                                          activityId);

    //trainee's part
    QVector <QString> trainees = getAllTrainees();
    for (int i = 0; i < trainees.size(); i++) {
        userId = trainees.value(i);
        role = "trainee";
        status = "not registered";
        ret = ret && insertIntoUserActivities(status, role, userId,
                                              activityId);

    }

    if (!ret) {
        qDebug() << "LocalDatabaseClient -> addNewActivity: "
                 << "failed to add something";
    }

    return ret;
}

//only trainer can addAssessment
bool LocalDatabaseClient::addAssessment(AssessmentObj assessmentObj) {
    if (_userRole.toLower() != UserDetails::ROLE_TRAINER) {
        qDebug() << "LocalDatabaseClient -> addAssessment: "
                 << "he/she is not a trainer :( ";
        return false;
    }

    bool ret;

    QString assessmentId = assessmentObj.getAssessId().trimmed();
    QString traineeId = assessmentObj.getTraineeId().trimmed();
    QString role = UserDetails::ROLE_TRAINEE;
    QString title = assessmentObj.getAssessTitle().trimmed();
    QString date = assessmentObj.getAssessDate().toString(FORMAT_DATE);
    QString grade = assessmentObj.getGrade().trimmed();
    QString feedback = assessmentObj.getFeedback().trimmed();
    QVector <QString>* graders = assessmentObj.getGraders();


    ret = insertIntoAssessment(assessmentId, traineeId,
                               role, title, date, grade,
                               feedback);

    for (int i = 0; i < graders->size(); i++) {
        QString trainerId = graders->at(i);
        ret = ret && insertIntoTrainerAssessment(trainerId,
                                                 _userRole,
                                                 assessmentId);

    }

    return ret;
}

bool LocalDatabaseClient::addNote(Notes* notesObj) {
    QSqlQuery query(_db);
    QString queryString;
    bool ret;

    QString noteId = notesObj->getNotesId().trimmed();
    QString noteTitle = notesObj->getNotesTitle().trimmed();
    QString date = notesObj->getNotesDate().toString(FORMAT_DATE);
    QString content = notesObj->getNotesContent().trimmed();
    QString userId = _userMatricNo;
    QString role = _userRole;

    queryString = "INSERT INTO notes ( "
                  "noteId, noteTitle, date, "
                  "content, userId, role) "
                  "VALUES ( "
                  ":noteId, :noteTitle, :date, "
                  ":content, :userId, :role)";
    ret = query.prepare(queryString);

    query.bindValue(":noteId", noteId);
    query.bindValue(":noteTitle", noteTitle);
    query.bindValue(":date", date);
    query.bindValue(":content", content);
    query.bindValue(":userId", userId);
    query.bindValue(":role", role);

    ret = ret && query.exec();

    if (!ret) {
        QString errorMsg = "LocalDatabaseClient -> addNote: "
                           + query.lastError().text();
        qDebug() << errorMsg;
    }

    return ret;
}

bool LocalDatabaseClient::updateNote(Notes* notesObj) {
    QSqlQuery query(_db);
    QString queryString;
    bool ret;

    QString noteId = notesObj->getNotesId().trimmed();
    QString noteTitle = notesObj->getNotesTitle().trimmed();
    QString content = notesObj->getNotesContent().trimmed();

    queryString = "UPDATE notes "
                  "SET noteTitle = :noteTitle, "
                  "content = :content "
                  "WHERE noteId = :noteId";

    ret = query.prepare(queryString);

    query.bindValue(":noteTitle", noteTitle);
    query.bindValue(":content", content);
    query.bindValue(":noteId", noteId);

    ret = ret && query.exec();

    if (!ret) {
        qDebug() << "LocalDatabaseClient -> updateNote: "
                 << query.lastError().text();
    }

    return ret;
}

bool LocalDatabaseClient::isValidUser(QString matricNo, QString role) {
    QSqlQuery query(_db);
    bool isSuccess;

    matricNo = matricNo.trimmed();
    matricNo = matricNo.toLower();

    role = role.trimmed();
    role = role.toLower();

    QString queryString = "SELECT u.matricNo, u.role "
                          "FROM user u "
                          "WHERE LOWER(u.matricNo) LIKE :matricNo "
                          "AND LOWER(u.role) LIKE :role ";

    isSuccess = query.prepare(queryString);
    query.bindValue(":matricNo", matricNo);
    query.bindValue(":role", role);
    isSuccess = isSuccess && query.exec();

    while (query.next()) {
        return true;
    }

    if (!isSuccess) {
        qDebug() << "LocalDatabaseClient -> isValidUser: "
                 << query.lastError().text();
    }

    return false;
}


QString LocalDatabaseClient::getUserName(QString matricNo) {
    QSqlQuery query(_db);
    QString queryString = "SELECT u.name "
                          "FROM user u "
                          "WHERE u.matricNo = :matricNo";
    bool isSuccess = query.prepare(queryString);
    query.bindValue(":matricNo", matricNo);
    isSuccess = isSuccess && query.exec();

    QString userName = "";
    while (query.next()) {
        QSqlRecord record = query.record();
        userName = record.value("name").toString();
        return userName;
    }

    if (!isSuccess) {
        qDebug() << "LocalDatabaseClient -> getUserName: "
                 << query.lastError().text();
    }

    return userName;
}

QString LocalDatabaseClient::generateActivityId() {
    QString activityId = generateId("activities");
    return activityId;
}

QString LocalDatabaseClient::generateLeaveApplicationId() {
    QString id = generateId("leaveApp");
    return id;
}

QString LocalDatabaseClient::generateAssessmentId() {
    QString assessmentId = generateId("assessment");
    return assessmentId;
}

QString LocalDatabaseClient::generateNotesId() {
    QString notesId = generateId("notes");
    return notesId;
}

QString LocalDatabaseClient::generateWorkingHoursId() {
    QString workingHoursId = generateId("workingHours");
    return workingHoursId;
}

QString LocalDatabaseClient::generateProcedureId() {
    QString procedureId = generateId("procedureTable");
    return procedureId;
}

QString LocalDatabaseClient::generateBlockRequestId() {
    QString blockRequestId = generateId("rosterBlockOutReq");
    return blockRequestId;
}

QString LocalDatabaseClient::generateLeaveId() {
    QString leaveId = generateId("leaveApp");
    return leaveId;
}



bool LocalDatabaseClient::syncAllTables() {
    ServerRequestObj newObj;
    newObj.setMatricNo(_userMatricNo);
    newObj.setRole(_userRole);

    QVector <QString> tableNames = TABLE_NAMES;

    for (int i = 0; i < tableNames.size(); i++) {
        if (!syncTable(newObj, tableNames[i])) {
            return false;
        }
    }

    _networkClient->getUserDetails();
    return _networkClient->requestSyncToServer(newObj);
}

//private methods
QVector <AssessmentObj> *LocalDatabaseClient::getTraineeAssessments() {
    QVector <AssessmentObj> * assessmentObjects =
            new QVector <AssessmentObj>();
    if (_userRole.toLower() != UserDetails::ROLE_TRAINEE) {
        qDebug() << "LocalDatabaseClient -> getTraineeAssessments: "
                 << "He/she is not a trainee! :( ";
        return assessmentObjects;
    }


    QSqlQuery query(_db);
    QString queryString;
    bool check;


    queryString = "SELECT assessmentId, title, "
                  "date, grade, feedback "
                  "FROM assessment "
                  "WHERE userId = :userId ";

    check = query.prepare(queryString);

    query.bindValue(":userId", _userMatricNo);

    check = check && query.exec();

    while (query.next()) {
        QSqlRecord record = query.record();
        QString assessmentId = record.value("assessmentId").toString();
        QString title = record.value("title").toString();
        QString dateString = record.value("date").toString();
        QString grade = record.value("grade").toString();
        QString feedback = record.value("feedback").toString();

        QDate date = QDate::fromString(dateString,
                                       FORMAT_DATE);

        QVector <QString>* graders = getGraders(assessmentId);

        AssessmentObj assessmentObj;
        assessmentObj.setAssessId(assessmentId);
        assessmentObj.setAssessTitle(title);
        assessmentObj.setAssessDate(date);
        assessmentObj.setGrade(grade);
        assessmentObj.setFeedback(feedback);
        assessmentObj.setTraineeId(_userMatricNo);
        assessmentObj.setGraders(graders);
        assessmentObjects->append(assessmentObj);
    }


    if (!check) {
        qDebug() << "LocalDatabaseClient -> getTraineeAssessments : "
                 << query.lastError().text();
    }

    return assessmentObjects;
}

QVector <AssessmentObj> *LocalDatabaseClient::getTrainerAssessments() {
    QVector <AssessmentObj> * assessmentObjects =
            new QVector <AssessmentObj>();
    if (_userRole.toLower() != UserDetails::ROLE_TRAINER) {
        qDebug() << "LocalDatabaseClient -> getTraineeAssessments: "
                 << "He/she is not a trainee! :( ";
        return assessmentObjects;
    }


    QSqlQuery query(_db);
    QString queryString;
    bool check;
    QString trainerId = _userMatricNo;

    queryString = "SELECT a.assessmentId, a.title, "
                  "a.date, a.grade, a.feedback, a.userId "
                  "FROM assessment a, trainerAssessment ta "
                  "WHERE a.assessmentId = ta.assessmentId "
                  "AND ta.trainerId = :trainerId ";

    check = query.prepare(queryString);
    query.bindValue(":trainerId", trainerId);
    check = check && query.exec();

    while (query.next()) {
        QSqlRecord record = query.record();
        QString assessmentId = record.value("assessmentId").toString();
        QString title = record.value("title").toString();
        QString dateString = record.value("date").toString();
        QString grade = record.value("grade").toString();
        QString feedback = record.value("feedback").toString();
        QString traineeId = record.value("userId").toString();

        QDate date = QDate::fromString(dateString, FORMAT_DATE);

        QVector <QString>* graders = getGraders(assessmentId);

        AssessmentObj assess;
        assess.setAssessId(assessmentId);
        assess.setAssessTitle(title);
        assess.setGrade(grade);
        assess.setFeedback(feedback);
        assess.setTraineeId(traineeId);
        assess.setAssessDate(date);
        assess.setGraders(graders);

        assessmentObjects->append(assess);

    }


    if (!check) {
        qDebug() << "LocalDatabaseClient -> getTrainerAssessments : "
                 << query.lastError().text();
    }

    return assessmentObjects;


}

QVector <QString>* LocalDatabaseClient::getGraders(QString assessmentId) {
    QVector <QString>* graders = new QVector<QString>();

    QSqlQuery query(_db);
    QString queryString;
    queryString = "SELECT u.name "
                  "FROM trainerAssessment ta, user u "
                  "WHERE ta.assessmentId = :assessId "
                  "AND ta.trainerId = u.matricNo "
                  "AND u.role = ta.role "
                  "AND ta.role LIKE ('trainer') ";
    bool check = query.prepare(queryString);

    query.bindValue(":assessId",
                    assessmentId);

    check = check && query.exec();

    while (query.next()) {
        QSqlRecord record = query.record();
        QString graderName = record.value("name").toString();
        graders->append(graderName);
    }

    if (!check) {
        qDebug() << "LocalDatabaseClient -> getGraders : "
                 << query.lastError().text();
    }

    return graders;
}

bool LocalDatabaseClient::syncTable(ServerRequestObj &obj,
                                    QString tableName) {
    if (tableName.isEmpty()) {
        return false;
    } else if (!_schema.contains(tableName)) {
        return false;
    }

    obj.addTableName(tableName);

    QSqlQuery query(_db);
    QString queryString;
    bool ret;

    queryString = "SELECT * FROM ";
    queryString.append(tableName);
    ret = query.exec(queryString);

    QVector <QString> columns = _schema[tableName];

    while (query.next()) {
        QSqlRecord record = query.record();
        obj.addTableContent(tableName, record, columns);
    }

    if (!ret) {
        qDebug() << "LocalDatabaseClient -> syncTable: "
                 << tableName << " : "
                 << query.lastError().text();
    }

    return ret;
}


void LocalDatabaseClient::setMatricNo(QString matricNo) {
    _userMatricNo = matricNo;
}

void LocalDatabaseClient::setUserRole(QString userRole) {
    _userRole = userRole;
}

void LocalDatabaseClient::setIsRosterMonster(bool isRosterMonster) {
    _isRosterMonster = isRosterMonster;
}

void LocalDatabaseClient::initiateClass() {
    if (_fileName.isEmpty()) {
        _fileName = DATABASE_NAME;
    } else if (!_fileName.endsWith(".sqlite")) {
        _fileName = _fileName + ".sqlite";
    }

    initializeHashTables();
    if (!createConnection()) {
        _db.close();
        if (!removeFile(_fileName)) {
            assert (false);
        }
        qDebug() << "LocalDatabaseClient -> initiateClass : "
                  "Can't create connection to db";
        std::exit(-1);
    }

    _networkClient = NetworkDatabaseClient::getObject();
    connect (_networkClient, SIGNAL(fileIsReady(QString)),
             this, SLOT(handleFileIsReady(QString)));\
    connect (_networkClient, SIGNAL(syncFailed()),
             this, SLOT(handleSyncFailed()));
}

bool LocalDatabaseClient::createConnection() {
    bool hasDb = QFile::exists(DATABASE_NAME);

    if (!hasDb) {
        LocalDatabaseClient::createFile(DATABASE_NAME);
        qDebug() << "file " << DATABASE_NAME << "created";
    }

    _db = QSqlDatabase::addDatabase(DATABASE_TYPE, CONNECTION_NAME);
    _db.setDatabaseName(DATABASE_NAME);

    if (!_db.open()) {
        /*
        QMessageBox::critical(0, qApp->tr("Cannot open database"),
            qApp->tr("Unable to establish a database connection.\n"
                     "This example needs SQLite support. Please read "
                     "the Qt SQL driver documentation for information how "
                     "to build it.\n\n"
                     "Click Cancel to exit."), QMessageBox::Cancel);
                     */
        return false;
    }

    if (!hasDb){
        return createTables() && startupTables();
    }

    return startupTables();
}

QVector <Form*> *LocalDatabaseClient::getForm(QString type) {
    qDebug() << "LocalDatabaseClient -> getForm: "
                "start getting forms...";

    QSqlQuery query(_db);
    QString queryString;
    QVector <Form*> *forms
            = new QVector <Form*> ;
    bool ret = true;

    QString userId = _userMatricNo;
    QString role = _userRole;
    userId = userId.trimmed().toUpper();
    role = role.trimmed().toLower();
    type = type.trimmed().toLower();

    queryString = "SELECT f.name, f.dateReleased, f.deadline, f.url, t.status "
                  "FROM forms f, userForms t "
                  "WHERE LOWER(f.formsId) = LOWER(t.formsId) "
                  "AND LOWER(f.type) = LOWER(:type) "
                  "AND UPPER(t.userId) = UPPER(:userId) "
                  "AND LOWER(t.role) = LOWER(:role) "
                  "GROUP BY f.url ";
    ret = ret && query.prepare(queryString);
    query.bindValue(":type", type);
    query.bindValue(":userId", userId);
    query.bindValue(":role", role);
    ret = ret &&  query.exec();

    while (query.next()) {
        QSqlRecord record = query.record();
        QString formName = record.value("name").toString();
        QString dateReleasedString = record.value("dateReleased").toString();
        QString deadlineString = record.value("deadline").toString();
        QString url = record.value("url").toString();
        QString status = record.value("status").toString();

        QDate dateReleased = QDate::fromString(dateReleasedString,
                                               FORMAT_DATE);
        QDate deadline = QDate::fromString(deadlineString,
                                               FORMAT_DATE);

        Form *curForm = new Form();
        curForm->setFormName(formName);
        curForm->setReleaseDate(dateReleased);
        curForm->setDeadline(deadline);
        curForm->setFormUrl(url);
        curForm->setFormStatus(status);
        curForm->setType(type);
        forms->append(curForm);
    }

    if (!ret) {
        QString errorMsg = "LocalDatabaseClient -> getForm: " +
                           query.lastError().text();
        qDebug() << errorMsg;
    }


    qDebug() << "LocalDatabaseClient -> getForm: "
                "done getting forms...";
    return forms;
}

bool LocalDatabaseClient::startupTables() {
    QSqlQuery query(_db);

    bool ret = true;

    ret = ret &&  query.exec("PRAGMA foreign_keys = ON;");

    return ret;
}

bool LocalDatabaseClient::createTables() {
    QSqlQuery query(_db);

    bool ret = true;

    //1 user
    ret = ret &&  query.exec(
                "CREATE TABLE user ( "
                "matricNo VARCHAR(20) NOT NULL UNIQUE, "
                "name VARCHAR(100) NOT NULL, "
                "personalEmail VARCHAR(100), "
                "nuhEmail VARCHAR(100), "
                "address TEXT, "
                "homePhone VARCHAR(30), "
                "handPhone VARCHAR(30), "
                "rotation VARCHAR(100), "
                "role VARCHAR(30) NOT NULL "
                "CHECK ( "
                "LOWER(role) LIKE ('trainer') OR "
                "LOWER(role) LIKE ('trainee') OR "
                "LOWER(role) LIKE ('admin')), "
                "rosterMonster INT NOT NULL "
                "CHECK (rosterMonster = 0 OR rosterMonster = 1), "
                "PRIMARY KEY (matricNo, role))");



    //2 forms
    ret = ret &&  query.exec(
                "CREATE TABLE forms ( "
                "formsId TEXT NOT NULL, "
                "name TEXT NOT NULL, "
                "dateReleased TEXT NOT NULL, "
                "deadline TEXT NOT NULL, "
                "url TEXT NOT NULL UNIQUE, "
                "type VARCHAR(20) NOT NULL "
                "CHECK (LOWER(type) LIKE ('online') "
                "OR LOWER(type) LIKE ('softcopy')), "
                "PRIMARY KEY (formsId))");



    //3 exams
    ret = ret &&  query.exec(
                "CREATE TABLE exams ( "
                "moduleId TEXT NOT NULL, "
                "moduleName TEXT NOT NULL, "
                "date TEXT NOT NULL, "
                "time TEXT NOT NULL, "
                "location TEXT, "
                "PRIMARY KEY (moduleId))");

    //4 activities
    ret = ret &&  query.exec(
                "CREATE TABLE activities ( "
                "activityId TEXT NOT NULL, "
                "title TEXT NOT NULL, "
                "registrationDeadlineDate TEXT NOT NULL, "
                "registrationDeadlineTime TEXT NOT NULL, "
                "eventDate TEXT NOT NULL, "
                "eventStartTime TEXT NOT NULL, "
                "eventEndTime TEXT NOT NULL, "
                "maxVacancy INT NOT NULL, "
                "curVacancy INT NOT NULL, "
                "location TEXT NOT NULL, "
                "description text NOT NULL, "
                "link TEXT, "
                "important INT NOT NULL "
                "CHECK (important = 1 OR important = 0), "
                "PRIMARY KEY (activityId))"
                );


    //5 assessment
    ret = ret &&  query.exec(
                "CREATE TABLE assessment ( "
                "assessmentId TEXT NOT NULL, "
                "userId VARCHAR(20) NOT NULL, "
                "role VARCHAR(30) NOT NULL "
                "CHECK (LOWER(role) LIKE ('trainee')), "
                "title TEXT NOT NULL, "
                "date TEXT NOT NULL, "
                "grade VARCHAR(20) NOT NULL, "
                "feedback TEXT NOT NULL, "
                "PRIMARY KEY (assessmentId), "
                "FOREIGN KEY (userId, role) REFERENCES user(matricNo, role) "
                "ON DELETE CASCADE)");


    //6 notes
    ret = ret &&  query.exec(
                "CREATE TABLE notes ( "
                "noteId TEXT NOT NULL, "
                "noteTitle TEXT NOT NULL, "
                "date TEXT NOT NULL, "
                "content TEXT NOT NULL, "
                "userId VARCHAR(20) NOT NULL, "
                "role VARCHAR(30) NOT NULL, "
                "FOREIGN KEY(userId, role) REFERENCES user(matricNo, role) "
                "ON DELETE CASCADE, "
                "PRIMARY KEY(userId, role, noteId))");


    //7 patient
    ret = ret &&  query.exec(
                "CREATE TABLE patient ( "
                "patientId VARCHAR (50) NOT NULL, "
                "dob TEXT NOT NULL, "
                "PRIMARY KEY (patientId))");


    //8 procedureTable
    ret = ret &&  query.exec(
                "CREATE TABLE procedureTable ( "
                "procedureId TEXT NOT NULL, "
                "name TEXT NOT NULL, "
                "patientId VARCHAR (50) NOT NULL, "
                "date TEXT, "
                "time TEXT, "
                "remarks TEXT, "
                "outcome TEXT NOT NULL CHECK ( "
                "LOWER(outcome) LIKE ('good') OR "
                "LOWER(outcome) LIKE ('bad') OR "
                "LOWER(outcome) LIKE ('dead') OR "
                "LOWER(outcome) LIKE ('alive')), "
                "PRIMARY KEY (procedureId), "
                "FOREIGN KEY (patientId) REFERENCES patient (patientId) "
                "ON DELETE CASCADE)");


    //9 workingHours
    ret = ret &&  query.exec(
                "CREATE TABLE workingHours ( "
                "userId VARCHAR(20) NOT NULL, "
                "role VARCHAR(30) NOT NULL, "
                "workingHoursId TEXT NOT NULL UNIQUE, "
                "dayIn TEXT NOT NULL UNIQUE, "
                "timeIn TEXT NOT NULL, "
                "dayOut TEXT NOT NULL, "
                "timeOut TEXT NOT NULL, "
                "PRIMARY KEY (workingHoursId, userId, role), "
                "FOREIGN KEY (userId, role) REFERENCES user (matricNo, role) "
                "ON DELETE CASCADE)");


    //10 rosterBlockOutReq
    ret = ret &&  query.exec(
                "CREATE TABLE rosterBlockOutReq ( "
                "reqId TEXT NOT NULL, "
                "submitDate TEXT NOT NULL, "
                "reqDate TEXT NOT NULL, "
                "reason TEXT NOT NULL, "
                "userId VARCHAR(20) NOT NULL, "
                "role VARCHAR(30) NOT NULL, "
                "status VARCHAR(20) NOT NULL "
                "CHECK (LOWER(status) LIKE('pending') "
                "OR lower(status) LIKE ('approved') "
                "OR lower(status) LIKE ('disapproved')), "
                "PRIMARY KEY (userId, role, reqId), "
                "FOREIGN KEY (userId, role) "
                "REFERENCES user(matricNo, role) ON DELETE CASCADE)");


    //11 rosterAssignment
    ret = ret &&  query.exec(
                "CREATE TABLE rosterAssignment ( "
                "rosterId TEXT NOT NULL, "
                "date TEXT NOT NULL, "
                "PRIMARY KEY (rosterId))");


    //12 leaveApp
    ret = ret &&  query.exec(
                "CREATE TABLE leaveApp ( "
                "leaveAppId TEXT NOT NULL, "
                "startDate TEXT NOT NULL, "
                "endDate TEXT NOT NULL, "
                "userId VARCHAR(20) NOT NULL, "
                "role  VARCHAR(30) NOT NULL, "
                "status VARCHAR(45) NOT NULL, "
                "CHECK ( "
                "LOWER(status) LIKE ('pending') "
                "OR LOWER(status) LIKE ('approved') "
                "OR LOWER(status) LIKE ('disapproved')), "
                "FOREIGN KEY(userId, role) REFERENCES "
                "user(matricNo, role) ON DELETE CASCADE, "
                "PRIMARY KEY(userId, leaveAppId))");


    //13 userRosterAssignment
    ret = ret &&  query.exec(
                "CREATE TABLE userRosterAssignment ( "
                "userId VARCHAR(20) NOT NULL, "
                "role VARCHAR(30) NOT NULL, "
                "rosterId TEXT NOT NULL, "
                "status VARCHAR(45) NOT NULL "
                "CHECK(LOWER(status) LIKE ('completed') "
                "OR LOWER(status) LIKE ('not completed') "
                "OR LOWER(status) LIKE ('pending')), "
                "FOREIGN KEY (userId, role) REFERENCES "
                "user(matricNo, role) ON DELETE CASCADE, "
                "FOREIGN KEY (rosterId) REFERENCES "
                "rosterAssignment (rosterId) ON DELETE CASCADE, "
                "PRIMARY KEY (userId, role, rosterId))");



    //14 userProcedure
    ret = ret &&  query.exec(
                "CREATE TABLE userProcedure ( "
                "userId VARCHAR(20) NOT NULL, "
                "role VARCHAR(30) NOT NULL, "
                "procedureId TEXT NOT NULL, "
                "FOREIGN KEY (userId, role) REFERENCES "
                "user (matricNo, role) ON DELETE CASCADE, "
                "FOREIGN KEY (procedureId) REFERENCES "
                "procedureTable (procedureId) ON DELETE CASCADE, "
                "PRIMARY KEY (userId, procedureId))");


    //15 userForms
    ret = ret &&  query.exec(
                "CREATE TABLE userForms ( "
                "userId VARCHAR(20) NOT NULL, "
                "role VARCHAR(30) NOT NULL, "
                "formsId TEXT NOT NULL, "
                "status VARCHAR(45) NOT NULL "
                "CHECK(LOWER(status) LIKE ('submitted') "
                "OR LOWER(status) LIKE ('not submitted')), "
                "FOREIGN KEY (userId, role) REFERENCES "
                "user(matricNo, role) ON DELETE CASCADE, "
                "FOREIGN KEY (formsId)REFERENCES "
                "forms (formsId) ON DELETE CASCADE, "
                "PRIMARY KEY (userId, formsId))");



    //16 traineeExams
    ret = ret &&  query.exec(
                "CREATE TABLE traineeExams ( "
                "traineeId VARCHAR(20) NOT NULL, "
                "role VARCHAR(30) NOT NULL "
                "CHECK (LOWER(role) LIKE ('trainee')), "
                "moduleId TEXT NOT NULL, "
                "seatNo VARCHAR(45) NOT NULL, "
                "FOREIGN KEY (traineeId, role) REFERENCES "
                "user (matricNo, role) ON DELETE CASCADE, "
                "FOREIGN KEY (moduleId) REFERENCES "
                "exams (moduleId) ON DELETE CASCADE, "
                "PRIMARY KEY (traineeId, moduleId))");


    //17 userActivities
    ret = ret &&  query.exec(
                "CREATE TABLE userActivities ( "
                "userId VARCHAR(20) NOT NULL, "
                "role VARCHAR(30) NOT NULL, "
                "activityId TEXT NOT NULL, "
                "status VARCHAR (45) NOT NULL "
                "CHECK ( "
                "(LOWER(role) LIKE ('trainee') AND "
                "(LOWER(status) LIKE ('registered') OR "
                "LOWER(status) LIKE ('not registered'))) OR "
                "(LOWER(role) LIKE ('trainer') AND "
                "(LOWER(status) LIKE ('open') OR "
                "LOWER(status) LIKE ('close')))), "
                "FOREIGN KEY (userId, role) REFERENCES "
                "user (matricNo, role) ON DELETE CASCADE, "
                "FOREIGN KEY (activityId) REFERENCES "
                "activities (activityId)  ON DELETE CASCADE, "
                "PRIMARY KEY (userId, activityId))");


    //18 trainerAssessment
    ret = ret &&  query.exec(
                "CREATE TABLE trainerAssessment ( "
                "trainerId VARCHAR(20) NOT NULL, "
                "role VARCHAR(30) NOT NULL "
                "CHECK(LOWER(role) LIKE ('trainer')) , "
                "assessmentId TEXT NOT NULL, "
                "FOREIGN KEY (trainerId, role) REFERENCES "
                "user (matricNo, role) ON DELETE CASCADE, "
                "FOREIGN KEY (assessmentId) REFERENCES "
                "assessment (assessmentId) ON DELETE CASCADE, "
                "PRIMARY KEY (trainerId, assessmentId))");



    return ret;
}

void LocalDatabaseClient::initializeHashTables() {
    qDebug() << "LocalDatabaseClient -> initializeHashTables: "
                "start setting up the hashTables...";
    qDebug() << "start setting up schema and numOfPrimaryKey";

    QString table = "user";
    QVector <QString> user = QVector <QString>()
                             << "matricNo" << "role" << "name"
                             << "nuhEmail" << "address" << "homePhone"
                             << "handPhone" << "rotation" << "personalEmail"
                             << "rosterMonster";
    _schema[table] = user;
    _numOfPrimaryKey[table] = 2;


    table = "forms";
    QVector <QString> forms = QVector <QString>()
                              << "formsId" << "name" << "dateReleased"
                              << "deadline" << "url" << "type";
    _schema[table] = forms;
    _numOfPrimaryKey[table] = 1;


    table = "exams";
    QVector <QString> exams = QVector <QString>()
                              << "moduleId" << "moduleName" << "date"
                              << "time" << "location";
    _schema[table] = exams;
    _numOfPrimaryKey[table] = 2;


    table = "activities";
    QVector <QString> activities = QVector <QString>()
                                   << "activityId" << "title"
                                   << "registrationDeadlineDate"
                                   << "registrationDeadlineTime"
                                   << "eventDate" << "eventStartTime"
                                   << "eventEndTime" << "maxVacancy"
                                   << "curVacancy" << "location"
                                   << "description" << "link"
                                   << "important";
    _schema[table] = activities;
    _numOfPrimaryKey[table] = 1;


    table = "assessment";
    QVector <QString> assessment = QVector <QString>()
                                   << "assessmentId" << "userId" << "role"
                                   << "title" << "date" << "grade"
                                   << "feedback";
    _schema[table] = assessment;
    _numOfPrimaryKey[table] = 1;


    table = "notes";
    QVector <QString> notes = QVector <QString>()
                              << "noteId" << "userId" << "role"
                              << "noteTitle" << "date" << "content";
    _schema[table] = notes;
    _numOfPrimaryKey[table] = 3;


    table = "patient";
    QVector <QString> patient = QVector <QString>()
                                << "patientId" << "dob";
    _schema[table] = patient;
    _numOfPrimaryKey[table] = 1;


    table = "procedureTable";
    QVector <QString> procedureTable = QVector <QString>()
                                       << "procedureId" << "name"
                                       << "patientId" << "date"
                                       << "time" << "remarks"
                                       << "outcome";
    _schema[table] = procedureTable;
    _numOfPrimaryKey[table] = 1;



    table = "workingHours";
    QVector <QString> workingHours = QVector <QString>()
                                     << "userId" << "role" << "workingHoursId"
                                     << "dayIn" << "timeIn" << "dayOut"
                                     << "timeOut";
    _schema[table] = workingHours;
    _numOfPrimaryKey[table] = 3;



    table = "rosterBlockOutReq";
    QVector <QString> rosterBlockOutReq = QVector <QString>()
                                          << "reqId" << "userId" << "role"
                                          << "submitDate"
                                          << "reqDate" << "reason"
                                          << "status";
    _schema[table] = rosterBlockOutReq;
    _numOfPrimaryKey[table] = 3;


    table = "rosterAssignment";
    QVector <QString> rosterAssignment = QVector <QString>()
                                         << "rosterId" << "date";
    _schema[table] = rosterAssignment;
    _numOfPrimaryKey[table] = 1;




    table = "leaveApp";
    QVector <QString> leaveApp = QVector <QString>()
                                 << "leaveAppId" << "userId" << "startDate"
                                 << "endDate" << "role" << "status";
    _schema[table] = leaveApp;
    _numOfPrimaryKey[table] = 2;


    table = "userRosterAssignment";
    QVector <QString> userRosterAssignment =
            QVector <QString>() << "userId" << "role" << "rosterId"
                                << "status";
    _schema[table] = userRosterAssignment;
    _numOfPrimaryKey[table] = 3;



    table = "userProcedure";
    QVector <QString> userProcedure = QVector <QString>()
                                      << "userId" << "procedureId" << "role";

    _schema[table] = userProcedure;
    _numOfPrimaryKey[table] = 2;


    table = "userForms";
    QVector <QString> userForms = QVector <QString>()
                                  << "userId" << "formsId" << "role"
                                  << "status";
    _schema[table] = userForms;
    _numOfPrimaryKey[table] = 2;


    table = "traineeExams";
    QVector <QString> traineeExams = QVector <QString>()
                                     << "traineeId" << "moduleId" << "role"
                                     << "seatNo";
    _schema[table] = traineeExams;
    _numOfPrimaryKey[table] = 2;



    table = "userActivities";
    QVector <QString> userActivities = QVector <QString>()
                                       << "userId" << "activityId" << "role"
                                       << "status";
    _schema[table] = userActivities;
    _numOfPrimaryKey[table] = 2;


    table = "trainerAssessment";
    QVector <QString> trainerAssessment = QVector <QString>()
                                          << "trainerId" << "assessmentId"
                                          << "role";
    _schema[table] = trainerAssessment;
    _numOfPrimaryKey[table] = 2;

    qDebug() << "schema size: " << _schema.size();
    qDebug() << "numOfPrimaryKey size: " << _numOfPrimaryKey.size();
    qDebug() << "done setting up schema and numOfPrimaryKey";
    qDebug() << "start setting up isPrimaryKey";

    for (int i = 0; i < TABLE_NAMES.size(); i++) {
        QString table = TABLE_NAMES[i];
        int numOfPrimaryKey = _numOfPrimaryKey[table];
        for (int j = 0; j < numOfPrimaryKey; j++) {
            QString key = _schema[table][j];
            _isPrimaryKey[table][key] = true;
        }

        for (int j = numOfPrimaryKey; j < _schema[table].size(); j++) {
            QString key = _schema[table][j];
            _isPrimaryKey[table][key] = false;
        }
    }

    qDebug() << "done setting up isPrimaryKey";
    qDebug() << "LocalDatabaseClient -> initializeHashTables: "
                "done setting up the hashTables...";
}

bool LocalDatabaseClient::createFile(QString name) {
    QFile file(name);
    if (file.open(QIODevice::ReadWrite)) {
        file.close();
        return true;
    }

    qDebug() << "for some reason, file " << name
             << " not created here\n";

    return false;
}

bool LocalDatabaseClient::removeFile(QString name) {
    bool hasFile = QFile::exists(DATABASE_NAME);

    if (!hasFile) {
        return true;
    }

    QFile file(name);
    bool ret = file.remove();

    if (!ret) {
        qDebug() << "LocalDatabaseClient -> removeFile: "
                 << file.errorString();
    }

    return ret;
}

bool LocalDatabaseClient::insertIntoUser(QString matricNo,
                                        QString name,
                                        QString role,
                                        int rosterMonster,
                                        QString personalEmail,
                                        QString nuhEmail,
                                        QString address,
                                        QString homePhone,
                                        QString handPhone,
                                        QString rotation) {
    QSqlQuery query(_db);
    QString queryString = "INSERT INTO user "
                          "(matricNo, name, role, rosterMonster, "
                          "personalEmail, nuhEmail, address, "
                          "homePhone, handPhone, rotation) "
                          "VALUES "
                          "(:matricNo, :name, :role, :rosterMonster, "
                          ":personalEmail, :nuhEmail, :address, "
                          ":homePhone, :handPhone, :rotation) ";


    bool isSuccess;


    isSuccess = query.prepare(queryString);

    query.bindValue(":matricNo", matricNo);
    query.bindValue(":name", name);
    query.bindValue(":role", role);
    query.bindValue(":rosterMonster", rosterMonster);
    query.bindValue(":personalEmail", personalEmail);
    query.bindValue(":nuhEmail", nuhEmail);
    query.bindValue(":address", address);
    query.bindValue(":homePhone", homePhone);
    query.bindValue(":handPhone", handPhone);
    query.bindValue(":rotation", rotation);

    isSuccess = isSuccess && query.exec();

    if (!isSuccess) {
        QString errorMsg = "LocalDatabaseClient -> insertIntoUser: " +
                           query.lastError().text();
        qDebug() << errorMsg;
    }


    return isSuccess;
}

bool LocalDatabaseClient::insertIntoActivities(QString activityId,
                                               QString title,
                                               QString registrationDeadlineTime,
                                               QString registrationDeadlineDate,
                                               QString eventDate,
                                               QString startTime,
                                               QString endTime,
                                               int maxVacancy,
                                               int curVacancy,
                                               QString location,
                                               QString description,
                                               QString eventLink,
                                               int important) {
    bool ret = true;
    QSqlQuery query(_db);
    QString queryString;

    queryString = "INSERT INTO activities ( "
                  "activityId, title, "
                  "registrationDeadlineTime, registrationDeadlineDate, "
                  "eventDate, eventStartTime, eventEndTime, "
                  "maxVacancy, curVacancy, "
                  "location, description, link, important) "
                  "VALUES ("
                  ":activityId, :title, "
                  ":registrationDeadlineTime, :registrationDeadlineDate, "
                  ":eventDate, :eventStartTime, :eventEndTime, "
                  ":maxVacancy, :curVacancy, "
                  ":location, :description, :link, :important) ";

    ret = ret && query.prepare(queryString);

    query.bindValue(":activityId", activityId);
    query.bindValue(":title", title);
    query.bindValue(":registrationDeadlineTime", registrationDeadlineTime);
    query.bindValue(":registrationDeadlineDate", registrationDeadlineDate);
    query.bindValue(":eventDate", eventDate);
    query.bindValue(":eventStartTime", startTime);
    query.bindValue(":eventEndTime", endTime);
    query.bindValue(":maxVacancy", maxVacancy);
    query.bindValue(":curVacancy", curVacancy);
    query.bindValue(":location", location);
    query.bindValue(":description", description);
    query.bindValue(":link", eventLink);
    query.bindValue(":important", important);

    ret = ret && query.exec();

    if (!ret) {
        qDebug() << "LocalDatabaseClient -> insertIntoActivities : "
                 << query.lastError().text();
    }

    return ret;
}

bool LocalDatabaseClient::insertIntoUserActivities(QString status,
                                                   QString role,
                                                   QString userId,
                                                   QString activityId) {
    bool ret;
    QSqlQuery query(_db);
    QString queryString;

    queryString = "INSERT INTO userActivities ("
                  "status, userId, role, activityId) "
                  "VALUES ("
                  ":status, :userId, :role, :activityId)";

    ret = query.prepare(queryString);

    query.bindValue(":status", status);
    query.bindValue(":userId", userId);
    query.bindValue(":role", role);
    query.bindValue(":activityId", activityId);

    ret = ret && query.exec();

    if (!ret) {
        qDebug() << "LocalDatabaseClient -> insertIntoUserActivities : "
                 << query.lastError().text();
    }

    return ret;
}

bool LocalDatabaseClient::insertIntoAssessment(QString assessmentId,
                                               QString traineeId,
                                               QString role,
                                               QString title,
                                               QString date,
                                               QString grade,
                                               QString feedback) {
    QSqlQuery query(_db);
    QString queryString;
    bool ret;

    queryString = "INSERT INTO assessment ( "
                  "assessmentId, userId, role, "
                  "title, date, grade, "
                  "feedback ) "
                  "VALUES ( "
                  ":assessmentId, :userId, :role, "
                  ":title, :date, :grade, "
                  ":feedback)";

    ret = query.prepare(queryString);

    query.bindValue(":assessmentId", assessmentId);
    query.bindValue(":userId", traineeId);
    query.bindValue(":role", role);
    query.bindValue(":title", title);
    query.bindValue(":date", date);
    query.bindValue(":grade", grade);
    query.bindValue(":feedback", feedback);

    ret = ret && query.exec();

    if (!ret) {
        qDebug() << "LocalDatabaseClient -> insertIntoAssessment : "
                 << query.lastError().text();
    }

    return ret;
}

bool LocalDatabaseClient::insertIntoTrainerAssessment(QString trainerId,
                                                      QString role,
                                                      QString assessmentId) {
    QSqlQuery query(_db);
    QString queryString;
    bool ret;

    queryString = "INSERT INTO trainerAssessment ( "
                  "trainerId, role, assessmentId) "
                  "VALUES ("
                  ":trainerId, :role, :assessmentId)";

    ret = query.prepare(queryString);

    query.bindValue(":trainerId", trainerId);
    query.bindValue(":role", role);
    query.bindValue(":assessmentId", assessmentId);

    ret = ret && query.exec();


    if (!ret) {
        qDebug() << "LocalDatabaseClient -> insertIntoTrainerAssessment : "
                 << query.lastError().text();
    }


    return ret;
}

bool LocalDatabaseClient::updateUserActivityStatus(EventItem* activity) {

    bool ret = true;

    QString userId = _userMatricNo;
    QString role = _userRole;
    QString activityStatus = "";
    QString activityId = activity->getActivityId();

    if (_userRole.toLower() == UserDetails::ROLE_TRAINEE) {
        int curVacancy = activity->getCurrentVacancy();

        bool isRegistered = activity->getIsRegistered();
        if (isRegistered) {
            activityStatus = "Registered";
            if (curVacancy <= 0) {
                return false;
            }
            curVacancy--;
        } else {

            activityStatus = "Not Registered";
            int maxVacancy = activity->getMaxVacancy();

            if (curVacancy >= maxVacancy) {
                return false;
            }
            curVacancy++;
        }
        ret = updateActivityCurVacancy(curVacancy, activityId);


    } else if (_userRole.toLower() == UserDetails::ROLE_TRAINER) {
        bool isClosed = activity->getIsClosed();
        if (!isClosed) {
            activityStatus = "open";
        } else {
            activityStatus = "close";
        }
    } else {
        return false;
    }

    ret = ret && updateUserActivityStatusOnly(userId,
                                              role,
                                              activityStatus,
                                              activityId);

    return ret;
}



bool LocalDatabaseClient::isMatricNoUnique(QString matricNo) {
    matricNo = matricNo.toLower().trimmed();

    QSqlQuery query(_db);
    QString queryString =   "SELECT * "
                            "FROM user u "
                            "WHERE LOWER(u.matricNo) LIKE :matricNo ";
    bool isSuccess = query.prepare(queryString);
    query.bindValue(":matricNo", matricNo);

    isSuccess = isSuccess && query.exec();

    while (query.next()) {
        return false;
    }

    if (!isSuccess) {
        QString errorMsg = "LocalDatabase -> isMatricNoUnique: " +
                           query.lastError().text();
        qDebug() << errorMsg;
    }

    return true;
}


int LocalDatabaseClient::getTableSize(QString tableName) {
    bool res;
    QSqlQuery q(_db);

    //prepare and bind value doesn't work for this one
    //but it is fine as this method is used only in this class
    //and it is private method so does not cause any security threat
    //(or we can't think of 1)
    QString queryString = "SELECT * FROM ";
    queryString.append(tableName);

    qDebug() << "LocalDatabaseClient -> getTableSize : "
                "getting table size of "
             << tableName;

    res = q.exec(queryString);

    if (!res) {
        qDebug() << "LocalDatabaseClient -> getTableSize : "
                 << q.lastError().text();
        return -1;
    }

    res = q.last();

    if (!res) {
        qDebug() << "LocalDatabaseClient -> getTableSize : "
                 << q.lastError().text();
        return -1;
    }

    int size = q.at() + 1;

    //assert (size >= 0);

    qDebug() << "LocalDatabaseClient -> getTableSize : "
                "successfully get table size of "
             << tableName;

    return size;
}

QString LocalDatabaseClient::generateId(QString tableName) {
    int index = getTableSize(tableName) + 1;
    QDateTime now = QDateTime::currentDateTime();
    QString dateTimeFormat = FORMAT_DATE + FORMAT_TIME + "ss";
    QString dateTime = now.toString(dateTimeFormat);
    QString matricNo = _userMatricNo;

    QString id = matricNo + tableName + dateTime + QString::number(index);
    return id;
}

QVector <QString> LocalDatabaseClient::getAllTrainees() {
    QVector <QString> trainees;

    QSqlQuery query(_db);
    QString role = "trainee";
    QString queryString = "SELECT u.matricNo "
                          "FROM user u "
                          "WHERE u.role = :role ";
    bool check = query.prepare(queryString);
    query.bindValue(":role", role);
    check = check && query.exec();

    while (query.next()) {
        QSqlRecord record = query.record();
        QString matricNo = record.value("matricNo").toString();
        trainees.append(matricNo);
    }

    if (!check) {
        qDebug() << "LocalDatabaseClient -> getAllTrainees: "
                 << query.lastError().text();
    }

    return trainees;
}

bool LocalDatabaseClient::isJsonValueExist(QJsonValue value) {
    return !value.isNull() && !value.isUndefined();
}

bool LocalDatabaseClient::isJsonValueExist(QJsonObject json, QString param) {
    return isJsonValueExist(json[param]);
}

bool LocalDatabaseClient::extractDataFrom(QString table, QJsonObject data) {
    qDebug() << "LocalDatabaseClient -> extractDataFrom: "
                "start extracting data ";

    if (table.isEmpty() || table.trimmed().isEmpty()) {
        qDebug() << "LocalDatabaseClient -> extractDataFrom: "
                    "QString table is empty!";
        return false;
    } else if (!_schema.contains(table)) {
        qDebug() << "LocalDatabaseClient -> extractDataFrom: "
                    "_schema doesn't contain QString table";
        return false;
    } else if (data.isEmpty()) {
        qDebug() << "LocalDatabaseClient -> extractDataFrom: "
                    "QJsonObject data is empty!";
        return true;
    }
    qDebug() << "QString table is " << table;

    for (int i = 0; i < _schema[table].size(); i++) {
        if (!isJsonValueExist(data, _schema[table][i])) {
            qDebug() << "Value " << _schema[table][i] << " does not exist";
            return false;
        }
    }


    bool isSuccess;
    table = table.trimmed();
    qDebug() << "tablename: " << table;
/*
    if (table == "activities") {
        isSuccess = extractDataFromActivity(data);
    } else if (table == "useractivities") {
        isSuccess = extractDataFromUserActivity(data);
    } else if (table == "user") {
        isSuccess = extractDataFromUser(data);
    } else {
        isSuccess = extractDataFromAnyTable(table, data);
    }*/
    isSuccess = extractDataFromAnyTable(table, data);

    qDebug() << "LocalDatabaseClient -> extractDataFrom: "
                "done extracting data, status: "
             << isSuccess;

    return isSuccess;
}

//expected only valid input
bool LocalDatabaseClient::extractDataFromAnyTable(QString tableName,
                                                  QJsonObject data) {
    if (data.isEmpty()) {
        return false;
    } else if (tableName.isEmpty()) {
        return false;
    } else if (!_schema.contains(tableName)) {
        return false;
    }

    qDebug() << "LocalDatabaseClient -> extractDataFromAnyTable: "
             << "table: " << tableName;

    bool isSuccess = insertIntoAnyTable(tableName,
                                   data.toVariantMap());
    if (!isSuccess) {
        isSuccess = updateAnyTable(tableName,
                                   data.toVariantMap());
    }

    return isSuccess;
}

bool LocalDatabaseClient::updateAnyTable(QString tableName,
                                         QVariantMap data) {
    if (tableName.isEmpty()) {
        return false;
    } else if (data.isEmpty()) {
        return false;
    } else if (!_schema.contains(tableName)) {
        return false;
    }

    qDebug() << "LocalDatabaseClient -> updateAnyTable: "
                "start updating " << tableName;

    QSqlQuery query(_db);
    QString queryString = "UPDATE " + tableName +
                          " SET ";

    QString commaSpace = ", ";

    for (int i = 0; i < _schema[tableName].size(); i++) {
        QString attribute = _schema[tableName].at(i);

        if (_isPrimaryKey[tableName][attribute]) {
            continue;
        }

        queryString += attribute + " = :" + attribute + commaSpace;
    }
    queryString.chop(commaSpace.length());
    queryString += " WHERE ";

    QString andString = " AND ";
    for (int i = 0; i < _schema[tableName].size(); i++) {
        QString attribute = _schema[tableName].at(i);

        if (!_isPrimaryKey[tableName][attribute]) {
            continue;
        }
        queryString += attribute + " = :" + attribute + andString;

    }
    queryString.chop(andString.length());

    qDebug() << "queryString: " << queryString;

    bool isSuccess;


    isSuccess = query.prepare(queryString);

    for (int i = 0; i < _schema[tableName].size(); i++) {
        QString key = _schema[tableName].at(i);
        QString loc = ":" + key;
        QString value = data[key].toString().trimmed();
        bool ok = false;
        int valueInt = value.toInt(&ok);

        if (ok) {
            query.bindValue(loc, valueInt);
        } else {
            query.bindValue(loc, value);
        }
    }

    isSuccess = isSuccess && query.exec();

    if (!isSuccess) {
        QString errorMsg = "LocalDatabaseClient -> updateAnyTable: " +
                           query.lastError().text();
        qDebug() << errorMsg;
    }

    qDebug() << "LocalDatabaseClient -> updateAnyTable: "
                "done updating " << tableName
             << " status: " << isSuccess;

    return isSuccess;
}

bool LocalDatabaseClient::insertIntoAnyTable(QString tableName,
                                             QVariantMap data) {

    if (tableName.isEmpty()) {
        qDebug() << "LocalDatabaseClient -> insertIntoAnyTable: "
                    "tableName is empty!";
        return false;
    } else if (!_schema.contains(tableName)) {
        qDebug() << "LocalDatabaseClient -> insertIntoAnyTable: "
                 << tableName << " not exist!";
        return false;
    } else if (data.isEmpty()) {
        qDebug() << "LocalDatabaseClient -> insertIntoAnyTable: "
                    "data is empty!";
        return true;
    }
    qDebug() << "LocalDatabaseClient -> insertIntoAnyTable: "
                "inserting into " << tableName;

    QSqlQuery query(_db);
    QString queryString = "INSERT INTO " + tableName +
                          "( ";

    QString commaSpace = ", ";
    for (int i = 0; i < _schema[tableName].size(); i++) {
        queryString += _schema[tableName].at(i);
        queryString += commaSpace;
    }
    queryString.chop(commaSpace.length());
    queryString += " ) VALUES ( ";
    for (int i = 0; i < _schema[tableName].size(); i++) {
        queryString += ":" + _schema[tableName].at(i);
        queryString += commaSpace;
    }
    queryString.chop(commaSpace.length());
    queryString += ")";

    qDebug() << "queryString: " << queryString;

    bool isSuccess;


    isSuccess = query.prepare(queryString);

    for (int i = 0; i < _schema[tableName].size(); i++) {
        QString key = _schema[tableName].at(i);
        QString loc = ":" + key;
        QString value = data[key].toString();
        bool ok = false;
        int valueInt = value.toInt(&ok);

        if (ok) {
            query.bindValue(loc, valueInt);
        } else {
            query.bindValue(loc, value);
        }
    }

    isSuccess = isSuccess && query.exec();

    if (!isSuccess) {
        QString errorMsg = "LocalDatabaseClient -> insertIntoAnyTable: " +
                           query.lastError().text();
        qDebug() << errorMsg;
    }

    qDebug() << "LocalDatabaseClient -> insertIntoAnyTable: "
                "done inserting into " << tableName
             << " status: " << isSuccess;

    return isSuccess;
}

bool LocalDatabaseClient::extractDataFromUser(QJsonObject data) {
    if (data.isEmpty()) {
        return false;
    }

    QString matricNo = data["matricNo"].toString();
    QString name = data["name"].toString();
    QString personalEmail = data["personalEmail"].toString();
    QString nuhEmail = data["nuhEmail"].toString();
    QString address = data["address"].toString();
    QString homePhone = data["homePhone"].toString();
    QString handPhone = data["handPhone"].toString();
    QString rotation = data["rotation"].toString();
    QString role = data["role"].toString();
    int rosterMonster = data["rosterMonster"].toInt();

    bool isSuccess = insertIntoUser(matricNo,
                                    name,
                                    role,
                                    rosterMonster,
                                    personalEmail,
                                    nuhEmail,
                                    address,
                                    homePhone,
                                    handPhone,
                                    rotation);

    if (!isSuccess) {
        isSuccess = updateUserOnly(matricNo,
                                   name,
                                   role,
                                   rosterMonster,
                                   personalEmail,
                                   nuhEmail,
                                   address,
                                   homePhone,
                                   handPhone,
                                   rotation);
    }

    return isSuccess;
}

//expected only valid input
bool LocalDatabaseClient::extractDataFromUserActivity(QJsonObject data) {
    if (data.isEmpty()) {
        return false;
    }

    QString status = data["status"].toString();
    QString role = data["role"].toString();
    QString userId = data["userId"].toString();
    QString activityId = data["activityId"].toString();

    bool isSuccess = insertIntoUserActivities(status,
                                              role,
                                              userId,
                                              activityId);

    if (!isSuccess) {
        isSuccess = updateUserActivityStatusOnly(userId,
                                                 role,
                                                 status,
                                                 activityId);
    }

    return isSuccess;
}

//expected only valid input
bool LocalDatabaseClient::extractDataFromActivity(QJsonObject data) {
    if (data.isEmpty()) {
        return false;
    }

    QString activityId = data["activityId"].toString();
    QString title = data["title"].toString();

    QString registrationDeadlineTime =
            data["registrationDeadlineTime"].toString();
    if (!isValidTime(registrationDeadlineTime)) {
        qDebug() << "file corrupted: registrationDeadlineTime: "
                 << registrationDeadlineTime;
        return false;
    }

    QString registrationDeadlineDate =
            data["registrationDeadlineDate"].toString();

    if (!isValidDate(registrationDeadlineDate)) {
        qDebug() << "file corrupted: registrationDeadlineDate: "
                 << registrationDeadlineDate;
        return false;
    }

    QString eventDate = data["eventDate"].toString();

    if (!isValidDate(eventDate)) {
        qDebug() << "file corrupted: eventDate: "
                 << eventDate;
        return false;
    }

    QString eventStartTime = data["eventStartTime"].toString();

    if (!isValidTime(eventStartTime)) {
        qDebug() << "file corrupted: eventStartTime: "
                 << eventStartTime;
        return false;
    }

    QString eventEndTime = data["eventEndTime"].toString();
    if (!isValidTime(eventEndTime)) {
        qDebug() << "file corrupted: eventEndTime: "
                 << eventEndTime;
        return false;
    }

    int maxVacancy = data["maxVacancy"].toInt();
    int curVacancy = data["curVacancy"].toInt();
    QString location = data["location"].toString();
    QString description = data["description"].toString();
    QString link = data["link"].toString();


    int important = data["important"].toInt();

    if (important != 0 && important != 1) {
        qDebug() << "file corrupted: important: " << important;
        return false;
    }

    bool isSuccess = insertIntoActivities(activityId,
                                          title,
                                          registrationDeadlineTime,
                                          registrationDeadlineDate,
                                          eventDate,
                                          eventStartTime,
                                          eventEndTime,
                                          maxVacancy,
                                          curVacancy,
                                          location,
                                          description,
                                          link,
                                          important);
    if (!isSuccess) {
        isSuccess = updateActivityTableOnly(activityId,
                                            title,
                                            registrationDeadlineTime,
                                            registrationDeadlineDate,
                                            eventDate,
                                            eventStartTime,
                                            eventEndTime,
                                            maxVacancy,
                                            curVacancy,
                                            location,
                                            description,
                                            link,
                                            important);
    }

    return isSuccess;

}

bool LocalDatabaseClient::updateActivityTableOnly(QString activityId,
                                                  QString title,
                                                  QString registrationDeadlineTime,
                                                  QString registrationDeadlineDate,
                                                  QString eventDate,
                                                  QString startTime,
                                                  QString endTime,
                                                  int maxVacancy,
                                                  int curVacancy,
                                                  QString location,
                                                  QString description,
                                                  QString eventLink,
                                                  int important) {
    QSqlQuery query(_db);
    QString queryString;
    bool isSuccess = true;
    queryString = "UPDATE activities "
                  "SET title = :title, "
                  "registrationDeadlineTime = :registrationDeadlineTime, "
                  "registrationDeadlineDate = :registrationDeadlineDate, "
                  "eventDate = :eventDate, "
                  "eventStartTime = :eventStartTime, "
                  "eventEndTime = :eventEndTime, "
                  "maxVacancy = :maxVacancy, "
                  "curVacancy = :curVacancy, "
                  "location = :location, "
                  "description = :description, "
                  "link = :link, "
                  "important = :important "
                  "WHERE activityId = :activityId ";
    isSuccess = isSuccess && query.prepare(queryString);

    query.bindValue(":title", title);
    query.bindValue(":registrationDeadlineTime", registrationDeadlineTime);
    query.bindValue(":registrationDeadlineDate", registrationDeadlineDate);
    query.bindValue(":eventDate", eventDate);
    query.bindValue(":eventStartTime", startTime);
    query.bindValue(":eventEndTime", endTime);
    query.bindValue(":maxVacancy", maxVacancy);
    query.bindValue(":curVacancy", curVacancy);
    query.bindValue(":location", location);
    query.bindValue(":description", description);
    query.bindValue(":link", eventLink);
    query.bindValue(":important", important);
    query.bindValue(":activityId", activityId);

    isSuccess = isSuccess && query.exec();

    if (!isSuccess) {
        qDebug() << "LocalDatabaseClient -> updateActivity: "
                 << query.lastError().text();
    }
    return isSuccess;
}

bool LocalDatabaseClient::updateUserActivityStatusOnly(QString userId,
                                                       QString role,
                                                       QString activityStatus,
                                                       QString activityId) {

    QSqlQuery query(_db);
    QString queryString;
    bool ret = true;

    queryString = "UPDATE userActivities "
                  "SET status = :status "
                  "WHERE activityId = :activityId "
                  "AND userId = :userId "
                  "AND role = :role";
    ret = ret &&  query.prepare(queryString);

    query.bindValue(":status", activityStatus);
    query.bindValue(":activityId", activityId);
    query.bindValue(":userId", userId);
    query.bindValue(":role", role);

    ret = ret && query.exec();

    if (!ret) {
        qDebug() << "LocalDatabaseClient -> updateActivityRegistrationStatusOnly: "
                 << query.lastError().text();
    }
    return ret;
}

bool LocalDatabaseClient::updateUserOnly(QString matricNo,
                                         QString name,
                                         QString role,
                                         int rosterMonster,
                                         QString personalEmail,
                                         QString nuhEmail,
                                         QString address,
                                         QString homePhone,
                                         QString handPhone,
                                         QString rotation) {

    QSqlQuery query(_db);
    QString queryString = "UPDATE user "
                          "SET rosterMonster = :rosterMonster, "
                          "personalEmail = :personalEmail, "
                          "nuhEmail = :nuhEmail, "
                          "address = :address, "
                          "homePhone = :homePhone, "
                          "handPhone = :handPhone, "
                          "rotation = :rotation "
                          "WHERE matricNo = :matricNo "
                          "AND name = :name "
                          "AND role = :role ";
    bool isSuccess;



    isSuccess = query.prepare(queryString);

    query.bindValue(":rosterMonster", rosterMonster);
    query.bindValue(":personalEmail", personalEmail);
    query.bindValue(":nuhEmail", nuhEmail);
    query.bindValue(":address", address);
    query.bindValue(":homePhone", homePhone);
    query.bindValue(":handPhone", handPhone);
    query.bindValue(":rotation", rotation);

    query.bindValue(":matricNo", matricNo);
    query.bindValue(":name", name);
    query.bindValue(":role", role);

    isSuccess = isSuccess && query.exec();

    if (!isSuccess) {
        QString errorMsg = "LocalDatabaseClient -> updateUserOnly: " +
                           query.lastError().text();
        qDebug() << errorMsg;
    }
    return isSuccess;
}

bool LocalDatabaseClient::isValidDate(QString date) {
    QDate checkDate = QDate::fromString(date, FORMAT_DATE);
    return checkDate.isValid();
}

bool LocalDatabaseClient::isValidTime(QString time) {
    QTime checkTime = QTime::fromString(time, FORMAT_TIME);
    return checkTime.isValid();
}

bool LocalDatabaseClient::retrieveJsonData(QJsonObject jsonFile,
                                           QString param,
                                           QVector<QString> names) {
    qDebug() << "LocalDatabaseClient -> retrieveJsonData: "
                "start retrieving data";

    if (param.isEmpty()) {
        return false;
    }

    qDebug() << "LocalDatabaseClient -> retrieveJsonData: "
                "param is " << param;

    param = param.trimmed();

    if (!isJsonValueExist(jsonFile, param)) {
        return false;
    }

    qDebug() << "LocalDatabaseClient -> retrieveJsonData: "
                "start extracting datas...";

    bool isSuccess = true;
    QJsonObject paramName = jsonFile[param].toObject();
    for (int i = 0; i < names.size(); i++) {
        QString oneOfTheParam = names.value(i);

        qDebug() << "LocalDatabaseClient -> retrieveJsonData: "
                    "one of the param: " << oneOfTheParam;

        if (!isJsonValueExist(paramName, oneOfTheParam)) {
           continue;
        }

        QJsonArray paramArray = paramName[oneOfTheParam].toArray();
        for (int i = 0; i < paramArray.size(); i++) {
            qDebug() << "LocalDatabaseClient -> retrieveJsonData: "
                        "getting datas...";
            if (!isJsonValueExist(paramArray.at(i))) {
                qDebug() << paramArray.at(i).toString() << " not exist!";
                continue;
            }

            QJsonObject paramValue = paramArray.at(i).toObject();
            if (param == "tableNames") {
                qDebug() << param << " : " << oneOfTheParam
                         << " is being extracted!";
                isSuccess = isSuccess &&
                         extractDataFrom(oneOfTheParam, paramValue);
            }
        }
    }

    qDebug() << "LocalDatabaseClient -> retrieveJsonData: "
                "done retrieving data, status: " << isSuccess;

    return isSuccess;
}

void LocalDatabaseClient::readReplyFile(QString filename) {
    qDebug() << "LocalDatabaseClient -> readReplyFile: "
                "reading... "
             << filename;

    QJsonObject jsonFile = _networkClient->loadJsonFile(filename);

    bool hasTable = retrieveJsonData(jsonFile,
                                     "tableNames",
                                     TABLE_NAMES);

    qDebug() << "retrieving tables status: " << hasTable;

    bool hasFile = retrieveJsonData(jsonFile,
                                    "fileNames",
                                    FILE_NAMES);
    qDebug() << "retrieving files status: " << hasFile;


    qDebug() << "LocalDatabaseClient -> readReplyFile: "
                "done retrieving datas from the reply file "
             << filename;


    emit this->hasSynced(hasTable | hasFile);

}

bool LocalDatabaseClient::clearAllTables() {
    qDebug() << "LocalDatabaseClient -> clearAllTables: "
                "start clearing all tables";

    bool isSuccess = true;
    for (int i = 0; i < TABLE_NAMES.size(); i++) {
        QString tableName = TABLE_NAMES.value(i);
        isSuccess = isSuccess && clearTable(tableName);
    }

    qDebug() << "LocalDatabaseClient -> clearAllTables: "
                "done clearing all tables, status: "
             << isSuccess;
    return isSuccess;
}

bool LocalDatabaseClient::clearTable(QString tableName) {
    qDebug() << "LocalDatabaseClient -> clearTable: "
             << "clearing... " << tableName;

    QSqlQuery query(_db);
    QString queryString = "DELETE FROM " + tableName;
    bool isSuccess = query.exec(queryString);

    if (!isSuccess) {
        qDebug() << "LocalDatabaseClient -> clearTable: "
                 << query.lastError().text();
    }

    return isSuccess;
}

//private slots
void LocalDatabaseClient::handleFileIsReady(QString filename) {
    readReplyFile(filename);
}

void LocalDatabaseClient::handleSyncFailed() {
    emit this->hasSynced(false);
}
