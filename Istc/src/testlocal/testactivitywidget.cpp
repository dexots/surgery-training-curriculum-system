#include "testlocal/testactivitywidget.h"



void TestActivityWidget::initTestCase() {
    _userDetails = UserDetails::getObject();
    _localDb = LocalDatabaseClient::getObject();
}

void TestActivityWidget::cleanupTestCase() {
    clearAllTables();

    UserDetails::_userDetails->deleteLater();
    _activityWidget->deleteLater();
    LocalDatabaseClient::_dbClient->deleteLater();
}

void TestActivityWidget::testIntegration() {
    int result;

    QString matricNo = "H1234567A";
    QString role = "trainee";
    _userDetails->setMatricNo(matricNo);
    _userDetails->setUserRole(role);

    _localDb->getUserDetails();

    _activityWidget = new ActivityWidget;

    QVERIFY (_activityWidget->_userRole == role);
    QBENCHMARK {
         result = _activityWidget->_userRole == role;
    }

    QVector<EventItem*>* dataInDb = _localDb->getActivitiesInfo();
    QVERIFY(dataInDb->size() == _activityWidget->_events.size());
    QBENCHMARK {
         result = dataInDb->size() == _activityWidget->_events.size();
    }

    for (int i = 0; i < dataInDb->size(); i++) {
        bool isEqual = dataInDb->value(i)->getActivityId() ==
                       _activityWidget->_events.value(i)->getActivityId();
        QVERIFY(isEqual);
        QBENCHMARK {
             result = isEqual;
        }
    }

    delete dataInDb;
    Q_UNUSED(result);
}

// Add activities with different types of names
void TestActivityWidget::testAddActivityNames() {
    QString trainerId = "T0123457Y";
    QString trainerName = "Adrian Lim";
    setTrainer(trainerId, trainerName);

    if (_activityWidget == NULL) {
        _activityWidget = new ActivityWidget;
    }

    // valid test cases
    bool isPositiveCase = true;
    addActivityNames("Heart Surgery Training", trainerName, isPositiveCase);
    addActivityNames(GenerateString::generateString(1,true,false),
                     trainerName, isPositiveCase);
    addActivityNames(GenerateString::generateString(1,false,true),
                     trainerName, isPositiveCase);
    addActivityNames(GenerateString::generateString(100,true,false),
                     trainerName, isPositiveCase);
    addActivityNames(GenerateString::generateString(100,true,true),
                     trainerName, isPositiveCase);
    addActivityNames(GenerateString::generateString(100,false,true),
                     trainerName, isPositiveCase);
    addActivityNames(GenerateString::generateString(10,true,false),
                     trainerName, isPositiveCase);
    addActivityNames(GenerateString::generateString(10,false,true),
                     trainerName, isPositiveCase);
    addActivityNames(GenerateString::generateString(10,true,true),
                     trainerName, isPositiveCase);
    addActivityNames("SELECT * FROM activity WHERE 1=(1",
                     trainerName, isPositiveCase);

    // invalid test cases
    isPositiveCase = false;
    addActivityNames("", trainerName, isPositiveCase);

    deleteUser(trainerId);
}

// Add activities with different types of room names
void TestActivityWidget::testAddActivityPlace() {
    QString trainerId = "T0123457Y";
    QString trainerName = "Lynnette Ng";
    setTrainer(trainerId, trainerName);

    if (_activityWidget == NULL) {
        _activityWidget = new ActivityWidget;
    }

    bool isPositiveCase = true;
    // valid test cases
    addActivityPlace("Heart Surgery Training", trainerName, isPositiveCase);
    addActivityPlace(GenerateString::generateString(1,true,false),
                     trainerName, isPositiveCase);
    addActivityPlace(GenerateString::generateString(1,false,true),
                     trainerName, isPositiveCase);
    addActivityPlace(GenerateString::generateString(100,true,false),
                     trainerName, isPositiveCase);
    addActivityPlace(GenerateString::generateString(100,true,true),
                     trainerName, isPositiveCase);
    addActivityPlace(GenerateString::generateString(100,false,true),
                     trainerName, isPositiveCase);
    addActivityPlace(GenerateString::generateString(10,true,false),
                     trainerName, isPositiveCase);
    addActivityPlace(GenerateString::generateString(10,false,true),
                     trainerName, isPositiveCase);
    addActivityPlace(GenerateString::generateString(10,true,true),
                     trainerName, isPositiveCase);
    addActivityPlace("SELECT * FROM activity WHERE 1=(1",
                     trainerName, isPositiveCase);

    //invalid test cases
    isPositiveCase = false;
    addActivityPlace("", trainerName, isPositiveCase);

    deleteUser(trainerId);
}

// Add activities with different vacancies (i.e. testing INT boundaries)
void TestActivityWidget::testAddActivityVacancies() {
    QString trainerId = "T0123457Y";
    QString trainerName = "Lynnette Ng";
    setTrainer(trainerId, trainerName);

    if (_activityWidget == NULL) {
        _activityWidget = new ActivityWidget;
    }

    bool isPositiveCase = true;
    addActivityVacancies(INT_MAX, trainerName, isPositiveCase);
    addActivityVacancies(10, trainerName, isPositiveCase);
    addActivityVacancies(1, trainerName, isPositiveCase);

    isPositiveCase = false;
    addActivityVacancies(INT_MIN, trainerName, isPositiveCase);
    addActivityVacancies(0, trainerName, isPositiveCase);
    addActivityVacancies(-1, trainerName, isPositiveCase);

    deleteUser(trainerId);
}

// Add activities with different description
void TestActivityWidget::testAddActivityDescription() {
    QString trainerId = "T0123457Y";
    QString trainerName = "Lynnette Ng";
    setTrainer(trainerId, trainerName);

    if (_activityWidget == NULL) {
        _activityWidget = new ActivityWidget;
    }

    bool isPositiveCase = true;
    // valid test cases
    addActivityDescription("Procedure for heart surgery", trainerName, isPositiveCase);
    addActivityDescription(GenerateString::generateString(1,true,false),
                     trainerName, isPositiveCase);
    addActivityDescription(GenerateString::generateString(1,false,true),
                     trainerName, isPositiveCase);
    addActivityDescription(GenerateString::generateString(100,true,false),
                     trainerName, isPositiveCase);
    addActivityDescription(GenerateString::generateString(100,true,true),
                     trainerName, isPositiveCase);
    addActivityDescription(GenerateString::generateString(100,false,true),
                     trainerName, isPositiveCase);
    addActivityDescription(GenerateString::generateString(10,true,false),
                     trainerName, isPositiveCase);
    addActivityDescription(GenerateString::generateString(10,false,true),
                     trainerName, isPositiveCase);
    addActivityDescription(GenerateString::generateString(10,true,true),
                     trainerName, isPositiveCase);
    addActivityDescription("SELECT * FROM activity WHERE 1=(1",
                     trainerName, isPositiveCase);

    isPositiveCase = false;
    addActivityDescription(NULL, trainerName, isPositiveCase);
    addActivityDescription("", trainerName, isPositiveCase);

    deleteUser(trainerId);
}

// Add activities with different combination of dates, registration deadlines and start/end times
void TestActivityWidget::testAddActivityDateAndTime() {
    QString trainerId = "T0123457Y";
    QString trainerName = "Lynnette Ng";
    setTrainer(trainerId, trainerName);

    if (_activityWidget == NULL) {
        _activityWidget = new ActivityWidget;
    }

    bool isPositiveCase = true;
    QDate date = QDate::currentDate();
    QDateTime deadline = QDateTime::currentDateTime();
    QDateTime currentDateTime = QDateTime::currentDateTime();
    deadline.setTime(QTime(23, 58));
    QTime startTime = QTime(23, 59);
    QTime endTime = startTime.addSecs(1);

    //valid test cases
    if (startTime < currentDateTime.time()) { // prevent activity date time is before current date time
        date = date.addDays(1);
        deadline = deadline.addDays(1);
    }
    addActivityDateAndTime(date, deadline, startTime, endTime, trainerName, isPositiveCase);

    isPositiveCase = false;
    //invalid test cases
    // activity date is null
    addActivityDateAndTime(QDate(), deadline, startTime, endTime, trainerName, isPositiveCase);
    // deadline is null
    addActivityDateAndTime(date, QDateTime(), startTime, endTime, trainerName, isPositiveCase);
    // start time is null
    addActivityDateAndTime(date, deadline, QTime(), endTime, trainerName, isPositiveCase);
    // end time is null1
    addActivityDateAndTime(date, deadline, startTime, QTime(), trainerName, isPositiveCase);
    // activity date < current date
    date = QDate::currentDate().addDays(-1);
    addActivityDateAndTime(date, deadline, startTime, endTime, trainerName, isPositiveCase);
    // activity date < deadline
    if (startTime < currentDateTime.time()) { // prevent activity date time is before current date time
        date = QDate::currentDate().addDays(1);
        deadline = QDateTime::currentDateTime().addDays(2);
    } else {
        deadline = QDateTime::currentDateTime().addDays(1);
    }
    addActivityDateAndTime(date, deadline, startTime, endTime, trainerName, isPositiveCase);
    // activity time = deadline
    date = QDate::currentDate().addDays(1);
    startTime = QTime(18, 00);
    endTime = startTime.addSecs(3600);
    deadline = QDateTime::currentDateTime().addDays(1);
    deadline.setTime(startTime);
    addActivityDateAndTime(date, deadline, startTime, endTime, trainerName, isPositiveCase);
    // activity date = deadline date and start time < deadline time
    startTime = QTime(18, 00);
    endTime = startTime.addSecs(3600);
    deadline.setTime(endTime);
    addActivityDateAndTime(date, deadline, startTime, endTime, trainerName, isPositiveCase);
    // deadline < current time
    date = QDate::currentDate().addDays(1);
    deadline = QDateTime::currentDateTime().addSecs(-3600);
    addActivityDateAndTime(date, deadline, startTime, endTime, trainerName, isPositiveCase);
    // start time = end time
    startTime = QTime(18, 00);
    endTime = startTime;
    addActivityDateAndTime(date, deadline, startTime, endTime, trainerName, isPositiveCase);
    // start time > end time
    startTime = QTime(18, 00);
    endTime = startTime.addSecs(-3600);
    addActivityDateAndTime(date, deadline, startTime, endTime, trainerName, isPositiveCase);

    deleteUser(trainerId);
}

// Add activity with different web link
void TestActivityWidget::testAddActivityWebLink() {
    QString trainerId = "T0123457Y";
    QString trainerName = "Lynnette Ng";
    setTrainer(trainerId, trainerName);

    if (_activityWidget == NULL) {
        _activityWidget = new ActivityWidget;
    }

    bool isPositiveCase = true;
    // valid test cases
    addActivityWebLink("www.nuh.gov.sg", trainerName, isPositiveCase);
    addActivityWebLink(GenerateString::generateString(1,true,false),
                     trainerName, isPositiveCase);
    addActivityWebLink(GenerateString::generateString(1,false,true),
                     trainerName, isPositiveCase);
    addActivityWebLink(GenerateString::generateString(100,true,false),
                     trainerName, isPositiveCase);
    addActivityWebLink(GenerateString::generateString(100,true,true),
                     trainerName, isPositiveCase);
    addActivityWebLink(GenerateString::generateString(100,false,true),
                     trainerName, isPositiveCase);
    addActivityWebLink(GenerateString::generateString(10,true,false),
                     trainerName, isPositiveCase);
    addActivityWebLink(GenerateString::generateString(10,false,true),
                     trainerName, isPositiveCase);
    addActivityWebLink(GenerateString::generateString(10,true,true),
                     trainerName, isPositiveCase);
    addActivityWebLink("SELECT * FROM activity WHERE 1=(1",
                     trainerName, isPositiveCase);
    addActivityWebLink(NULL, trainerName, isPositiveCase);
    addActivityWebLink("", trainerName, isPositiveCase);

    deleteUser(trainerId);
}

// Failure cases
void TestActivityWidget::testInvalidAddActivityVacancies() {
    QString trainerId = "T0123457Y";
    QString trainerName = "Lynnette Ng";
    setTrainer(trainerId, trainerName);

    if (_activityWidget == NULL) {
        _activityWidget = new ActivityWidget;
    }


    deleteUser(trainerId);
}

/*
 * Helper functions
*/

void TestActivityWidget::setTrainer(QString trainerId, QString trainerName) {
    UserDetails *userDetails = UserDetails::getObject();
    userDetails->setMatricNo(trainerId);
    userDetails->setUserName(trainerName);
    userDetails->setUserRole(UserDetails::ROLE_TRAINER);

    _localDb->getUserDetails();

    _localDb->addCurrentUser();
}

void TestActivityWidget::deleteUser(QString matricNo) {
    _localDb->deleteCurrentUser(matricNo);
}

void TestActivityWidget::addActivityNames(QString name, QString trainerName
                                          , bool isPositiveCase) {
    QVector<QString> info = QVector<QString>() << name
                          << trainerName
                          << "Seminar Room 2"
                          << "Procedure for heart surgery"
                          << "www.nuh.gov.sg";
    int vacancies = 50;

    QDate date = QDate::currentDate().addDays(1);
    QDateTime deadline = QDateTime::currentDateTime().addDays(1);
    deadline.setTime(QTime(00, 00));
    QTime startTime = QTime(1, 0);
    QTime endTime = startTime.addSecs(3600);
    QVector<QTime> time = QVector<QTime>() << startTime << endTime;

    bool isAdded = _activityWidget->processActivityDetails(info, date, deadline,
                      time, vacancies, true);

    if (isPositiveCase) {
        verify(isAdded);
    } else {
        verify(!isAdded);
    }
}

void TestActivityWidget::addActivityPlace(QString place, QString trainerName
                                          , bool isPositiveCase) {
    QVector<QString> info = QVector<QString>() << "Activity"
                          << trainerName
                          << place
                          << "Procedure for heart surgery"
                          << "www.nuh.gov.sg";
    int vacancies = 50;

    QDate date = QDate::currentDate().addDays(1);
    QDateTime deadline = QDateTime::currentDateTime().addDays(1);
    deadline.setTime(QTime(00, 00));
    QTime startTime = QTime(1, 0);
    QTime endTime = startTime.addSecs(3600);
    QVector<QTime> time = QVector<QTime>() << startTime << endTime;

    bool isAdded = _activityWidget->processActivityDetails(info, date, deadline,
                      time, vacancies, true);

    if (isPositiveCase) {
        verify(isAdded);
    } else {
        verify(!isAdded);
    }
}

void TestActivityWidget::addActivityVacancies(int vacancy, QString trainerName,
                                              bool isPositiveCase) {
    QVector<QString> info = QVector<QString>() << "Activity"
                          << trainerName
                          << "Seminar Room 2"
                          << "Procedure for heart surgery"
                          << "www.nuh.gov.sg";

    QDate date = QDate::currentDate().addDays(1);
    QDateTime deadline = QDateTime::currentDateTime().addDays(1);
    deadline.setTime(QTime(00, 00));
    QTime startTime = QTime(1, 0);
    QTime endTime = startTime.addSecs(3600);
    QVector<QTime> time = QVector<QTime>() << startTime << endTime;

    bool isAdded = _activityWidget->processActivityDetails(info, date, deadline,
                      time, vacancy, true);

    if (isPositiveCase) {
        verify(isAdded);
    } else {
        verify(!isAdded);
    }
}

void TestActivityWidget::addActivityDescription(QString description, QString trainerName,
                                                bool isPositiveCase) {
    QVector<QString> info = QVector<QString>() << "Activity"
                          << trainerName
                          << "Seminar Room 2"
                          << description
                          << "www.nuh.gov.sg";
    int vacancies = 50;

    QDate date = QDate::currentDate().addDays(1);
    QDateTime deadline = QDateTime::currentDateTime().addDays(1);
    deadline.setTime(QTime(00, 00));
    QTime startTime = QTime(1, 0);
    QTime endTime = startTime.addSecs(3600);
    QVector<QTime> time = QVector<QTime>() << startTime << endTime;

    bool isAdded = _activityWidget->processActivityDetails(info, date, deadline,
                      time, vacancies, true);

    if (isPositiveCase) {
        verify(isAdded);
    } else {
        verify(!isAdded);
    }
}

void TestActivityWidget::addActivityWebLink(QString webLink, QString trainerName,
                                            bool isPositiveCase) {
    QVector<QString> info = QVector<QString>() << "Activity"
                          << trainerName
                          << "Seminar Room 2"
                          << "Procedure for heart surgery"
                          << webLink;
    int vacancies = 50;

    QDate date = QDate::currentDate().addDays(1);
    QDateTime deadline = QDateTime::currentDateTime().addDays(1);
    deadline.setTime(QTime(00, 00));
    QTime startTime = QTime(1, 0);
    QTime endTime = startTime.addSecs(3600);
    QVector<QTime> time = QVector<QTime>() << startTime << endTime;

    bool isAdded = _activityWidget->processActivityDetails(info, date, deadline,
                      time, vacancies, true);

    if (isPositiveCase) {
        verify(isAdded);
    } else {
        verify(!isAdded);
    }
}

void TestActivityWidget::addActivityDateAndTime(QDate activityDate,
                                                QDateTime registrationDeadline,
                                                QTime startTime, QTime endTime,
                                                QString trainerName, bool isPositiveCase) {
    QVector<QString> info = QVector<QString>() << "Activity"
                          << trainerName
                          << "Seminar Room 2"
                          << "Procedure for heart surgery"
                          << "www.nuh.gov.sg";

    int vacancy = 50;
    QVector<QTime> time = QVector<QTime>() << startTime << endTime;

    bool isAdded = _activityWidget->processActivityDetails(info, activityDate,
                                    registrationDeadline, time, vacancy, true);

    if (isPositiveCase) {
        verify(isAdded);
    } else {
        verify(!isAdded);
    }
}

void TestActivityWidget::verify(bool variable) {
    int result;

    QVERIFY(variable);
    QBENCHMARK {
        result = variable;
    }
    Q_UNUSED(result);
}

void TestActivityWidget::clearAllTables() {
    _localDb->clearAllTables();
}
