#include "testlocal/testpatientbox.h"

void TestPatientBox::init() {
    _patientBox = new PatientBox();

}

void TestPatientBox::cleanup() {
    _patientBox->done(0);
    _patientBox->deleteLater();
}

void TestPatientBox::testInputId_data() {
    QTest::addColumn<QTestEventList>("events");
    QTest::addColumn<QString>("expected");

    QTestEventList invalid1;
    QString longString = "123kjhsdflqweksjdfhsjdf";
    invalid1.addKeyClicks(longString);
    QTest::newRow("Invalid1") << invalid1 << "123kjhsdfl";

    QTestEventList invalid3;
    string = "a";
    invalid1.addKeyClicks(string);
    QTest::newRow("Invalid2") << invalid3 << "a";
}

void TestPatientBox::testInputId() {
    QFETCH(QTestEventList, events);
    QFETCH(QString, expected);

    events.simulate(_patientBox->_inputId);
    QString actual = _patientBox->_inputId->toPlainText();

    QVERIFY (actual == expected);

}
