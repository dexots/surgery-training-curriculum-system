#include "testlocal/generatestring.h"

void GenerateString::FillWithAlphaNumeric(std::string& string,
                                          unsigned int StringLength) {
    for (unsigned int i = 0 ; i < StringLength ; ++i) {
        char result = rand() % 3;
        if (result == 0) {
          string[i] = (char)((rand() % (('z' + 1) - 'a')) + 'a');
        } else if (result == 1) {
          string[i] = (char)((rand() % (('Z' + 1) - 'A')) + 'A');
        } else {
          string[i] = (char)((rand() % (('9' + 1) - '0')) + '0');
        }
    }
}

void GenerateString::FillWithNumeric(std::string& string,
                                     unsigned int StringLength) {
    for (unsigned int i = 0 ; i < StringLength ; ++i) {
        string[i] = (char)((rand() % (('9' + 1) - '0')) + '0');
    }
}

void GenerateString::FillWithAlpha(std::string& string,
                                   unsigned int StringLength) {
    for (unsigned int i = 0 ; i < StringLength ; ++i) {
        if (rand() % 2) {
            string[i] = (char)((rand() % (('z'+1) - 'a')) + 'a');
        } else {
            string[i] = (char)((rand() % (('Z'+1) - 'A')) + 'A');
        }
    }
}

QString GenerateString::generateString(unsigned int StringLength,
                                       bool wantNumber,
                                       bool wantAlpha) {
    std::string result;
    result.resize(StringLength + 1, 0);

    if (wantNumber && wantAlpha) {
        FillWithAlphaNumeric(result, StringLength);
    } else if (wantNumber) {
        FillWithNumeric(result, StringLength);
    } else if (wantAlpha) {
        FillWithAlpha(result, StringLength);
    }

    return QString::fromStdString(result);
}
