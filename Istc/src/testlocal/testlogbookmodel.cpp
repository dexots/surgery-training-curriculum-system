#include "testlocal/testlogbookmodel.h"

// functions executed by QtTest before test suite
void TestLogbookModel::initTestCase() {
    _logbookModel = LogbookModel::getObject();
}

// functions executed by QtTest after test suite
void TestLogbookModel::cleanupTestCase() {
    LogbookModel::_logObject->deleteLater();
}

//test calNoOfHours() method
void TestLogbookModel::testCalNoOfHours() {
    QDateTime from = QDateTime::currentDateTime();
    QDateTime to = QDateTime::currentDateTime();
    QDateTime start; qint64 hours;

    start = from.addDays(-1);
    hours = _logbookModel->calNoOfHours(start, to);
    compareResult(hours, 24);

    start = from.addDays(-10);
    hours = _logbookModel->calNoOfHours(start, to);
    compareResult(hours, 24*10);

    start = from;
    hours = _logbookModel->calNoOfHours(start, to);
    compareResult(hours, 0);
}

//test calNoOfSecs() method
void TestLogbookModel::testCalNoOfSecs() {
    QDateTime from = QDateTime::currentDateTime();
    QDateTime to = QDateTime::currentDateTime();
    QDateTime start;  qint64 secs;

    start = from.addDays(-1);
    secs = _logbookModel->calNoOfSecs(start, to);
    compareResult(secs, 24*60*60);

    start = from.addSecs(-1);
    secs = _logbookModel->calNoOfSecs(start, to);
    compareResult(secs, 1);

    start = from.addSecs(-10);
    secs = _logbookModel->calNoOfSecs(start, to);
    compareResult(secs, 10);

    start = from;
    secs = _logbookModel->calNoOfSecs(start, to);
    compareResult(secs, 0);

}

void TestLogbookModel::compareResult(qint64 hours, qint64 ans) {
    QVERIFY (hours == ans);

    int result;
    QBENCHMARK {
         result = hours == ans;
    }
    Q_UNUSED(result);
}

void TestLogbookModel::verify(bool variable) {
    int result;

    QVERIFY(variable);
    QBENCHMARK {
        result = variable;
    }
    Q_UNUSED(result);
}

