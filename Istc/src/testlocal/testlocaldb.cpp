#include "testlocal/testlocaldb.h"

void TestLocalDb::initTestCase() {
    _localDbClient = LocalDatabaseClient::getObject();
    _user = UserDetails::getObject();

    clearAllTables();

}

void TestLocalDb::cleanupTestCase() {
    UserDetails::_userDetails->deleteLater();
    LocalDatabaseClient::_dbClient->deleteLater();
}


void TestLocalDb::cleanup() {
    clearAllTables();
}

void TestLocalDb::testValidUserByRole() {
    // Partition by different roles
    QVector<QString> details = QVector<QString>() << "Lynnette"
                               << "H1234567A" << UserDetails::ROLE_TRAINEE;

    validUserDetails(details, true);

    details = QVector<QString>() << "Lynnette"
                               << "H1234567A" << UserDetails::ROLE_TRAINER;
    validUserDetails(details, true);

    details = QVector<QString>() << "Lynnette"
                               << "H1234567A" << UserDetails::ROLE_ADMIN;
}

void TestLocalDb::testValidUserByRosterMon() {
    // Partition by roster monsters
    QVector<QString>details = QVector<QString>() << "Lynnette"
                               << "H1234567A" << UserDetails::ROLE_TRAINEE;
    validUserDetails(details, false);

    details = QVector<QString>() << "Lynnette"
                               << "H1234567A" << UserDetails::ROLE_TRAINEE;
    validUserDetails(details, true);

    details = QVector<QString>() << "Lynnette"
                               << "H1234567A" << UserDetails::ROLE_TRAINER;
    validUserDetails(details, false);

    details = QVector<QString>() << "Lynnette"
                               << "H1234567A" << UserDetails::ROLE_TRAINER;
    validUserDetails(details, true);

    details = QVector<QString>() << "Lynnette"
                               << "H1234567A" << UserDetails::ROLE_ADMIN;
    validUserDetails(details, false);

    details = QVector<QString>() << "Lynnette"
                               << "H1234567A" << UserDetails::ROLE_ADMIN;
    validUserDetails(details, true);
}

void TestLocalDb::testValidUserByName() {
    // Partition by name types
    QVector<QString>details = QVector<QString>() << "Lynnette"
                               << "H1234567A" << UserDetails::ROLE_TRAINEE;
    validUserDetails(details, true);

    details = QVector<QString>() << "LYNNETTE"
                               << "H1234567A" << UserDetails::ROLE_TRAINEE;
    validUserDetails(details, true);

    details = QVector<QString>() << "adahsgdajhsdgjahsdgajsdjlaksdjlaksdjl"
                               << "H1234567A" << UserDetails::ROLE_TRAINEE;
    validUserDetails(details, true);

    details = QVector<QString>() << "1"
                               << "H1234567A" << UserDetails::ROLE_TRAINEE;
    validUserDetails(details, true);

    details = QVector<QString>() << "a"
                               << "H1234567A" << UserDetails::ROLE_TRAINEE;
    validUserDetails(details, true);

    details = QVector<QString>() << "A"
                               << "H1234567A" << UserDetails::ROLE_TRAINEE;
    validUserDetails(details, true);

    details = QVector<QString>() << "1kjahsdjka"
                               << "H1234567A" << UserDetails::ROLE_TRAINEE;
    validUserDetails(details, true);

    details = QVector<QString>() << "1231253465"
                               << "H1234567A" << UserDetails::ROLE_TRAINEE;
    validUserDetails(details, true);

    details = QVector<QString>() << "123jkh123"
                               << "H1234567A" << UserDetails::ROLE_TRAINEE;
    validUserDetails(details, true);

    details = QVector<QString>() << "üMünödi"
                               << "H1234567A" << UserDetails::ROLE_TRAINEE;
    validUserDetails(details, true);
}

void TestLocalDb::testInvalidUserRole() {
    // This will throw out "LocalDatabaseClient -> addCurrentUser: CHECK constraint failed"
    // But that's because the ROLE doesn't exist, and it's checking all tables

    QVector<QString>details = QVector<QString>() << "Lynnette"
                               << "H1234567A" << "UserDetails::ROLE_TRAINEE";
    addInvalidUser(details, true);

    details = QVector<QString>() << "Lynnette"
                               << "H1234567A" << "1";
    addInvalidUser(details, true);

    details = QVector<QString>() << "Lynnette"
                               << "H1234567A" << "a";
    addInvalidUser(details, true);
}

void TestLocalDb::testGetAssessment() {
    // Set up Trainers
    QString traineeId = "H1234568A";
    QString traineeRole = UserDetails::ROLE_TRAINEE;
    QString traineeName = "Lynnette";
    addValidUser(traineeName, traineeId, traineeRole, true);

    QVector <AssessmentObj> *assessTests = _localDbClient->getAssessments();

    QString trainerId = "T0234567Z";
    QString trainerRole = UserDetails::ROLE_TRAINER;
    QString trainerName = "David Lim";
    addValidUser(trainerName, trainerId, trainerRole, true);

    // Add assessments
    QDate assessDate = QDate::fromString("18 Jan 2015",
                                         "dd MMM yyyy");
    QString assessId = _localDbClient->generateAssessmentId();
    QString assessTitle = "abc";
    QString feedback = "hahaha";
    QString grade = "b*";

    AssessmentObj assess = createAssessmentObj(assessDate, assessId, assessTitle, feedback,
                           grade, traineeId, trainerId);

    assessTests->append(assess);

    addAssessmentToDb(assess);

    // Get Assessments and compare
    _user->setMatricNo(traineeId);
    _user->setUserRole(traineeRole);
    _localDbClient->getUserDetails();

    QVector <AssessmentObj>* assessResult = _localDbClient->getAssessments();

    checkAssessmentAddition(assessTests, assessResult);


    deleteUser(traineeId, traineeRole);
    deleteUser(trainerId, trainerRole);
}

// Add actvities with different names

void TestLocalDb::testAddActivityName0() {
    addTestAddActivityNameData();

    int currentSize = _localDbClient->getActivitiesInfo()->size();

    QString trainerName = _user->getUserName();
    bool isPositiveCase = true;
    QString expected = "Heart Surgery Training";
    addActivityNames(expected, trainerName, isPositiveCase);
    QCOMPARE(_localDbClient->getActivitiesInfo()->size(), ++currentSize);
    EventItem *lastAddedEvent = _localDbClient->getActivitiesInfo()->at(currentSize - 1);
    QCOMPARE(lastAddedEvent->getName(), expected);

    int result;
    QBENCHMARK {
        result = lastAddedEvent->getName() == expected;
        Q_UNUSED (result);
    }
}



void TestLocalDb::testAddActivityName1() {
    addTestAddActivityNameData();

    int currentSize = _localDbClient->getActivitiesInfo()->size();

    QString trainerName = _user->getUserName();
    bool isPositiveCase = true;
    QString expected = "SELECT * FROM activity WHERE 1=(1";
    addActivityNames(expected, trainerName, isPositiveCase);
    QCOMPARE(_localDbClient->getActivitiesInfo()->size(), ++currentSize);
    EventItem *lastAddedEvent = _localDbClient->getActivitiesInfo()->at(currentSize - 1);
    QCOMPARE(lastAddedEvent->getName(), expected);

    int result;
    QBENCHMARK {
        result = lastAddedEvent->getName() == expected;
        Q_UNUSED (result);
    }
}

void TestLocalDb::testAddActivityName2() {
    addTestAddActivityNameData();

    int currentSize = _localDbClient->getActivitiesInfo()->size();

    QString trainerName = _user->getUserName();
    bool isPositiveCase = true;
    QString expected = GenerateString::generateString(1, true, false);
    qDebug() << "LOOK FOR THIS: " + expected;
    addActivityNames(expected, trainerName, isPositiveCase);
    QCOMPARE(_localDbClient->getActivitiesInfo()->size(), ++currentSize);
    EventItem *lastAddedEvent = _localDbClient->getActivitiesInfo()->at(currentSize - 1);
    QCOMPARE(lastAddedEvent->getName(), expected);

    int result;
    QBENCHMARK {
        result = lastAddedEvent->getName() == expected;
        Q_UNUSED (result);
    }
}

void TestLocalDb::testAddActivityName3() {
    addTestAddActivityNameData();

    int currentSize = _localDbClient->getActivitiesInfo()->size();

    QString trainerName = _user->getUserName();
    bool isPositiveCase = true;
    QString expected = GenerateString::generateString(1, false, true);
    qDebug() << "LOOK FOR THIS: " + expected;
    addActivityNames(expected, trainerName, isPositiveCase);
    QCOMPARE(_localDbClient->getActivitiesInfo()->size(), ++currentSize);
    EventItem *lastAddedEvent = _localDbClient->getActivitiesInfo()->at(currentSize - 1);
    QCOMPARE(lastAddedEvent->getName(), expected);
    int result;
    QBENCHMARK {
        result = lastAddedEvent->getName() == expected;
        Q_UNUSED (result);
    }

}

void TestLocalDb::testAddActivityName4() {
    addTestAddActivityNameData();

    int currentSize = _localDbClient->getActivitiesInfo()->size();

    QString trainerName = _user->getUserName();
    bool isPositiveCase = true;
    QString expected = GenerateString::generateString(100, true, false);
    qDebug() << "LOOK FOR THIS: " + expected;
    addActivityNames(expected, trainerName, isPositiveCase);
    QCOMPARE(_localDbClient->getActivitiesInfo()->size(), ++currentSize);
    EventItem *lastAddedEvent = _localDbClient->getActivitiesInfo()->at(currentSize - 1);
    QCOMPARE(lastAddedEvent->getName(), expected);

    int result;
    QBENCHMARK {
        result = lastAddedEvent->getName() == expected;
        Q_UNUSED (result);
    }
}

void TestLocalDb::testAddActivityName5() {
    addTestAddActivityNameData();

    int currentSize = _localDbClient->getActivitiesInfo()->size();

    QString trainerName = _user->getUserName();
    bool isPositiveCase = true;
    QString expected = GenerateString::generateString(100, true, true);
    qDebug() << "LOOK FOR THIS: " + expected;
    addActivityNames(expected, trainerName, isPositiveCase);
    QCOMPARE(_localDbClient->getActivitiesInfo()->size(), ++currentSize);
    EventItem *lastAddedEvent = _localDbClient->getActivitiesInfo()->at(currentSize - 1);
    QCOMPARE(lastAddedEvent->getName(), expected);

    int result;
    QBENCHMARK {
        result = lastAddedEvent->getName() == expected;
        Q_UNUSED (result);
    }
}

void TestLocalDb::testAddActivityName6() {
    addTestAddActivityNameData();

    int currentSize = _localDbClient->getActivitiesInfo()->size();

    QString trainerName = _user->getUserName();
    bool isPositiveCase = true;
    QString expected = GenerateString::generateString(100, false, true);
    qDebug() << "LOOK FOR THIS: " + expected;
    addActivityNames(expected, trainerName, isPositiveCase);
    QCOMPARE(_localDbClient->getActivitiesInfo()->size(), ++currentSize);
    EventItem *lastAddedEvent = _localDbClient->getActivitiesInfo()->at(currentSize - 1);
    QCOMPARE(lastAddedEvent->getName(), expected);

    int result;
    QBENCHMARK {
        result = lastAddedEvent->getName() == expected;
        Q_UNUSED (result);
    }
}

void TestLocalDb::testAddActivityName7() {
    addTestAddActivityNameData();

    int currentSize = _localDbClient->getActivitiesInfo()->size();

    QString trainerName = _user->getUserName();
    bool isPositiveCase = true;
    QString expected = GenerateString::generateString(10, true, false);
    qDebug() << "LOOK FOR THIS: " + expected;
    addActivityNames(expected, trainerName, isPositiveCase);
    QCOMPARE(_localDbClient->getActivitiesInfo()->size(), ++currentSize);
    EventItem *lastAddedEvent = _localDbClient->getActivitiesInfo()->at(currentSize - 1);
    QCOMPARE(lastAddedEvent->getName(), expected);

    int result;
    QBENCHMARK {
        result = lastAddedEvent->getName() == expected;
        Q_UNUSED (result);
    }
}

void TestLocalDb::testAddActivityName8() {
    addTestAddActivityNameData();

    int currentSize = _localDbClient->getActivitiesInfo()->size();

    QString trainerName = _user->getUserName();
    bool isPositiveCase = true;
    QString expected = GenerateString::generateString(10, true, true);
    qDebug() << "LOOK FOR THIS: " + expected;
    addActivityNames(expected, trainerName, isPositiveCase);
    QCOMPARE(_localDbClient->getActivitiesInfo()->size(), ++currentSize);
    EventItem *lastAddedEvent = _localDbClient->getActivitiesInfo()->at(currentSize - 1);
    QCOMPARE(lastAddedEvent->getName(), expected);

    int result;
    QBENCHMARK {
        result = lastAddedEvent->getName() == expected;
        Q_UNUSED (result);
    }

}

void TestLocalDb::testAddActivityName9() {
    addTestAddActivityNameData();

    int currentSize = _localDbClient->getActivitiesInfo()->size();

    QString trainerName = _user->getUserName();
    bool isPositiveCase = true;
    QString expected = GenerateString::generateString(10, false, true);
    qDebug() << "LOOK FOR THIS: " + expected;
    addActivityNames(expected, trainerName, isPositiveCase);
    QCOMPARE(_localDbClient->getActivitiesInfo()->size(), ++currentSize);
    EventItem *lastAddedEvent = _localDbClient->getActivitiesInfo()->at(currentSize - 1);
    QCOMPARE(lastAddedEvent->getName(), expected);

    int result;
    QBENCHMARK {
        result = lastAddedEvent->getName() == expected;
        Q_UNUSED (result);
    }
}

// Playing around with the feedback
void TestLocalDb::testAddAssessmentFeedback() {
    // Create Trainees
    QString traineeId = "H1234567A";
    QString traineeRole = UserDetails::ROLE_TRAINEE;
    QString name = "Lynnette";
    addValidUser(name, traineeId, traineeRole, true);

    checkIfUserExists(traineeId, traineeRole);

    // Create Trainers
    QString trainerId = "T0234567Z";
    QString trainerRole = UserDetails::ROLE_TRAINER;
    QString trainerName = "Junwei";
    addValidUser(trainerName, trainerId, trainerRole, true);

    checkIfUserExists(trainerId, trainerRole);

    addAssessmentFeedback(GenerateString::generateString(1, true, false),
            traineeId, trainerId);
    addAssessmentFeedback(GenerateString::generateString(1, false, true),
            traineeId, trainerId);
    addAssessmentFeedback(GenerateString::generateString(100, true, true),
            traineeId, trainerId);
    addAssessmentFeedback(GenerateString::generateString(10000, true, true),
            traineeId, trainerId);
    addAssessmentFeedback("",
            traineeId, trainerId);
    // Special case where it is an SQL statement
    addAssessmentFeedback("); SELECT * FROM assessment WHERE 1=(1",
                          traineeId, trainerId);

    deleteUser(traineeId, traineeRole);
    deleteUser(trainerId, trainerRole);
}

void TestLocalDb::addAssessmentFeedback(QString feedback, QString traineeId,
                                       QString trainerId) {
    QDate assessDate = QDate::currentDate();
    QString assessId = _localDbClient->generateAssessmentId();
    QString assessTitle = "abc";
    QString grade = "B+";

    addAssessment(assessDate, assessId,
                         assessTitle, feedback, grade, traineeId, trainerId);
}

void TestLocalDb::testAddAssessmentTitle() {
    // Create Trainees
    QString traineeId = "H1234568A";
    QString traineeRole = UserDetails::ROLE_TRAINEE;
    QString name = "Lynnette";
    addValidUser(name, traineeId, traineeRole, true);

    checkIfUserExists(traineeId, traineeRole);

    // Create Trainers
    QString trainerId = "T0234567Z";
    QString trainerRole = UserDetails::ROLE_TRAINER;
    QString trainerName = "Junwei";
    addValidUser(trainerName, trainerId, trainerRole, true);

    checkIfUserExists(trainerId, trainerRole);

    addAssessmentTitle(GenerateString::generateString(1, true, false),
            traineeId, trainerId);
    addAssessmentTitle(GenerateString::generateString(1, false, true),
            traineeId, trainerId);
    addAssessmentTitle(GenerateString::generateString(100, true, true),
            traineeId, trainerId);
    addAssessmentTitle(GenerateString::generateString(10000, true, true),
            traineeId, trainerId);
    // Special case where it is an SQL statement
    addAssessmentFeedback("); SELECT * FROM assessment WHERE 1=(1",
                          traineeId, trainerId);

    deleteUser(traineeId, traineeRole);
    deleteUser(trainerId, trainerRole);
}

void TestLocalDb::addAssessmentTitle(QString assessTitle, QString
                                     traineeId, QString trainerId) {
    QDate assessDate = QDate::currentDate();
    QString assessId = _localDbClient->generateAssessmentId();
    QString feedback = "abc";
    QString grade = "B+";

    addAssessment(assessDate, assessId,
                  assessTitle, feedback, grade, traineeId, trainerId);
}

void TestLocalDb::testValidEmptyTables() {
    clearAllTables();
    // Valid table names but empty as no data yet
    QVector<QString> tableNames = QVector<QString>() << "activities"
                                  << "assessment" << "exams" << "forms"
                                  << "leaveApp" << "notes" << "patient"
                                  << "procedureTable" << "rosterAssignment"
                                  << "rosterBlockoutReq" << "traineeExams"
                                  << "trainerAssessment" << "user" << "userActivities"
                                  << "userForms" << "userProcedure"
                                  << "userRosterAssignment" << "workingHours";

    for (int i=0; i<tableNames.size(); i++) {
        invalidTableSizes(tableNames.at(i));
    }
}

void TestLocalDb::testInvalidTableNames() {
    //Testing invalid table names
    QVector<QString> shortNames = QVector<QString>() << "junwei"
                                  << "lynnette" << "ccs" << "dex";

    QString longName = "adfjkhskjdfhskjdfsjdfksjdfjksdhfjjfsdjshfskdjf";
    QVector<QString> numberName = QVector<QString>() << "1qweertyui"
                                  << "1234456" << "asdjhakjsdha5" << "123hgkjhk342";
    QString noName = "";
    QString weirdString = "Schöne Grüße";

    for (int i=0; i<shortNames.size(); i++) {
        invalidTableSizes(shortNames.at(i));
    }

    invalidTableSizes(longName);

    for (int i=0; i<numberName.size(); i++) {
        invalidTableSizes(numberName.at(i));
    }

    invalidTableSizes(noName);
    invalidTableSizes(weirdString);
}

void TestLocalDb::testValidTableSizes() {
    QString traineeId = "H1234567A";
    QString traineeRole = UserDetails::ROLE_TRAINEE;
    QString name = "Lynnette";
    addValidUser(name, traineeId, traineeRole, true);

    validTableSizes("user", 1);

    // Delete users
    deleteUser(traineeId, traineeRole);
}

/*
 * Helper Functions
*/
void TestLocalDb::addValidUser(QString name, QString matricNo,
                               QString role, bool isRosterMonster) {
    bool isSuccess = addUserToDb(name, matricNo, role, isRosterMonster);

    QVERIFY(isSuccess);

    QBENCHMARK {
        int result = isSuccess;
        Q_UNUSED(result);
    }

}

void TestLocalDb::addInvalidUser(QVector<QString> details, bool isRosterMonster) {
    QString name = details.at(0);
    QString matricNo = details.at(1);
    QString role = details.at(2);

    bool isSuccess = addUserToDb(name, matricNo, role, isRosterMonster);

    QVERIFY(!isSuccess);
    QBENCHMARK {
        int result = !isSuccess;
        Q_UNUSED(result);
    }
}

bool TestLocalDb::addUserToDb(QString name, QString matricNo,
                              QString role, bool isRosterMonster) {
    _user->setMatricNo(matricNo);
    _user->setUserRole(role);
    _user->setUserName(name);
    _user->setIsRosterMonster(isRosterMonster);
    _localDbClient->getUserDetails();
    bool isSuccess = _localDbClient->addCurrentUser();

    return isSuccess;
}

void TestLocalDb::addActivityNames(QString name, QString trainerName, bool isPositiveCase) {
    int vacancy = 50;
    QDate date = QDate::currentDate().addDays(1);
    QDateTime deadline = QDateTime::currentDateTime().addDays(1);
    deadline.setTime(QTime(00, 00));
    QTime startTime = QTime(1, 0);
    QTime endTime = startTime.addSecs(3600);

    EventItem *newEvent = new EventItem(date, startTime, endTime, name, "Seminar Room 2",
                                        trainerName, vacancy, vacancy, "Procedure for heart surgery",
                                        "www.nuh.gov.sg", true);
    newEvent->setRegistrationDeadline(deadline);
    newEvent->setActivityId(_localDbClient->generateActivityId());
    bool isAdded = _localDbClient->addNewActivity(newEvent);

    if (isPositiveCase) {
        QVERIFY (isAdded);
    } else {
        QVERIFY (!isAdded);
    }

    QBENCHMARK {
        int result = isAdded;
        Q_UNUSED(result);
    }
}

void TestLocalDb::addActivityPlace(QString place, QString trainerName, bool isPositiveCase) {
    int vacancy = 50;
    QDate date = QDate::currentDate().addDays(1);
    QDateTime deadline = QDateTime::currentDateTime().addDays(1);
    deadline.setTime(QTime(00, 00));
    QTime startTime = QTime(1, 0);
    QTime endTime = startTime.addSecs(3600);

    EventItem *newEvent = new EventItem(date, startTime, endTime, "Activity", place,
                                        trainerName, vacancy, vacancy, "Procedure for heart surgery",
                                        "www.nuh.gov.sg", true);
    newEvent->setRegistrationDeadline(deadline);
    newEvent->setActivityId(_localDbClient->generateActivityId());
    bool isAdded = _localDbClient->addNewActivity(newEvent);

    if (isPositiveCase) {
        QVERIFY (isAdded);
    } else {
        QVERIFY (!isAdded);
    }

    QBENCHMARK {
        int result = isAdded;
        Q_UNUSED(result);
    }
}

void TestLocalDb::addActivityVacancies(int vacancy, QString trainerName, bool isPositiveCase) {
    QDate date = QDate::currentDate().addDays(1);
    QDateTime deadline = QDateTime::currentDateTime().addDays(1);
    deadline.setTime(QTime(00, 00));
    QTime startTime = QTime(1, 0);
    QTime endTime = startTime.addSecs(3600);

    EventItem *newEvent = new EventItem(date, startTime, endTime, "Activity", "Seminar Room 2",
                                        trainerName, vacancy, vacancy, "Procedure for heart surgery",
                                        "www.nuh.gov.sg", true);
    newEvent->setRegistrationDeadline(deadline);
    newEvent->setActivityId(_localDbClient->generateActivityId());
    bool isAdded = _localDbClient->addNewActivity(newEvent);

    if (isPositiveCase) {
        QVERIFY (isAdded);
    } else {
        QVERIFY (!isAdded);
    }

    QBENCHMARK {
        int result = isAdded;
        Q_UNUSED(result);
    }
}

void TestLocalDb::addActivityDescription(QString description, QString trainerName, bool isPositiveCase) {
    int vacancy = 50;

    QDate date = QDate::currentDate().addDays(1);
    QDateTime deadline = QDateTime::currentDateTime().addDays(1);
    deadline.setTime(QTime(00, 00));
    QTime startTime = QTime(1, 0);
    QTime endTime = startTime.addSecs(3600);

    EventItem *newEvent = new EventItem(date, startTime, endTime, "Activity", "Seminar Room 2",
                                        trainerName, vacancy, vacancy, description,
                                        "www.nuh.gov.sg", true);
    newEvent->setRegistrationDeadline(deadline);
    newEvent->setActivityId(_localDbClient->generateActivityId());
    bool isAdded = _localDbClient->addNewActivity(newEvent);

    if (isPositiveCase) {
        QVERIFY (isAdded);
    } else {
        QVERIFY (!isAdded);
    }

    QBENCHMARK {
        int result = isAdded;
        Q_UNUSED(result);
    }
}

void TestLocalDb::addActivityWebLink(QString webLink, QString trainerName, bool isPositiveCase) {
    int vacancy = 50;

    QDate date = QDate::currentDate().addDays(1);
    QDateTime deadline = QDateTime::currentDateTime().addDays(1);
    deadline.setTime(QTime(00, 00));
    QTime startTime = QTime(1, 0);
    QTime endTime = startTime.addSecs(3600);

    EventItem *newEvent = new EventItem(date, startTime, endTime, "Activity", "Seminar Room 2",
                                        trainerName, vacancy, vacancy, "Procedure for heart surgery",
                                        webLink, true);
    newEvent->setRegistrationDeadline(deadline);
    newEvent->setActivityId(_localDbClient->generateActivityId());
    bool isAdded = _localDbClient->addNewActivity(newEvent);

    if (isPositiveCase) {
        QVERIFY (isAdded);
    } else {
        QVERIFY (!isAdded);
    }

    QBENCHMARK {
        int result = isAdded;
        Q_UNUSED(result);
    }
}

void TestLocalDb::addActivityDateAndTime(QDate activityDate, QDateTime registrationDeadline,
                                         QTime startTime, QTime endTime,
                                         QString trainerName, bool isPositiveCase) {
    int vacancy = 50;

    EventItem *newEvent = new EventItem(activityDate, startTime, endTime, "Activity", "Seminar Room 2",
                                        trainerName, vacancy, vacancy, "Procedure for heart surgery",
                                        "www.nuh.gov.sg", true);
    newEvent->setRegistrationDeadline(registrationDeadline);
    newEvent->setActivityId(_localDbClient->generateActivityId());
    bool isAdded = _localDbClient->addNewActivity(newEvent);

    if (isPositiveCase) {
        QVERIFY (isAdded);
    } else {
        QVERIFY (!isAdded);
    }

    QBENCHMARK {
        int result = isAdded;
        Q_UNUSED(result);
    }
}

void TestLocalDb::checkIfUserExists(QString matricNo, QString role) {
    bool isExist = _localDbClient->isValidUser(matricNo, role);
    QVERIFY(isExist);
    QBENCHMARK {
        int result = isExist;
        Q_UNUSED(result);
    }
}

void TestLocalDb::deleteUser(QString matricNo, QString role) {
    _localDbClient->deleteCurrentUser(matricNo);
    bool isExist = _localDbClient->isValidUser(matricNo, role);
    QCOMPARE(isExist, false);
    QBENCHMARK {
        int result = !isExist;
        Q_UNUSED(result);
    }
}

void TestLocalDb::addAssessment(QDate assessDate, QString assessId, QString assessTitle,
                                QString feedback, QString grade, QString traineeId, QString trainerId) {
    AssessmentObj assess = createAssessmentObj(assessDate, assessId,
                           assessTitle, feedback, grade, traineeId, trainerId);

    addAssessmentToDb(assess);
}

void TestLocalDb::addAssessmentToDb(AssessmentObj assess) {
    bool isSuccess = _localDbClient->addAssessment(assess);
    QVERIFY (isSuccess);

    QBENCHMARK {
        int result = isSuccess;
        Q_UNUSED(result);
    }
}

AssessmentObj TestLocalDb::createAssessmentObj(QDate assessDate, QString assessId, QString assessTitle,
                         QString feedback, QString grade, QString traineeId, QString trainerId) {
    AssessmentObj assess;
    assess.setAssessDate(assessDate);
    assess.setAssessId(assessId);
    assess.setAssessTitle(assessTitle);
    assess.setFeedback(feedback);
    assess.setGrade(grade);
    assess.setTraineeId(traineeId);
    assess.addGrader(trainerId);

    return assess;
}

void TestLocalDb::invalidTableSizes(QString tableName) {
    int error = _localDbClient->getTableSize(tableName);
    int expected = -1;

    QCOMPARE (error, expected);
    QBENCHMARK {
        int result = error == expected;
        Q_UNUSED(result);
    }}

void TestLocalDb::validTableSizes(QString tableName, int tableSize) {
    int size = _localDbClient->getTableSize(tableName);

    QCOMPARE(size, tableSize);

    QBENCHMARK {
        int result = size == tableSize;
        Q_UNUSED(result);
    }
}

void TestLocalDb::validUserDetails(QVector<QString> details, bool isRosterMonster) {
    QString name = details.at(0);
    QString matricNo = details.at(1);
    QString role = details.at(2);

    addValidUser(name, matricNo, role, isRosterMonster);

    QCOMPARE (matricNo, _localDbClient->_userMatricNo);

    QCOMPARE (role, _localDbClient->_userRole);

    QBENCHMARK {
        int result = matricNo == _localDbClient->_userMatricNo &&
                     role == _localDbClient->_userRole;
        Q_UNUSED(result);
    }

    deleteUser(matricNo, role);
}

void TestLocalDb::compareVectorSizes(int size1, int size2) {
    QVERIFY (size1 == size2);

    QBENCHMARK {
        int result = size1 == size2;
        Q_UNUSED(result);
    }
}

void TestLocalDb::checkAssessmentAddition(QVector<AssessmentObj>*  test,
                                          QVector<AssessmentObj>* fromDb) {
    compareVectorSizes(test->size(), fromDb->size());
    bool isReturn = true;

    for (int i = 0; i < fromDb->size(); i++) {
        bool assessEqual = fromDb->value(i).getAssessId()
                == test->value(i).getAssessId();
        QVERIFY (assessEqual);

        isReturn = isReturn && assessEqual;

    }
    QBENCHMARK {
        int result = isReturn;
        Q_UNUSED (result);
    }


    delete test;
    delete fromDb;
}

void TestLocalDb::clearAllTables() {
    _localDbClient->clearAllTables();
}

void TestLocalDb::addTestAddActivityNameData() {
    // Create Trainers
    QString trainerId = "T0234567Z";
    QString trainerRole = UserDetails::ROLE_TRAINER;
    QString trainerName = "Junwei";

    bool isSuccess = addUserToDb(trainerName, trainerId, trainerRole, true);
    QVERIFY(isSuccess);

    bool isExist = _localDbClient->isValidUser(trainerId, trainerRole);
    QVERIFY(isExist);

}
