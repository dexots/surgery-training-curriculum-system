#include "testlocal/testprofilebox.h"

void TestProfileBox::cleanup() {
    _profileBox->done(0);
    _profileBox->deleteLater();
}

void TestProfileBox::testInput_data() {
    QTest::addColumn<QTestEventList>("events");
    QTest::addColumn<QString>("expected");

    QTestEventList list1;
    list1.addKeyClick('6');
    list1.addKeyClick('0');
    list1.addKeyClick('0');
    list1.addKeyClick('1');
    list1.addKeyClick('4');
    list1.addKeyClick('5');
    list1.addKeyClick('6');
    list1.addKeyClick('7');
    QTest::newRow("a phone number") << list1 << "60014567";

    QTestEventList list2;
    list2.addKeyClick('6');
    list2.addKeyClick('0');
    list2.addKeyClick('0');
    list2.addKeyClick('9');
    list2.addKeyClick('1');
    list2.addKeyClick('2');
    list2.addKeyClick('3');
    list2.addKeyClick('4');

    QTest::newRow("another phone number") << list2 << "60091234";
}

void TestProfileBox::testInput() {
    _profileBox = new ProfileBox("Home Contact", "+6561234567");

    QFETCH(QTestEventList, events);
    QFETCH(QString, expected);

    events.simulate(_profileBox->_inputContent);

    QString actual = _profileBox->_inputContent->text();

    QVERIFY (actual == expected);

    int result;

    QBENCHMARK {
         result = actual == expected;
    }
    Q_UNUSED(result);
}
