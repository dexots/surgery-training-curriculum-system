#include "gui/addrosterbox.h"

const QString AddRosterBox::COLOR_BUTTONBACKGROUND = "rgb(39, 174, 96)";
const QString AddRosterBox::COLOR_BUTTONBORDER = "white";
const QString AddRosterBox::COLOR_BUTTONFONT = "white";
const QString AddRosterBox::FONT_BUTTON = "bold 14";
const QString AddRosterBox::PADDING_BUTTON = "6";
const QString AddRosterBox::RADIUS_BUTTONBORDER = "10";
const QString AddRosterBox::STYLE_BUTTONBORDER = "outset";
const QString AddRosterBox::WIDTH_BUTTON = "50";
const QString AddRosterBox::WIDTH_BUTTONBORDER = "2";

const QString AddRosterBox::QSTRING_EMPTY = "";
const QString AddRosterBox::QSTRING_SPACE = " ";
const QString AddRosterBox::FILENAME = "rosreq.txt";

const int AddRosterBox::WIDTH_MINIMUM = 300;
const int AddRosterBox::HEIGHT_MINIMUM = 600;

AddRosterBox::AddRosterBox() {
    setUpMainLayout();
    setUpMonthYearLabel();
    setUpInputLabels();
    setUpInputBoxes();
    setUpSubmitCancelBtn();
    setBoxAttributes();
}

AddRosterBox::~AddRosterBox() { }

void AddRosterBox::setUpMainLayout() {
    _bold.setBold(true);
    _mainLayout = new QVBoxLayout;
    _curDate = QDate::currentDate();
}

void AddRosterBox::setUpMonthYearLabel() {
    QLabel *month = new QLabel("Month: ");
    month->setFont(_bold);

    _monthBox = new QComboBox;
    QStringList monthList;
    monthList << "Jan" << "Feb" << "Mar" << "Apr"
             << "May" << "Jun" << "July" << "Aug"
             << "Sept" << "Oct" << "Nov" << "Dec";
    _monthBox->addItems(monthList);
    _monthBox->setCurrentIndex(_curDate.month()-1);

    QLabel *year = new QLabel("Year: ");
    year->setFont(_bold);

    _yearBox = new QComboBox;
    for (int i=2015; i<=2030; i++) {
        _yearBox->addItem(QString::number(i));
    }
    _yearBox->setCurrentIndex(_curDate.year()-2015);

    QHBoxLayout *monthYearLayout = new QHBoxLayout;
    monthYearLayout->addWidget(month);
    monthYearLayout->addWidget(_monthBox);
    monthYearLayout->addWidget(year);
    monthYearLayout->addWidget(_yearBox);

    _mainLayout->addLayout(monthYearLayout);
}

void AddRosterBox::setUpInputLabels() {
    QLabel *day = new QLabel("Day: ");
    day->setFont(_bold);

    QLabel *rosterPlaceholder = new QLabel("Enter names of team doing roster\nSeparate each name with comma (,)");
    rosterPlaceholder->setFont(_bold);

    QHBoxLayout *labelLayout = new QHBoxLayout;
    labelLayout->addWidget(day);
    labelLayout->addWidget(rosterPlaceholder);
    _mainLayout->addLayout(labelLayout);
}

void AddRosterBox::setUpInputBoxes() {
    QVBoxLayout *boxesLayout = new QVBoxLayout;

   int daysInMonth = 31;
    for (int i=1; i<=daysInMonth; i++) {
        QLabel *date = new QLabel;
        date->setText(QString::number(i));
        InputBox *inputBox = new InputBox;
        inputBox->setMaximumHeight(20);
        inputBox->setMaximumWidth(200);
        inputBox->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        _inputBoxes.append(inputBox);

        QHBoxLayout *inputLayout = new QHBoxLayout;
        inputLayout->addWidget(date);
        inputLayout->addWidget(inputBox);
        boxesLayout->addLayout(inputLayout);
    }

    QFrame *boxesFrame = new QFrame;
    boxesFrame->setLayout(boxesLayout);

    _scrollArea = new QScrollArea;
    _scrollArea->setWidget(boxesFrame);
    _scrollArea->setFrameShape(QFrame::NoFrame);
    _mainLayout->addWidget(_scrollArea);

}

void AddRosterBox::setUpSubmitCancelBtn() {
    _submitButton = new QPushButton;
    _submitButton->setText("Submit Roster");
    _submitButton->setCursor(Qt::PointingHandCursor);
    setButtonStyleSheet(_submitButton);
    _submitButton->setSizePolicy(QSizePolicy::Fixed,
                                 QSizePolicy::Fixed);
    connect(_submitButton, SIGNAL(clicked()),
            this, SLOT(handleSubmit()));

    _cancelButton = new QPushButton;
    _cancelButton->setText("Cancel");
    _cancelButton->setCursor(Qt::PointingHandCursor);
    setButtonStyleSheet(_cancelButton);
    _cancelButton->setSizePolicy(QSizePolicy::Fixed,
                                 QSizePolicy::Fixed);
    connect(_cancelButton, SIGNAL(clicked()),
            this, SLOT(handleCancel()));

    QHBoxLayout *buttonLayout = new QHBoxLayout;
    buttonLayout->addWidget(_submitButton);
    buttonLayout->addWidget(_cancelButton);
    buttonLayout->setSpacing(0);
    _mainLayout->addLayout(buttonLayout);
}

void AddRosterBox::setBoxAttributes() {
    this->setLayout(_mainLayout);
    this->setContentsMargins(10, 10, 10, 10);
    this->setMinimumWidth(WIDTH_MINIMUM);
    this->setMinimumHeight(HEIGHT_MINIMUM);

    this->setAttribute(Qt::WA_TranslucentBackground, true);
    this->setWindowFlags(Qt::FramelessWindowHint);
}

void AddRosterBox::passToModels() {

}


/*
 * Helper methods
 */
void AddRosterBox::paintEvent(QPaintEvent *event) {
    QDialog::paintEvent(event);
    QPainter painter(this);
    QBrush brush(Qt::white);
    painter.setBrush(brush);
    painter.setPen(Qt::white);
    painter.drawRoundRect(QRect(0, 0, this->width(), this->height()));
}

void AddRosterBox::changeColor(QColor font, QColor back, QWidget *widget) {
    widget->setAutoFillBackground(true);
    QPalette pallete = widget->palette();
    pallete.setColor(widget->foregroundRole(), font);
    pallete.setColor(widget->backgroundRole(), back);
    widget->setPalette(pallete);
}

void AddRosterBox::setButtonStyleSheet(QPushButton *button) {
    button->setStyleSheet("background-color:" + COLOR_BUTTONBACKGROUND + ";"
                          "border-style:" + STYLE_BUTTONBORDER + ";"
                          "border-width:" + WIDTH_BUTTONBORDER + "px;"
                          "border-radius:" + RADIUS_BUTTONBORDER + "px;"
                          "border-color:" + COLOR_BUTTONBORDER + ";"
                          "font:" + FONT_BUTTON + "px;"
                          "color:" + COLOR_BUTTONFONT + ";"
                          "min-width:" + WIDTH_BUTTON + "px;"
                          "padding:" + PADDING_BUTTON + "px;");
}

/*
 * Private slots
 */
void AddRosterBox::handleSubmit() {
    QString year = _yearBox->currentText();
    int month = _monthBox->currentIndex()+1;

    for (int i=0; i<_inputBoxes.length(); i++) {
        InputBox *inputBox = _inputBoxes.at(i);
        QString inputString = inputBox->toPlainText();
        if (inputString != "") {
            QDate *date = new QDate(year.toInt(),
                                   month, i);

            QStringList split = inputString.split(",");
            for (int j=0; j<split.length(); j++) {
                RosterAssignObj *roster = new RosterAssignObj(*date,
                                                             RosterAssignObj::STATUS_NOT_COMPLETED,
                                                             QString::number(j),
                                                             split[j],
                                                             "trainee");
                _rosterAssignObjs.append(roster);
            }
        }

    }

    passToModels();
    QDialog::done(0);
}

void AddRosterBox::handleCancel() {
    QDialog::done(0);
}


