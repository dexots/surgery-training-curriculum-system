#include "gui/eventbox.h"

const QString COLOR_BUTTONBACKGROUND = "rgb(39, 174, 96)";
const QString COLOR_BUTTONBORDER = "white";
const QString COLOR_BUTTONFONT = "white";
const QString RADIUS_BUTTONBORDER = "10";
const QString STYLE_BUTTONBORDER = "outset";
const QString WIDTH_BUTTON = "50";
const QString WIDTH_BUTTONBORDER = "2";
const QString FONT_BUTTON = "bold 14";
const QString PADDING_BUTTON = "6";
const QString MARGIN_BUTTON = "5";

const int CONTENT_MARGINS = 10;

const QString TAG_REGISTER = "Register";
const QString TAG_DEREGISTER = "Deregister";
const QString TAG_STATUS_REGISTER = "<FONT COLOR='red'><br><br>You are NOT registered for this event.<br>"
                                    "Click to join this event.";
const QString TAG_STATUS_DEREGISTER = "<FONT COLOR='red'><br><br>You are registered for this event.<br>"
                                      "If you are unavailable, you may click to de-register yourself.";
const QString TAG_CLOSE = "Close";

//for trainer
EventBox::EventBox(QString title, QString description,
                   QString link) {

    _boldFont.setBold(true);

    _titleLbl = new QLabel();
    _titleLbl->setText(title);
    _titleLbl->setFont(_boldFont);

    _descLbl = new QLabel(description);
    _descLbl->setFont(_boldFont);

    _linkLbl = new QLabel();
    _linkLbl->setText("Event Link: " + link);
    _linkLbl->setFont(_boldFont);

    _statusLbl = new QLabel();
    _buttonLayout = new QHBoxLayout;

    setCloseButton();
    setupLayout();
}

//for trainee
EventBox::EventBox(QString title, QString description,
                   QString link, bool isRegistered, bool canRegister) {

    _isRegistered = isRegistered;
    _boldFont.setBold(true);

    _titleLbl = new QLabel();
    _titleLbl->setText(title);
    _titleLbl->setFont(_boldFont);

    _descLbl = new QLabel(description);
    _descLbl->setFont(_boldFont);

    _linkLbl = new QLabel();
    _linkLbl->setText("Event Link: " + link);
    _linkLbl->setFont(_boldFont);

    _statusLbl = new QLabel();

    _buttonLayout = new QHBoxLayout;
    if (canRegister) {
        setButtonLayout();
        setRegisteredStatusText();
    } else {
        setCloseButton();
    }

    setupLayout();
}

EventBox::~EventBox() {
    delete _titleLbl;
    delete _descLbl;
    delete _statusLbl;
    delete _linkLbl;

    delete _registerBtn;
    delete _closeBtn;

    delete _buttonLayout;
    delete _mainLayout;
}

void EventBox::setButtonLayout() {
    setRegisterButton();
    setCloseButton();
}

void EventBox::setRegisterButton()
{
    _registerBtn = new QPushButton();
    EventBox::setButtonStyleSheet(_registerBtn);
    setRegisteredBtnText();
    _buttonLayout->addWidget(_registerBtn);

    connect(_registerBtn, SIGNAL(clicked()),
            this, SLOT(clickedRegisteredBtn()));
}

void EventBox::setCloseButton()
{
    _closeBtn = new QPushButton(TAG_CLOSE);

    EventBox::setButtonStyleSheet(_closeBtn);
    _buttonLayout->addWidget(_closeBtn);

    connect(_closeBtn, SIGNAL(clicked()),
            this, SLOT(clickedCloseBtn()));
}

void EventBox::setRegisteredBtnText() {
    if (_isRegistered) {
        _registerBtn->setText(TAG_DEREGISTER);
    } else {
        _registerBtn->setText(TAG_REGISTER);
    }
}

void EventBox::setupLayout() {
    _mainLayout = new QVBoxLayout;
    _mainLayout->addWidget(_titleLbl);
    _mainLayout->addWidget(_descLbl);
    _mainLayout->addWidget(_linkLbl);
    _mainLayout->addWidget(_statusLbl);
    _mainLayout->addLayout(_buttonLayout);

    this->setLayout(_mainLayout);
    this->setContentsMargins(CONTENT_MARGINS, CONTENT_MARGINS,
                             CONTENT_MARGINS, CONTENT_MARGINS);
    this->setAttribute(Qt::WA_TranslucentBackground, true);
    this->setWindowFlags(Qt::FramelessWindowHint);
}

void EventBox::setRegisteredStatusText() {
    if (_isRegistered) {
        _statusLbl->setText(TAG_STATUS_DEREGISTER);
    } else {
        _statusLbl->setText(TAG_STATUS_REGISTER);
    }
}

/*
 * Slots
 */

void EventBox::clickedCloseBtn() {
    QDialog::done(0);
}

void EventBox::clickedRegisteredBtn() {
    QDialog::done(0);
    _isRegistered = !_isRegistered;
    emit this->userHasClickedRegister(_isRegistered);

    setRegisteredBtnText();
    setRegisteredStatusText();
}

/*
 * Helper Methods
 */
void EventBox::paintEvent(QPaintEvent *event) {
    QDialog::paintEvent(event);
    QPainter painter(this);
    QBrush brush(Qt::white);
    painter.setBrush(brush);
    painter.setPen(Qt::white);
    painter.drawRoundRect(QRect(0, 0, this->width(), this->height()));
}

void EventBox::setButtonStyleSheet(QPushButton *button) {
    button->setStyleSheet("background-color:" + COLOR_BUTTONBACKGROUND + ";"
                          "border-style:" + STYLE_BUTTONBORDER + ";"
                          "border-width:" + WIDTH_BUTTONBORDER + "px;"
                          "border-radius:" + RADIUS_BUTTONBORDER + "px;"
                          "border-color:" + COLOR_BUTTONBORDER + ";"
                          "font:" + FONT_BUTTON + "px;"
                          "color:" + COLOR_BUTTONFONT + ";"
                          "min-width:" + WIDTH_BUTTON + "px;"
                          "padding:" + PADDING_BUTTON + "px;"
                          "margin:" + MARGIN_BUTTON + "px;");
    button->setCursor(Qt::PointingHandCursor);
}
