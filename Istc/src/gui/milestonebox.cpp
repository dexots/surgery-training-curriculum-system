#include "gui/milestonebox.h"

const QString MilestoneBox::COLOR_BUTTONBACKGROUND = "rgb(39, 174, 96)";
const QString MilestoneBox::COLOR_BUTTONBORDER = "white";
const QString MilestoneBox::COLOR_BUTTONFONT = "white";
const QString MilestoneBox::FONT_BUTTON = "bold 14";
const QString MilestoneBox::PADDING_BUTTON = "6";
const QString MilestoneBox::RADIUS_BUTTONBORDER = "10";
const QString MilestoneBox::STYLE_BUTTONBORDER = "outset";
const QString MilestoneBox::WIDTH_BUTTON = "50";
const QString MilestoneBox::WIDTH_BUTTONBORDER = "2";
const QString MilestoneBox::MESSAGE_MISSINGTITLE = "Title cannot be empty";
const QString MilestoneBox::MESSAGE_NUMBERLOGICERROR = "Completed number should not be larger than maximum number";

const QString MilestoneBox::QSTRING_EMPTY = "";

const int MilestoneBox::WIDTH_MINIMUM = 500;
const int MilestoneBox::HEIGHT_MINIMUM = 300;

MilestoneBox::MilestoneBox() {
    initializeVariable();
    setUpTitleLayout();
    setUpMaxLayout();
    setUpCompletedLayout();
    setUpErrorLabel();
    setUpAddAndCancelButton();
    setBoxAttribute();
}

MilestoneBox::~MilestoneBox() {
    delete _inputTitle;
    delete _inputMax;
    delete _inputCompleted;
    delete _titleLabel;
    delete _errorLabel;
    delete _addButton;
    delete _cancelButton;
    delete _titleLayout;
    delete _maxLayout;
    delete _completedLayout;
    delete _buttonLayout;
    delete _boxLayout;
}

void MilestoneBox::initializeVariable() {
    _inputTitle = new InputBox;
    _inputMax = new InputBox;
    _inputCompleted = new InputBox;

    connect(_inputTitle, SIGNAL(stringReady(QString)),
            this, SLOT(handleTitle(QString)));
    connect(_inputMax, SIGNAL(stringReady(QString)),
            this, SLOT(handleRequiredNumber(QString)));
    connect(_inputCompleted, SIGNAL(stringReady(QString)),
            this, SLOT(handleCompletedNumber(QString)));

    _titleLabel = new QLabel;
    _errorLabel = new QLabel;

    _addButton = new QPushButton;
    _cancelButton = new QPushButton;

    _titleString = QSTRING_EMPTY;
    _maxString = QSTRING_EMPTY;
    _completedString = QSTRING_EMPTY;

    _titleLayout = new QHBoxLayout;
    _maxLayout = new QHBoxLayout;
    _completedLayout = new QHBoxLayout;
    _buttonLayout = new QHBoxLayout;
    _boxLayout = new QVBoxLayout;

    _bold.setBold(true);
}

void MilestoneBox::setUpTitleLayout() {
    QLabel *title = new QLabel;
    title->setText("Title of Procedure: ");
    title->setFont(_bold);

    _inputTitle->setMaximumHeight(30);

    _titleLayout->addWidget(title);
    _titleLayout->addWidget(_inputTitle);
}

void MilestoneBox::setUpMaxLayout() {
    QLabel *max = new QLabel;
    max->setText("Num of Procedures to Complete ");
    max->setFont(_bold);

    _inputMax->setMaximumHeight(30);

    _maxLayout->addWidget(max);
    _maxLayout->addWidget(_inputMax);
}

void MilestoneBox::setUpCompletedLayout() {
    QLabel *label = new QLabel;
    label->setText("Num of Procedures Already Completed ");
    label->setFont(_bold);

    _inputCompleted->setMaximumHeight(30);

    _completedLayout->addWidget(label);
    _completedLayout->addWidget(_inputCompleted);
}

void MilestoneBox::setUpErrorLabel() {
    _errorLabel->setFont(_bold);
    changeColor(Qt::red, Qt::white, _errorLabel);
    _errorLabel->hide();
}

void MilestoneBox::setUpAddAndCancelButton() {
    //Update button
    _addButton = new QPushButton;
    _addButton->setText("Add");
    _addButton->setCursor(Qt::PointingHandCursor);
    setButtonStyleSheet(_addButton);
    connect (_addButton, SIGNAL(clicked()),
             this, SLOT(handleAdd()));
    _addButton->setSizePolicy(QSizePolicy::Fixed,
                              QSizePolicy::Fixed);

    //cancel
    _cancelButton = new QPushButton;
    _cancelButton->setText("Cancel");
    _cancelButton->setCursor(Qt::PointingHandCursor);
    setButtonStyleSheet(_cancelButton);
    connect (_cancelButton, SIGNAL(clicked()),
             this, SLOT(handleCancel()));
    _cancelButton->setSizePolicy(QSizePolicy::Fixed,
                                 QSizePolicy::Fixed);

    _buttonLayout->addWidget(_addButton);
    _buttonLayout->addWidget(_cancelButton);
    _buttonLayout->setSpacing(0);
}

void MilestoneBox::setBoxAttribute() {
    _titleLabel->setText("Add your own Procedure Milestone");
    _titleLabel->setFont(_bold);

    _boxLayout->addWidget(_titleLabel);
    _boxLayout->addLayout(_titleLayout);
    _boxLayout->addLayout(_maxLayout);
    _boxLayout->addLayout(_completedLayout);
    _boxLayout->addWidget(_errorLabel);
    _boxLayout->addLayout(_buttonLayout);

    this->setLayout(_boxLayout);
    this->setContentsMargins(10, 10, 10, 10);
    this->setMinimumWidth(WIDTH_MINIMUM);
    this->setMinimumHeight(HEIGHT_MINIMUM);

    this->setAttribute(Qt::WA_TranslucentBackground, true);
    this->setWindowFlags(Qt::FramelessWindowHint);
}

/*
 * Helper Functions
 */
void MilestoneBox::paintEvent(QPaintEvent *event) {
    QDialog::paintEvent(event);
    QPainter painter(this);
    QBrush brush(Qt::white);
    painter.setBrush(brush);
    painter.setPen(Qt::white);
    painter.drawRoundRect(QRect(0, 0, this->width(), this->height()));
}

void MilestoneBox::changeColor(QColor font, QColor back, QWidget *widget) {
    widget->setAutoFillBackground(true);
    QPalette pallete = widget->palette();
    pallete.setColor(widget->foregroundRole(), font);
    pallete.setColor(widget->backgroundRole(), back);
    widget->setPalette(pallete);
}

void MilestoneBox::setButtonStyleSheet(QPushButton *button) {
    button->setStyleSheet("background-color:" + COLOR_BUTTONBACKGROUND + ";"
                          "border-style:" + STYLE_BUTTONBORDER + ";"
                          "border-width:" + WIDTH_BUTTONBORDER + "px;"
                          "border-radius:" + RADIUS_BUTTONBORDER + "px;"
                          "border-color:" + COLOR_BUTTONBORDER + ";"
                          "font:" + FONT_BUTTON + "px;"
                          "color:" + COLOR_BUTTONFONT + ";"
                          "min-width:" + WIDTH_BUTTON + "px;"
                          "padding:" + PADDING_BUTTON + "px;");
}

/*
 * Private Slots
 */
void MilestoneBox::handleAdd() {
    _titleString = _inputTitle->toPlainText();
    int requiredNumber =_inputMax->toPlainText().toInt();
    int completedNumber = _inputCompleted->toPlainText().toInt();

    _errorLabel->clear();
    if (_titleString == QSTRING_EMPTY) {
        _errorLabel->setText(_errorLabel->text() + MESSAGE_MISSINGTITLE);
        _errorLabel->show();
    }

    if (requiredNumber < completedNumber) {
        _errorLabel->setText(_errorLabel->text() + MESSAGE_NUMBERLOGICERROR);
        _errorLabel->show();
    }

    if (_errorLabel->text() == QSTRING_EMPTY) {
        emit this->passInfo(_titleString, completedNumber, requiredNumber);
        QDialog::done(0);
    }
}

void MilestoneBox::handleCancel() {
    QDialog::done(0);
}

void MilestoneBox::handleTitle(QString inputString) {
    _inputTitle->setText(inputString);
}

void MilestoneBox::handleRequiredNumber(QString inputString) {
    bool isNumeric;
    inputString.toInt(&isNumeric);
    if(isNumeric) {
        _inputMax->setText(inputString);
    }
}

void MilestoneBox::handleCompletedNumber(QString inputString) {
    bool isNumeric;
    inputString.toInt(&isNumeric);
    if(isNumeric) {
        _inputCompleted->setText(inputString);
    }
}
