#include "gui/trainingwidget.h"

TrainingWidget::TrainingWidget() {
    createLayout();
}

TrainingWidget::~TrainingWidget() {
}

void TrainingWidget::createLayout() {
    getUserRole();

    if (_userRole == UserDetails::ROLE_TRAINEE) {
        addMilestoneWidget();
    }
    addActivityWidget();
    if (_userRole == UserDetails::ROLE_TRAINEE) {
        addExaminationWidget();
    }
    addFormsWidget();

    CateTable *trainingTable = new CateTable("Training",
                                             "",
                                             ":/icon/ButtonIcon/Training Icon.png",
                                             QColor (46, 204, 113, 255),
                                             _trainingSubCategories);
    connect(trainingTable, SIGNAL(backToHome()), this, SLOT(handleHomeSignal()));

    _widgetLayout = new QVBoxLayout;
    _widgetLayout->addWidget(trainingTable);
    _widgetLayout->setContentsMargins(0, 0, 0, 0);
    this->setLayout(_widgetLayout);

}

void TrainingWidget::addMilestoneWidget()
{
    qDebug() << "TrainingWidget -> addMilestoneWidget : adding milestone widget";
    MilestoneWidget *milestoneWidget = new MilestoneWidget();
    TrainingWidget::colorWidget(QColor (149, 231, 184, 255), milestoneWidget);

    _milestoneCate = new SubCate("Training Requirements",
                                        "View your Training Progress",
                                        "",
                                        Qt::gray, //desc font color
                                        QColor (214, 246, 228, 255), //default color
                                        QColor (149, 231, 184, 255), //selected color
                                        milestoneWidget);
    _trainingSubCategories.append(_milestoneCate);
    qDebug() << "TrainingWidget -> addMilestoneWidget : added milestone widget";
}

void TrainingWidget::addActivityWidget()
{
    qDebug() << "TrainingWidget -> addActivityWidget : adding activity widget";
    ActivityWidget *activityWidget = new ActivityWidget();
    TrainingWidget::colorWidget(QColor (149, 231, 184, 255),
                              activityWidget);

    connect(this, SIGNAL(updateActivityTable()), activityWidget, SLOT(updateTable()));

    _activityCate = new SubCate("Workshops",
                                        "Check upcoming activities",
                                        "",
                                        Qt::gray, //desc font color
                                        QColor (214, 246, 228, 255), //default color
                                        QColor (149, 231, 184, 255), //selected color
                                        activityWidget);
    _trainingSubCategories.append(_activityCate);
    qDebug() << "TrainingWidget -> addActivityWidget : added activity widget";
}

void TrainingWidget::addExaminationWidget()
{
    qDebug() << "TrainingWidget -> addExaminationWidget : adding examination widget";
    ExaminationWidget *examinationWidget = new ExaminationWidget();
    TrainingWidget::colorWidget(QColor (149, 231, 184, 255),
                              examinationWidget);

    connect(this, SIGNAL(updateExamTable()), examinationWidget, SLOT(updateTable()));

    _examCate = new SubCate("Examination",
                                    "View your examination schedule",
                                    "",
                                    Qt::gray, //desc font color
                                    QColor (214, 246, 228, 255), //default color
                                    QColor (149, 231, 184, 255), //selected color
                                    examinationWidget);
    _trainingSubCategories.append(_examCate);
    qDebug() << "TrainingWidget -> addExaminationWidget : added examination widget";
}

void TrainingWidget::addFormsWidget()
{
    qDebug() << "TrainingWidget -> addFormsWidget: adding forms widget";
    FormsWidget *formsWidget = new FormsWidget();
    TrainingWidget::colorWidget(QColor (149, 231, 184, 255),
                              formsWidget);

    connect(this, SIGNAL(updateFormsTable()), formsWidget, SLOT(updateTable()));

    _formCate = new SubCate("Forms",
                                    "Submit evaluation forms",
                                    "",
                                    Qt::gray, //desc font color
                                    QColor (214, 246, 228, 255), //default color
                                    QColor (149, 231, 184, 255), //selected color
                                    formsWidget);
    _trainingSubCategories.append(_formCate);
    qDebug() << "TrainingWidget -> addFormsWidget : added forms widget";
}

void TrainingWidget::colorWidget(QColor color, QWidget *widget) {
    widget->setAutoFillBackground(true);
    QPalette pallete = widget->palette();
    pallete.setColor(widget->backgroundRole(), color);
    widget->setPalette(pallete);
}

void TrainingWidget::getUserRole() {
    qDebug() << "TrainingWidget -> getUserRole: Getting user role";
    UserDetails *userDetails = UserDetails::getObject();
    _userRole = userDetails->getUserRole();
    qDebug() << "TrainingWidget -> getUserRole : user role is " << _userRole;
}

//private slot
void TrainingWidget::handleHomeSignal() {
    emit this->backToHome();
}

//public slot
void TrainingWidget::updateAllTables() {
    emit updateActivityTable();
    emit updateFormsTable();
    if (_userRole == UserDetails::ROLE_TRAINEE) {
        emit updateExamTable();
    }
}
