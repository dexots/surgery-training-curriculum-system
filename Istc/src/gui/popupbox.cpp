#include "gui/popupbox.h"

const QString PopupBox::COLOR_BUTTONBACKGROUND = "rgb(39, 174, 96)";
const QString PopupBox::COLOR_BUTTONBORDER = "white";
const QString PopupBox::COLOR_BUTTONFONT = "white";
const QString PopupBox::FONT_BUTTON = "bold 14";
const QString PopupBox::PADDING_BUTTON = "6";

const QString PopupBox::RADIUS_BUTTONBORDER = "10";
const QString PopupBox::STYLE_BUTTONBORDER = "outset";

const QString PopupBox::WIDTH_BUTTON = "50";
const QString PopupBox::WIDTH_BUTTONBORDER = "2";

int PopupBox::WIDTH_MINIMUM = 500;
int PopupBox::HEIGHT_MINIMUM = 300;

PopupBox::PopupBox() {
    this->setAttribute(Qt::WA_TranslucentBackground, true);
    this->setWindowFlags(Qt::FramelessWindowHint);
}

PopupBox::~PopupBox() {

}

void PopupBox::paintEvent(QPaintEvent *event) {
    QDialog::paintEvent(event);
    QPainter painter(this);
    QBrush brush(Qt::white);
    painter.setBrush(brush);
    painter.setPen(Qt::white);
    painter.drawRoundRect(QRect(0, 0, this->width(), this->height()));
}

void PopupBox::setWidth(int width) {
    WIDTH_MINIMUM = width;
}

void PopupBox::setHeight(int height) {
    HEIGHT_MINIMUM = height;
}

void PopupBox::changeColor(QColor font, QColor back, QWidget *widget) {
    widget->setAutoFillBackground(true);
    QPalette pallete = widget->palette();
    pallete.setColor(widget->foregroundRole(), font);
    pallete.setColor(widget->backgroundRole(), back);
    widget->setPalette(pallete);
}

void PopupBox::setButtonStyleSheet(QPushButton *button) {
    button->setStyleSheet("background-color:" + COLOR_BUTTONBACKGROUND + ";"
                          "border-style:" + STYLE_BUTTONBORDER + ";"
                          "border-width:" + WIDTH_BUTTONBORDER + "px;"
                          "border-radius:" + RADIUS_BUTTONBORDER + "px;"
                          "border-color:" + COLOR_BUTTONBORDER + ";"
                          "font:" + FONT_BUTTON + "px;"
                          "color:" + COLOR_BUTTONFONT + ";"
                          "min-width:" + WIDTH_BUTTON + "px;"
                          "padding:" + PADDING_BUTTON + "px;");
    button->setCursor(Qt::PointingHandCursor);
}

// To be overrided
void PopupBox::handleButton() {

}

void PopupBox::handleCancel() {
    QDialog::done(0);
}

