#include "gui/researchaddbox.h"

const QString ResearchAddBox::RESEARCH_FILE = "research.txt";

const QString ResearchAddBox::STATES_CONST[7] = {
    "Planning",
    "DSRB",
    "Data Collection",
    "Analysis",
    "Submission",
    "Drafting",
    "Accepted"
};

const QString ResearchAddBox::COLOR_BUTTONBACKGROUND = "rgb(39, 174, 96)";
const QString ResearchAddBox::COLOR_BUTTONBORDER = "white";
const QString ResearchAddBox::COLOR_BUTTONFONT = "white";
const QString ResearchAddBox::FONT_BUTTON = "bold 14";
const QString ResearchAddBox::PADDING_BUTTON = "6";
const QString ResearchAddBox::RADIUS_BUTTONBORDER = "10";
const QString ResearchAddBox::STYLE_BUTTONBORDER = "outset";
const QString ResearchAddBox::WIDTH_BUTTON = "50";
const QString ResearchAddBox::WIDTH_BUTTONBORDER = "2";

const QString ResearchAddBox::QSTRING_EMPTY = "";

const int ResearchAddBox::WIDTH_MINIMUM = 500;
const int ResearchAddBox::HEIGHT_MINIMUM = 300;

ResearchAddBox::ResearchAddBox() {
    _isEdit = false;
    initializeVariable();
    initializeEmptyStrings();
    doBasicSetup();
}

ResearchAddBox::ResearchAddBox(QString titleString,
                QString descString, QString collabString,
                 QString folderString, QString statusString,
                 QString initiatedString, QString teamLead) {

    _isEdit = true;
    initializeVariable();
    initializePreStrings(titleString, descString, collabString,
                         folderString, statusString,
                         initiatedString, teamLead);
    doBasicSetup();
}

ResearchAddBox::~ResearchAddBox() {
    delete _inputTitle;
    delete _inputDesc;
    delete _inputCollab;
    delete _inputFolder;
    delete _inputTeamLead;

    delete _statusBox;

    delete _errorLabel;
    delete _titleLabel;

    delete _addButton;
    delete _cancelButton;

    delete _titleLayout;
    delete _descLayout;
    delete _collabLayout;
    delete _statusLayout;
    delete _descLayout;
    delete _initiatedLayout;
    delete _teamLeadLayout;
    delete _folderLayout;
    delete _buttonLayout;
    delete _boxLayout;
}


void ResearchAddBox::initializeVariable() {
    _inputTitle = new InputBox;
    _inputTeamLead = new InputBox;
    _inputDesc = new InputBox;
    _inputCollab = new InputBox;
    _inputFolder = new InputBox;

    _statusBox = new QComboBox;

    _errorLabel = new QLabel;
    _titleLabel = new QLabel;
    _warningLabel = new QLabel;

    _addButton = new QPushButton;
    _cancelButton = new QPushButton;

    _titleLayout = new QHBoxLayout;
    _descLayout = new QHBoxLayout;
    _collabLayout = new QHBoxLayout;
    _statusLayout = new QHBoxLayout;
    _initiatedLayout = new QHBoxLayout;
    _folderLayout = new QHBoxLayout;
    _teamLeadLayout = new QHBoxLayout;
    _buttonLayout = new QHBoxLayout;
    _boxLayout = new QVBoxLayout;

    _bold.setBold(true);
}

void ResearchAddBox::initializeEmptyStrings() {
    _titleString = QSTRING_EMPTY;
    _descString = QSTRING_EMPTY;
    _collabString = QSTRING_EMPTY;
    _folderString = QSTRING_EMPTY;
    _statusString = QSTRING_EMPTY;
    _initiatedString = QSTRING_EMPTY;
    _teamLead = QSTRING_EMPTY;
    _warningString = QSTRING_EMPTY;
}

void ResearchAddBox::initializePreStrings(QString titleString,
                QString descString, QString collabString,
                 QString folderString, QString statusString,
                 QString initiatedString, QString teamLead) {

    _titleString = titleString;
    _descString = descString;
    _collabString = collabString;
    _folderString = folderString;
    _statusString = statusString;
    _initiatedString = initiatedString;
    _teamLead = teamLead;
}

void ResearchAddBox::doBasicSetup() {
    setUpTitleLayout();
    setUpDescLayout();
    setUpCurrProjStatus();
    setUpInitiatedOnLayout();
    setUpTeamLead();
    setUpCollaboratorsLayout();
    setUpFolderLink();
    setUpAddAndCancelButton();
    setBoxAttribute();
    setUpErrorLabel();
    setUpWarningLabel();
}

void ResearchAddBox::setUpTitleLayout() {
    QLabel *label = new QLabel;
    label->setText("Title of Project: ");
    label->setFont(_bold);

    _inputTitle->setMaximumHeight(30);
    _inputTitle->setText(_titleString);

    connect(_inputTitle, SIGNAL(stringReady(QString)),
            this, SLOT(handleTitle(QString)));

    _titleLayout->addWidget(label);
    _titleLayout->addWidget(_inputTitle);
}

void ResearchAddBox::setUpDescLayout() {
    QLabel *label = new QLabel;
    label->setText("Description: ");
    label->setFont(_bold);

    _inputDesc->setMaximumHeight(50);
    _inputDesc->setText(_descString);
    //_inputDesc->setVerticalScrollBar(Qt::ScrollBarAlwaysOff);

    connect(_inputDesc, SIGNAL(stringReady(QString)),
            this, SLOT(handleDescription(QString)));

    _descLayout->addWidget(label);
    _descLayout->addWidget(_inputDesc);
}

void ResearchAddBox::setUpCurrProjStatus() {
    QLabel *label = new QLabel;
    label->setText("Current Project Status: ");
    label->setFont(_bold);

    for (int i=0; i<7; i++) {
        _statusBox->addItem(STATES_CONST[i]);
    }

    if (_statusString != QSTRING_EMPTY) {
        _statusBox->setCurrentText(_statusString);
    }

    _statusLayout->addWidget(label);
    _statusLayout->addWidget(_statusBox);
}

void ResearchAddBox::setUpErrorLabel() {
    _errorLabel->setFont(_bold);
    changeColor(Qt::red, Qt::white, _errorLabel);
    _errorLabel->hide();
}

void ResearchAddBox::setUpWarningLabel() {
    _warningLabel->setFont(_bold);
    changeColor(Qt::red, Qt::white, _warningLabel);
    _warningLabel->setText("After adding new project, application will restart automatically");
}

void ResearchAddBox::setUpTeamLead() {
    QLabel *label = new QLabel;
    label->setText("Team Lead: ");
    label->setFont(_bold);

    _inputTeamLead->setMaximumHeight(50);
    _inputTeamLead->setText(_teamLead);
    //_inputTeamLead->setHorizontalScrollBar(Qt::ScrollBarAsNeeded);

    connect(_inputTeamLead, SIGNAL(stringReady(QString)),
            this, SLOT(handleTeamLead(QString)));


    _teamLeadLayout->addWidget(label);
    _teamLeadLayout->addWidget(_inputTeamLead);
}

void ResearchAddBox::setUpInitiatedOnLayout() {
    if (_initiatedString == QSTRING_EMPTY) {
        QDate date = QDate::currentDate();
        _initiatedString = date.toString("dd MMM yyyy");
    }

    QLabel *label = new QLabel;
    label->setText("Initated On: " + _initiatedString);
    label->setFont(_bold);

    _initiatedLayout->addWidget(label);
}

void ResearchAddBox::setUpCollaboratorsLayout() {
    QLabel *label = new QLabel;
    label->setText("Collaborators (comma separated): ");
    label->setFont(_bold);

    _inputCollab->setMaximumHeight(50);
    _inputCollab->setText(_collabString);
    //_inputCollab->setVerticalScrollBar(Qt::ScrollBarAsNeeded);

    connect(_inputCollab, SIGNAL(stringReady(QString)),
            this, SLOT(handleCollaborators(QString)));

    _collabLayout->addWidget(label);
    _collabLayout->addWidget(_inputCollab);
}

void ResearchAddBox::setUpFolderLink() {
    QLabel *folder = new QLabel;
    folder->setText("Folder Link: ");
    folder->setFont(_bold);

    _inputFolder->setMaximumHeight(30);
    _inputFolder->setText(_folderString);

    connect(_inputFolder, SIGNAL(stringReady(QString)),
            this, SLOT(handleFolderLink(QString)));

    _folderLayout->addWidget(folder);
    _folderLayout->addWidget(_inputFolder);
}

void ResearchAddBox::setUpAddAndCancelButton() {
    //Update button
    _addButton = new QPushButton;
    if (_isEdit) {
        _addButton->setText("Edit");
    } else {
        _addButton->setText("Add");
    }
    _addButton->setCursor(Qt::PointingHandCursor);
    setButtonStyleSheet(_addButton);
    connect (_addButton, SIGNAL(clicked()),
             this, SLOT(handleAdd()));
    _addButton->setSizePolicy(QSizePolicy::Fixed,
                              QSizePolicy::Fixed);

    //cancel
    _cancelButton = new QPushButton;
    _cancelButton->setText("Cancel");
    _cancelButton->setCursor(Qt::PointingHandCursor);
    setButtonStyleSheet(_cancelButton);
    connect (_cancelButton, SIGNAL(clicked()),
             this, SLOT(handleCancel()));
    _cancelButton->setSizePolicy(QSizePolicy::Fixed,
                                 QSizePolicy::Fixed);

    _buttonLayout->addWidget(_addButton);
    _buttonLayout->addWidget(_cancelButton);
    _buttonLayout->setSpacing(0);
}

void ResearchAddBox::setBoxAttribute() {
    _titleLabel->setText("Adding a new Research Project");
    _titleLabel->setFont(_bold);

    _boxLayout->addWidget(_titleLabel);
    _boxLayout->addLayout(_initiatedLayout);
    _boxLayout->addLayout(_titleLayout);
    _boxLayout->addLayout(_descLayout);
    _boxLayout->addLayout(_statusLayout);
    _boxLayout->addLayout(_teamLeadLayout);
    _boxLayout->addLayout(_collabLayout);
    _boxLayout->addLayout(_folderLayout);
    _boxLayout->addWidget(_errorLabel);
    _boxLayout->addWidget(_warningLabel);
    _boxLayout->addLayout(_buttonLayout);

    this->setLayout(_boxLayout);
    this->setContentsMargins(10, 10, 10, 10);
    this->setMinimumWidth(WIDTH_MINIMUM);
    this->setMinimumHeight(HEIGHT_MINIMUM);

    this->setAttribute(Qt::WA_TranslucentBackground, true);
    this->setWindowFlags(Qt::FramelessWindowHint);
}

/*
 * Helper Methods
 */
void ResearchAddBox::paintEvent(QPaintEvent *event) {
    QDialog::paintEvent(event);
    QPainter painter(this);
    QBrush brush(Qt::white);
    painter.setBrush(brush);
    painter.setPen(Qt::white);
    painter.drawRoundRect(QRect(0, 0, this->width(), this->height()));
}

void ResearchAddBox::changeColor(QColor font, QColor back, QWidget *widget) {
    widget->setAutoFillBackground(true);
    QPalette pallete = widget->palette();
    pallete.setColor(widget->foregroundRole(), font);
    pallete.setColor(widget->backgroundRole(), back);
    widget->setPalette(pallete);
}

void ResearchAddBox::setButtonStyleSheet(QPushButton *button) {
    button->setStyleSheet("background-color:" + COLOR_BUTTONBACKGROUND + ";"
                          "border-style:" + STYLE_BUTTONBORDER + ";"
                          "border-width:" + WIDTH_BUTTONBORDER + "px;"
                          "border-radius:" + RADIUS_BUTTONBORDER + "px;"
                          "border-color:" + COLOR_BUTTONBORDER + ";"
                          "font:" + FONT_BUTTON + "px;"
                          "color:" + COLOR_BUTTONFONT + ";"
                          "min-width:" + WIDTH_BUTTON + "px;"
                          "padding:" + PADDING_BUTTON + "px;");
}

/*
 * Private Slots
 */
void ResearchAddBox::handleAdd() {
    QString labelText = QSTRING_EMPTY;

    _titleString = _inputTitle->toPlainText();
    _descString = _inputDesc->toPlainText();
    _collabString = _inputCollab->toPlainText();
    _teamLead = _inputTeamLead->toPlainText();
    _statusString = QString::number(_statusBox->currentIndex());

    _folderString = _inputFolder->toPlainText();
    if (_folderString != QSTRING_EMPTY) {
        QString frontString = _folderString.left(6);
        if (frontString != "https://") {
            QString frontString = _folderString.left(5);
            if (frontString != "http://") {
                _folderString = "http://" + _folderString;
            }
        }
    }

    if (_titleString == QSTRING_EMPTY) {
        labelText += "Title Empty\n";
        _errorLabel->setText(labelText);
        _errorLabel->show();
    } else {
        if (_isEdit) {
            emit this->editedInfo(_titleString, _descString,
                                  _collabString, _folderString,
                                  _statusString, _initiatedString,
                                  _teamLead);
        } else {
            writeToFile();
        }

        QDialog::done(0);
    }
}

void ResearchAddBox::handleCancel() {
    QDialog::done(0);
}

void ResearchAddBox::writeToFile() {
    QFile file(RESEARCH_FILE);

    QString filename = _titleString.replace(" ", "");
    QFile researchFile("./" + filename+".txt");

    // Read file to end of research file
    QString data = QSTRING_EMPTY;
    if (!file.open(QFile::ReadOnly | QFile::Text)) {
        assert(false);
        return;
    } else {
        QTextStream in(&file);
        data = in.readAll();
        file.close();
    }

    // Write to end of file
    if (!file.open(QFile::WriteOnly | QFile::Text)) {
        assert(false);
        return;
    } else {
        QTextStream out(&file);
        out << data << "\n" << _titleString
            << ":==./" << filename << ".txt\n";
        file.close();
    }

    // Write to research file
    if (!researchFile.open(QFile::WriteOnly | QFile::Text)) {
        assert(false);
        return;
    } else {
        QTextStream out(&researchFile);
        out << _titleString << "\n" << _descString << "\n" <<
               _initiatedString << "\n" << _collabString << "\n"
            << _teamLead << "\n" << _statusString << "\n"
            << _folderString << "\nPlanning Details\n" << "DSRB Details\n"
            << "Data Details\n" << "Analysis Details\n"
            << "Submission Details\n" << "Drafting Details\n"
            << "Accepted Details";

        researchFile.close();
    }

    //showSuccessMsg();

    //restart application
    qApp->quit();
    QProcess::startDetached(qApp->arguments()[0], qApp->arguments());
}

void ResearchAddBox::showSuccessMsg() {
    GeneralPopupBox *successBox = new GeneralPopupBox("", 3);
    successBox->exec();
}

void ResearchAddBox::handleTitle(QString inputString) {
    _inputTitle->setText(inputString);
}

void ResearchAddBox::handleDescription(QString inputString) {
    _inputDesc->setText(inputString);
}

void ResearchAddBox::handleCollaborators(QString inputString) {
    _inputCollab->setText(inputString);
}

void ResearchAddBox::handleFolderLink(QString inputString) {
    _inputFolder->setText(inputString);
}

void ResearchAddBox::handleTeamLead(QString inputString) {
    _inputTeamLead->setText(inputString);
}
