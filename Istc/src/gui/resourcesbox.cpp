#include "gui/resourcesbox.h"

const QString ResourcesBox::COLOR_BUTTONBACKGROUND = "rgb(39, 174, 96)";
const QString ResourcesBox::COLOR_BUTTONBORDER = "white";
const QString ResourcesBox::COLOR_BUTTONFONT = "white";
const QString ResourcesBox::FONT_BUTTON = "bold 14";
const QString ResourcesBox::PADDING_BUTTON = "6";
const QString ResourcesBox::RADIUS_BUTTONBORDER = "10";
const QString ResourcesBox::STYLE_BUTTONBORDER = "outset";
const QString ResourcesBox::STRING_SPACE = " ";
const QString ResourcesBox::WIDTH_BUTTON = "50";
const QString ResourcesBox::WIDTH_BUTTONBORDER = "2";
const QString ResourcesBox::QSTRING_EMPTY = "";

const int ResourcesBox::WIDTH_MINIMUM = 500;
const int ResourcesBox::HEIGHT_MINIMUM = 250;

ResourcesBox::ResourcesBox(QString resourceType) {
    _resourceType = resourceType;
    initializeVariable();
    setUpText();
    setUpLink();
    setUpErrorLabel();
    setUpAddAndCancelButton();
    setUpLayout();
}

ResourcesBox::~ResourcesBox() {
    delete _errorLabel;
    delete _textLabel;
    delete _linkLabel;
    delete _inputTitle;
    delete _inputLink;
    delete _linkLayout;
    delete _titleLayout;
    delete _buttonLayout;
    delete _mainLayout;
}

void ResourcesBox::initializeVariable() {
    _errorLabel = new QLabel;
    _textLabel = new QLabel;
    _linkLabel = new QLabel;

    _inputTitle = new QTextEdit;
    _inputLink = new QTextEdit;

    _addButton = new QPushButton;
    _cancelButton = new QPushButton;

    _titleString = QSTRING_EMPTY;
    _linkString = QSTRING_EMPTY;

    _linkLayout = new QHBoxLayout;
    _titleLayout = new QHBoxLayout;
    _buttonLayout = new QHBoxLayout;
    _mainLayout = new QVBoxLayout;

    _bold.setBold(true);
}

void ResourcesBox::setUpText() {
    _textLabel->setText("Name of " + _resourceType + ": ");
    _textLabel->setFont(_bold);

    _inputTitle->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    _inputTitle->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    _inputTitle->setFrameStyle(QFrame::Box);
    _inputTitle->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
    _inputTitle->setFixedHeight(50);
    _inputTitle->setFixedWidth(300);
    _inputTitle->setPlaceholderText("E.g. Cardiac Journals of NUH");
    _inputTitle->setLineWrapMode(QTextEdit::WidgetWidth);

    _titleLayout->addWidget(_textLabel);
    _titleLayout->addWidget(_inputTitle);
}

void ResourcesBox::setUpLink() {
    _linkLabel->setText("Link: ");
    _linkLabel->setFont(_bold);

    _inputLink->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    _inputLink->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    _inputLink->setFrameStyle(QFrame::Box);
    _inputLink->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
    _inputLink->setFixedHeight(50);
    _inputLink->setFixedWidth(300);
    _inputLink->setPlaceholderText("E.g. http://nuhs.edu.sg");
    _inputLink->setLineWrapMode(QTextEdit::WidgetWidth);


    _linkLayout->addWidget(_linkLabel);
    _linkLayout->addWidget(_inputLink);
}

void ResourcesBox::setUpErrorLabel() {
    _errorLabel->setFont(_bold);
    changeColor(Qt::red, Qt::white, _errorLabel);
    _errorLabel->hide();
}

void ResourcesBox::setUpAddAndCancelButton() {
    //Add button
    _addButton = new QPushButton;
    _addButton->setText("Add");
    _addButton->setCursor(Qt::PointingHandCursor);
    setButtonStyleSheet(_addButton);
    connect (_addButton, SIGNAL(clicked()),
             this, SLOT(handleAdd()));
    _addButton->setSizePolicy(QSizePolicy::Fixed,
                              QSizePolicy::Fixed);

    //cancel
    _cancelButton = new QPushButton;
    _cancelButton->setText("Cancel");
    _cancelButton->setCursor(Qt::PointingHandCursor);
    setButtonStyleSheet(_cancelButton);
    connect (_cancelButton, SIGNAL(clicked()),
             this, SLOT(handleCancel()));
    _cancelButton->setSizePolicy(QSizePolicy::Fixed,
                                 QSizePolicy::Fixed);

    _buttonLayout->addWidget(_addButton);
    _buttonLayout->addWidget(_cancelButton);
    _buttonLayout->setSpacing(0);
}

void ResourcesBox::setUpLayout() {
    _mainLayout->addLayout(_titleLayout);
    _mainLayout->addLayout(_linkLayout);
    _mainLayout->addLayout(_buttonLayout);
    _mainLayout->addWidget(_errorLabel);

    this->setLayout(_mainLayout);
    this->setContentsMargins(10, 10, 10, 10);
    this->setMinimumWidth(WIDTH_MINIMUM);
    this->setMinimumHeight(HEIGHT_MINIMUM);

    this->setAttribute(Qt::WA_TranslucentBackground, true);
    this->setWindowFlags(Qt::FramelessWindowHint);
}

/*
 * Helper functions
 */

void ResourcesBox::changeColor(QColor font, QColor back, QWidget *widget) {
    widget->setAutoFillBackground(true);
    QPalette pallete = widget->palette();
    pallete.setColor(widget->foregroundRole(), font);
    pallete.setColor(widget->backgroundRole(), back);
    widget->setPalette(pallete);
}

void ResourcesBox::setButtonStyleSheet(QPushButton *button) {
    button->setStyleSheet("background-color:" + COLOR_BUTTONBACKGROUND + ";"
                          "border-style:" + STYLE_BUTTONBORDER + ";"
                          "border-width:" + WIDTH_BUTTONBORDER + "px;"
                          "border-radius:" + RADIUS_BUTTONBORDER + "px;"
                          "border-color:" + COLOR_BUTTONBORDER + ";"
                          "font:" + FONT_BUTTON + "px;"
                          "color:" + COLOR_BUTTONFONT + ";"
                          "min-width:" + WIDTH_BUTTON + "px;"
                          "padding:" + PADDING_BUTTON + "px;");
}

void ResourcesBox::paintEvent(QPaintEvent *event) {
    QDialog::paintEvent(event);
    QPainter painter(this);
    QBrush brush(Qt::white);
    painter.setBrush(brush);
    painter.setPen(Qt::white);
    painter.drawRoundRect(QRect(0, 0, this->width(), this->height()));
}

/*
 * Private slots
 */
void ResourcesBox::handleAdd() {
    QVector<QString> info;
    _titleString = _inputTitle->toPlainText();
    _linkString = _inputLink->toPlainText();

    if (_titleString == QSTRING_EMPTY && _linkString == QSTRING_EMPTY) {
        _errorLabel->show();
        _errorLabel->setText("Please add a title and hyperlink");
    } else if (_titleString == QSTRING_EMPTY) {
        _errorLabel->show();
        _errorLabel->setText("Title of link required");
    } else if (_linkString == QSTRING_EMPTY) {
        _errorLabel->show();
        _errorLabel->setText("Hyper link required");
    } else {
        info.append(_titleString);
        info.append(_linkString);

        emit this->passInfo(info);
        QDialog::done(0);
    }
}

void ResourcesBox::handleCancel() {
    QDialog::done(0);
}
