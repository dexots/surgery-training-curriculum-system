#include "gui/activitywidget.h"

const QVector<int> ActivityWidget::WIDTH_COLUMN_TRAINEE (QVector<int>() << 30 << 210 << 90 << 90 << 180 << 100 << 100);
const QVector<int> ActivityWidget::WIDTH_COLUMN_TRAINER (QVector<int>() << 70 << 30 << 210 << 90 << 90 << 100 << 90 << 90);

const QVector<QString> ActivityWidget::HEADER_TRAINEETABLE (QVector<QString>() << "No" << "Title" << "Date" << "Time"
                                                        << "Trainer" << "Vacancies" << "Status");
const QVector<QString> ActivityWidget::HEADER_TRAINERTABLE (QVector<QString>() << "" << "No" << "Title" << "Date" << "Time"
                                                        << "Trainer" << "Vacancies" << "Status");

const int ActivityWidget::NUMBER_COLUMN_TRAINEE = 7;
const int ActivityWidget::NUMBER_COLUMN_TRAINER = 8;

const QColor ActivityWidget::COLOR_CONTENTBACKGROUND = QColor(235, 241, 233);
const QColor ActivityWidget::COLOR_CONTENTFONT = QColor(Qt::black);
const QColor ActivityWidget::COLOR_HEADERBACKGROUND = QColor(112, 173, 71);
const QColor ActivityWidget::COLOR_HEADERTEXT = QColor(Qt::white);
const QColor ActivityWidget::COLOR_IMPORTANTCONTENTBACKGROUND = QColor(199, 216, 192);

const QString ActivityWidget::FORMAT_DATE = "dd MMM yyyy";
const QString ActivityWidget::FORMAT_TIME = "hhmm";
const QString ActivityWidget::ICON_ADD = QString::fromUtf8(":/icon/ButtonIcon/add.png");
const QString ActivityWidget::LABEL_ADDACTIVITY = "Add Activity";
const QString ActivityWidget::LABEL_CLOSEDEVENT = "Closed";
const QString ActivityWidget::LABEL_REGISTERD = "Registered";
const QString ActivityWidget::LABEL_NOTREGISTERD = "Not registered";
const QString ActivityWidget::LABEL_OPENEVENT = "Open";
const QString ActivityWidget::MESSAGE_OUTOFVACANCY = "Registration failed! Out of Vacancy!";
const QString ActivityWidget::STRING_EMPTY = "";

ActivityWidget::ActivityWidget() {
    qDebug() << "ActivityWidget : Constructing Activity Widget";
    _dummyReducedHeight = 0;
    _isTimeToUpdate = false;
    getUserRole();
    setUpActivityLayout();
    setUpAddIconForTrainer();
    setUpActivityTable();
    setUpTableContentFromEventItems();
    setUpDummyWidget();
    setLayout(_activityLayout);
    qDebug() << "ActivityWidget : Activity widget is constructed";
}

ActivityWidget::~ActivityWidget() {
    delete _activityLayout;
    delete _activityTable;
    delete _dummyWidget;
}

//private
void ActivityWidget::getUserRole() {
    UserDetails *userDetails = UserDetails::getObject();
    _userRole = userDetails->getUserRole();
}

void ActivityWidget::setUpActivityLayout() {
    qDebug() << "ActivityWidget -> setUpActivityLayout : Initializing activity widgetlayout";
    _activityLayout = new QVBoxLayout;
    _activityLayout->setAlignment(Qt::AlignCenter | Qt::AlignTop);
    qDebug() << "ActivityWidget -> setUpActivityLayout : Activity widget layout is initialized";
}

// Only for TRAINER
void ActivityWidget::setUpAddIconForTrainer() {
    if (_userRole == UserDetails::ROLE_TRAINER) {
        qDebug() << "ActivityWidget -> setUpActivityLayout : Initializing add icon for trainer";
        QWidget *alignWidget = new QWidget;
        alignWidget->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Fixed);

        QLabel *addActivity = new QLabel;
        addActivity->setText(LABEL_ADDACTIVITY);
        addActivity->setSizePolicy(QSizePolicy::Fixed,
                                   QSizePolicy::Fixed);

        QLabel *addLogo = new QLabel;
        addLogo->setPixmap(QPixmap(ICON_ADD));
        addLogo->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

        ClickableWidget *addClick = new ClickableWidget(addLogo);

        connect(addClick, SIGNAL(isClicked()),
                this, SLOT(addActivity()));

        QHBoxLayout *addLayout = new QHBoxLayout;
        addLayout->addWidget(alignWidget, 10);
        addLayout->addWidget(addActivity, 0);
        addLayout->addWidget(addClick);

        QWidget *fixedWidget = new QWidget;
        fixedWidget->setLayout(addLayout);
        fixedWidget->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Fixed);
        _activityLayout->addWidget(fixedWidget, 0);
        qDebug() << "ActivityWidget -> setUpActivityLayout : Add icon initialized for trainer";
    }
}

void ActivityWidget::setUpActivityTable() {
    qDebug() << "ActivityWidget -> setUpActivityTable : Initializing activity table";
    if (_userRole == UserDetails::ROLE_TRAINEE) {
        _activityTable = new TableWidget(true, NUMBER_COLUMN_TRAINEE, WIDTH_COLUMN_TRAINEE);
        _activityTable->setHeader(HEADER_TRAINEETABLE, COLOR_HEADERTEXT, COLOR_HEADERBACKGROUND);
    } else {
        _activityTable = new TableWidget(true, NUMBER_COLUMN_TRAINER, WIDTH_COLUMN_TRAINER);
        _activityTable->setHeader(HEADER_TRAINERTABLE, COLOR_HEADERTEXT, COLOR_HEADERBACKGROUND);
    }

    _activityTable->setContBackground(COLOR_CONTENTBACKGROUND);
    _activityTable->setContFont(COLOR_CONTENTFONT);

    _activityLayout->addWidget(_activityTable);


    qDebug() << "ActivityWidget -> setUpActivityTable : Activity table is initialized";
}

void ActivityWidget::colorRowIfImportantEvent(int i)
{
    if (_events.at(i)->getIsImportant()) {
        int nColumn;
        if (_userRole == UserDetails::ROLE_TRAINEE) {
            nColumn = NUMBER_COLUMN_TRAINEE;
        } else {
            nColumn = NUMBER_COLUMN_TRAINER;
        }

        for (int j = 0; j < nColumn; j ++) {
            _activityTable->colorCell(i, j, COLOR_CONTENTFONT, COLOR_IMPORTANTCONTENTBACKGROUND);
        }
    } else {
        // do nothing
    }
}

void ActivityWidget::setUpTableContentFromEventItems() {
    qDebug()  << "ActivityWidget -> setUpTableContentFromEventItems : Setting up activity table with event items";
    _trainingModel = TrainingModel::getObject();
    _events = _trainingModel->getFutureTrainingActivities();

    qDebug() << "ActivityWidget -> setUpTableContentFromEventItems : Number of activities: " << _events.size();

    addRowsToTable();
    addClickableTitle();
    addClickableEditIcon();
    colorImportantEventsBackground();

    qDebug() << "ActivityWidget -> setUpTableContentFromEventItems : Finished setting up activity table";

    connect (_activityTable, SIGNAL(resizedTable(int)),
             this, SLOT(handleTableResize(int)));

    qDebug() << "ActivityWidget -> setUpTableContentFromEventItems : "
                "Connect up the signal (resizedTable(int) to the activity widget. This function is now ended";

}

void ActivityWidget::addRowsToTable()
{
    for (int i = 0 ; i < _events.size(); i ++) {
        EventItem* curEvent = _events.at(i);
        connect (curEvent, SIGNAL(clickedRegisterButton(QString, bool)),
                 this, SLOT(updateStatusColumn(QString, bool)));
        connect (curEvent, SIGNAL(isUpdated(QString)), this, SLOT(updateEventToDatabase(QString)));

        QVector<QString> contentOfARow = QVector<QString>()
                << QString::number(i + 1) << ""
                << _events.at(i)->getDate().toString(FORMAT_DATE)
                << _events.at(i)->getStartTime().toString(FORMAT_TIME) + " to "
                   + _events.at(i)->getEndTime().toString(FORMAT_TIME)
                << _events.at(i)->getTrainer()
                << _events.at(i)->getVacancy();

        if (_userRole == UserDetails::ROLE_TRAINEE) {
            if (_events.at(i)->getIsRegistered()) {
                contentOfARow.push_back(LABEL_REGISTERD);
            } else {
                contentOfARow.push_back(LABEL_NOTREGISTERD);
            }
        } else { //for trainer and admin. TODO:make sure if admin is required this page
            if (_events.at(i)->getIsClosed()) {
                contentOfARow.push_back(LABEL_CLOSEDEVENT);
            } else {
                contentOfARow.push_back(LABEL_OPENEVENT);
            }

            contentOfARow.push_front(STRING_EMPTY);
        }

        _activityTable->addRow(contentOfARow);
    }
}

void ActivityWidget::addClickableTitle()
{
    for (int i = 0; i < _events.size(); i ++) {
        _events.at(i)->constructClickableTitle();
        if (_userRole == UserDetails::ROLE_TRAINEE) {
            _activityTable->addCell(_events.at(i)->getClickableTitle(), i, 1);
        } else {
            _activityTable->addCell(_events.at(i)->getClickableTitle(), i, 2);
        }
    }
}

void ActivityWidget::addClickableEditIcon() {
    if (_userRole != UserDetails::ROLE_TRAINEE) {
        for (int i = 0; i < _events.size(); i ++) {
            _events.at(i)->constructClickableEditIcon();
            _activityTable->addCell(_events.at(i)->getClickableEditIcon(), i, 0);
        }
    }

}

void ActivityWidget::colorImportantEventsBackground()
{
    for (int i = 0; i < _events.size(); i ++) {
        colorRowIfImportantEvent(i);
    }
}

void ActivityWidget::setUpDummyWidget() {
    _dummyWidget = new QWidget();
    _dummyWidget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    _activityLayout->addWidget(_dummyWidget, 10);


}

//private slots
void ActivityWidget::updateStatusColumn(QString activityId, bool isWantedToRegister) {
    qDebug() << "ActivityWidget -> updateStatusColumn : Updating event item status";

    //TODO optimize searching for recently registered/deregistered activity
    // or change data structure to achieve efficiency
    for (int i = 0; i < _events.size(); i ++) {
        if (_events.at(i)->getActivityId() == activityId) {

            if (isWantedToRegister) {
                //assert(!_events.at(i)->getIsRegistered());
                _events.at(i)->setIsUserRegistered(true);

                if (_trainingModel->updateActivity(_events.at(i))) {
                    _activityTable->addCell(LABEL_REGISTERD, i, 6);
                } else {
                    _events.at(i)->setIsUserRegistered(false);
                    PopUpMsgBox *outOfVacancyPopup = new PopUpMsgBox(MESSAGE_OUTOFVACANCY);
                    outOfVacancyPopup->exec();
                }
            } else {
                //assert(!_events.at(i)->getIsRegistered());
                _activityTable->addCell(LABEL_NOTREGISTERD, i, 6);
                _events.at(i)->setIsUserRegistered(false);
                _trainingModel->updateActivity(_events.at(i));
            }

            updateVacancyColumn(i);


            if (_events.at(i)->getIsImportant()) {
                _activityTable->colorCell(i, 6, COLOR_CONTENTFONT, COLOR_IMPORTANTCONTENTBACKGROUND);
            }
        }
    }

    qDebug() << "ActivityWidget -> updateStatusColumn : Finished updating event item status";
}

void ActivityWidget::addActivity() {
    qDebug() << "ActivityWidget -> addActivity : Showing the pop up activityBox to add new event";
    ActivityBox *activityBox = new ActivityBox();
    connect (activityBox, SIGNAL(passInfo(QVector<QString>,QDate,QDateTime,QVector<QTime>,int,bool)),
             this, SLOT(processActivityDetails(QVector<QString>,QDate,QDateTime,QVector<QTime>,int,bool)));
    activityBox->exec();
    qDebug() << "ActivityWidget -> addActivity : Showed the pop up activityBox to add new event";
}

void ActivityWidget::updateVacancyColumn(int eventIndex) {
    _events.clear();
    _trainingModel->getTrainingActivitiesFromLocalDB();
    _events = _trainingModel->getFutureTrainingActivities();

    _activityTable->addCell(_events.at(eventIndex)->getVacancy(), eventIndex, 5);
    if (_events.at(eventIndex)->getIsImportant()) {
        _activityTable->colorCell(eventIndex, 5, COLOR_CONTENTFONT, COLOR_IMPORTANTCONTENTBACKGROUND);
    }

    addClickableEditIcon();
    colorImportantEventsBackground();
}

//private slots
bool ActivityWidget::processActivityDetails(QVector<QString> info, QDate date, QDateTime registrationDeadline,
                                            QVector<QTime> time, int maxVacancy, bool isImportant) {
    qDebug() << "ActivityWidget -> processActivityDetails : Processing";
    assert(info.size() == 5);
    assert(time.size() == 2);

    QString title = info.at(0);
    QString trainer = info.at(1);
    QString location = info.at(2);
    QString description = info.at(3);
    QString eventLink = info.at(4);

    QTime startTime = time.at(0);
    QTime endTime = time.at(1);

    if (maxVacancy <= 0) {
        return false;
    }

    EventItem *newEvent = new EventItem(date, startTime, endTime, title,
                                        location, trainer, maxVacancy,
                                        maxVacancy, description, eventLink,
                                        isImportant);
    newEvent->setActivityId(_trainingModel->generateActivityId());
    newEvent->setRegistrationDeadline(registrationDeadline);

    bool isAdded = _trainingModel->addNewActivity(newEvent);

    if (isAdded) {
        _trainingModel->getTrainingActivitiesFromLocalDB();
        _events = _trainingModel->getFutureTrainingActivities();

        newEvent = _events.at(_events.size() - 1);

        connect (newEvent, SIGNAL(isUpdated(QString)), this, SLOT(updateEventToDatabase(QString)));

        info = QVector<QString>()
                << QString::number(_events.size()) << ""
                << newEvent->getDate().toString(FORMAT_DATE)
                << newEvent->getStartTime().toString(FORMAT_TIME) + " to "
                   + newEvent->getEndTime().toString(FORMAT_TIME)
                << newEvent->getTrainer()
                << newEvent->getVacancy();

        if (newEvent->getIsClosed()) {
            info.append(LABEL_CLOSEDEVENT);
        } else {
            info.append(LABEL_OPENEVENT);
        }

        if (_userRole != UserDetails::ROLE_TRAINEE) {
            info.push_front(STRING_EMPTY);
        } else {
            // do nothing
        }

        _dummyWidget->hide();
        _activityTable->addRow(info);

        int lastEventIndex = _events.size() - 1;

        _activityTable->addCell(newEvent->getClickableEditIcon(), lastEventIndex, 0);
        _activityTable->addCell(newEvent->getClickableTitle(), lastEventIndex, 2);
        _activityTable->showWidget(lastEventIndex, 0);
        _activityTable->showWidget(lastEventIndex, 2);

        colorRowIfImportantEvent(_events.size() - 1);

        addClickableEditIcon();
        colorImportantEventsBackground();

        if (_dummyWidget->isHidden()) {
            _dummyWidget->show();
        }
    } else {
        //do nothing
    }

    return isAdded;
}

void ActivityWidget::updateEventToDatabase(QString activityId) {
    qDebug() << "ActivityWidget -> updateEventToDatabase : Updating event to database";
    for (int i = 0; i < _events.size(); i ++) {
        if (_events.at(i)->getActivityId() == activityId) {
            _trainingModel->updateActivity(_events.at(i));
        }
    }
    qDebug() << "ActivityWidget -> updateEventToDatabase : Updated event to database";
    updateTable();
}

void ActivityWidget::updateTable() {
    qDebug() << "ActivityWidget -> updateTable : Updating table";
    assert(_activityTable);

    _activityTable->deleteAll();
    _events.clear();

    _trainingModel->getTrainingActivitiesFromLocalDB();
    _events = _trainingModel->getFutureTrainingActivities();

    _isTimeToUpdate = true;

    int dummyWidgetHeight = this->height() / (_events.size() + 1) * 2;
    _dummyWidget->setMaximumHeight(dummyWidgetHeight);
    _dummyWidget->setMinimumHeight(dummyWidgetHeight);
    this->adjustSize();



    qDebug() << "ActivityWidget -> updateTable : Updated table";
}

void ActivityWidget::handleTableResize(int minHeight) {
    int height = _dummyWidget->height();
    height -= minHeight;

    if (height <= 0) {
        height = 0;
        _dummyWidget->hide();
    } else {
        _dummyWidget->show();
    }
    _dummyWidget->setMaximumHeight(height);
    _dummyWidget->setMinimumHeight(height);
}

//protected
void ActivityWidget::resizeEvent(QResizeEvent *event) {
    QWidget::resizeEvent(event);
    if (_isTimeToUpdate) {
        _isTimeToUpdate = false;

        addRowsToTable();
        addClickableTitle();
        addClickableEditIcon();
        colorImportantEventsBackground();
    } else {
        //ignore
    }
}
