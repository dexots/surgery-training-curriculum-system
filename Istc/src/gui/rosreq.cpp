#include "gui/rosreq.h"

const QColor RosReq::HEADER_BACK = QColor(99, 145, 213, 255);
const QColor RosReq::HEADER_FONT = Qt::white;
const QColor RosReq::CONT_BACK = QColor(219, 237, 249, 255);
const QColor RosReq::CONT_FONT = Qt::black;
const QColor RosReq::MY_OWN_COLOR = QColor (145, 199, 235, 255);

const QString RosReq::COLOR_BUTTONBACKGROUND = "rgb(39, 174, 96)";
const QString RosReq::COLOR_BUTTONFONT = "white";
const QString RosReq::FONT_BUTTON = "bold 14";
const QString RosReq::LEFTMARGIN_BUTTON = "2";
const QString RosReq::PADDING_BUTTON = "6";
const QString RosReq::RADIUS_BUTTONBORDER = "5";
const QString RosReq::STATUS_PENDINGS = "Pending";
const QString RosReq::WIDTH_BUTTON = "50";

void RosReq::addRowsToTable()
{
    QVector <RosterRequestObj*> *objs = _rosterModel->getBlockOutReq();

    _hisContRow = 0;

    for (int i = 0; i < objs->size(); i++) {
        RosterRequestObj *curObj = objs->value(i);
        QString date = curObj->getDateRequest().toString("dd MMM yyyy");
        QString reason = curObj->getReason();
        QString status = curObj->getStatus();

        QVector <QString> row;
        row.append(date);
        row.append(reason);
        row.append(status);

        _hisTable->addRow(row, _hisContRow);
        _hisContRow++;
    }
    delete objs;
}

RosReq::RosReq() {
    _rosterModel = RosterModel::getObject();

    //req label
    _req = new QLabel;
    _req->setText("Roster Request for: ");

    QFont reqFont;
    reqFont.setBold(true);
    _req->setFont(reqFont);
    _req->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    _req->setContentsMargins(0, 0, 0, 0);

    //get today date
    _todayDate = QDate::currentDate();

    //app date label
    _appDate = _todayDate.addMonths(2);

    _appDateLabel = new QLabel;
    QString appDateString = _appDate.toString("MMMM yyyy");
    _appDateLabel->setText(appDateString);

    QFont boldAndUnderline;
    boldAndUnderline.setUnderline(true);
    boldAndUnderline.setBold(true);

    _appDateLabel->setFont(boldAndUnderline);
    colorWidget(CONT_FONT, MY_OWN_COLOR, _appDateLabel);
    _appDateLabel->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    _appDateLabel->setContentsMargins(0, 0, 0, 0);

    //reqLayout
    _reqLayout = new QHBoxLayout;
    _reqLayout->addWidget(_req, 0, 0);
    _reqLayout->addWidget(_appDateLabel, 0, 0);
    _reqLayout->setContentsMargins(0, 0, 0, 5);

    //period
    _period = new QLabel;
    _period->setText("Application Period: ");

    QFont boldFont;
    boldFont.setBold(true);

    _period->setFont(boldFont);

    //from date
    _fromDate = _todayDate;//get from database

    //from date label
    _fromDateLabel = new QLabel;
    QString fromDateString = _fromDate.toString("dd MMM yyyy");
    _fromDateLabel->setText(fromDateString);
    _fromDateLabel->setFont(boldFont);

    //to label
    _to = new QLabel;
    _to->setText(" to ");
    _to->setFont(boldFont);

    //to date
    _toDate = _fromDate.addMonths(1);//get from database

    //to date label
    _toDateLabel = new QLabel;
    QString toDateString = _toDate.toString("dd MMM yyyy");
    _toDateLabel->setText(toDateString);
    _toDateLabel->setFont(boldFont);

    //pick
    QPushButton *pickLabel = new QPushButton;
    pickLabel->setText("<Click to submit an application>");
    setButtonStyleSheet(pickLabel);
    colorWidget(CONT_FONT, MY_OWN_COLOR, pickLabel);
    pickLabel->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    connect(pickLabel, SIGNAL(clicked()), this, SLOT(handlePickDate()));

    //req history
    _reqHistory = new QLabel;
    _reqHistory->setText("Request History");
    _reqHistory->setFont(boldFont);
    _reqHistory->setContentsMargins(0, 10, 0, 0);

    //history table
    QVector <int> noOfColofHis;
    noOfColofHis.append(100);
    noOfColofHis.append(300);
    noOfColofHis.append(100);
    _hisTable = new TableWidget(true, 3, noOfColofHis, 50);

    QVector <QString> headerHis;
    headerHis.append("Day");
    headerHis.append("Reason");
    headerHis.append("Status");
    _hisTable->setHeader(headerHis, HEADER_FONT, HEADER_BACK);
    _hisTable->setContBackground(CONT_BACK);
    _hisTable->setContFont(CONT_FONT);

    addRowsToTable();

    //dummy widget to align the components:
    _dummyWidget = new QWidget;
    _dummyWidget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    connect (_hisTable, SIGNAL(resizedTable(int)),
             this, SLOT(handleResize(int)));

    //final layout
    _reqRosLayout = new QVBoxLayout;
    _reqRosLayout->addLayout(_reqLayout);
    _reqRosLayout->addWidget(pickLabel);
    _reqRosLayout->addWidget(_reqHistory);
    _reqRosLayout->addWidget(_hisTable);
    _reqRosLayout->addWidget(_dummyWidget, 1);
    _reqRosLayout->setSpacing(0);

    this->setLayout(_reqRosLayout);
    RosReq::colorWidget(Qt::black, MY_OWN_COLOR, this);
}

RosReq::~RosReq() {
    delete _req;
    delete _period;
    delete _fromDateLabel;
    delete _to;
    delete _toDateLabel;
    delete _reqHistory;
    delete _hisTable;
    delete _dummyWidget;
    delete _reqRosLayout;
}

//private methods
void RosReq::colorWidget(QColor font, QColor background, QWidget *widget) {
    widget->setAutoFillBackground(true);
    QPalette pallete = widget->palette();
    pallete.setColor(widget->foregroundRole(), font);
    pallete.setColor(widget->backgroundRole(), background);
    widget->setPalette(pallete);
}

void RosReq::setButtonStyleSheet(QPushButton *button) {
    button->setStyleSheet("background-color:" + COLOR_BUTTONBACKGROUND + ";"
                          "font:" + FONT_BUTTON + "px;"
                          "color:" + COLOR_BUTTONFONT + ";"
                          "margin-left:" + LEFTMARGIN_BUTTON + "px;"
                          "border-radius:" + RADIUS_BUTTONBORDER + "px;"
                          "min-width:" + WIDTH_BUTTON + "px;"
                          "padding:" + PADDING_BUTTON + "px;"
                         );
    button->setCursor(Qt::PointingHandCursor);
}

//private slots
void RosReq::handlePickDate() {
    RosReqBox *rosreqBox = new RosReqBox();
    connect(rosreqBox, SIGNAL(passInfo(QVector<QString>)),
            this, SLOT(updateInfo(QVector<QString>)));
    rosreqBox->exec();
    delete rosreqBox;
}

void RosReq::handleResize(int changedSize) {
    int size = _dummyWidget->height() - changedSize;
    if (size < 0) {
        size = 0;
        _dummyWidget->hide();
    } else {
        _dummyWidget->show();
    }
    _dummyWidget->setMaximumHeight(size);
}

void RosReq::updateInfo(QVector<QString> info) {
    QString startDateString = info.at(0);
    QString endDateString = info.at(1);
    QString reason = info.at(2);
    QDate startDate = QDate::fromString(startDateString,
                                        "dd MMM yyyy");
    QDate endDate = QDate::fromString(endDateString,
                                      "dd MMM yyyy");
    UserDetails *userDetails = UserDetails::getObject();
    QString userId = userDetails->getMatricNo();


    while (startDate.daysTo(endDate) >= 0) {
        // Update table current hack]
        QString rosterRequestId = _rosterModel->generateBlockRequestId();

        QVector<QString> currInfo;
        currInfo.append(startDate.toString("dd MMM yyyy"));
        currInfo.append(reason);
        currInfo.append(STATUS_PENDINGS);

        _dummyWidget->hide();

        _hisTable->addRow(currInfo, _hisContRow);
        _hisContRow++;

        //updateDb
        RosterRequestObj *newObj = new RosterRequestObj();
        newObj->setDateRequest(startDate);
        newObj->setDateSubmit(QDate::currentDate());
        newObj->setReason(reason);
        newObj->setRosterRequestId(rosterRequestId);
        newObj->setStatus(STATUS_PENDINGS);
        newObj->setUserId(userId);
        _rosterModel->saveBlockOutReq(newObj);

        startDate = startDate.addDays(1);

        if (_dummyWidget->isHidden()) {
            _dummyWidget->show();
        }
    }
    emit rosterRequestAdded();
}

/*
 * Public Slots
 */
void RosReq::updateTable() {
    assert (_hisTable);
    _hisTable->deleteAll();
    _dummyWidget->setMinimumHeight(this->height());
    addRowsToTable();
}
