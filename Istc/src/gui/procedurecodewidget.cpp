#include "gui/procedurecodewidget.h"

CodeWidget::CodeWidget() {
    _logbookModel = LogbookModel::getObject();

    QLabel *addPatient = new QLabel;
    addPatient->setText("Add Patient");
    addPatient->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

    QLabel *addLogo = new QLabel;
    addLogo->setPixmap(QPixmap(QString::fromUtf8(":/icon/ButtonIcon/add.png")));
    addLogo->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

    QHBoxLayout *addLayout = new QHBoxLayout;
    addLayout->addWidget(addPatient);
    addLayout->addWidget(addLogo);
    addLayout->setContentsMargins(0, 0, 0, 0);

    QWidget *addWidget = new QWidget;
    addWidget->setLayout(addLayout);
    ClickableWidget *addClick = new ClickableWidget(addWidget);

    connect(addClick, SIGNAL(isClicked()), this, SLOT(addPatient()));

    QHBoxLayout *alignLayout = new QHBoxLayout;
    alignLayout->addWidget(addClick);
    alignLayout->setAlignment(addClick, Qt::AlignRight);

    QVector <int> widthOfCol;
    widthOfCol.append(70);
    widthOfCol.append(80);
    widthOfCol.append(80);
    widthOfCol.append(90);
    widthOfCol.append(80);
    widthOfCol.append(60);
    widthOfCol.append(80);
    widthOfCol.append(120);

    _table = new TableWidget(true, 8, widthOfCol, 50);

    QVector <QString> header;
    header.append("");
    header.append("Date");
    header.append("Time");
    header.append("Patient ID");
    header.append("DOB");
    header.append("Procedure Code");
    header.append("Outcome");
    header.append("Remarks");

    _table->setHeader(header, Qt::white, QColor(237, 125, 49, 255));
    _table->setContBackground(QColor(252, 236, 232, 255));
    _table->setContFont(Qt::black);

    _contRow = 0;

    CodeWidget::addData();

    QVBoxLayout *codeLayout = new QVBoxLayout;

    _dummyWidget = new QWidget;
    _dummyWidget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    codeLayout->addLayout(alignLayout, 0);
    codeLayout->addWidget(_table, 5);
    codeLayout->addWidget(_dummyWidget, 10);
    codeLayout->setSpacing(0);

    connect (_table, SIGNAL(resizedTable(int)),
             this, SLOT(handleResize(int)));

    this->setLayout(codeLayout);

    CodeWidget::colorWidget(QColor (247, 223, 121, 255),
                            this);
}

CodeWidget::~CodeWidget() {
    delete _table;
    delete _dummyWidget;

    for (int i = _clicks.size() - 1; i >= 0; i--) {
        delete _clicks.at(i);
        delete _proceduralCode->at(i);
    }
    delete _proceduralCode;
}

//private
void CodeWidget::colorWidget(QColor color, QWidget *widget) {
    widget->setAutoFillBackground(true);
    QPalette pallete = widget->palette();
    pallete.setColor(widget->backgroundRole(), color);
    widget->setPalette(pallete);
}

void CodeWidget::addData() {
    _proceduralCode = _logbookModel->getProcedures();

    //add to table
    for (int i = 0; i < _proceduralCode->size(); i ++) {
        ProceduralCode *proceduralCode = _proceduralCode->at(i);

        QString procedureCode = proceduralCode->getProcedureCode();
        QString shortProcedureCode = procedureCode.mid(0, procedureCode.indexOf(":"));

        QVector<QString> contentOfARow = QVector<QString>()
                << "" << proceduralCode->getProcedureDate().toString("ddd, dd MMM yyyy")
                << proceduralCode->getProcedureTime().toString("hh:mm")
                << proceduralCode->getPatientID()
                << proceduralCode->getPatientDOB().toString("dd MMM yyyy")
                << shortProcedureCode
                << proceduralCode->getProcedureOutcome()
                << proceduralCode->getProcedureRemarks();

        _table->addRow(contentOfARow, _contRow++);

        connect(proceduralCode, SIGNAL(clickedUpdateButton()), this, SLOT(updatePatientDetails()));
    }

    //replace with clickable icon
    for (int i = 0; i < _proceduralCode->size(); i ++) {
        _table->addCell(_proceduralCode->at(i)->getClickableEditIcon(), i, 0);
    }

    connect (_table, SIGNAL(resizedTable(int)),
             this, SLOT(handleResize(int)));
}

//private slots
void CodeWidget::addPatient() {
    PatientBox *patientDetails = new PatientBox;
    connect (patientDetails, SIGNAL(passInfo(QVector<QString>)),
             this, SLOT(havePatientDetails(QVector<QString>)));
    patientDetails->exec();
}

void CodeWidget::havePatientDetails(QVector<QString> info) {
    ProceduralCode *code = new ProceduralCode(info.at(0), info.at(1),
                                              info.at(2), info.at(3),
                                              info.at(4).toInt(), info.at(5), info.at(6));
    QString codeId = _logbookModel->generateNewProcedureId();
    code->setProcedureID(codeId);

    if (!_logbookModel->addProcedure(code)) {
        //to do: error string
        std::cout << "patientId exists but dob doesn't correct\n";
        return;
    }

    _proceduralCode->append(code);

    //content of the new table row
    int lastItemIndex = _proceduralCode->size() - 1;

    QString procedureCode = code->getProcedureCode();
    QString shortProcedureCode = procedureCode.mid(0, procedureCode.indexOf(":"));

    info.clear();
    info.append("");
    info.append(code->getProcedureDate().toString("ddd, dd MMM yyyy"));
    info.append(code->getProcedureTime().toString("hh:mm"));
    info.append(code->getPatientID());
    info.append(code->getPatientDOB().toString("dd MMM yyyy"));
    info.append(shortProcedureCode);
    info.append(code->getProcedureOutcome());
    info.append(code->getProcedureRemarks());



    _dummyWidget->hide();
    _table->addRow(info, _contRow++);

    connect(code, SIGNAL(clickedUpdateButton()), this, SLOT(updatePatientDetails()));

    _table->addCell(code->getClickableEditIcon(), lastItemIndex, 0);

    _table->showWidget(lastItemIndex, 0);

    if (_dummyWidget->isHidden()) {
        _dummyWidget->show();
    }
}

void CodeWidget::updatePatientDetails() {
    //this is not a good method
    //to do: improve the efficiency
    for (int i = 0; i < _proceduralCode->size(); i ++) {

        QString procedureCode = _proceduralCode->at(i)->getProcedureCode();
        QString shortProcedureCode = procedureCode.mid(0, procedureCode.indexOf(":"));

        _table->addCell(_proceduralCode->at(i)->getProcedureDate().toString("ddd, dd MMM yyyy"), i, 1);
        _table->addCell(_proceduralCode->at(i)->getProcedureTime().toString("hh:mm"), i, 2);
        _table->addCell(_proceduralCode->at(i)->getPatientID(), i, 3);
        _table->addCell(_proceduralCode->at(i)->getPatientDOB().toString("dd MMM yyyy"), i, 4);
        _table->addCell(shortProcedureCode, i, 5);
        _table->addCell(_proceduralCode->at(i)->getProcedureOutcome(), i, 6);
        _table->addCell(_proceduralCode->at(i)->getProcedureRemarks(), i, 7);

        _logbookModel->updateProcedure(_proceduralCode->at(i));
    }
}

void CodeWidget::handleResize(int changedSize) {
    int oriHeight = _dummyWidget->height();
    int size = oriHeight - changedSize;
    if (size < 0) {
        size = 0;
        _dummyWidget->hide();
    } else {
        _dummyWidget->show();
    }
    _dummyWidget->setMaximumHeight(size);
}

/*
 * Public Slots
 */
void CodeWidget::updateTable() {
    assert(_table);
    _table->deleteAll();
    _contRow = 0;
    _dummyWidget->setMinimumHeight(this->height());
    addData();
}
