#include "gui/rosreqbox.h"

const QString RosReqBox::COLOR_BUTTONBACKGROUND = "rgb(39, 174, 96)";
const QString RosReqBox::COLOR_BUTTONBORDER = "white";
const QString RosReqBox::COLOR_BUTTONFONT = "white";
const QString RosReqBox::FONT_BUTTON = "bold 15";
const QString RosReqBox::PADDING_BUTTON = "6";
const QString RosReqBox::RADIUS_BUTTONBORDER = "10";
const QString RosReqBox::STYLE_BUTTONBORDER = "outset";
const QString RosReqBox::WIDTH_BUTTON = "50";
const QString RosReqBox::WIDTH_BUTTONBORDER = "2";

const QString RosReqBox::TAG_TITLE = "Submit Roster Request for Exemption";
const QString RosReqBox::TAG_START_DATE = "Start Date";
const QString RosReqBox::TAG_END_DATE = "End Date";
const QString RosReqBox::TAG_REASON = "Reason";
const QString RosReqBox::TAG_SUBMIT = "Submit";
const QString RosReqBox::TAG_CANCEL = "Cancel";
const QString RosReqBox::TAG_ERROR = "Please enter a reason";
const QString RosReqBox::TAG_DATE_ERROR = "Date must be after today";

const int RosReqBox::WIDTH_MINIMUM = 500;
const int RosReqBox::HEIGHT_MINIMUM = 300;

RosReqBox::RosReqBox() {
    initialise();
    makeTitle();
    makeStartDateLayout();
    makeEndDateLayout();
    makeErrorLabel();
    makeReasonLayout();
    makeButtonLayout();
    makeMainLayout();
}

RosReqBox::~RosReqBox() {
    delete _titleLabel;
    delete _monthLabel;
    delete _startDateLabel;
    delete _endDateLabel;
    delete _errorLabel;

    delete _startDate;
    delete _endDate;

    delete _reasonBox;
    delete _submitBtn;
    delete _cancelBtn;

    delete _startDateLayout;
    delete _endDateLayout;
    delete _buttonLayout;
    delete _reasonLayout;
    delete _mainLayout;
}

/*
 * Helper methods to set layout
 */
void RosReqBox::initialise() {
    _bold.setBold(true);

    _titleLabel = new QLabel;
    _monthLabel = new QLabel;
    _startDateLabel = new QLabel;
    _endDateLabel = new QLabel;
    _reasonLabel = new QLabel;
    _errorLabel = new QLabel;

    _startDate = new QDateEdit;
    _endDate = new QDateEdit;
    _startDateString = "";
    _endDateString = "";

    _reasonBox = new InputBox;
    _reasonString = "";
    _currentDate = QDate::currentDate();

    _submitBtn = new QPushButton();
    _cancelBtn = new QPushButton();

    _startDateLayout = new QHBoxLayout();
    _endDateLayout = new QHBoxLayout();
    _buttonLayout = new QHBoxLayout();
    _reasonLayout = new QVBoxLayout();
    _mainLayout = new QVBoxLayout();

}

void RosReqBox::makeTitle() {
    _titleLabel->setText(TAG_TITLE);
    _titleLabel->setFont(_bold);
}

void RosReqBox::makeErrorLabel() {
    _errorLabel->setText(TAG_ERROR);
    _errorLabel->setFont(_bold);
    changeColor(Qt::red, Qt::white, _errorLabel);
    _errorLabel->hide();
}

void RosReqBox::makeStartDateLayout() {
    _startDateLabel->setText(TAG_START_DATE);
    _startDateLabel->setFont(_bold);

    _startDate->setMinimumDate(QDate(2013,1,1));
    _startDate->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
    _startDate->setDisplayFormat("d MMM yyyy");
    _startDate->setDate(_currentDate);

    connect(_startDate, SIGNAL(dateChanged(QDate)),
            this, SLOT(handleStartDate(QDate)));

    _startDateLayout->addWidget(_startDateLabel);
    _startDateLayout->addWidget(_startDate);
}

void RosReqBox::makeEndDateLayout() {
    _endDateLabel->setText(TAG_END_DATE);
    _endDateLabel->setFont(_bold);

    _endDate->setMinimumDate(QDate(2013,1,1));
    _endDate->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
    _endDate->setDisplayFormat("d MMM yyyy");
    _endDate->setDate(_currentDate);

    connect(_endDate, SIGNAL(dateChanged(QDate)),
            this, SLOT(handleEndDate(QDate)));

    _endDateLayout->addWidget(_endDateLabel);
    _endDateLayout->addWidget(_endDate);
}

void RosReqBox::makeReasonLayout() {
    _reasonLabel->setText(TAG_REASON);
    _reasonLabel->setFont(_bold);
    _reasonBox->setPlaceholderText("Enter Reason Here");

    connect(_reasonBox, SIGNAL(stringReady(QString)),
            this, SLOT(handleRemarks(QString)));

    _reasonLayout->addWidget(_reasonLabel);
    _reasonLayout->addWidget(_reasonBox);
}

void RosReqBox::makeButtonLayout() {
    _submitBtn->setText(TAG_SUBMIT);
    setButtonStyleSheet(_submitBtn);
    connect(_submitBtn, SIGNAL(clicked()),
            this, SLOT(handleButton()));
    _submitBtn->setSizePolicy(QSizePolicy::Fixed,
                           QSizePolicy::Fixed);

    _cancelBtn->setText(TAG_CANCEL);
    setButtonStyleSheet(_cancelBtn);
    connect(_cancelBtn, SIGNAL(clicked()),
            this, SLOT(handleCancel()));
    _cancelBtn->setSizePolicy(QSizePolicy::Fixed,
                           QSizePolicy::Fixed);

    _buttonLayout->addWidget(_submitBtn);
    _buttonLayout->addWidget(_cancelBtn);
}

void RosReqBox::makeMainLayout() {
    _mainLayout->addWidget(_titleLabel);
    _mainLayout->addLayout(_startDateLayout);
    _mainLayout->addLayout(_endDateLayout);
    _mainLayout->addLayout(_reasonLayout);
    _mainLayout->addWidget(_errorLabel);
    _mainLayout->addLayout(_buttonLayout);

    this->setLayout(_mainLayout);
    this->setContentsMargins(10, 10, 10, 10);
    this->setMinimumWidth(WIDTH_MINIMUM);
    this->setMinimumHeight(HEIGHT_MINIMUM);

    this->setAttribute(Qt::WA_TranslucentBackground, true);

    this->setWindowFlags(Qt::FramelessWindowHint);
}

/*
 * Slots
 */
void RosReqBox::handleButton() {
    _reasonString = _reasonBox->toPlainText();

    _startDateString = _startDate->date().toString("dd MMM yyyy");
    _endDateString = _endDate->date().toString("dd MMM yyyy");

    if (_reasonString == "") {
        _errorLabel->setText(TAG_ERROR);
        _errorLabel->show();
    } else if (dateLaterThanCurrent()) {
        _errorLabel->setText(TAG_DATE_ERROR);
        _errorLabel->show();
    } else {
        _request.append(_startDateString);
        _request.append(_endDateString);
        _request.append(_reasonString);

        emit this->passInfo(_request);
        QDialog::done(0);
    }
}

void RosReqBox::handleRemarks(QString string) {
    _reasonString = string;
    _reasonBox->setPlainText(_reasonString);
}

bool RosReqBox::dateLaterThanCurrent() {
    if (_startDate->date() < _currentDate ||
            _endDate->date() < _currentDate) {
        return true;
    }
    return false;
}

void RosReqBox::handleCancel() {
    QDialog::done(0);
}

void RosReqBox::handleStartDate(QDate startDate) {
    _startDateString = startDate.toString("dd MMM yyyy");
}

void RosReqBox::handleEndDate(QDate endDate) {
    _endDateString = endDate.toString("dd MMM yyyy");
}

/*
 * Helper functions
 */
void RosReqBox::paintEvent(QPaintEvent *event) {
    QDialog::paintEvent(event);
    QPainter painter(this);
    QBrush brush(Qt::white);
    painter.setBrush(brush);
    painter.setPen(Qt::white);
    painter.drawRoundRect(QRect(0, 0, this->width(), this->height()));
}

void RosReqBox::changeColor(QColor font, QColor back, QWidget *widget) {
    widget->setAutoFillBackground(true);
    QPalette pallete = widget->palette();
    pallete.setColor(widget->foregroundRole(), font);
    pallete.setColor(widget->backgroundRole(), back);
    widget->setPalette(pallete);
}

void RosReqBox::setButtonStyleSheet(QPushButton *button) {
    button->setStyleSheet("background-color:" + COLOR_BUTTONBACKGROUND + ";"
                          "border-style:" + STYLE_BUTTONBORDER + ";"
                          "border-width:" + WIDTH_BUTTONBORDER + "px;"
                          "border-radius:" + RADIUS_BUTTONBORDER + "px;"
                          "border-color:" + COLOR_BUTTONBORDER + ";"
                          "font:" + FONT_BUTTON + "px;"
                          "color:" + COLOR_BUTTONFONT + ";"
                          "min-width:" + WIDTH_BUTTON + "px;"
                          "padding:" + PADDING_BUTTON + "px;");
    button->setCursor(Qt::PointingHandCursor);
}

