#include <gui/assessmentwidget.h>

const QVector<int> AssessmentWidget::WIDTH_COLUMN (QVector<int>() << 40 << 110 << 120 << 80 << 340 << 120);

const QVector<QString> AssessmentWidget::HEADER_TABLE_TRAINEE (QVector<QString>() << "No" << "Date" << "Title"
                                                        << "Grade" << "Feedback" << "Grader");
const QVector<QString> AssessmentWidget::HEADER_TABLE_TRAINER (QVector<QString>() << "No" << "Date" << "Title"
                                                        << "Grade" << "Feedback" << "Trainee");

const int AssessmentWidget::NUMBER_COLUMN = 6;

const QColor AssessmentWidget::COLOR_CONTENTBACKGROUND = QColor(207, 174, 220);
const QColor AssessmentWidget::COLOR_CONTENTFONT = QColor(Qt::black);
const QColor AssessmentWidget::COLOR_HEADERBACKGROUND = QColor(207, 174, 220);
const QColor AssessmentWidget::COLOR_HEADERTEXT = QColor(Qt::white);

const QString AssessmentWidget::SIZE_FONT = "15";

const QString AssessmentWidget::ADD_ICON = QString::fromUtf8(":/icon/ButtonIcon/add.png");

AssessmentWidget::AssessmentWidget() {
    qDebug() << "AssessmentWidget : Constructing Assessment Widget";
    _personalModel = PersonalModel::getObject();
    getUserDetails();
    setUpAssessmentLayout();
    setUpAssessmentTable();
    setWidgetStyleSheet();
    setUpDummyWidget();
    setLayout(_assessmentLayout);
    qDebug() << "AssessmentWidget : Finished constructing Assessment Widget";
}

AssessmentWidget::~AssessmentWidget() {
    delete _dummyWidget;
    delete _assessmentTable;
    delete _assessmentLayout;
    delete _assessments;
}

void AssessmentWidget::getUserDetails() {
    UserDetails *userDetails = UserDetails::getObject();
    _role = userDetails->getUserRole();
    _matricNum = userDetails->getMatricNo();
}

void AssessmentWidget::setUpAssessmentLayout() {
    qDebug() << "AssessmentWidget -> setUpAssessmentLayout : Initializing assessment widget layout";
    _assessmentLayout = new QVBoxLayout();

    if (_role == UserDetails::ROLE_TRAINER) {
        setUpAddIconForTrainer();
    } else {
        //do nothing
    }
    _assessmentLayout->setAlignment(Qt::AlignCenter | Qt::AlignTop);
    qDebug() << "AssessmentWidget -> setUpAssessmentLayout : Finished initializing assessment widget layout";
}

void AssessmentWidget::setUpAddIconForTrainer() {
    QWidget *alignWidget = new QWidget;
    alignWidget->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Fixed);

    QLabel *addAssessment = new QLabel;
    addAssessment->setText("Add Assessment");
    addAssessment->setSizePolicy(QSizePolicy::Fixed,
                                 QSizePolicy::Fixed);

    QLabel *addLogo = new QLabel;
    addLogo->setPixmap(QPixmap(ADD_ICON));
    addLogo->setSizePolicy(QSizePolicy::Fixed,
                           QSizePolicy::Fixed);
    ClickableWidget *addClick = new ClickableWidget(addLogo);

    QHBoxLayout *addLayout = new QHBoxLayout;
    addLayout->addWidget(alignWidget, 10);
    addLayout->addWidget(addAssessment, 0);
    addLayout->addWidget(addClick, 0);

    connect(addClick, SIGNAL(isClicked()),
            this, SLOT(addAssessment()));

    QWidget *fixedWidget = new QWidget;
    fixedWidget->setLayout(addLayout);
    fixedWidget->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Fixed);
    _assessmentLayout->addWidget(fixedWidget, 0);
}

void AssessmentWidget::addAssessmentsToTable()
{
    qDebug() << "AssessmentWidget -> addAssessmentsToTable : Setting up content of assessment table";
    _assessments = _personalModel->getAssessments();

    qDebug() << "AssessmentWidget -> addAssessmentsToTable : Number of assessments is " << _assessments->size();

    for (int i = 0; i < _assessments->size(); i++) {
        AssessmentObj curAssessment = _assessments->value(i);
        QString index = QString::number(i + 1);
        QString assessDate = curAssessment.getAssessDate()
                                    .toString("dd MMM yyyy");
        QString assessTitle = curAssessment.getAssessTitle();
        QString grade = curAssessment.getGrade();
        QString feedback = curAssessment.getFeedback();

        QVector<QString> contentOfARow = QVector<QString>()
                << index << assessDate << assessTitle << grade
                << feedback;

        if (_role == UserDetails::ROLE_TRAINEE) {
            QString graders = curAssessment.getGradersString();
            contentOfARow << graders;
        } else if (_role == UserDetails::ROLE_TRAINER) {
            //TODO: show trainee name instead of id?
            QString trainee = curAssessment.getTraineeId();
            contentOfARow << trainee;
        }

        _assessmentTable->addRow(contentOfARow);
    }

    qDebug() << "AssessmentWidget -> addAssessmentsToTable : Finished setting up content of assessment table";
}

void AssessmentWidget::setUpAssessmentTable() {
    qDebug() << "AssessmentWidget -> setUpAssessmentTable : Initializing assessment table";
    _assessmentTable = new TableWidget(true, NUMBER_COLUMN, WIDTH_COLUMN);
    _assessmentTable->setContBackground(COLOR_CONTENTBACKGROUND);
    _assessmentTable->setContFont(COLOR_CONTENTFONT);

    if (_role == UserDetails::ROLE_TRAINEE) {
        _assessmentTable->setHeader(HEADER_TABLE_TRAINEE, COLOR_HEADERTEXT, COLOR_HEADERBACKGROUND);
    } else if (_role == UserDetails::ROLE_TRAINER) {
        _assessmentTable->setHeader(HEADER_TABLE_TRAINER, COLOR_HEADERTEXT, COLOR_HEADERBACKGROUND);
    }

    addAssessmentsToTable();

    _assessmentLayout->addWidget(_assessmentTable, 10);

    connect (_assessmentTable, SIGNAL(resizedTable(int)),
             this, SLOT(handleTableResized(int)));




    qDebug() << "AssessmentWidget -> setUpAssessmentTable : Finished initializing assessment table";
}


void AssessmentWidget::setUpDummyWidget() {
    _dummyWidget = new QWidget(this);
    _dummyWidget->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::MinimumExpanding);

    _assessmentLayout->addWidget(_dummyWidget, 20);
}

void AssessmentWidget::setWidgetStyleSheet() {
    qDebug() << "AssessmentWidget -> setWidgetStyleSheet : Setting assessment widget stylesheet";
    this->setStyleSheet("QLabel{font:" + SIZE_FONT + "px;}");
    qDebug() << "AssessmentWidget -> setWidgetStyleSheet : Finished setting assessment widget stylesheet";
}

/*
 * Public Slots
 */
void AssessmentWidget::updateTable() {
    qDebug() << "AssessmentWidget -> updateAssessmentTable : Updating assessment table";

    assert(_assessmentTable);
    _assessmentTable->deleteAll();
    _assessments->clear();
    _dummyWidget->setMinimumHeight(this->height());
    addAssessmentsToTable();

    qDebug() << "AssessmentWidget -> updateAssessmentTable : Updated assessment table";
}

/*
 * Private Slots
 */
void AssessmentWidget::addAssessment() {
    qDebug() << "AssessmentWidget -> addAssessment : Showing pop up box for adding new assessment";
    AssessmentBox *assessmentBox = new AssessmentBox();
    //TODO: pass a list of students matric to assessment box to prevent non exist students matric no is entered
    connect(assessmentBox, SIGNAL(passInfo(QDate, QVector<QString>)), this, SLOT(assessmentReceived(QDate, QVector<QString>)));
    assessmentBox->exec();
    qDebug() << "AssessmentWidget -> addAssessment : Showed pop up box for adding new assessment";
}

void AssessmentWidget::assessmentReceived(QDate date, QVector<QString> info) {
    qDebug() << "AssessmentWidget -> assessmentReceived : Received new assessment and adding to database";
    AssessmentObj newAssessment = AssessmentObj();

    QString traineeMatric = info.at(0);
    QString assessTitle = info.at(1);
    QString grade = info.at(2);
    QString feedback = info.at(3);

    newAssessment.setAssessDate(date);
    newAssessment.setTraineeId(traineeMatric);
    newAssessment.setAssessTitle(assessTitle);
    newAssessment.setGrade(grade);
    newAssessment.setFeedback(feedback);

    newAssessment.setAssessId(_personalModel->generateAssessmentId());

    newAssessment.addGrader(_matricNum);

    bool isAdded = _personalModel->addAssessment(newAssessment);

    if (isAdded) {
        qDebug() << "AssessmentWidget -> assessmentReceived : Added new assessment to database";
        QString index = QString::number(_assessments->size() + 1);
        QString assessDate = newAssessment.getAssessDate()
                                        .toString("dd MMM yyyy");

        QVector<QString> contentOfARow = QVector<QString>()
                    << index << assessDate << assessTitle << grade
                    << feedback << traineeMatric;

        _dummyWidget->hide();
        _assessmentTable->addRow(contentOfARow);


        if (_dummyWidget->isHidden()) {
            _dummyWidget->show();
        }
    } else {
        qDebug() << "AssessmentWidget -> assessmentReceived : Failed to add new assessment to database";
        //should be able to add without error
        assert(false);
    }
}

void AssessmentWidget::handleTableResized(int minHeightChanged) {

    int height = _dummyWidget->height();

    int difference = height - minHeightChanged;
    if (difference <= 0) {
        difference = 0;
        _dummyWidget->hide();
    } else {
        _dummyWidget->show();
    }
    _dummyWidget->setMaximumHeight(difference);

}

