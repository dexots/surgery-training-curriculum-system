#include "gui/settingsbox.h"

const QString SettingsBox::COLOR_BUTTONBACKGROUND = "rgb(39, 174, 96)";
const QString SettingsBox::COLOR_BUTTONBORDER = "white";
const QString SettingsBox::COLOR_BUTTONFONT = "white";
const QString SettingsBox::FONT_BUTTON = "bold 14";
const QString SettingsBox::PADDING_BUTTON = "6";
const QString SettingsBox::RADIUS_BUTTONBORDER = "10";
const QString SettingsBox::STYLE_BUTTONBORDER = "outset";
const QString SettingsBox::WIDTH_BUTTON = "50";
const QString SettingsBox::WIDTH_BUTTONBORDER = "2";

const QString SettingsBox::TAG_ABOUT = "ABOUT";
const QString SettingsBox::TAG_SYNC = "Sync Interval";
const QString SettingsBox::TAG_OK = "OK";
const QString SettingsBox::TAG_UPDATE = "Update";
const QString SettingsBox::TAG_COLOUR = "Colour Scheme";

const QString SettingsBox::SETTINGS_FILE = "usersettings.txt";

const int SettingsBox::WIDTH_MINIMUM = 400;
const int SettingsBox::HEIGHT_MINIMUM = 300;

SettingsBox::SettingsBox() {
    initialise();
    readSettingsFile();
    setSyncLayout();
    setColourLayout();
    setAboutLayout();
    setButtonLayout();
    customisePopupBox();
}

SettingsBox::~SettingsBox() {
    delete _syncLabel;
    delete _colourLabel;
    delete _aboutLabel;
    delete _aboutWriteupLabel;
    delete _syncBox;
    delete _colourBox;
    delete _button;
    delete _mainLayout;
    delete _colourLayout;
    delete _syncLayout;
    delete _aboutLayout;
}

void SettingsBox::initialise() {
    _bold.setBold(true);
    _mainLayout = new QVBoxLayout();
    _syncLayout = new QVBoxLayout();
    _colourLayout = new QVBoxLayout();
    _aboutLayout = new QVBoxLayout();
    _buttonLayout = new QHBoxLayout();
    _syncBox = new QComboBox();
    _colourBox = new QComboBox();
    _syncLabel = new QLabel;
    _aboutLabel = new QLabel;
    _colourLabel = new QLabel;
    _aboutWriteupLabel = new QLabel;
    _button = new QPushButton();

    _settingsChanged = false;
    _aboutWriteup = "";
}

void SettingsBox::readSettingsFile() {
    if (!isFileExist(SETTINGS_FILE)) {
        createSettingsFile();
    }

    QFile file(SETTINGS_FILE);

    if (!file.open(QFile::ReadOnly | QFile::Text)) {
        showErrorBox();
        return;
    }
    QTextStream in(&file);
    QString data = in.readAll();
    populateData(data);
    file.close();
}

bool SettingsBox::isFileExist(QString filename) {
    QFile file (filename);
    bool isExist = file.exists();
    file.close();
    return isExist;
}

void SettingsBox::createSettingsFile() {
    QFile file(SETTINGS_FILE);
    if (!file.open(QFile::ReadWrite | QFile::Text)) {
        showErrorBox();
        return;
    }
    QTextStream out(&file);
    out << "3 hours\n" << "Integrated Surgery Training Curriculum is created by Chan Jun Wei, Chua Chin Siang, Ng Hui Xian, Ong Shi Rong and Ong Teck Sheng from NUS School of Computing.";
    file.close();
}

void SettingsBox::populateData(QString data) {
    QStringList split = data.split("\n");
    _syncInterval = split[0];
    _aboutWriteup = split[1];

}

void SettingsBox::setSyncLayout() {
    _syncLabel->setText(TAG_SYNC);
    _syncLabel->setFont(_bold);

    setUpSyncBox();

    _syncLayout->addWidget(_syncLabel);
    _syncLayout->addWidget(_syncBox);

    addToMainLayout(_syncLayout);
}

void SettingsBox::setColourLayout() {
    _colourLabel->setText(TAG_COLOUR);
    _colourLabel->setFont(_bold);

    QStringList colourList;
    colourList << "Default" << "Dark" << "Light";
    _colourBox->addItems(colourList);
    connect(_colourBox, SIGNAL(currentTextChanged(QString)),
            this, SLOT(handleColourChange()));

    _colourLayout->addWidget(_colourLabel);
    _colourLayout->addWidget(_colourBox);

    addToMainLayout(_colourLayout);
}

void SettingsBox::setUpSyncBox() {
    list << "3 hours" << "6 hours" << "12 hours";
    _syncBox->addItems(list);
    _syncBox->setCurrentIndex(_syncBox->findText(_syncInterval));
    connect(_syncBox, SIGNAL(currentTextChanged(QString)),
            this, SLOT(handleSyncChange()));
}

void SettingsBox::setButtonLayout() {
    _button->setText(TAG_OK);
    setButtonStyleSheet(_button);
    _button->setCursor(Qt::PointingHandCursor);
    connect(_button, SIGNAL(clicked()),
            this, SLOT(handleButton()));
    _button->setSizePolicy(QSizePolicy::Fixed,
                           QSizePolicy::Fixed);

    _buttonLayout->addWidget(_button, Qt::AlignHCenter);

    addToMainLayout(_buttonLayout);
}

void SettingsBox::customisePopupBox() {
    this->setLayout(_mainLayout);
    this->setContentsMargins(10, 10, 10, 10);
    this->setMinimumWidth(WIDTH_MINIMUM);
    this->setMinimumHeight(HEIGHT_MINIMUM);

    this->setAttribute(Qt::WA_TranslucentBackground, true);

    this->setWindowFlags(Qt::FramelessWindowHint);
}

void SettingsBox::setAboutLayout() {
    _aboutLabel->setText(TAG_ABOUT);
    _aboutLabel->setFont(_bold);
    _aboutWriteupLabel->setText(_aboutWriteup);
    _aboutWriteupLabel->setWordWrap(true);
    _aboutWriteupLabel->setMinimumHeight(150);
    _aboutLayout->addWidget(_aboutLabel);
    _aboutLayout->addWidget(_aboutWriteupLabel);

    addToMainLayout(_aboutLayout);
}

void SettingsBox::addToMainLayout(QHBoxLayout *layout) {
    _mainLayout->addLayout(layout);
}

void SettingsBox::addToMainLayout(QVBoxLayout *layout) {
    _mainLayout->addLayout(layout);
}

void SettingsBox::addToMainLayout(QWidget *widget) {
    _mainLayout->addWidget(widget);
}

void SettingsBox::saveSettingsFile() {
    QFile file(SETTINGS_FILE);

    if (!file.open(QFile::WriteOnly | QFile::Text)) {
        showErrorBox();
        return;
    }
    QTextStream out(&file);

    out <<  _syncInterval << "\n" << _aboutWriteup;
    file.close();

    showSuccessMsg();
}

void SettingsBox::showErrorBox() {
    ErrorBox *errorBox = new ErrorBox("SETTINGS");
    errorBox->exec();
}

void SettingsBox::showSuccessMsg() {
    GeneralPopupBox *successBox = new GeneralPopupBox("", 3);
    successBox->exec();
}

// Private Slots
void SettingsBox::handleSyncChange() {
    _button->setText(TAG_UPDATE);
    _syncInterval = _syncBox->currentText();
    _settingsChanged = true;
}

void SettingsBox::handleButton() {
    if (_settingsChanged) {
        saveSettingsFile();
    }

    QDialog::done(0);
}


// To do in future: Handle Colour Change
void SettingsBox::handleColourChange() {

}

// General Events
void SettingsBox::paintEvent(QPaintEvent *event) {
    QDialog::paintEvent(event);
    QPainter painter(this);
    QBrush brush(Qt::white);
    painter.setBrush(brush);
    painter.setPen(Qt::white);
    painter.drawRoundRect(QRect(0, 0, this->width(), this->height()));
}

void SettingsBox::changeColor(QColor font, QColor back, QWidget *widget) {
    widget->setAutoFillBackground(true);
    QPalette pallete = widget->palette();
    pallete.setColor(widget->foregroundRole(), font);
    pallete.setColor(widget->backgroundRole(), back);
    widget->setPalette(pallete);
}

void SettingsBox::setButtonStyleSheet(QPushButton *button) {
    button->setStyleSheet("background-color:" + COLOR_BUTTONBACKGROUND + ";"
                          "border-style:" + STYLE_BUTTONBORDER + ";"
                          "border-width:" + WIDTH_BUTTONBORDER + "px;"
                          "border-radius:" + RADIUS_BUTTONBORDER + "px;"
                          "border-color:" + COLOR_BUTTONBORDER + ";"
                          "font:" + FONT_BUTTON + "px;"
                          "color:" + COLOR_BUTTONFONT + ";"
                          "min-width:" + WIDTH_BUTTON + "px;"
                          "padding:" + PADDING_BUTTON + "px;");
}
