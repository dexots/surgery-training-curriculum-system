#include "gui/researchwidget.h"

QString RESEARCH_FILE = "research.txt";
QString NO_PROJECT_FILE = "noproject.txt";

ResearchWidget::ResearchWidget() {
    ResearchWidget::readResearchFile();
    ResearchWidget::createLayout();
}

ResearchWidget::~ResearchWidget() {
}

QString ResearchWidget::getClassName() {
    return "research";
}

void ResearchWidget::createLayout() {
    _widgetLayout = new QVBoxLayout;
    QWidget *noProject = new QWidget;
    ResearchWidget::colorWidget(QColor (240, 146, 136, 255),
                              noProject);
    if (!_hasProjects) {
        ResearchInstance *noProject = new ResearchInstance(NO_PROJECT_FILE);

        SubCate *noProjectCate = new SubCate("No Project",
                                            "Please contact your trainer to add a project :)",
                                            "",
                                            QColor (59, 56, 56, 255), //desc font color
                                            QColor (249, 211, 207, 255), //default color
                                            QColor (240, 146, 136, 255), //selected color
                                            noProject);

        _researchSubCategories.append(noProjectCate);
    }

    createResearchTable();

}

void ResearchWidget::readResearchFile() {
    QFile file(RESEARCH_FILE);

    if (!file.open(QFile::ReadOnly | QFile::Text)) {
        // showerrorbox
        // assert(false);
        QString errorMsg = RESEARCH_FILE + " is not found! ";
        PopUpMsgBox* errorBox = new PopUpMsgBox(errorMsg);
        errorBox->exec();
        errorBox->deleteLater();

        _hasProjects = false;
        return;
    }

    QTextStream in(&file);
    QString data = in.readAll();
    populateData(data);
    file.close();

}

void ResearchWidget::createResearchTable() {
    CateTable *researchTable = new CateTable("Research",
                                             "",
                                             ":/icon/ButtonIcon/Research Icon.png",
                                             QColor (231, 76, 60, 255),
                                             _researchSubCategories);

    _widgetLayout->addWidget(researchTable);
    _widgetLayout->setContentsMargins(0, 0, 0, 0);
    this->setLayout(_widgetLayout);

    connect(researchTable, SIGNAL(backToHome()), this, SLOT(handleHomeSignal()));

}

void ResearchWidget::populateData(QString data) {
    _hasProjects = !data.isEmpty();
    if (!_hasProjects){
        return;
    }

    data = data.trimmed();

    QStringList split = data.split("\n");
    for (int i=0; i<split.length(); i++) {
        split[i] = split[i].trimmed();
        createSubCate(split[i]);
    }
}

void ResearchWidget::createSubCate(QString cateDetails) {
    QStringList split = cateDetails.split(":==");

    int paramNo = 2;
    if (split.size() != paramNo) {
        return;
    }

    for (int i = 0; i < split.size(); i++) {
        split[i] = split[i].trimmed();
    }

    ResearchInstance *newProject = new ResearchInstance(split[1]);
    SubCate *newCate = new SubCate(split[0],
                                        "",
                                        "",
                                        QColor (59, 56, 56, 255),
                                        QColor (249, 211, 207, 255),
                                        QColor (240, 146, 136, 255),
                                        newProject);

    connect(newProject, SIGNAL(load(QString,QString)),
            this, SLOT(handleHyperlink(QString,QString)));

    _researchSubCategories.append(newCate);


}

void ResearchWidget::colorWidget(QColor color, QWidget *widget) {
    widget->setAutoFillBackground(true);
    QPalette pallete = widget->palette();
    pallete.setColor(widget->backgroundRole(), color);
    widget->setPalette(pallete);
}

/*
 * Slots
 */

void ResearchWidget::handleHomeSignal() {
    emit this->backToHome();
}

void ResearchWidget::handleHyperlink(QString hyperlink, QString sender) {
    emit this->load(hyperlink, sender);
}


