#include "gui/botleftwidget.h"

const int BotLeftWidget::HEIGHT_EVENTITEM = 150;
const int BotLeftWidget::NUMBER_COLUMN = 1;

const QString BotLeftWidget::ALIGN_EVENTLISTHEADER = "AlignCenter";
const QString BotLeftWidget::FONTSIZE_EVENTLISTHEADER = "10";
const QString BotLeftWidget::FONTSTYLE_EVENTLISTHEADER = "bold";
const QString BotLeftWidget::FORMAT_DATE = "dd MMM yyyy (ddd)";
const QString BotLeftWidget::ICON_CALENDAR = ":/icon/ButtonIcon/calendar.png";
const QString BotLeftWidget::PADDING_EVENTLISTHEADER = "0";
const QString BotLeftWidget::TAG_EVENTLISTHEADER = "_events FOR\n";

const QColor BotLeftWidget::COLOR_CONTENTBACKGROUND = QColor(Qt::white);
const QColor BotLeftWidget::COLOR_CONTENTFONT = QColor(Qt::black);

BotLeftWidget::BotLeftWidget() {
    qDebug() << "AssessmentWidget : Constructing Bottom Left Widget";
    this->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Minimum);
    getUserDetails();
    setUpBotLeftWidgetLayout();
    setUpCalendarToLayout();
    setUpEventListHeaderToLayout();
    setUpEventContentToEventListLayout();
    setUpDummyWidget();
    this->setLayout(_widgetLayout);
    connectQtSignalSlot();
    qDebug() << "AssessmentWidget : Finished constructing Bottom Left Widget";
}

BotLeftWidget::~BotLeftWidget() {
    delete _eventListLayout;
    delete _widgetLayout;
    delete _calendar;
    delete _eventListHeader;
    delete _eventListTable;
}
void BotLeftWidget::getUserDetails() {
    qDebug() << "BotLeftWidget -> getUserRole : Getting user role";
    _userRole = UserDetails::getObject()->getUserRole();
    if (_userRole == UserDetails::ROLE_TRAINER) {
        qDebug() << "BotLeftWidget -> getUserRole : Getting user name";
        _userName = UserDetails::getObject()->getUserName();
        qDebug() << "BotLeftWidget -> getUserRole : User name is " << _userName;
    }
    qDebug() << "BotLeftWidget -> getUserRole : User role is " << _userRole;
}

void BotLeftWidget::setUpBotLeftWidgetLayout() {
    qDebug() << "BotLeftWidget -> setUpBotLeftWidgetLayout : Initializing bottom left widget layout";
    _widgetLayout = new QVBoxLayout;
    qDebug() << "BotLeftWidget -> setUpBotLeftWidgetLayout : Finished initializing bottom left widget layout";
}

void BotLeftWidget::setUpCalendarToLayout() {
    qDebug() << "BotLeftWidget -> setUpCalendarToLayout : "
             << "Initializing calendar widget and adding to bottom left widget layout";
    _calendar = new MyCalendarWidget();
    _calendar->setVisible(true);
    _widgetLayout->addWidget(_calendar);
    qDebug() << "BotLeftWidget -> setUpCalendarToLayout : "
             << "Finish initializing calendar widget and adding to bottom left widget layout";
}

void BotLeftWidget::setUpEventListHeaderToLayout() {
    qDebug() << "BotLeftWidget -> setUpEventListHeaderToLayout : "
             << "Initializing event list header and adding to bottom left widget layout";
    QHBoxLayout *eventListHeaderLayout = new QHBoxLayout();
    eventListHeaderLayout->setAlignment(Qt::AlignLeft);

    QPixmap calendarIcon = QPixmap(ICON_CALENDAR);
    QLabel *calendarIconLabel = new QLabel();
    calendarIconLabel->setPixmap(calendarIcon);
    eventListHeaderLayout->addWidget(calendarIconLabel);

    _eventListHeader = new QLabel();
    _eventListHeader->setText(QDate::currentDate().toString(FORMAT_DATE));
    _eventListHeader->setStyleSheet("font:" + FONTSIZE_EVENTLISTHEADER + "pt;"
                                   "qproperty-alignment:" + ALIGN_EVENTLISTHEADER + ";"
                                   "padding-top:" + PADDING_EVENTLISTHEADER + "px;");
    eventListHeaderLayout->addWidget(_eventListHeader);

    QFrame *eventListHeaderFrame = new QFrame();
    eventListHeaderFrame->setLayout(eventListHeaderLayout);

    _widgetLayout->addWidget(eventListHeaderFrame);

    QFrame *line = new QFrame();
    line->setFrameShape(QFrame::HLine);
    line->setLineWidth(3);

    _widgetLayout->addWidget(line);
    qDebug() << "BotLeftWidget -> setUpEventListHeaderToLayout : "
             << "Finished initializing event list header and adding to bottom left widget layout";
}

void BotLeftWidget::addEventListContent()
{
    _events = _trainingModel->getTrainingActivities();

    _eventListTable->deleteAll();
    for (int i = 0; i < _events.size(); i++) {
        _events.at(i)->constructClickableSummary();
        ClickableWidget *clickableSummary = _events.at(i)->getClickableSummary();

        QVector<QWidget*> contentOfARow = QVector<QWidget*>()
                                            << clickableSummary;
        qDebug() << _events.at(i)->getName() << " is added to eventListLayout";
        if (_userRole == UserDetails::ROLE_TRAINEE) {
            if (_events.at(i)->getDate() == _calendar->selectedDate() &&
                    _events.at(i)->getIsRegistered()) {
                _eventListTable->addRow(contentOfARow);
            } else {
                // do nothing
            }
        } else if (_userRole == UserDetails::ROLE_TRAINER) {
            if (_events.at(i)->getDate() == _calendar->selectedDate() &&
                    _events.at(i)->getTrainer() == _userName) {
                _eventListTable->addRow(contentOfARow);
            } else {
                // do nothing
            }
        } else {
            //do nothing
        }
    }
}

void BotLeftWidget::setUpEventContentToEventListLayout() {
    qDebug() << "BotLeftWidget -> setUpEventContentToEventListLayout : "
             << "Adding event item to event list layout";

    _eventListLayout = new QVBoxLayout;

    QVector<int> columnsWidth = QVector<int>() << _calendar->width();
    _eventListTable = new TableWidget(false, NUMBER_COLUMN, columnsWidth);
    _eventListTable->setContBackground(COLOR_CONTENTBACKGROUND);
    _eventListTable->setContFont(COLOR_CONTENTFONT);
    _eventListTable->setNoGrid();
    _eventListLayout->addWidget(_eventListTable);

    _trainingModel = TrainingModel::getObject();
    connect(_trainingModel, SIGNAL(activitiesInfoRefreshed()), this, SLOT(updateEventList()));
    addEventListContent();

    _widgetLayout->addLayout(_eventListLayout);
    qDebug() << "BotLeftWidget -> setUpEventContentToEventListLayout : "
             << "Finished adding event item to event list layout";
}

void BotLeftWidget::updateEventListHeader() {
    qDebug() << "BotLeftWidget -> updateEventListHeader : "
             << "Updating event list header to " << _calendar->selectedDate().toString(FORMAT_DATE);
    _eventListHeader->setText(_calendar->selectedDate().toString(FORMAT_DATE));
    qDebug() << "BotLeftWidget -> updateEventListHeader : "
             << "Updated event list header to " << _calendar->selectedDate().toString(FORMAT_DATE);
}

void BotLeftWidget::updateEventList() {
    qDebug() << "BotLeftWidget -> updateEventList : "
             << "Updating event list";

    addEventListContent();

    qDebug() << "BotLeftWidget -> updateEventList : "
             << "Updated event list";
}

void BotLeftWidget::setUpDummyWidget() {
    QWidget *dummyWidget = new QWidget();
    dummyWidget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    _widgetLayout->addWidget(dummyWidget);
}

void BotLeftWidget::connectQtSignalSlot() {
    qDebug() << "BotLeftWidget -> connectQtSignalSlot : "
             << "Connecting calendar selectedDateUpdated signal to bottom left widget updateEventListHeader slot";
    connect(_calendar, SIGNAL(selectionChanged()),
            this, SLOT(updateEventListHeader()));
    qDebug() << "BotLeftWidget -> connectQtSignalSlot : "
             << "Connected calendar selectedDateUpdated signal to bottom left widget updateEventListHeader slot";

    qDebug() << "BotLeftWidget -> connectQtSignalSlot : "
             << "Connecting calendar selectedDateUpdated signal to bottom left widget updateEventList slot";
    connect(_calendar, SIGNAL(selectionChanged()),
            this, SLOT(updateEventList()));
    qDebug() << "BotLeftWidget -> connectQtSignalSlot : "
             << "Connected calendar selectedDateUpdated signal to bottom left widget updateEventList slot";
}
