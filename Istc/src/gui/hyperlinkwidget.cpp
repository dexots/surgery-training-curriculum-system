#include "gui/hyperlinkwidget.h"

const QString HyperlinkWidget::ICON_ADD = QString::fromUtf8(":/icon/ButtonIcon/add.png");
const QString HyperlinkWidget::DELIMITER = " :== ";
const QString HyperlinkWidget::NEWLINE = "\n";

HyperlinkWidget::HyperlinkWidget(QString resourceType) {
    _resourceType = resourceType;
    HyperlinkWidget::getUserRole();
    HyperlinkWidget::initiateWidget();
}

HyperlinkWidget::~HyperlinkWidget() {
    for (int i = _displayTexts.size() - 1; i >= 0; i--) {
        delete _displayTexts.at(i);
    }

    delete _listLayout;
    delete _list;
    delete _area;
    delete _widgetLayout;
}

//public method
void HyperlinkWidget::addLinkAndText(QVector<QString> hyperlinks
                                     , QVector<QString> texts) {
    assert (hyperlinks.size() == texts.size());

    for (int i = 0; i < hyperlinks.size(); i++) {
        addLinkAndText(hyperlinks.at(i),
                       texts.at(i));
    }
}

void HyperlinkWidget::addLinkAndText(QString hyperlink, QString text) {
    QLabel *label = new QLabel;
    label->setText(text);
    label->setAlignment(Qt::AlignTop | Qt::AlignLeft);
    label->setFont(_font);
    HyperlinkWidget::changeColor(Qt::blue, Qt::transparent, label);

    ClickableWidget *displayText = new ClickableWidget(label);
    _listLayout->addWidget(displayText);
    connect (displayText, SIGNAL(isClicked()),
             this, SLOT(handleClick()));

    _displayTexts.append(displayText);
    _hyperlinks.append(hyperlink);
    _texts.append(text);
}

void HyperlinkWidget::initiateWidget() {
    _widgetLayout = new QVBoxLayout;
    this->setLayout(_widgetLayout);

    HyperlinkWidget::setUpAddButton();

    _listLayout = new QVBoxLayout;
    _list = new QWidget;
    _list->setLayout(_listLayout);

    _font.setBold(true);
    _font.setUnderline(true);

    HyperlinkWidget::setupScrollArea();
}

void HyperlinkWidget::getUserRole() {
    UserDetails *userDetails = UserDetails::getObject();
    _userRole = userDetails->getUserRole();
}

void HyperlinkWidget::setUpAddButton() {
    QLabel *addRes = new QLabel;
    addRes->setText("Add Resource");
    addRes->setSizePolicy(QSizePolicy::Fixed,
                               QSizePolicy::Fixed);

    QLabel *addLogo = new QLabel;
    addLogo->setPixmap(QPixmap(ICON_ADD));
    addLogo->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

    QHBoxLayout *addLayout = new QHBoxLayout;
    addLayout->addWidget(addRes);
    addLayout->addWidget(addLogo);
    addLayout->setContentsMargins(0, 0, 0, 0);

    QWidget *addWidget = new QWidget;
    addWidget->setLayout(addLayout);
    ClickableWidget *addClick = new ClickableWidget(addWidget);

    connect(addClick, SIGNAL(isClicked()),
            this, SLOT(handleAdd()));

    QHBoxLayout *layout = new QHBoxLayout;
    layout->addWidget(addClick);
    layout->setAlignment(addClick, Qt::AlignRight);

    if (_userRole == UserDetails::ROLE_TRAINER) {
        _widgetLayout->addLayout(layout);
    }

}


void HyperlinkWidget::setupScrollArea() {
    _area = new QScrollArea(this);
    _area->setWidgetResizable(true);
    _area->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    _area->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    _area->setWidget(_list);
    _area->setContentsMargins(0, 0, 0, 0);
    _area->setFrameStyle(QFrame::NoFrame);
    _widgetLayout->addWidget(_area);

}

void HyperlinkWidget::changeColor(QColor font, QColor back, QWidget *widget) {
    widget->setAutoFillBackground(true);
    QPalette pallete = widget->palette();
    pallete.setColor(widget->foregroundRole(), font);
    pallete.setColor(widget->backgroundRole(), back);
    widget->setPalette(pallete);
}

//private slots
void HyperlinkWidget::handleClick() {
    QWidget *buttonWidget = qobject_cast<QWidget*>(sender());
    if (!buttonWidget) {
        return;
    }

    int indexOfButton = _listLayout->indexOf(buttonWidget);

    QString hyperlink = _hyperlinks.at(indexOfButton);

    emit this->openLink(hyperlink);
}

void HyperlinkWidget::handleAdd() {
    ResourcesBox *resBox = new ResourcesBox(_resourceType);
    resBox->exec();
    connect(resBox, SIGNAL(passInfo(QVector<QString>)),
            this, SLOT(writeFileContent(QVector<QString>)));

}

void HyperlinkWidget::writeFileContent(QVector<QString> info) {
    _texts.append(info.at(0));
    _hyperlinks.append(info.at(1));

    QString fileLink;
    if (_resourceType == ResourceWidget::TYPE_JOURNAL) {
        fileLink = ResourceWidget::JOURNAL_LINK;
    } else if (_resourceType == ResourceWidget::TYPE_QUESTION) {
        fileLink = ResourceWidget::QUESTION_LINK;
    } else if (_resourceType == ResourceWidget::TYPE_TEXTBOOK) {
        fileLink = ResourceWidget::TEXTBOOK_LINK;
    } else {
        assert (false);
    }

    QFile outputFile(fileLink);
    if (outputFile.open(QIODevice::WriteOnly |
                        QIODevice::Text)) {
        QString string = "";
        for (int i=0; i<_texts.length(); i++) {
            string = string + _texts.at(i) + DELIMITER
                    + _hyperlinks.at(i) + NEWLINE;
        }

        QTextStream out(&outputFile);
        out << string;
        outputFile.close();
    }

    showSuccessBox();
}

void HyperlinkWidget::showSuccessBox() {
    GeneralPopupBox *genBox = new GeneralPopupBox("", 0);
    genBox->exec();
}


