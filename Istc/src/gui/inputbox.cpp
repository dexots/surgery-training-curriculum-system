#include "gui/inputbox.h"

InputBox::InputBox() {
    this->setTabChangesFocus(true);
}

InputBox::~InputBox() {
}

//public
void InputBox::keyPressEvent(QKeyEvent *e) {
    if (e->key() == Qt::Key_Return) {
        QString inputString = this->toPlainText();
        //std::cerr<<inputString.toStdString();
        this->clear();
        emit this->stringReady(inputString);
    } else {
        QTextEdit::keyPressEvent(e);
    }
}

void InputBox::focusOutEvent(QFocusEvent *e){
    if (e->lostFocus()){
        QString inputString = this->toPlainText();
        this->clear();
        emit this->stringReady(inputString);
    }
    QTextEdit::focusOutEvent(e);
}
