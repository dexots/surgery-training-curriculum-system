#include "gui/clickablewidget.h"

const QString ClickableWidget::STRING_EMPTY = "";

ClickableWidget::ClickableWidget(QWidget *widget) {
    initializeWidget(widget);
}

ClickableWidget::ClickableWidget(QWidget *widget, QWidget *parent = 0) :
    QWidget(parent) {
    initializeWidget(widget);
}

ClickableWidget::~ClickableWidget() {
}

QString ClickableWidget::getWidgetName() {
    return _widgetName;
}

//private
void ClickableWidget::initializeWidget(QWidget *widget) {
    QVBoxLayout *layout = new QVBoxLayout;
    widget->setCursor(Qt::PointingHandCursor);
    _widgetName = widget->accessibleName();
    layout->addWidget(widget);
    this->setLayout(layout);
}

//protected
void ClickableWidget::mousePressEvent(QMouseEvent *event) {
    if (event->button() == Qt::LeftButton && this->isEnabled()) {
        emit this->isClicked();

        if (UserDetails::getObject()->isNotInitialized()) {
            return;
        }

        if (UserDetails::getObject()->getUserRole() == UserDetails::ROLE_TRAINEE) {
            emit this->isClicked(_widgetName);
            emit this->isClicked(true);
        }
    }
}

bool ClickableWidget::event(QEvent *event) {
    QInputEvent* inputEvent = dynamic_cast<QInputEvent*>(event);
    if (inputEvent && this->isEnabled()) {
        switch (inputEvent->type()){
            case QEvent::TouchBegin: {
                ClickableWidget::handleTouch();
                return true;
            }

            case QEvent::MouseButtonPress: {
                QMouseEvent* mouseEvent = dynamic_cast<QMouseEvent*>(inputEvent );
                ClickableWidget::mousePressEvent(mouseEvent);
                return true;
            }

            default: {
                return false;
            }
        }
    }
    return false;
}

void ClickableWidget::handleTouch() {
    emit this->isClicked();
    emit this->isClicked(_widgetName);
    emit this->isClicked(true);
}
