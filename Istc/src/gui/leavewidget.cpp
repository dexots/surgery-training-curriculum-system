#include "gui/leavewidget.h"

const QString LeaveWidget::BORDER_DATEEDIT = "0px";

const QString LeaveWidget::COLOR_APPLYBUTTON = "rgb(39, 174, 96)";
const QString LeaveWidget::COLOR_BUTTONTEXT = "white";
const QString LeaveWidget::COLOR_DATEEDITBACKGROUND = "rgb(99, 145, 213)";
const QString LeaveWidget::COLOR_RESETBUTTON = "rgb(231, 76, 60)";

const QColor LeaveWidget::COLOR_CONTENTBACKGROUND = QColor(219, 237, 249, 255);
const QColor LeaveWidget::COLOR_CONTENTFONT = QColor(Qt::black);
const QColor LeaveWidget::COLOR_HEADERBACKGROUND = QColor(99, 145, 213, 255);
const QColor LeaveWidget::COLOR_HEADERTEXT = QColor(Qt::white);
const QColor LeaveWidget::COLOR_BACK = QColor(145, 199, 235, 255);

const QString LeaveWidget::HEIGHT_BUTTON = "40";
const QString LeaveWidget::WIDTH_BUTTON = "130";
const QString LeaveWidget::LEFTMARGIN_BUTTON = "20";
const QString LeaveWidget::MESSAGE_DAYSLEFT = "<b> days left</b>";
const QString LeaveWidget::RADIUS_BUTTONBORDER = "5";
const QString LeaveWidget::SIZE_FONT = "12";

const QString LeaveWidget::TAG_ENDDATE = "<b>End date:</b>";
const QString LeaveWidget::TAG_LEAVEAPPLICATION = "<b>Leave application:</b>";
const QString LeaveWidget::TAG_LEAVEHISTORY = "<b>Leave History:</b>";
const QString LeaveWidget::TAG_STARTDATE = "<b>Start date:</b>";
const QString LeaveWidget::TAG_TOTALDAYS = "<b>Total days: </b>";

const QVector<int> LeaveWidget::WIDTH_COLUMN (QVector<int>() << 40 << 180 << 180 << 180 << 220);

const QVector<QString> LeaveWidget::HEADER_TABLE (QVector<QString>() << "No" << "Start Date"
                                                     << "End Date" << "Number of days" << "Application Status");

const int LeaveWidget::DAY_LEAVE = 12;
const int LeaveWidget::NUMBER_COLUMN = 5;


LeaveWidget::LeaveWidget() {
    _rosterModel = RosterModel::getObject();

    setUpLeaveLayout();
    setUpLeaveApplicationLayout();
    setUpLeaveHistoryLayout();
    setUpDummyWidget();
    setWidgetStyleSheet();

    setLayout(_leaveLayout);
}

LeaveWidget::~LeaveWidget() {
    delete _leaveLayout;
    delete _leaveApplicationLayout;

    delete _daysLeftLabel;
    delete _totalDaysLabel;

    delete _remainingDayBar;
    delete _startDate;
    delete _endDate;
    delete _leaveHistoryTable;
    delete _dummyWidget;
}

void LeaveWidget::setUpLeaveLayout() {
    _leaveApplicationLayout = new QVBoxLayout();
    _leaveApplicationLayout->setAlignment(Qt::AlignTop | Qt::AlignLeft);
    _leaveLayout = new QVBoxLayout();
    _leaveLayout->setAlignment(Qt::AlignTop | Qt::AlignCenter);
}

void LeaveWidget::setUpLeaveApplicationLayout() {
    setUpRemaingDayBar();
    setUpDatePickLayout();

    QFrame *leaveApplicationFrame = new QFrame();
    leaveApplicationFrame->setLayout(_leaveApplicationLayout);

    _leaveLayout->addWidget(leaveApplicationFrame);
}

void LeaveWidget::setUpRemaingDayBar() {
    int daysHave = _rosterModel->getNumberOfLeaveApplications();
    _daysLeft = DAY_LEAVE - daysHave;

    _daysLeftLabel = new QLabel("<b>" + QString::number(_daysLeft) + "</b>" + MESSAGE_DAYSLEFT);
    _leaveApplicationLayout->addWidget(_daysLeftLabel);

    _remainingDayBar = new ProgressBar();
    _remainingDayBar->setBadToGood(true);
    _remainingDayBar->setMinimum(0);
    _remainingDayBar->setMaximum(DAY_LEAVE);
    _remainingDayBar->setValue(_daysLeft);
    _leaveApplicationLayout->addWidget(_remainingDayBar);
}

void LeaveWidget::setUpDatePickLayout()
{
    QLabel *leaveApplicationLabel = new QLabel(TAG_LEAVEAPPLICATION);
    _leaveApplicationLayout->addWidget(leaveApplicationLabel);

    // set up start date line
    QHBoxLayout *startDateLayout = new QHBoxLayout();
    startDateLayout->setAlignment(Qt::AlignLeft);

    QLabel * startDateTag = new QLabel(TAG_STARTDATE);
    startDateLayout->addWidget(startDateTag);

    MyCalendarWidget *startCalendar = new MyCalendarWidget();

    _startDate = new QDateEdit(QDate::currentDate());
    _startDate->setMinimumDate(QDate::currentDate());
    _startDate->setCalendarPopup(true);
    _startDate->setCalendarWidget(startCalendar);
    _startDate->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
    _startDate->setCursor(Qt::PointingHandCursor);
    startDateLayout->addWidget(_startDate);

    QFrame *startDateFrame = new QFrame();
    startDateFrame->setLayout(startDateLayout);

    _leaveApplicationLayout->addWidget(startDateFrame);

    // set up end date line
    QHBoxLayout *endDateLayout = new QHBoxLayout();
    endDateLayout->setAlignment(Qt::AlignLeft);

    QLabel * endDateTag = new QLabel(TAG_ENDDATE);
    endDateLayout->addWidget(endDateTag);

    MyCalendarWidget *endCalendar = new MyCalendarWidget();

    _endDate = new QDateEdit(QDate::currentDate());
    _endDate->setMinimumDate(QDate::currentDate());
    _endDate->setCalendarPopup(true);
    _endDate->setCalendarWidget(endCalendar);
    _endDate->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
    _endDate->setCursor(Qt::PointingHandCursor);
    endDateLayout->addWidget(_endDate);

    QFrame *endDateFrame = new QFrame();
    endDateFrame->setLayout(endDateLayout);

    _leaveApplicationLayout->addWidget(endDateFrame);

    //connect date edit to total days
    connect(_startDate, SIGNAL(dateChanged(QDate)), this, SLOT(updateTotalDays()));
    connect(_endDate, SIGNAL(dateChanged(QDate)), this, SLOT(updateTotalDays()));

    //set up total days in the period
    int totalDays = _endDate->date().toJulianDay() - _startDate->date().toJulianDay() + 1;
    _totalDaysLabel = new QLabel(TAG_TOTALDAYS + QString::number(totalDays));
    _leaveApplicationLayout->addWidget(_totalDaysLabel);

    //set up button
    QHBoxLayout *buttonLayout = new QHBoxLayout();
    buttonLayout->setAlignment(Qt::AlignLeft);

    QPushButton *applyButton = new QPushButton("Apply");
    applyButton->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
    applyButton->setStyleSheet("background-color:" + COLOR_APPLYBUTTON + ";");
    applyButton->setCursor(Qt::PointingHandCursor);
    buttonLayout->addWidget(applyButton);

    QPushButton *resetButton = new QPushButton("Reset");
    resetButton->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
    resetButton->setStyleSheet("background-color:" + COLOR_RESETBUTTON + ";");
    resetButton->setCursor(Qt::PointingHandCursor);
    buttonLayout->addWidget(resetButton);

    QFrame *buttonFrame = new QFrame();
    buttonFrame->setLayout(buttonLayout);

    connect(applyButton, SIGNAL(clicked()), this, SLOT(sendLeaveApplication()));
    connect(resetButton, SIGNAL(clicked()), this, SLOT(resetLeaveApplication()));

    _leaveApplicationLayout->addWidget(buttonFrame);
    LeaveWidget::colorWidget(COLOR_BACK, this);
}

void LeaveWidget::addRowsToTable()
{
    QVector <LeaveObj> datas = _rosterModel->getLeaveApplications();

    for (int i = 0; i < datas.size(); i++) {
        LeaveObj curObj = datas.value(i);

        QString index = QString::number(i + 1);
        QString startDate = curObj.getStartDate().toString("dd MMM yyyy");
        QString endDate = curObj.getEndDate().toString("dd MMM yyyy");
        QString noOfDate = QString::number(curObj.getNumOfDates());
        QString status = curObj.getApplicationStatus();
        QVector<QString> contentOfARow = QVector<QString>()
                                        << index
                                        << startDate
                                        << endDate
                                        << noOfDate
                                        << status;
        _leaveHistoryTable->addRow(contentOfARow);
    }
    _currentIndex = datas.size() + 1;
}

void LeaveWidget::setUpLeaveHistoryLayout() {
    QVBoxLayout *leaveHistoryLayout = new QVBoxLayout();

    QLabel *newline = new QLabel("");
    leaveHistoryLayout->addWidget(newline);

    QLabel *leaveHistoryLabel = new QLabel(TAG_LEAVEHISTORY);
    leaveHistoryLayout->addWidget(leaveHistoryLabel);

    _leaveHistoryTable = new TableWidget(true, NUMBER_COLUMN, WIDTH_COLUMN);
    _leaveHistoryTable->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);

    _leaveHistoryTable->setHeader(HEADER_TABLE, COLOR_HEADERTEXT, COLOR_HEADERBACKGROUND);
    _leaveHistoryTable->setContBackground(COLOR_CONTENTBACKGROUND);
    _leaveHistoryTable->setContFont(COLOR_CONTENTFONT);

    addRowsToTable();

    leaveHistoryLayout->addWidget(_leaveHistoryTable);

    QFrame *leaveHistoryFrame = new QFrame();
    leaveHistoryFrame->setLayout(leaveHistoryLayout);

    _leaveLayout->addWidget(leaveHistoryFrame);

    connect (_leaveHistoryTable, SIGNAL(resizedTable(int)),
             this, SLOT(handleTableResized(int)));
}

void LeaveWidget::setWidgetStyleSheet() {
    this->setStyleSheet("QLabel{font:" + SIZE_FONT + "px;}"
                        "QDateEdit{background-color:" + COLOR_DATEEDITBACKGROUND + ";"
                                  "border:" + BORDER_DATEEDIT + ";}"
                        "QPushButton{color:" + COLOR_BUTTONTEXT + ";"
                                     "margin-left:" + LEFTMARGIN_BUTTON + "px;"
                                     "border-radius:" + RADIUS_BUTTONBORDER + "px;"
                                     "width:" + WIDTH_BUTTON + "px;"
                                     "height:" + HEIGHT_BUTTON + "px;}");
}

void LeaveWidget::updateTotalDays() {
    //make endDate at least equal to start date
    if (_endDate->date().toJulianDay() < _startDate->date().toJulianDay()) {
        _endDate->setDate(_startDate->date());
    }

    int totalDays = _endDate->date().toJulianDay() - _startDate->date().toJulianDay() + 1;
    _totalDaysLabel->setText(TAG_TOTALDAYS + QString::number(totalDays));
}

void LeaveWidget::sendLeaveApplication() {
    QString leaveId = _rosterModel->generateLeaveId();
    QString status = "Pending";

    LeaveObj obj;
    obj.setLeaveId(leaveId);
    obj.setApplicationStatus(status);
    obj.setStartDate(_startDate->date());
    obj.setEndDate(_endDate->date());

    bool isSuccess = sendToServer(obj);

    //to do: refactor to method
    if (isSuccess) {

        int totalDays = _endDate->date().toJulianDay() - _startDate->date().toJulianDay() + 1;

        QVector<QString> contentOfARow = QVector<QString>()
                << QString::number(_currentIndex)
                << _startDate->date().toString("dd MMM yyyy")
                << _endDate->date().toString("dd MMM yyyy")
                << QString::number(totalDays)
                << status;
        _leaveHistoryTable->addRow(contentOfARow, _currentIndex - 1);
        _currentIndex++;


        resetLeaveApplication();
    }
}

void LeaveWidget::setUpDummyWidget() {
    _dummyWidget = new QWidget();
    _dummyWidget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    _leaveLayout->addWidget(_dummyWidget);
}

// TODO: Send leave to server
// send error msg
bool LeaveWidget::sendToServer(LeaveObj leave) {
    bool isSuccess = _rosterModel->addLeaveApplication(leave);

    if (isSuccess) {
        GeneralPopupBox *generalPopupBox = new GeneralPopupBox("", 0);
        generalPopupBox->exec();
    } else {
        std::cout << "not successfully connect to server!\n";
    }
    return isSuccess;
}

void LeaveWidget::resetLeaveApplication() {
    _startDate->setDate(QDate::currentDate());
    _endDate->setDate(QDate::currentDate());
    updateTotalDays();
}

void LeaveWidget::colorWidget(QColor color, QWidget *widget) {
    widget->setAutoFillBackground(true);
    QPalette pallete = widget->palette();
    pallete.setColor(widget->foregroundRole(), Qt::black);
    pallete.setColor(widget->backgroundRole(), color);
    widget->setPalette(pallete);
}


/*
 *  Private Slots
 */
void LeaveWidget::handleTableResized(int minHeightChanged) {

    int height = _dummyWidget->height();

    int difference = height - minHeightChanged;
    if (difference <= 0) {
        difference = 0;
        _dummyWidget->hide();
    } else {
        _dummyWidget->show();
    }
    _dummyWidget->setMaximumHeight(difference);

}

/*
 *  Public Slots
 */
void LeaveWidget::updateTable() {
    assert(_leaveHistoryTable);
    _leaveHistoryTable->deleteAll();
    _dummyWidget->setMinimumHeight(this->height());
    addRowsToTable();
}
