#include "gui/curriculumwidget.h"

const QString CurriculumWidget::BORDER_DATEEDIT = "0px";
const QString CurriculumWidget::BOTTOMBORDER_CHECKBOXINDICATOR = "3px solid black";
const QString CurriculumWidget::LEFTBORDER_CHECKBOXINDICATOR = "1px solid black";
const QString CurriculumWidget::LEFTMARGIN_BUTTON = "20";
const QString CurriculumWidget::LEFTPADDING_CHECKBOX = "40";
const QString CurriculumWidget::RADIUS_BUTTONBORDER = "5";
const QString CurriculumWidget::RIGHTBORDER_CHECKBOXINDICATOR = "3px solid black";

const QString CurriculumWidget::COLOR_BUTTON = "rgb(39, 174, 96)";
const QString CurriculumWidget::COLOR_BUTTONTEXT = "white";
const QString CurriculumWidget::COLOR_DATEEDITBACKGROUND = "rgb(178, 127, 199)";

const QString CurriculumWidget::HEIGHT_BUTTON = "40";
const QString CurriculumWidget::WIDTH_BUTTON = "130";

const QString CurriculumWidget::ICON_CHECK = "url(:/icon/ButtonIcon/check.png)";
const QString CurriculumWidget::LABEL_TO = "<b>to</b>";

const QString CurriculumWidget::MESSAGE_GENERATECV = "<b>Generate Curriculum Vitae (CV) for period from</b>";
const QString CurriculumWidget::NEWLINE = "";
const QString CurriculumWidget::PADDING_CHECKBOX = "7";
const QString CurriculumWidget::PADDING_LABEL = "7";
const QString CurriculumWidget::SIZE_FONT = "15";

const QString CurriculumWidget::TAG_DOCX = "DOCX";
const QString CurriculumWidget::TAG_EDUCATION = "Education";
const QString CurriculumWidget::TAG_FORMAT = "<b>In format:</b>";
const QString CurriculumWidget::TAG_GENERATE = "Generate";
const QString CurriculumWidget::TAG_PDF = "PDF";
const QString CurriculumWidget::TAG_RESEARCH = "Research";
const QString CurriculumWidget::TAG_TRAINING = "Training";

const QString CurriculumWidget::TOPBORDER_CHECKBOXINDICATOR = "1px solid black";

CurriculumWidget::CurriculumWidget() {
    setUpCurriculumLayout();
    setGenerateType();
    //setGenerateFormat();
    singleGenerateFormat();     //For only generating in PDF
    setGenerateButton();
    setWidgetStyleSheet();
    setLayout(_curriculumLayout);
}

CurriculumWidget::~CurriculumWidget() {
    delete _curriculumLayout;
}

void CurriculumWidget::setUpCurriculumLayout() {
    _curriculumLayout = new QVBoxLayout();
    _curriculumLayout->setAlignment(Qt::AlignTop | Qt::AlignLeft);
}

void CurriculumWidget::setGenerateType() {
    QHBoxLayout *generatePeriodLayout = new QHBoxLayout();
    generatePeriodLayout->setAlignment(Qt::AlignLeft);

    QLabel *generatePeriodLabel = new QLabel(MESSAGE_GENERATECV);
    generatePeriodLayout->addWidget(generatePeriodLabel);

    MyCalendarWidget *fromDateCalendar = new MyCalendarWidget();
    QDateEdit *fromDate = new QDateEdit(QDate::currentDate().addYears(-3));
    fromDate->setCalendarPopup(true);
    fromDate->setCalendarWidget(fromDateCalendar);
    fromDate->setDisplayFormat("dd MMMM yyyy");
    generatePeriodLayout->addWidget(fromDate);

    QLabel *toLabel = new QLabel(LABEL_TO);
    generatePeriodLayout->addWidget(toLabel);

    MyCalendarWidget *tillDateCalendar = new MyCalendarWidget();
    QDateEdit *tillDate = new QDateEdit(QDate::currentDate());
    tillDate->setCalendarPopup(true);
    tillDate->setCalendarWidget(tillDateCalendar);
    tillDate->setDisplayFormat("dd MMMM yyyy");
    generatePeriodLayout->addWidget(tillDate);

    QFrame *generatePeriodLine = new QFrame();
    generatePeriodLine->setLayout(generatePeriodLayout);
    _curriculumLayout->addWidget(generatePeriodLine);

    QCheckBox *educationBox = new QCheckBox(TAG_EDUCATION);
    educationBox->setChecked(true);
    _curriculumLayout->addWidget(educationBox);

    QCheckBox *trainingBox = new QCheckBox(TAG_TRAINING);
    trainingBox->setChecked(true);
    _curriculumLayout->addWidget(trainingBox);

    QCheckBox *researchBox = new QCheckBox(TAG_RESEARCH);
    researchBox->setChecked(true);
    _curriculumLayout->addWidget(researchBox);
}

void CurriculumWidget::singleGenerateFormat() {
    QLabel *newlineLabel = new QLabel(NEWLINE);
    _curriculumLayout->addWidget(newlineLabel);

    QLabel *formatLabel = new QLabel("Generate CV in PDF Format");
    _curriculumLayout->addWidget(formatLabel);
}

void CurriculumWidget::setGenerateFormat() {
    QLabel *newlineLabel = new QLabel(NEWLINE);
    _curriculumLayout->addWidget(newlineLabel);

    QHBoxLayout *formatLayout = new QHBoxLayout();
    QLabel *inFormatLabel = new QLabel(TAG_FORMAT);
    formatLayout->addWidget(inFormatLabel);
    QFrame *inFormatLine = new QFrame();
    inFormatLine->setLayout(formatLayout);
    _curriculumLayout->addWidget(inFormatLine);

    QCheckBox *docxBox = new QCheckBox(TAG_DOCX);
    docxBox->setChecked(true);
    _curriculumLayout->addWidget(docxBox);    //Future enhancements: Add write to docx

    QCheckBox *pdfBox = new QCheckBox(TAG_PDF);
    pdfBox->setChecked(true);
    _curriculumLayout->addWidget(pdfBox);
}

void CurriculumWidget::setGenerateButton() {
    QLabel *newlineLabel = new QLabel(NEWLINE);
    _curriculumLayout->addWidget(newlineLabel);

    QPushButton *generateButton = new QPushButton(TAG_GENERATE);
    generateButton->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
    generateButton->setCursor(Qt::PointingHandCursor);
    connect(generateButton, SIGNAL(clicked()), this, SLOT(generateCV()));

    _curriculumLayout->addWidget(generateButton);
}

void CurriculumWidget::setWidgetStyleSheet() {
    this->setStyleSheet("QLabel{font:" + SIZE_FONT + "px;"
                        "padding:" + PADDING_LABEL + "px;}"
                        "QCheckBox{font:" + SIZE_FONT + "px;"
                                   "padding:" + PADDING_CHECKBOX + "px;"
                                   "padding-left:" + LEFTPADDING_CHECKBOX + "px;}"
                        "QCheckBox::indicator:unchecked{border-left:" + LEFTBORDER_CHECKBOXINDICATOR + ";"
                                                       "border-top:" + TOPBORDER_CHECKBOXINDICATOR + ";"
                                                       "border-right:" + RIGHTBORDER_CHECKBOXINDICATOR + ";"
                                                       "border-bottom:" + BOTTOMBORDER_CHECKBOXINDICATOR + ";}"
                        "QCheckBox::indicator:checked{border-left:" + LEFTBORDER_CHECKBOXINDICATOR + ";"
                                                     "border-top:" + TOPBORDER_CHECKBOXINDICATOR + ";"
                                                     "border-right:" + RIGHTBORDER_CHECKBOXINDICATOR + ";"
                                                     "border-bottom:" + BOTTOMBORDER_CHECKBOXINDICATOR + ";"
                                                     "image:" + ICON_CHECK + ";}"
                        "QDateEdit{background-color:" + COLOR_DATEEDITBACKGROUND + ";"
                                  "font:" + SIZE_FONT + "px;"
                                  "border:" + BORDER_DATEEDIT + ";}"
                        "QPushButton{background-color:" + COLOR_BUTTON + ";"
                                    "font:" + SIZE_FONT + "px;"
                                    "color:" + COLOR_BUTTONTEXT + ";"
                                    "margin-left:" + LEFTMARGIN_BUTTON + "px;"
                                    "border-radius:" + RADIUS_BUTTONBORDER + "px;"
                                    "width:" + WIDTH_BUTTON + "px;"
                                    "height:" + HEIGHT_BUTTON + "px;}");

}

//TODO: Add the data part in
bool CurriculumWidget::generateCV() {
    QString location = QStandardPaths::writableLocation(QStandardPaths::DesktopLocation);
    location.append("/cv.pdf");

    QString saveFileName = QFileDialog::getSaveFileName(this, tr("Save File"),
                            location);

    QTextDocument doc;
    doc.setHtml("Hello, I am testing pdf");
    _printer.setOutputFileName(saveFileName);
    _printer.setOutputFormat(QPrinter::PdfFormat);

    doc.print(&_printer);
    _printer.newPage();

    cvPopUp(saveFileName);

    return true;
}

void CurriculumWidget::cvPopUp(QString saveFileName) {
    GeneralPopupBox *cvBox = new GeneralPopupBox(saveFileName, 1);
    cvBox->exec();
}

