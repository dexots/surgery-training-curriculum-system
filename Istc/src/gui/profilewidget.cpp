#include "gui/profilewidget.h"

ProfileWidget::ProfileWidget() {
    ProfileWidget::createLayout();
}

ProfileWidget::~ProfileWidget() {
    delete _widgetLayout;
}

void ProfileWidget::createLayout() {
    _widgetLayout = new QVBoxLayout;

    // set up personal details subpage
    PersonalWidget *personalWidget = new PersonalWidget();
    ProfileWidget::colorWidget(QColor (178, 127, 199, 255), personalWidget);

    SubCate *personalCate = new SubCate("Profile",
                                        "View Personal Details",
                                        "",
                                        Qt::white, //desc font color
                                        QColor (207, 174, 220, 255), //default color
                                        QColor (178, 127, 199, 255), //selected color
                                        personalWidget);

    // set up curriculum vitae subpage
    CurriculumWidget *curriculumWidget = new CurriculumWidget();
    ProfileWidget::colorWidget(QColor (178, 127, 199, 255), curriculumWidget);



    SubCate *curriculumCate = new SubCate("Curriculum Vitae",
                                        "View your Professional Profile",
                                        "",
                                        Qt::white, //desc font color
                                        QColor (207, 174, 220, 255), //default color
                                        QColor (178, 127, 199, 255), //selected color
                                        curriculumWidget);

    // set up assessment subpage
    AssessmentWidget *assessmentWidget = new AssessmentWidget();
    ProfileWidget::colorWidget(QColor (178, 127, 199, 255),
                              assessmentWidget);

    SubCate *assessmentCate = new SubCate("Assessment",
                                    "View past records and training feedback",
                                    "",
                                    Qt::white, //desc font color
                                    QColor (207, 174, 220, 255), //default color
                                    QColor (178, 127, 199, 255), //selected color
                                    assessmentWidget);

    connect(this, SIGNAL(updateAssessmentTable()), assessmentWidget, SLOT(updateTable()));

    // set up notes subpage
    NotesWidget *notesWidget = new NotesWidget();
    ProfileWidget::colorWidget(QColor (178, 127, 199, 255),
                              notesWidget);

    SubCate *notesCate = new SubCate("Notes",
                                    "Write it down so you won't forget",
                                    "",
                                    Qt::white, //desc font color
                                    QColor (207, 174, 220, 255), //default color
                                    QColor (178, 127, 199, 255), //selected color
                                    notesWidget);

    connect(this, SIGNAL(updateNotesTable()), notesWidget, SLOT(updateTable()));

    QVector <SubCate*> profileSubCategories;
    profileSubCategories.append(personalCate);
    profileSubCategories.append(curriculumCate);
    profileSubCategories.append(assessmentCate);
    profileSubCategories.append(notesCate);


    UserDetails* userDetails = UserDetails::getObject();
    QString name = userDetails->getUserName();
    QString matricNo = userDetails->getMatricNo();
    QString rotation = userDetails->getRotation();
    QString userNameLine = name + " (" + matricNo + ")";
    QString rotationLine = "Current Rotation: " + rotation;

    CateTable *profileTable = new CateTable(userNameLine,
                                            rotationLine,
                                            ":/icon/ButtonIcon/profile logo.png",
                                            QColor (155, 89, 182, 255),
                                            profileSubCategories);

    _widgetLayout->addWidget(profileTable);
    _widgetLayout->setContentsMargins(0, 0, 0, 0);
    this->setLayout(_widgetLayout);

    connect(profileTable, SIGNAL(backToHome()), this, SLOT(handleHomeSignal()));
}

void ProfileWidget::colorWidget(QColor color, QWidget *widget) {
    widget->setAutoFillBackground(true);
    QPalette pallete = widget->palette();
    pallete.setColor(widget->backgroundRole(), color);
    widget->setPalette(pallete);
}

//private slots
void ProfileWidget::handleHomeSignal() {
    emit this->backToHome();
}

//public slots
void ProfileWidget::updateAllTables() {
    emit updateAssessmentTable();
    emit updateNotesTable();
}
