#include "gui/profilebox.h"

const QString ProfileBox::COLOR_BUTTONBACKGROUND = "rgb(39, 174, 96)";
const QString ProfileBox::COLOR_BUTTONBORDER = "white";
const QString ProfileBox::COLOR_BUTTONFONT = "white";
const QString ProfileBox::FONT_BUTTON = "bold 14";
const QString ProfileBox::PADDING_BUTTON = "6";
const QString ProfileBox::RADIUS_BUTTONBORDER = "10";
const QString ProfileBox::STYLE_BUTTONBORDER = "outset";
const QString ProfileBox::WIDTH_BUTTON = "50";
const QString ProfileBox::WIDTH_BUTTONBORDER = "2";

const QString ProfileBox::QSTRING_EMPTY = "";
const QString ProfileBox::QSTRING_UPDATE = "Profile settings updated!";

const int ProfileBox::WIDTH_MINIMUM = 300;
const int ProfileBox::HEIGHT_MINIMUM = 150;

ProfileBox::ProfileBox(QString type, QString inputString) {
    _bold.setBold(true);
    _type = type;

    setUpContentLabel();
    setUpInputBox(type, inputString);
    setUpAddCancelBtn();
    setUpMainLayout();
}

ProfileBox::~ProfileBox() {
    delete _inputContent;
    delete _contentLabel;
    delete _addButton;
    delete _cancelButton;

    delete _mainLayout;
    delete _addCancelLayout;
}

/*
 * Set up Main Layout
 */

void ProfileBox::setUpContentLabel() {
    _contentLabel = new QLabel;
    _contentLabel->setText(_type);
    _contentLabel->setFont(_bold);
}

void ProfileBox::setUpInputBox(QString type, QString inputString) {
    _inputContent = new QLineEdit;
    _inputContent->setPlaceholderText(inputString);
    checkTypeAndSetRestrictions(type);

    connect(_inputContent, SIGNAL(textEdited(QString)),
            this, SLOT(handleContent(QString)));
}

void ProfileBox::checkTypeAndSetRestrictions(QString type) {
    if (type == "Personal Email" ||
        type == "School Email") {

        QRegExp mailRe("[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}");
        mailRe.setCaseSensitivity(Qt::CaseInsensitive);
        mailRe.setPatternSyntax(QRegExp::RegExp);

        QRegExpValidator *v = new QRegExpValidator(this);
        v->setRegExp(mailRe);

        _inputContent->setInputMethodHints(_inputContent->inputMethodHints()
                                           | Qt::ImhEmailCharactersOnly);
        _inputContent->setValidator(v);

    } else if (type == "Home Contact" ||
               type == "Mobile Contact") {
        QIntValidator *v = new QIntValidator(this);
        _inputContent->setInputMethodHints(_inputContent->inputMethodHints()
                                           | Qt::ImhDigitsOnly);
        _inputContent->setValidator(v);

    } else {
        return;
    }
}

void ProfileBox::setUpAddCancelBtn() {
    //Add button
    _addButton = new QPushButton;
    _addButton->setText("Update");
    _addButton->setCursor(Qt::PointingHandCursor);
    setButtonStyleSheet(_addButton);
    connect (_addButton, SIGNAL(clicked()),
             this, SLOT(handleButton()));
    _addButton->setSizePolicy(QSizePolicy::Fixed,
                              QSizePolicy::Fixed);

    //cancel
    _cancelButton = new QPushButton;
    _cancelButton->setText("Cancel");
    _cancelButton->setCursor(Qt::PointingHandCursor);
    setButtonStyleSheet(_cancelButton);
    connect (_cancelButton, SIGNAL(clicked()),
             this, SLOT(handleCancel()));
    _cancelButton->setSizePolicy(QSizePolicy::Fixed,
                                 QSizePolicy::Fixed);

    _addCancelLayout = new QHBoxLayout;
    _addCancelLayout->addWidget(_addButton);
    _addCancelLayout->addWidget(_cancelButton);
    _addCancelLayout->setSpacing(0);
}

void ProfileBox::setUpMainLayout() {
    _mainLayout = new QVBoxLayout;

    _mainLayout->addWidget(_contentLabel);
    _mainLayout->addWidget(_inputContent);
    _mainLayout->addLayout(_addCancelLayout);
    _addButton->hide();

    this->setLayout(_mainLayout);
    this->setContentsMargins(10, 10, 10, 10);
    this->setMinimumWidth(WIDTH_MINIMUM);
    this->setMinimumHeight(HEIGHT_MINIMUM);

    this->setAttribute(Qt::WA_TranslucentBackground, true);

    this->setWindowFlags(Qt::FramelessWindowHint);
}

/*
 * slots
 */

void ProfileBox::handleContent(QString content) {
    if (isNotEmailType() || isValidEmailContent(content)) {
        _addButton->show();
    }
}

bool ProfileBox::isNotEmailType() {
    if (_type == "Personal Email") {
        return false;
    } else if (_type == "School Email") {
        return false;
    } else {
        return true;
    }
}

bool ProfileBox::isValidEmailContent(QString content) {
    if (content.count(QLatin1Char('@')) == 1 &&
            content.contains(".")) {
        return true;
    }
    return false;
}

void ProfileBox::handleButton() {
    _inputString = _inputContent->text();

    // Send to profile widget
    emit this->passInfo(_type, _inputString);
    QDialog::done(0);
}

void ProfileBox::handleCancel() {
    QDialog::done(0);
}

/*
 * Helper functions
 */
void ProfileBox::paintEvent(QPaintEvent *event) {
    QDialog::paintEvent(event);
    QPainter painter(this);
    QBrush brush(Qt::white);
    painter.setBrush(brush);
    painter.setPen(Qt::white);
    painter.drawRoundRect(QRect(0, 0, this->width(), this->height()));
}

void ProfileBox::changeColor(QColor font, QColor back, QWidget *widget) {
    widget->setAutoFillBackground(true);
    QPalette pallete = widget->palette();
    pallete.setColor(widget->foregroundRole(), font);
    pallete.setColor(widget->backgroundRole(), back);
    widget->setPalette(pallete);
}

void ProfileBox::setButtonStyleSheet(QPushButton *button) {
    button->setStyleSheet("background-color:" + COLOR_BUTTONBACKGROUND + ";"
                          "border-style:" + STYLE_BUTTONBORDER + ";"
                          "border-width:" + WIDTH_BUTTONBORDER + "px;"
                          "border-radius:" + RADIUS_BUTTONBORDER + "px;"
                          "border-color:" + COLOR_BUTTONBORDER + ";"
                          "font:" + FONT_BUTTON + "px;"
                          "color:" + COLOR_BUTTONFONT + ";"
                          "min-width:" + WIDTH_BUTTON + "px;"
                          "padding:" + PADDING_BUTTON + "px;");
}
