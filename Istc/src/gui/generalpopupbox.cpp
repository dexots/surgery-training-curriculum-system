#include "gui/generalpopupbox.h"

const QString GeneralPopupBox::COLOR_BUTTONBACKGROUND = "rgb(39, 174, 96)";
const QString GeneralPopupBox::COLOR_BUTTONBORDER = "white";
const QString GeneralPopupBox::COLOR_BUTTONFONT = "white";

const QString GeneralPopupBox::FONT_BUTTON = "bold 14";
const QString GeneralPopupBox::PADDING_BUTTON = "6";
const QString GeneralPopupBox::RADIUS_BUTTONBORDER = "10";
const QString GeneralPopupBox::STYLE_BUTTONBORDER = "outset";

const QString GeneralPopupBox::WIDTH_BUTTON = "50";
const QString GeneralPopupBox::WIDTH_BUTTONBORDER = "2";

const QString GeneralPopupBox::STRING_CV_SUCCESS = "CV Successfully Generated at \n";
const QString GeneralPopupBox::STRING_FORM_SUCCESS = "Form Successfully Downloaded at \n";
const QString GeneralPopupBox::STRING_SETTINGS_SUCCESS = "Settings Successfully Saved";
const QString GeneralPopupBox::STRING_GENERAL_SUCCESS = "Success!";

const int GeneralPopupBox::WIDTH_MINIMUM = 400;
const int GeneralPopupBox::HEIGHT_MINIMUM = 200;

enum typeOfBox
{
    UNSET,
    CV,
    FORM,
    SETTINGS,
    COUNT
};

GeneralPopupBox::GeneralPopupBox(QString saveFileName, int type) {
    _bold.setBold(true);

    setSuccessLabel(saveFileName, type);

    _okButton = new QPushButton;
    _okButton->setText("OK");
    _okButton->setCursor(Qt::PointingHandCursor);
    GeneralPopupBox::setButtonStyleSheet(_okButton);
    connect(_okButton, SIGNAL(clicked()),
            this, SLOT(handleButton()));
    _okButton->setSizePolicy(QSizePolicy::Fixed,
                             QSizePolicy::Fixed);

    _buttonLayout = new QHBoxLayout();
    _buttonLayout->addWidget(_okButton, Qt::AlignHCenter);

    _mainLayout = new QVBoxLayout;
    _mainLayout->addWidget(_successLabel);
    _mainLayout->addLayout(_buttonLayout);

    this->setLayout(_mainLayout);
    this->setContentsMargins(10, 10, 10, 10);
    this->setMinimumWidth(WIDTH_MINIMUM);
    this->setMinimumHeight(HEIGHT_MINIMUM);

    this->setAttribute(Qt::WA_TranslucentBackground, true);

    this->setWindowFlags(Qt::FramelessWindowHint);
}

GeneralPopupBox::~GeneralPopupBox() {
    delete _successLabel;
    delete _okButton;
    delete _mainLayout;
}

void GeneralPopupBox::setSuccessLabel(QString saveFileName, int type) {
    _successLabel = new QLabel;
    QString successString;
    switch (type) {
        case 1: {
            successString = STRING_CV_SUCCESS;
            _successLabel->setText(successString.append(saveFileName));
            break;
        }
        case 2: {
            successString = STRING_FORM_SUCCESS;
            _successLabel->setText(successString.append(saveFileName));
            break;
        }
        case 3: {
            _successLabel->setText(STRING_SETTINGS_SUCCESS);
            break;
        }
        default: {
            _successLabel->setText(STRING_GENERAL_SUCCESS);
            break;
        }
    }

    _successLabel->setFont(_bold);

}

void GeneralPopupBox::handleButton() {
    QDialog::done(0);
}

void GeneralPopupBox::paintEvent(QPaintEvent *event) {
    QDialog::paintEvent(event);
    QPainter painter(this);
    QBrush brush(Qt::white);
    painter.setBrush(brush);
    painter.setPen(Qt::white);
    painter.drawRoundRect(QRect(0, 0, this->width(), this->height()));
}

void GeneralPopupBox::changeColor(QColor font, QColor back, QWidget *widget) {
    widget->setAutoFillBackground(true);
    QPalette pallete = widget->palette();
    pallete.setColor(widget->foregroundRole(), font);
    pallete.setColor(widget->backgroundRole(), back);
    widget->setPalette(pallete);
}

void GeneralPopupBox::setButtonStyleSheet(QPushButton *button) {
    button->setStyleSheet("background-color:" + COLOR_BUTTONBACKGROUND + ";"
                          "border-style:" + STYLE_BUTTONBORDER + ";"
                          "border-width:" + WIDTH_BUTTONBORDER + "px;"
                          "border-radius:" + RADIUS_BUTTONBORDER + "px;"
                          "border-color:" + COLOR_BUTTONBORDER + ";"
                          "font:" + FONT_BUTTON + "px;"
                          "color:" + COLOR_BUTTONFONT + ";"
                          "min-width:" + WIDTH_BUTTON + "px;"
                          "padding:" + PADDING_BUTTON + "px;");
}
