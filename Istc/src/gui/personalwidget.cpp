#include "gui/personalwidget.h"

const QString PersonalWidget::ICON_ADDRESS = ":/icon/ButtonIcon/address.png";
const QString PersonalWidget::ICON_EDIT = ":/icon/ButtonIcon/edit.png";
const QString PersonalWidget::ICON_HOMECONTACT = ":/icon/ButtonIcon/phone.png";
const QString PersonalWidget::ICON_MOBILECONTACT = ":/icon/ButtonIcon/mobile.png";
const QString PersonalWidget::ICON_PERSONALEMAIL = ":/icon/ButtonIcon/Personal Email.png";
const QString PersonalWidget::ICON_SCHOOLEMAIL = ":/icon/ButtonIcon/schoolEmail.png";

const QString PersonalWidget::PICTURE_PROFILE = ":/icon/ButtonIcon/penguin.png";
const QString PersonalWidget::PROFILE_FILE = "userprofile.txt";

const QString PersonalWidget::LEFTPADDING_LABEL = "7";
const QString PersonalWidget::SIZE_FONT = "15";

const QString PersonalWidget::TAG_BOLD = "<b>";
const QString PersonalWidget::TAG_CURRENTROTATION = "Current Rotation:";

const string PersonalWidget::STRING_EMPTY = "";
const QString PersonalWidget::QSTRING_EMPTY = "";

PersonalWidget::PersonalWidget() {
    setUpPersonalLayout();
    readSettingsFile();
    setPersonalInfo();
    setUpUpperLayout();
    setUpLowerLayout();
    setLayout(_personalLayout);
}

PersonalWidget::~PersonalWidget() {
    delete _personalLayout;
}

void PersonalWidget::initialiseVariables() {
    _userName = QSTRING_EMPTY;
    _userStudentID = QSTRING_EMPTY;
    _userAddress = QSTRING_EMPTY;
    _userHomeContact = QSTRING_EMPTY;
    _userMobileContact = QSTRING_EMPTY;
    _userSchoolEmail = QSTRING_EMPTY;
    _userPersonalEmail = QSTRING_EMPTY;
    _userCurrentRotation = QSTRING_EMPTY;

}

void PersonalWidget::setUpPersonalLayout() {
    _personalLayout = new QVBoxLayout();
    _personalLayout->setAlignment(Qt::AlignTop);
}

void PersonalWidget::setPersonalInfo() {
    setUserName(_userName);
    setUserCurrentRotation(_userCurrentRotation);
    setUserStudentID(_userStudentID);
    setUserAddress(_userAddress);
    setUserHomeContact(_userHomeContact);
    setUserMobileContact(_userMobileContact);
    setUserSchoolEmail(_userSchoolEmail);
    setUserPersonalEmail(_userPersonalEmail);
}

void PersonalWidget::setUserName(QString userName) {
    _userName = userName;
}

void PersonalWidget::setUserCurrentRotation(QString userCurrentRotation) {
    _userCurrentRotation = userCurrentRotation;
}

void PersonalWidget::setUserStudentID(QString userStudentID) {
    _userStudentID = userStudentID;
}

void PersonalWidget::setUserAddress(QString userAddress) {
    _userAddress = userAddress;
}

void PersonalWidget::setUserHomeContact(QString userHomeContact) {
    _userHomeContact = userHomeContact;
}

void PersonalWidget::setUserMobileContact(QString userMobileContact) {
    _userMobileContact = userMobileContact;
}

void PersonalWidget::setUserSchoolEmail(QString userSchoolEmail) {
    _userSchoolEmail = userSchoolEmail;
}

void PersonalWidget::setUserPersonalEmail(QString userPersonalEmail) {
    _userPersonalEmail = userPersonalEmail;
}

void PersonalWidget::setUpUpperLayout() {
    QHBoxLayout *upperLayout = new QHBoxLayout();
    upperLayout->setAlignment(Qt::AlignLeft);

    _userProfilePicture = QPixmap(PICTURE_PROFILE);
    QLabel *profilePicture = new QLabel();
    profilePicture->setPixmap(_userProfilePicture);
    upperLayout->addWidget(profilePicture);

    QVBoxLayout *upperRightLayout = new QVBoxLayout();
    QLabel *userName = new QLabel(TAG_BOLD + _userName);
    upperRightLayout->addWidget(userName);

    QLabel *userStudentID = new QLabel(_userStudentID);
    upperRightLayout->addWidget(userStudentID);

    QLabel* newlineLabel = new QLabel("");
    upperRightLayout->addWidget(newlineLabel);

    QLabel *rotationTag = new QLabel(TAG_BOLD + TAG_CURRENTROTATION);
    upperRightLayout->addWidget(rotationTag);

    QLabel *userCurrentRotation = new QLabel(_userCurrentRotation);
    upperRightLayout->addWidget(userCurrentRotation);

    QFrame *upperRightLayoutFrame = new QFrame();
    upperRightLayoutFrame->setLayout(upperRightLayout);

    upperLayout->addWidget(upperRightLayoutFrame);

    QFrame *upperLayoutFrame = new QFrame();
    upperLayoutFrame->setLayout(upperLayout);

    _personalLayout->addWidget(upperLayoutFrame);
}

void PersonalWidget::setUpLowerLayout() {
    setUpPersonalEmail();
    setUpSchoolEmail();
    setUpUserAddress();
    setUpHomeContact();
    setUpMobileContact();

    this->setStyleSheet("QLabel{font:" + SIZE_FONT + "px;}");
}

void PersonalWidget::setUpPersonalEmail() {
    QHBoxLayout *personalEmailLayout = new QHBoxLayout();
    personalEmailLayout->setAlignment(Qt::AlignLeft);

    QLabel *personalEmailIcon = new QLabel();
    personalEmailIcon->setPixmap(ICON_PERSONALEMAIL);
    personalEmailLayout->addWidget(personalEmailIcon);

    _personalEmailLbl = new QLabel(_userPersonalEmail);
    personalEmailLayout->addWidget(_personalEmailLbl);

    QLabel *editIcon = new QLabel();
    editIcon->setPixmap(ICON_EDIT);
    ClickableWidget *edit = new ClickableWidget(editIcon);
    personalEmailLayout->addWidget(edit);
    connect(edit, SIGNAL(isClicked()),
            this, SLOT(editPersonalEmail()));

    QFrame *personalEmailFrame = new QFrame();
    personalEmailFrame->setLayout(personalEmailLayout);
    _personalLayout->addWidget(personalEmailFrame);
}

void PersonalWidget::setUpSchoolEmail() {
    QHBoxLayout *schoolEmailLayout = new QHBoxLayout();
    schoolEmailLayout->setAlignment(Qt::AlignLeft);

    QLabel *schoolEmailIcon = new QLabel();
    schoolEmailIcon->setPixmap(ICON_SCHOOLEMAIL);
    schoolEmailLayout->addWidget(schoolEmailIcon);

    _schoolEmailLbl = new QLabel(_userSchoolEmail);
    schoolEmailLayout->addWidget(_schoolEmailLbl);

    QLabel *editIcon = new QLabel();
    editIcon->setPixmap(ICON_EDIT);
    ClickableWidget *edit = new ClickableWidget(editIcon);
    schoolEmailLayout->addWidget(edit);
    connect(edit, SIGNAL(isClicked()),
            this, SLOT(editSchoolEmail()));

    QFrame *schoolEmailFrame = new QFrame();
    schoolEmailFrame->setLayout(schoolEmailLayout);
    _personalLayout->addWidget(schoolEmailFrame);
}

void PersonalWidget::setUpUserAddress() {
    QHBoxLayout *userAddressLayout = new QHBoxLayout();
    userAddressLayout->setAlignment(Qt::AlignLeft);

    QLabel *userAddressIcon = new QLabel();
    userAddressIcon->setPixmap(ICON_ADDRESS);
    userAddressIcon->setStyleSheet("padding-left:" + LEFTPADDING_LABEL + "px;");
    userAddressLayout->addWidget(userAddressIcon);

    _userAddLbl = new QLabel(_userAddress);
    _userAddLbl->setStyleSheet("padding-left:" + LEFTPADDING_LABEL + "px;");
    userAddressLayout->addWidget(_userAddLbl);

    QLabel *editIcon = new QLabel();
    editIcon->setPixmap(ICON_EDIT);
    ClickableWidget *edit = new ClickableWidget(editIcon);
    userAddressLayout->addWidget(edit);
    connect(edit, SIGNAL(isClicked()),
            this, SLOT(editUserAddress()));

    QFrame *userAddressFrame = new QFrame();
    userAddressFrame->setLayout(userAddressLayout);
    _personalLayout->addWidget(userAddressFrame);
}

void PersonalWidget::setUpHomeContact() {
    QHBoxLayout *homeContactLayout = new QHBoxLayout();
    homeContactLayout->setAlignment(Qt::AlignLeft);

    QLabel *homeContactIcon = new QLabel();
    homeContactIcon->setPixmap(ICON_HOMECONTACT);
    homeContactLayout->addWidget(homeContactIcon);

    _homeContactLbl = new QLabel(_userHomeContact);
    homeContactLayout->addWidget(_homeContactLbl);

    QLabel *editIcon = new QLabel();
    editIcon->setPixmap(ICON_EDIT);
    ClickableWidget *edit = new ClickableWidget(editIcon);
    homeContactLayout->addWidget(edit);
    connect(edit, SIGNAL(isClicked()),
            this, SLOT(editHomeContact()));

    QFrame *homeContactFrame = new QFrame();
    homeContactFrame->setLayout(homeContactLayout);
    _personalLayout->addWidget(homeContactFrame);
}

void PersonalWidget::setUpMobileContact() {
    QHBoxLayout *mobileContactLayout = new QHBoxLayout();
    mobileContactLayout->setAlignment(Qt::AlignLeft);

    QLabel *mobileContactIcon = new QLabel();
    mobileContactIcon->setStyleSheet("padding-left:" + LEFTPADDING_LABEL + "px;");
    mobileContactIcon->setPixmap(ICON_MOBILECONTACT);
    mobileContactLayout->addWidget(mobileContactIcon);

    _mobileContactLbl = new QLabel(_userMobileContact);
    _mobileContactLbl->setStyleSheet("padding-left:" + LEFTPADDING_LABEL + "px;");
    mobileContactLayout->addWidget(_mobileContactLbl);

    QLabel *editIcon = new QLabel();
    editIcon->setPixmap(ICON_EDIT);
    ClickableWidget *edit = new ClickableWidget(editIcon);
    mobileContactLayout->addWidget(edit);
    connect(edit, SIGNAL(isClicked()),
            this, SLOT(editMobileContact()));

    QFrame *mobileContactFrame = new QFrame();
    mobileContactFrame->setLayout(mobileContactLayout);
    _personalLayout->addWidget(mobileContactFrame);
}

void PersonalWidget::readSettingsFile() {
    QFile file(PROFILE_FILE);
    if (!file.open(QFile::ReadOnly | QFile::Text)) {
        //showErrorBox();
        return;
    }
    QTextStream in(&file);
    QString data = in.readAll();
    populateData(data);
    file.close();
}

void PersonalWidget::populateData(QString data) {
    QStringList split = data.split("\n");
    for (int i = 0; i < split.size(); i++) {
        if (split[i].trimmed().isEmpty()) {
            continue;
        }

        QString line = split.value(i);
        QStringList lineTokens = line.split(":=");

        QString type = lineTokens.value(0);
        type = type.trimmed();
        QString value = lineTokens.value(1);
        value = value.trimmed();

        if (type == "name") {
            _userName = value;
        } else if (type == "rotation") {
            _userCurrentRotation = value;
        } else if (type == "matricNo") {
            _userStudentID = value;
        } else if (type == "address") {
            _userAddress = value;
        } else if (type == "personalContact") {
            _userMobileContact = value;
        } else if (type == "homeContact") {
            _userHomeContact = value;
        } else if (type == "schoolEmail") {
            _userSchoolEmail = value;
        } else if (type == "personalEmail") {
            _userPersonalEmail = value;
        } else {
            return;
        }
    }
}

void PersonalWidget::saveSettingsFile() {
    QFile file(PROFILE_FILE);
    if (!file.open(QFile::WriteOnly | QFile::Text)) {
        showErrorBox();
        return;
    }

    QTextStream out(&file);
    QString data = formatData();
    out << data;
    file.close();
    showSuccessMsg();
}

QString PersonalWidget::formatData() {
    QString dataString = _userName + "\n" + _userCurrentRotation + "\n" +
                        _userStudentID + "\n" + _userAddress + "\n" +
                        _userHomeContact + "\n" + _userMobileContact + "\n" +
                        _userSchoolEmail + "\n" + _userPersonalEmail;

    return dataString;
}

void PersonalWidget::showSuccessMsg() {
    GeneralPopupBox *successBox = new GeneralPopupBox("", 3);
    successBox->exec();
}

void PersonalWidget::showErrorBox() {
    ErrorBox *errorBox = new ErrorBox("SETTINGS");
    errorBox->exec();
}

/*
 * SLOTS
 */

void PersonalWidget::editPersonalEmail() {
    _profileBox = new ProfileBox("Personal Email", _userPersonalEmail);
    connect(_profileBox, SIGNAL(passInfo(QString, QString)),
            this, SLOT(haveInfo(QString, QString)));
    _profileBox->exec();
}

void PersonalWidget::editSchoolEmail() {
    _profileBox = new ProfileBox("School Email", _userSchoolEmail);
    connect(_profileBox, SIGNAL(passInfo(QString, QString)),
            this, SLOT(haveInfo(QString, QString)));
    _profileBox->exec();
}

void PersonalWidget::editUserAddress() {
    _profileBox = new ProfileBox("User Address", _userAddress);
    connect(_profileBox, SIGNAL(passInfo(QString, QString)),
            this, SLOT(haveInfo(QString, QString)));
    _profileBox->exec();
}

void PersonalWidget::editHomeContact() {
    _profileBox = new ProfileBox("Home Contact", _userHomeContact);
    connect(_profileBox, SIGNAL(passInfo(QString, QString)),
            this, SLOT(haveInfo(QString, QString)));
    _profileBox->exec();
}

void PersonalWidget::editMobileContact() {
    _profileBox = new ProfileBox("Mobile Contact", _userMobileContact);
    connect(_profileBox, SIGNAL(passInfo(QString, QString)),
            this, SLOT(haveInfo(QString, QString)));
    _profileBox->exec();
}

void PersonalWidget::haveInfo(QString type, QString info) {
    updateInfo(type, info);
    saveSettingsFile();
}

void PersonalWidget::updateInfo(QString type, QString info) {
    if (type == "Personal Email") {
        setUserPersonalEmail(info);
        _personalEmailLbl->setText(info);
    } else if (type == "School Email") {
        setUserSchoolEmail(info);
        _schoolEmailLbl->setText(info);
    } else if (type == "User Address") {
        setUserAddress(info);
        _userAddLbl->setText(info);
    } else if (type == "Home Contact") {
        setUserHomeContact(info);
        _homeContactLbl->setText(info);
    } else if (type == "Mobile Contact") {
        setUserMobileContact(info);
        _mobileContactLbl->setText(info);
    } else {
        return;
    }

}
