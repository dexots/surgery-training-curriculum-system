#include "gui/loginui.h"

const char *LoginUi::ICON_LOGO = ":/icon/ButtonIcon/ISTC.png";
const char *LoginUi::BUTTON_OPENID_NAME = "Login With NUS Open Id";
const char *LoginUi::BUTTON_WITHOUT_OPENID_NAME = "Login";

const char *LoginUi::FONT_NAME = "Segoe WP";
const int LoginUi::FONT_BUTTON_SIZE = 14;
const QColor LoginUi::GREEN = QColor(39, 174, 96, 255);

const QString LoginUi::ROSTER_MONSTER = "I'm a roster monster!";
const QString LoginUi::STRING_EMPTY = "";

LoginUi::LoginUi() {
    LoginUi::initializeVariables();
    LoginUi::initializeClass();
    LoginUi::createView();
}

LoginUi::~LoginUi() {
    LoginUi::clearGarbage();
}

//public method
void LoginUi::showErrorMessage(QString errorMessage) {
    qDebug() << "LoginUi -> showErrorMessage: "
                "showing error message...";

    _errorMessage->setText(errorMessage);
    _errorMessage->show();

    qDebug() << "LoginUi -> showErrorMessage: "
                "error message is shown...";
}


//private methods
void LoginUi::initializeVariables() {
    _role = STRING_EMPTY;
    _matricNo = STRING_EMPTY;
    _fullname = STRING_EMPTY;
    _email = STRING_EMPTY;
    _isRosterMonster = false;
    _urlParser = new Login;
}

void LoginUi::initializeClass() {    
    qDebug() << "LoginUi -> intializeClass: "
                "start initializing class...";

    _layout = new QVBoxLayout;

    initializeLogo();
    initializeMatricNoInput();
    initializeFullnameInput();
    initializeEmailInput();
    initializeRotationInput();
    initializeRole();
    initializeButton();
    initializeErrorMsg();
    initializeLoginBrowser();
    colorButton(Qt::white, this);

    qDebug() << "LoginUi -> intializeClass: "
                "done initializing class...";
}

void LoginUi::initializeLogo() {
    _appLogo = new QLabel();
    _appLogo->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    QPixmap appPixmap = QPixmap(QString::fromUtf8(ICON_LOGO));
    _appLogo->setPixmap(appPixmap);

    _appLogo->setFixedHeight(_appLogo->pixmap()->height());
    _appLogo->setFixedWidth(_appLogo->pixmap()->width());
}

void LoginUi::initializeMatricNoInput() {
    _matricNoInput = new QLineEdit;
    _matricNoInput->setPlaceholderText("Matric Number");
    _matricNoInput->setMinimumWidth(350);
}

void LoginUi::initializeFullnameInput() {
    _fullnameInput = new QLineEdit;
    _fullnameInput->setPlaceholderText("Name");
    _fullnameInput->setMinimumWidth(350);
}

void LoginUi::initializeEmailInput() {
    _emailInput = new QLineEdit;
    _emailInput->setPlaceholderText("NUH Email");
    _emailInput->setMinimumWidth(350);
}

void LoginUi::initializeRotationInput() {
    _rotationInput = new QLineEdit;
    _rotationInput->setPlaceholderText("Rotation");
    _rotationInput->setMinimumWidth(350);
}

void LoginUi::initializeRole() {
    _roleDropDown = new QComboBox();
    _roleDropDown->addItem(UserDetails::ROLE_TRAINEE);
    _roleDropDown->addItem(UserDetails::ROLE_TRAINER);
    _roleDropDown->addItem(UserDetails::ROLE_ADMIN);
    _roleDropDown->setMinimumWidth(350);

    _roleDropDown->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

    _rosterMonsterCheck = new QCheckBox(ROSTER_MONSTER, this);
    _rosterMonsterCheck->setChecked(_isRosterMonster);

    _roleLayout = new QVBoxLayout;
    _roleLayout->addWidget(_roleDropDown, Qt::AlignCenter);
    _roleLayout->addWidget(_rosterMonsterCheck, Qt::AlignCenter);

    connect(_rosterMonsterCheck, SIGNAL(stateChanged(int)),
            this, SLOT(handleRosterMonster()));

}

void LoginUi::initializeButton() {
    QFont displayFont(FONT_NAME, FONT_BUTTON_SIZE);

    QLabel *loginButton = new QLabel;
    loginButton->setText(BUTTON_WITHOUT_OPENID_NAME);
    loginButton->setFont(displayFont);
    loginButton->setAlignment(Qt::AlignCenter);
    loginButton->setMinimumHeight(50);
    LoginUi::colorButton(GREEN, loginButton);

    _loginButton = new ClickableWidget(loginButton);
    _loginButton->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    _loginButton->setFixedWidth(_appLogo->pixmap()->width());

    QLabel *loginOpenIdButton = new QLabel;
    loginOpenIdButton->setText(BUTTON_OPENID_NAME);
    loginOpenIdButton->setFont(displayFont);
    loginOpenIdButton->setAlignment(Qt::AlignCenter);
    loginOpenIdButton->setMinimumHeight(50);
    LoginUi::colorButton(GREEN, loginOpenIdButton);

    _loginOpenIdButton = new ClickableWidget(loginOpenIdButton);
    _loginOpenIdButton->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    _loginOpenIdButton->setFixedWidth(_appLogo->pixmap()->width());

}

void LoginUi::initializeErrorMsg() {
    _errorMessage = new QLabel();
    changeFontColor(Qt::red, _errorMessage);
    _errorMessage->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
}


//temporarily include method:
void LoginUi::initializeLoginBrowser() {
    _loginBrowser = new LoginBrowser;

    connect (_loginBrowser, SIGNAL(showWidget()),
             this, SLOT(handleShowWidget()));
    connect (_loginBrowser, SIGNAL(urlChanged(QUrl)),
             this, SLOT(handleUrlChanged(QUrl)));
}

void LoginUi::clearGarbage() {
    delete _loginButton;
    delete _loginOpenIdButton;
    delete _matricNoInput;
    delete _fullnameInput;
    delete _emailInput;
    delete _rotationInput;
    delete _roleDropDown;
    delete _rosterMonsterCheck;
    delete _roleLayout;
    delete _appLogo;
    delete _errorMessage;
    delete _layout;
}

void LoginUi::createView() {
    _layout->addWidget(_appLogo);
    _layout->addWidget(_fullnameInput);
    _layout->addWidget(_matricNoInput);
    _layout->addWidget(_emailInput);
    _layout->addWidget(_rotationInput);
    _layout->addLayout(_roleLayout);
    _layout->addWidget(_loginButton);
    _layout->addWidget(_loginOpenIdButton);
    _layout->addWidget(_errorMessage);

    _layout->setAlignment(_fullnameInput, Qt::AlignHCenter);
    _layout->setAlignment(_matricNoInput, Qt::AlignHCenter);
    _layout->setAlignment(_emailInput, Qt::AlignHCenter);
    _layout->setAlignment(_rotationInput, Qt::AlignHCenter);
    _layout->setAlignment(_roleLayout, Qt::AlignHCenter);
    _layout->setAlignment(_appLogo, Qt::AlignHCenter);
    _layout->setAlignment(_loginButton, Qt::AlignHCenter);
    _layout->setAlignment(_loginOpenIdButton, Qt::AlignHCenter);
    _layout->setAlignment(_errorMessage, Qt::AlignHCenter);

    _layout->addWidget(_loginBrowser);
    _loginBrowser->hide();

    this->setLayout(_layout);
    connect(_loginButton, SIGNAL(isClicked()),
            this, SLOT(handleNotOpenIdButton()));
    connect(_loginOpenIdButton, SIGNAL(isClicked()),
            this, SLOT(handleOpenIdButton()));
}

void LoginUi::colorButton(QColor background, QWidget *widget){
    widget->setAutoFillBackground(true);
    QPalette pallete = widget->palette();
    pallete.setColor(widget->backgroundRole(), background);
    widget->setPalette(pallete);
}

void LoginUi::changeFontColor(QColor font, QWidget *widget){
    widget->setAutoFillBackground(true);
    QPalette pallete = widget->palette();
    pallete.setColor(widget->foregroundRole(), font);
    widget->setPalette(pallete);
}

void LoginUi::reinitiateUi(){
    _matricNoInput->setText(STRING_EMPTY);
    _fullnameInput->setText(STRING_EMPTY);
    _emailInput->setText(STRING_EMPTY);
    _errorMessage->setText(STRING_EMPTY);
    _errorMessage->hide();
}

//created for open id login
void LoginUi::backToLoginUi()
{
    _loginBrowser->hide();

    _appLogo->show();
    _fullnameInput->show();
    _matricNoInput->show();
    _emailInput->show();
    _rotationInput->show();
    _roleDropDown->show();
    _rosterMonsterCheck->show();
    _loginButton->show();
}

void LoginUi::passInfoToMainWindow() {


    UserDetails* userDetails = UserDetails::getObject();

    userDetails->setMatricNo(_matricNo);
    userDetails->setUserName(_fullname);
    userDetails->setUserEmail(_email);
    userDetails->setRotation(_rotation);
    userDetails->setUserRole(_role);
    userDetails->setIsRosterMonster(_isRosterMonster);

    emit this->loginSuccess();
}

bool LoginUi::checkDataCompleteness() {
    _matricNo = _matricNoInput->text().trimmed();
    _fullname = _fullnameInput->text().trimmed();
    _email = _emailInput->text().trimmed();
    _rotation = _rotationInput->text().trimmed();
    _role = _roleDropDown->currentText().trimmed();


    QString errorMsg = "";
    if (_matricNo.isEmpty()) {
        errorMsg += "Please key in your matric number\n";

        qDebug() << "LoginUi -> checkDataCompleteness: "
                 << errorMsg;

        showErrorMessage(errorMsg);
        return false;
    } else if (_fullname.isEmpty()) {
        errorMsg += "Please key in your name\n";

        qDebug() << "LoginUi -> checkDataCompleteness: "
                 << errorMsg;

        showErrorMessage(errorMsg);
        return false;
    } else if (_email.isEmpty()) {
        errorMsg += "Please key in your nuh email\n";

        qDebug() << "LoginUi -> checkDataCompleteness: "
                 << errorMsg;

        showErrorMessage(errorMsg);
        return false;
    } else if (_role.isEmpty()) {
        //something is fishy if we reach here because
        //the _role is inside the dropdownlist and it should be
        //allocated before the code executes here

        errorMsg = "Please select your role";

        qDebug() << "LoginUi -> checkDataCompleteness: "
                 << errorMsg;

        showErrorMessage(errorMsg);
        return false;
    } else if (_rotation.isEmpty()) {
        errorMsg += "Please key in your rotation. eg: Team Infinity\n";

        qDebug() << "LoginUi -> checkDataCompleteness: "
                 << errorMsg;

        showErrorMessage(errorMsg);
        return false;
    }

    qDebug() << "LoginUi -> checkDataCompleteness: "
                "no errors!";

    return true;
}


void LoginUi::hideDataFields() {
    _appLogo->hide();
    _fullnameInput->hide();
    _matricNoInput->hide();
    _emailInput->hide();
    _rotationInput->hide();
    _roleDropDown->hide();
    _rosterMonsterCheck->hide();
    _loginButton->hide();
    _loginOpenIdButton->hide();
    _errorMessage->hide();
}

//private slots

void LoginUi::handleOpenIdButton(){
    qDebug() << "LoginUi -> handleOpenIdButton: "
                "start handling open id button...";

    if (!checkDataCompleteness()) {
        return;
    }

    hideDataFields();

    _loginBrowser->show();

    QUrl nusOpenIdSite = _urlParser->getNusOpenIdUrl();
    _loginBrowser->load(nusOpenIdSite);

    qDebug() << "start loading... " << nusOpenIdSite;

    qDebug() << "LoginUi -> handleOpenIdButton: "
                "done handling open id button...";
}

void LoginUi::handleNotOpenIdButton() {
    qDebug() << "LoginUi -> handleNotOpenIdButton: "
                "start handling not open id button...";

    if (!checkDataCompleteness()) {
        return;
    }

    hideDataFields();
    passInfoToMainWindow();

    qDebug() << "LoginUi -> handleNotOpenIdButton: "
                "done handling not open id button...";
}


void LoginUi::handleRosterMonster() {
    if (_rosterMonsterCheck->isChecked()) {
        _isRosterMonster = true;
    } else if (!_rosterMonsterCheck->isChecked()) {
        _isRosterMonster = false;
    } else {
        assert (false);
    }
}

//creted for nus open id
void LoginUi::handleShowWidget() {
    backToLoginUi();
    showErrorMessage(Login::LOGIN_CANCEL);
}


void LoginUi::handleUrlChanged(QUrl url) {
    QString urlString = url.toDisplayString(QUrl::PrettyDecoded);
    urlString = urlString.trimmed().toLower();
    if (urlString.startsWith("https://bluebell.d1.comp.nus.edu.sg/~surgery")) {
        QString loginStatus = _urlParser->checkLoginStatus(urlString);
        if (loginStatus == Login::LOGIN_FAIL ||
            loginStatus == Login::LOGIN_CANCEL) {
            backToLoginUi();
            showErrorMessage(loginStatus);
            return;
        } else if (loginStatus == Login::LOGIN_SUCCESS) {
            passInfoToMainWindow();
        } else {
            backToLoginUi();
            showErrorMessage("unknown error");
            return;
        }
    }


}








