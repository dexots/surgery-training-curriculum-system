#include "gui/activitybox.h"

const QString ActivityBox::COLOR_BUTTONBACKGROUND = "rgb(39, 174, 96)";
const QString ActivityBox::COLOR_BUTTONBORDER = "white";
const QString ActivityBox::COLOR_BUTTONFONT = "white";

const QString ActivityBox::FORMAT_DATE = "dd MMM yyyy";
const QString ActivityBox::FORMAT_DATETIME = "dd MMM yyyy h:m AP";
const QString ActivityBox::FORMAT_TIME = "h:m";
const QString ActivityBox::FONT_BUTTON = "bold 14";

const QString ActivityBox::LABEL_ACTIVITYDETAILS = "Activity Details: ";
const QString ActivityBox::LABEL_ADDBUTTON = "Add";
const QString ActivityBox::LABEL_CANCELBUTTON = "Cancel";
const QString ActivityBox::LABEL_DATE = "Date: ";
const QString ActivityBox::LABEL_DESCRIPTION = "Description: ";
const QString ActivityBox::LABEL_ENDTIME = "End Time: ";
const QString ActivityBox::LABEL_EVENTLINK = "Event Link: ";
const QString ActivityBox::LABEL_IMPORTANT = "Important";
const QString ActivityBox::LABEL_LOCATION = "Location: ";
const QString ActivityBox::LABEL_MAXVACANCY = "Max Vacancy: ";
const QString ActivityBox::LABEL_REGISTRATIONDEADLINE = "Registration Deadline: ";
const QString ActivityBox::LABEL_STARTTIME = "Start Time: ";
const QString ActivityBox::LABEL_TITLE = "Title: ";
const QString ActivityBox::LABEL_TRAINER = "Trainer: ";
const QString ActivityBox::LABEL_UPDATEBUTTON = "Update";

const QString ActivityBox::MESSAGE_DEADLINELOGICERROR = "Activity date should be later than registration deadline";
const QString ActivityBox::MESSAGE_INVALIDEVENTLINK = "Please key in valid event link";
const QString ActivityBox::MESSAGE_MISSINGDESCRIPTION = "Please key in activity description";
const QString ActivityBox::MESSAGE_MISSINGLOCATION = "Please key in activity location";
const QString ActivityBox::MESSAGE_MISSINGMAXVACANCY = "Please key in activity maximum vacancy";
const QString ActivityBox::MESSAGE_MISSINGTITLE = "Please key in activity title";
const QString ActivityBox::MESSAGE_MISSINGTRAINER = "Please key in activity trainer";
const QString ActivityBox::MESSAGE_TIMELOGICERROR = "End time should be later than start time";

const QString ActivityBox::PADDING_BUTTON = "6";

const QString ActivityBox::PLACEHOLDER_DESCRIPTION = "Description about the Activity";
const QString ActivityBox::PLACEHOLDER_EVENTLINK = "Web link for event (optional)";
const QString ActivityBox::PLACEHOLDER_LOCATION = "Venue for activity";
const QString ActivityBox::PLACEHOLDER_MAXVACANCY = "Maximum vacancy for activity";
const QString ActivityBox::PLACEHOLDER_TITLE = "Title for the activity";
const QString ActivityBox::PLACEHOLDER_TRAINER = "Trainer NUSNET ID for the activity";

const QString ActivityBox::RADIUS_BUTTONBORDER = "10";
const QString ActivityBox::STYLE_BUTTONBORDER = "outset";

const QString ActivityBox::STRING_COLON = ":";
const QString ActivityBox::STRING_EMPTY = "";
const QString ActivityBox::STRING_NEWLINE = "<br>";
const QString ActivityBox::STRING_SPACE = " ";

const QString ActivityBox::WIDTH_BUTTON = "50";
const QString ActivityBox::WIDTH_BUTTONBORDER = "2";

const int ActivityBox::HEIGHT_NORMALINPUTBOX = 30;
const int ActivityBox::HEIGHT_LARGEINPUTBOX = 80;

const QTime ActivityBox::DEFAULTTIME_DEADLINE = QTime(23, 59);

//constructor
ActivityBox::ActivityBox() {
    initializeBoxLayout();
    setUpActivityDetailsLabel();
    setUpTitleAndTrainerInput();
    setUpDateAndRegistrationDeadlineInput();
    setUpStartAndEndTimeInput();
    setUpMaxVacancyAndLocationInput();
    setUpDescriptionInput();
    setUpEventLinkInput();
    setUpIsImportantCheckbox();
    setUpErrorLabel();
    setUpAddAndCancelButton();

    this->setLayout(_boxLayout);
    setActivityBoxAttribute();
}

ActivityBox::ActivityBox(QString title, QString trainer, QDate date,
                         QDateTime registrationDeadline, QTime startTime,
                         QTime endTime, int maxVacancy, QString location,
                         QString description, bool isImportant, QString eventLink) {
    initializeBoxLayout();
    setUpActivityDetailsLabel();
    setUpTitleAndTrainerInput(title, trainer);
    setUpDateAndRegistrationDeadlineInput(date, registrationDeadline);
    setUpStartAndEndTimeInput(startTime, endTime);
    setUpMaxVacancyAndLocationInput(maxVacancy, location);
    setUpDescriptionInput(description);
    setUpEventLinkInput(eventLink);
    setUpIsImportantCheckbox(isImportant);
    setUpErrorLabel();
    setUpUpdateAndCancelButton();

    this->setLayout(_boxLayout);
    setActivityBoxAttribute();
}

//deconstructor
ActivityBox::~ActivityBox() {
    delete _boxLayout;
    delete _descriptionInput;
    delete _locationInput;
    delete _maxVacancyInput;
    delete _titleInput;
    delete _trainerInput;
    delete _registrationDeadlineInput;
    delete _dateInput;
    delete _startTimeInput;
    delete _endTimeInput;
    delete _isImportantCheckBox;
    delete _errorLabel;
}

//protected
void ActivityBox::paintEvent(QPaintEvent *event) {
    QDialog::paintEvent(event);
    QPainter painter(this);
    QBrush brush(Qt::white);
    painter.setBrush(brush);
    painter.setPen(Qt::white);
    painter.drawRoundRect(QRect(0, 0, this->width(), this->height()));
}

//private
void ActivityBox::initializeBoxLayout() {
    _boxLayout = new QVBoxLayout();
}

void ActivityBox::setUpActivityDetailsLabel() {
    QLabel *activityDetailsLabel = new QLabel(LABEL_ACTIVITYDETAILS);

    QFont activityDetailsFont = QFont();
    activityDetailsFont.setUnderline(true);
    activityDetailsFont.setBold(true);

    activityDetailsLabel->setFont(activityDetailsFont);

    _boxLayout->addWidget(activityDetailsLabel);
}

void ActivityBox::setUpTitleAndTrainerInput(const QString &title, const QString &trainer) {
    _boldFont = QFont();
    _boldFont.setBold(true);

    //left
    QLabel *titleLabel = new QLabel(LABEL_TITLE);
    titleLabel->setFont(_boldFont);

    _titleInput = new InputBox();
    _titleInput->setText(title);
    _titleInput->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    _titleInput->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    _titleInput->setFrameStyle(QFrame::NoFrame);
    _titleInput->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
    _titleInput->setFixedHeight(HEIGHT_NORMALINPUTBOX);
    _titleInput->setPlaceholderText(PLACEHOLDER_TITLE);

    connect(_titleInput, SIGNAL(stringReady(QString)),
            this, SLOT(handleTitle(QString)));

    QHBoxLayout *titleLayout = new QHBoxLayout;
    titleLayout->addWidget(titleLabel);
    titleLayout->addWidget(_titleInput);

    QWidget *titleWidget = new QWidget();
    titleWidget->setLayout(titleLayout);

    //right
    QLabel *trainerLabel = new QLabel(LABEL_TRAINER);
    trainerLabel->setFont(_boldFont);

    _trainerInput = new InputBox();
    _trainerInput->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    _trainerInput->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    _trainerInput->setFrameStyle(QFrame::NoFrame);
    _trainerInput->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
    _trainerInput->setFixedHeight(HEIGHT_NORMALINPUTBOX);
    _trainerInput->setPlaceholderText(PLACEHOLDER_TRAINER);
    _trainerInput->setDisabled(true);


    if (trainer == STRING_EMPTY) {
        UserDetails *userDetails = UserDetails::getObject();
        _trainerInput->setText(userDetails->getUserName());
    } else {
        _trainerInput->setText(trainer);
    }

    connect(_trainerInput, SIGNAL(stringReady(QString)),
            this, SLOT(handleTrainer(QString)));

    QHBoxLayout *trainerLayout = new QHBoxLayout;
    trainerLayout->addWidget(trainerLabel);
    trainerLayout->addWidget(_trainerInput);

    QWidget *trainerWidget = new QWidget();
    trainerWidget->setLayout(trainerLayout);

    //combine both
    QHBoxLayout *rowTwoLayout = new QHBoxLayout();
    rowTwoLayout->addWidget(titleWidget, 0, Qt::AlignLeft);
    rowTwoLayout->addWidget(trainerWidget, 0, Qt::AlignLeft);

    addToBoxLayout(rowTwoLayout);
}

void ActivityBox::setUpDateAndRegistrationDeadlineInput(QDate activityDate, QDateTime registrationDeadline) {
    //left
    QLabel *dateLabel = new QLabel(LABEL_DATE);
    dateLabel->setFont(_boldFont);

    MyCalendarWidget *dateCalendar = new MyCalendarWidget();

    _dateInput = new QDateEdit(activityDate);
    _dateInput->setMinimumDate(QDate::currentDate());
    _dateInput->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
    _dateInput->setDisplayFormat(FORMAT_DATE);
    _dateInput->setCalendarPopup(true);
    _dateInput->setCalendarWidget(dateCalendar);

    QHBoxLayout *dateLayout = new QHBoxLayout;
    dateLayout->addWidget(dateLabel);
    dateLayout->addWidget(_dateInput);

    QWidget *dateWidget = new QWidget();
    dateWidget->setLayout(dateLayout);

    //right
    QLabel *registrationDeadlineLabel = new QLabel(LABEL_REGISTRATIONDEADLINE);
    registrationDeadlineLabel->setFont(_boldFont);

    MyCalendarWidget *deadlineCalendar = new MyCalendarWidget();

    _registrationDeadlineInput = new QDateTimeEdit(registrationDeadline);
    _registrationDeadlineInput->setTime(DEFAULTTIME_DEADLINE);
    _registrationDeadlineInput->setMinimumDateTime(QDateTime::currentDateTime());
    _registrationDeadlineInput->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
    _registrationDeadlineInput->setDisplayFormat(FORMAT_DATETIME);
    _registrationDeadlineInput->setCalendarPopup(true);
    _registrationDeadlineInput->setCalendarWidget(deadlineCalendar);

    QHBoxLayout *registrationDeadlineLayout = new QHBoxLayout;
    registrationDeadlineLayout->addWidget(registrationDeadlineLabel);
    registrationDeadlineLayout->addWidget(_registrationDeadlineInput);

    QWidget *registrationDeadlineWidget = new QWidget();
    registrationDeadlineWidget->setLayout(registrationDeadlineLayout);

    //combine
    QHBoxLayout *rowThreeLayout = new QHBoxLayout();
    rowThreeLayout->addWidget(dateWidget, 0, Qt::AlignLeft);
    rowThreeLayout->addWidget(registrationDeadlineWidget, 0, Qt::AlignLeft);

    addToBoxLayout(rowThreeLayout);
}

void ActivityBox::setUpStartAndEndTimeInput(QTime startTime, QTime endTime) {
    //left
    QLabel *startTimeLabel = new QLabel(LABEL_STARTTIME);
    startTimeLabel->setFont(_boldFont);

    _startTimeInput = new QTimeEdit(startTime);
    _startTimeInput->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);

    QHBoxLayout *startTimeLayout = new QHBoxLayout;
    startTimeLayout->addWidget(startTimeLabel);
    startTimeLayout->addWidget(_startTimeInput);

    QWidget *startTimeWidget = new QWidget();
    startTimeWidget->setLayout(startTimeLayout);

    //right
    QLabel *endTimeLabel = new QLabel(LABEL_ENDTIME);
    endTimeLabel->setFont(_boldFont);

    _endTimeInput = new QTimeEdit(endTime);
    _endTimeInput->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);

    QHBoxLayout *endTimeLayout = new QHBoxLayout;
    endTimeLayout->addWidget(endTimeLabel);
    endTimeLayout->addWidget(_endTimeInput);

    QWidget *endTimeWidget = new QWidget();
    endTimeWidget->setLayout(endTimeLayout);

    //combine both
    QHBoxLayout *rowFourLayout = new QHBoxLayout();
    rowFourLayout->addWidget(startTimeWidget, 0, Qt::AlignLeft);
    rowFourLayout->addWidget(endTimeWidget, 0, Qt::AlignLeft);

    addToBoxLayout(rowFourLayout);
}

void ActivityBox::setUpMaxVacancyAndLocationInput(int maxVacancy, const QString &location) {
    //left
    QLabel *maxVacancyLabel = new QLabel(LABEL_MAXVACANCY);
    maxVacancyLabel->setFont(_boldFont);

    _maxVacancyInput = new InputBox();
    _maxVacancyInput->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    _maxVacancyInput->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    _maxVacancyInput->setFrameStyle(QFrame::NoFrame);
    _maxVacancyInput->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
    _maxVacancyInput->setFixedHeight(HEIGHT_NORMALINPUTBOX);
    _maxVacancyInput->setPlaceholderText(PLACEHOLDER_MAXVACANCY);

    if (maxVacancy != 0) {
        _maxVacancyInput->setText(QString::number(maxVacancy));
    }

    connect(_maxVacancyInput, SIGNAL(stringReady(QString)),
            this, SLOT(handleMaxVacancy(QString)));

    QHBoxLayout *maxVacancyLayout = new QHBoxLayout;
    maxVacancyLayout->addWidget(maxVacancyLabel);
    maxVacancyLayout->addWidget(_maxVacancyInput);

    QWidget *maxVacancyWidget = new QWidget();
    maxVacancyWidget->setLayout(maxVacancyLayout);

    //right
    QLabel *locationLabel = new QLabel(LABEL_LOCATION);
    locationLabel->setFont(_boldFont);

    _locationInput = new InputBox();
    _locationInput->setText(location);
    _locationInput->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    _locationInput->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    _locationInput->setFrameStyle(QFrame::NoFrame);
    _locationInput->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
    _locationInput->setFixedHeight(HEIGHT_NORMALINPUTBOX);
    _locationInput->setPlaceholderText(PLACEHOLDER_LOCATION);

    connect(_locationInput, SIGNAL(stringReady(QString)),
            this, SLOT(handleLocation(QString)));

    QHBoxLayout *locationLayout = new QHBoxLayout;
    locationLayout->addWidget(locationLabel);
    locationLayout->addWidget(_locationInput);

    QWidget *locationWidget = new QWidget();
    locationWidget->setLayout(locationLayout);

    //combine both
    QHBoxLayout *rowFiveLayout = new QHBoxLayout();
    rowFiveLayout->addWidget(maxVacancyWidget, 0, Qt::AlignLeft);
    rowFiveLayout->addWidget(locationWidget, 0, Qt::AlignLeft);

    addToBoxLayout(rowFiveLayout);
}

void ActivityBox::setUpDescriptionInput(const QString &description) {
    QLabel *descriptionLabel = new QLabel(LABEL_DESCRIPTION);
    descriptionLabel->setFont(_boldFont);

    _descriptionInput = new InputBox();
    _descriptionInput->setText(description);
    _descriptionInput->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    _descriptionInput->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    _descriptionInput->setFrameStyle(QFrame::Box);
    _descriptionInput->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
    _descriptionInput->setFixedHeight(HEIGHT_LARGEINPUTBOX);
    _descriptionInput->setPlaceholderText(PLACEHOLDER_DESCRIPTION);

    connect(_descriptionInput, SIGNAL(stringReady(QString)),
            this, SLOT(handleDescription(QString)));

    QHBoxLayout *descriptionLayout = new QHBoxLayout;
    descriptionLayout->addWidget(descriptionLabel, 0);
    descriptionLayout->addWidget(_descriptionInput, 10);

    addToBoxLayout(descriptionLayout);
}

void ActivityBox::setUpEventLinkInput(const QString &eventLink) {
    QLabel *eventLinkLabel = new QLabel(LABEL_EVENTLINK);
    eventLinkLabel->setFont(_boldFont);

    _eventLinkInput = new InputBox();
    _eventLinkInput->setText(eventLink);
    _eventLinkInput->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    _eventLinkInput->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    _eventLinkInput->setFrameStyle(QFrame::NoFrame);
    _eventLinkInput->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
    _eventLinkInput->setFixedHeight(HEIGHT_NORMALINPUTBOX);
    _eventLinkInput->setPlaceholderText(PLACEHOLDER_EVENTLINK);

    connect(_eventLinkInput, SIGNAL(stringReady(QString)),
            this, SLOT(handleEventLink(QString)));

    QHBoxLayout *eventLinkLayout = new QHBoxLayout;
    eventLinkLayout->addWidget(eventLinkLabel, 0);
    eventLinkLayout->addWidget(_eventLinkInput, 10);

    addToBoxLayout(eventLinkLayout);
}

void ActivityBox::setUpIsImportantCheckbox(bool isImportant) {
    _isImportantCheckBox = new QCheckBox(LABEL_IMPORTANT);
    _isImportantCheckBox->setChecked(isImportant);

    addToBoxLayout(_isImportantCheckBox);
}

void ActivityBox::setUpErrorLabel() {
    _errorLabel = new QLabel();
    _errorLabel->setFont(_boldFont);
    changeColor(Qt::red, Qt::white , _errorLabel);
    _errorLabel->hide();

    addToBoxLayout(_errorLabel);
}

void ActivityBox::setUpAddAndCancelButton() {
    QPushButton *addButton = setUpAddButton();
    QPushButton *cancelButton = setUpCancelButton();

    QHBoxLayout *buttonLayout = new QHBoxLayout;
    buttonLayout->addWidget(addButton);
    buttonLayout->addWidget(cancelButton);
    buttonLayout->setSpacing(0);
    addToBoxLayout(buttonLayout);
}

void ActivityBox::setUpUpdateAndCancelButton() {
    QPushButton *updateButton = setUpUpdateButton();
    QPushButton *cancelButton = setUpCancelButton();

    QHBoxLayout *buttonLayout = new QHBoxLayout;
    buttonLayout->addWidget(updateButton);
    buttonLayout->addWidget(cancelButton);
    buttonLayout->setSpacing(0);
    addToBoxLayout(buttonLayout);
}

QPushButton* ActivityBox::setUpAddButton() {
    QPushButton *addButton = new QPushButton();
    addButton->setText(LABEL_ADDBUTTON);
    addButton->setCursor(Qt::PointingHandCursor);
    setButtonStyleSheet(addButton);
    connect (addButton, SIGNAL(clicked()), this, SLOT(handleAddOrUpdate()));
    addButton->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    return addButton;
}

QPushButton* ActivityBox::setUpCancelButton() {
    QPushButton *cancelButton = new QPushButton;
    cancelButton->setText(LABEL_CANCELBUTTON);
    cancelButton->setCursor(Qt::PointingHandCursor);
    setButtonStyleSheet(cancelButton);
    connect (cancelButton, SIGNAL(clicked()), this, SLOT(handleCancel()));
    cancelButton->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    return cancelButton;
}

QPushButton* ActivityBox::setUpUpdateButton() {
    QPushButton *updateButton = new QPushButton;
    updateButton->setText(LABEL_UPDATEBUTTON);
    updateButton->setCursor(Qt::PointingHandCursor);
    setButtonStyleSheet(updateButton);
    connect (updateButton, SIGNAL(clicked()), this, SLOT(handleAddOrUpdate()));
    updateButton->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    return updateButton;
}

void ActivityBox::setActivityBoxAttribute() {
    this->setContentsMargins(10, 10, 10, 10);
    this->setAttribute(Qt::WA_TranslucentBackground, true);
    this->setWindowFlags(Qt::FramelessWindowHint);
}

void ActivityBox::addToBoxLayout(QHBoxLayout *toBeAddedLayout) {
    _boxLayout->addLayout(toBeAddedLayout);
}

void ActivityBox::addToBoxLayout(QWidget *toBeAddedWidget) {
    _boxLayout->addWidget(toBeAddedWidget);
}

void ActivityBox::changeColor(QColor font, QColor back, QWidget* widget) {
    widget->setAutoFillBackground(true);
    QPalette pallete = widget->palette();
    pallete.setColor(widget->foregroundRole(), font);
    pallete.setColor(widget->backgroundRole(), back);
    widget->setPalette(pallete);
}

void ActivityBox::setButtonStyleSheet(QPushButton *button) {
    button->setStyleSheet("background-color:" + COLOR_BUTTONBACKGROUND + ";"
                          "border-style:" + STYLE_BUTTONBORDER + ";"
                          "border-width:" + WIDTH_BUTTONBORDER + "px;"
                          "border-radius:" + RADIUS_BUTTONBORDER + "px;"
                          "border-color:" + COLOR_BUTTONBORDER + ";"
                          "font:" + FONT_BUTTON + "px;"
                          "color:" + COLOR_BUTTONFONT + ";"
                          "min-width:" + WIDTH_BUTTON + "px;"
                          "padding:" + PADDING_BUTTON + "px;");
}

void ActivityBox::showMessageIfEmptyInput(QString input, QString errorMessage) {
    if (input == STRING_EMPTY) {
        _errorLabel->setText(_errorLabel->text() + STRING_NEWLINE +
                             errorMessage);
        _errorLabel->show();
    } else {
        //do nothing
    }
}

void ActivityBox::showMessageIfTimeLogicError() {
    if (_startTimeInput->time() >= _endTimeInput->time()) {
        _errorLabel->setText(_errorLabel->text() + STRING_NEWLINE +
                             MESSAGE_TIMELOGICERROR);
        _errorLabel->show();
    } else {
        //do nothing
    }
}

void ActivityBox::showMessageIfDeadlineLogicError() {
    qDebug() << "FINDME: registration deadline: " << _registrationDeadlineInput->date().toString() <<
                " date: " << _dateInput->date().toString();
    qDebug() << "FINDME: starttime: " << _startTimeInput->time().toString() <<
                " registration deadline: " << _registrationDeadlineInput->time().toString();
    if ((_registrationDeadlineInput->date() > _dateInput->date()) ||
        (_registrationDeadlineInput->date() ==  _dateInput->date() &&
         _startTimeInput->time() <= _registrationDeadlineInput->time())) {
        _errorLabel->setText(_errorLabel->text() + STRING_NEWLINE +
                             MESSAGE_DEADLINELOGICERROR);
        _errorLabel->show();
    } else {
        //do nothing
    }
}

//TODO: check if url is valid
void ActivityBox::showMessageIfInvalidEventLink() {}

//private slots

void ActivityBox::handleTitle(QString inputString) {
    _titleInput->setText(inputString);
}

void ActivityBox::handleTrainer(QString inputString) {
    _trainerInput->setText(inputString);
}

void ActivityBox::handleMaxVacancy(QString inputString) {
    bool isNumeric;
    inputString.toInt(&isNumeric);
    if(isNumeric) {
        _maxVacancyInput->setText(inputString);
    }
}

void ActivityBox::handleLocation(QString inputString) {
    _locationInput->setText(inputString);
}

void ActivityBox::handleDescription(QString inputString) {
    _descriptionInput->setText(inputString);
}

void ActivityBox::handleEventLink(QString inputString) {
    _eventLinkInput->setText(inputString);
}

void ActivityBox::handleAddOrUpdate(){
    QString title = _titleInput->toPlainText();
    QString trainer = _trainerInput->toPlainText();
    QString maxVacancy = _maxVacancyInput->toPlainText();
    QString location = _locationInput->toPlainText();
    QString description = _descriptionInput->toPlainText();
    QString eventLink = _eventLinkInput->toPlainText();
    QDate date = _dateInput->date();
    QTime startTime = _startTimeInput->time();
    QTime endTime = _endTimeInput->time();
    QDateTime registrationDeadline = _registrationDeadlineInput->dateTime();

    bool isImportant = _isImportantCheckBox->isChecked();

    _errorLabel->setText(STRING_EMPTY);
    showMessageIfEmptyInput(title, MESSAGE_MISSINGTITLE);
    showMessageIfEmptyInput(trainer, MESSAGE_MISSINGTRAINER);
    showMessageIfEmptyInput(maxVacancy, MESSAGE_MISSINGMAXVACANCY);
    showMessageIfEmptyInput(location, MESSAGE_MISSINGLOCATION);
    showMessageIfEmptyInput(description, MESSAGE_MISSINGDESCRIPTION);
    showMessageIfTimeLogicError();
    showMessageIfDeadlineLogicError();
    showMessageIfInvalidEventLink();

    // no text in error label implies that no error
    if (_errorLabel->text() == STRING_EMPTY){
        QVector<QString> info;
        info.append(title);
        info.append(trainer);
        info.append(location);
        info.append(description);
        info.append(eventLink);

        QVector<QTime> time;
        time.append(startTime);
        time.append(endTime);

        emit this->passInfo(info, date, registrationDeadline, time,
                            maxVacancy.toInt(), isImportant);
        QDialog::done(0);
    }
}

void ActivityBox::handleCancel(){
    QDialog::done(0);
}
