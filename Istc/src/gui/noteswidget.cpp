#include "gui/noteswidget.h"

const QString NotesWidget::ICON_ADD = ":/icon/ButtonIcon/add.png";
const QString NotesWidget::SIZE_FONT = "15";

const QVector<int> NotesWidget::WIDTH_COLUMN (QVector<int>() << 50 << 130 << 120);

const QVector<QString> NotesWidget::HEADER_TABLE (QVector<QString>() << "" << "Date" << "Title");
const int NotesWidget::HEIGHT_ROW = 60;
const int NotesWidget::NUMBER_COLUMN = 3;
const int NotesWidget::WIDTH_TABLE = 800;

const QColor NotesWidget::COLOR_CONTENTBACKGROUND = QColor(207, 174, 220);
const QColor NotesWidget::COLOR_CONTENTFONT = QColor(Qt::black);
const QColor NotesWidget::COLOR_HEADERBACKGROUND = QColor(207, 174, 220);
const QColor NotesWidget::COLOR_HEADERTEXT = QColor(Qt::white);


NotesWidget::NotesWidget() {
    _personalModel = PersonalModel::getObject();

    setUpNotesLayout();
    setUpAddNotesIcon();
    setUpNotesTitleAndDateTime();
    setWidgetStyleSheet();
    setLayout(_notesLayout);
}

NotesWidget::~NotesWidget() {
    delete _notesTable;
    delete _notesLayout;
    delete _alignWidget;

    PersonalModel::deleteObject();
}

void NotesWidget::setUpNotesLayout() {
    _notesLayout = new QVBoxLayout();
}

void NotesWidget::setUpAddNotesIcon() {
    QHBoxLayout *addNotesLayout = new QHBoxLayout();
    addNotesLayout->setAlignment(Qt::AlignRight);

    QLabel *addNotesLabel = new QLabel("Add Notes");
    addNotesLayout->addWidget(addNotesLabel);

    QPixmap addIcon = QPixmap(ICON_ADD);
    QLabel *addIconLabel = new QLabel();
    addIconLabel->setPixmap(addIcon);
    addNotesLayout->addWidget(addIconLabel);

    QFrame *addNotesFrame = new QFrame();
    addNotesFrame->setLayout(addNotesLayout);
    addNotesFrame->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Maximum);
    ClickableWidget *clickableAdd = new ClickableWidget(addNotesFrame);

    connect(clickableAdd, SIGNAL(isClicked()),
            this, SLOT(addNote()));

    addNotesLayout->setAlignment(clickableAdd, Qt::AlignRight);

    _notesLayout->addWidget(clickableAdd, 0);
}

//private slots
void NotesWidget::addNote() {
    NotesBox *noteDetails = new NotesBox;
    connect (noteDetails, SIGNAL(passInfo(QVector<QString>)),
             this, SLOT(haveNoteDetails(QVector<QString>)));
    noteDetails->exec();
}

void NotesWidget::handleResize(int changedSize) {
        int size = _alignWidget->height() - changedSize;
        if (size < 0) {
            size = 0;
            _alignWidget->hide();
        } else {
            _alignWidget->show();
        }
        _alignWidget->setMaximumHeight(size);

}

void NotesWidget::haveNoteDetails(QVector<QString> info) {
    Notes* newNote = new Notes(info.at(0), info.at(1), info.at(2));
    QString notesId = _personalModel->generateNotesId();
    newNote->setNotesId(notesId);

    if (!_personalModel->addNote(newNote)) {
        std::cout << "some error occurs!!\n";
        return;
    }

    _notes.append(newNote);

    //content of the new table row
    int lastItemIndex = _notes.size() - 1;

    info.clear();
    info.append("");
    info.append(_notes.at(lastItemIndex)->getNotesDate().toString("dd MMM yyyy"));
    info.append(_notes.at(lastItemIndex)->getNotesTitle());

    _alignWidget->hide();

    _notesTable->addRow(info, _contRow++);

    //connect Qt Signal
    connect(_notes.at(lastItemIndex), SIGNAL(clickedUpdateButton(QString)),
            this, SLOT(updateNotesTable(QString)));

    //replace with clickable icon
    _notesTable->addCell(_notes.at(lastItemIndex)->getClickableEditIcon(), lastItemIndex, 0);

    //show the widget
    _notesTable->showWidget(lastItemIndex, 0);

    if (_alignWidget->isHidden()) {
        _alignWidget->show();
    }
}

void NotesWidget::addRowsToTable() {
    qDebug() << "NotesWidget -> addRowsToTable: "
                "start adding rows to table";

    _contRow = 0;

    _notes = _personalModel->getNotes();
    for (int i = 0; i < _notes.size(); i ++) {
        Notes *notes = _notes.at(i);
        QVector<QString> contentOfARow = QVector<QString>()
                << ""
                << notes->getNotesDate().toString("dd MMM yyyy")
                << notes->getNotesTitle();

        _notesTable->addRow(contentOfARow, _contRow++);

        connect(notes, SIGNAL(clickedUpdateButton(QString)),
                this, SLOT(updateNotesTable(QString)));
    }

    //Convert Note Name cell to ClickableWidget
    for (int i = 0; i < _notes.size(); i++) {
        _notesTable->addCell(_notes.at(i)->getClickableEditIcon(), i, 0);
    }

    qDebug() << "NotesWidget -> addRowsToTable: "
                "done adding rows to table";
}

void NotesWidget::setUpNotesTitleAndDateTime() {
    _notesTable = new TableWidget(true, NUMBER_COLUMN, WIDTH_COLUMN, HEIGHT_ROW);

    _notesTable->setHeader(HEADER_TABLE, COLOR_HEADERTEXT, COLOR_HEADERBACKGROUND);
    _notesTable->setContBackground(COLOR_CONTENTBACKGROUND);
    _notesTable->setContFont(COLOR_CONTENTFONT);

    addRowsToTable();

    //Set up layout
    _notesLayout->addWidget(_notesTable, 2);

    setUpAlignWidget();

    connect (_notesTable, SIGNAL(resizedTable(int)),
             this, SLOT(handleResize(int)));
}


void NotesWidget::setUpAlignWidget() {
    _alignWidget = new QWidget;
    _alignWidget->setSizePolicy(QSizePolicy::Expanding,
                                QSizePolicy::MinimumExpanding);

    //Set up layout
    _notesLayout->addWidget(_alignWidget, 5);
}

void NotesWidget::setWidgetStyleSheet() {
    this->setStyleSheet("QLabel{font:" + SIZE_FONT + "px;}");
}

void NotesWidget::updateNotesTable(QString notesId) {
    for (int i = 0; i < _notes.size(); i ++) {
        Notes* curNote = _notes.at(i);
        if (curNote->getNotesId() == notesId) {
            qDebug() << "NotesWidget::updateNotesTable " << _notes.at(i)->getNotesDate().toString("dd MMM yyyy");

            if (!_personalModel->updateNote(curNote)) {
                std::cout << "update note got error!\n";
                //delete curNote;
                return;
            }

            _notesTable->addCell(_notes.at(i)->getNotesDate().toString("dd MMM yyyy"), i, 1);
            _notesTable->addCell(_notes.at(i)->getNotesTitle(), i, 2);
        }
        //delete curNote;
    }
}

/*
 *  Public Slots
 */
void NotesWidget::updateTable() {
    assert(_notesTable);
    _notesTable->deleteAll();

    _personalModel->clearNotes();
    _alignWidget->setMinimumHeight(this->height());
    addRowsToTable();
}
