#include "cateros.h"

CateRos::CateRos() :
    CateBox() {
    CateRos::initializeCateRos();
}

//private methods:
void CateRos::initializeCateRos(){
    const QColor COLOR_BLUE = QColor (50, 149, 219, 255);
    const QString CATEGORY_ROS = "Roster";
    const QPixmap ICON_ROS = QPixmap(QString::fromUtf8(":/icon/ButtonIcon/Roster Icon.png"));

    CateBox::setCategoryName(CATEGORY_ROS);
    CateBox::setPixmap(ICON_ROS);
    CateBox::setColor(COLOR_BLUE);
    CateBox::createMyself();
}

