#include "gui/rostermonsterwidget.h"

const QColor RosterMonsterWidget::HEADER_BACK = QColor(99, 145, 213, 255);
const QColor RosterMonsterWidget::HEADER_FONT = Qt::white;
const QColor RosterMonsterWidget::CONT_BACK = QColor(219, 237, 249, 255);
const QColor RosterMonsterWidget::CONT_FONT = Qt::black;
const QColor RosterMonsterWidget::WIDGET_COLOR = QColor (145, 199, 235, 255);

const int RosterMonsterWidget::CONTENT_MARGINS = 0;

const QString RosterMonsterWidget::ICON_CROSS = QString::fromUtf8(":/icon/ButtonIcon/cross_small.png");
const QString RosterMonsterWidget::ICON_TICK = QString::fromUtf8(":/icon/ButtonIcon/tick_small.png");
const QString RosterMonsterWidget::STATUS_APPROVED = "Approved";
const QString RosterMonsterWidget::STATUS_DISAPPROVED = "Disapproved";
const QString RosterMonsterWidget::STATUS_PENDINGS = "Pending";
const QString RosterMonsterWidget::TAG_TOP_LABEL = "All Submitted Roster Requests by Team";

RosterMonsterWidget::RosterMonsterWidget() {
    _rosterModel = RosterModel::getObject();

    setUpTopLabel();
    setUpTable();
    setUpTableData();
    setUpMainLayout();
    addAllTeamRequests();
}

RosterMonsterWidget::~RosterMonsterWidget() {
    delete _topLabel;
    delete _rosterMonsterTable;
    delete _rosMonLayout;
    delete _dummyWidget;
}

void RosterMonsterWidget::setUpTopLabel() {
    QFont labelFont;
    labelFont.setBold(true);

    _topLabel = new QLabel(TAG_TOP_LABEL);
    _topLabel->setFont(labelFont);
    _topLabel->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    _topLabel->setContentsMargins(CONTENT_MARGINS, CONTENT_MARGINS,
                                  CONTENT_MARGINS, CONTENT_MARGINS);
}

void RosterMonsterWidget::setUpTable() {
    QVector <int> noOfColsofRos;
    noOfColsofRos.append(30);
    noOfColsofRos.append(70);
    noOfColsofRos.append(200);
    noOfColsofRos.append(180);

    _rosterMonsterTable = new TableWidget(true, 4, noOfColsofRos, 50);

    QVector <QString> header;
    header.append("ID");
    header.append("Day");
    header.append("Reason");
    header.append("Status");

    _rosterMonsterTable->setHeader(header, HEADER_FONT, HEADER_BACK);
    _rosterMonsterTable->setContBackground(CONT_BACK);
    _rosterMonsterTable->setContFont(CONT_FONT);



    connect (_rosterMonsterTable, SIGNAL(resizedTable(int)),
             this, SLOT(handleResize(int)));
}

ClickableWidget* RosterMonsterWidget::createTickClickableWidget() {
    QLabel *tickLogo = new QLabel();
    tickLogo->setPixmap(QPixmap(ICON_TICK));

    QHBoxLayout *tickLayout = new QHBoxLayout();
    tickLayout->addWidget(tickLogo);

    QWidget *tickWidget = new QWidget();
    tickWidget->setLayout(tickLayout);
    ClickableWidget *tickButton = new ClickableWidget(tickWidget);

    return tickButton;
}

ClickableWidget* RosterMonsterWidget::createCrossClickableWidget() {
    QLabel *crossLogo = new QLabel();
    crossLogo->setPixmap(QPixmap(ICON_CROSS));

    QHBoxLayout *crossLayout = new QHBoxLayout();
    crossLayout->addWidget(crossLogo);

    QWidget *crossWidget = new QWidget();
    crossWidget->setLayout(crossLayout);
    ClickableWidget *crossButton = new ClickableWidget(crossWidget);

    return crossButton;
}

void RosterMonsterWidget::setUpMainLayout() {
    // Dummy widget for component alignments
    _dummyWidget = new QWidget;
    _dummyWidget->setSizePolicy(QSizePolicy::Expanding,
                                QSizePolicy::Expanding);

    _rosMonLayout = new QVBoxLayout;
    _rosMonLayout->addWidget(_topLabel, 0);
    _rosMonLayout->addWidget(_rosterMonsterTable, 1);
    _rosMonLayout->addWidget(_dummyWidget, 2);
    _rosMonLayout->setSpacing(0);

    this->setLayout(_rosMonLayout);
    colorWidget(Qt::black, WIDGET_COLOR, this);
}

// TODO: Get from server/ database
void RosterMonsterWidget::setUpTableData() {

}


void RosterMonsterWidget::colorWidget(QColor font, QColor background, QWidget *widget) {
    widget->setAutoFillBackground(true);
    QPalette pallete = widget->palette();
    pallete.setColor(widget->foregroundRole(), font);
    pallete.setColor(widget->backgroundRole(), background);
    widget->setPalette(pallete);
}

void RosterMonsterWidget::addAllTeamRequests() {
    _teamRequests = _rosterModel->getTeamRequest();

    _appContRow = 0;

    for (int i = 0; i < _teamRequests->size(); i ++) {
        RosterRequestObj *obj = _teamRequests->value(i);

        QString index = QString::number(i + 1);
        QString date = obj->getDateRequest().toString("dd MMM yyyy");
        QString reason = obj->getReason();
        QString status = obj->getStatus();

        QVector <QString> row;
        row.append(index);
        row.append(date);
        row.append(reason);
        row.append(status);

        _rosterMonsterTable->addRow(row, _appContRow);

        if (status == STATUS_PENDINGS) {
            QWidget *dummyWidget = new QWidget();
            dummyWidget->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);
            ClickableWidget *tickButton = createTickClickableWidget();
            ClickableWidget *crossButton = createCrossClickableWidget();

            connect(tickButton, SIGNAL(isClicked()), obj, SLOT(approveRoster()));
            connect(crossButton, SIGNAL(isClicked()), obj, SLOT(disapproveRoster()));
            connect(obj, SIGNAL(statusUpdated(QString)), this, SLOT(approveRoster(QString)));
            connect(obj, SIGNAL(statusUpdated(QString)), this, SLOT(disapproveRoster(QString)));

            QHBoxLayout *approvalLayout = new QHBoxLayout();
            approvalLayout->addWidget(dummyWidget, 20);
            approvalLayout->addWidget(tickButton, 0);
            approvalLayout->addWidget(crossButton, 0);
            approvalLayout->addWidget(dummyWidget, 20);

            QWidget *approvalWidget = new QWidget();
            approvalWidget->setLayout(approvalLayout);

            _rosterMonsterTable->addCell(approvalWidget, _appContRow, 3);
            _rosterMonsterTable->showWidget(_appContRow, 3);
        }
        _appContRow++;
    }
}

//private slots
void RosterMonsterWidget::approveRoster(QString rosterRequestId) {
    for (int i = 0; i < _teamRequests->size(); i ++) {
        RosterRequestObj *currentRosterRequest = _teamRequests->value(i);

        QString status = currentRosterRequest->getStatus();
        QString currentRequestId = currentRosterRequest->getRosterRequestId();

        if (rosterRequestId == currentRequestId) {
            _rosterModel->updateBlockOutReq(currentRosterRequest);
            _rosterMonsterTable->addCell(status, i, 3);
            _rosterMonsterTable->showWidget(i, 3);
        }
    }
}

void RosterMonsterWidget::disapproveRoster(QString rosterRequestId) {
    for (int i = 0; i < _teamRequests->size(); i ++) {
        RosterRequestObj *currentRosterRequest = _teamRequests->value(i);

        QString status = currentRosterRequest->getStatus();
        QString currentRequestId = currentRosterRequest->getRosterRequestId();

        if (rosterRequestId == currentRequestId) {
            _rosterModel->updateBlockOutReq(currentRosterRequest);
            _rosterMonsterTable->addCell(status, i, 3);
            _rosterMonsterTable->showWidget(i, 3);
        }
    }
}

/*
 * Helper Methods:
 * This method is currenly useless in this class
 * cause no size changing involved.
 */
void RosterMonsterWidget::handleResize(int changedSize) {
    int size = _dummyWidget->height() - changedSize;
    if (size <= 0) {
        size = 0;
        _dummyWidget->hide();
    } else {
        _dummyWidget->show();
    }
    _dummyWidget->setMaximumHeight(size);
}

/*
 * Public Slots
 */
void RosterMonsterWidget::updateTable() {
    assert(_rosterMonsterTable);
    _rosterMonsterTable->deleteAll();
    _dummyWidget->setMinimumHeight(this->height());
    addAllTeamRequests();
}
