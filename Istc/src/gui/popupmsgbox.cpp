#include "gui/popupmsgbox.h"

const QString PopUpMsgBox::LABEL_CLOSEBUTTON = "Close" ;

PopUpMsgBox::PopUpMsgBox(QString message) {
    qDebug() << "PopUpMsgBox : Initializing pop up message box";
    _mainLayout = new QVBoxLayout();
    setMessageLabel(message);
    setCloseButton();
    this->setLayout(_mainLayout);
    qDebug() << "PopUpMsgBox : Initialized pop up message box";
}

PopUpMsgBox::~PopUpMsgBox() {
    delete _mainLayout;
}

void PopUpMsgBox::setMessageLabel(QString message)
{
    QLabel *messageLabel = new QLabel(message);
    _mainLayout->addWidget(messageLabel);
}

void PopUpMsgBox::setCloseButton() {
    QPushButton *closeButton = new QPushButton(LABEL_CLOSEBUTTON);

    PopupBox::setButtonStyleSheet(closeButton);

    connect(closeButton, SIGNAL(clicked()),
            this, SLOT(clickedCloseButton()));

    _mainLayout->addWidget(closeButton);
}

void PopUpMsgBox::paintEvent(QPaintEvent *event) {
    QDialog::paintEvent(event);
    QPainter painter(this);
    QBrush brush(Qt::white);
    painter.setBrush(brush);
    painter.setPen(Qt::white);
    painter.drawRoundRect(QRect(0, 0, this->width(), this->height()));
}

//slots
void PopUpMsgBox::clickedCloseButton() {
    QDialog::done(0);
}
