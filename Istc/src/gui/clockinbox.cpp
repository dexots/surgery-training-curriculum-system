#include "gui/clockinbox.h"

const QString ClockInBox::COLOR_BUTTONBACKGROUND = "rgb(39, 174, 96)";
const QString ClockInBox::COLOR_BUTTONBORDER = "white";
const QString ClockInBox::COLOR_BUTTONFONT = "white";

const QString ClockInBox::FONT_BUTTON = "bold 14";
const QString ClockInBox::PADDING_BUTTON = "6";
const QString ClockInBox::RADIUS_BUTTONBORDER = "10";
const QString ClockInBox::STYLE_BUTTONBORDER = "outset";

const QString ClockInBox::WIDTH_BUTTON = "50";
const QString ClockInBox::WIDTH_BUTTONBORDER = "2";

const QString ClockInBox::QSTRING_EMPTY = "";
const QString ClockInBox::QSTRING_CLOCKIN = "Clock In";
const QString ClockInBox::QSTRING_CLOCKOUT = "Clock Out";
const QString ClockInBox::QSTRING_CANCEL = "Cancel";

ClockInBox::ClockInBox(bool clockInOrOut) {
    if (!clockInOrOut) {
        _clockInOrOut = QSTRING_CLOCKIN;
    } else {
        _clockInOrOut = QSTRING_CLOCKOUT;
    }

    initializeVariables();
    initializeTime();
    initializeButtons();
    setBoxLayout();
}

ClockInBox::~ClockInBox() {
    delete _header;
    delete _actualTime;
    delete _clockButton;
    delete _cancelButton;
    delete _boxLayout;
    delete _timeLayout;
    delete _buttonLayout;
}

/** Set Up GUI
 *
 */

void ClockInBox::initializeVariables() {
    _header = new QLabel(QSTRING_EMPTY);
    _actualTime = new QDateTimeEdit;
    _clockButton = new QPushButton;
    _cancelButton = new QPushButton;
    _boxLayout = new QVBoxLayout;
    _timeLayout = new QVBoxLayout;
    _buttonLayout = new QHBoxLayout;
}

void ClockInBox::initializeTime() {
    _header->setText(_clockInOrOut + " time:");

    _actualTime->setDate(QDate::currentDate());
    _actualTime->setTime(QTime::currentTime());

    _timeLayout->addWidget(_header);
    _timeLayout->addWidget(_actualTime);
}

void ClockInBox::initializeButtons() {
    // Clock in button
    _clockButton->setText(_clockInOrOut);
    _clockButton->setCursor(Qt::PointingHandCursor);
    setButtonStyleSheet(_clockButton);
    connect (_clockButton, SIGNAL(clicked()),
             this, SLOT(handleAdd()));
    _clockButton->setSizePolicy(QSizePolicy::Fixed,
                              QSizePolicy::Fixed);

    //cancel
    _cancelButton->setText("Cancel");
    _cancelButton->setCursor(Qt::PointingHandCursor);
    setButtonStyleSheet(_cancelButton);
    connect (_cancelButton, SIGNAL(clicked()),
             this, SLOT(handleCancel()));
    _cancelButton->setSizePolicy(QSizePolicy::Fixed,
                                 QSizePolicy::Fixed);

    //button layout
    _buttonLayout->addWidget(_clockButton);
    _buttonLayout->addWidget(_cancelButton);
    _buttonLayout->setSpacing(0);
}

void ClockInBox::setBoxLayout() {
    _boxLayout->addLayout(_timeLayout);
    _boxLayout->addLayout(_buttonLayout);

    this->setLayout(_boxLayout);

    this->setContentsMargins(10, 10, 10, 10);

    this->setAttribute(Qt::WA_TranslucentBackground, true);

    this->setWindowFlags(Qt::FramelessWindowHint);
}

/**
 * Helper methods
 */

void ClockInBox::paintEvent(QPaintEvent *event) {
    QDialog::paintEvent(event);
    QPainter painter(this);
    QBrush brush(Qt::white);
    painter.setBrush(brush);
    painter.setPen(Qt::white);
    painter.drawRoundRect(QRect(0, 0, this->width(), this->height()));
}

void ClockInBox::setButtonStyleSheet(QPushButton *button) {
    button->setStyleSheet("background-color:" + COLOR_BUTTONBACKGROUND + ";"
                          "border-style:" + STYLE_BUTTONBORDER + ";"
                          "border-width:" + WIDTH_BUTTONBORDER + "px;"
                          "border-radius:" + RADIUS_BUTTONBORDER + "px;"
                          "border-color:" + COLOR_BUTTONBORDER + ";"
                          "font:" + FONT_BUTTON + "px;"
                          "color:" + COLOR_BUTTONFONT + ";"
                          "min-width:" + WIDTH_BUTTON + "px;"
                          "padding:" + PADDING_BUTTON + "px;");
}

/** Private Slots
 *
 */

void ClockInBox::handleAdd() {
    QDateTime time = _actualTime->dateTime();
    emit this->passInfo(time);
    QDialog::done(0);
}

void ClockInBox::handleCancel() {
    QDialog::done(0);
}
