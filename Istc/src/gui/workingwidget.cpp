#include "gui/workingwidget.h"

const int WorkingWidget::BASE = 10;
const int WorkingWidget::TIMER_INTERVAL = 10 * 60;
const int WorkingWidget::SEC_TO_MILLI = 1000;

const QString WorkingWidget::ERROR_LOGIN = "Cannot login twice at the same day!";

WorkingWidget::WorkingWidget() {
    _timer = new QTimer(this);
    connect (_timer, SIGNAL(timeout()),
             this, SLOT(updateTime()));

    _logbookModel = LogbookModel::getObject();

    _inState = false;

    //setup the first row
    _clockOut = new QLabel;
    _clockOut->setText("Clock Out");
    _clockOut->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

    _clockIn = new QLabel;
    _clockIn->setText("Clock In");
    _clockIn->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

    _clockLogo = new QLabel;
    _clockLogo->setPixmap(QPixmap(QString::fromUtf8(":/icon/ButtonIcon/clock.png")));
    _clockLogo->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

    _clockLayout = new QHBoxLayout;
    _clockLayout->addWidget(_clockOut);
    _clockLayout->addWidget(_clockIn);
    _clockLayout->addWidget(_clockLogo);
    _clockLayout->setContentsMargins(0, 0, 0, 0);

    QWidget *clockWidget = new QWidget;
    clockWidget->setLayout(_clockLayout);
    _clockWidget = new ClickableWidget(clockWidget);
    _clockWidget->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

    //connect(_clockWidget, SIGNAL(isClicked()), this, SLOT(changeState()));
    connect(_clockWidget, SIGNAL(isClicked()), this, SLOT(clickedClock()));


    //this is to align right
    QHBoxLayout *anotherLayout = new QHBoxLayout;
    anotherLayout->addWidget(_clockWidget);
    QWidget *anotherWidget = new QWidget;
    anotherWidget->setLayout(anotherLayout);
    anotherLayout->setAlignment(_clockWidget, Qt::AlignRight);
    anotherLayout->setContentsMargins(0, 0, 0, 0);

    _clockOut->hide();
    _clockIn->show();


    //2nd row
    QDate todayDate = QDate::currentDate();

    QDate firstDateOfWeek = todayDate.addDays(1 - todayDate.dayOfWeek());
    QDate lastDateOfWeek = firstDateOfWeek.addDays(6);

    QLabel *curWeek = new QLabel;
    QString curWeekText = QString("Current Week ( ");
    curWeekText += firstDateOfWeek.toString("dd MMM");
    curWeekText += QString(" - ");
    curWeekText += lastDateOfWeek.toString("dd MMM yyyy");
    curWeekText += QString(" ) :");
    curWeek->setText(curWeekText);

    QFont boldFont;
    boldFont.setBold(true);
    curWeek->setFont(boldFont);

    //3rd row
    int weekHour = _logbookModel->calculateCurWeekHour();
    _curWeekProgress = new ProgressBar;
    _curWeekProgress->setBadToGood(false);
    _curWeekProgress->setMinimum(0);
    _curWeekProgress->setMaximum(80);
    _curWeekProgress->setValue(weekHour);
    _curWeekProgress->setFormat("%v out of %m hours");
    _curWeekProgress->show();

    //4th row
    QLabel *curLimit = new QLabel;
    curLimit->setText("Current consecutive hours limit:");
    curLimit->setFont(boldFont);

    //5th row
    int conHour = _logbookModel->calculateConHour();
    _curLimitProgress = new ProgressBar;
    _curLimitProgress->setBadToGood(false);
    _curLimitProgress->setMinimum(0);
    _curLimitProgress->setMaximum(30);
    _curLimitProgress->setValue(conHour);
    _curLimitProgress->setFormat("%v out of %m hours");
    _curLimitProgress->show();

    //the table
    QVector <int> noOfCol;
    noOfCol.append(100);
    noOfCol.append(100);
    noOfCol.append(100);
    noOfCol.append(100);
    noOfCol.append(100);

    _table = new TableWidget(true, 5 , noOfCol);

    QVector <QString> header;
    header.append("Day In");
    header.append("Time In");
    header.append("Day Out");
    header.append("Time Out");
    header.append("No Of Hours");

    _table->setHeader(header, Qt::white, QColor(237, 125, 49, 255));
    _table->setContBackground(QColor(252, 236, 232, 255));
    _table->setContFont(Qt::black);

    addRowsToTable();

    connect (_table, SIGNAL(resizedTable(int)),
             this, SLOT(handleTableResize(int)));

    qDebug() << "WorkingWidget -> WorkingWidget: "
                "setting up dummyWidget";
    //dummyWidget
    _dummyWidget = new QWidget;
    _dummyWidget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    qDebug() << "WorkingWidget -> WorkingWidget: "
                "done setting up dummyWidget";

    _workingLayout = new QVBoxLayout;
    _workingLayout->setSpacing(0);
    _workingLayout->addWidget(anotherWidget);
    _workingLayout->addWidget(curWeek);
    _workingLayout->addWidget(_curWeekProgress);
    _workingLayout->addWidget(curLimit);
    _workingLayout->addWidget(_curLimitProgress);
    _workingLayout->setSpacing(15);
    _workingLayout->addWidget(_table);
    _workingLayout->addWidget(_dummyWidget);

    this->setLayout(_workingLayout);
    WorkingWidget::colorWidget(QColor (243, 156, 18, 255),
                               this);
}

WorkingWidget::~WorkingWidget(){
    delete _dummyWidget;
    delete _curWeekProgress;
    delete _curLimitProgress;
    delete _clockIn;
    delete _clockOut;
    delete _clockLogo;
    delete _clockLayout;
    delete _clockWidget;
    delete _table;
    delete _workingLayout;
    delete _timer;
    LogbookModel::deleteObject();
}


//private methods
void WorkingWidget::addRowsToTable() {

    qDebug() << "WorkingWidget -> workingWidget: "
                "loading db...";
    QVector <WorkingHoursObject>
            works = _logbookModel->getWorkingHoursData();

    qDebug() << "WorkingWidget -> workingWidget: "
                "Done loading db...";

    qDebug() << "WorkingWidget -> WorkingWidget: "
                "start adding rows...";
    int contRow = 0;
    QVector <QString> row;
    for (int i = works.size() - 1; i >= 0; i--) {
        WorkingHoursObject work = works.at(i);
        QDateTime checkIn = work.getCheckInTime();
        QDateTime checkOut = work.getCheckOutTime();

        QString dayIn = checkIn.toString("dd, ddd");
        QString timeIn = checkIn.toString("hh:mm");

        _includedDates.append(dayIn);
        row.append(dayIn);
        row.append(timeIn);


        if (checkOut.isNull()) {
            row.append("");
            row.append("");

            _logbookModel->activateWorkingHoursObject(work);
            _inState = true;
            _timer->start(TIMER_INTERVAL * SEC_TO_MILLI);

            _clockIn->hide();
            _clockOut->show();
\
        } else {
            QString dayOut = checkOut.toString("dd, ddd");
            QString timeOut = checkOut.toString("hh:mm");
            row.append(dayOut);
            row.append(timeOut);
        }

        qint64 noOfHours = work.getNoOfHours();
        QString hourString;
        if (noOfHours < 0) {
            hourString = "-";
        } else {
            hourString = QString::number(noOfHours,
                                         BASE);
        }
        row.append(hourString);

        _table->addRow(row, contRow);
        contRow++;

        row.clear();
    }
    qDebug() << "WorkingWidget -> WorkingWidget: "
                "done adding rows...";

}

void WorkingWidget::colorWidget(QColor color, QWidget *widget){
    widget->setAutoFillBackground(true);
    QPalette pallete = widget->palette();
    pallete.setColor(widget->backgroundRole(), color);
    widget->setPalette(pallete);
}

//private slots
void WorkingWidget::clickedClock() {
    ClockInBox *clockInBox = new ClockInBox(_inState);
    connect(clockInBox, SIGNAL(passInfo(QDateTime)),
            this, SLOT(changeState(QDateTime)));
    clockInBox->exec();
}

void WorkingWidget::changeState(QDateTime time){
    if (_inState){
        _clockIn->show();
        _clockOut->hide();
        _inState = false;
        //_clockOutTime = QDateTime::currentDateTime();
        _clockOutTime = time;

        bool isLogout = _logbookModel->setLogout(_clockOutTime);
        assert (isLogout);

        QString timeDif = QString::number(_logbookModel->calculateConHour(),
                                          BASE);
        updateTime();

        int row = 0;

        _table->addCell(_clockOutTime.toString("dd , ddd"), row, 2);
        _table->addCell(_clockOutTime.toString("hhmm"), row, 3);
        _table->addCell(timeDif, row, 4);
        _timer->stop();
    } else {
        _clockInTime = time;
        //_clockInTime = QDateTime::currentDateTime();

        bool isLogin = _logbookModel->setLogin(_clockInTime);
        if (!isLogin) {
            PopUpMsgBox* errorBox = new PopUpMsgBox(ERROR_LOGIN);
            errorBox->exec();
            errorBox->deleteLater();
            return;
        }

        QString dayIn = _clockInTime.toString("dd , ddd");
        if (_includedDates.contains(dayIn)) {
            //return error message, maybe?
            return;
        }
        _includedDates.append(dayIn);


        _clockIn->hide();
        _clockOut->show();
        _inState = true;
        QVector <QString> row;

        row.append(_clockInTime.toString("dd , ddd"));
        row.append(_clockInTime.toString("hhmm"));
        row.append("");
        row.append("");
        row.append("");

        _dummyWidget->hide();

        _table->addRowFromTop(row);


        _timer->start(TIMER_INTERVAL * SEC_TO_MILLI);

        if (_dummyWidget->isHidden()) {
            _dummyWidget->show();
        }
    }
}

void WorkingWidget::updateTime() {
    int weekHour = _logbookModel->calculateCurWeekHour();
    _curWeekProgress->setValue(weekHour);

    int conHour = _logbookModel->calculateConHour();
    _curLimitProgress->setValue(conHour);

}

void WorkingWidget::handleTableResize(int minHeight) {

    int height = _dummyWidget->height();
    height -= minHeight;
    if (height <= 0) {
        height = 0;
        _dummyWidget->hide();
    } else {
        _dummyWidget->show();
    }
    _dummyWidget->setMaximumHeight(height);

}

/*
 * Public Slots
 */
void WorkingWidget::updateTable() {
    assert(_table);
    _table->deleteAll();
    _logbookModel->clearWorkingHoursDatas();
    _includedDates.clear();
    _dummyWidget->setMinimumHeight(this->height());
    addRowsToTable();
}
