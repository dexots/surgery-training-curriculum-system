#include "gui/examinationwidget.h"

const QVector<int> ExaminationWidget::WIDTH_COLUMN (QVector<int>() << 120 << 240 << 120 << 120 << 120 << 80);

const QVector<QString> ExaminationWidget::HEADER_TABLE (QVector<QString>() << "Serial Number" << "Module Name" << "Date"
                                                        << "Time" << "Location" << "Seat No");

const int ExaminationWidget::HEIGHT_ROW = 50;
const int ExaminationWidget::NUMBER_COLUMN = 6;

const QColor ExaminationWidget::COLOR_CONTENTBACKGROUND = QColor(235, 241, 233);
const QColor ExaminationWidget::COLOR_CONTENTFONT = QColor(Qt::black);
const QColor ExaminationWidget::COLOR_HEADERBACKGROUND = QColor(112, 173, 71);
const QColor ExaminationWidget::COLOR_HEADERTEXT = QColor(Qt::white);

const QString ExaminationWidget::MESSAGE_NOTICE = "*Location and Seat No will be released at a nearer date";

ExaminationWidget::ExaminationWidget() {

    setUpExaminationLayout();

    _examinationTable = new TableWidget(true, NUMBER_COLUMN, WIDTH_COLUMN, HEIGHT_ROW);

    _examinationTable->setHeader(HEADER_TABLE, COLOR_HEADERTEXT, COLOR_HEADERBACKGROUND);

    _examinationTable->setContBackground(COLOR_CONTENTBACKGROUND);
    _examinationTable->setContFont(COLOR_CONTENTFONT);

    addRowsToTable();

    _examinationLayout->addWidget(_examinationTable, 2);

    QLabel *noticeLabel = new QLabel();
    noticeLabel->setText("<h3>" + MESSAGE_NOTICE);

    _examinationLayout->addWidget(noticeLabel);

    _dummyWidget = new QWidget();
    _dummyWidget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    _examinationLayout->addWidget(_dummyWidget, 10);

    setLayout(_examinationLayout);
}

ExaminationWidget::~ExaminationWidget() {
    delete _dummyWidget;
    delete _examinationLayout;
}

void ExaminationWidget::addRowsToTable() {
    TrainingModel *trainingModel = TrainingModel::getObject();
    QVector<Exam> exams = trainingModel->getExamInfo();

    QVector<QString> contentOfARow;
    for (int i = 0; i < exams.size(); i ++) {
        contentOfARow = QVector<QString>()
                        << exams.value(i).getModuleCode()
                        << exams.value(i).getModuleName()
                        << exams.value(i).getExamDate().toString("d MMM yyyy")
                        << exams.value(i).getExamTime().toString("hh:mm")
                        << exams.value(i).getExamLocation()
                        << exams.value(i).getSeatNo();
        _examinationTable->addRow(contentOfARow);
    }
}

void ExaminationWidget::setUpExaminationLayout() {
    _examinationLayout = new QVBoxLayout;
    _examinationLayout->setAlignment(Qt::AlignCenter | Qt::AlignTop);
}

/*
 *  Public Slots
 */
void ExaminationWidget::updateTable() {
    assert(_examinationTable);
    _examinationTable->deleteAll();
    _dummyWidget->setMinimumHeight(this->height());
    addRowsToTable();
}
