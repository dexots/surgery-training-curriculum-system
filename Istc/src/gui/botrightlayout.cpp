#include "botrightlayout.h"


//destructor
BotRightLayout::~BotRightLayout() {
    BotRightLayout::clearGarbage();
}

//constructor
BotRightLayout::BotRightLayout() {
    BotRightLayout::initializeLayout();
}

//private methods:
void BotRightLayout::clearGarbage(){
    delete _mainLayout;
}

void BotRightLayout::initializeLayout(){
    _cur.state = PAGE::MAIN;
    BotRightLayout::createLayout(_cur);
}

void BotRightLayout::createLayout(PAGE reqState){
    switch (reqState.state) {
    case PAGE::MAIN:
        _mainLayout = new BotRightMain;
        this->addLayout(_mainLayout);
        break;

    case PAGE::PRO:
        break;

    case PAGE::TRAIN:
        break;

    case PAGE::ROS:
        break;

    case PAGE::LOG:
        break;

    case PAGE::RESEARCH:
        break;

    case PAGE::RESOURCES:
        break;

    default:
        assert (false); //impossible to reach here
        break;
    }
}

void BotRightLayout::deleteLayout(PAGE reqState){
    switch (reqState.state) {
    case PAGE::MAIN:
        delete _mainLayout;
        break;

    case PAGE::PRO:
        break;

    case PAGE::TRAIN:
        break;

    case PAGE::ROS:
        break;

    case PAGE::LOG:
        break;

    case PAGE::RESEARCH:
        break;

    case PAGE::RESOURCES:
        break;

    default:
        assert (false); //impossible to reach here
        break;
    }
}
