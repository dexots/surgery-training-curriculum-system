#include "gui/syncbox.h"

const QString SyncBox::COLOR_BUTTONBACKGROUND = "rgb(39, 174, 96)";
const QString SyncBox::COLOR_BUTTONBORDER = "white";
const QString SyncBox::COLOR_BUTTONFONT = "white";
const QString SyncBox::FONT_BUTTON = "bold 14";
const QString SyncBox::PADDING_BUTTON = "6";
const QString SyncBox::RADIUS_BUTTONBORDER = "10";
const QString SyncBox::STYLE_BUTTONBORDER = "outset";
const QString SyncBox::WIDTH_BUTTON = "50";
const QString SyncBox::WIDTH_BUTTONBORDER = "2";
const QString SyncBox::SYNC_TEXT = "Syncing...";
const QString SyncBox::SYNC_SUCCESS = "Sync successfully! :)";
const QString SyncBox::SYNC_FAIL = "Sync failed! :(";
const QString SyncBox::SPINNER_LOCATION = ":/icon/ButtonIcon/spinner.gif";

const int SyncBox::WIDTH_MINIMUM = 400;
const int SyncBox::HEIGHT_MINIMUM = 200;


SyncBox::SyncBox() {
    _localDbClient = LocalDatabaseClient::getObject();

    _bold.setBold(true);

    // Spinner Icon
    _spinner = new QMovie(SPINNER_LOCATION);
    _spinnerLabel = new QLabel;
    _spinnerLabel->setMovie(_spinner);
    _spinner->start();

    _syncLabel = new QLabel;
    _syncLabel->setText(SYNC_TEXT);
    _syncLabel->setFont(_bold);

    _statusLabel = new QLabel;
    _statusLabel->setFont(_bold);
    _statusLabel->hide();

    _spinnerLayout = new QHBoxLayout();
    _spinnerLayout->addWidget(_spinnerLabel);
    _spinnerLayout->addWidget(_syncLabel);
    _spinnerLayout->addWidget(_statusLabel);

    _okButton = new QPushButton;
    _okButton->setText("OKIE");
    _okButton->setCursor(Qt::PointingHandCursor);
    SyncBox::setButtonStyleSheet(_okButton);
    connect(_okButton, SIGNAL(clicked()),
            this, SLOT(handleButton()));
    _okButton->setSizePolicy(QSizePolicy::Fixed,
                             QSizePolicy::Fixed);

    _buttonLayout = new QHBoxLayout;
    _buttonLayout->addWidget(_okButton, Qt::AlignHCenter);

    // Main Layout
    _mainLayout = new QVBoxLayout;
    _mainLayout->addLayout(_spinnerLayout);
    _mainLayout->addLayout(_buttonLayout);

    this->setLayout(_mainLayout);
    this->setContentsMargins(10, 10, 10, 10);
    this->setMinimumWidth(WIDTH_MINIMUM);
    this->setMinimumHeight(HEIGHT_MINIMUM);

    this->setAttribute(Qt::WA_TranslucentBackground, true);

    this->setWindowFlags(Qt::FramelessWindowHint);

    connect (_localDbClient, SIGNAL(hasSynced(bool)),
             this, SLOT(handleSyncSignal(bool)));

    _localDbClient->syncAllTables();
}

SyncBox::~SyncBox() {
    delete _syncLabel;
    delete _statusLabel;
    delete _spinnerLabel;
    delete _okButton;
    delete _mainLayout;
    delete _spinnerLayout;
    delete _spinner;
}

void SyncBox::paintEvent(QPaintEvent *event) {
    QDialog::paintEvent(event);
    QPainter painter(this);
    QBrush brush(Qt::white);
    painter.setBrush(brush);
    painter.setPen(Qt::white);
    painter.drawRoundRect(QRect(0, 0, this->width(), this->height()));
}

void SyncBox::changeColor(QColor font, QColor back, QWidget *widget) {
    widget->setAutoFillBackground(true);
    QPalette pallete = widget->palette();
    pallete.setColor(widget->foregroundRole(), font);
    pallete.setColor(widget->backgroundRole(), back);
    widget->setPalette(pallete);
}

void SyncBox::setButtonStyleSheet(QPushButton *button) {
    button->setStyleSheet("background-color:" + COLOR_BUTTONBACKGROUND + ";"
                          "border-style:" + STYLE_BUTTONBORDER + ";"
                          "border-width:" + WIDTH_BUTTONBORDER + "px;"
                          "border-radius:" + RADIUS_BUTTONBORDER + "px;"
                          "border-color:" + COLOR_BUTTONBORDER + ";"
                          "font:" + FONT_BUTTON + "px;"
                          "color:" + COLOR_BUTTONFONT + ";"
                          "min-width:" + WIDTH_BUTTON + "px;"
                          "padding:" + PADDING_BUTTON + "px;");
}

void SyncBox::handleButton() {
    QDialog::done(0);
}

void SyncBox::handleSyncSignal(bool isSuccess) {
    if (isSuccess) {
        _statusLabel->setText(SYNC_SUCCESS);
        _spinnerLabel->hide();
        emit this->receivedSuccessSyncSignal();
    } else {
        _statusLabel->setText(SYNC_FAIL);
    }
    _syncLabel->hide();
    _statusLabel->show();
}
