#include "gui/resourcewidget.h"

const QString ResourceWidget::DELIMITER = ":==";
const QString ResourceWidget::TEXTBOOK_LINK = "textbooks.txt";
const QString ResourceWidget::JOURNAL_LINK = "journals.txt";
const QString ResourceWidget::QUESTION_LINK = "questionbanks.txt";
const QString ResourceWidget::CLASS_NAME = "resource";

const QString ResourceWidget::TYPE_JOURNAL = "JOURNAL";
const QString ResourceWidget::TYPE_QUESTION = "QUESTION";
const QString ResourceWidget::TYPE_TEXTBOOK = "TEXTBOOK";

ResourceWidget::ResourceWidget() {
    ResourceWidget::createLayout();
}

ResourceWidget::~ResourceWidget() {
    delete _resourcesTable;
    delete _widgetLayout;
}

//public method:
QString ResourceWidget::getClassName() {
    return CLASS_NAME;
}

//private methods:
void ResourceWidget::createLayout() {
    _widgetLayout = new QVBoxLayout;

    _textbookWidget = new HyperlinkWidget(TYPE_TEXTBOOK);
    generateTextbooks();
    _textbookWidget->setContentsMargins(20,20,20,20);
    ResourceWidget::colorWidget(QColor (208, 206, 206, 255),
                                _textbookWidget);

    SubCate *textBooksCate = new SubCate("TextBooks",
                                        "TextBooks and Reference Books for your modules",
                                        "",
                                        Qt::gray,
                                        QColor (236, 240, 241, 255),
                                        QColor (208, 206, 206, 255),
                                        _textbookWidget);

    _journalWidget = new HyperlinkWidget(TYPE_JOURNAL);
    generateJournals();
    _journalWidget->setContentsMargins(20,20,20,20);
    ResourceWidget::colorWidget(QColor (208, 206, 206, 255),
                                _journalWidget);

    SubCate *journalCate = new SubCate("e-Journals",
                                        "Notable online journals accessible using NUH account",
                                        "",
                                        Qt::gray,
                                        QColor (236, 240, 241, 255),
                                        QColor (208, 206, 206, 255),
                                        _journalWidget);

    _questionWidget = new HyperlinkWidget(TYPE_QUESTION);
    generateQuestionBanks();
    _questionWidget->setContentsMargins(20,20,20,20);
    ResourceWidget::colorWidget(QColor (208, 206, 206, 255),
                                _questionWidget);

    SubCate *questionCate = new SubCate("Question Banks",
                                        "Prepare for your medical school examinations",
                                        "",
                                        Qt::gray,
                                        QColor (236, 240, 241, 255),
                                        QColor (208, 206, 206, 255),
                                        _questionWidget);

    QVector <SubCate*> resourcesSubCategories;
    resourcesSubCategories.append(textBooksCate);
    resourcesSubCategories.append(journalCate);
    resourcesSubCategories.append(questionCate);

    _resourcesTable = new CateTable("Resources",
                                    "",
                                    ":/icon/ButtonIcon/Resources Icon.png",
                                    QColor (149, 165, 166, 255),
                                    resourcesSubCategories);

    _widgetLayout->addWidget(_resourcesTable);
    _widgetLayout->setContentsMargins(0, 0, 0, 0);
    this->setLayout(_widgetLayout);

    connect(_resourcesTable, SIGNAL(backToHome()), this, SLOT(handleHomeSignal()));
}

void ResourceWidget::colorWidget(QColor color, QWidget *widget) {
    widget->setAutoFillBackground(true);
    QPalette pallete = widget->palette();
    pallete.setColor(widget->backgroundRole(), color);
    widget->setPalette(pallete);
}

//private slot
void ResourceWidget::handleHomeSignal() {
    emit this->backToHome();
}

void ResourceWidget::handleHyperlink(QString hyperlink) {
    emit this->load(hyperlink, CLASS_NAME);
}

void ResourceWidget::generateTextbooks() {
    getFileContent(TEXTBOOK_LINK);
    _textbookWidget->addLinkAndText(_hyperlinks, _texts);
    _hyperlinks.clear();
    _texts.clear();

    connect (_textbookWidget, SIGNAL(openLink(QString)),
             this, SLOT(handleHyperlink(QString)));
}

void ResourceWidget::generateJournals() {
    getFileContent(JOURNAL_LINK);
    _journalWidget->addLinkAndText(_hyperlinks, _texts);
    _hyperlinks.clear();
    _texts.clear();

    connect (_journalWidget, SIGNAL(openLink(QString)),
             this, SLOT(handleHyperlink(QString)));
}

void ResourceWidget::generateQuestionBanks() {
    getFileContent(QUESTION_LINK);
    _questionWidget->addLinkAndText(_hyperlinks, _texts);
    _hyperlinks.clear();
    _texts.clear();

    connect (_questionWidget, SIGNAL(openLink(QString)),
             this, SLOT(handleHyperlink(QString)));
}

void ResourceWidget::getFileContent(QString fileLink) {
    QFile inputFile(fileLink);
    if (inputFile.open(QIODevice::ReadOnly
                       | QIODevice::Text)) {
        QTextStream in(&inputFile);
        while ( !in.atEnd() ) {
            QString line = in.readLine();
            QStringList list = line.split(DELIMITER);
            QString name = list.at(0).trimmed();
            QString link = list.at(1).trimmed();
            _texts.append(name);
            _hyperlinks.append(link);
        }
        inputFile.close();
    }

}
