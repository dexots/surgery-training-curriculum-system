#include "gui/catetable.h"

const char *CateTable::HOME_BUTTON = ":/icon/ButtonIcon/back.png";
const char *CateTable::HEAD_FONT_NAME = "Gotham Rounded Medium";
const int CateTable::HEAD_NAME_SIZE = 14;
const int CateTable::HEAD_DESC_SIZE = 11;
const int CateTable::SUB_CAT_COL_SIZE = 240;
const int CateTable::LAYOUT_SPACE = 2;

CateTable::CateTable(QString headerName,
                     QString headerDesc,
                     QString headerIcon,
                     QColor headerColor,
                     QVector <SubCate*> subCategories) {
    CateTable::initializeClass();
    CateTable::createHeader(headerName, headerDesc, headerIcon, headerColor);
    CateTable::createSubCatBar(subCategories);
    CateTable::addInCatCorWidget(subCategories);
    CateTable::setupTable();
}

CateTable::~CateTable() {
    delete _headIconLabel;
    delete _headNameLabel;
    delete _headDescLabel;
    delete _homeLabel;
    delete _headNameAndDesc;
    delete _headerLayout;
    delete _headerWidget;
    delete _subCateLayout;
    delete _botLayout;
    delete _tableLayout;
}

//private methods
void CateTable::initializeClass() {
    _headIconLabel = new QLabel;
    _headNameLabel = new QLabel;
    _headDescLabel = new QLabel;
    _headerLayout = new QHBoxLayout;
    _headNameAndDesc = new QVBoxLayout;
    _headerWidget = new QWidget;
    _subCateLayout = new QVBoxLayout;
    _botLayout = new QHBoxLayout;
    _tableLayout = new QVBoxLayout;
}

void CateTable::createHeader(QString headerName,
                             QString headerDesc,
                             QString headerIcon,
                             QColor headerColor) {
    CateTable::insHeaderTitle(headerName);
    CateTable::insHeaderDesc(headerDesc);
    CateTable::insHeaderLogo(headerIcon);
    CateTable::setupHomeButton();
    CateTable::createHeaderLayout();
    CateTable::createHeaderWidget(headerColor);
}

void CateTable::insHeaderTitle(QString headerName) {
    QFont nameLabelFont(HEAD_FONT_NAME, HEAD_NAME_SIZE, QFont::Bold);

    _headNameLabel->setText(headerName);
    _headNameLabel->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    _headNameLabel->setAlignment(Qt::AlignBottom | Qt::AlignRight);
    _headNameLabel->setFont(nameLabelFont);

}

void CateTable::insHeaderDesc(QString headerDesc) {
    QFont descLabelFont(HEAD_FONT_NAME, HEAD_DESC_SIZE, QFont::Normal);

    _headDescLabel->setText(headerDesc);
    _headDescLabel->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    _headDescLabel->setAlignment(Qt::AlignTop | Qt::AlignRight);
    _headDescLabel->setFont(descLabelFont);

}

void CateTable::insHeaderLogo(QString headerIcon) {
    QPixmap icon = CateTable::getPixmapBy(headerIcon.toStdString().c_str());

    _headIconLabel->setPixmap(icon);
    _headIconLabel->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Fixed);
}

void CateTable::setupHomeButton() {
    QPixmap home = CateTable::getPixmapBy(HOME_BUTTON);

    QLabel *homeLogo = new QLabel;
    homeLogo->setPixmap(home);
    homeLogo->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Fixed);
    _homeLabel = new ClickableWidget(homeLogo);
    _homeLabel->setFixedHeight(homeLogo->pixmap()->height() + 50);
    _homeLabel->setCursor(Qt::PointingHandCursor);

    connect(_homeLabel, SIGNAL(isClicked()), this, SLOT(handleHomeButton()));
}

void CateTable::createHeaderLayout() {
    _headNameAndDesc->addWidget(_headNameLabel);
    _headNameAndDesc->addWidget(_headDescLabel);

    _headerLayout->addWidget(_homeLabel);
    _headerLayout->addLayout(_headNameAndDesc);
    _headerLayout->addWidget(_headIconLabel);
}

void CateTable::createHeaderWidget(QColor headerColor) {
    _headerWidget->setLayout(_headerLayout);
    _headerWidget->setAutoFillBackground(true);
    CateTable::colorWidget(headerColor, _headerWidget);

    _headerWidget->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Fixed);
    _headerWidget->setFixedHeight(_homeLabel->height());
}

void CateTable::colorWidget(QColor headerColor, QWidget *widget) {
    QPalette pallete = widget->palette();
    pallete.setColor(widget->backgroundRole(), headerColor);
    widget->setPalette(pallete);
}

void CateTable::createSubCatBar(QVector <SubCate*> subCategories) {
    for (int i = 0; i < subCategories.size(); i++){
        subCategories.at(i)->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Expanding);
        subCategories.at(i)->setFixedWidth(SUB_CAT_COL_SIZE);

        _subCateLayout->addWidget(subCategories.at(i));
        _subCatWidget.append(subCategories.at(i));

        connect(subCategories.at(i), SIGNAL(isClicked()), this, SLOT(changeView()));
    }
    _subCateLayout->setSpacing(LAYOUT_SPACE);
    _botLayout->addLayout(_subCateLayout);
    _botLayout->setSpacing(LAYOUT_SPACE);
}

void CateTable::addInCatCorWidget(QVector <SubCate*> subCategories) {
    for (int i = 0; i < subCategories.size(); i++){
        QWidget *widget = subCategories.at(i)->getCorWidget();
        _botLayout->addWidget(widget);
    }

    subCategories.at(0)->showSelectedBackground();
    _prevCorWidgetIdx = 0;

    for (int i = 1; i < subCategories.size(); i++){
        subCategories.at(i)->getCorWidget()->hide();
    }
}

QPixmap CateTable::getPixmapBy(const char* pixmap) {
    QPixmap icon = QPixmap(QString::fromUtf8(pixmap));
    return icon;
}

void CateTable::setupTable() {
    _tableLayout->addWidget(_headerWidget);
    _tableLayout->addLayout(_botLayout);
    _tableLayout->setSpacing(LAYOUT_SPACE);
    this->setLayout(_tableLayout);
}

//private slots
void CateTable::changeView() {
    QWidget *buttonWidget = qobject_cast<QWidget*>(sender());
    if (!buttonWidget) {
        return;
    }

    int indexOfButton = _subCateLayout->indexOf(buttonWidget);

    if (indexOfButton == _prevCorWidgetIdx){
        return;
    }

    _subCatWidget.at(_prevCorWidgetIdx)->getCorWidget()->hide();
    _subCatWidget.at(indexOfButton)->getCorWidget()->show();

    _subCatWidget.at(_prevCorWidgetIdx)->showDefaultBackground();
    _subCatWidget.at(indexOfButton)->showSelectedBackground();

    _prevCorWidgetIdx = indexOfButton;
}

void CateTable::handleHomeButton() {
    emit this->backToHome();
}

