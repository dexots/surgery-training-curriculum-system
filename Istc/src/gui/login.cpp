#include "gui/login.h"

const char *Login::CONTENT_TYPE_HEADER = "application/x-www-form-urlencoded";

const QString Login::LOGIN_SUCCESS = "Success!";
const QString Login::LOGIN_FAIL = "Login Failed :(";
const QString Login::LOGIN_CANCEL = "Cancelled";

const QString Login::URL_LOGIN_SUCCESS = "id_res";
const QString Login::URL_LOGIN_CANCEL = "cancel";


Login::Login() {
    Login::initializeClass();
    Login::addDefaultParam();
}

Login::~Login() {
    Login::clearGarbage();
}

void Login::addParam(const char* field, const char* value) {
    _params->addQueryItem(field, value);
}

QUrl Login::getNusOpenIdUrl() {
    QUrl serverUrl = QUrl(_params->toString(QUrl::FullyEncoded));
    serverUrl.setQuery(serverUrl.query(QUrl::FullyEncoded), QUrl::TolerantMode);

    return serverUrl;
}

QString Login::checkLoginStatus(QUrl givenUrl) {
    //openid.mode should return id_res if successful
    //if return cancel, then is fail
    givenUrl.setQuery(givenUrl.query(QUrl::FullyDecoded), QUrl::DecodedMode);
    QString decodedUrl = givenUrl.toDisplayString(QUrl::PrettyDecoded);
    QUrlQuery *decodedUrlQuery = new QUrlQuery(decodedUrl);

    QString loginStatus = decodedUrlQuery->queryItemValue("openid.mode",
                                                          QUrl::FullyDecoded);

    if (loginStatus == NULL) {
        loginStatus = decodedUrlQuery->queryItemValue("https://bluebell.dl.comp.nus.edu.sg/~surgery?openid.mode",
                                                      QUrl::FullyDecoded);
    }

    if (loginStatus == NULL) {
        return LOGIN_FAIL;
    } else if (loginStatus.contains(URL_LOGIN_SUCCESS)) {
        return LOGIN_SUCCESS;
    } else if (loginStatus.contains(URL_LOGIN_CANCEL)){
        return LOGIN_CANCEL;
    }

    return LOGIN_FAIL;
}

//private functions:
void Login::initializeClass() {
    _networkManager = new QNetworkAccessManager;
    _params = new QUrlQuery;
}

void Login::clearGarbage() {
    delete _networkManager;
    delete _params;
}

void Login::addDefaultParam() {
    _params->addQueryItem("https://openid.nus.edu.sg/auth/index?openid.ns",
                          "http://specs.openid.net/auth/2.0");

    _params->addQueryItem("openid.mode",
                          "checkid_setup");

    //should return to our own server
    _params->addQueryItem("openid.return_to",
                          "https://bluebell.d1.comp.nus.edu.sg/~surgery");

    _params->addQueryItem("openid.realm",
                          "http://istc");

    _params->addQueryItem("openid.ns.ax",
                          "http://openid.net/srv/ax/1.0");

    _params->addQueryItem("openid.ax_mode",
                          "fetch_request");

    _params->addQueryItem("openid.ax_type_namePerson",
                          "http://axschema.org/namePerson");

    _params->addQueryItem("openid.ax_type_namePerson_friendly",
                          "http://axschema.org/namePerson/friendly");

    _params->addQueryItem("openid.ax_type_contact_email",
                          "http://axschema.org/contact/email");

    _params->addQueryItem("openid.ax_if_available",
                          "namePerson,namePerson_friendly,contact_email");

    _params->addQueryItem("openid.ns.sreg",
                          "http://openid.net/extensions/sreg/1.1");

    _params->addQueryItem("openid.sreg.optional",
                          "fullname,nickname,email");

    _params->addQueryItem("openid.claimed_id",
                          "http://specs.openid.net/auth/2.0/identifier_select");

    _params->addQueryItem("openid.identity",
                          "http://specs.openid.net/auth/2.0/identifier_select");

    _params->addQueryItem("controller",
                          "server");

    _params->addQueryItem("action",
                          "index");

    _params->addQueryItem("module",
                          "default");
}



QVector<QString> *Login::retrieveInfo(QUrl givenUrl) {
    //openid.sreg.nickname : matric num
    //openid.sreg.fullname : fullname (remove all the +)
    //openid.sreg.email : email
    givenUrl.setQuery(givenUrl.query(QUrl::FullyDecoded), QUrl::DecodedMode);
    QString decodedUrl = givenUrl.toDisplayString(QUrl::PrettyDecoded);
    QUrlQuery *decodedUrlQuery = new QUrlQuery(decodedUrl);

    QString matricNo = decodedUrlQuery->queryItemValue("openid.sreg.nickname",
                                                       QUrl::FullyDecoded);
    QString fullName = decodedUrlQuery->queryItemValue("openid.sreg.fullname",
                                                       QUrl::FullyDecoded);
    QString email = decodedUrlQuery->queryItemValue("openid.sreg.email",
                                                    QUrl::FullyDecoded);

    fullName.replace(QString("+"), QString(" "));

    QVector<QString> *userInfo = new QVector<QString>;
    userInfo->append(matricNo);
    userInfo->append(fullName);
    userInfo->append(email);

    //std::cerr<<"reach here, decoding\n"
    //         <<decodedUrl.toStdString()<<"\n"
    //         <<matricNo.toStdString()<<"\n"
    //         <<fullName.toStdString()<<"\n"
    //         <<email.toStdString()<<"\n";

    return userInfo;
}



