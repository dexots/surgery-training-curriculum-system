#include "gui/patientbox.h"

const QString PatientBox::COLOR_BUTTONBACKGROUND = "rgb(39, 174, 96)";
const QString PatientBox::COLOR_BUTTONBORDER = "white";
const QString PatientBox::COLOR_BUTTONFONT = "white";

const QString PatientBox::FONT_BUTTON = "bold 14";
const QString PatientBox::PADDING_BUTTON = "6";
const QString PatientBox::RADIUS_BUTTONBORDER = "10";
const QString PatientBox::STYLE_BUTTONBORDER = "outset";

const QString PatientBox::WIDTH_BUTTON = "50";
const QString PatientBox::WIDTH_BUTTONBORDER = "2";

const QString PatientBox::STRING_COLON = ":";
const QString PatientBox::STRING_EMPTY = "";
const QString PatientBox::STRING_SPACE = " ";

const QString PatientBox::INPUT_FILE = ":/text/LocalFile/procedurecode.txt";

PatientBox::PatientBox() {
    initializeBoxLayout();
    initializeQString();
    setUpPatientDetailsLabel();
    setUpIdAndDateInput();
    setUpDobAndTimeInput();
    setUpProcedureCodeInput();
    setUpOutcomeInput();
    setUpRemarksInput();
    setUpErrorLabel();
    setUpAddAndCancelButton();

    this->setLayout(_boxLayout);
    setPatientBoxAttribute();
}

PatientBox::PatientBox(QString patientID, QString procedureDate, QString patientDOB,
                       QString procedureTime, int procedureCodeIndex, QString procedureRemarks,
                       QString outcome) {
    initializeBoxLayout();
    initializeQString();
    setUpPatientDetailsLabel();
    setUpIdAndDateInput(patientID, procedureDate);
    setUpDobAndTimeInput(patientDOB, procedureTime);
    setUpProcedureCodeInput(procedureCodeIndex);
    setUpOutcomeInput(outcome);
    setUpRemarksInput(procedureRemarks);
    setUpErrorLabel();
    setUpUpdateAndCancelButton();

    this->setLayout(_boxLayout);
    setPatientBoxAttribute();
}

PatientBox::~PatientBox() {
    delete _patientDetails;
    delete _id;
    delete _inputId;
    delete _inputDobEdit;
    delete _date;
    delete _actualDate;
    delete _dob;
    delete _time;
    delete _actualTime;
    delete _codeLabel;
    delete _codeBox;
    delete _remarks;
    delete _inputRemarks;
    delete _errorLabel;
    delete _addButton;
    delete _boxLayout;
}

void PatientBox::paintEvent(QPaintEvent *event) {
    QDialog::paintEvent(event);
    QPainter painter(this);
    QBrush brush(Qt::white);
    painter.setBrush(brush);
    painter.setPen(Qt::white);
    painter.drawRoundRect(QRect(0, 0, this->width(), this->height()));
}

void PatientBox::setEditMode() {
    _inputId->setEnabled(false);
    _inputDobEdit->setEnabled(false);
    _actualDate->setEnabled(false);
    _actualTime->setEnabled(false);
    _codeBox->setEnabled(false);
    _outcomeBox->setEnabled(false);
}

void PatientBox::initializeBoxLayout() {
    _boxLayout = new QVBoxLayout;
}

void PatientBox::initializeQString() {
    _idString = STRING_EMPTY;
    _dateString = STRING_EMPTY;
    _dobString = STRING_EMPTY;
    _timeString = STRING_EMPTY;
    _codeString = STRING_EMPTY;
    _remarksString = STRING_EMPTY;
}

void PatientBox::setUpPatientDetailsLabel() {
    _patientDetails = new QLabel();
    _patientDetails->setText("Patient Details:");

    _boldAndUnderline.setUnderline(true);
    _boldAndUnderline.setBold(true);

    _patientDetails->setFont(_boldAndUnderline);

    _boxLayout->addWidget(_patientDetails);
}

void PatientBox::setUpIdAndDateInput(const QString &patientID, const QString &procedureDate) {
    //left
    _id = new QLabel;
    _id->setText("ID: ");

    _bold.setBold(true);
    _id->setFont(_bold);

    _inputId = new QTextEdit;
    _inputId->setText(patientID);
    _inputId->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    _inputId->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    _inputId->setFrameStyle(QFrame::NoFrame);
    _inputId->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
    _inputId->setFixedHeight(30);
    _inputId->setPlaceholderText("Patient's' ID.");

    connect(_inputId, SIGNAL(textChanged()),
            this, SLOT(handleLoginId()));

    /*
    connect(_inputId, SIGNAL(stringReady(QString)),
            this, SLOT(handleLoginId(QString)));
    */

    QHBoxLayout *idLayout = new QHBoxLayout;
    idLayout->addWidget(_id);
    idLayout->addWidget(_inputId);

    QWidget *idWidget = new QWidget;
    idWidget->setLayout(idLayout);

    //right
    _currentDate = QDate::currentDate();

    _date = new QLabel;
    _date->setText("Date: ");
    _date->setFont(_bold);

    _actualDate = new QDateEdit;
    _actualDate->setMinimumDate(QDate(2013,1,1));
    _actualDate->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
    _actualDate->setDisplayFormat("d MMM yyyy");
    _actualDate->setDate(_currentDate);

    updateInputDate(_actualDate, procedureDate);

    connect(_actualDate, SIGNAL(dateChanged(QDate)),
            this, SLOT(handleDate(QDate)));

    QHBoxLayout *dateLayout = new QHBoxLayout;
    dateLayout->addWidget(_date);
    dateLayout->addWidget(_actualDate);

    QWidget *dateWidget = new QWidget;
    dateWidget->setLayout(dateLayout);

    //combine the row two
    QHBoxLayout *rowTwoLayout = new QHBoxLayout;
    rowTwoLayout->addWidget(idWidget, 0, Qt::AlignLeft);
    rowTwoLayout->addWidget(dateWidget, 0, Qt::AlignRight);

    addToBoxLayout(rowTwoLayout);
}

void PatientBox::setUpDobAndTimeInput(const QString &patientDOB, const QString &procedureTime) {
    //left
    _dob = new QLabel;
    _dob->setText("DOB: ");
    _dob->setFont(_bold);

    _inputDobEdit = new QDateEdit;
    _inputDobEdit->setMinimumDate(QDate(1930,1,1));
    _inputDobEdit->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
    _inputDobEdit->setDisplayFormat("d MMM yyyy");
    _inputDobEdit->setDate(_currentDate);

    updateInputDate(_inputDobEdit, patientDOB);

    connect(_inputDobEdit, SIGNAL(dateChanged(QDate)),
            this, SLOT(handleDob(QDate)));

    QHBoxLayout *dobLayout = new QHBoxLayout;
    dobLayout->addWidget(_dob);
    dobLayout->addWidget(_inputDobEdit);

    //right
    _time = new QLabel;
    _time->setText("Time: ");
    _time->setFont(_bold);

    _currentTime = QTime::currentTime();
    _actualTime = new QTimeEdit(_currentTime);

    updateInputTime(_actualTime, procedureTime);

    connect(_inputDobEdit, SIGNAL(timeChanged(QTime)),
            this, SLOT(handleTime(QTime)));

    QHBoxLayout *timeLayout = new QHBoxLayout;
    timeLayout->addWidget(_time);
    timeLayout->addWidget(_actualTime);

    //combine row 3
    QHBoxLayout *rowThreeLayout = new QHBoxLayout;
    rowThreeLayout->addLayout(dobLayout);
    rowThreeLayout->addLayout(timeLayout);
    rowThreeLayout->setAlignment(dobLayout, Qt::AlignLeft);
    rowThreeLayout->setAlignment(timeLayout, Qt::AlignRight);

    addToBoxLayout(rowThreeLayout);
}


void PatientBox::setUpProcedureCodeInput(int procedureCodeIndex) {
    _codeBox = new QComboBox();
    createCodeItems();
    _codeBox->setCurrentIndex(procedureCodeIndex);

    _codeLabel = new QLabel;
    _codeLabel->setText("Procedure Code: ");
    _codeLabel->setFont(_bold);

    connect(_codeBox, SIGNAL(currentTextChanged(QString)),
            this, SLOT(handleCode(QString)));

    QHBoxLayout *codeLayout = new QHBoxLayout;
    codeLayout->addWidget(_codeLabel, 0);
    codeLayout->addWidget(_codeBox, 10);

    addToBoxLayout(codeLayout);
}

void PatientBox::setUpOutcomeInput() {
    _outcomeLabel = new QLabel("Outcome: ");
    _outcomeLabel->setFont(_bold);

    _outcomeBox = new QComboBox();
    _outcomeBox->addItem("Good");
    _outcomeBox->addItem("Bad");
    _outcomeBox->addItem("Alive");
    _outcomeBox->addItem("Dead");

    connect(_outcomeBox, SIGNAL(currentTextChanged(QString)),
            this, SLOT(handleOutcome(QString)));

    _outcomeLayout = new QHBoxLayout;
    _outcomeLayout->addWidget(_outcomeLabel);
    _outcomeLayout->addWidget(_outcomeBox);
    _outcomeLayout->addStretch();

    addToBoxLayout(_outcomeLayout);
}

void PatientBox::setUpOutcomeInput(QString outcome) {
    _outcomeLabel = new QLabel("Outcome: ");
    _outcomeLabel->setFont(_bold);

    _outcomeBox = new QComboBox();
    _outcomeBox->addItem(outcome);
    _outcomeBox->setEditable(false);

    connect(_outcomeBox, SIGNAL(currentTextChanged(QString)),
            this, SLOT(handleOutcome(QString)));

    _outcomeLayout = new QHBoxLayout;
    _outcomeLayout->addWidget(_outcomeLabel);
    _outcomeLayout->addWidget(_outcomeBox);
    _outcomeLayout->addStretch();

    addToBoxLayout(_outcomeLayout);
}

void PatientBox::setUpRemarksInput(const QString &procedureRemarks) {
    _remarks = new QLabel;
    _remarks->setText("Notes: ");
    _remarks->setFont(_bold);

    _inputRemarks = new InputBox;
    _inputRemarks->setText(procedureRemarks);
    _inputRemarks->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    _inputRemarks->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    _inputRemarks->setFrameStyle(QFrame::Box);
    _inputRemarks->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
    _inputRemarks->setFixedHeight(80);
    _inputRemarks->setPlaceholderText("Notes about the Procedure");

    connect(_inputRemarks, SIGNAL(stringReady(QString)),
            this, SLOT(handleRemarks(QString)));

    QHBoxLayout *remarksLayout = new QHBoxLayout;
    remarksLayout->addWidget(_remarks, 0);
    remarksLayout->addWidget(_inputRemarks, 10);

    addToBoxLayout(remarksLayout);
}

void PatientBox::setUpErrorLabel() {
    _errorLabel = new QLabel;
    _errorLabel->setFont(_bold);
    changeColor(Qt::red, Qt::white, _errorLabel);
    _errorLabel->hide();

    addToBoxLayout(_errorLabel);
}

void PatientBox::setUpAddAndCancelButton() {
    _addButton = new QPushButton;
    _addButton->setText("Add");
    _addButton->setCursor(Qt::PointingHandCursor);
    setButtonStyleSheet(_addButton);
    connect (_addButton, SIGNAL(clicked()),
             this, SLOT(handleAddOrUpdate()));
    _addButton->setSizePolicy(QSizePolicy::Fixed,
                              QSizePolicy::Fixed);

    //cancel
    _cancelButton = new QPushButton;
    _cancelButton->setText("Cancel");
    _cancelButton->setCursor(Qt::PointingHandCursor);
    setButtonStyleSheet(_cancelButton);
    connect (_cancelButton, SIGNAL(clicked()),
             this, SLOT(handleCancel()));
    _cancelButton->setSizePolicy(QSizePolicy::Fixed,
                                 QSizePolicy::Fixed);

    //button layout
    _buttonLayout = new QHBoxLayout;
    _buttonLayout->addWidget(_addButton);
    _buttonLayout->addWidget(_cancelButton);
    _buttonLayout->setSpacing(0);

    addToBoxLayout(_buttonLayout);
}

void PatientBox::setUpUpdateAndCancelButton() {
    _updateButton = new QPushButton;
    _updateButton->setText("Update");
    _updateButton->setCursor(Qt::PointingHandCursor);
    setButtonStyleSheet(_updateButton);
    connect (_updateButton, SIGNAL(clicked()),
             this, SLOT(handleAddOrUpdate()));
    _updateButton->setSizePolicy(QSizePolicy::Fixed,
                              QSizePolicy::Fixed);

    //cancel
    _cancelButton = new QPushButton;
    _cancelButton->setText("Cancel");
    _cancelButton->setCursor(Qt::PointingHandCursor);
    PatientBox::setButtonStyleSheet(_cancelButton);
    connect (_cancelButton, SIGNAL(clicked()),
             this, SLOT(handleCancel()));
    _cancelButton->setSizePolicy(QSizePolicy::Fixed,
                                 QSizePolicy::Fixed);

    //button layout
    _buttonLayout = new QHBoxLayout;
    _buttonLayout->addWidget(_updateButton);
    _buttonLayout->addWidget(_cancelButton);
    _buttonLayout->setSpacing(0);

    addToBoxLayout(_buttonLayout);
}

void PatientBox::setPatientBoxAttribute() {
    this->setContentsMargins(10, 10, 10, 10);

    this->setAttribute(Qt::WA_TranslucentBackground, true);

    this->setWindowFlags(Qt::FramelessWindowHint);
}

void PatientBox::addToBoxLayout(QHBoxLayout *toBeAddedLayout) {
    _boxLayout->addLayout(toBeAddedLayout);
}

void PatientBox::addToBoxLayout(QWidget *toBeAddedWidget) {
    _boxLayout->addWidget(toBeAddedWidget);
}

void PatientBox::updateInputDate(QDateEdit *dateEdit, QString newDate) {
    if (!newDate.isEmpty()) {
        qDebug() << "PatientBox -> updateInputDate: Setting previously added procedure date";
        try {
            QStringList date = newDate.split(STRING_SPACE);
            int year = date.at(0).toInt();
            int month = date.at(1).toInt();
            int day = date.at(2).toInt();
            dateEdit->setDate(QDate(year, month, day));
            qDebug() << "PatientBox -> updateInputDate: Finished setting previously added procedure date";
        }
        catch (int e) {
            qDebug() << "PatientBox -> updateInputDate: Error setting date";
        }
    }
}

void PatientBox::updateInputTime(QTimeEdit *timeEdit, QString newTime) {
    if (!newTime.isEmpty()) {
        //input format as e.g. 18:00
        qDebug() << "PatientBox -> updateInputTime: Setting previously added procedure time";
        try {
            QStringList time = newTime.split(STRING_COLON);
            int hour = time.at(0).toInt();
            int minute = time.at(1).toInt();
            timeEdit->setTime(QTime(hour, minute));
            qDebug() << "PatientBox -> updateInputTime: Finished setting previously added procedure time";
        }
        catch (int e){
            qDebug() << "PatientBox -> updateInputTime: Error setting time";
        }
    }
}

void PatientBox::changeColor(QColor font, QColor back, QWidget *widget) {
    widget->setAutoFillBackground(true);
    QPalette pallete = widget->palette();
    pallete.setColor(widget->foregroundRole(), font);
    pallete.setColor(widget->backgroundRole(), back);
    widget->setPalette(pallete);
}

void PatientBox::setButtonStyleSheet(QPushButton *button) {
    button->setStyleSheet("background-color:" + COLOR_BUTTONBACKGROUND + ";"
                          "border-style:" + STYLE_BUTTONBORDER + ";"
                          "border-width:" + WIDTH_BUTTONBORDER + "px;"
                          "border-radius:" + RADIUS_BUTTONBORDER + "px;"
                          "border-color:" + COLOR_BUTTONBORDER + ";"
                          "font:" + FONT_BUTTON + "px;"
                          "color:" + COLOR_BUTTONFONT + ";"
                          "min-width:" + WIDTH_BUTTON + "px;"
                          "padding:" + PADDING_BUTTON + "px;");
}

//private slots
void PatientBox::handleLoginId() {
    _idString = _inputId->toPlainText();
    int maxChar = 10;

    if (_idString.length() > maxChar) {
        _idString.chop(_idString.length() - maxChar);
        _inputId->setPlainText(_idString);

        // Set the cursor to the back of line
        QTextCursor cursor = _inputId->textCursor();
        cursor.setPosition(_inputId->document()->characterCount() - 1);
        _inputId->setTextCursor(cursor);
    }

}

void PatientBox::handleDate(QDate inputString) {
    _dateString = inputString.toString("dd MMM yyyy");
}

void PatientBox::handleDob(QDate inputString) {
    _dobString = inputString.toString("dd MMM yyyy");
}

void PatientBox::handleTime(QTime inputString) {
    _timeString = inputString.toString();
}

void PatientBox::handleCode(QString inputString) {
    _codeString = inputString;
}

void PatientBox::handleRemarks(QString inputString) {
    _remarksString = inputString;
    _inputRemarks->setPlainText(_remarksString);
}

void PatientBox::handleOutcome(QString inputString) {
    _outcomeString = inputString;
}

void PatientBox::handleAddOrUpdate() {
    QVector<QString> info;

    _idString = _inputId->toPlainText();
    _remarksString = _inputRemarks->toPlainText();
    _dobString = _inputDobEdit->date().toString("yyyy M d");
    _codeString = _codeBox->currentText();
    _outcomeString = _outcomeBox->currentText();
    _dateString = _actualDate->date().toString("yyyy M d");
    _timeString = _actualTime->time().toString("h:m");

    if (_idString == STRING_EMPTY && _dobString == STRING_EMPTY) {
        _errorLabel->show();
        _errorLabel->setText("Please key in patient ID and patient's DOB");
    } else if (_idString == STRING_EMPTY) {
        _errorLabel->show();
        _errorLabel->setText("Please key in patient ID.");
    } else if (_dobString == STRING_EMPTY) {
        _errorLabel->show();
        _errorLabel->setText("Please key in patient's DOB");
    } else {
        info.append(_dateString);
        info.append(_timeString);
        info.append(_idString);
        info.append(_dobString);
        info.append(QString::number(_codes.indexOf(_codeString)));
        info.append(_outcomeString);
        info.append(_remarksString);

        emit this->passInfo(info);
        QDialog::done(0);
    }
}

void PatientBox::handleCancel() {
    QDialog::done(0);
}

void PatientBox::createCodeItems() {
    readCodeFile();
    for (int i = 0; i < _codes.size(); i++){
        _codeBox->addItem(_codes.at(i));
    }
}

void PatientBox::readCodeFile() {
    QFile inputFile(INPUT_FILE);

    if (inputFile.open(QIODevice::ReadOnly
                       | QIODevice::Text)) {
        QTextStream in(&inputFile);
        while ( !in.atEnd() ) {
            QString codeName = in.readLine();
            _codes.append(codeName);
        }
        inputFile.close();
    }
}
