#include "gui/assessmentbox.h"

const QString AssessmentBox::COLOR_BUTTONBACKGROUND = "rgb(39, 174, 96)";
const QString AssessmentBox::COLOR_BUTTONBORDER = "white";
const QString AssessmentBox::COLOR_BUTTONFONT = "white";
const QString AssessmentBox::FONT_BUTTON = "bold 14";
const QString AssessmentBox::PADDING_BUTTON = "6";
const QString AssessmentBox::RADIUS_BUTTONBORDER = "10";
const QString AssessmentBox::STYLE_BUTTONBORDER = "outset";
const QString AssessmentBox::WIDTH_BUTTON = "50";
const QString AssessmentBox::WIDTH_BUTTONBORDER = "2";
const QString AssessmentBox::QSTRING_EMPTY = "";
const QString AssessmentBox::QSTRING_NEWLINE = "\n";
const QString AssessmentBox::HTML_ALERT = "<font color=\"DeepPink\">";
const QString AssessmentBox::HTML_NORMAL = "<font color=\"Black\">";

const QString AssessmentBox::MESSAGE_INVALIDMATRICNO = "Please key in valid trainee matric number";
const QString AssessmentBox::MESSAGE_MISSINGMATRICNO = "Please key in trainee matric number";
const QString AssessmentBox::MESSAGE_MISSINGMODULETITLE = "Please key in module title";

const int AssessmentBox::WIDTH_MINIMUM = 500;
const int AssessmentBox::HEIGHT_MINIMUM = 300;

AssessmentBox::AssessmentBox() {
    initialiseVariables();
    setUpDateLayout();
    setUpMatricLayout();
    setUpGradeLayout();
    setUpCommentsLayout();
    setUpErrorLabel();
    setUpAddCancelButton();
    setUpLayout();
}

AssessmentBox::~AssessmentBox() {
    delete _errorLabel;
    delete _dateLabel;
    delete _matricLabel;
    delete _moduleLabel;
    delete _gradeLabel;
    delete _commentsLabel;

    delete _gradesBox;

    delete _inputComments;
    delete _inputMatric;
    delete _inputModule;

    delete _matricLayout;
    delete _gradeLayout;
    delete _commentsLayout;
    delete _buttonLayout;
    delete _mainLayout;
}

void AssessmentBox::initialiseVariables() {
    _errorLabel = new QLabel;
    _dateLabel = new QLabel;
    _matricLabel = new QLabel;
    _moduleLabel = new QLabel;
    _gradeLabel = new QLabel;
    _commentsLabel = new QLabel;

    _dateString = QSTRING_EMPTY;
    _commentsString = QSTRING_EMPTY;
    _moduleString = QSTRING_EMPTY;
    _matricString = QSTRING_EMPTY;

    _gradesBox = new QComboBox;
    _date = QDate::currentDate();

    _inputComments = new InputBox;
    _inputMatric = new InputBox;
    _inputModule = new InputBox;

    _matricLayout = new QHBoxLayout;
    _gradeLayout = new QVBoxLayout;
    _commentsLayout = new QVBoxLayout;
    _buttonLayout = new QHBoxLayout;
    _mainLayout = new QVBoxLayout;

    _boldFont.setBold(true);
}

void AssessmentBox::setUpDateLayout() {
    _dateString = _date.toString("dd MMM yyyy");
    _dateLabel->setText(_dateString);
    _dateLabel->setFont(_boldFont);
}

void AssessmentBox::setUpErrorLabel() {
    _errorLabel->setFont(_boldFont);
    AssessmentBox::changeColor(Qt::red, Qt::white, _errorLabel);
    _errorLabel->hide();
}

void AssessmentBox::setUpMatricLayout() {
    _matricLabel->setText("Trainee's Matric Number:");
    _matricLabel->setFont(_boldFont);

    _inputMatric->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    _inputMatric->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    _inputMatric->setFrameStyle(QFrame::Box);
    _inputMatric->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
    _inputMatric->setFixedHeight(25);
    _inputMatric->setFixedWidth(250);
    _inputMatric->setPlaceholderText("Matric Number:");
    _inputMatric->setLineWrapMode(InputBox::WidgetWidth);

    connect(_inputMatric, SIGNAL(stringReady(QString)), this, SLOT(handleMatric(QString)));

    //QWidget *widget = new QWidget();

    _matricLayout->addWidget(_matricLabel);
    _matricLayout->addWidget(_inputMatric);
    //_matricLayout->addWidget(widget);
}

void AssessmentBox::setUpGradeLayout() {
    _moduleLabel->setText("Module Title: ");
    _moduleLabel->setFont(_boldFont);

    _inputModule->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    _inputModule->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    _inputModule->setFrameStyle(QFrame::Box);
    _inputModule->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
    _inputModule->setFixedHeight(25);
    _inputModule->setFixedWidth(250);
    _inputModule->setPlaceholderText("Basic Medical Knowledge");
    _inputModule->setLineWrapMode(InputBox::WidgetWidth);

    connect(_inputModule, SIGNAL(stringReady(QString)), this, SLOT(handleModule(QString)));

    //QWidget *widget = new QWidget;

    QHBoxLayout *moduleLayout = new QHBoxLayout;
    moduleLayout->addWidget(_moduleLabel);
    moduleLayout->addWidget(_inputModule);
    //moduleLayout->addWidget(widget);

    _gradeLabel->setText("Grade: ");
    _gradeLabel->setFont(_boldFont);

    QStringList list;
    list << "A" << "A-" << "B+" << "B" << "B-" << "C+" << "C" << "C-"
         << "D" << "F";
    _gradesBox->addItems(list);

    QHBoxLayout *layout = new QHBoxLayout;
    layout->addWidget(_gradeLabel);
    layout->addWidget(_gradesBox);
    //layout->addWidget(widget);

    _gradeLayout->addLayout(moduleLayout);
    _gradeLayout->addLayout(layout);
}

void AssessmentBox::setUpCommentsLayout() {
    _commentsLabel->setText("Comments:");
    _commentsLabel->setFont(_boldFont);

    _inputComments->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    _inputComments->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    _inputComments->setFrameStyle(QFrame::Box);
    _inputComments->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
    _inputComments->setFixedHeight(200);
    _inputComments->setFixedWidth(WIDTH_MINIMUM);
    _inputComments->setPlaceholderText("Comments about Trainee's Performance");
    _inputComments->setLineWrapMode(InputBox::WidgetWidth);

    connect(_inputComments, SIGNAL(stringReady(QString)), this, SLOT(handleComments(QString)));

    _commentsLayout->addWidget(_commentsLabel);
    _commentsLayout->addWidget(_inputComments);
}

void AssessmentBox::setUpAddCancelButton() {
    //Add button
    _addButton = new QPushButton;
    _addButton->setText("Add");
    _addButton->setCursor(Qt::PointingHandCursor);
    setButtonStyleSheet(_addButton);
    connect (_addButton, SIGNAL(clicked()),
             this, SLOT(handleAddOrUpdate()));
    _addButton->setSizePolicy(QSizePolicy::Fixed,
                              QSizePolicy::Fixed);

    //cancel
    _cancelButton = new QPushButton;
    _cancelButton->setText("Cancel");
    _cancelButton->setCursor(Qt::PointingHandCursor);
    setButtonStyleSheet(_cancelButton);
    connect (_cancelButton, SIGNAL(clicked()),
             this, SLOT(handleCancel()));
    _cancelButton->setSizePolicy(QSizePolicy::Fixed,
                                 QSizePolicy::Fixed);

    _buttonLayout->addWidget(_addButton);
    _buttonLayout->addWidget(_cancelButton);
    _buttonLayout->setSpacing(0);
}

void AssessmentBox::setUpLayout() {
    _mainLayout->addWidget(_dateLabel);
    _mainLayout->addLayout(_matricLayout);
    _mainLayout->addLayout(_gradeLayout);
    _mainLayout->addLayout(_commentsLayout);
    _mainLayout->addWidget(_errorLabel);
    _mainLayout->addLayout(_buttonLayout);

    this->setLayout(_mainLayout);
    this->setContentsMargins(10, 10, 10, 10);
    this->setMinimumWidth(WIDTH_MINIMUM);
    this->setMinimumHeight(HEIGHT_MINIMUM);

    this->setAttribute(Qt::WA_TranslucentBackground, true);
    this->setWindowFlags(Qt::FramelessWindowHint);
}

/*
 * Helper Methods
 */
void AssessmentBox::paintEvent(QPaintEvent *event) {
    QDialog::paintEvent(event);
    QPainter painter(this);
    QBrush brush(Qt::white);
    painter.setBrush(brush);
    painter.setPen(Qt::white);
    painter.drawRoundRect(QRect(0, 0, this->width(), this->height()));
}

void AssessmentBox::changeColor(QColor font, QColor back, QWidget *widget) {
    widget->setAutoFillBackground(true);
    QPalette pallete = widget->palette();
    pallete.setColor(widget->foregroundRole(), font);
    pallete.setColor(widget->backgroundRole(), back);
    widget->setPalette(pallete);
}

void AssessmentBox::setButtonStyleSheet(QPushButton *button) {
    button->setStyleSheet("background-color:" + COLOR_BUTTONBACKGROUND + ";"
                          "border-style:" + STYLE_BUTTONBORDER + ";"
                          "border-width:" + WIDTH_BUTTONBORDER + "px;"
                          "border-radius:" + RADIUS_BUTTONBORDER + "px;"
                          "border-color:" + COLOR_BUTTONBORDER + ";"
                          "font:" + FONT_BUTTON + "px;"
                          "color:" + COLOR_BUTTONFONT + ";"
                          "min-width:" + WIDTH_BUTTON + "px;"
                          "padding:" + PADDING_BUTTON + "px;");
}

void AssessmentBox::showMessageIfEmptyInput(QString input, QString errorMessage) {
    if (input == QSTRING_EMPTY) {
        _errorLabel->setText(_errorLabel->text() + QSTRING_NEWLINE +
                             errorMessage);
        _errorLabel->show();
    } else {
        //do nothing
    }
}

void AssessmentBox::showMessageIfInvalidMatricNo() {
     LocalDatabaseClient *localDbClient = LocalDatabaseClient::getObject();
     qDebug() << "AssessmentBox -> showMessageIfInvalidMatricNo : matric no is "
              << _matricString;
     if (!localDbClient->isValidUser(_matricString, UserDetails::ROLE_TRAINEE)) {
         qDebug() << "AssessmentBox -> showMessageIfInvalidMatricNo : invalid user";
         _errorLabel->setText(_errorLabel->text() + QSTRING_NEWLINE +
                              MESSAGE_INVALIDMATRICNO);
         _errorLabel->show();
     } else {
         // do nothing
     }
}

/*
 * Private slots
 */

void AssessmentBox::handleAddOrUpdate() {
    QVector<QString> info;

    _matricString = _inputMatric->toPlainText();
    _commentsString = _inputComments->toPlainText();
    _moduleString = _inputModule->toPlainText();
    QString grade = _gradesBox->currentText();

    _errorLabel->setText(QSTRING_EMPTY);
    showMessageIfEmptyInput(_matricString, MESSAGE_MISSINGMATRICNO);
    showMessageIfEmptyInput(_moduleString, MESSAGE_MISSINGMODULETITLE);
    showMessageIfInvalidMatricNo();

    if (_errorLabel->text() == QSTRING_EMPTY) {
        info.append(_matricString.toUpper());
        info.append(_moduleString);
        info.append(grade);
        info.append(_commentsString);

        emit this->passInfo(_date, info);
        QDialog::done(0);
    }

}

void AssessmentBox::handleCancel() {
    QDialog::done(0);
}

void AssessmentBox::handleMatric(QString inputString) {
    _matricString = inputString;
    LocalDatabaseClient *localDbClient = LocalDatabaseClient::getObject();
    qDebug() << "AssessmentBox -> handleMatric : matric no is "
             << _matricString;
    if (!localDbClient->isValidUser(_matricString, UserDetails::ROLE_TRAINEE)) {
        _inputMatric->setText(HTML_ALERT + inputString);
    } else {
        _inputMatric->setText(HTML_NORMAL + inputString);
    }
}

void AssessmentBox::handleModule(QString inputString) {
    _inputModule->setText(inputString);
}

void AssessmentBox::handleComments(QString inputString) {
    _inputComments->setText(inputString);
}
