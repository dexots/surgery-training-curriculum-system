#include "gui/formswidget.h"

const QVector<int> FormsWidget::WIDTH_COLUMN (QVector<int>() << 320 << 160 << 160 << 160);

const QVector<QString> FormsWidget::HEADER_TABLE1 (QVector<QString>() << "Form Name"
                                                   << "Date Released" << "Deadline" << "Status");

const QVector<QString> FormsWidget::HEADER_TABLE2 (QVector<QString>() << "Form Name"
                                                   << "Date Released" << "Deadline" << "Download");
const int FormsWidget::HEIGHT_ROW = 70;
const int FormsWidget::NUMBER_COLUMN = 4;

const QColor FormsWidget::COLOR_CONTENTBACKGROUND = QColor(235, 241, 233);
const QColor FormsWidget::COLOR_CONTENTFONT = QColor(Qt::black);
const QColor FormsWidget::COLOR_HEADERBACKGROUND = QColor(112, 173, 71);
const QColor FormsWidget::COLOR_HEADERTEXT = QColor(Qt::white);
const QColor FormsWidget::COLOR_SUBMITTEDCONTENTFONT = QColor(Qt::gray);

const QString FormsWidget::ICON_DOWNLOAD = ":/icon/ButtonIcon/download.png";
const QString FormsWidget::ICON_DOWNLOADED = ":/icon/ButtonIcon/downloaded.png";


FormsWidget::FormsWidget() {
    setUpFormsLayout();
    setUpOnlineFormTable();
    setUpSoftcopyFormTable();
    setUpDummyWidget();
    setLayout(_formsLayout);
}

FormsWidget::~FormsWidget() {
    delete _formsLayout;
}

void FormsWidget::setUpFormsLayout() {
    _formsLayout = new QVBoxLayout;
    _formsLayout->setAlignment(Qt::AlignCenter | Qt::AlignTop);
}

void FormsWidget::setUpOnlineFormTable() {
    QLabel *onlineFormLabel = new QLabel("<h3><b>Online Forms:</b></h3>");
    _formsLayout->addWidget(onlineFormLabel, 0);

    TableWidget *table = new TableWidget(true, NUMBER_COLUMN, WIDTH_COLUMN, HEIGHT_ROW);

    table->setHeader(HEADER_TABLE1, COLOR_HEADERTEXT, COLOR_HEADERBACKGROUND);

    table->setContBackground(COLOR_CONTENTBACKGROUND);
    table->setContFont(COLOR_CONTENTFONT);

    TrainingModel *trainingModel = TrainingModel::getObject();
    QVector<Form*> onlineForms = trainingModel->getOnlineForms();

    qDebug() << "FormsWidget -> setUpOnlineFormTable: "
                "setuping datas...";

    for(int i = 0; i < onlineForms.size(); i ++) {
        Form *currentOnlineForm = onlineForms.value(i);

        QVector<QString> contentOfARow = QVector<QString>()
                                        << currentOnlineForm->getFormName()
                                        << currentOnlineForm->getReleaseDate().toString("d MMM yyyy")
                                        << currentOnlineForm->getDeadline().toString("d MMM yyyy")
                                        << currentOnlineForm->getOnlineFormStatus();
        table->addRow(contentOfARow);

        ClickableWidget *clickableFormName = currentOnlineForm->getClickableTitle();
        connect (currentOnlineForm, SIGNAL(loadWebpage(QString)),
                 this, SLOT(handleLoadWebpage(QString)));
        table->addCell(clickableFormName, i, 0);
    }

    _formsLayout->addWidget(table, 2);
}

void FormsWidget::addRowsToTable()
{
    TrainingModel *trainingModel = TrainingModel::getObject();
    QVector<Form*> softcopyForms = trainingModel->getSoftcopyForms();

    for(int i = 0; i < softcopyForms.size(); i ++) {
        Form *currentSoftcopyForm = softcopyForms.value(i);

        QVector<QString> contentOfARow = QVector<QString>()
                                        << currentSoftcopyForm->getFormName()
                                        << currentSoftcopyForm->getReleaseDate().toString("d MMM yyyy")
                                        << currentSoftcopyForm->getDeadline().toString("d MMM yyyy")
                                        << "";
        _formsTable->addRow(contentOfARow);

        QPixmap downloadIcon = QPixmap(ICON_DOWNLOAD);
        QLabel *downloadIconLabel = new QLabel();
        downloadIconLabel->setPixmap(downloadIcon);
        downloadIconLabel->setAlignment(Qt::AlignCenter);
        ClickableWidget *clickableDownload = new ClickableWidget(downloadIconLabel);

        connect(clickableDownload, SIGNAL(isClicked()), this, SLOT(downloadFile()));

        _formsTable->addCell(clickableDownload, i, 3);
    }
}

void FormsWidget::setUpSoftcopyFormTable() {
    QLabel *hardcopyFormLabel = new QLabel("<h3><b>Softcopy Forms:</b></h3>");
    _formsLayout->addSpacing(20);
    _formsLayout->addWidget(hardcopyFormLabel, 0);

    _formsTable = new TableWidget(true, NUMBER_COLUMN, WIDTH_COLUMN, HEIGHT_ROW);

    _formsTable->setHeader(HEADER_TABLE1, COLOR_HEADERTEXT, COLOR_HEADERBACKGROUND);

    _formsTable->setContBackground(COLOR_CONTENTBACKGROUND);
    _formsTable->setContFont(COLOR_CONTENTFONT);

    addRowsToTable();

    _formsLayout->addWidget(_formsTable, 2);
}

void FormsWidget::setUpDummyWidget() {
    QWidget *dummyWidget = new QWidget();
    dummyWidget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    _formsLayout->addWidget(dummyWidget, 10);
}

// TODO: Dynamically download file
bool FormsWidget::downloadFile() {
    QString downloadFolder = QStandardPaths::writableLocation(QStandardPaths::DownloadLocation);
    downloadFolder.append("/form.pdf");

    QString saveFileName = QFileDialog::getSaveFileName(this, tr("Save Form"),
                           downloadFolder);

    QString formLink = ":/form/LocalFile/form.pdf";
    QFile::copy(formLink, saveFileName);
    if (isFileExists(saveFileName.toStdString())) {
        downloadSuccess(saveFileName);
    } else {
        downloadFailure();
    }

    return true;
}

void FormsWidget::handleLoadWebpage(QString urlString) {
    qDebug() << "FormsWidget -> handleLoadWebpage : Initializing browser";
    //this->hide();
    QUrl url = QUrl(urlString);
    Browser *browser = new Browser();
    browser->load(url);
    browser->show();
    delete browser;
    qDebug() << "FormsWidget -> handleLoadWebpage : Initialized browser";
}

bool FormsWidget::isFileExists (const std::string& fileName) {
    struct stat buffer;
    return (stat (fileName.c_str(), &buffer) == 0);
}

void FormsWidget::downloadSuccess(QString saveFileName) {
    GeneralPopupBox *successBox = new GeneralPopupBox(saveFileName, 2);
    successBox->exec();
}

void FormsWidget::downloadFailure() {
    ErrorBox *errorBox = new ErrorBox("DOWNLOAD");
    errorBox->exec();
}

/*
 * Public Slots
 */
void FormsWidget::updateTable() {
    assert(_formsTable);
    _formsTable->deleteAll();
    addRowsToTable();
}
