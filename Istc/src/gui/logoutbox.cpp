#include "gui/logoutbox.h"

const QString LogoutBox::STRING_EXIT = "Are you sure you want to logout?";
const QString LogoutBox::STRING_YES = "YES";
const QString LogoutBox::STRING_NO = "NO";

const QString LogoutBox::COLOR_BUTTONGREENBACKGROUND = "rgb(39, 174, 96)";
const QString LogoutBox::COLOR_BUTTONREDBACKGROUND = "rgb(231,76,60)";
const QString LogoutBox::COLOR_BUTTONBORDER = "white";
const QString LogoutBox::COLOR_BUTTONFONT = "white";

const QString LogoutBox::FONT_BUTTON = "bold 14";
const QString LogoutBox::PADDING_BUTTON = "6";
const QString LogoutBox::RADIUS_BUTTONBORDER = "10";
const QString LogoutBox::STYLE_BUTTONBORDER = "outset";

const QString LogoutBox::WIDTH_BUTTON = "50";
const QString LogoutBox::WIDTH_BUTTONBORDER = "2";

const int LogoutBox::WIDTH_MINIMUM = 300;
const int LogoutBox::HEIGHT_MINIMUM = 150;

LogoutBox::LogoutBox() {
    _bold.setBold(true);

    _exitLabel = new QLabel;

    _exitLabel->setText(STRING_EXIT);
    _exitLabel->setFont(_bold);

    //OK Button
    _okButton = new QPushButton;
    _okButton->setText(STRING_YES);
    _okButton->setMaximumWidth(WIDTH_BUTTON.toInt());
    LogoutBox::setButtonStyleSheet(_okButton, COLOR_BUTTONGREENBACKGROUND);
    _okButton->setCursor(Qt::PointingHandCursor);
    connect (_okButton, SIGNAL(clicked()),
             QApplication::instance(), SLOT(quit()));

    //No Button
    _cancelButton = new QPushButton;
    _cancelButton->setText(STRING_NO);
    _cancelButton->setMaximumWidth(WIDTH_BUTTON.toInt());
    LogoutBox::setButtonStyleSheet(_cancelButton, COLOR_BUTTONREDBACKGROUND);
    _cancelButton->setCursor(Qt::PointingHandCursor);
    connect (_cancelButton, SIGNAL(clicked()),
             this, SLOT(handleCancel()));

    QHBoxLayout *buttonLayout = new QHBoxLayout;
    buttonLayout->addWidget(_okButton);
    buttonLayout->addWidget(_cancelButton);

    QVBoxLayout *boxLayout = new QVBoxLayout;
    boxLayout->addWidget(_exitLabel);
    boxLayout->addLayout(buttonLayout);

    this->setLayout(boxLayout);
    this->setContentsMargins(10, 10, 10, 10);
    this->setMinimumWidth(WIDTH_MINIMUM);
    this->setMinimumHeight(HEIGHT_MINIMUM);
    this->setAttribute(Qt::WA_TranslucentBackground, true);
    this->setWindowFlags(Qt::FramelessWindowHint);

}

LogoutBox::~LogoutBox() {
    delete _exitLabel;
    delete _okButton;
    delete _cancelButton;
}

//Private slots
void LogoutBox::handleCancel() {
    QDialog::done(0);
}

void LogoutBox::paintEvent(QPaintEvent *event) {
    QDialog::paintEvent(event);
    QPainter painter(this);
    QBrush brush(Qt::white);
    painter.setBrush(brush);
    painter.setPen(Qt::white);
    painter.drawRoundRect(QRect(0, 0, this->width(), this->height()));
}

void LogoutBox::setButtonStyleSheet(QPushButton *button, QString buttonColour){
    button->setStyleSheet("background-color:" + buttonColour + ";"
                          "border-style:" + STYLE_BUTTONBORDER + ";"
                          "border-width:" + WIDTH_BUTTONBORDER + "px;"
                          "border-radius:" + RADIUS_BUTTONBORDER + "px;"
                          "border-color:" + COLOR_BUTTONBORDER + ";"
                          "font:" + FONT_BUTTON + "px;"
                          "color:" + COLOR_BUTTONFONT + ";"
                          "min-width:" + WIDTH_BUTTON + "px;"
                          "padding:" + PADDING_BUTTON + "px;");
}
