#include "gui/tablewidget.h"

const int TableWidget::DEFAULT_HEADER_HEIGHT = 50;
const double TableWidget::ENLARGE_FACTOR = 3;

TableWidget::TableWidget(bool hasHeader,
                         int noOfCol,
                         QVector <int> widthOfCol) {
    TableWidget::initializeClass(hasHeader, noOfCol, widthOfCol);
    TableWidget::setupGridLayout();
    TableWidget::setupScrollArea();
    TableWidget::setupLayout();
    TableWidget::setupYourself();
}

TableWidget::TableWidget(bool hasHeader,
                         int noOfCol,
                         QVector <int> widthOfCol,
                         int rowHeight) {
    TableWidget::initializeClass(hasHeader, noOfCol, widthOfCol);
    TableWidget::setFixedRowHeight(rowHeight);
    TableWidget::setupGridLayout();
    TableWidget::setupScrollArea();
    TableWidget::setupLayout();
    TableWidget::setupYourself();
}

TableWidget::TableWidget(bool hasHeader,
                         int noOfCol,
                         QVector <int> widthOfCol,
                         QWidget *parent) :
    QWidget(parent) {
    TableWidget::initializeClass(hasHeader, noOfCol, widthOfCol);
    TableWidget::setupGridLayout();
    TableWidget::setupScrollArea();
    TableWidget::setupLayout();
    TableWidget::setupYourself();
}

TableWidget::TableWidget(bool hasHeader,
                         int noOfCol,
                         QVector <int> widthOfCol,
                         int rowHeight,
                         QWidget *parent) :
    QWidget(parent) {
    TableWidget::initializeClass(hasHeader, noOfCol, widthOfCol);
    TableWidget::setFixedRowHeight(rowHeight);
    TableWidget::setupGridLayout();
    TableWidget::setupScrollArea();
    TableWidget::setupLayout();
    TableWidget::setupYourself();
}

TableWidget::~TableWidget() {
    TableWidget::clearGarbage();
}

//public methods
void TableWidget::setHeader(QVector <QString> headerNames,
                            QColor font,
                            QColor background) {
    TableWidget::setHeader(headerNames,
                           font,
                           background,
                           DEFAULT_HEADER_HEIGHT);
}

void TableWidget::setHeader(QVector <QString> headerNames,
                            QColor font,
                            QColor background,
                            int headerHeight) {
    TableWidget::setupHeader(headerNames,
                             font,
                             background,
                             headerHeight);
}

void TableWidget::setContBackground(QColor defaultBackground) {
    qDebug() << "TableWidget -> setContBackground: "
                "start setting content background...";

    _deContBack = defaultBackground;

    qDebug() << "TableWidget -> setContBackground: "
                "done setting content background...";
}

void TableWidget::setContFont(QColor defaultFont) {
    qDebug() << "TableWidget -> setContFont: "
                "start setting content font...";

    _deContFont = defaultFont;

    qDebug() << "TableWidget -> setContFont: "
                "done setting content font...";
}

void TableWidget::setNoGrid() {
    qDebug() << "TableWidget -> setNoGrid: "
                "start setting table widget to have no outline"
                "between table cells";

    _gridLayout->setHorizontalSpacing(0);
    _gridLayout->setVerticalSpacing(0);

    qDebug() << "TableWidget -> setNoGrid: "
                "done setting no grid";
}

void TableWidget::colorCell(int row,
                            int col,
                            QColor font,
                            QColor background) {
    qDebug() << "TableWidget -> colorCell(int row, int col, QColor font, QColor background): "
                "start coloring cell...";

    TableWidget::colorCell(row, col, font, background, _gridLayout);

    qDebug() << "TableWidget -> colorCell(int row, int col, QColor font, QColor background): "
                "done coloring cell...";
}

void TableWidget::underlineCell(int row, int col) {
    QLayoutItem *item = _gridLayout->itemAtPosition(row, col);
    assert (item != 0);

    QWidget *widget = item->widget();
    QLabel *label = qobject_cast<QLabel*>(widget);
    QFont font;
    font.setUnderline(true);

    label->setFont(font);
}

void TableWidget::addRow(QVector<QString> contentOfARow) {
    assert (contentOfARow.size() == _noOfCol);

    qDebug() << "TableWidget -> addRow(QVector<QString> contentOfARow): "
                "start adding a row..";

    _curRow++;

    for (int i = 0; i < contentOfARow.size(); i++) {
        TableWidget::addCell(contentOfARow.at(i), _curRow, i);
    }

    qDebug() << "TableWidget -> addRow(QVector<QString> contentOfARow): "
                "done adding a row..";
}

void TableWidget::addRow(QVector<QString> contentOfARow, int row) {
    qDebug() << "TableWidget -> addRow(QVector<QString> contentOfARow, int row): "
                "start adding a row..";

    if (row <= _curRow) {
        for (int i = 0; i < contentOfARow.size(); i++) {
            TableWidget::addCell(contentOfARow.at(i), row, i);
        }
    } else if (row == _curRow + 1) {
        TableWidget::addRow(contentOfARow);
    } else {
        //error code
        assert (false);
    }

    qDebug() << "TableWidget -> addRow(QVector<QString> contentOfARow, int row): "
                "done adding a row..";
}

void TableWidget::addRow(QVector<QWidget*> contentOfARow) {
    assert (contentOfARow.size() == _noOfCol);

    qDebug() << "TableWidget -> addRow(QVector<QWidget*> contentOfARow): "
                "start adding a row..";

    _curRow++;

    for (int i = 0; i < contentOfARow.size(); i++) {
        TableWidget::addCell(contentOfARow.at(i), _curRow, i);
    }

    qDebug() << "TableWidget -> addRow(QVector<QWidget*> contentOfARow): "
                "done adding a row..";

}

void TableWidget::addRow(QVector<QWidget *> contentOfARow, int row) {
    qDebug() << "TableWidget -> addRow(QVector<QWidget*> contentOfARow, int row): "
                "start adding a row..";

    if (row <= _curRow) {
        //replace
        for (int i = 0; i < contentOfARow.size(); i++) {
            TableWidget::addCell(contentOfARow.at(i), row, i);
        }
    } else if (row == _curRow + 1) {
        //add a new row
        TableWidget::addRow(contentOfARow);
    } else {
        //error code
        assert (false);
    }

    qDebug() << "TableWidget -> addRow(QVector<QWidget*> contentOfARow, int row): "
                "done adding a row..";
}

void TableWidget::addCell(QString content, int row, int col) {
    assert (row - _curRow <= 2);

    if (_rowCount == _curRow){
        QVector <QWidget*> oneRow (_noOfCol);
        _contents.append(oneRow);
        _rowCount++;
    } else if (_curRow >= row) {
        TableWidget::deleteCell(row, col);
        _contents[row][col] = 0;
    } else {
        _curRow = row;
    }
    _area->show();

    TableWidget::addCell(content, row, col, _gridLayout);
    TableWidget::colorCell(row,
                           col,
                           _deContFont,
                           _deContBack,
                           _gridLayout);
}

void TableWidget::addCell(QWidget *content, int row, int col) {
    assert (row - _curRow <= 2);

    if (_rowCount == _curRow){
        _rowCount++;
        QVector <QWidget*> oneRow (_noOfCol);
        _contents.append(oneRow);
    } else if (_curRow >= row) {
        TableWidget::deleteCell(row, col);
        _contents[row][col] = 0;
    } else {
        _curRow = row;
    }
    _area->show();

    TableWidget::addCell(content, row, col, _gridLayout);
    TableWidget::colorCell(row,
                           col,
                           _deContFont,
                           _deContBack,
                           _gridLayout);
}

int TableWidget::getNumOfRow() {
    return _rowCount;
}

//private
void TableWidget::deleteCell(int row, int col){
    QLayoutItem *item = _gridLayout->itemAtPosition(row, col);

    if (item == 0){
        return;
    }

    TableWidget::deleteChildWidgets(item);

}


void TableWidget::addCell(QString content,
                          int row,
                          int col,
                          QGridLayout *layout) {

    QLabel *label = new QLabel;
    TableWidget::initializeLabel(label,
                                 content,
                                 _deContFont,
                                 _deContBack);

    addCell(label, row, col, layout);
}

void TableWidget::addCell(QWidget *content,
                          int row,
                          int col,
                          QGridLayout *layout) {
    qDebug() << "TableWidget -> addCell(QWidget *content, "
                "int row, int col, QGridLayout *layout) : "
                "start adding the cell of " + layout->objectName();

    QWidget *widget = content;

    int height = 0;

    if (!_hasHeader || layout != _gridLayout) {
        qDebug() << "Table has no header or it is not the content layout";

        QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(widget->sizePolicy().hasHeightForWidth());

        widget->setSizePolicy(sizePolicy);

        layout->addWidget(widget, row, col, 0);

        height = widget->height();

    } else {
        qDebug() << "Table has Header and it is the content layout";

        widget->setFixedWidth(_widthOfHeaders[col]);
        widget->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Minimum);

        layout->addWidget(widget, row, col, 0);

        height = widget->heightForWidth(_widthOfHeaders[col]);
    }

    if (layout == _gridLayout) {
        qDebug() << "does things for the content layout...";

        _contents[row][col] = widget;
        showWidget(row, col);

        height *= ENLARGE_FACTOR;

        if (_heightOfContents.size() == row) {

            if (height <= 0 && _hasHeader) {
                height = _headerLayout->rowMinimumHeight(0);
            }

            _heightOfContents.append(height);

            qDebug() << "_heightOfContents is " << height;
            qDebug() << "_heightOfContents size is row";
        } else if (_heightOfContents.size() < row) {
            qDebug() << "size of _heightOfContents is less than row and "
                        "this is actually an error code as it should not happen!";
            return;
        } else {
            if (_heightOfContents[row] >= height) {
                height = 0;
            } else {
                int originalHeight = _heightOfContents[row];
                _heightOfContents[row] = height;
                height = height - originalHeight;
            }

            qDebug() << "_heightOfContents is " << _heightOfContents[row];
            qDebug() << "else";
        }

        emit this->resizedTable(height);
        qDebug() << "emitted signal resizedTable(" << height << ")";

    }

    qDebug() << "TableWidget -> addCell(QWidget *content, "
                "int row, int col, QGridLayout *layout) : "
                "done adding the cell of " + layout->objectName();
}

void TableWidget::colorCell(int row,
                            int col,
                            QColor font,
                            QColor background,
                            QGridLayout *layout) {
    QLayoutItem *item = layout->itemAtPosition(row, col);
    assert (item != 0);

    QWidget *widget = item->widget();
    assert (widget != 0);
    TableWidget::colorWidget(font, background, widget);
}

void TableWidget::deleteRow(int row) {
    assert (row <= _curRow);

    for (int r = row + 1; r <= _curRow; r++){
        for (int c = 0; c < _noOfCol; c++){
            QLayoutItem* item = _gridLayout->itemAtPosition( r, c );
            QWidget* itemWidget = item->widget();
            TableWidget::addCell(itemWidget, r - 1, c);

            _contents[r - 1][c] = _contents[r][c];
        }
    }
    TableWidget::deleteRow();
}

void TableWidget::deleteRow() {
    for (int c = 0; c < _noOfCol; c++) {
       QLayoutItem* item = _gridLayout->itemAtPosition( _curRow, c );
       assert (item != 0);
       TableWidget::deleteChildWidgets(item);
    }
    _curRow--;
    _rowCount--;

    _contents.pop_back();
    _heightOfContents.pop_back();

    if (_curRow < 0) {
        _area->hide();
    }
}

void TableWidget::deleteAll() {
    if (_contents.empty()) {
        return;
    }

    for (int r = _curRow; r >=0 ; r--){
        for (int c = 0; c < _noOfCol; c++){
           QLayoutItem* item = _gridLayout->itemAtPosition( r, c );

           assert (item != 0);

           TableWidget::deleteChildWidgets(item);
        }
        _contents[r].clear();
    }
    _curRow = -1;
    _rowCount = 0;
    _contents.clear();
    _heightOfContents.clear();

    _area->hide();
}

void TableWidget::showWidget(int row, int col){
    QLayoutItem *item = _gridLayout->itemAtPosition(row, col);
    assert (item != 0);

    QWidget *widget = item->widget();
    assert (widget != 0);

    widget->showMaximized();
}

//warning: a very inefficient method,
//think carefully when you want to use it
void TableWidget::addRowFromTop(QVector<QString> contentOfARow) {
    assert (!contentOfARow.empty());


    QVector <QWidget*> newRow (_noOfCol);
    _contents.prepend(newRow);

    addRow(contentOfARow);


    for (int r = _rowCount - 1 ; r > 0; r--) {
        for (int c = 0; c < _noOfCol; c++) {
            QLayoutItem *prevItem = _gridLayout->itemAtPosition(r, c);
            assert (prevItem != 0);
            QLabel *prevLabel = (QLabel*) prevItem->widget();

            QLayoutItem *updateItem = _gridLayout->itemAtPosition(r - 1, c);
            assert (updateItem != 0);
            QLabel *updateLabel = (QLabel*) updateItem->widget();

            prevLabel->setText(updateLabel->text());
        }
    }

    for (int c = 0; c < _noOfCol; c++) {
        QLayoutItem *prevItem = _gridLayout->itemAtPosition(0, c);
        assert (prevItem != 0);
        QLabel *prevLabel = (QLabel*) prevItem->widget();
        prevLabel->setText(contentOfARow.at(c));
    }

}


//private methods
void TableWidget::deleteChildWidgets(QLayoutItem *item) {
    if (item->layout()) {
        // Process all child items recursively.
        for (int i = 0; i < item->layout()->count(); i++) {
            deleteChildWidgets(item->layout()->itemAt(i));
        }
    }
    delete item->widget();
}


void TableWidget::setFixedRowHeight(int height) {
    _fixRowHeight = height;
}


void TableWidget::initializeClass(bool hasHeader,
                                  int noOfCol,
                                  QVector <int> widthOfCol) {
    assert (noOfCol == widthOfCol.size());

    _hasHeader = hasHeader;
    _noOfCol = noOfCol;
    _rowCount = 0;
    _curRow = -1;
    _fixRowHeight = -1;
    _stretchOfCol = widthOfCol;
    _headerLayout = new QGridLayout;

}

void TableWidget::setupGridLayout() {
    _gridLayout = new QGridLayout;
    _gridLayout->setHorizontalSpacing(1);
    _gridLayout->setVerticalSpacing(1);

    int left = 0;
    int top = 0;
    int right = 0;
    int bot = 0;
    _gridLayout->setContentsMargins(left, top, right, bot);
}

void TableWidget::setupScrollArea() {
    QWidget *widget = new QWidget;
    widget->setLayout(_gridLayout);

    _area = new QScrollArea(this);
    _area->setWidgetResizable(true);
    _area->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    _area->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    _area->setWidget(widget);
    _area->setContentsMargins(0, 0, 0, 0);
    _area->setFrameStyle(QFrame::NoFrame);

}

void TableWidget::setupLayout() {
    _layout = new QVBoxLayout;

    if (_hasHeader){
        _headerLayout->setContentsMargins(0, 0, 0, 0);
        _headerLayout->setVerticalSpacing(1);
        _headerLayout->setHorizontalSpacing(1);
        _layout->addLayout(_headerLayout, 0);
        _widthOfHeaders = QVector <int> (_noOfCol);
    }

    if (_gridLayout->isEmpty()) {
        _area->hide();
    }

    _layout->addWidget(_area, 10);
    _layout->setSpacing(1);

    int left = 0;
    int top = 0;
    int right = 0;
    int bot = 0;
    _layout->setContentsMargins(left, top, right, bot);
}

void TableWidget::setupYourself() {
    this->setLayout(_layout);
    this->setSizePolicy(QSizePolicy::Expanding,
                        QSizePolicy::Minimum);

    colorWidget(Qt::black,
                Qt::white,
                this);
}

void TableWidget::clearGarbage() {
    for (int i = _headerLabels.size() - 1; i >= 0; i--) {
        delete _headerLabels.value(i);
    }

    for (int i = _contents.size() - 1; i>= 0; i--) {
        for (int j = _contents.value(i).size() - 1; j >= 0; j--) {
            delete _contents[i][j];
        }
    }

    delete _headerLayout;
    delete _gridLayout;
    delete _layout;
    delete _area;
}

void TableWidget::setupHeader(QVector <QString> headerNames,
                              QColor font,
                              QColor background,
                              int headerHeight) {
    if (!_hasHeader) {
        QString errorMsg = "TableWidget -> setupHeader: "
                           "shouldn't have header!\n";
        std::cout << errorMsg.toStdString();
        qDebug() << errorMsg;
        return;
    }

    for (int i = 0; i < headerNames.size(); i++) {
        QLabel *label = new QLabel;
        TableWidget::initializeLabel(label,
                                     headerNames.at(i),
                                     _deContFont,
                                     _deContBack);

        label->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum);
        _headerLabels.append(label);

        TableWidget::addCell(label, 0, i, _headerLayout);
        TableWidget::colorCell(0, i, font, background, _headerLayout);
    }

    for (int col = 0; col < _noOfCol; col++) {
        int stretch = _stretchOfCol.value(col);
        _headerLayout->setColumnStretch(col, stretch);
    }

    _headerLayout->setRowMinimumHeight(0, headerHeight);

    _headerLayout->update();
}

void TableWidget::colorWidget(QColor font, QColor background, QWidget *widget) {
    widget->setAutoFillBackground(true);
    QPalette pallete = widget->palette();
    pallete.setColor(widget->foregroundRole(), font);
    pallete.setColor(widget->backgroundRole(), background);
    widget->setPalette(pallete);
}

void TableWidget::initializeLabel(QLabel *label,
                                  QString txt,
                                  QColor font,
                                  QColor background) {
    label->setText(txt);
    label->setWordWrap(true);
    label->setAlignment(Qt::AlignCenter);
    TableWidget::colorWidget(font, background, label);
}

//protected
void TableWidget::resizeEvent(QResizeEvent *event) {
    QWidget::resizeEvent(event);

    qDebug() << "TableWidget -> resizeEvent: "
                "using our own method of resizing event...";

    //if we doesn't have header, it will auto align
    if (!_hasHeader) {
        qDebug() << "If there is no header...";



        qDebug() << "TableWidget -> resizeEvent: "
                    "done resizing table that have no header.";

        return;
    }

    qDebug() << "If there is a header....";

    qDebug() << "TableWidget -> resizeEvent: "
             << "resizing table contents to have the same width with headers ";


    for (int i = 0; i < _headerLabels.size(); i++) {
        int headerWidth = _headerLabels.value(i)->width();
        _widthOfHeaders[i] = headerWidth;
        for (int j = 0; j < _contents.size(); j++) {
            if (!_contents[j][i]) {
                return;
            }

            _contents[j][i]->setFixedWidth(headerWidth);
        }
    }



    qDebug() << "TableWidget -> resizeEvent: "
             << "done resizing table contents";

}
