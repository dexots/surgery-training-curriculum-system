#include "gui/researchinstance.h"

const QString ResearchInstance::ICON_ADD = QString::fromUtf8(":/icon/ButtonIcon/add.png");
const QString ResearchInstance::ICON_EDIT = ":/icon/ButtonIcon/edit.png";

const QString ResearchInstance::STATES_CONST[7] = {
    "Planning",
    "DSRB",
    "Data Collection",
    "Analysis",
    "Submission",
    "Drafting",
    "Accepted"
};

const QString ResearchInstance::PROGRESS_CONST[3] = {
    "Done",
    "In Progress",
    "Incomplete"
};

const char *ResearchInstance::FOLDER_PATH = ":/icon/ButtonIcon/folder.png";
const QColor ResearchInstance::PAST = Qt::green;
const QColor ResearchInstance::ONGOING = Qt::yellow;
const QColor ResearchInstance::PENDING = Qt::red;
const QColor ResearchInstance::MY_COLOR = QColor (240, 146, 136, 255);

const int ResearchInstance::PROJECT_PROGRESS_HEIGHT = 50;

const QString STRING_EMPTY = "";
const int DATA_SIZE = 14;

ResearchInstance::ResearchInstance(QString fileName) {
    fileName = fileName.trimmed();
    _fileName = fileName;

    ResearchInstance::initiateClass();
    ResearchInstance::doBasicSetup();
    ResearchInstance::readResearchFile();
}

ResearchInstance::ResearchInstance() {
    ResearchInstance::initiateClass();
    ResearchInstance::doBasicSetup();
}

ResearchInstance::~ResearchInstance() {
    ResearchInstance::clearGarbage();
}

void ResearchInstance::setHyperLink(QString hyperlink) {
    _hyperlink = hyperlink;
}

void ResearchInstance::setTitle(QString title) {
    _titleLabel->setText(title);
    _titleLabel->setFont(boldFont);
}

void ResearchInstance::setDesc(QString desc) {
    _descLabel->setText(desc);
}

void ResearchInstance::setState(int state) {
    _state = QString::number(state);

    for (int i = 0; i < state ; i++) {
        ResearchInstance::colorWidget(Qt::black,
                                      PAST,
                                      _projectWidgets.at(i));
        _projectWidgets.at(i)->setFont(nonBold);
    }

    _projectWidgets.at(state)->setFont(boldFont);
    ResearchInstance::colorWidget(Qt::black,
                                  ONGOING,
                                  _projectWidgets.at(state));

    for (int i = state + 1; i < _projectWidgets.size(); i++) {
        ResearchInstance::colorWidget(Qt::black,
                                      PENDING,
                                      _projectWidgets.at(i));
        _projectWidgets.at(i)->setFont(nonBold);
    }

    _statusBox->setCurrentIndex(_state.toInt());
}

void ResearchInstance::setIniDate(QString date) {
    _dateLabel->setText(date);
    ResearchInstance::colorWidget(Qt::white,
                                  MY_COLOR,
                                  _dateLabel);
}

void ResearchInstance::setTeamLead(QString name) {
    _leadLabel->setText(name);
    ResearchInstance::colorWidget(Qt::white,
                                  MY_COLOR,
                                  _leadLabel);
}

void ResearchInstance::setColla(QString names) {
    _colLabel->setText(names);
    ResearchInstance::colorWidget(Qt::white,
                                  MY_COLOR,
                                  _colLabel);
}

//private
void ResearchInstance::colorWidget(QColor font, QColor back, QWidget *widget) {
    widget->setAutoFillBackground(true);
    QPalette pallete = widget->palette();
    pallete.setColor(widget->foregroundRole(), font);
    pallete.setColor(widget->backgroundRole(), back);
    widget->setPalette(pallete);
}

void ResearchInstance::initiateClass() {
    _titleLabel = new QLabel;
    _descLabel = new QLabel;
    _projectProgress = new QHBoxLayout;
    _plan = new QLabel;
    _dsrb = new QLabel;
    _data = new QLabel;
    _analysis = new QLabel;
    _submit = new QLabel;
    _draft = new QLabel;
    _accept = new QLabel;
    _dateLayout = new QHBoxLayout;
    _dateStatic = new QLabel;
    _dateLabel = new QLabel;
    _leadLayout = new QHBoxLayout;
    _leadStatic = new QLabel;
    _leadLabel = new QLabel;
    _colLayout = new QHBoxLayout;
    _colStatic = new QLabel;
    _colLabel = new QLabel;
    _folderLabel = new QLabel;
    _folderLogo = QPixmap(QString::fromUtf8(FOLDER_PATH));
    _viewLabel = new QLabel;
    _statusLabel = new QLabel;
    _statusBox = new QComboBox;

    _folderLayout = new QVBoxLayout;
    _instanceLayout = new QVBoxLayout;
    _statusLayout = new QHBoxLayout;

    _title = "";
    _desc = "";
    _initDate = "";
    _teamLead = "";
    _colla = "";
    _state = "";
    _folderLink = "";
    _planningDetails = "";
    _dsrbDetails = "";
    _dataDetails = "";
    _analysisDetails = "";
    _submissionDetails = "";
    _draftingDetails = "";
    _acceptedDetails = "";
}

void ResearchInstance::clearGarbage() {
    delete _titleLabel;
    delete _descLabel;
    delete _projectProgress;
    delete _plan;
    delete _dsrb;
    delete _data;
    delete _analysis;
    delete _submit;
    delete _draft;
    delete _accept;
    delete _dateLayout;
    delete _dateStatic;
    delete _dateLabel;
    delete _leadLayout;
    delete _leadStatic;
    delete _leadLabel;
    delete _colLayout;
    delete _colStatic;
    delete _colLabel;
    delete _folderLabel;
    delete _viewLabel;
    delete _statusLabel;
    delete _statusBox;

    delete _folderLayout;
    delete _statusLayout;
    delete _instanceLayout;
}

void ResearchInstance::doBasicSetup() {
    initialiseFonts();
    getUserRole();
    setUpAddButton();
    setStatusLayout();
    addInstanceLayout();
    addLeadLayout();
    addProjectProgress();
    addDateLayout();
    addColLayout();
    addFolderLayout();

    this->setLayout(_instanceLayout);
    ResearchInstance::colorWidget(Qt::black, MY_COLOR, this);

}

/*
 * Setting up the widget
 */
void ResearchInstance::initialiseFonts() {
    boldFont.setBold(true);
    nonBold.setBold(false);
}

void ResearchInstance::setStatusLayout() {
    _statusLabel->setText("Current Project Status: ");
    _statusLabel->setFont(boldFont);

    for (int i=0; i<7; i++) {
        _statusBox->addItem(STATES_CONST[i]);
    }
    connect(_statusBox, SIGNAL(currentIndexChanged(int)),
            this, SLOT(statusChanged()));

    _statusLayout->addWidget(_statusLabel);
    _statusLayout->addWidget(_statusBox);
}

void ResearchInstance::addInstanceLayout() {
    QHBoxLayout *titleLayout = new QHBoxLayout;
    QLabel *editIcon = new QLabel();
    editIcon->setPixmap(ICON_EDIT);
    ClickableWidget *clickableEdit = new ClickableWidget(editIcon);
    connect(clickableEdit, SIGNAL(isClicked()),
            this, SLOT(handleEdit()));

    titleLayout->addWidget(_titleLabel);
    titleLayout->addWidget(clickableEdit, Qt::AlignLeft);

    _instanceLayout->addLayout(titleLayout);
    _instanceLayout->addWidget(_descLabel);
    _instanceLayout->addLayout(_statusLayout);
    _instanceLayout->addLayout(_projectProgress);
    _instanceLayout->addLayout(_dateLayout);
    _instanceLayout->addLayout(_leadLayout);
    _instanceLayout->addLayout(_colLayout);
    _instanceLayout->addLayout(_folderLayout);
    _instanceLayout->setSpacing(0);
}


void ResearchInstance::addProjectProgress() {
    addPlanningWidget();
    addDSRBWidget();
    addDataWidget();
    addAnalysisWidget();
    addSubmissionWidget();
    addDraftWidget();
    addAcceptedWidget();

    makeClickableWidgets();

    _projectWidgets.append(_plan);
    _projectWidgets.append(_dsrb);
    _projectWidgets.append(_data);
    _projectWidgets.append(_analysis);
    _projectWidgets.append(_submit);
    _projectWidgets.append(_draft);
    _projectWidgets.append(_accept);
}

void ResearchInstance::addPlanningWidget() {
    _plan->setText("Planning");
    _plan->setWordWrap(true);
    _plan->setAlignment(Qt::AlignCenter);
    _plan->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    _plan->setFrameStyle(QFrame::Box);
    _plan->setFixedHeight(PROJECT_PROGRESS_HEIGHT);
}

void ResearchInstance::addDSRBWidget() {
    _dsrb->setText("DSRB");
    _dsrb->setWordWrap(true);
    _dsrb->setAlignment(Qt::AlignCenter);
    _dsrb->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    _dsrb->setFrameStyle(QFrame::Box);
    _dsrb->setFixedHeight(PROJECT_PROGRESS_HEIGHT);
}

void ResearchInstance::addDataWidget() {
    _data->setText("Data Collection");
    _data->setWordWrap(true);
    _data->setAlignment(Qt::AlignCenter);
    _data->setMinimumWidth(100);
    _data->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    _data->setFrameStyle(QFrame::Box);
    _data->setFixedHeight(PROJECT_PROGRESS_HEIGHT);
}

void ResearchInstance::addAnalysisWidget() {
    _analysis->setText("Analysis");
    _analysis->setWordWrap(true);
    _analysis->setAlignment(Qt::AlignCenter);
    _analysis->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    _analysis->setFrameStyle(QFrame::Box);
    _analysis->setFixedHeight(PROJECT_PROGRESS_HEIGHT);
}

void ResearchInstance::addSubmissionWidget() {
    _submit->setText("Submission");
    _submit->setWordWrap(true);
    _submit->setAlignment(Qt::AlignCenter);
    _submit->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    _submit->setFrameStyle(QFrame::Box);
    _submit->setFixedHeight(PROJECT_PROGRESS_HEIGHT);
}

void ResearchInstance::addDraftWidget() {
    _draft->setText("Drafting");
    _draft->setWordWrap(true);
    _draft->setAlignment(Qt::AlignCenter);
    _draft->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    _draft->setFrameStyle(QFrame::Box);
    _draft->setFixedHeight(PROJECT_PROGRESS_HEIGHT);
}

void ResearchInstance::addAcceptedWidget() {
    _accept->setText("Accepted");
    _accept->setWordWrap(true);
    _accept->setAlignment(Qt::AlignCenter);
    _accept->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    _accept->setFrameStyle(QFrame::Box);
    _accept->setFixedHeight(PROJECT_PROGRESS_HEIGHT);
}

void ResearchInstance::makeClickableWidgets() {
    ClickableWidget *plan = new ClickableWidget(_plan);
    ClickableWidget *dsrb = new ClickableWidget(_dsrb);
    ClickableWidget *data = new ClickableWidget(_data);
    ClickableWidget *analysis = new ClickableWidget(_analysis);
    ClickableWidget *draft = new ClickableWidget(_draft);
    ClickableWidget *submit = new ClickableWidget(_submit);
    ClickableWidget *accept = new ClickableWidget(_accept);

    connect(plan, SIGNAL(isClicked()),
            this, SLOT(clickedPlan()));
    connect(dsrb, SIGNAL(isClicked()),
            this, SLOT(clickedDsrb()));
    connect(data, SIGNAL(isClicked()),
            this, SLOT(clickedData()));
    connect(analysis, SIGNAL(isClicked()),
            this, SLOT(clickedAnalysis()));
    connect(draft, SIGNAL(isClicked()),
            this, SLOT(clickedDraft()));
    connect(submit, SIGNAL(isClicked()),
            this, SLOT(clickedSubmit()));
    connect(accept, SIGNAL(isClicked()),
            this, SLOT(clickedAccept()));

    _projectProgress->addWidget(plan);
    _projectProgress->addWidget(dsrb);
    _projectProgress->addWidget(data);
    _projectProgress->addWidget(analysis);
    _projectProgress->addWidget(submit);
    _projectProgress->addWidget(draft);
    _projectProgress->addWidget(accept);
    _projectProgress->setSpacing(0);
    _projectProgress->setContentsMargins(0, 10, 10, 0);
}

/*
 * Other layouts
 */

void ResearchInstance::addDateLayout() {
    _dateStatic->setText("Initiated on: ");
    _dateStatic->setFont(boldFont);

    _dateLayout->addWidget(_dateStatic);
    _dateLayout->addWidget(_dateLabel);
}

void ResearchInstance::addLeadLayout() {
    _leadStatic->setText("Team Lead: ");
    _leadStatic->setFont(boldFont);

    _leadLayout->addWidget(_leadStatic);
    _leadLayout->addWidget(_leadLabel);
}

void ResearchInstance::addColLayout() {
    _colStatic->setText("Collaborators: ");
    _colStatic->setFont(boldFont);

    _colLayout->addWidget(_colStatic);
    _colLayout->addWidget(_colLabel);

}

void ResearchInstance::addFolderLayout() {
    _folderLabel->setPixmap(_folderLogo);
    ClickableWidget *folderWidget = new ClickableWidget(_folderLabel);
    _viewLabel->setText("View Folder");
    _viewLabel->setFont(boldFont);

    _folderLayout->addWidget(folderWidget);
    //_folderLayout->addWidget(_viewLabel);
    _folderLayout->setContentsMargins(0, 10, 10, 0  );

    connect(folderWidget, SIGNAL(isClicked()),
            this, SLOT(handleHyperlink()));
}

void ResearchInstance::handleHyperlink() {
    emit this->load(_hyperlink, "research");
}

/*
 * File Operations
 */

void ResearchInstance::readResearchFile() {
    qDebug() << "ResearchInstance -> readResearchFile : Reading research file "
             << _fileName;
    QFile file(_fileName);
    if (!file.open(QFile::ReadOnly | QFile::Text)) {
        qDebug() << "ResearchInstance -> readResearchFile : Unable to open research file "
                 << _fileName;
        //show error box
        //assert(false);
        //std::cout << "Error";

        showErrorBox();

        return;
    }

    QTextStream in(&file);
    QString data = in.readAll();
    populateData(data);
    file.close();
    qDebug() << "ResearchInstance -> readResearchFile : Successful to read research file "
             << _fileName;
}

void ResearchInstance::saveResearchFile() {
    QFile file(_fileName);

    if (!file.open(QFile::WriteOnly | QFile::Text)) {
        //showErrorBox();
        return;
    }
    QTextStream out(&file);

    out <<  _title << "\n" << _desc << "\n" << _initDate << "\n" <<
            _teamLead << "\n" << _colla << "\n" << _state << "\n" <<
            _hyperlink << "\n" << _planningDetails << "\n" <<
            _dsrbDetails << "\n" << _dataDetails << "\n" <<
            _analysisDetails << "\n" << _submissionDetails << "\n" <<
            _draftingDetails << "\n" << _acceptedDetails;
    file.close();

}

void ResearchInstance::populateData(QString data) {
    if (data.isEmpty()) {
        return;
    }

    QStringList split = data.split("\n");

    if (split.size() != DATA_SIZE) {
        showErrorBox();
        std::cout << _fileName.toStdString() << "\n";
        return;
    }

    _title = split[0];
    _desc = split[1];
    _initDate = split[2];
    _teamLead = split[3];
    _colla = split[4];
    _state = split[5];
    _hyperlink = split[6];

    setTitle(_title);
    setDesc(_desc);
    setIniDate(_initDate);
    setTeamLead(_teamLead);
    setColla(_colla);
    setState(_state.toInt());
    setHyperLink(_hyperlink);

    // Set data for the states
    setPlanning(split[7]);
    setDSRB(split[8]);
    setData(split[9]);
    setAnalysis(split[10]);
    setSubmission(split[11]);
    setDrafting(split[12]);
    setAccepted(split[13]);
}

void ResearchInstance::showErrorBox() {
    ErrorBox *errorBox = new ErrorBox("SETTINGS");
    errorBox->exec();
    errorBox->deleteLater();
}

/*
 * Helper functions for populateData
 */
void ResearchInstance::setPlanning(QString planning) {
    _planningDetails = planning;
}

void ResearchInstance::setDSRB(QString dsrb) {
    _dsrbDetails = dsrb;
}

void ResearchInstance::setData(QString data) {
    _dataDetails = data;
}

void ResearchInstance::setAnalysis(QString analysis) {
    _analysisDetails = analysis;
}

void ResearchInstance::setSubmission(QString submission) {
    _submissionDetails = submission;
}

void ResearchInstance::setDrafting(QString drafting) {
    _draftingDetails = drafting;
}

void ResearchInstance::setAccepted(QString accepted) {
    _acceptedDetails = accepted;
}

/*
 * SLOTS
 */

void ResearchInstance::clickedPlan() {
    ResearchBox *researchBox = new ResearchBox(STATES_CONST[0],
                                                _planningDetails
                                                );
    connect(researchBox, SIGNAL(passInfo(QString,QString)),
            this, SLOT(haveInfo(QString,QString)));
    researchBox->exec();
}

void ResearchInstance::clickedDsrb() {
    ResearchBox *researchBox = new ResearchBox(STATES_CONST[1],
                                                _dsrbDetails
                                                );
    connect(researchBox, SIGNAL(passInfo(QString,QString)),
            this, SLOT(haveInfo(QString,QString)));
    researchBox->exec();
}

void ResearchInstance::clickedData() {
    ResearchBox *researchBox = new ResearchBox(STATES_CONST[2],
                                                _dataDetails
                                                );
    connect(researchBox, SIGNAL(passInfo(QString,QString)),
            this, SLOT(haveInfo(QString,QString)));
    researchBox->exec();
}

void ResearchInstance::clickedAnalysis() {
    ResearchBox *researchBox = new ResearchBox(STATES_CONST[3],
                                                _analysisDetails
                                                );
    connect(researchBox, SIGNAL(passInfo(QString,QString)),
            this, SLOT(haveInfo(QString,QString)));
    researchBox->exec();
}

void ResearchInstance::clickedSubmit() {
    ResearchBox *researchBox = new ResearchBox(STATES_CONST[4],
                                                _submissionDetails
                                                );
    connect(researchBox, SIGNAL(passInfo(QString,QString)),
            this, SLOT(haveInfo(QString,QString)));
    researchBox->exec();
}

void ResearchInstance::clickedDraft() {
    ResearchBox *researchBox = new ResearchBox(STATES_CONST[5],
                                                _draftingDetails
                                                );
    connect(researchBox, SIGNAL(passInfo(QString,QString)),
            this, SLOT(haveInfo(QString,QString)));
    researchBox->exec();
}

void ResearchInstance::clickedAccept() {
    ResearchBox *researchBox = new ResearchBox(STATES_CONST[6],
                                                _acceptedDetails);
    connect(researchBox, SIGNAL(passInfo(QString,QString)),
            this, SLOT(haveInfo(QString,QString)));
    researchBox->exec();
}

void ResearchInstance::statusChanged() {
    int state = _statusBox->currentIndex();
    _state = QString::number(state);
    setState(state);
    saveResearchFile();
}

void ResearchInstance::handleEdit() {
    QString stateString = _statusBox->currentText();
    ResearchAddBox *resBox = new ResearchAddBox(_title, _desc,
                                                _colla, _hyperlink,
                                                stateString, _initDate,
                                                _teamLead);
    connect(resBox, SIGNAL(editedInfo(QString,QString,QString,QString,QString,QString,QString)),
            this, SLOT(editReceived(QString,QString,QString,QString,QString,QString,QString)));
    resBox->exec();
}

void ResearchInstance::editReceived(QString titleString,
           QString descString, QString collabString,
           QString folderString, QString statusString,
           QString initiatedString, QString teamLead) {

    _title = titleString;
    _desc = descString;
    _initDate = initiatedString;
    _teamLead = teamLead;
    _colla = collabString;
    _hyperlink = folderString;
    _state = statusString;

    setTitle(_title);
    setDesc(_desc);
    setIniDate(_initDate);
    setTeamLead(_teamLead);
    setColla(_colla);
    setState(_state.toInt());
    setHyperLink(_hyperlink);


    saveResearchFile();
}

void ResearchInstance::haveInfo(QString state, QString details) {

    if (state == STATES_CONST[0]) {
        _planningDetails = details;
    } else if (state == STATES_CONST[1]) {
        _dsrbDetails = details;
    } else if (state == STATES_CONST[2]) {
        _dataDetails = details;
    } else if (state == STATES_CONST[3]) {
        _analysisDetails = details;
    } else if (state == STATES_CONST[4]) {
        _submissionDetails = details;
    } else if (state == STATES_CONST[5]) {
        _draftingDetails = details;
    } else if (state == STATES_CONST[6]) {
        _acceptedDetails = details;
    }

    saveResearchFile();
}

void ResearchInstance::getUserRole() {
    UserDetails *userDetails = UserDetails::getObject();
    _userRole = userDetails->getUserRole();
}

void ResearchInstance::setUpAddButton() {
    QLabel *addRes = new QLabel;
    addRes->setText("Add Research Project");
    addRes->setSizePolicy(QSizePolicy::Fixed,
                               QSizePolicy::Fixed);

    QLabel *addLogo = new QLabel;
    addLogo->setPixmap(QPixmap(ICON_ADD));
    addLogo->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

    QHBoxLayout *addLayout = new QHBoxLayout;
    addLayout->addWidget(addRes);
    addLayout->addWidget(addLogo);
    addLayout->setContentsMargins(0, 0, 0, 0);

    QWidget *addWidget = new QWidget;
    addWidget->setLayout(addLayout);
    ClickableWidget *addClick = new ClickableWidget(addWidget);

    connect(addClick, SIGNAL(isClicked()),
            this, SLOT(handleAdd()));

    QHBoxLayout *layout = new QHBoxLayout;
    layout->addWidget(addClick);
    layout->setAlignment(addClick, Qt::AlignRight);

    if (_userRole == UserDetails::ROLE_TRAINER) {
        _instanceLayout->addLayout(layout);
    }

}

void ResearchInstance::handleAdd() {
    ResearchAddBox *resBox = new ResearchAddBox();
    resBox->exec();
}

