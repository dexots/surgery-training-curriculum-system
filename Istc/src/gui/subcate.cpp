#include "gui/subcate.h"

SubCate::SubCate(QString name,
                 QString desc,
                 const char *icon,
                 QColor descColor,
                 QColor defaultColor,
                 QColor selectedColor,
                 QWidget *corWidget) :
    CateBox() {
    CateBox::preSetupCateBox(name, icon, defaultColor);
    SubCate::setCorWidget(corWidget);
    SubCate::setDefaultDescColor(defaultColor);
    SubCate::setSelectedColor(selectedColor);
    SubCate::setupDescLabel(desc, descColor);
    this->setCursor(Qt::PointingHandCursor);
}

SubCate::~SubCate() {
    delete _corWidget;
    delete _descriptionLabel;
}

//public method
QWidget *SubCate::getCorWidget() {
    return _corWidget;
}

//protected method
void SubCate::handleTouch() {
    CateBox::handleTouch();
}

void SubCate::mousePressEvent(QMouseEvent *event) {
    CateBox::mousePressEvent(event);
}

void SubCate::createMyself() {
    QLayout *widgetLayout = SubCate::setupSubCateLayout();
    this->setLayout(widgetLayout);
    this->setAutoFillBackground(true);
    this->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Preferred);
}

QLayout *SubCate::setupSubCateLayout() {
    QVBoxLayout *newLayout = new QVBoxLayout;
    QLayout *oriLayout = CateBox::setupCateBoxLayout();
    newLayout->addLayout(oriLayout);
    newLayout->addWidget(_descriptionLabel);
    return newLayout;
}

//private methods
void SubCate::showDefaultBackground() {
    CateBox::colorWidget(_colorDefault);
}

void SubCate::showSelectedBackground() {
    CateBox::colorWidget(_colorSelected);
}

void SubCate::setupDescLabel(QString desc, QColor descColor) {
    SubCate::initializeLabel();
    SubCate::insDesc(desc);
    SubCate::setDescFontColor(descColor);
    SubCate::createMyself();
}

void SubCate::setDefaultDescColor(QColor defaultColor) {
    _colorDefault = defaultColor;
}

void SubCate::setSelectedColor(QColor selectedColor) {
    _colorSelected = selectedColor;
}

void SubCate::setCorWidget(QWidget *corWidget) {
    _corWidget = corWidget;
}

void SubCate::initializeLabel() {
    QFont descFont(FONT_NAME, FONT_INPUT_AREA_SIZE);

    _descriptionLabel = new QLabel;
    _descriptionLabel->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
    _descriptionLabel->setAlignment(Qt::AlignTop | Qt::AlignRight);
    _descriptionLabel->setWordWrap(true);
    _descriptionLabel->setFont(descFont);

}

void SubCate::insDesc(QString descString) {
    _descriptionLabel->setText(descString);
}

void SubCate::setDescFontColor(QColor descColor) {
    QPalette pallete = _descriptionLabel->palette();
    pallete.setColor(_descriptionLabel->foregroundRole(), descColor);
    _descriptionLabel->setPalette(pallete);
}

