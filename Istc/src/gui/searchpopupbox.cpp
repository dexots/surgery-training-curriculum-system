#include "gui/searchpopupbox.h"

const QString SearchPopupBox::COLOR_BUTTONBACKGROUND = "rgb(39, 174, 96)";
const QString SearchPopupBox::COLOR_BUTTONBORDER = "white";
const QString SearchPopupBox::COLOR_BUTTONFONT = "white";
const QString SearchPopupBox::FONT_BUTTON = "bold 14";
const QString SearchPopupBox::PADDING_BUTTON = "6";
const QString SearchPopupBox::RADIUS_BUTTONBORDER = "10";
const QString SearchPopupBox::STYLE_BUTTONBORDER = "outset";
const QString SearchPopupBox::WIDTH_BUTTON = "50";
const QString SearchPopupBox::WIDTH_BUTTONBORDER = "2";

const QString SearchPopupBox::TAG_SEARCHING = "Searching for ";
const QString SearchPopupBox::TAG_OK = "OK";

const int SearchPopupBox::WIDTH_MINIMUM = 400;
const int SearchPopupBox::HEIGHT_MINIMUM = 200;

//TODO: Handle the search request

SearchPopupBox::SearchPopupBox(QString searchRequest) {
    _bold.setBold(true);

    QString searchReqString = TAG_SEARCHING + searchRequest + "...";
    _searchRequestText = new QLabel(searchReqString);
    _searchRequestText->setFont(_bold);

    _searchText = new QLabel("Nothing found.");
    _searchText->setFont(_bold);

    _okButton = new QPushButton();
    _okButton->setText(TAG_OK);
    setButtonStyleSheet(_okButton);
    _okButton->setCursor(Qt::PointingHandCursor);
    connect(_okButton, SIGNAL(clicked()),
            this, SLOT(handleButton()));
    _okButton->setSizePolicy(QSizePolicy::Fixed,
                             QSizePolicy::Fixed);
    _buttonLayout = new QHBoxLayout();
    _buttonLayout->addWidget(_okButton);

    _mainLayout = new QVBoxLayout;
    _mainLayout->addWidget(_searchRequestText);
    _mainLayout->addWidget(_searchText);
    _mainLayout->addLayout(_buttonLayout);

    this->setLayout(_mainLayout);
    this->setContentsMargins(10, 10, 10, 10);
    this->setMinimumWidth(WIDTH_MINIMUM);
    this->setMinimumHeight(HEIGHT_MINIMUM);

    this->setAttribute(Qt::WA_TranslucentBackground, true);

    this->setWindowFlags(Qt::FramelessWindowHint);
}

SearchPopupBox::~SearchPopupBox() {

}

void SearchPopupBox::handleButton() {
    QDialog::done(0);
}

void SearchPopupBox::paintEvent(QPaintEvent *event) {
    QDialog::paintEvent(event);
    QPainter painter(this);
    QBrush brush(Qt::white);
    painter.setBrush(brush);
    painter.setPen(Qt::white);
    painter.drawRoundRect(QRect(0, 0, this->width(), this->height()));
}

void SearchPopupBox::changeColor(QColor font, QColor back, QWidget *widget) {
    widget->setAutoFillBackground(true);
    QPalette pallete = widget->palette();
    pallete.setColor(widget->foregroundRole(), font);
    pallete.setColor(widget->backgroundRole(), back);
    widget->setPalette(pallete);
}

void SearchPopupBox::setButtonStyleSheet(QPushButton *button) {
    button->setStyleSheet("background-color:" + COLOR_BUTTONBACKGROUND + ";"
                          "border-style:" + STYLE_BUTTONBORDER + ";"
                          "border-width:" + WIDTH_BUTTONBORDER + "px;"
                          "border-radius:" + RADIUS_BUTTONBORDER + "px;"
                          "border-color:" + COLOR_BUTTONBORDER + ";"
                          "font:" + FONT_BUTTON + "px;"
                          "color:" + COLOR_BUTTONFONT + ";"
                          "min-width:" + WIDTH_BUTTON + "px;"
                          "padding:" + PADDING_BUTTON + "px;");
}
