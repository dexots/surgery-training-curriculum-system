#include "gui/botrightmain.h"

const QString BotRightMain::CATEGORY_TRAIN = "Training";
const QString BotRightMain::CATEGORY_ROS = "Roster and Leave";
const QString BotRightMain::CATEGORY_LOG = "Logbook";
const QString BotRightMain::CATEGORY_RESEARCH = "Research";
const QString BotRightMain::CATEGORY_RESOURCES = "Resources";

const char *BotRightMain::ICON_TRAIN = ":/icon/ButtonIcon/Training Icon.png";
const char *BotRightMain::ICON_ROS = ":/icon/ButtonIcon/Roster Icon.png";
const char *BotRightMain::ICON_LOG = ":/icon/ButtonIcon/Logbook Icon.png";
const char *BotRightMain::ICON_RESEARCH = ":/icon/ButtonIcon/Research Icon.png";
const char *BotRightMain::ICON_RESOURCES = ":/icon/ButtonIcon/Resources Icon";

const QColor BotRightMain::COLOR_GREEN = QColor (46, 204, 113, 255);
const QColor BotRightMain::COLOR_BLUE = QColor (50, 149, 219, 255);
const QColor BotRightMain::COLOR_YELLOW = QColor (249, 105, 14, 255);
const QColor BotRightMain::COLOR_RED = QColor (231, 76, 60, 255);
const QColor BotRightMain::COLOR_GREY = QColor (149, 165, 166, 255);

BotRightMain::BotRightMain() {
    qDebug() << "BotRightMain : Initializing bottom right boxes widget";
    BotRightMain::initializeBoxes();
    qDebug() << "BotRightMain : Initialized bottom right boxes widget";
}

BotRightMain::~BotRightMain() {
    qDebug() << "~BotRightMain : Clearing garbage";
    BotRightMain::clearGarbage();
    qDebug() << "~BotRightMain : Cleared garbage";
}

//private slots:
void BotRightMain::clickedTrain() {
    qDebug() << "BotRightMain -> ClickedTrain : Emitting wantTrain signal";
    emit this->wantTrain();
    qDebug() << "BotRightMain -> ClickedTrain : Emitted wantTrain signal";
}

void BotRightMain::clickedRos() {
    qDebug() << "BotRightMain -> clickedRos : Emitting clickedRos signal";
    emit this->wantRos();
    qDebug() << "BotRightMain -> ClickedRos : Emitted clickedRos signal";
}

void BotRightMain::clickedLog() {
    qDebug() << "BotRightMain -> clickedLog : Emitting clickedLog signal";
    emit this->wantLog();
    qDebug() << "BotRightMain -> clickedLog : Emitted clickedLog signal";
}

void BotRightMain::clickedResearch() {
    qDebug() << "BotRightMain -> clickedResearch : Emitting clickedResearch signal";
    emit this->wantResearch();
    qDebug() << "BotRightMain -> clickedResearch : Emitted clickedResearch signal";
}

void BotRightMain::clickedResources() {
    qDebug() << "BotRightMain -> clickedResources : Emitting clickedResources signal";
    emit this->wantResources();
    qDebug() << "BotRightMain -> clickedResources : Emitted clickedResources signal";
}

//private methods:
void BotRightMain::clearGarbage() {
    delete _widgetLayout;
    delete _cateTrain;
    delete _cateRos;
    delete _cateLog;
    delete _cateResearch;
    delete _cateResources;
}

void BotRightMain::initializeBoxes() {
    _widgetLayout = new QVBoxLayout;
    BotRightMain::initializeLayoutTop();
    BotRightMain::initializeLayoutMid();
    BotRightMain::initializeLayoutBot();
    this->setLayout(_widgetLayout);
}

void BotRightMain::initializeLayoutTop() {
    qDebug() << "BotRightMain -> InitializeLayoutTop : "
             << "Initializing top row boxes widget layout";
    BotRightMain::initializeCateTrain();
    _widgetLayout->addWidget(_cateTrain);
    qDebug() << "BotRightMain -> InitializeLayoutTop : "
             << "Initialized top row boxes widget layout";
}

void BotRightMain::initializeLayoutMid() {
    qDebug() << "BotRightMain -> initializeLayoutMid : "
             << "Initializing middle row boxes widget layout";
    BotRightMain::initializeCateRos();
    BotRightMain::initializeCateLog();
    BotRightMain::combineTwoLayouts(_cateRos, _cateLog);
    qDebug() << "BotRightMain -> initializeLayoutMid : "
             << "Initialized middle row boxes widget layout";
}

void BotRightMain::initializeLayoutBot() {
    qDebug() << "BotRightMain -> initializeLayoutBot : "
             << "Initializing bottom row boxes widget layout";
    BotRightMain::initializeCateResearch();
    BotRightMain::initializeCateResources();
    BotRightMain::combineTwoLayouts(_cateResearch, _cateResources);
    qDebug() << "BotRightMain -> initializeLayoutBot : "
             << "Initialized bottom row boxes widget layout";
}

void BotRightMain::initializeCateTrain() {
    qDebug() << "BotRightMain -> initializeCateTrain : "
             << "Initializing training category box widget";
    _cateTrain = new CateBox(CATEGORY_TRAIN, ICON_TRAIN, COLOR_GREEN);
    connect(_cateTrain, SIGNAL(isClicked()), this, SLOT(clickedTrain()));
    qDebug() << "BotRightMain -> initializeCateTrain : "
             << "Initialized training category box widget";
}

void BotRightMain::initializeCateRos() {
    qDebug() << "BotRightMain -> initializeCateRos : "
             << "Initializing roster category box widget";
    _cateRos = new CateBox(CATEGORY_ROS, ICON_ROS, COLOR_BLUE);
    connect(_cateRos, SIGNAL(isClicked()), this, SLOT(clickedRos()));
    qDebug() << "BotRightMain -> initializeCateRos : "
             << "Initialized roster category box widget";
}

void BotRightMain::initializeCateLog() {
    qDebug() << "BotRightMain -> initializeCateLog : "
             << "Initializing log category box widget";
    _cateLog = new CateBox(CATEGORY_LOG, ICON_LOG, COLOR_YELLOW);
    connect(_cateLog, SIGNAL(isClicked()), this, SLOT(clickedLog()));
    qDebug() << "BotRightMain -> initializeCateLog : "
             << "Initialized log category box widget";
}

void BotRightMain::initializeCateResearch() {
    qDebug() << "BotRightMain -> initializeCateResearch : "
             << "Initializing research category box widget";
    _cateResearch = new CateBox(CATEGORY_RESEARCH, ICON_RESEARCH, COLOR_RED);
    connect(_cateResearch, SIGNAL(isClicked()), this, SLOT(clickedResearch()));
    qDebug() << "BotRightMain -> initializeCateResearch : "
             << "Initialized research category box widget";
}

void BotRightMain::initializeCateResources() {
    qDebug() << "BotRightMain -> initializeCateResources : "
             << "Initializing resources category box widget";
    _cateResources = new CateBox(CATEGORY_RESOURCES, ICON_RESOURCES, COLOR_GREY);
    connect(_cateResources, SIGNAL(isClicked()), this, SLOT(clickedResources()));
    qDebug() << "BotRightMain -> initializeCateResources : "
             << "Initialized resources category box widget";
}

void BotRightMain::combineTwoLayouts(CateBox *left, CateBox *right) {
    qDebug() << "BotRightMain -> combineTwoLayouts : "
             << "Adding two category box widgets into one layout";
    QHBoxLayout *tempLayout = new QHBoxLayout;
    tempLayout->addWidget(left);
    tempLayout->addWidget(right);
    _widgetLayout->addLayout(tempLayout);
    qDebug() << "BotRightMain -> combineTwoLayouts : "
             << "Added two category box widgets into one layout";
}

