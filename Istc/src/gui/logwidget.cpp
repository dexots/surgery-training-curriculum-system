#include "gui/logwidget.h"

LogWidget::LogWidget() {
    LogWidget::createLayout();
}

LogWidget::~LogWidget() {
    delete _widgetLayout;
}

void LogWidget::createLayout(){
    _widgetLayout = new QVBoxLayout;

    qDebug() << "LogWidget -> createLayout: "
                "initializing workingHours...";

    WorkingWidget *workingHours = new WorkingWidget;

    SubCate *workingHoursCate = new SubCate("Working Hours",
                                            "Log your working hours and view your timesheet",
                                            "",
                                            Qt::white, //desc font color
                                            QColor (243, 156, 18, 255), //default color
                                            QColor (244, 179, 80, 255), //selected color
                                            workingHours);

    connect(this, SIGNAL(updateWorkingHourTable()), workingHours, SLOT(updateTable()));

    CodeWidget *procedureCode = new CodeWidget;

    SubCate *codeCate = new SubCate("Procedural Code",
                                    "Records of visited patients",
                                    "",
                                    Qt::white, //desc font color
                                    QColor (243, 156, 18, 255), //default color
                                    QColor (244, 179, 80, 255), //selected color
                                    procedureCode);

    connect(this, SIGNAL(updateProcedureCodeTable()), procedureCode, SLOT(updateTable()));

    QVector <SubCate*> logSubCategories;
    logSubCategories.append(workingHoursCate);
    logSubCategories.append(codeCate);

    CateTable *logTable = new CateTable("Logbook",
                                        "",
                                        ":/icon/ButtonIcon/Logbook Icon.png",
                                        QColor (249, 105, 14, 255),
                                        logSubCategories);
    _widgetLayout->addWidget(logTable);
    _widgetLayout->setContentsMargins(0, 0, 0, 0);
    this->setLayout(_widgetLayout);

    connect(logTable, SIGNAL(backToHome()), this, SLOT(handleHomeSignal()));
}

void LogWidget::colorWidget(QColor color, QWidget *widget) {
    widget->setAutoFillBackground(true);
    QPalette pallete = widget->palette();
    pallete.setColor(widget->backgroundRole(), color);
    widget->setPalette(pallete);
}

/*
 * Private Slots
 */
void LogWidget::handleHomeSignal() {
    emit this->backToHome();
}

/*
 * Public Slots
 */
void LogWidget::updateAllTables() {
    emit updateProcedureCodeTable();
    emit updateWorkingHourTable();
}


