#include "gui/topwidget.h"

const QString ISTC_LOGO = ":/icon/ButtonIcon/small_istc.png";
const QString SEARCH_LOGO = ":/icon/ButtonIcon/search.png";
const QString PROFILE_LOGO = ":/icon/ButtonIcon/profile logo.png";
const QString REFRESH_LOGO = ":/icon/ButtonIcon/refresh.png";
const QString SETTINGS_LOGO = ":/icon/ButtonIcon/settings.png";
const QString LOGOUT_LOGO = ":/icon/ButtonIcon/logout.png";

TopWidget::TopWidget() {
    QHBoxLayout *layout = new QHBoxLayout;
    QHBoxLayout *updateLayout = new QHBoxLayout;
    QHBoxLayout *topRight = new QHBoxLayout;
    QHBoxLayout *bottomRight = new QHBoxLayout;
    QVBoxLayout *left = new QVBoxLayout;
    QVBoxLayout *right = new QVBoxLayout;

    _searchLayout = new QHBoxLayout;
    _searchBar = new QWidget;
    QHBoxLayout *searchLogoLayout = new QHBoxLayout;
    QHBoxLayout *searchLineLayout = new QHBoxLayout;
    QHBoxLayout *profileLayout = new QHBoxLayout;
    QHBoxLayout *profileLineLayout = new QHBoxLayout;
    QHBoxLayout *profileLogoLayout = new QHBoxLayout;
    QHBoxLayout *logoutLogoLayout = new QHBoxLayout;

    layout->setContentsMargins(0, 0, 0, 0);
    updateLayout->setContentsMargins(0, 0, 0, 0);
    topRight->setContentsMargins(0, 0, 0, 0);
    bottomRight->setContentsMargins(0, 0, 0, 0);
    left->setContentsMargins(0, 0, 0, 0);
    right->setContentsMargins(0, 0, 0, 0);

    _searchLayout->setContentsMargins(0, 0, 0, 0);
    searchLogoLayout->setContentsMargins(0, 0, 0, 0);
    searchLineLayout->setContentsMargins(0, 0, 0, 0);
    profileLayout->setContentsMargins(0, 0, 0, 0);
    profileLineLayout->setContentsMargins(0, 0, 0, 0);
    profileLogoLayout->setContentsMargins(0, 0, 0, 0);
    logoutLogoLayout->setContentsMargins(0, 0, 0, 0);

    QPixmap *istcLogo = new QPixmap(ISTC_LOGO);
    QPixmap *searchLogo = new QPixmap(SEARCH_LOGO);
    QPixmap *profileLogo = new QPixmap(PROFILE_LOGO);
    QPixmap refreshLogo = QPixmap(REFRESH_LOGO);
    QPixmap settingsLogo = QPixmap(SETTINGS_LOGO);
    QPixmap *logoutLogo = new QPixmap(LOGOUT_LOGO);

    _searchLine = new QLineEdit;

    QLabel *latestText = new QLabel;
    QLabel *updateText = new QLabel;
    QLabel *userText = new QLabel;
    QLabel *nowDate = new QLabel;

    QLabel *istcLogoBox = new QLabel;
    QLabel *searchLogoBox = new QLabel;
    QLabel *profileLogoBox = new QLabel;
    QLabel *refreshLogoBox = new QLabel;
    QLabel *settingsLogoBox = new QLabel;
    QLabel *logoutLogoBox = new QLabel;

    QDate today = QDate::currentDate();

    ClickableWidget *clickableIstc = new ClickableWidget(istcLogoBox);
    ClickableWidget *clickableProfileLogoBox = new ClickableWidget(profileLogoBox);
    ClickableWidget *clickableRefreshLogoBox = new ClickableWidget(refreshLogoBox);
    ClickableWidget *clickableSettingsLogoBox = new ClickableWidget(settingsLogoBox);
    ClickableWidget *clickableLogoutBox = new ClickableWidget(logoutLogoBox);

    connect(clickableIstc, SIGNAL(isClicked()),
            this, SLOT(clickedIstc()));
    connect(clickableRefreshLogoBox, SIGNAL(isClicked()),
            this, SLOT(clickedRefresh()));
    connect(clickableSettingsLogoBox, SIGNAL(isClicked()),
            this, SLOT(clickedSettings()));
    connect(clickableLogoutBox, SIGNAL(isClicked()),
            this, SLOT(clickedLogout()));

    //Overall layout
    left->setSizeConstraint(QLayout::SetMinAndMaxSize);
    layout->addLayout(left, 1);
    layout->addLayout(right, 7);

    //Right side layout
    right->addLayout(topRight);
    right->addLayout(bottomRight);
    topRight->addWidget(_searchBar);
    topRight->addWidget(nowDate);
    bottomRight->addLayout(updateLayout);
    bottomRight->addLayout(profileLayout);
    _searchLayout->addLayout(searchLogoLayout);
    _searchLayout->addLayout(searchLineLayout);
    profileLayout->addLayout(profileLineLayout);
    profileLayout->addLayout(profileLogoLayout);
    profileLayout->addWidget(clickableRefreshLogoBox);
    profileLayout->addWidget(clickableSettingsLogoBox);
    profileLayout->addLayout(logoutLogoLayout);
    _searchBar->setLayout(_searchLayout);

    //ISTC Logo and place it above calendar
    istcLogoBox->setMinimumWidth(260);
    istcLogoBox->setMaximumWidth(260);
    istcLogoBox->setMaximumHeight(140);
    istcLogoBox->setPixmap(*istcLogo);
    istcLogoBox->setAlignment(Qt::AlignCenter);
    left->addWidget(clickableIstc);

    //Search bar
    _searchLine->setFrame(0);
    _searchLine->setStyleSheet("font-size: 15px; font-weight:1; color: grey; border-bottom-color: grey");
    _searchLine->setPlaceholderText("Search");
    searchLogoBox->setMinimumWidth(40);
    searchLogoBox->setMaximumWidth(40);
    searchLogoBox->setPixmap(*searchLogo);
    searchLogoLayout->addWidget(searchLogoBox);
    searchLineLayout->addWidget(_searchLine);
    searchLineLayout->setAlignment(_searchLine, Qt::AlignLeft);

    // TODO: dynamic search
    connect(_searchLine, SIGNAL(returnPressed()),
            this, SLOT(enteredSearch()));

    //Set up name
    UserDetails* userDetails = UserDetails::getObject();

    QString displayTxt = "Hello! ";
    QString name = userDetails->getUserName();
    QString role = userDetails->getUserRole();
    displayTxt += name;
    displayTxt += "\n(" + role.toUpper() +")";
    userText->setText(displayTxt);
    userText->setStyleSheet("font-size: 15px; font-weight:600");
    profileLogoBox->setMinimumWidth(40);
    profileLogoBox->setMaximumWidth(40);
    profileLogoBox->setPixmap(*profileLogo);
    logoutLogoBox->setMinimumWidth(40);
    logoutLogoBox->setMaximumWidth(40);
    logoutLogoBox->setPixmap(*logoutLogo);
    profileLineLayout->addWidget(userText);
    profileLineLayout->setAlignment(userText, Qt::AlignRight);
    logoutLogoLayout->addWidget(clickableLogoutBox);
    refreshLogoBox->setPixmap(refreshLogo);
    refreshLogoBox->setFixedWidth(45);
    clickableRefreshLogoBox->setFixedWidth(50);
    settingsLogoBox->setPixmap(settingsLogo);
    settingsLogoBox->setFixedWidth(45);
    clickableSettingsLogoBox->setFixedWidth(50);

    //Set up connection of clickable profile logo
    clickableProfileLogoBox->setMinimumWidth(40);
    clickableProfileLogoBox->setMaximumWidth(40);
    connect(clickableProfileLogoBox, SIGNAL(isClicked()), this, SLOT(clickedProfileLogo()));
    profileLogoLayout->addWidget(clickableProfileLogoBox);

    //Set up update bar
    bottomRight->setAlignment(updateLayout, Qt::AlignLeft);
    bottomRight->setAlignment(profileLayout, Qt::AlignRight);
    latestText->setText("Latest Update: ");
    latestText->setStyleSheet("font-size: 15px; font-weight:600");
    updateText->setText("MED1234 Basic Medical Course has been cancelled.");
    updateText->setStyleSheet("font-size: 15px; font-weight:600; color:red");
    updateLayout->addWidget(latestText);
    updateLayout->setAlignment(latestText, Qt::AlignLeft);
    updateLayout->addWidget(updateText);
    updateLayout->setAlignment(updateText, Qt::AlignLeft);

    //Set up date display
    nowDate->setText(today.toString("dd MMMM yyyy (dddd)"));
    nowDate->setStyleSheet("font-size: 15px; font-weight:600");
    topRight->setAlignment(nowDate, Qt::AlignRight);

    this->setLayout(layout);
}

//public method
void TopWidget::setSearchBarVisibility(bool isVisible) {
    if (isVisible) {
        _searchBar->show();
    } else {
        _searchBar->hide();
    }
}

/*
 * Private Slots
 */
void TopWidget::clickedIstc() {
    emit this->wantMain();
}

void TopWidget::clickedProfileLogo() {
    emit this->wantProfile();
}

void TopWidget::clickedRefresh() {
    SyncBox *syncBox = new SyncBox;

    connect (syncBox, SIGNAL(receivedSuccessSyncSignal()),
             this, SLOT(handleSuccessSyncSignal()));
    syncBox->exec();
}

void TopWidget::clickedSettings() {
    SettingsBox *settingsBox = new SettingsBox;
    settingsBox->exec();
}

void TopWidget::clickedLogout() {
    LogoutBox *logoutBox = new LogoutBox;
    logoutBox->exec();
}

void TopWidget::enteredSearch() {
    SearchPopupBox *searchPopupBox = new SearchPopupBox(_searchLine->text());
    searchPopupBox->exec();
}

void TopWidget::handleSuccessSyncSignal() {
    emit this->wantSync();
}
