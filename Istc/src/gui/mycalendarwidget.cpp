#include "gui/mycalendarwidget.h"

const char *MyCalendarWidget::ICON_PREVMONTH = ":/icon/ButtonIcon/Prev Month Button.png";
const char *MyCalendarWidget::ICON_NEXTMONTH = ":/icon/ButtonIcon/Next Month Button.png";

const int MyCalendarWidget::HEIGHT_NAVBAR = 30;
const int MyCalendarWidget::HEIGHT_PREVNEXTMONTHICON = 20;
const int MyCalendarWidget::SIZE_CURRENTDATEFONT = 13;
const int MyCalendarWidget::SIZE_HORIZONTALHEADERFONT = 12;
const int MyCalendarWidget::SIZE_NAVBARFONT = 20;
const int MyCalendarWidget::SIZE_ORIPREVNEXTMONTHICON = 0;
const int MyCalendarWidget::WIDTH_PREVNEXTMONTHICON = 30;

const QColor MyCalendarWidget::COLOR_HORIZONTALHEADERBACKGROUND = QColor(26, 188, 156);
const QColor MyCalendarWidget::COLOR_HORIZONTALHEADERTEXT = QColor(Qt::white);
const QColor MyCalendarWidget::COLOR_CALENDARVIEWEVENROW = QColor(236, 240, 241, 255);
const QColor MyCalendarWidget::COLOR_CALENDARVIEWODDROW = QColor(189, 195, 199, 255);
const QColor MyCalendarWidget::COLOR_CALENDARVIEWOTHERTEXT = QColor(Qt::gray);
const QColor MyCalendarWidget::COLOR_CALENDARVIEWTHISMONTHTEXT = QColor(Qt::black);

const QString MyCalendarWidget::COLOR_NAVBARBACKGROUND = "rgb(52, 73, 94)";

MyCalendarWidget::MyCalendarWidget() {

    connect(this, SIGNAL(darkGreyWeekOfYearChanged(int)), this, SLOT(setDarkGreyWeekOfYear(int)));
    connect(this, SIGNAL(lightGreyWeekOfYearChanged(int)), this, SLOT(setLightGreyWeekOfYear(int)));

    this->setFirstDayOfWeek(Qt::Monday);
    this->setHorizontalHeaderFormat(QCalendarWidget::SingleLetterDayNames);
    this->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
    this->setVerticalHeaderFormat(QCalendarWidget::VerticalHeaderFormat(0));

    setNavigationBarAppearance();
    setHorizontalHeaderAppearance();
}

MyCalendarWidget::~MyCalendarWidget() {

}

void MyCalendarWidget::setNavigationBarAppearance() {
    setNavigationBarBackgroundColour();
    setNavigationBarTextColor();
    setPrevMonthButton();
    setNextMonthButton();
}

void MyCalendarWidget::setNavigationBarBackgroundColour() {
    QWidget *navBar = this->findChild<QWidget *>("qt_calendar_navigationbar");
    navBar->setStyleSheet("background-color:" + COLOR_NAVBARBACKGROUND + ";");
}

void MyCalendarWidget::setNavigationBarTextColor() {
    QWidget *yearButton = this->findChild<QWidget *>("qt_calendar_yearbutton");
    yearButton->setStyleSheet("min-height:" + QString::number(HEIGHT_NAVBAR) + "px;"
                              "font:" + QString::number(SIZE_NAVBARFONT) + "px;");

    QWidget *monthButton = this->findChild<QWidget *>("qt_calendar_monthbutton");
    monthButton->setStyleSheet("min-height:" + QString::number(HEIGHT_NAVBAR) + "px;"
                               "font:" + QString::number(SIZE_NAVBARFONT) + "px;");
    monthButton->setCursor(Qt::PointingHandCursor);

    QWidget *yearEdit = this->findChild<QWidget *>("qt_calendar_yearedit");
    yearEdit->setStyleSheet("min-height:" + QString::number(HEIGHT_NAVBAR) + "px;"
                            "font:" + QString::number(SIZE_NAVBARFONT) + "px;");
    yearEdit->setCursor(Qt::PointingHandCursor);
}

void MyCalendarWidget::setPrevMonthButton() {
    QWidget *prevMonthButton = this->findChild<QWidget *>("qt_calendar_prevmonth");
    prevMonthButton->setStyleSheet("border-image: url(" + QString::fromLatin1(ICON_PREVMONTH) + ");"
                                   "icon-size:" + QString::number(SIZE_ORIPREVNEXTMONTHICON) + "px;"
                                   "width:" + QString::number(WIDTH_PREVNEXTMONTHICON) + "px;"
                                   "height:" + QString::number(HEIGHT_PREVNEXTMONTHICON) + "px;"
                                   );
    prevMonthButton->setCursor(Qt::PointingHandCursor);
}

void MyCalendarWidget::setNextMonthButton() {
    QWidget *nextMonthButton = this->findChild<QWidget *>("qt_calendar_nextmonth");
    nextMonthButton->setStyleSheet("border-image: url(" + QString::fromLatin1(ICON_NEXTMONTH) + ");"
                                   "icon-size:" + QString::number(SIZE_ORIPREVNEXTMONTHICON) + "px;"
                                   "width:" + QString::number(WIDTH_PREVNEXTMONTHICON) + "px;"
                                   "height:" + QString::number(HEIGHT_PREVNEXTMONTHICON) + "px;"
                                   );
    nextMonthButton->setCursor(Qt::PointingHandCursor);
}

void MyCalendarWidget::setHorizontalHeaderAppearance() {
    QBrush hHeaderBgBrush;
    hHeaderBgBrush.setColor(COLOR_HORIZONTALHEADERBACKGROUND);

    QBrush hHeaderTextBrush;
    hHeaderTextBrush.setColor(COLOR_HORIZONTALHEADERTEXT);

    QTextCharFormat textFormat;
    textFormat.setBackground(hHeaderBgBrush);
    textFormat.setForeground(hHeaderTextBrush);
    textFormat.setFontWeight(QFont::Bold);
    textFormat.setFontPointSize(SIZE_HORIZONTALHEADERFONT);

    this->setWeekdayTextFormat(Qt::Monday, textFormat);
    this->setWeekdayTextFormat(Qt::Tuesday, textFormat);
    this->setWeekdayTextFormat(Qt::Wednesday, textFormat);
    this->setWeekdayTextFormat(Qt::Thursday, textFormat);
    this->setWeekdayTextFormat(Qt::Friday, textFormat);
    this->setWeekdayTextFormat(Qt::Saturday, textFormat);
    this->setWeekdayTextFormat(Qt::Sunday, textFormat);
}

void MyCalendarWidget::paintCell(QPainter *painter, const QRect &rect, const QDate &date) const {

    if (date.addMonths(1).month() == this->monthShown()) { //current cell date is last month date
        emit darkGreyWeekOfYearChanged(date.weekNumber());
        emit lightGreyWeekOfYearChanged(date.weekNumber() + 1);
    }

    if (date.weekNumber() == _darkGreyWeekOfYear[0]
            || date.weekNumber() == _darkGreyWeekOfYear[1]
            || date.weekNumber() == _darkGreyWeekOfYear[2]) { // odd row
        painter->fillRect(rect, COLOR_CALENDARVIEWODDROW);
        painter->save();

        setPainterFontAndPen(date, painter);

        painter->drawText(rect, Qt::AlignCenter ,QString::number(date.day()));
        painter->restore();
    } else if (date.weekNumber() == _lightGreyWeekOfYear[0]
                    || date.weekNumber() == _lightGreyWeekOfYear[1]
                    || date.weekNumber() == _lightGreyWeekOfYear[2]) { // even row
        painter->fillRect(rect, COLOR_CALENDARVIEWEVENROW);
        painter->save();

        setPainterFontAndPen(date, painter);

        painter->drawText(rect, Qt::AlignCenter ,QString::number(date.day()));
        painter->restore();
    }
}

void MyCalendarWidget::setPainterFontAndPen(const QDate &date, QPainter *painter) const
{
    QFont dateFont = QFont();
    if (date == QDate::currentDate()) {
        dateFont.setBold(true);
        dateFont.setPointSize(SIZE_CURRENTDATEFONT);
        painter->setFont(dateFont);
    }

    if (date == this->selectedDate()) {
        dateFont.setUnderline(true);
        painter->setFont(dateFont);
    } else if (date.month() == this->monthShown()) {
        painter->setPen(COLOR_CALENDARVIEWTHISMONTHTEXT);
    } else {
        painter->setPen(COLOR_CALENDARVIEWOTHERTEXT);
    }
}

void MyCalendarWidget::setDarkGreyWeekOfYear(int darkGreyWeekOfYear) {
    int weekNumber = countWeekNumber();
    _darkGreyWeekOfYear[0] = (darkGreyWeekOfYear - 1) % weekNumber + 1;
    _darkGreyWeekOfYear[1] = (_darkGreyWeekOfYear[0] + 1) % weekNumber + 1;
    _darkGreyWeekOfYear[2] = (_darkGreyWeekOfYear[1] + 1) % weekNumber + 1;
}

void MyCalendarWidget::setLightGreyWeekOfYear(int lightGreyWeekOfYear) {
    int weekNumber = countWeekNumber();
    _lightGreyWeekOfYear[0] = (lightGreyWeekOfYear - 1) % weekNumber + 1;
    _lightGreyWeekOfYear[1] = (_lightGreyWeekOfYear[0] + 1) % weekNumber + 1;
    _lightGreyWeekOfYear[2] = (_lightGreyWeekOfYear[1] + 1) % weekNumber + 1;
}

int MyCalendarWidget::countWeekNumber() {
    QDate DEC25;
    QDate DEC31;
    if (this->monthShown() == 1) { // currently showing Jan
        // refer weekNumber to last year week number
        DEC25 = QDate(this->yearShown() - 1, 12, 25);
        DEC31 = QDate(this->yearShown() - 1, 12, 31);
    } else { // currently showing other than December
        // refer weekNumber to current year week number
        DEC25 = QDate(this->yearShown(), 12, 25);
        DEC31 = QDate(this->yearShown(), 12, 31);
    }
    return std::max(DEC25.weekNumber(), DEC31.weekNumber());
}
