#include "gui/notesbox.h"

const QString NotesBox::COLOR_BUTTONBACKGROUND = "rgb(39, 174, 96)";
const QString NotesBox::COLOR_BUTTONBORDER = "white";
const QString NotesBox::COLOR_BUTTONFONT = "white";
const QString NotesBox::FONT_BUTTON = "bold 14";
const QString NotesBox::PADDING_BUTTON = "6";
const QString NotesBox::RADIUS_BUTTONBORDER = "10";
const QString NotesBox::STYLE_BUTTONBORDER = "outset";
const QString NotesBox::STRING_SPACE = " ";
const QString NotesBox::WIDTH_BUTTON = "50";
const QString NotesBox::WIDTH_BUTTONBORDER = "2";
const QString NotesBox::QSTRING_EMPTY = "";

const int NotesBox::WIDTH_MINIMUM = 500;
const int NotesBox::HEIGHT_MINIMUM = 300;

NotesBox::NotesBox() {
    initializeVariable();
    setUpDateLabel();
    setUpNoteTitleInput();
    setUpNoteContentInput();
    setUpErrorLabel();
    setUpAddAndCancelButton();
    setNotesBoxAttribute();
}

NotesBox::NotesBox(QString notesDate, QString notesTitle, QString notesContent) {
    initializeVariable();
    setUpDateLabel(notesDate);
    setUpNoteTitleInput(notesTitle);
    setUpNoteContentInput(notesContent);
    setUpErrorLabel();
    setUpUpdateAndCancelButton();
    setNotesBoxAttribute();
}

NotesBox::~NotesBox() {
    delete _dateLabel;
    delete _inputTitle;
    delete _inputNote;
    delete _errorLabel;
    delete _addButton;
    delete _cancelButton;
    delete _boxLayout;
}

//private
void NotesBox::initializeVariable() {
    _titleString = QSTRING_EMPTY;
    _noteString = QSTRING_EMPTY;
    _bold.setBold(true);
    _boxLayout = new QVBoxLayout;
}

void NotesBox::setUpDateLabel(const QString &notesDate) {
    //notes Date format to be "yyyy M d"
    if(notesDate.isEmpty()) {
        _date = QDate::currentDate();
    } else {
        qDebug() << "NotesBox -> setUpDateLabel: Finished setting previously added notes date";
        try {
            QStringList date = notesDate.split(STRING_SPACE);
            int year = date.at(0).toInt();
            int month = date.at(1).toInt();
            int day = date.at(2).toInt();
            _date = QDate(year, month, day);
            qDebug() << "NotesBox -> setUpDateLabel: Finished setting previously added notes date";
        }
        catch (int e) {
            qDebug() << "NotesBox -> setUpDateLabel: Error setting date";
        }
    }

    _dateString = _date.toString("dd MMM yyyy");

    //setup the first row
    _dateLabel = new QLabel;
    _dateLabel->setText(_dateString);
    _dateLabel->setFont(_bold);
    _boxLayout->addWidget(_dateLabel);
}

void NotesBox::setUpNoteTitleInput(const QString &notesTitle) {
    _inputTitle = new InputBox;
    _inputTitle->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    _inputTitle->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    _inputTitle->setFrameStyle(QFrame::Box);
    _inputTitle->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
    _inputTitle->setFixedHeight(30);
    _inputTitle->setPlaceholderText("Title of Note");

    if(!notesTitle.isEmpty()) {
        _inputTitle->setText(notesTitle);
    }

    _boxLayout->addWidget(_inputTitle);

    connect(_inputTitle, SIGNAL(stringReady(QString)),
            this, SLOT(handleTitle(QString)));
}

void NotesBox::setUpNoteContentInput(const QString &notesContent) {
    _inputNote = new QTextEdit;
    _inputNote->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    _inputNote->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    _inputNote->setFrameStyle(QFrame::Box);
    _inputNote->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
    _inputNote->setFixedHeight(300);
    _inputNote->setFixedWidth(WIDTH_MINIMUM);
    _inputNote->setPlaceholderText("Your Note Here");
    _inputNote->setLineWrapMode(QTextEdit::WidgetWidth);

    if(!notesContent.isEmpty()) {
        _inputNote->setText(notesContent);
    }

    _boxLayout->addWidget(_inputNote);
}

void NotesBox::setUpErrorLabel() {
    _errorLabel = new QLabel;
    _errorLabel->setFont(_bold);
    NotesBox::changeColor(Qt::red, Qt::white, _errorLabel);
    _errorLabel->hide();
    _boxLayout->addWidget(_errorLabel);
}

void NotesBox::setUpAddAndCancelButton() {
    //Add button
    _addButton = new QPushButton;
    _addButton->setText("Add");
    _addButton->setCursor(Qt::PointingHandCursor);
    setButtonStyleSheet(_addButton);
    connect (_addButton, SIGNAL(clicked()),
             this, SLOT(handleAddOrUpdate()));
    _addButton->setSizePolicy(QSizePolicy::Fixed,
                              QSizePolicy::Fixed);

    //cancel
    _cancelButton = new QPushButton;
    _cancelButton->setText("Cancel");
    _cancelButton->setCursor(Qt::PointingHandCursor);
    setButtonStyleSheet(_cancelButton);
    connect (_cancelButton, SIGNAL(clicked()),
             this, SLOT(handleCancel()));
    _cancelButton->setSizePolicy(QSizePolicy::Fixed,
                                 QSizePolicy::Fixed);

    QHBoxLayout *buttonLayout = new QHBoxLayout;
    buttonLayout->addWidget(_addButton);
    buttonLayout->addWidget(_cancelButton);
    buttonLayout->setSpacing(0);
    _boxLayout->addLayout(buttonLayout);
}

void NotesBox::setUpUpdateAndCancelButton() {
    //Update button
    _addButton = new QPushButton;
    _addButton->setText("Update");
    _addButton->setCursor(Qt::PointingHandCursor);
    setButtonStyleSheet(_addButton);
    connect (_addButton, SIGNAL(clicked()),
             this, SLOT(handleAddOrUpdate()));
    _addButton->setSizePolicy(QSizePolicy::Fixed,
                              QSizePolicy::Fixed);

    //cancel
    _cancelButton = new QPushButton;
    _cancelButton->setText("Cancel");
    _cancelButton->setCursor(Qt::PointingHandCursor);
    setButtonStyleSheet(_cancelButton);
    connect (_cancelButton, SIGNAL(clicked()),
             this, SLOT(handleCancel()));
    _cancelButton->setSizePolicy(QSizePolicy::Fixed,
                                 QSizePolicy::Fixed);

    QHBoxLayout *buttonLayout = new QHBoxLayout;
    buttonLayout->addWidget(_addButton);
    buttonLayout->addWidget(_cancelButton);
    buttonLayout->setSpacing(0);
    _boxLayout->addLayout(buttonLayout);
}

void NotesBox::setNotesBoxAttribute() {
    this->setLayout(_boxLayout);
    this->setContentsMargins(10, 10, 10, 10);
    this->setMinimumWidth(WIDTH_MINIMUM);
    this->setMinimumHeight(HEIGHT_MINIMUM);

    this->setAttribute(Qt::WA_TranslucentBackground, true);
    this->setWindowFlags(Qt::FramelessWindowHint);
}

void NotesBox::paintEvent(QPaintEvent *event) {
    QDialog::paintEvent(event);
    QPainter painter(this);
    QBrush brush(Qt::white);
    painter.setBrush(brush);
    painter.setPen(Qt::white);
    painter.drawRoundRect(QRect(0, 0, this->width(), this->height()));
}

//private
void NotesBox::changeColor(QColor font, QColor back, QWidget *widget) {
    widget->setAutoFillBackground(true);
    QPalette pallete = widget->palette();
    pallete.setColor(widget->foregroundRole(), font);
    pallete.setColor(widget->backgroundRole(), back);
    widget->setPalette(pallete);
}

void NotesBox::setButtonStyleSheet(QPushButton *button) {
    button->setStyleSheet("background-color:" + COLOR_BUTTONBACKGROUND + ";"
                          "border-style:" + STYLE_BUTTONBORDER + ";"
                          "border-width:" + WIDTH_BUTTONBORDER + "px;"
                          "border-radius:" + RADIUS_BUTTONBORDER + "px;"
                          "border-color:" + COLOR_BUTTONBORDER + ";"
                          "font:" + FONT_BUTTON + "px;"
                          "color:" + COLOR_BUTTONFONT + ";"
                          "min-width:" + WIDTH_BUTTON + "px;"
                          "padding:" + PADDING_BUTTON + "px;");
}

//private slots
void NotesBox::handleTitle(QString inputString) {
    _titleString = inputString;
    _inputTitle->setPlainText(inputString);
}

void NotesBox::handleAddOrUpdate() {
    QVector<QString> info;

    _dateString = QDate::currentDate().toString("yyyy M d");
    _titleString = _inputTitle->toPlainText();
    _noteString = _inputNote->toPlainText();

    if (_titleString == QSTRING_EMPTY && _noteString == QSTRING_EMPTY) {
        _errorLabel->show();
        _errorLabel->setText("Please add a title and note content");
    } else if (_titleString == QSTRING_EMPTY) {
        _errorLabel->show();
        _errorLabel->setText("Title of note required");
    } else if (_noteString == QSTRING_EMPTY) {
        _errorLabel->show();
        _errorLabel->setText("Note content empty");
    } else {
        qDebug() << "NotesBox::handleAddOrUpdate: " << _dateString;
        info.append(_dateString);
        info.append(_titleString);
        info.append(_noteString);

        emit this->passInfo(info);
        QDialog::done(0);
    }
}

void NotesBox::handleCancel() {
    QDialog::done(0);
}
