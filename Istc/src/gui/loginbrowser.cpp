#include "gui/loginbrowser.h"

const int LoginBrowser::FONT_SIZE = 17;

const QString LoginBrowser::LABEL_ERROR = "ERROR! Check your Internet Connection";
const QString LoginBrowser::LABEL_LOADING = "LOADING PAGE.....";

const QString LoginBrowser::LABEL_BACK = QString::fromUtf8(":/icon/ButtonIcon/back.png");
const QString LoginBrowser::LABEL_HOME = QString::fromUtf8(":/icon/ButtonIcon/home.png");
const QString LoginBrowser::LABEL_FORWARD = QString::fromUtf8(":/icon/ButtonIcon/forward.png");

LoginBrowser::LoginBrowser() {
    _font.setBold(true);
    _font.setPointSize(FONT_SIZE);

    LoginBrowser::initializeBrowser();
}

LoginBrowser::~LoginBrowser() {
    clearGarbage();
}

//public:
void LoginBrowser::load(QUrl url) {
    _browserView->load(url);
}

void LoginBrowser::clearCookie() {

    _browserView->page()->
            networkAccessManager()->
            setCookieJar(new QNetworkCookieJar());

}

void LoginBrowser::hideHomeButton() {
    _homeClick->hide();
}

//private methods:
void LoginBrowser::closeWebView() {
    clearCookie();
    _browserView->deleteLater();
}

void LoginBrowser::clearGarbage() {
    closeWebView();

    delete _backLabel;
    delete _backClick;
    delete _forwardLabel;
    delete _loadingLabel;
    delete _loadErrorLabel;
    delete _forwardClick;
    delete _homeLabel;
    delete _homeClick;
    delete _buttonLayout;
    delete _browserLayout;
}

void LoginBrowser::initializeBrowser() {

    _browserView = new QWebView;
    connect (_browserView, SIGNAL(urlChanged(QUrl)),
             this, SLOT(timeToChangeUrl(QUrl)));
    connect (_browserView, SIGNAL(loadFinished(bool)),
             this, SLOT(loadStatus(bool)));
    connect (_browserView, SIGNAL(loadStarted()),
             this, SLOT(loadStart()));


    initiateBack();
    initiateForward();
    initiateHome();
    initiateLoadingLabel();
    initiateLoadErrorLabel();
    initiateButtonsLayout();
    initiateBrowserLayout();
    setupCertificate();

    this->setLayout(_browserLayout);
}

void LoginBrowser::initiateBack(){
    _backLabel = new QLabel;
    _backLabel->setPixmap(QPixmap(LABEL_BACK));
    _backClick = new ClickableWidget(_backLabel);


    connect (_backClick, SIGNAL(isClicked()),
             _browserView, SLOT(back()));

}

void LoginBrowser::initiateForward() {
    _forwardLabel = new QLabel;
    _forwardLabel->setPixmap(QPixmap(LABEL_FORWARD));
    _forwardClick = new ClickableWidget(_forwardLabel);


    connect (_forwardClick, SIGNAL(isClicked()),
             _browserView, SLOT(forward()));

}

void LoginBrowser::initiateHome() {
    _homeLabel = new QLabel;
    _homeLabel->setPixmap(QPixmap(LABEL_HOME));
    _homeClick = new ClickableWidget(_homeLabel);
    connect (_homeClick, SIGNAL(isClicked()),
             this, SLOT(timeToShowWidget()));
}

void LoginBrowser::initiateLoadingLabel() {
    _loadingLabel = new QLabel;
    _loadingLabel->setText(LABEL_LOADING);
    _loadingLabel->setFont(_font);

    QPalette palette = _loadingLabel->palette();
    palette.setColor(_loadingLabel->foregroundRole(), Qt::blue);
    _loadingLabel->setPalette(palette);
    _loadingLabel->hide();
}

void LoginBrowser::initiateLoadErrorLabel() {
    _loadErrorLabel = new QLabel;
    _loadErrorLabel->setText(LABEL_ERROR);
    _loadErrorLabel->setFont(_font);

    QPalette palette = _loadErrorLabel->palette();
    palette.setColor(_loadErrorLabel->foregroundRole(), Qt::red);
    _loadErrorLabel->setPalette(palette);
    _loadErrorLabel->hide();
}

void LoginBrowser::initiateButtonsLayout() {
    _buttonLayout = new QHBoxLayout;
    _buttonLayout->addWidget(_backClick);
    _buttonLayout->addWidget(_forwardClick);
    _buttonLayout->addWidget(_homeClick);
    _buttonLayout->addWidget(_loadingLabel);
    _buttonLayout->addWidget(_loadErrorLabel);
    _buttonLayout->setSpacing(0);
}

void LoginBrowser::initiateBrowserLayout() {
    _browserLayout = new QVBoxLayout;
    _browserLayout->addLayout(_buttonLayout, 1);

    _browserLayout->addWidget(_browserView, 10);
}

bool LoginBrowser::setupCertificate() {
    qDebug() << "LoginBrowser -> setupCertificate: "
                "setuping the certificate...";

    // Read the SSL certificate
    QFile file(":/ssl/LocalFile/bluebell.d1.comp.nus.edu.sg.crt");
    if (!file.open(QIODevice::ReadOnly)) {
        qDebug() << "LoginBrowser -> setupCertificate: "
                    "can't open .cert file";
        std::cout << "can't open cert file\n";
        return false;
    }
    const QByteArray bytes = file.readAll();

    qDebug() << "setup certificate success!!!\n";

    // Create a certificate object
    const QSslCertificate certificate(bytes);

    qDebug() << certificate.toText();

    QSslSocket::addDefaultCaCertificate(certificate);


    qDebug() << "LoginBrowser -> setupCertificate: "
                "done setuping the certificate";

    return true;
}


//private slots
void LoginBrowser::timeToShowWidget() {
    _browserView->stop();

    _backClick->hide();
    _forwardClick->hide();
    _homeClick->hide();

    clearCookie();
    _loadingLabel->hide();
    _loadErrorLabel->hide();
    emit this->showWidget();
}

void LoginBrowser::timeToChangeUrl(QUrl url) {
    emit this->urlChanged(url);
}

void LoginBrowser::loadStatus(bool success) {
    if (!success) {
        //QUrl url = _browserView->url();
        //emit this->urlChanged(url);
        _loadErrorLabel->show();
    }
    _loadingLabel->hide();
}

void LoginBrowser::loadStart() {
    _loadErrorLabel->hide();
    _loadingLabel->show();
}

