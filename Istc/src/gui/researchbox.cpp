#include "gui/researchbox.h"

const QString ResearchBox::COLOR_BUTTONBACKGROUND = "rgb(39, 174, 96)";
const QString ResearchBox::COLOR_BUTTONBORDER = "white";
const QString ResearchBox::COLOR_BUTTONFONT = "white";
const QString ResearchBox::FONT_BUTTON = "bold 14";
const QString ResearchBox::PADDING_BUTTON = "6";
const QString ResearchBox::RADIUS_BUTTONBORDER = "10";
const QString ResearchBox::STYLE_BUTTONBORDER = "outset";
const QString ResearchBox::WIDTH_BUTTON = "50";
const QString ResearchBox::WIDTH_BUTTONBORDER = "2";

const QString ResearchBox::QSTRING_EMPTY = "";
const QString ResearchBox::QSTRING_UPDATE = "Research Progress updated!";

const int ResearchBox::WIDTH_MINIMUM = 500;
const int ResearchBox::HEIGHT_MINIMUM = 350;

ResearchBox::ResearchBox(QString contentLabel, QString details) {
    _bold.setBold(true);

    _state = contentLabel;
    QString contentString = "Stage: " + _state;
    _contentLabel = new QLabel(contentString);
    _contentLabel->setFont(_bold);
    _details = details;

    setUpDetailsLayout();
    setUpAddCancelLayout();
    setUpMainLayout();
}

ResearchBox::~ResearchBox() {
    delete _detailsBox;
    delete _contentLabel;
    delete _detailsLabel;
    delete _statusLabel;

    delete _addButton;
    delete _cancelButton;
    delete _statusBox;

    delete _mainLayout;
    delete _addCancelLayout;
    delete _detailsLayout;
    delete _statusLayout;
}

/*
 * Setting up the popup box
 */

void ResearchBox::setUpDetailsLayout() {
    _detailsLabel = new QLabel("Details: ");
    _detailsLabel->setFont(_bold);

    _detailsBox = new InputBox;
    _detailsBox->setText(_details);
   // _detailsBox->setPlaceholderText(_details);
    connect(_detailsBox, SIGNAL(textChanged(QString)),
            this, SLOT(handleContent()));

    _detailsLayout = new QVBoxLayout();
    _detailsLayout->addWidget(_detailsLabel);
    _detailsLayout->addWidget(_detailsBox);
}

void ResearchBox::setUpAddCancelLayout() {
    //Add button
    _addButton = new QPushButton;
    _addButton->setText("Update");
    _addButton->setCursor(Qt::PointingHandCursor);
    setButtonStyleSheet(_addButton);
    connect (_addButton, SIGNAL(clicked()),
             this, SLOT(handleButton()));
    _addButton->setSizePolicy(QSizePolicy::Fixed,
                              QSizePolicy::Fixed);

    //cancel
    _cancelButton = new QPushButton;
    _cancelButton->setText("Cancel");
    _cancelButton->setCursor(Qt::PointingHandCursor);
    setButtonStyleSheet(_cancelButton);
    connect (_cancelButton, SIGNAL(clicked()),
             this, SLOT(handleCancel()));
    _cancelButton->setSizePolicy(QSizePolicy::Fixed,
                                 QSizePolicy::Fixed);

    _addCancelLayout = new QHBoxLayout;
    _addCancelLayout->addWidget(_addButton);
    _addCancelLayout->addWidget(_cancelButton);
    _addCancelLayout->setSpacing(0);
}

void ResearchBox::setUpMainLayout() {
    _mainLayout = new QVBoxLayout;

    _mainLayout->addWidget(_contentLabel);
    _mainLayout->addLayout(_detailsLayout);
    _mainLayout->addLayout(_addCancelLayout);
    //_addButton->hide();

    this->setLayout(_mainLayout);
    this->setContentsMargins(10, 10, 10, 10);
    this->setMinimumWidth(WIDTH_MINIMUM);
    this->setMinimumHeight(HEIGHT_MINIMUM);

    this->setAttribute(Qt::WA_TranslucentBackground, true);

    this->setWindowFlags(Qt::FramelessWindowHint);
}

/*
 * Slots
 */

void ResearchBox::handleContent() {
    _addButton->show();
    //_details = content;
}

void ResearchBox::handleButton() {
    QString detailsBoxString = _detailsBox->toPlainText();
    if (detailsBoxString != "") {
        _details = detailsBoxString;
    }
    emit this->passInfo(_state, _details);
    QDialog::done(0);
}

void ResearchBox::handleStatusChange() {
    _status = _statusBox->currentText();
    _addButton->show();
}

void ResearchBox::handleCancel() {
    emit this->passInfo(_state, _details);

    QDialog::done(0);
}


/*
 * Helper functions
 */
void ResearchBox::paintEvent(QPaintEvent *event) {
    QDialog::paintEvent(event);
    QPainter painter(this);
    QBrush brush(Qt::white);
    painter.setBrush(brush);
    painter.setPen(Qt::white);
    painter.drawRoundRect(QRect(0, 0, this->width(), this->height()));
}

void ResearchBox::changeColor(QColor font, QColor back, QWidget *widget) {
    widget->setAutoFillBackground(true);
    QPalette pallete = widget->palette();
    pallete.setColor(widget->foregroundRole(), font);
    pallete.setColor(widget->backgroundRole(), back);
    widget->setPalette(pallete);
}

void ResearchBox::setButtonStyleSheet(QPushButton *button) {
    button->setStyleSheet("background-color:" + COLOR_BUTTONBACKGROUND + ";"
                          "border-style:" + STYLE_BUTTONBORDER + ";"
                          "border-width:" + WIDTH_BUTTONBORDER + "px;"
                          "border-radius:" + RADIUS_BUTTONBORDER + "px;"
                          "border-color:" + COLOR_BUTTONBORDER + ";"
                          "font:" + FONT_BUTTON + "px;"
                          "color:" + COLOR_BUTTONFONT + ";"
                          "min-width:" + WIDTH_BUTTON + "px;"
                          "padding:" + PADDING_BUTTON + "px;");
}
