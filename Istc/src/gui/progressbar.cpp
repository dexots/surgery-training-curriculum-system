#include "gui/progressbar.h"

ProgressBar::ProgressBar() {
    ProgressBar::initializeProgressBar();
}

ProgressBar::~ProgressBar() {
}

//public methods
void ProgressBar::setBadToGood(bool isBadToGood) {
    _isBadToGood = isBadToGood;
}

void ProgressBar::setValue(int value) {
    QProgressBar::setValue(value);
    ProgressBar::setTextInBar(value);
    ProgressBar::changeProgressBarColor(value);
}

void ProgressBar::setTextInBar(int value) {
    QString text = QString::number(value) + " out of " + QString::number(this->maximum());
    QProgressBar::setFormat(text);
}

//private methods
void ProgressBar::initializeProgressBar() {
    QString style = "QProgressBar { border: 2px solid white; border-radius: 5px; text-align: center;}";
    _styleBad = style + "QProgressBar::chunk { background-color: red; width: 10px;}";
    _styleGood = style + "QProgressBar::chunk { background-color: rgb(48, 230, 48); width: 10px;}";
    _styleOk = style + "QProgressBar::chunk { background-color: rgb(255, 192, 0); width: 10px;}";
    this->setStyleSheet(style);

}

void ProgressBar::changeProgressBarColor(int value) {
    int min = this->minimum();
    int max = this->maximum();

    if (value >= ceil(max / 3.0) && value <= floor(2 * max / 3.0)) {
        ProgressBar::colorProgressBar(_styleOk);
        return;
    }

    if (_isBadToGood) {
        if (value >= min && value <= floor(max / 3.0)) {
            ProgressBar::colorProgressBar(_styleBad);
        } else if (value >= ceil(2 * max / 3.0) && value <= max) {
            ProgressBar::colorProgressBar(_styleGood);
        }
    } else {
        if (value >= min && value <= floor(max / 3.0)) {
            ProgressBar::colorProgressBar(_styleGood);
        } else if (value >= ceil(2 * max / 3.0) && value <= max) {
            ProgressBar::colorProgressBar(_styleBad);
        }
    }
}

void ProgressBar::colorProgressBar(QString style) {
    this->setStyleSheet(style);
}

void ProgressBar::addValue() {
    if (this->value() + 1 <= this->maximum()) {
        this->setValue(this->value() + 1);
        if (this->value() == this->maximum()) {
            emit reachMaxValue(true);
        }
    } else {
        // do nothing
    }
    emit isUpdated();
}

void ProgressBar::minusValue() {
    if (this->value() - 1 >= this->minimum()) {
        this->setValue(this->value() - 1);
        if (this->value() == this->minimum()) {
            emit reachMinValue(true);
        }
    } else {
        // do nothing
    }
    emit isUpdated();
}
