#include "gui/catebox.h"

const char *CateBox::FONT_NAME = "Gotham Rounded Medium";
const int CateBox::FONT_BUTTON_SIZE = 14;
const int CateBox::FONT_INPUT_AREA_SIZE = 12;

//public constructor
CateBox::CateBox(QString name, const char* icon, QColor color) {
    CateBox::preSetupCateBox(name, icon, color);
    CateBox::createMyself();
}

//protected constructor
CateBox::CateBox() {
}

//destructor
CateBox::~CateBox() {
}

//protected methods
void CateBox::preSetupCateBox(QString name, const char* icon, QColor color) {
    CateBox::setCategoryName(name);
    CateBox::setPixmap(icon);
    CateBox::setColor(color);
    CateBox::initializeClass();
}

void CateBox::setInputAs(QString originalInput) {
    _inputArea->setText(originalInput);
}

void CateBox::mousePressEvent(QMouseEvent *event) {
    if (event->button() == Qt::LeftButton) {
        emit this->isClicked();
    }
}

bool CateBox::event(QEvent *event) {
    QInputEvent* inputEvent = dynamic_cast<QInputEvent*>(event);

    if (inputEvent) {
        switch (inputEvent->type()) {
            case QEvent::TouchBegin: {
                CateBox::handleTouch();
                return true;
            }

            case QEvent::MouseButtonPress: {
                QMouseEvent* mouseEvent = dynamic_cast<QMouseEvent*>(inputEvent );
                CateBox::mousePressEvent(mouseEvent);
                return true;
            }

            default: {
                return false;
            }
        }
    }
    return false;
}

void CateBox::handleTouch() {
    emit this->isClicked();
}

void CateBox::colorWidget(QColor background) {
    QPalette pallete = this->palette();
    pallete.setColor(this->backgroundRole(), background);
    pallete.setColor(this->foregroundRole(), Qt::black);
    this->setPalette(pallete);
}

void CateBox::createMyself() {
    QLayout *widgetLayout = CateBox::setupCateBoxLayout();
    this->setLayout(widgetLayout);
}

QLayout *CateBox::setupCateBoxLayout() {
    CateBox::setupInputArea();
    CateBox::setupIcon();
    CateBox::setupButton();
    return CateBox::createWidgetLayout();
}

//private methods
void CateBox::initializeClass() {
    _hLayout = new QHBoxLayout;
    _vLayout = new QVBoxLayout;
    _button = new QLabel;
    _buttonIcon = new QLabel;
    _inputArea = new QLabel;

    CateBox::initializeWidget();
}

void CateBox::setCategoryName(QString nameOfCategory) {
    _nameOfCategory = nameOfCategory;
}

void CateBox::setPixmap(const char* pixmap) {
    _pixmap = QPixmap(QString::fromUtf8(pixmap));;
}

void CateBox::setColor(QColor colorOfWidget) {
    _colorOfWidget = colorOfWidget;
}

void CateBox::setupInputArea() {
    QFont displayFont(FONT_NAME, FONT_INPUT_AREA_SIZE);

    _inputArea->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
    _inputArea->setAlignment(Qt::AlignTop | Qt::AlignLeft);
    _inputArea->setWordWrap(true);
    _inputArea->setFont(displayFont);

    _hLayout->addWidget(_inputArea);
}

void CateBox::setupButton() {
    QFont buttonFont(FONT_NAME, FONT_BUTTON_SIZE, QFont::Bold);

    _button->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
    _button->setAlignment(Qt::AlignBottom | Qt::AlignRight);
    _button->setText(_nameOfCategory);
    _button->setFont(buttonFont);
    _button->setWordWrap(true);

    _vLayout->addWidget(_button);
}

void CateBox::setupIcon() {
    _buttonIcon->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::MinimumExpanding);
    _buttonIcon->setPixmap(_pixmap);
    _buttonIcon->setAlignment(Qt::AlignBottom | Qt::AlignRight);
    _vLayout->addWidget(_buttonIcon);
}

QLayout *CateBox::createWidgetLayout() {
    _hLayout->addLayout(_vLayout);
    return _hLayout;
}

void CateBox::initializeWidget(){
    this->setAttribute(Qt::WA_AcceptTouchEvents);
    this->setAutoFillBackground(true);
    CateBox::colorWidget(_colorOfWidget);}

