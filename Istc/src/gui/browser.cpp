#include "gui/browser.h"

const int Browser::FONT_SIZE = 17;

const QString Browser::LABEL_ERROR = "ERROR! Check your Internet Connection";
const QString Browser::LABEL_LOADING = "LOADING PAGE.....";

const QString Browser::LABEL_BACK = QString::fromUtf8(":/icon/ButtonIcon/back.png");
const QString Browser::LABEL_HOME = QString::fromUtf8(":/icon/ButtonIcon/home.png");
const QString Browser::LABEL_FORWARD = QString::fromUtf8(":/icon/ButtonIcon/forward.png");

Browser::Browser() {
    _font.setBold(true);
    _font.setPointSize(FONT_SIZE);

    Browser::initializeBrowser();
}

Browser::~Browser() {
    Browser::clearGarbage();
}

//public:
void Browser::load(QUrl url) {
    //_browserView->load(url);
    QDesktopServices::openUrl(url);

    timeToShowWidget();
}

void Browser::clearCookie() {
    /*
    _browserView->page()->
            networkAccessManager()->
            setCookieJar(new QNetworkCookieJar());
            */
}

void Browser::hideHomeButton() {
    _homeClick->hide();
}

//private methods:
void Browser::closeWebView() {
    clearCookie();
    //_browserView->deleteLater();
}

void Browser::clearGarbage() {
    closeWebView();

    delete _backLabel;
    delete _backClick;
    delete _forwardLabel;
    delete _loadingLabel;
    delete _loadErrorLabel;
    delete _forwardClick;
    delete _homeLabel;
    delete _homeClick;
    delete _buttonLayout;
    delete _browserLayout;
}

void Browser::initializeBrowser() {
    /*
    _browserView = new QWebView;
    connect (_browserView, SIGNAL(urlChanged(QUrl)),
             this, SLOT(timeToChangeUrl(QUrl)));
    connect (_browserView, SIGNAL(loadFinished(bool)),
             this, SLOT(loadStatus(bool)));
    connect (_browserView, SIGNAL(loadStarted()),
             this, SLOT(loadStart()));
    */

    Browser::initiateBack();
    Browser::initiateForward();
    Browser::initiateHome();
    Browser::initiateLoadingLabel();
    Browser::initiateLoadErrorLabel();
    Browser::initiateButtonsLayout();
    Browser::initiateBrowserLayout();

    this->setLayout(_browserLayout);
}

void Browser::initiateBack(){
    _backLabel = new QLabel;
    _backLabel->setPixmap(QPixmap(LABEL_BACK));
    _backClick = new ClickableWidget(_backLabel);

    /*
    connect (_backClick, SIGNAL(isClicked()),
             _browserView, SLOT(back()));
             */
}

void Browser::initiateForward() {
    _forwardLabel = new QLabel;
    _forwardLabel->setPixmap(QPixmap(LABEL_FORWARD));
    _forwardClick = new ClickableWidget(_forwardLabel);

    /*
    connect (_forwardClick, SIGNAL(isClicked()),
             _browserView, SLOT(forward()));
             */
}

void Browser::initiateHome() {
    _homeLabel = new QLabel;
    _homeLabel->setPixmap(QPixmap(LABEL_HOME));
    _homeClick = new ClickableWidget(_homeLabel);
    connect (_homeClick, SIGNAL(isClicked()),
             this, SLOT(timeToShowWidget()));
}

void Browser::initiateLoadingLabel() {
    _loadingLabel = new QLabel;
    _loadingLabel->setText(LABEL_LOADING);
    _loadingLabel->setFont(_font);

    QPalette palette = _loadingLabel->palette();
    palette.setColor(_loadingLabel->foregroundRole(), Qt::blue);
    _loadingLabel->setPalette(palette);
    _loadingLabel->hide();
}

void Browser::initiateLoadErrorLabel() {
    _loadErrorLabel = new QLabel;
    _loadErrorLabel->setText(LABEL_ERROR);
    _loadErrorLabel->setFont(_font);

    QPalette palette = _loadErrorLabel->palette();
    palette.setColor(_loadErrorLabel->foregroundRole(), Qt::red);
    _loadErrorLabel->setPalette(palette);
    _loadErrorLabel->hide();
}

void Browser::initiateButtonsLayout() {
    _buttonLayout = new QHBoxLayout;
    _buttonLayout->addWidget(_backClick);
    _buttonLayout->addWidget(_forwardClick);
    _buttonLayout->addWidget(_homeClick);
    _buttonLayout->addWidget(_loadingLabel);
    _buttonLayout->addWidget(_loadErrorLabel);
    _buttonLayout->setSpacing(0);
}

void Browser::initiateBrowserLayout() {
    _browserLayout = new QVBoxLayout;
    _browserLayout->addLayout(_buttonLayout, 1);

    //_browserLayout->addWidget(_browserView, 10);
}

//private slots
void Browser::timeToShowWidget() {
    //_browserView->stop();

    _backClick->hide();
    _forwardClick->hide();
    _homeClick->hide();

    clearCookie();
    _loadingLabel->hide();
    _loadErrorLabel->hide();
    emit this->showWidget();
}

void Browser::timeToChangeUrl(QUrl url) {
    emit this->urlChanged(url);
}

void Browser::loadStatus(bool success) {
    if (!success) {
        _loadErrorLabel->show();
    }
    _loadingLabel->hide();
}

void Browser::loadStart() {
    _loadErrorLabel->hide();
    _loadingLabel->show();
}
