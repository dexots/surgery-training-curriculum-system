#include "gui/yourrosterwidget.h"

const QString YourRosterWidget::COL0 = "Day";
const QString YourRosterWidget::COL1 = "Status";
const QString YourRosterWidget::LABEL_ADDROSTER = "Add Roster";

const char *YourRosterWidget::ICON_LEFT = ":/icon/ButtonIcon/Prev Month Button.png";
const char *YourRosterWidget::ICON_RIGHT = ":/icon/ButtonIcon/Next Month Button.png";
const char *YourRosterWidget::ICON_ADD = ":/icon/ButtonIcon/add.png";

const bool YourRosterWidget::HAS_HEADER = true;
const int YourRosterWidget::NO_OF_COL = 2;

const int YourRosterWidget::COL0_WIDTH = 150;
const int YourRosterWidget::COL1_WIDTH = 150;

const QColor YourRosterWidget::MONTH_BAR_BACK = QColor(52, 73, 94, 255);
const QColor YourRosterWidget::MONTH_BAR_FONT = Qt::white;
const QColor YourRosterWidget::HEADER_BACK = QColor(99, 145, 213, 255);
const QColor YourRosterWidget::HEADER_FONT = Qt::white;
const QColor YourRosterWidget::CONT_BACK = QColor(219, 237, 249, 255);
const QColor YourRosterWidget::CONT_FONT = Qt::black;
const QColor YourRosterWidget::MY_OWN_COLOR = QColor (145, 199, 235, 255);

YourRosterWidget::YourRosterWidget() {
    YourRosterWidget::initializeClass();
    YourRosterWidget::getEarliestAndLatestDate();
    YourRosterWidget::getContent();
    YourRosterWidget::setupTable();
    YourRosterWidget::setupMonthBar();
    YourRosterWidget::initializeDummyWidget();
    YourRosterWidget::initializeLayout();
    YourRosterWidget::createYourself();
    YourRosterWidget::addContents();
}

YourRosterWidget::~YourRosterWidget() {
    YourRosterWidget::clearGarbage();
}

//private methods:
void YourRosterWidget::initializeClass() {
    QVector<int> widthOfCol;
    widthOfCol.append(COL0_WIDTH);
    widthOfCol.append(COL1_WIDTH);
    _table = new TableWidget(HAS_HEADER,
                             NO_OF_COL,
                             widthOfCol);

    _table->setContentsMargins(0, 0, 0, 0);
    _todayDate = QDate::currentDate();
    _curDate = _todayDate;
    _rosterModel = RosterModel::getObject();
    _isRosterMonster = YourRosterWidget::getIsRosterMonster();

    _dummyMaxHeight = 0;
}

void YourRosterWidget::clearGarbage() {
    delete _dummyWidget;
    delete _table;
    delete _monthBarPrev;
    delete _monthBarHeader;
    delete _monthBarNext;
    delete _monthBarLayout;
    delete _monthBar;
    delete _rosterLayout;
    RosterModel::deleteObject();
}

bool YourRosterWidget::getIsRosterMonster() {
    UserDetails *userDetails = UserDetails::getObject();
    return userDetails->getIsRosterMonster();
}

void YourRosterWidget::getContent() {
    QVector <RosterAssignObj> rosterAssignObjs =
            _rosterModel->getYourRoster(_curDate);

    for (int i = 0; i < rosterAssignObjs.size(); i++) {
        QVector <QString> row;
        RosterAssignObj curObj = rosterAssignObjs.value(i);
        QString date = curObj.getAssignedDate().toString("dd, ddd");
        QString status = curObj.getStatus();
        row.append(date);
        row.append(status);
        _contents.append(row);
    }
}

void YourRosterWidget::getEarliestAndLatestDate() {
    int curYear = _curDate.year();
    int curMonth = _curDate.month();

    _earliestDate = QDate (curYear, curMonth, 1);
    _earliestDate = _earliestDate.addMonths(-1);

    _latestDate = _earliestDate.addMonths(3);
}

void YourRosterWidget::setupTable() {
    YourRosterWidget::initializeTableHeader();
    _table->setContBackground(CONT_BACK);
    _table->setContFont(CONT_FONT);

    connect (_table, SIGNAL(resizedTable(int)),
             this, SLOT(handleTableResized(int)));
}

void YourRosterWidget::initializeTableHeader() {
    QVector <QString>  headerCols;
    headerCols.append(COL0);
    headerCols.append(COL1);
    _table->setHeader(headerCols, HEADER_FONT, HEADER_BACK);
}

void YourRosterWidget::addContents() {
    for (int row = 0; row < _contents.size(); row++) {
        addContentRow(_contents.at(row), row);
    }
}

void YourRosterWidget::addContentRow(QVector<QString> contentRow, int rowId) {
    _dummyWidget->hide();


    _table->addRow(contentRow, rowId);

    QString content = contentRow.at(1);
    content = content.toLower();

    if (content == RosterAssignObj::STATUS_COMPLETED) {
        for (int col = 0; col < contentRow.size(); col++) {
            _table->colorCell(rowId, col, Qt::black, QColor (149, 231, 184, 255));
        }
    } else if (content == RosterAssignObj::STATUS_WAIT) {
        for (int col = 0; col < contentRow.size(); col++) {
            _table->colorCell(rowId, col, Qt::black, QColor (241, 196, 15, 255));
        }
    } else if (content == RosterAssignObj::STATUS_NOT_COMPLETED) {
        for (int col = 0; col < contentRow.size(); col++) {
            _table->colorCell(rowId, col, Qt::black, QColor (240, 146, 136, 255));
        }
    } else {
        //in case you add new stuff,
        //this assertion is to remind you to add it.
        //otherwise, it is impossible to reach here
        assert (false);
    }

    if (_dummyWidget->isHidden()) {
        _dummyWidget->show();
    }
}

void YourRosterWidget::setupMonthBar() {
    YourRosterWidget::initializeMonthBarHeader();
    YourRosterWidget::initializeMonthBarPrev();
    YourRosterWidget::initializeMonthBarNext();
    YourRosterWidget::initializeMonthBarLayout();
    YourRosterWidget::initializeMonthBar();
}

void YourRosterWidget::initializeMonthBarHeader() {
    _monthBarHeader = new QLabel;

    YourRosterWidget::setHeaderText();

    YourRosterWidget::colorWidget(MONTH_BAR_FONT,
                                  MONTH_BAR_BACK,
                                  _monthBarHeader);

    _monthBarHeader->setAlignment(Qt::AlignCenter);
    _monthBarHeader->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
}

void YourRosterWidget::setHeaderText() {
    QString monthBarHeader;
    QString curMonth = QDate::longMonthName(_curDate.month());
    QString curYear = QString::number(_curDate.year());
    monthBarHeader.append(curMonth);
    monthBarHeader.append(" ");
    monthBarHeader.append(curYear);
    _monthBarHeader->setText(monthBarHeader);
}

void YourRosterWidget::initializeMonthBarPrev() {
    QLabel *prevLabel = new QLabel();
    QPixmap prev = QPixmap(QString::fromUtf8(ICON_LEFT));
    prevLabel->setPixmap(prev);
    _monthBarPrev = new ClickableWidget(prevLabel);
    _monthBarPrev->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

    connect(_monthBarPrev, SIGNAL(isClicked()), this, SLOT(handlePrev()));
}

void YourRosterWidget::initializeMonthBarNext() {
    QLabel *nextLabel = new QLabel();
    QPixmap next = QPixmap(QString::fromUtf8(ICON_RIGHT));
    nextLabel->setPixmap(next);
    _monthBarNext = new ClickableWidget(nextLabel);
    _monthBarNext->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

    connect(_monthBarNext, SIGNAL(isClicked()), this, SLOT(handleNext()));
}

void YourRosterWidget::initializeMonthBarLayout() {
    _monthBarLayout = new QHBoxLayout;
    _monthBarLayout->addWidget(_monthBarPrev);
    _monthBarLayout->addWidget(_monthBarHeader);
    _monthBarLayout->addWidget(_monthBarNext);
    _monthBarLayout->setContentsMargins(0, 0, 0, 0);
}

void YourRosterWidget::initializeMonthBar() {
    _monthBar = new QWidget;
    _monthBar->setLayout(_monthBarLayout);
    YourRosterWidget::colorWidget(MONTH_BAR_FONT,
                                  MONTH_BAR_BACK,
                                  _monthBar);
    _monthBar->setContentsMargins(0, 0, 0, 0);
    _monthBar->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
}

void YourRosterWidget::initializeDummyWidget() {
    _dummyWidget = new QWidget;
    _dummyWidget->setSizePolicy(QSizePolicy::Expanding,
                                QSizePolicy::Expanding);
}

void YourRosterWidget::initializeLayout() {
    _rosterLayout = new QVBoxLayout(this);
    _rosterLayout->setSpacing(0);
    _rosterLayout->setMargin(0);
    _rosterLayout->setContentsMargins(5, 10, 10, 5);

    if (_isRosterMonster) {
        YourRosterWidget::initializeAddLayout();
        _rosterLayout->addLayout(_addLayout);
    }

    _rosterLayout->addWidget(_monthBar, 0);
    _rosterLayout->addWidget(_table, 1);
    _rosterLayout->addWidget(_dummyWidget, 10);
}

void YourRosterWidget::createYourself() {
    this->setLayout(_rosterLayout);
    YourRosterWidget::colorWidget(  Qt::black,
                                    MY_OWN_COLOR,
                                    this);
}

// Only for ROSTER MONSTER
void YourRosterWidget::initializeAddLayout() {
    QLabel *addRoster = new QLabel;
    addRoster->setText(LABEL_ADDROSTER);
    addRoster->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

    QLabel *addLogo = new QLabel;
    QPixmap addPixmap = QPixmap(QString::fromUtf8(ICON_ADD));
    addLogo->setPixmap(addPixmap);
    addLogo->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

    QHBoxLayout *layout = new QHBoxLayout;
    layout->addWidget(addRoster);
    layout->addWidget(addLogo);
    layout->setContentsMargins(0, 0, 0, 0);

    QWidget *addWidget = new QWidget;
    addWidget->setLayout(layout);
    ClickableWidget *addClick = new ClickableWidget(addWidget);

    connect(addClick, SIGNAL(isClicked()),
            this, SLOT(addRoster()));

    _addLayout = new QHBoxLayout;
    _addLayout->addWidget(addClick);
    _addLayout->setAlignment(addClick, Qt::AlignRight);
}

void YourRosterWidget::colorWidget(QColor font, QColor background, QWidget *widget) {
    widget->setAutoFillBackground(true);
    QPalette pallete = widget->palette();
    pallete.setColor(widget->foregroundRole(), font);
    pallete.setColor(widget->backgroundRole(), background);
    widget->setPalette(pallete);
}

//private slots:
void YourRosterWidget::handleNext() {

    _curDate = _curDate.addMonths(1);

    if (_curDate.daysTo(_latestDate) > 0 &&
            _curDate.daysTo(_earliestDate) <= 0) {
        restoreDummyWidget();

        setHeaderText();
        _table->deleteAll();
        _contents.clear();
        getContent();
        addContents();
    } else {
        _curDate = _curDate.addMonths(-1);
    }
}

void YourRosterWidget::handlePrev() {
    _curDate = _curDate.addMonths(-1);
    if (_curDate.daysTo(_latestDate) > 0 &&
            _curDate.daysTo(_earliestDate) <= 0) {
        restoreDummyWidget();

        setHeaderText();
        _table->deleteAll();
        _contents.clear();
        getContent();
        addContents();
    } else {
        _curDate = _curDate.addMonths(1);
    }
}

void YourRosterWidget::addRoster() {
    AddRosterBox *addRosterBox = new AddRosterBox();
    addRosterBox->exec();

    connect(addRosterBox, SIGNAL(gotRosterUpdates(QString)),
            this, SLOT(hasRosterInputDetails(QString)));
}

void YourRosterWidget::handleTableResized(int minHeight) {
    int height = _dummyWidget->height();

    _dummyMaxHeight = qMax(height, _dummyMaxHeight);

    height -= minHeight;

    if (height <= 0) {
        height = 0;
        _dummyWidget->hide();
    } else {
        _dummyWidget->show();
    }
    _dummyWidget->setMaximumHeight(height);

}

void YourRosterWidget::restoreDummyWidget() {
    _dummyWidget->setMaximumHeight(_dummyMaxHeight);
}

//protected
void YourRosterWidget::resizeEvent(QResizeEvent *event) {
    QWidget::resizeEvent(event);

    _dummyMaxHeight = event->size().height();
}

/*
 * Public Slots
 */
void YourRosterWidget::updateTable() {
    assert(_table);
    _table->deleteAll();
    _contents.clear();
    _dummyWidget->setMinimumHeight(this->height());
    getContent();
    addContents();
}
