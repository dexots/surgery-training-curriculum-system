#include "gui/errorbox.h"

const QString ErrorBox::COLOR_BUTTONBACKGROUND = "rgb(39, 174, 96)";
const QString ErrorBox::COLOR_BUTTONBORDER = "white";
const QString ErrorBox::COLOR_BUTTONFONT = "white";

const QString ErrorBox::FONT_BUTTON = "bold 14";
const QString ErrorBox::PADDING_BUTTON = "6";
const QString ErrorBox::RADIUS_BUTTONBORDER = "10";
const QString ErrorBox::STYLE_BUTTONBORDER = "outset";
const QString ErrorBox::WIDTH_BUTTON = "50";
const QString ErrorBox::WIDTH_BUTTONBORDER = "2";

const QString ErrorBox::STRING_DOWNLOAD_ERROR = "Error Downloading File! :(";
const QString ErrorBox::STRING_SAVE_ERROR = "Error with Settings File :(";
const QString ErrorBox::STRING_CONNECTION_ERROR = "Error: No Connection :(";

const int ErrorBox::WIDTH_MINIMUM = 400;
const int ErrorBox::HEIGHT_MINIMUM = 200;

ErrorBox::ErrorBox(string type) {
    _bold.setBold(true);

    _errorLabel = new QLabel;

    QString upperType = QString(type.c_str());
    type = upperType.toStdString();

    if (type == "DOWNLOAD") {
        _errorLabel->setText(STRING_DOWNLOAD_ERROR);
    } else if (type == "SETTINGS") {
        _errorLabel->setText(STRING_SAVE_ERROR);
    } else if (type == "CONNECTION") {
        _errorLabel->setText(STRING_CONNECTION_ERROR);
    }
    _errorLabel->setFont(_bold);

    _okButton = new QPushButton;
    _okButton->setText("OK");
    setButtonStyleSheet(_okButton);
    _okButton->setCursor(Qt::PointingHandCursor);
    connect(_okButton, SIGNAL(clicked()),
            this, SLOT(handleButton()));
    _okButton->setSizePolicy(QSizePolicy::Fixed,
                             QSizePolicy::Fixed);

    _mainLayout = new QVBoxLayout;
    _mainLayout->addWidget(_errorLabel);
    _mainLayout->addSpacing(20);
    _mainLayout->addWidget(_okButton, Qt::AlignHCenter);

    this->setLayout(_mainLayout);
    this->setContentsMargins(10, 10, 10, 10);
    this->setMinimumWidth(WIDTH_MINIMUM);
    this->setMinimumHeight(HEIGHT_MINIMUM);

    this->setAttribute(Qt::WA_TranslucentBackground, true);

    this->setWindowFlags(Qt::FramelessWindowHint);
}

ErrorBox::~ErrorBox() {
    delete _errorLabel;
    delete _okButton;
    delete _mainLayout;
}

void ErrorBox::paintEvent(QPaintEvent *event) {
    QDialog::paintEvent(event);
    QPainter painter(this);
    QBrush brush(Qt::white);
    painter.setBrush(brush);
    painter.setPen(Qt::white);
    painter.drawRoundRect(QRect(0, 0, this->width(), this->height()));
}

void ErrorBox::changeColor(QColor font, QColor back, QWidget *widget) {
    widget->setAutoFillBackground(true);
    QPalette pallete = widget->palette();
    pallete.setColor(widget->foregroundRole(), font);
    pallete.setColor(widget->backgroundRole(), back);
    widget->setPalette(pallete);
}

void ErrorBox::setButtonStyleSheet(QPushButton *button) {
    button->setStyleSheet("background-color:" + COLOR_BUTTONBACKGROUND + ";"
                          "border-style:" + STYLE_BUTTONBORDER + ";"
                          "border-width:" + WIDTH_BUTTONBORDER + "px;"
                          "border-radius:" + RADIUS_BUTTONBORDER + "px;"
                          "border-color:" + COLOR_BUTTONBORDER + ";"
                          "font:" + FONT_BUTTON + "px;"
                          "color:" + COLOR_BUTTONFONT + ";"
                          "min-width:" + WIDTH_BUTTON + "px;"
                          "padding:" + PADDING_BUTTON + "px;");
}

void ErrorBox::handleButton() {
    QDialog::done(0);
}
