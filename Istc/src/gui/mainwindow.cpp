#include "gui/mainwindow.h"

const QString MainWindow::NAME_FILE_USER = "userprofile.txt";

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent) {
    MainWindow::initializeClass();
    MainWindow::initializeWindow();
}

MainWindow::~MainWindow() {
    MainWindow::clearGarbage();
}

//private methods
void MainWindow::clearGarbage() {
    delete _loginView;

    if (_isLogin) {
        delete _browser;
        delete _resourceWidget;
        delete _rosterWidget;
        delete _researchWidget;
        delete _trainingWidget;
        delete _logWidget;
        delete _topWidget;
        delete _botLeftWidget;
        delete _botRightMain;
        delete _botLayout;
    }
    LocalDatabaseClient::deleteObject();
    delete _windowLayout;
    delete _windowWidget;

    //@deprecated webkit
    //QWebSettings::clearMemoryCaches();
    //QWebSettings::clearIconDatabase();
}

void MainWindow::initializeClass() {
    _botLayout = new QHBoxLayout;
    _windowLayout = new QVBoxLayout;
    _windowWidget = new QWidget;

    initializeDatabase();
}

void MainWindow::initializeTopWidget() {
    _topWidget = new TopWidget;

    connect(_topWidget, SIGNAL(wantProfile()), this, SLOT(viewProfile()));
    connect(_topWidget, SIGNAL(wantMain()), this, SLOT(backHome()));
    connect(_topWidget, SIGNAL(wantSync()),
            this, SLOT(syncEveryWidget()));
}

void MainWindow::initializeBotLeftWidget() {
    _botLeftWidget = new BotLeftWidget;
}

void MainWindow::initializeDatabase() {
    _localData = LocalDatabaseClient::getObject();
}


void MainWindow::initializeWindow() {
    MainWindow::maximizeWindow();
    MainWindow::setupWidget();
    MainWindow::initializeLogin();
}

void MainWindow::checkLoginStat() {
    //this value cannot be changed!!!!!!!
    _isLogin = false;

    if (isFileExist(NAME_FILE_USER)) {
        _isLogin = true;
    }

    if (!_isLogin) {
        _loginView->show();
        return;
    } else {
        if (!retrieveDetailsFromFile()) {
            //pls don't comment out this assertion :(
            assert (false);
        }
    }
    MainWindow::setupPage();
    MainWindow::showPage();
}


void MainWindow::setupPage() {
    MainWindow::initializeTopWidget();
    MainWindow::initializeBotLeftWidget();
    MainWindow::initializeBrowser();
    MainWindow::initializeBotRightMain();
    MainWindow::initializeProfileWidget();
    MainWindow::initializeRosterWidget();
    MainWindow::initializeResourcesWidget();
    MainWindow::initializeResearchWidget();
    MainWindow::initializeTrainingWidget();
    MainWindow::initializeLogWidget();
    MainWindow::setupLayout();
    MainWindow::hideAllWidgets();

    _botRightMain->installEventFilter(this);

}

void MainWindow::initializeBrowser() {
    _browser = new Browser;
    _browser->hide();
    connect (_browser, SIGNAL(showWidget()),
             this, SLOT(hideBrowser()));
}

void MainWindow::initializeLogin() {
    _loginView = new LoginUi;
    _windowLayout->addWidget(_loginView);
    _loginView->hide();

    checkLoginStat();

    connect(_loginView, SIGNAL(loginSuccess()),
            this, SLOT(userHasLogin()));
}

void MainWindow::initializeBotRightMain() {
    _botRightMain = new BotRightMain;

    connect(_botRightMain, SIGNAL(wantTrain()), this, SLOT(viewTrain()));
    connect(_botRightMain, SIGNAL(wantRos()), this, SLOT(viewRos()));
    connect(_botRightMain, SIGNAL(wantLog()), this, SLOT(viewLog()));
    connect(_botRightMain, SIGNAL(wantResearch()), this, SLOT(viewResearch()));
    connect(_botRightMain, SIGNAL(wantResources()), this, SLOT(viewResources()));
}

void MainWindow::initializeProfileWidget() {
    _profileWidget = new ProfileWidget;

    connect(_profileWidget, SIGNAL(backToHome()), this, SLOT(backHome()));
}

void MainWindow::initializeResourcesWidget() {
    _resourceWidget = new ResourceWidget;

    connect(_resourceWidget, SIGNAL(backToHome()), this, SLOT(backHome()));
    connect(_resourceWidget, SIGNAL(load(QString,QString)),
            this, SLOT(loadUrl(QString,QString)));
}

void MainWindow::initializeRosterWidget() {
    _rosterWidget = new RosterWidget;

    connect(_rosterWidget, SIGNAL(backToHome()), this, SLOT(backHome()));
}

void MainWindow::initializeResearchWidget() {
    _researchWidget = new ResearchWidget;

    connect(_researchWidget, SIGNAL(backToHome()), this, SLOT(backHome()));
    connect(_researchWidget, SIGNAL(load(QString,QString)),
            this, SLOT(loadUrl(QString,QString)));
}

void MainWindow::initializeTrainingWidget() {
    _trainingWidget = new TrainingWidget;

    connect(_trainingWidget, SIGNAL(backToHome()), this, SLOT(backHome()));

}

void MainWindow::initializeLogWidget() {
    _logWidget = new LogWidget;

    connect(_logWidget, SIGNAL(backToHome()), this, SLOT(backHome()));
}

void MainWindow::setupLayout() {
    _botLayout->addWidget(_botLeftWidget);
    _botLayout->addWidget(_botRightMain);
    _botLayout->addWidget(_profileWidget);
    _botLayout->addWidget(_rosterWidget);
    _botLayout->addWidget(_resourceWidget);
    _botLayout->addWidget(_researchWidget);
    _botLayout->addWidget(_trainingWidget);
    _botLayout->addWidget(_logWidget);
    _botLayout->addWidget(_browser);

    _windowLayout->addWidget(_topWidget);
    _windowLayout->addLayout(_botLayout);

    _topWidget->setSearchBarVisibility(false);
}

void MainWindow::hideAllWidgets() {
    _botLeftWidget->hide();
    _botRightMain->hide();
    _profileWidget->hide();
    _resourceWidget->hide();
    _rosterWidget->hide();
    _researchWidget->hide();
    _trainingWidget->hide();
    _topWidget->hide();
    _loginView->hide();
    _logWidget->hide();
}

void MainWindow::hideBotRightWidgets() {
    _botRightMain->hide();
    _profileWidget->hide();
    _resourceWidget->hide();
    _rosterWidget->hide();
    _researchWidget->hide();
    _trainingWidget->hide();
    _logWidget->hide();
}


void MainWindow::setupWidget() {
    _windowWidget->setAutoFillBackground(true);
    QPalette pallete = _windowWidget->palette();
    pallete.setColor(_windowWidget->backgroundRole(), Qt::white);
    _windowWidget->setPalette(pallete);

    _windowWidget->setLayout(_windowLayout);

    _windowWidget->setMaximumHeight(_clientHeight);
    _windowWidget->setMaximumWidth(_clientWidth);
    this->setCentralWidget(_windowWidget);
    this->setWindowFlags(Qt::FramelessWindowHint);
}

void MainWindow::showPage() {
    _topWidget->show();
    _botLeftWidget->show();
    _botRightMain->show();
}

void MainWindow::maximizeWindow() {
    QDesktopWidget widget;
    QRect mainScreenSize = widget.availableGeometry(widget.primaryScreen());

    _clientHeight = mainScreenSize.height();
    _clientWidth = mainScreenSize.width();

    this->resize(_clientWidth, _clientHeight);

    int posX = 0;
    int posY = 0;
    this->move(posX, posY);
}

//private slots
void MainWindow::viewProfile() {
    _trainingWidget->hide();
    _resourceWidget->hide();
    _rosterWidget->hide();
    _researchWidget->hide();
    _logWidget->hide();
    _botRightMain->hide();
    _browser->hide();
    _profileWidget->show();
    _topWidget->setSearchBarVisibility(true);
}

void MainWindow::viewTrain() {
    _botRightMain->hide();
    _trainingWidget->show();
    _topWidget->setSearchBarVisibility(true);
}

void MainWindow::viewRos() {
    _botRightMain->hide();
    _rosterWidget->show();
    _topWidget->setSearchBarVisibility(true);
}

void MainWindow::viewLog() {
    _botRightMain->hide();
    _logWidget->show();
    _topWidget->setSearchBarVisibility(true);
}

void MainWindow::viewResearch() {
    _botRightMain->hide();
    _researchWidget->show();
    _topWidget->setSearchBarVisibility(true);
}

void MainWindow::viewResources() {
    _botRightMain->hide();
    _resourceWidget->show();
    _topWidget->setSearchBarVisibility(true);
}

void MainWindow::backHome() {
    _profileWidget->hide();
    _trainingWidget->hide();
    _resourceWidget->hide();
    _rosterWidget->hide();
    _researchWidget->hide();
    _logWidget->hide();
    _topWidget->setSearchBarVisibility(false);
    _browser->hide();
    _botRightMain->show();
}

void MainWindow::userHasLogin() {
    _loginView->hide();

    UserDetails* userDetails = UserDetails::getObject();

    QString matricNo = userDetails->getMatricNo();

    QString errorMsg;
    if (isFileExist(NAME_FILE_USER)) {
        if (!isUserMatches(matricNo)) {
            _loginView->show();

            errorMsg = "Sorry! Your matricNo doesn't match with the"
                       "current user's matricNo!";
            _loginView->showErrorMessage(errorMsg);
        }
    } else {
        if (!_localData->isCurrentUserExist() && !_localData->addCurrentUser()){

            _loginView->show();
            _loginView->showErrorMessage("Some User Details are wrong!!");
            return;
        }

        if (!createUserProfileFile(userDetails)) {
            assert (false);
        }
    }

    _localData->getUserDetails();


    _isLogin = true;


    MainWindow::setupPage();
    MainWindow::showPage();
}

bool MainWindow::retrieveDetailsFromFile() {
    QFile file (NAME_FILE_USER);
    if (!file.open(QFile::ReadOnly | QFile::Text)) {
        return false;
    }

    bool hasMatricNo = false;
    bool hasName = false;
    bool hasRole = false;
    bool hasRosterMonster = false;
    bool hasRotation = false;

    UserDetails* userDetails = UserDetails::getObject();

    QTextStream in(&file);
    while (!in.atEnd()) {
        QString line = in.readLine();
        if (line.contains("matricNo")) {
            QStringList matricNoLineInFile = line.split(":=");
            QString matricNoInFile = matricNoLineInFile.at(1).trimmed();
            userDetails->setMatricNo(matricNoInFile);
            hasMatricNo = true;
        } else if (line.contains("name")) {
            QStringList nameLineInFile = line.split(":=");
            QString nameInFile = nameLineInFile.at(1).trimmed();
            userDetails->setUserName(nameInFile);
            hasName = true;
        } else if (line.contains("role")) {
            QStringList roleLineInFile = line.split(":=");
            QString roleInFile = roleLineInFile.at(1).trimmed();
            userDetails->setUserRole(roleInFile);
            hasRole = true;
        } else if (line.contains("rosterMonster")) {
            QStringList rosterLineInFile = line.split(":=");
            QString rosterInFile = rosterLineInFile.at(1).trimmed();
            bool isRosterMonster = rosterInFile.toInt() == 1;
            userDetails->setIsRosterMonster(isRosterMonster);
            hasRosterMonster = true;
        } else if (line.contains("rotation")) {
            QStringList rotationLineInFile = line.split(":=");
            QString rotationInFile = rotationLineInFile.at(1).trimmed();
            userDetails->setRotation(rotationInFile);
            hasRotation = true;
        }
    }

    _localData->getUserDetails();

    file.close();
    return hasMatricNo && hasName && hasRole && hasRosterMonster && hasRotation;
}

bool MainWindow::isUserMatches(QString matricNo) {
    QFile file (NAME_FILE_USER);
    if (!file.open(QFile::ReadOnly | QFile::Text)) {
        return false;
    }

    bool isMatched = false;
    QTextStream in(&file);
    while (!in.atEnd()) {
        QString line = in.readLine();
        if (line.contains("matricNo")) {
            QStringList matricNoLineInFile = line.split(":=");
            QString matricNoInFile = matricNoLineInFile.at(1).trimmed();
            isMatched = matricNoInFile.toLower() == matricNo.toLower();
            break;
        }
    }

    file.close();
    return isMatched;
}

bool MainWindow::isFileExist(QString fileName) {
    QFile file(fileName);
    bool ret = file.exists();
    file.close();
    return ret;
}

bool MainWindow::createUserProfileFile(UserDetails* userDetails) {
    QFile file(NAME_FILE_USER);
    if (!file.open(QFile::WriteOnly | QFile::Text)) {
        return false;
    }
    QString name = "name := " + userDetails->getUserName();
    QString rotation = "rotation := " + userDetails->getRotation();
    QString matricNo = "matricNo := " + userDetails->getMatricNo();
    QString address = "address := " + userDetails->getAddress();
    QString personalContact = "personalContact := " + userDetails->getHandPhone();
    QString homeContact = "homeContact := " + userDetails->getHomePhone();
    QString schoolEmail = "schoolEmail := " + userDetails->getUserEmail();
    QString personalEmail = "personalEmail := " + userDetails->getPersonalEmail();
    QString role = "role := " + userDetails->getUserRole();


    QString rosterMonster = "rosterMonster := ";

    bool isRosterMonster = userDetails->getIsRosterMonster();
    if (isRosterMonster) {
        rosterMonster += "1";
    } else {
        rosterMonster += "0";
    }

    QTextStream out (&file);
    out << name << "\n" << rotation << "\n"
        << matricNo << "\n" << address << "\n"
        << personalContact << "\n" << homeContact << "\n"
        << schoolEmail << "\n" << personalEmail << "\n"
        << role << "\n" << rosterMonster << "\n";
    out.flush();
    file.close();
    return true;
}


void MainWindow::loadUrl(QString hyperlink, QString sender) {
    _urlSender = sender;
    std::cout << _urlSender.toStdString();

    if (_urlSender == _resourceWidget->getClassName()) {
        _resourceWidget->hide();
    } else if (_urlSender == _researchWidget->getClassName()) {
        _researchWidget->hide();
    } else {
        showErrorBox();
        return;
    }
    _topWidget->setSearchBarVisibility(false);
    _browser->load(QUrl(hyperlink));
    _browser->show();

}

void MainWindow::hideBrowser() {
    _browser->clearCookie();
    _browser->hide();

    if (_urlSender == _resourceWidget->getClassName()) {
        viewResources();
    } else if (_urlSender == _researchWidget->getClassName()) {
        viewResearch();
    } else {
        //error
        return;
    }
}

void MainWindow::showErrorBox() {
    ErrorBox *errorBox = new ErrorBox("CONNECTION");
    errorBox->exec();
}

void MainWindow::syncEveryWidget() {
    _trainingWidget->updateAllTables();
    _profileWidget->updateAllTables();
    _rosterWidget->updateAllTables();
    _logWidget->updateAllTables();
}

//Mouse tracking
bool MainWindow::eventFilter(QObject* obj, QEvent *event) {
    if (obj==(QObject*)_botRightMain) {
        if (event->type() == QEvent::Enter) {
         _botRightMain->setCursor(Qt::PointingHandCursor);
        }
        return true;
    } else {
        return QWidget::eventFilter(obj, event);
    }
}
