#include "gui/milestonewidget.h"

const QVector<int> MilestoneWidget::WIDTH_COLUMN (QVector<int>() << 80 << 40 << 200 << 40 << 40);

const int MilestoneWidget::NUMBER_COLUMN = 5;
const int MilestoneWidget::HEIGHT_PROGRESSBAR = 20;

const QString MilestoneWidget::ICON_ADD = QString::fromUtf8(":/icon/ButtonIcon/add_small.png");
const QString MilestoneWidget::ICON_MINUS = QString::fromUtf8(":/icon/ButtonIcon/minus_small.png");
const QString MilestoneWidget::ICON_MORE = QString::fromUtf8(":/icon/ButtonIcon/more_small.png");

const QString MilestoneWidget::MILESTONE_FILE = "milestones.txt";
const QString MilestoneWidget::DELIMITER = ":==";
const QString MilestoneWidget::STRING_NEWLINE = "\n";
const QString MilestoneWidget::STRING_EMPTY = "";
const QString MilestoneWidget::STRING_COMMA = ",";
const QString MilestoneWidget::STRING_COLON = ":";
const QString MilestoneWidget::STRING_SPACE = " ";
const QString MilestoneWidget::FORMAT_DATE = "dd/MM/yy";

const int MilestoneWidget::NUM_PROC = 6; //Currently only have 6 to track
const int MilestoneWidget::SIZE_PROC = 4;

const QColor MilestoneWidget::COLOR_CONTENTBACKGROUND = QColor(149, 231, 184);
const QColor MilestoneWidget::COLOR_CONTENTFONT = QColor(Qt::black);

MilestoneWidget::MilestoneWidget() {
    _procSum = 0;
    readMilestoneFile();
    setUpMilestoneLayout();

    addProgressBar("Training Attendance", 0, 100, true, 66);
    addProgressBar("Roster Count", 0, 100, true, 33);
    addProgressBar("Working Hours", 0, 100, true, 60);

    _procBar = addProgressBar("Procedures Performed", 0, 150, true, _procSum);

    setUpProcedureMilestones();
    setUpDummyWidget();

    setLayout(_milestoneLayout);
}

MilestoneWidget::~MilestoneWidget() {
    delete _milestoneLayout;
    delete _dummyWidget;
}

void MilestoneWidget::readMilestoneFile() {
    qDebug() << "MilestoneWidget -> readMilestoneFile: "
             << "start reading milestone file..";

    QFile file(MILESTONE_FILE);
    if (!file.open(QFile::ReadOnly | QFile::Text)) {
        // Make an empty vector
        QStringList list;
        _procMilestone.fill(list, NUM_PROC);


        //show popupbox
        QString errorMsg = MILESTONE_FILE + " is not found!!";
        PopUpMsgBox* noFileBox = new PopUpMsgBox(errorMsg);
        noFileBox->exec();
        noFileBox->deleteLater();

        qDebug() << "MilestoneWidget -> readMilestoneFile: "
                 << "failed to open file";
    } else {
        QTextStream in(&file);
        QString data = in.readAll();

        qDebug() << "MilestoneWidget -> readMilestoneFile: "
                 << "read data sucessfully";

        populateData(data);
    }

    file.close();

    qDebug() << "MilestoneWidget -> readMilestoneFile: "
                "done reading milestone file";
}

void MilestoneWidget::populateData(QString data) {
    qDebug() << "MilestoneWidget -> populateData: "
                "populating data...";

    if (data.isEmpty()) {
        QStringList list;
        _procMilestone.fill(list, NUM_PROC);

        qDebug() << "MilestoneWidget -> populateData: "
                    "data is empty...";
        return;
    }
    data = data.trimmed();


    qDebug() << "MilestoneWidget -> populateData: "
                "reading data.. make sure data is "
             << "<procedure Name> <min> <max> <current> <recordedDate>"
             << "format ";

    QStringList split = data.split(STRING_NEWLINE);
    for (int i = 0; i < split.length(); i++) {
        split[i] = split[i].trimmed();

        if (split[i].isEmpty()) {
            continue;
        }

        qDebug() << "MilestoneWidget -> populateData: "
                    "the index number " << i << " is : "
                 << split[i];

        // Data: <procedure Name> <min> <max> <current> <recordedDate>
        QStringList procData = split[i].split(DELIMITER);

        if (procData.isEmpty() || procData.size() < SIZE_PROC) {
            continue;
        }

        _procMilestone.append(procData);
        _procSum += procData.at(3).trimmed().toInt();
    }

    if (_procMilestone.isEmpty()) {
        QStringList list;
        _procMilestone.fill(list, NUM_PROC);

        QString errorMsg = MILESTONE_FILE + " is not in a correct format!\n";
        qDebug() << "MilestoneWidget -> populateData: "
                    "_procMilestone is empty...";

        //todo: change this to errorbox
        PopUpMsgBox* msgBox = new PopUpMsgBox(errorMsg);
        msgBox->exec();
        msgBox->deleteLater();

        return;
    }

    qDebug() << "MilestoneWidget -> populateData: "
                "done populating data";
}

void MilestoneWidget::setUpMilestoneLayout() {
    _milestoneLayout = new QVBoxLayout;
    _milestoneLayout->setAlignment(Qt::AlignCenter | Qt::AlignTop);
    setUpAddLayout();
}

void MilestoneWidget::setUpProcedureMilestones() {
    _procedureMilestonesTable = new TableWidget(false, NUMBER_COLUMN, WIDTH_COLUMN);
    _procedureMilestonesTable->setContBackground(COLOR_CONTENTBACKGROUND);
    _procedureMilestonesTable->setContFont(COLOR_CONTENTFONT);
    _procedureMilestonesTable->setNoGrid();
    addProcedureMilestones();
    _milestoneLayout->addWidget(_procedureMilestonesTable);
}

void MilestoneWidget::setUpDummyWidget() {
    _dummyWidget = new QWidget();
    _dummyWidget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    _milestoneLayout->addWidget(_dummyWidget);
}

void MilestoneWidget::setUpAddLayout() {
    QWidget *alignWidget = new QWidget;
    alignWidget->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Fixed);

    QLabel *addActivity = new QLabel;
    addActivity->setText("Add Procedure Milestone");
    addActivity->setSizePolicy(QSizePolicy::Fixed,
                               QSizePolicy::Fixed);

    QLabel *addLogo = new QLabel;
    addLogo->setPixmap(QPixmap(ICON_ADD));
    addLogo->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

    ClickableWidget *addClick = new ClickableWidget(addLogo);

    connect(addClick, SIGNAL(isClicked()),
            this, SLOT(handleAdd()));

    QHBoxLayout *addLayout = new QHBoxLayout;
    addLayout->addWidget(alignWidget, 10);
    addLayout->addWidget(addActivity, 0);
    addLayout->addWidget(addClick);

    QWidget *fixedWidget = new QWidget;
    fixedWidget->setLayout(addLayout);
    fixedWidget->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Fixed);
    _milestoneLayout->addWidget(fixedWidget, 0);
}

ProgressBar* MilestoneWidget::addProgressBar(QString label, int minValue, int maxValue,
                                    bool isBadToGood, int progressValue) {
    QLabel *barLabel = setUpBarLabel(label, true);
    ProgressBar *bar = setUpProgressBar(minValue, maxValue, isBadToGood, progressValue);

    QVBoxLayout *progressBarLayout = new QVBoxLayout;
    progressBarLayout->addWidget(barLabel);
    progressBarLayout->addWidget(bar);

    _milestoneLayout->addLayout(progressBarLayout);
    _milestoneLayout->setAlignment(progressBarLayout, Qt::AlignHCenter);

    return bar;
}

QLabel* MilestoneWidget::setUpBarLabel(QString label, bool bold) {
    QLabel *barLabel = new QLabel();
    if (bold) {
        barLabel->setText("<h3><b>" + label + ":</b></h3>");
    } else {
        barLabel->setText(label);
    }

    barLabel->setStyleSheet("padding-top: 20px;");

    return barLabel;
}

ProgressBar* MilestoneWidget::setUpProgressBar(int minValue, int maxValue,
                                       bool isBadToGood, int progressValue) {
    ProgressBar *bar = new ProgressBar;
    bar->setMaximumHeight(HEIGHT_PROGRESSBAR);
    bar->setBadToGood(isBadToGood);
    bar->setMinimum(minValue);
    bar->setMaximum(maxValue);
    bar->setValue(progressValue);

    return bar;
}

void MilestoneWidget::addProcedureMilestones() {

    for (int i=0; i<_procMilestone.length(); i++) {
        QStringList proc = _procMilestone.at(i);

        if (proc.isEmpty()) {
            continue;
        }

        addProcedureProgressBar(proc.at(0).trimmed(),
                                        proc.at(1).trimmed().toInt(),
                                        proc.at(2).trimmed().toInt(), true,
                                        proc.at(3).trimmed().toInt());
    }

}

void MilestoneWidget::addProcedureProgressBar(QString label, int minValue, int maxValue,
                                   bool isBadToGood, int progressValue) {
    qDebug() << "MilestoneWidget -> addProcedureProgressBar: Adding Procedure Progress Bar";
    QLabel *barLabel = setUpBarLabel(label, false);
    ProgressBar *bar = setUpProgressBar(minValue, maxValue, isBadToGood, progressValue);
    bar->setContentsMargins(10, 10, 10 , 10);

    ClickableWidget *clickableAdd = createClickableAddWidget(label);
    ClickableWidget *clickableMinus = createClickableMinusWidget(label);
    ClickableWidget *clickableMore = createClickableMoreWidget(label);

    bar->setAccessibleName(label);

    connect(clickableMinus, SIGNAL(isClicked()),
            bar, SLOT(minusValue()));
    connect(clickableMinus, SIGNAL(isClicked()),
            _procBar, SLOT(minusValue()));
    connect(clickableMinus, SIGNAL(isClicked(bool)),
            clickableAdd, SLOT(setEnabled(bool)));
    connect(clickableMinus, SIGNAL(isClicked(QString)),
            this, SLOT(removeRecordedDate(QString)));
    connect(clickableAdd, SIGNAL(isClicked()),
            bar, SLOT(addValue()));
    connect(clickableAdd, SIGNAL(isClicked(bool)),
            clickableMinus, SLOT(setEnabled(bool)));
    connect(clickableAdd, SIGNAL(isClicked(bool)),
            clickableMore, SLOT(setEnabled(bool)));
    connect(clickableAdd, SIGNAL(isClicked()),
            _procBar, SLOT(addValue()));
    connect(clickableAdd, SIGNAL(isClicked(QString)),
            this, SLOT(addRecordedDate(QString)));
    connect(bar, SIGNAL(reachMinValue(bool)),
            clickableMinus, SLOT(setDisabled(bool)));
    connect(bar, SIGNAL(reachMinValue(bool)),
            clickableMore, SLOT(setDisabled(bool)));
    connect(bar, SIGNAL(reachMaxValue(bool)),
            clickableAdd, SLOT(setDisabled(bool)));
    connect(clickableMore, SIGNAL(isClicked(QString)),
            this, SLOT(showProgressBarRecords(QString)));

    QWidget *barWidget = new QWidget();
    QVBoxLayout *barLayout = new QVBoxLayout();
    barLayout->addWidget(bar);
    barWidget->setLayout(barLayout);
    barWidget->setAutoFillBackground(true);

    QVector<QWidget*> contentOfARow = QVector<QWidget*>()
            << barLabel << clickableMinus << barWidget << clickableAdd << clickableMore;
    _procedureMilestonesTable->addRow(contentOfARow);

    if (progressValue == minValue) {
        qDebug() << label << " hide minus and more buttons";
        clickableMinus->setEnabled(false);
        clickableMore->setEnabled(false);
    } else {
        //do nothing
    }

    if (progressValue == maxValue) {
        qDebug() << label << " hide add button";
        clickableAdd->setEnabled(false);
    } else {
        //do nothing
    }

    _procedureProgressBars.append(bar);
    qDebug() << "MilestoneWidget -> addProcedureProgressBar: Added Procedure Progress Bar";
}

ClickableWidget* MilestoneWidget::createClickableAddWidget(QString accessibleName) {
    QLabel *addLogo = new QLabel;
    addLogo->setPixmap(QPixmap(ICON_ADD));

    QHBoxLayout *addLayout = new QHBoxLayout;
    addLayout->addWidget(addLogo);
    addLayout->setContentsMargins(0, 0, 0, 0);

    QWidget *addWidget = new QWidget;
    addWidget->setLayout(addLayout);
    addWidget->setAccessibleName(accessibleName);
    ClickableWidget *addClick = new ClickableWidget(addWidget, this);

    return addClick;
}

ClickableWidget* MilestoneWidget::createClickableMinusWidget(QString accessibleName) {
    QLabel *minusLogo = new QLabel;
    minusLogo->setPixmap(QPixmap(ICON_MINUS));
    minusLogo->setAccessibleName(accessibleName);

    QHBoxLayout *minusLayout = new QHBoxLayout;
    minusLayout->addWidget(minusLogo);
    minusLayout->setContentsMargins(0, 0, 0, 0);

    QWidget *minusWidget = new QWidget;
    minusWidget->setLayout(minusLayout);
    minusWidget->setAccessibleName(accessibleName);
    ClickableWidget *minusClick = new ClickableWidget(minusWidget, this);

    return minusClick;
}

ClickableWidget* MilestoneWidget::createClickableMoreWidget(QString accessibleName) {
    QLabel *moreLogo = new QLabel;
    moreLogo->setPixmap(QPixmap(ICON_MORE));

    QHBoxLayout *moreLayout = new QHBoxLayout;
    moreLayout->addWidget(moreLogo);
    moreLayout->setContentsMargins(0, 0, 0, 0);

    QWidget *moreWidget = new QWidget;
    moreWidget->setLayout(moreLayout);
    moreWidget->setAccessibleName(accessibleName);
    ClickableWidget *moreClick = new ClickableWidget(moreWidget, this);

    return moreClick;
}

void MilestoneWidget::handleAdd() {
    MilestoneBox *mileBox = new MilestoneBox();
    connect(mileBox, SIGNAL(passInfo(QString, int, int)),
            this, SLOT(receivedMilestone(QString, int, int)));
    mileBox->exec();
}

void MilestoneWidget::receivedMilestone(QString title, int completedNumber, int requiredNumber) {
    QStringList newStringList;
    newStringList << title << "0" << QString::number(requiredNumber) << QString::number(completedNumber);

    QStringList recordedDatesStringList;
    for (int i = 1; i <= completedNumber; i ++) {
        recordedDatesStringList << QString::number(i) + STRING_COLON + "NIL";
    }
    QString recordedDatesString = recordedDatesStringList.join(STRING_COMMA);

    newStringList << recordedDatesString;

    _procMilestone.append(newStringList);

    addProcedureProgressBar(title, 0, requiredNumber, true, completedNumber);

    writeMilestoneFile();
}

//private slots

void MilestoneWidget::writeMilestoneFile() {
    qDebug() << "MilestoneWidget -> writeMilestoneFile : Writing to Milestone file";
    QFile file(MILESTONE_FILE);
    if (!file.open(QFile::WriteOnly | QFile::Text)) {
        qDebug() << "MilestoneWidget -> writeMilestoneFile : Failed to write to Milestone file";
        assert(false);
        return;
    }

    QString string = "";
    for (int i = 0; i < _procedureProgressBars.size(); i ++) {
        QStringList proc = _procMilestone.at(i);
        QStringList newStringList = QStringList();
        newStringList<< proc.at(0) << proc.at(1) << proc.at(2);
        newStringList << STRING_SPACE + QString::number(_procedureProgressBars.at(i)->value()) + STRING_SPACE;
        if (proc.size() == 5) {
            newStringList << proc.at(4);
        } else {
            // do nothing
        }
        string += newStringList.join(DELIMITER) + STRING_NEWLINE;
    }

    QTextStream out(&file);
    out << string;
    file.close();
    qDebug() << "MilestoneWidget -> writeMilestoneFile : Writed to Milestone file";
}

void MilestoneWidget::showProgressBarRecords(QString accessibleName) {
    for (int i = 0; i < _procedureProgressBars.size(); i ++) {
        if (_procedureProgressBars.at(i)->accessibleName() == accessibleName) {
            QStringList procedureMilestoneInfo = _procMilestone.at(i);

            QString message = "<b> DATE RECORDED: </b><br>";
            if (procedureMilestoneInfo.size() < 5) { //while add new with > 0 completed number
                message += "NIL";
            } else {
                QStringList datesString = procedureMilestoneInfo.at(procedureMilestoneInfo.size() - 1).split(STRING_COMMA);

                for (int i = 0; i < datesString.size(); i ++) {
                    message += datesString.at(i).trimmed() + "<br>";
                }
            }

            PopUpMsgBox *recordedDatePopup = new PopUpMsgBox(message);
            recordedDatePopup->exec();
            break;
        } else {
            // do nothing
        }
    }
}

void MilestoneWidget::addRecordedDate(QString accessibleName) {
    for (int i = 0; i < _procedureProgressBars.size(); i ++) {
        if (_procedureProgressBars.at(i)->accessibleName() == accessibleName) {
            QStringList procedureMilestoneInfo = _procMilestone.at(i);

            QString recordedDatesString;
            int index = _procedureProgressBars.at(i)->value();
            if (procedureMilestoneInfo.size() == 5) {
                recordedDatesString = procedureMilestoneInfo.at(procedureMilestoneInfo.size() - 1);
                recordedDatesString += STRING_COMMA + QString::number(index) + STRING_COLON +
                                        QDate::currentDate().toString(FORMAT_DATE);

                //remove original recorded date string
                procedureMilestoneInfo.removeLast();
            } else {
                recordedDatesString = QString::number(index) + STRING_COLON +
                                        QDate::currentDate().toString(FORMAT_DATE);
            }
            procedureMilestoneInfo.append(recordedDatesString);
            _procMilestone.remove(i);
            _procMilestone.insert(i, procedureMilestoneInfo);
            break;
        } else {
            // do nothing
        }
    }
    writeMilestoneFile();
}

void MilestoneWidget::removeRecordedDate(QString accessibleName) {
    for (int i = 0; i < _procedureProgressBars.size(); i ++) {
        if (_procedureProgressBars.at(i)->accessibleName() == accessibleName) {
            QStringList procedureMilestoneInfo = _procMilestone.at(i);


            //assert(procedureMilestoneInfo.size() == 5);

            QString recordedDatesString;

            recordedDatesString = procedureMilestoneInfo.at(procedureMilestoneInfo.size() - 1);
            QStringList recordedDatesStringList = recordedDatesString.split(STRING_COMMA);
            recordedDatesStringList.removeLast();

            //remove original recorded date string
            procedureMilestoneInfo.removeLast();

            if (recordedDatesStringList.size() != 0) {
                recordedDatesString = recordedDatesStringList.join(STRING_COMMA);
                procedureMilestoneInfo.append(recordedDatesString);
            } else {
                // do nothing
            }

            _procMilestone.remove(i);
            _procMilestone.insert(i, procedureMilestoneInfo);
            break;
        } else {
            // do nothing
        }
    }
    writeMilestoneFile();
}
