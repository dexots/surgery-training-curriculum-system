#include "gui/rosterwidget.h"

RosterWidget::RosterWidget(){
    RosterWidget::createLayout();
}

RosterWidget::~RosterWidget() {
    delete _widgetLayout;
}

void RosterWidget::createLayout() {
    _widgetLayout = new QVBoxLayout;
    YourRosterWidget *yourRosterWidget = new YourRosterWidget;

    SubCate *yourRosterCate = new SubCate("Your Roster",
                                        "View your allocated roster",
                                        "",
                                        Qt::gray,
                                        QColor (219, 237, 249, 255),
                                        QColor (145, 199, 235, 255),
                                        yourRosterWidget);

    connect(this, SIGNAL(updateYourRosterTable()), yourRosterWidget, SLOT(updateTable()));

    RosReq *submitReqWidget = new RosReq;

    SubCate *submitReqCate = new SubCate("Submit Roster Request",
                                        "Request to block out dates or to select dates for Roster",
                                        "",
                                        Qt::gray,
                                        QColor (219, 237, 249, 255),
                                        QColor (145, 199, 235, 255),
                                        submitReqWidget);

    connect(this, SIGNAL(updateRosterRequestTable()), submitReqWidget, SLOT(updateTable()));

    LeaveWidget *leaveWidget = new LeaveWidget();

    SubCate *leaveCate = new SubCate("Annual Leave Application and Request",
                                    "Take an official break or view application status",
                                    "",
                                    Qt::gray, //desc font color
                                    QColor (219, 237, 249, 255),
                                    QColor (145, 199, 235, 255),
                                    leaveWidget);

    connect(this, SIGNAL(updateLeaveTable()), leaveWidget, SLOT(updateTable()));

    RosterMonsterWidget *rosterMonsterWidget = new RosterMonsterWidget();
    SubCate *rosterMonsterCate = new SubCate("All Roster Requests",
                                    "For Roster Monsters",
                                    "",
                                    Qt::gray,
                                    QColor (219, 237, 249, 255),
                                    QColor (145, 199, 235, 255),
                                    rosterMonsterWidget);

    connect(submitReqWidget, SIGNAL(rosterRequestAdded()), rosterMonsterWidget, SLOT(updateTable()));

    QVector <SubCate*> rosterSubCategories;
    rosterSubCategories.append(yourRosterCate);
    rosterSubCategories.append(submitReqCate);
    rosterSubCategories.append(leaveCate);

    if (RosterWidget::isRosterMonster()) {
        connect(this, SIGNAL(updateRosterMonsterTable()), rosterMonsterWidget, SLOT(updateTable()));
        rosterSubCategories.append(rosterMonsterCate);
    }

    CateTable *rosterTable = new CateTable("Roster and Leave",
                                             "",
                                             ":/icon/ButtonIcon/Roster Icon.png",
                                             QColor (52, 152, 219, 255),
                                             rosterSubCategories);

    _widgetLayout->addWidget(rosterTable);
    _widgetLayout->setContentsMargins(0, 0, 0, 0);
    this->setLayout(_widgetLayout);

    connect(rosterTable, SIGNAL(backToHome()), this, SLOT(handleHomeSignal()));
}

bool RosterWidget::isRosterMonster() {
    UserDetails *userDetails = UserDetails::getObject();
    return userDetails->getIsRosterMonster();
}

void RosterWidget::colorWidget(QColor color, QWidget *widget) {
    widget->setAutoFillBackground(true);
    QPalette pallete = widget->palette();
    pallete.setColor(widget->backgroundRole(), color);
    widget->setPalette(pallete);
}

//private slots
void RosterWidget::handleHomeSignal() {
    emit this->backToHome();
}

//public slots
void RosterWidget::updateAllTables() {
    emit updateYourRosterTable();
    emit updateLeaveTable();
    emit updateRosterRequestTable();\
    if (RosterWidget::isRosterMonster()) {
        emit updateRosterMonsterTable();
    }
}
