#include "catetrain.h"

CateTrain::CateTrain() :
    CateBox() {
    CateTrain::initializeCateTrain();
}

//private methods:
void CateTrain::initializeCateTrain(){
    const QColor COLOR_GREEN = QColor (46, 204, 113, 255);//rgba
    const QString CATEGORY_TRAIN = "Training";
    const QPixmap ICON_TRAIN = QPixmap(QString::fromUtf8(":/icon/ButtonIcon/Training Icon.png"));

    CateBox::setCategoryName(CATEGORY_TRAIN);
    CateBox::setPixmap(ICON_TRAIN);
    CateBox::setColor(COLOR_GREEN);
    CateBox::createMyself();
}
