#include "objects/eventitem.h"

const QString EventItem::STRING_EMPTY = "";
const QString EventItem::STRING_SPACE = " ";
const QString EventItem::STRING_COLON = ":";

const QString EventItem::NEWLINE = "<br>";
const QString EventItem::LABEL_DATE = "Date: ";
const QString EventItem::LABEL_TIME = "Time: ";
const QString EventItem::LABEL_LOCATION = "Location: ";
const QString EventItem::LABEL_TRAINER = "Trainer: ";
const QString EventItem::LABEL_VACANCY = "Vacancy: ";
const QString EventItem::LABEL_DESCRIPTION = "Description: ";

const QString EventItem::FORMAT_VACANCYSTRING = "%1 (out of %2)";
const QString EventItem::FORMAT_TIME = "hh:mm";

const QString EventItem::ICON_EDIT = ":/icon/ButtonIcon/edit.png";

const int EventItem::SIZE_FONT = 10;

EventItem::EventItem(QDate eventDate, QTime eventStartTime,
                     QTime eventEndTime, QString eventName, QString eventLocation,
                     QString eventTrainer, int curVacancy, int maxVacancy,
                     QString eventDescription, QString eventLink,
                     bool isImportant, bool isClosed) {

    initializeQString();
    setDate(eventDate);
    setStartTime(eventStartTime);
    setEndTime(eventEndTime);
    setName(eventName);
    setLocation(eventLocation);
    setTrainer(eventTrainer);
    setVacancy(curVacancy, maxVacancy);
    setDescription(eventDescription);
    setLink(eventLink);
    setIsImportant(isImportant);
    setIsClosed(isClosed);

    constructClickableTitle();
    constructClickableEditIcon();
}

EventItem::~EventItem() {
    delete _clickableSummary;
    delete _clickableTitle;
}

void EventItem::initializeQString() {
    _eventName = QString();
    _eventLocation = QString();
    _eventTrainer = QString();
    _eventDescription = QString();
    _eventLink = QString();
}

void EventItem::setActivityId(QString activityId) {
    _activityId = activityId;
}

void EventItem::setDate(QDate eventDate) {
    _eventDate = eventDate;
}

void EventItem::setStartTime(QTime eventStartTime) {
    _eventStartTime = eventStartTime;
}

void EventItem::setEndTime(QTime eventEndTime) {
    _eventEndTime = eventEndTime;
}

void EventItem::setName(QString eventName) {
    _eventName = eventName;
}

void EventItem::setLocation(QString eventLocation) {
    _eventLocation = eventLocation;
}

void EventItem::setLink(QString eventLink) {
    _eventLink = eventLink;
}

void EventItem::setIsImportant(bool isImportant) {
    _isImportant = isImportant;
}

void EventItem::setIsUserRegistered(bool isRegistered) {
    _isRegistered = isRegistered;
}

void EventItem::setIsClosed(bool isClosed) {
    _isClosed = isClosed;
}

void EventItem::setTrainer(QString eventTrainer) {
    _eventTrainer = eventTrainer;
}

void EventItem::setVacancy(int curVacancy, int maxVacancy) {
    _curVacancy = curVacancy;
    _maxVacancy = maxVacancy;

    _eventVacancy = QString(FORMAT_VACANCYSTRING).
            arg(QString::number(_curVacancy), QString::number(_maxVacancy));
}

void EventItem::setDescription(QString eventDescription) {
    _eventDescription = eventDescription;
}

void EventItem::setRegistrationDeadline(QDateTime registrationDeadline) {
    _registrationDeadline = registrationDeadline;
}

void EventItem::constructClickableSummary() {
    QVBoxLayout *labelLayout = new QVBoxLayout();

    QLabel *timeLabel = new QLabel(_eventStartTime.toString(FORMAT_TIME) + " - " +
                              _eventEndTime.toString(FORMAT_TIME));
    timeLabel->setStyleSheet("font: 13px");
    labelLayout->addWidget(timeLabel);

    QLabel *nameLabel = new QLabel(_eventName);
    nameLabel->setStyleSheet("font: 13px");
    nameLabel->setWordWrap(true);
    labelLayout->addWidget(nameLabel);

    QLabel *locationLabel = new QLabel (_eventLocation);
    locationLabel->setStyleSheet("font: 13px");
    locationLabel->setWordWrap(true);
    labelLayout->addWidget(locationLabel);

    QFrame *line = new QFrame();
    line->setFrameShape(QFrame::HLine);
    line->setLineWidth(3);
    labelLayout->addWidget(line);

    QFrame *labelFrame = new QFrame();
    labelFrame->setLayout(labelLayout);
    _clickableSummary = new ClickableWidget(labelFrame);

    connect (_clickableSummary, SIGNAL(isClicked()), this, SLOT(popUpWindow()));
}

void EventItem::constructClickableTitle() {
    QLabel *titleLabel = new QLabel("<u>" + _eventName);
    titleLabel->setAlignment(Qt::AlignCenter);
    titleLabel->setWordWrap(true);

    _clickableTitle = new ClickableWidget(titleLabel);

    connect (_clickableTitle, SIGNAL(isClicked()), this, SLOT(popUpRegisterWindow()));
}

void EventItem::constructClickableEditIcon() {
    QLabel *editIconLabel = new QLabel();
    editIconLabel->setPixmap(ICON_EDIT);
    editIconLabel->setAlignment(Qt::AlignCenter);
    _clickableEditIcon = new ClickableWidget(editIconLabel);
    connect (_clickableEditIcon, SIGNAL(isClicked()), this, SLOT(popUpActivityBox()));
}

//private slots
void EventItem::popUpWindow() {
    QString desc = _eventStartTime.toString(FORMAT_TIME) + " - " +
            _eventEndTime.toString(FORMAT_TIME) + NEWLINE +
            LABEL_LOCATION + _eventLocation + NEWLINE +
            LABEL_TRAINER + _eventTrainer + NEWLINE +
            LABEL_VACANCY + _eventVacancy + NEWLINE +
            NEWLINE +
            LABEL_DESCRIPTION + _eventDescription;

    EventBox *eventBox;
    if (UserDetails::getObject()->getUserRole() == UserDetails::ROLE_TRAINEE) {
        eventBox = new EventBox(_eventName, desc,
                                 _eventLink, _isRegistered, false);
    } else {
        eventBox = new EventBox(_eventName, desc, _eventLink);
    }

    eventBox->exec();
}

void EventItem::popUpRegisterWindow() {
    QString desc = _eventStartTime.toString(FORMAT_TIME) + " - " +
            _eventEndTime.toString(FORMAT_TIME) + NEWLINE +
            LABEL_LOCATION + _eventLocation + NEWLINE +
            LABEL_TRAINER + _eventTrainer + NEWLINE +
            LABEL_VACANCY + _eventVacancy + NEWLINE +
            NEWLINE +
            LABEL_DESCRIPTION + _eventDescription;

    EventBox *eventBox;
    if (UserDetails::getObject()->getUserRole() == UserDetails::ROLE_TRAINEE) {
        eventBox = new EventBox(_eventName, desc,
                                 _eventLink, _isRegistered);
    } else {
        eventBox = new EventBox(_eventName, desc, _eventLink);
    }

    connect (eventBox, SIGNAL(userHasClickedRegister(bool)),
             this, SLOT(handleUserClickRegister(bool)));

    eventBox->exec();

}

void EventItem::popUpActivityBox() {
    ActivityBox *activityBox = new ActivityBox(_eventName, _eventTrainer, _eventDate,
                                               _registrationDeadline, _eventStartTime,
                                               _eventEndTime, _maxVacancy, _eventLocation,
                                               _eventDescription, _isImportant, _eventLink);

    connect (activityBox, SIGNAL(passInfo(QVector<QString>, QDate, QDateTime, QVector<QTime>, int, bool)),
             this, SLOT(updateActivityInfo(QVector<QString>, QDate, QDateTime, QVector<QTime>, int, bool)));

    activityBox->exec();
}

void EventItem::updateActivityInfo(QVector<QString> info, QDate date, QDateTime registrationDeadline,
                                   QVector<QTime> time, int maxVacancy, bool isImportant) {
    assert(info.size() == 5);
    assert(time.size() == 2);

    QString name = info.at(0);
    QString trainer = info.at(1);
    QString location = info.at(2);
    QString description = info.at(3);
    QString eventLink = info.at(4);

    QTime startTime = time.at(0);
    QTime endTime = time.at(1);

    _eventName = name;
    _eventTrainer = trainer;
    _eventLocation = location;
    _eventDescription = description;
    _eventLink = eventLink;
    _eventStartTime = startTime;
    _eventEndTime = endTime;
    _eventDate = date;
    _registrationDeadline = registrationDeadline;
    _maxVacancy = maxVacancy;
    _isImportant = isImportant;

    assert(_activityId != STRING_EMPTY);
    emit isUpdated(_activityId);
}

//private

void EventItem::handleUserClickRegister(bool isRegister) {
    setIsUserRegistered(isRegister);
    emit this->clickedRegisterButton(_activityId, isRegister);
}

QString EventItem::getActivityId() {
    return _activityId;
}

QDate EventItem::getDate() {
    return _eventDate;
}

QString EventItem::getDescription() {
    return _eventDescription;
}

QString EventItem::getName() {
    return _eventName;
}

QTime EventItem::getStartTime() {
    return _eventStartTime;
}

QTime EventItem::getEndTime() {
    return _eventEndTime;
}

QDateTime EventItem::getRegistrationDeadline() {
    return _registrationDeadline;
}

QString EventItem::getTrainer() {
    return _eventTrainer;
}

QString EventItem::getLink() {
    return _eventLink;
}

QString EventItem::getLocation() {
    return _eventLocation;
}

QString EventItem::getVacancy() {
    return _eventVacancy;
}

int EventItem::getCurrentVacancy() {
    return _curVacancy;
}

int EventItem::getMaxVacancy() {
    return _maxVacancy;
}

bool EventItem::getIsRegistered() {
    return _isRegistered;
}

bool EventItem::getIsImportant() {
    return _isImportant;
}

bool EventItem::getIsClosed() {
    return _isClosed;
}

ClickableWidget* EventItem::getClickableSummary() {
    return _clickableSummary;
}

ClickableWidget* EventItem::getClickableTitle() {
    return _clickableTitle;
}

ClickableWidget* EventItem::getClickableEditIcon() {
    return _clickableEditIcon;
}
