#include "objects/leaveobj.h"

LeaveObj::LeaveObj() {
    setApplicationStatus("");
    setLeaveId("");
}

LeaveObj::LeaveObj(QDate startDate,
                   QDate endDate,
                   QString applicationStatus,
                   QString leaveId) {
    setStartDate(startDate);
    setEndDate(endDate);
    setApplicationStatus(applicationStatus);
    setLeaveId(leaveId);
}

LeaveObj::~LeaveObj() {
}


void LeaveObj::setStartDate(QDate startDate) {
    _startDate = startDate;
}

void LeaveObj::setEndDate(QDate endDate) {
    _endDate = endDate;
}

void LeaveObj::setApplicationStatus(QString applicationStatus) {
    _applicationStatus = applicationStatus;
}

void LeaveObj::setLeaveId(QString leaveId) {
    _leaveId = leaveId;
}

QDate LeaveObj::getStartDate() {
    assert (!_startDate.isNull());
    return _startDate;
}

QDate LeaveObj::getEndDate() {
    assert (!_endDate.isNull());
    return _endDate;
}

QString LeaveObj::getApplicationStatus() {
    assert (_applicationStatus != "");
    return _applicationStatus;
}

QString LeaveObj::getLeaveId() {
    assert (_leaveId != "");
    return _leaveId;
}

qint64 LeaveObj::getNumOfDates() {
    assert (!_startDate.isNull());
    assert (!_endDate.isNull());

    return _startDate.daysTo(_endDate);
}
