#include "objects/rosterassignobj.h"

const QString RosterAssignObj::STATUS_COMPLETED = "completed";
const QString RosterAssignObj::STATUS_NOT_COMPLETED = "not completed";
const QString RosterAssignObj::STATUS_WAIT = "pending";


RosterAssignObj::RosterAssignObj() {
    QDate date = QDate();
    QString status = "";
    QString rosterAssignId = "";
    QString userId = "";
    QString userRole = "";
    initializeClass(date,
                    status,
                    rosterAssignId,
                    userId,
                    userRole);
}

RosterAssignObj::RosterAssignObj(QDate date,
                                 QString status,
                                 QString rosterAssignId,
                                 QString userId,
                                 QString userRole) {
    initializeClass(date,
                    status,
                    rosterAssignId,
                    userId,
                    userRole);
}

RosterAssignObj::~RosterAssignObj() {
}

//public methods:
QDate RosterAssignObj::getAssignedDate() {
    assert (!_assignedDate.isNull());
    return _assignedDate;
}

QString RosterAssignObj::getStatus() {
    assert (!_status.isEmpty());
    return _status;
}

QString RosterAssignObj::getRosterAssignId() {
    assert (!_rosterAssignId.isEmpty());
    return _rosterAssignId;
}

QString RosterAssignObj::getUserId() {
    assert (!_userId.isEmpty());
    return _userId;
}

QString RosterAssignObj::getRole() {
    assert (!_userRole.isEmpty());
    return _userRole;
}

void RosterAssignObj::setAssignedDate(QDate date) {
    _assignedDate = date;
}

void RosterAssignObj::setStatus(QString status) {
    _status = status;
}

void RosterAssignObj::setRosterAssignId(QString rosterAssignId) {
    _rosterAssignId = rosterAssignId;
}

void RosterAssignObj::setUserId(QString userId) {
    _userId = userId;
}

void RosterAssignObj::setUserRole(QString userRole) {
    _userRole = userRole;
}

//private methods
void RosterAssignObj::initializeClass(QDate date,
                                      QString status,
                                      QString rosterAssignId,
                                      QString userId,
                                      QString role) {
    setAssignedDate(date);
    setStatus(status);
    setRosterAssignId(rosterAssignId);
    setUserId(userId);
    setUserRole(role);
}
