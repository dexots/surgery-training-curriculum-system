#include "objects/serverrequestobj.h"

ServerRequestObj::ServerRequestObj() {
    setMatricNo("");
    setRole("");
    setPassword("");
}

ServerRequestObj::~ServerRequestObj() {
}

//public methods
void ServerRequestObj::setMatricNo(QString matricNo) {
    _userMatricNo = matricNo;
}

void ServerRequestObj::setRole(QString role) {
    _userRole = role;
}

void ServerRequestObj::setPassword(QString password) {
    _clientPassword = password;
}

void ServerRequestObj::addTableName(QString table) {
    _tableNames.append(table);
}

void ServerRequestObj::addFileName(QString file) {
    _fileNames.append(file);
}

QJsonObject ServerRequestObj::toJsonObj() {
    assert (!_userMatricNo.isEmpty() &&
            !_userRole.isEmpty() &&
            !_clientPassword.isEmpty());

    QJsonObject jsonObject;
    jsonObject["userMatricNo"] = _userMatricNo;
    jsonObject["userRole"] = _userRole;
    jsonObject["clientPassword"] = _clientPassword;
    jsonObject["tableNames"] = constructTableJson(_tableNames, _tablesContents);
    jsonObject["fileNames"] = convertVectorToJson(_fileNames);

    return jsonObject;
}

void ServerRequestObj::addTableContent(QString tablename,
                                       QSqlRecord record,
                                       QVector<QString> columns) {
    QJsonObject json = convertRecordToJson(record, columns);
    addJsonToMap(tablename, json);
}


QJsonObject ServerRequestObj::convertRecordToJson(QSqlRecord record,
                                                  QVector<QString> params) {
    QJsonObject json;
    for (int i = 0; i < params.size(); i++) {
        json[params[i]] = record.value(params[i]).toString();
    }

    return json;
}

void ServerRequestObj::addJsonToMap(QString name, QJsonObject object) {
    _tablesContents[name].append(object);
}

QJsonObject ServerRequestObj::convertVectorToJson(QVector<QString> strVector) {
    QJsonObject jsonObject;
    for (int i = 0; i < strVector.size(); i++) {
        jsonObject[QString::number(i)] = strVector.value(i);
    }
    return jsonObject;
}

QJsonObject ServerRequestObj::constructTableJson(QVector<QString> strVector,
                                                 QMap<QString, QVector<QJsonObject> >
                                                 contents) {

    QJsonObject jsonObject;
    QJsonArray jsonArray;
    for (int i = 0; i < strVector.size(); i++) {
       QString objName = strVector.value(i);
       for (int j = 0; j < contents[objName].size(); j++) {
            jsonArray.append(contents[objName][j]);
       }
       jsonObject[objName] = jsonArray;
       jsonArray = QJsonArray();
    }
    return jsonObject;
}
