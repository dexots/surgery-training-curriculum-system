#include "objects/userdetails.h"

UserDetails* UserDetails::_userDetails = NULL;

const QString UserDetails::ROLE_TRAINER = "trainer";
const QString UserDetails::ROLE_TRAINEE = "trainee";
const QString UserDetails::ROLE_ADMIN = "admin";
const QString UserDetails::STRING_EMPTY = "";

//constructor
UserDetails::UserDetails() {
    UserDetails::initiateClass();
}

//destructor
UserDetails::~UserDetails() {
    UserDetails::clearGarbage();
}

//public methods
UserDetails* UserDetails::getObject() {
    if (_userDetails == NULL) {
        _userDetails = new UserDetails;
    }

    return _userDetails;
}

void UserDetails::deleteObject() {
    if (_userDetails != NULL) {
        delete _userDetails;
    }
}

QString UserDetails::getMatricNo() {
    assert (!_userMatricNo.isEmpty());
    return _userMatricNo;
}

QString UserDetails::getUserEmail() {
    return _userEmail;
}

QString UserDetails::getPersonalEmail() {
    return _personalEmail;
}

QString UserDetails::getUserName() {
    assert (!_userName.isEmpty());
    return _userName;
}

QString UserDetails::getUserRole() {
    assert (!_userRole.isEmpty());
    return _userRole;
}

QString UserDetails::getRotation() {
    return _rotation;
}

QString UserDetails::getAddress() {
    return _address;
}

QString UserDetails::getHandPhone() {
    return _handPhone;
}

QString UserDetails::getHomePhone() {
    return _homePhone;
}

bool UserDetails::getIsRosterMonster() {
    return _isRosterMonster;
}

void UserDetails::setMatricNo(QString matricNo) {
    _userMatricNo = matricNo;
}

void UserDetails::setUserEmail(QString userEmail) {
    _userEmail = userEmail;
}

void UserDetails::setPersonalEmail(QString personalEmail) {
    _personalEmail = personalEmail;
}

void UserDetails::setUserName(QString username) {
    _userName = username;
}

void UserDetails::setUserRole(QString userRole) {
    _userRole = userRole;
}

void UserDetails::setRotation(QString rotation) {
    _rotation = rotation;
}

void UserDetails::setAddress(QString address) {
    _address = address;
}

void UserDetails::setHandPhone(QString handPhone) {
    _handPhone = handPhone;
}

void UserDetails::setHomePhone(QString homePhone) {
    _homePhone = homePhone;
}

void UserDetails::setIsRosterMonster(bool isRosterMonster) {
    _isRosterMonster = isRosterMonster;
}

bool UserDetails::isNotInitialized() {
    return _userMatricNo.isEmpty() &&
           _userRole.isEmpty();
}

//private methods
void UserDetails::initiateClass() {
    setMatricNo(STRING_EMPTY);
    setUserEmail(STRING_EMPTY);
    setPersonalEmail(STRING_EMPTY);
    setUserName(STRING_EMPTY);
    setUserRole(STRING_EMPTY);
    setRotation(STRING_EMPTY);
    setAddress(STRING_EMPTY);
    setHandPhone(STRING_EMPTY);
    setHomePhone(STRING_EMPTY);
    setIsRosterMonster(false);
}

void UserDetails::clearGarbage() {
}
