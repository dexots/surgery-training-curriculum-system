#include "objects/exam.h"

Exam::Exam() {}

Exam::~Exam() {}

void Exam::setModuleCode(QString moduleCode) {
    _moduleCode = moduleCode;
}

void Exam::setModuleName(QString moduleName) {
    _moduleName = moduleName;
}

void Exam::setExamDate(QDate examDate) {
    _examDate = examDate;
}

void Exam::setExamTime(QTime examTime) {
    _examTime = examTime;
}

void Exam::setExamLocation(QString examLocation) {
    _examLocation = examLocation;
}

void Exam::setSeatNo(QString seatNo) {
    _seatNo = seatNo;
}

QString Exam::getModuleCode() {
    assert (!_moduleCode.isNull());
    return _moduleCode;
}

QString Exam::getModuleName() {
    assert (!_moduleName.isNull());
    return _moduleName;
}

QDate Exam::getExamDate() {
    assert (!_examDate.isNull());
    return _examDate;
}

QTime Exam::getExamTime() {
    assert (!_examTime.isNull());
    return _examTime;
}

QString Exam::getExamLocation() {
    assert (!_examLocation.isNull());
    return _examLocation;
}

QString Exam::getSeatNo() {
    assert (!_seatNo.isNull());
    return _seatNo;
}


