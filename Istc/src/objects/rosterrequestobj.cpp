#include "objects/rosterrequestobj.h"

const QString RosterRequestObj::STATUS_APPROVED = "Approved";
const QString RosterRequestObj::STATUS_DISAPPROVED = "Disapproved";
const QString RosterRequestObj::STATUS_PENDINGS = "Pending";

RosterRequestObj::RosterRequestObj(){
    setReason("");
    setStatus("");
    setUserId("");
    setRosterRequestId("");
}

RosterRequestObj::~RosterRequestObj(){
}

QDate RosterRequestObj::getDateRequest() {
    assert (!_dateRequest.isNull());
    return _dateRequest;
}

QDate RosterRequestObj::getDateSubmit() {
    assert (!_dateSubmit.isNull());
    return _dateSubmit;
}

QString RosterRequestObj::getReason() {
    assert (_reason != "");
    return _reason;
}

QString RosterRequestObj::getStatus() {
    assert (_status != "");
    return _status;
}

QString RosterRequestObj::getUserId() {
    assert (_userId != "");
    return _userId;
}

QString RosterRequestObj::getRosterRequestId() {
    assert (!_rosterRequestId.isEmpty());
    return _rosterRequestId;
}

void RosterRequestObj::setDateRequest(QDate dateRequest) {
    _dateRequest = dateRequest;
}

void RosterRequestObj::setDateSubmit(QDate dateSubmit) {
    _dateSubmit = dateSubmit;
}

void RosterRequestObj::setReason(QString reason) {
    _reason = reason;
}

void RosterRequestObj::setStatus(QString status) {
    _status = status;
}

void RosterRequestObj::setUserId(QString userId) {
    _userId = userId;
}

void RosterRequestObj::setRosterRequestId(QString rosterRequestId) {
    _rosterRequestId = rosterRequestId;
}

//private slots

void RosterRequestObj::approveRoster() {
    qDebug() << "RosterRequestObj -> approveRoster: Updating roster request to approved";
    setStatus(STATUS_APPROVED);
    emit statusUpdated(_rosterRequestId);
    qDebug() << "RosterRequestObj -> approveRoster: Updated roster request to approved";
}

void RosterRequestObj::disapproveRoster() {
    qDebug() << "RosterRequestObj -> disapproveRoster: Updating roster request to disapproved";
    setStatus(STATUS_DISAPPROVED);
    emit statusUpdated(_rosterRequestId);
    qDebug() << "RosterRequestObj -> disapproveRoster: Updated roster request to disapproved";
}
