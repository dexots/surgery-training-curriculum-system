#include "objects/proceduralcode.h"

const QString ProceduralCode::ICON_EDIT = ":/icon/ButtonIcon/edit.png";
const QString ProceduralCode::STRING_SPACE = " ";
const QString ProceduralCode::STRING_COLON = ":";

ProceduralCode::ProceduralCode() {
    QString procedureDate = "";
    QString procedureTime = "";
    QString patientID = "";
    QString patientDOB = "";
    int procedureCodeIndex = -1;
    QString procedureRemarks = "";
    QString outcome = "";

    initializeClass(procedureDate,
                    procedureTime,
                    patientID,
                    patientDOB,
                    procedureCodeIndex,
                    outcome,
                    procedureRemarks);
}

ProceduralCode::ProceduralCode(QString procedureDate, QString procedureTime, QString patientID,
                               QString patientDOB, int procedureCodeIndex,
                               QString outcome, QString procedureRemarks) {
    initializeClass(procedureDate,
                    procedureTime,
                    patientID,
                    patientDOB,
                    procedureCodeIndex,
                    outcome,
                    procedureRemarks);
}

ProceduralCode::~ProceduralCode() {}

ClickableWidget* ProceduralCode::getClickableEditIcon() {
    return _clickableEditIcon;
}

void ProceduralCode::setProcedureDate(QString procedureDate) {
    if (procedureDate.isEmpty()) {
        return;
    }

    //input format for procedureDate e.g. "2014 9 14"
    qDebug() << "ProceduralCode -> setProcedureDate: Setting procedure date";
    QStringList date = procedureDate.split(STRING_SPACE);

    try {
        int year = date.at(0).toInt();
        int month = date.at(1).toInt();
        int day = date.at(2).toInt();

        _procedureDate = QDate(year, month, day);
        qDebug() << "ProceduralCode -> setProcedureDate: Finished setting procedure date";
    } catch (int e) {
        _procedureDate = QDate::currentDate();
        qDebug() << "ProceduralCode -> setProcedureDate: Failed to set procedure date";
    }
}

void ProceduralCode::setProcedureDate(QDate procedureDate) {
    _procedureDate = procedureDate;
}

void ProceduralCode::setProcedureTime(QString procedureTime) {
    if (procedureTime.isEmpty()) {
        return;
    }

    //input format for procedure time e.g. "8:00"
    qDebug() << "ProceduralCode -> setProcedureTime: Setting procedure time";
    QStringList time = procedureTime.split(STRING_COLON);

    int hour = 0;
    int minute = 0;
    try {
        hour = time.at(0).toInt();
        minute = time.at(1).toInt();
        qDebug() << "ProceduralCode -> setProcedureTime: Finished setting procedure time";
    } catch (int e) {
        qDebug() << "ProceduralCode -> setProcedureTime: Failed to set procedure time";
    }

    _procedureTime = QTime(hour, minute);
}

void ProceduralCode::setProcedureTime(QTime procedureTime) {
    _procedureTime = procedureTime;
}

void ProceduralCode::setPatientID(QString patientID) {
    _patientID = patientID;
}

void ProceduralCode::setPatientDOB(QString patientDOB) {
    if (patientDOB.isEmpty()) {
        return;
    }

    //input format for patientDOB e.g. "2014 9 14"
    qDebug() << "ProceduralCode -> setPatientDOB: Setting patient date of birth";
    QStringList date = patientDOB.split(STRING_SPACE);
    try {
        int year = date.at(0).toInt();
        int month = date.at(1).toInt();
        int day = date.at(2).toInt();

        _patientDOB = QDate(year, month, day);
        qDebug() << "ProceduralCode -> setPatientDOB: Finished setting patient date of birth";
    } catch (int e) {
        _patientDOB = QDate::currentDate();
        qDebug() << "ProceduralCode -> setPatientDOB: Failed to set patient date of birth";
    }
}

void ProceduralCode::setPatientDOB(QDate patientDOB) {
    _patientDOB = patientDOB;
}

bool ProceduralCode::setProcedureCodeName(QString procedureName) {
    if (_hash.isEmpty() ||
            !_hash.contains(procedureName)) {
        assert(false);
        return false;
    }

    int num = _hash.value(procedureName);

    setProcedureCodeIndex(num);
    return true;
}

void ProceduralCode::setProcedureCodeIndex(int procedureCodeIndex) {
    _procedureCodeIndex = procedureCodeIndex;
}

void ProceduralCode::setProcedureRemarks(QString procedureRemarks) {
    _procedureRemarks = procedureRemarks;
}

void ProceduralCode::setProcedureID(QString procedureID) {
    _procedureID = procedureID;
}

void ProceduralCode::setProcedureOutcome(QString outcome) {
    _outcome = outcome;
}

QDate ProceduralCode::getProcedureDate() {
    return _procedureDate;
}

QTime ProceduralCode::getProcedureTime() {
    return _procedureTime;
}

QString ProceduralCode::getPatientID() {
    return _patientID;
}

QString ProceduralCode::getProcedureOutcome() {
    return _outcome;
}

QDate ProceduralCode::getPatientDOB() {
    return _patientDOB;
}

QString ProceduralCode::getProcedureCode() {
    if (_procedureCodeIndex < _procedureCode->size()) {
        return _procedureCode->at(_procedureCodeIndex);
    }
    return "procedure code index out of range";
}

QString ProceduralCode::getProcedureID() {
    assert (_procedureID != "");
    return _procedureID;
}

QString ProceduralCode::getProcedureRemarks() {
    return _procedureRemarks;
}

//private
void ProceduralCode::initializeClass(QString procedureDate,
                                     QString procedureTime,
                                     QString patientID,
                                     QString patientDOB,
                                     int procedureCodeIndex,
                                     QString outcome,
                                     QString procedureRemarks){
    QString errorProcedureID = "";
    initializeQString();

    setupProcedureCodeData();
    setProcedureID(errorProcedureID);
    setProcedureDate(procedureDate);
    setProcedureTime(procedureTime);
    setPatientID(patientID);
    setPatientDOB(patientDOB);
    setProcedureCodeIndex(procedureCodeIndex);
    setProcedureOutcome(outcome);
    setProcedureRemarks(procedureRemarks);

    constructClickableEditIcon();
}

void ProceduralCode::initializeQString() {
    _patientID = QString();
    _procedureRemarks = QString();
    _procedureCode = new QVector<QString>();
    _outcome = QString();
}

void ProceduralCode::setupProcedureCodeData() {
    importProcedureCodeFile();
    setupProcedureCodeMap(_procedureCode);
}

void ProceduralCode::setupProcedureCodeMap(QVector<QString> *codes) {
    for (int i = 0; i < codes->size(); i++) {
        QStringList lists = codes->at(i).split(":");
        QString numString = lists.at(0).trimmed();
        int num = numString.toInt();
        QString nameString = codes->at(i).trimmed();

        _hash.insert(nameString, num);
    }
}

void ProceduralCode::importProcedureCodeFile() {
    qDebug() << "ProceduralCode -> importProcedureCodeFile: Importing procedure code to vector";
    QFile inputFile(":/text/LocalFile/procedurecode.txt");

    if (inputFile.open(QIODevice::ReadOnly
                       | QIODevice::Text)) {
        QTextStream in(&inputFile);
        while ( !in.atEnd() ) {
            QString procedureCode = in.readLine();
            _procedureCode->push_back(procedureCode);
            qDebug() << "ProceduralCode -> importProcedureCodeFile: "
                     << procedureCode <<" is added to procedure code list";
        }
        inputFile.close();
    }
    qDebug() << "ProceduralCode -> importProcedureCodeFile: Imported procedure code to vector";
}


void ProceduralCode::constructClickableEditIcon() {
    QLabel *editIconLabel = new QLabel();
    editIconLabel->setPixmap(ICON_EDIT);
    editIconLabel->setAlignment(Qt::AlignCenter);
    _clickableEditIcon = new ClickableWidget(editIconLabel);

    connect (_clickableEditIcon, SIGNAL(isClicked()), this, SLOT(popUpWindow()));
}

void ProceduralCode::popUpWindow() {
    PatientBox *patientBox = new PatientBox(_patientID, _procedureDate.toString("yyyy M d"),
                                            _patientDOB.toString("yyyy M d"),
                                            _procedureTime.toString("h:m"),
                                            _procedureCodeIndex, _procedureRemarks,
                                            _outcome);

    patientBox->setEditMode();

    connect (patientBox, SIGNAL(passInfo(QVector<QString>)),
             this, SLOT(updateProceduralCode(QVector<QString>)));


    patientBox->exec();
}

//private slot
void ProceduralCode::updateProceduralCode(QVector<QString> info) {
    setProcedureDate(info.at(0));
    setProcedureTime(info.at(1));
    setPatientID(info.at(2));
    setPatientDOB(info.at(3));
    setProcedureCodeIndex(info.at(4).toInt());
    setProcedureOutcome(info.at(5));
    setProcedureRemarks(info.at(6));

    emit clickedUpdateButton();
}
