#include "objects/form.h"

const QString Form::FORM_TYPE_ONLINE = "online";
const QString Form::FORM_TYPE_SOFTCOPY = "softcopy";

Form::Form() {}

Form::~Form() {}

void Form::setFormName(QString formName) {
    _formName = formName;
}

void Form::setReleaseDate(QDate releaseDate) {
    _releaseDate = releaseDate;
}

void Form::setDeadline(QDate deadline) {
    _deadline = deadline;
}

void Form::setFormStatus(QString formStatus) {
    _formStatus = formStatus;
}

void Form::setFormUrl(QString formUrl) {
    _urlAddress = formUrl;
}

void Form::setType(QString type) {
    assert (type.toLower().trimmed() == FORM_TYPE_ONLINE ||
            type.toLower().trimmed() == FORM_TYPE_SOFTCOPY);

    _type = type;
}

QString Form::getFormName() {
    assert (!_formName.isNull());
    return _formName;
}

QDate Form::getReleaseDate() {
    assert (!_releaseDate.isNull());
    return _releaseDate;
}

QDate Form::getDeadline() {
    assert (!_deadline.isNull());
    return _deadline;
}

QString Form::getOnlineFormStatus() {
    assert (!_formStatus.isNull());
    return _formStatus;
}

QString Form::getUrlAddress() {
    assert (!_urlAddress.isNull());
    return _urlAddress;
}

QString Form::getType() {
    assert (!_type.isNull());
    return _type;
}

ClickableWidget* Form::getDownloadIcon() {
    assert (_clickableDownloadIcon);
    return _clickableDownloadIcon;
}

ClickableWidget* Form::getClickableTitle() {
    assert (_type == FORM_TYPE_ONLINE);
    QLabel *formName = new QLabel("<u>" + _formName + "</u>");
    formName->setAlignment(Qt::AlignCenter);
    _clickableTitle = new ClickableWidget(formName);
    connect (_clickableTitle, SIGNAL(isClicked()), this, SLOT(handleTitleClicked()));

    return _clickableTitle;
}

//private slots
void Form::handleTitleClicked() {
    assert (_type == FORM_TYPE_ONLINE);
    emit loadWebpage(_urlAddress);
}
