#include "objects/workinghoursobject.h"

const int WorkingHoursObject::MIN_TO_SEC = 60;
const int WorkingHoursObject::HOUR_TO_MIN = 60;

const int WorkingHoursObject::ERROR_NO_OF_SEC = -1;

WorkingHoursObject::WorkingHoursObject() {
    setNoOfSec(ERROR_NO_OF_SEC);
    setWorkingHourId("");
}

WorkingHoursObject::~WorkingHoursObject() {
}

//public methods:
QDateTime WorkingHoursObject::getCheckInTime() {
    assert (!_checkInTime.isNull());
    return _checkInTime;
}

QDateTime WorkingHoursObject::getCheckOutTime() {
    return _checkOutTime;
}


qint64 WorkingHoursObject::getNoOfSec() {
    return _noOfSec;
}

qint64 WorkingHoursObject::getNoOfHours() {
    return _noOfSec / ( MIN_TO_SEC * HOUR_TO_MIN );
}

QString WorkingHoursObject::getWorkingHourId() {
    assert (!_workingHourId.isEmpty());
    return _workingHourId;
}

void WorkingHoursObject::setCheckInTime(QDateTime checkInTime) {
    _checkInTime = checkInTime;
}

void WorkingHoursObject::setCheckOutTime(QDateTime checkOutTime) {
    _checkOutTime = checkOutTime;
}

void WorkingHoursObject::setNoOfSec(qint64 s) {
    _noOfSec = s;
}

void WorkingHoursObject::setWorkingHourId(QString workingHourId) {
    _workingHourId = workingHourId;
}

bool WorkingHoursObject::isEmpty() {
    return _checkInTime.isNull() &&
            _checkOutTime.isNull() &&
            _noOfSec == ERROR_NO_OF_SEC;
}

bool WorkingHoursObject::isEqual(WorkingHoursObject other) {
    return _workingHourId == other.getWorkingHourId();
}
