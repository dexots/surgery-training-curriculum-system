#include "objects/notes.h"

const QString Notes::ICON_EDIT = ":/icon/ButtonIcon/edit.png";
const QString Notes::STRING_SPACE = " ";

Notes::Notes() {
    constructClickableEditIcon();
}

Notes::Notes(QString notesDate, QString notesTitle, QString notesContent) {
    setNotesDate(notesDate);
    setNotesTitle(notesTitle);
    setNotesContent(notesContent);
    constructClickableEditIcon();
}

Notes::~Notes(){}

//public
void Notes::setNotesDate(QDate notesDate) {
    _notesDate = notesDate;
}

void Notes::setNotesDate(QString notesDate) {
    //input format yyyy M d e.g. "2015 4 23"
    qDebug() << "Notes -> setNotesDate: Setting notes date";
    QStringList date = notesDate.split(STRING_SPACE);

    try {
        int year = date.at(0).toInt();
        int month = date.at(1).toInt();
        int day = date.at(2).toInt();

        _notesDate = QDate(year, month, day);
        qDebug() << "Notes -> setNotesDate: Finished setting notes date";
    }
    catch (int e) {
        _notesDate = QDate::currentDate();
        qDebug() << "Notes -> setNotesDate: Failed to set notes date";
    }
}

void Notes::setNotesTitle(QString notesTitle) {
    _notesTitle = notesTitle;
}

void Notes::setNotesContent(QString notesContent) {
    _notesContent = notesContent;
}
void Notes::setNotesId(QString notesId) {
    _noteId = notesId;
}

QDate Notes::getNotesDate() {
    assert (!_notesDate.isNull());
    return _notesDate;
}

QString Notes::getNotesId() {
    assert (!_noteId.isEmpty());
    return _noteId;
}

QString Notes::getNotesTitle() {
    assert (!_notesTitle.isEmpty());
    return _notesTitle;
}

QString Notes::getNotesContent() {
    assert (!_notesContent.isEmpty());
    return _notesContent;
}

ClickableWidget* Notes::getClickableEditIcon() {
    return _clickableEditIcon;
}

//private
void Notes::constructClickableEditIcon() {
    QLabel *editIconLabel = new QLabel();
    editIconLabel->setPixmap(ICON_EDIT);
    editIconLabel->setAlignment(Qt::AlignCenter);
    _clickableEditIcon = new ClickableWidget(editIconLabel);

    connect (_clickableEditIcon, SIGNAL(isClicked()), this, SLOT(popUpWindow()));
}

//private slots
void Notes::popUpWindow() {
    NotesBox *notesBox = new NotesBox(_notesDate.toString("dd MMM yyyy"),
                                      _notesTitle, _notesContent);

    connect (notesBox, SIGNAL(passInfo(QVector<QString>)),
             this, SLOT(updateNotes(QVector<QString>)));

    notesBox->exec();
    delete notesBox;
}

void Notes::updateNotes(QVector<QString> info) {
    setNotesDate(info.at(0));
    setNotesTitle(info.at(1));
    setNotesContent(info.at(2));

    emit clickedUpdateButton(_noteId);
}
