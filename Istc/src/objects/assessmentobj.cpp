#include "objects/assessmentobj.h"

AssessmentObj::AssessmentObj() {
    setAssessId("");
    setAssessTitle("");
    setGrade("");
    setFeedback("");
    setTraineeId("");
    _graders = new QVector <QString>();
}

AssessmentObj::~AssessmentObj() {
    //delete _graders;
}

QDate AssessmentObj::getAssessDate() {
    assert (!_assessDate.isNull());
    return _assessDate;
}

QString AssessmentObj::getAssessTitle() {
    assert (_assessTitle != "");
    return _assessTitle;
}

QString AssessmentObj::getGrade() {
    return _grade;
}

QString AssessmentObj::getFeedback() {
    return _feedback;
}

QString AssessmentObj::getAssessId() {
    assert (_assessId != "");
    return _assessId;
}

QString AssessmentObj::getTraineeId() {
    assert (_traineeId != "");
    return _traineeId;
}

QVector <QString> *AssessmentObj::getGraders() {
    assert (!_graders->empty());
    return _graders;
}

QString AssessmentObj::getGradersString() {
    QString graderString = "";
    for (int i = 0; i < _graders->size(); i++) {
        graderString += _graders->value(i) + " ";
    }
    graderString = graderString.trimmed();
    return graderString;
}

void AssessmentObj::setAssessDate(QDate assessDate) {
    _assessDate = assessDate;
}

void AssessmentObj::setAssessTitle(QString assessTitle) {
    _assessTitle = assessTitle;
}

void AssessmentObj::setGrade(QString grade) {
    _grade = grade;
}

void AssessmentObj::setFeedback(QString feedback) {
    _feedback = feedback;
}

void AssessmentObj::setGraders(QVector<QString> *graders) {
    _graders = graders;
}

void AssessmentObj::addGrader(QString grader) {
    _graders->append(grader);
}

void AssessmentObj::setAssessId(QString assessId) {
    _assessId = assessId;
}

void AssessmentObj::setTraineeId(QString traineeId) {
    _traineeId = traineeId;
}
