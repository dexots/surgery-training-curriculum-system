#ifndef LOGIN_H
#define LOGIN_H

#include <iostream>

//@deprecated webkit
//#include <QWebView>

#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QUrl>
#include <QUrlQuery>
#include <QByteArray>
#include <QWidget>
#include <QVector>
#include <QString>
#include <cassert>

class QNetworkAccessManager;
class QNetworkReply;
class QNetworkRequest;
class QUrl;
class QUrlQuery;
class QBtyeArray;
class QWidget;
class QWebView;
class QString;
class cassert;

namespace login {
    class Login;
}

class Login:public QWidget {
    Q_OBJECT

public:
    explicit Login();
    ~Login();

public:
    void addParam(const char* field, const char* value);
    QUrl getNusOpenIdUrl();
    QString checkLoginStatus(QUrl givenUrl);
    QVector<QString> *retrieveInfo(QUrl givenUrl);

private:
    void initializeClass();
    void clearGarbage();
    void addDefaultParam();

private:
    QNetworkAccessManager *_networkManager;
    QUrlQuery *_params;

private:
    static const char* CONTENT_TYPE_HEADER;
    static const char* SERVER_ADDRESS;
    static const QString LOGIN_SUCCESS;
    static const QString LOGIN_CANCEL;
    static const QString LOGIN_FAIL;
    static const QString URL_LOGIN_SUCCESS;
    static const QString URL_LOGIN_CANCEL;
};



#endif // LOGIN_H
