﻿-- Table1: user --
insert into user VALUES("H1234567A", "Tan Mei Mei Lynnette", "lynnette.ng@outlook.com", 
"lynnette.ng@nuhs.edu.sg", "12 Singapore Road, Singapore 12345", "+65 6123 4567", 
"+65 9123 4567", "Team Infinity", "trainee", 1);
insert into user VALUES("T0234567Z", "David Chan", "davidchan@gmail.com", 
"davidchan@nuhs.edu.sg", "12 Science Park Road, Singapore 236478", "+65 6473 2837", 
"+65 9238 2937", "Team Infinity", "trainer", 1);
insert into user VALUES("T0236478W", "Dex Lim Xing Yu", "dexlim@gmail.com", 
"dexlim@nuhs.edu.sg", "23 Tampines Road, Singapore 463238", "+65 6473 9237", 
"+65 9483 7238", "Team Awesome", "trainer", 0);

-- Table2: forms --
insert into forms VALUES("A0112084U2014-10-0810:101", "Leaving Evaluation Form", 
"2015-01-01", "2015-05-05", "https://www.dropbox.com/s/9s34gpyieq8vzdy/Presentation%20Form.docx?dl=0", "softcopy");
insert into forms VALUES("A0112084U2014-10-0810:132", "Common MO Residency Evaluation Form", 
"2015-01-01", "2015-05-05", "https://www.dropbox.com/s/newy2w4ssexgsv8/Presentation%20Form2.docx?dl=0", "softcopy");
insert into forms VALUES("A0112084U2014-10-0810:161", "Mini Clinical Evaluation Exercise", 
"2015-01-01", "2015-05-05", "https://docs.google.com/forms/d/17cfEdloyIXaXH9v7FQCHzQTD8xN3OFJZuEG0gIg9UrA/viewform?usp=send_form", "online");

-- Table3: userForms --
insert into userForms VALUES("H1234567A", "trainee", "A0112084U2014-10-0810:101", "Submitted");
insert into userForms VALUES("H1234567A", "trainee", "A0112084U2014-10-0810:132", "Not Submitted");
insert into userForms VALUES("H1234567A", "trainee", "A0112084U2014-10-0810:161", "Not Submitted");



-- Table4: exams --
insert into exams VALUES("clf1101", "Basic Cardiac Life Support", "2015-06-03", "14:00", "Hall 2");
insert into exams VALUES("clf2015", "Acute Cardiac Life Support", "2015-06-13", "09:00", "Hall 3");
insert into exams VALUES("med1224", "Basic Medical Knowledge", "2015-06-20", "10:00", "Hall 4");

-- Table5: traineeExams -- 
insert into traineeExams VALUES("H1234567A", "trainee", "clf1101", 23);
insert into traineeExams VALUES("H1234567A", "trainee", "clf2015", 45);
insert into traineeExams VALUES("H1234567A", "trainee", "med1224", 123);

-- Table6: activities --
insert into activities VALUES("T0234567Z2015-01-0110:101", "CXR Reading Workshop", 
"2015-03-25", "23:59",
"2015-03-27", "14:00", "16:00", 50, 49, "Seminar Room 1", 
"Lorem ipsum dolor sit amet, everti fabellas ius eu, quo id reque liber. Iriure equidem duo eu.", 
"www.google.com", 1);
insert into activities VALUES("T0234567Z2015-01-0111:102", "ECG Reading Workshop", 
"2015-03-25", "23:59",
"2015-04-02", "15:00", "18:00", 50, 35, "Seminar Room 1", 
"Lorem ipsum dolor sit amet, everti fabellas ius eu, quo id reque liber. Iriure equidem duo eu.", 
"www.google.com", 1);
insert into activities VALUES("T0234567Z2015-01-0112:003", "Basic Cardiac Life Support",
"2015-03-19", "23:59", 
"2015-03-20", "12:00", "14:00", 50, 50, "Seminar Room 1", 
"Lorem ipsum dolor sit amet, everti fabellas ius eu, quo id reque liber. Iriure equidem duo eu.", 
"www.google.com", 0);
insert into activities VALUES("T0234567Z2015-01-0112:104", "Acute Cardiac Life Support", 
"2015-03-19", "23:59", 
"2015-03-24", "12:00", "14:00", 50, 50, "Seminar Room 1", 
"Lorem ipsum dolor sit amet, everti fabellas ius eu, quo id reque liber. Iriure equidem duo eu.", 
"www.google.com", 0);
insert into activities VALUES("T0234567Z2015-01-0112:155", "Chest Tube Insertion & Removal", 
"2015-03-19", "23:59", 
"2015-03-25", "13:00", "16:00", 50, 30, "Seminar Room 1", 
"Lorem ipsum dolor sit amet, everti fabellas ius eu, quo id reque liber. Iriure equidem duo eu.", 
"www.google.com", 0);
insert into activities VALUES("T0234567Z2015-01-0112:306", "External Cardiac Pacing and Epicardial Pacing Wire Removal", 
"2015-04-09", "23:59", 
"2015-04-10", "13:00", "16:00", 50, 30, "Seminar Room 1", 
"Lorem ipsum dolor sit amet, everti fabellas ius eu, quo id reque liber. Iriure equidem duo eu.", 
"www.google.com", 0);
insert into activities VALUES("T0234567Z2015-01-0112:347", "Evidence-based Medicine Workshop", 
"2015-04-12", "23:59", 
"2015-04-14", "16:00", "18:00", 50, 30, "Seminar Room 1", 
"Lorem ipsum dolor sit amet, everti fabellas ius eu, quo id reque liber. Iriure equidem duo eu.", 
"www.google.com", 1);
insert into activities VALUES("T0234567Z2015-01-0112:448", "Open communication", 
"2015-03-25", "23:59", 
"2015-03-29", "17:00", "19:00", 50,20, "Seminar Room 2", 
"Lorem ipsum dolor sit amet, everti fabellas ius eu, quo id reque liber.", 
"www.google.com", 1);


-- Table7: userActivities -- 
insert into userActivities VALUES("H1234567A", "trainee", "T0234567Z2015-01-0110:101", "Registered");
insert into userActivities VALUES("H1234567A", "trainee", "T0234567Z2015-01-0111:102", "Not Registered"); 
insert into userActivities VALUES("H1234567A", "trainee", "T0234567Z2015-01-0112:003", "Registered");
insert into userActivities VALUES("H1234567A", "trainee", "T0234567Z2015-01-0112:104", "Not Registered");
insert into userActivities VALUES("H1234567A", "trainee", "T0234567Z2015-01-0112:155", "Not Registered");
insert into userActivities VALUES("H1234567A", "trainee", "T0234567Z2015-01-0112:306", "Not Registered");
insert into userActivities VALUES("H1234567A", "trainee", "T0234567Z2015-01-0112:347", "Not Registered");
insert into userActivities VALUES("H1234567A", "trainee", "T0234567Z2015-01-0112:448", "Registered");

insert into userActivities VALUES("T0234567Z", "trainer", "T0234567Z2015-01-0110:101", "open");
insert into userActivities VALUES("T0234567Z", "trainer", "T0234567Z2015-01-0111:102", "open");
insert into userActivities VALUES("T0234567Z", "trainer", "T0234567Z2015-01-0112:003", "open");
insert into userActivities VALUES("T0234567Z", "trainer", "T0234567Z2015-01-0112:104", "open");
insert into userActivities VALUES("T0234567Z", "trainer", "T0234567Z2015-01-0112:155", "open");
insert into userActivities VALUES("T0234567Z", "trainer", "T0234567Z2015-01-0112:306", "open");
insert into userActivities VALUES("T0234567Z", "trainer", "T0234567Z2015-01-0112:347", "open");
insert into userActivities VALUES("T0234567Z", "trainer", "T0234567Z2015-01-0112:448", "open");


-- Table8: assessment -- 
insert into assessment VALUES("T0236478W2015-02-2112:111", "H1234567A", "trainee", 
"Basic Medical Knowledge", "2015-02-20", "A", "Good grasp of the fundamental concepts");
insert into assessment VALUES("T0236478W2015-02-2112:132", "H1234567A", "trainee", 
"ECG Reading Workshop", "2015-02-10", "B+", "Needs more practice");
insert into assessment VALUES("T0236478W2015-02-2211:113", "H1234567A", "trainee", 
"Chest Tube Insertion", "2015-03-13", "B+", "Needs a more steady hand");

-- Table9: trainerAssessment -- 
insert into trainerAssessment VALUES("T0234567Z", "trainer", "T0236478W2015-02-2112:111");
insert into trainerAssessment VALUES("T0236478W", "trainer", "T0236478W2015-02-2112:132");
insert into trainerAssessment VALUES("T0236478W", "trainer", "T0236478W2015-02-2211:113");

-- Table10: notes --
insert into notes VALUES("H1234567A2014-12-2613:111", "Surgery Training TODO", 
"2015-03-15", "Remember to submit the forms and read through textbook", "H1234567A", "trainee");
insert into notes VALUES("H1234567A2014-12-3014:112", "Exam briefing", 
"2015-03-13", "Bring a pen and calculator", "H1234567A", "trainee");
insert into notes VALUES("H1234567A2015-03-0110:103", "Meeting with Dr. Ooi", 
"2015-03-20", "Write personal statement and submit resume", "H1234567A", "trainee");

insert into notes VALUES("H1234567A2014-12-2613:111", "Surgery Training TODO", 
"2015-03-15", "Remember to submit the forms and read through textbook", "T0234567Z", "trainer");
insert into notes VALUES("H1234567A2014-12-3014:112", "Exam briefing", 
"2015-03-13", "Bring a pen and calculator", "T0234567Z", "trainer");
insert into notes VALUES("H1234567A2015-03-0110:103", "Meeting with Dr. Ooi", 
"2015-03-20", "Write personal statement and submit resume", "T0234567Z", "trainer");



-- Table11: patient (with name field removed) --
insert into patient VALUES("X3505959H", "1993-07-10");
insert into patient VALUES("X3505934E", "1992-05-03");
insert into patient VALUES("X3458950P", "1965-12-03");

-- Table12: procedureTable --
insert into procedureTable VALUES("H1234567A2014-07-0414:101", 
"SD821H: Heart, Proximal Aortic Aneurysm, Aortic Root Replacement with Coronary Artery Reimplantation", 
"X3505959H", "2014-07-04", "14:00", "Arterial Switch Operation with Le Compte Manoeuvre, closure of VSD and direct closure of PFO", "Bad");
insert into procedureTable VALUES("H1234567A2014-08-0412:112", 
"SD823H: Heart, Pulmonary Valve Lesions, Balloon Valvuloplasty", 
"X3505934E", "2014-08-04", "12:00", "Correction of Amomalous Pulmonary Venous Drianage", "GOOD");
insert into procedureTable VALUES("H1234567A2014-03-0713:203", 
"SD726H: Heart, Robotic Surgery, Mitral Valve Repair, Thymectomy and Atrial Septal Defect", 
"X3458950P", "2014-03-07", "13:00", "Single chamber pacemaker insertion", "alive");

-- Table13: userProcedure -- 
insert into userProcedure VALUES("H1234567A", "trainee", "H1234567A2014-07-0414:101");
insert into userProcedure VALUES("H1234567A", "trainee", "H1234567A2014-08-0412:112");
insert into userProcedure VALUES("H1234567A", "trainee", "H1234567A2014-03-0713:203");

insert into userProcedure VALUES("T0234567Z", "trainer", "H1234567A2014-07-0414:101");
insert into userProcedure VALUES("T0234567Z", "trainer", "H1234567A2014-08-0412:112");
insert into userProcedure VALUES("T0234567Z", "trainer", "H1234567A2014-03-0713:203");




-- Table14: workingHours -- 
insert into workingHours VALUES("H1234567A", "trainee", "H1234567A2015-02-0214:001", "2015-02-02", "09:00", "2015-02-02", "14:00");
insert into workingHours VALUES("H1234567A", "trainee", "H1234567A2015-02-0418:002", "2015-02-04", "09:00", "2015-02-04", "18:00");
insert into workingHours VALUES("H1234567A", "trainee", "H1234567A2015-02-0521:003", "2015-02-05", "09:00", "2015-02-05", "21:00");
insert into workingHours VALUES("H1234567A", "trainee", "H1234567A2015-02-0620:004", "2015-02-06", "10:00", "2015-02-06", "20:00");
insert into workingHours VALUES("H1234567A", "trainee", "H1234567A2015-02-0720:325", "2015-02-07", "08:13", "2015-02-07", "20:32");
insert into workingHours VALUES("H1234567A", "trainee", "H1234567A2015-02-0819:476", "2015-02-08", "09:12", "2015-02-08", "19:47");

insert into workingHours VALUES("T0234567Z", "trainer", "T0234567Z-02-0214:001", "2015-04-02", "09:00", "2015-04-02", "14:00");
insert into workingHours VALUES("T0234567Z", "trainer", "T0234567Z-02-0418:002", "2015-04-04", "09:00", "2015-04-04", "18:00");
insert into workingHours VALUES("T0234567Z", "trainer", "T0234567Z-02-0521:003", "2015-04-05", "09:00", "2015-04-05", "21:00");
insert into workingHours VALUES("T0234567Z", "trainer", "T0234567Z-02-0620:004", "2015-04-16", "10:00", "2015-02-16", "20:00");
insert into workingHours VALUES("T0234567Z", "trainer", "T0234567Z-02-0720:325", "2015-04-19", "08:13", "2015-02-19", "20:32");
insert into workingHours VALUES("T0234567Z", "trainer", "T0234567Z-02-0819:476", "2015-04-21", "09:12", "2015-02-21", "19:47");


-- Table15: rosterBlockOutReq --
insert into rosterBlockOutReq VALUES("H1234567A2015-02-0219:471", "2015-02-04", "2015-02-15", "Brother's graduation", "H1234567A", "trainee", "Approved");
insert into rosterBlockOutReq VALUES("H1234567A2015-02-0819:412", "2015-03-05", "2015-03-20", "Father's birthday", "H1234567A", "trainee", "Pending");
insert into rosterBlockOutReq VALUES("H1234567A2015-02-0820:433", "2015-03-06", "2015-03-28", "Girlfriend's company dinner", "H1234567A", "trainee", "Pending");

-- Table16: rosterAssignment --
insert into rosterAssignment VALUES("H1234567A2015-02-0711:101", "2015-03-13");
insert into rosterAssignment VALUES("H1234567A2015-02-0711:122", "2015-03-17");
insert into rosterAssignment VALUES("H1234567A2015-02-0711:133", "2015-03-23");
insert into rosterAssignment VALUES("H1234567A2015-02-0711:144", "2015-03-30");

-- Table: userRosterAssignment -- 
insert into userRosterAssignment VALUES("H1234567A", "trainee", "H1234567A2015-02-0711:101", "Completed");
insert into userRosterAssignment VALUES("H1234567A", "trainee", "H1234567A2015-02-0711:122", "Not Completed");
insert into userRosterAssignment VALUES("H1234567A", "trainee", "H1234567A2015-02-0711:133", "Not Completed");
insert into userRosterAssignment VALUES("H1234567A", "trainee", "H1234567A2015-02-0711:144", "Not Completed");


-- Table: leaveApp --
insert into leaveApp VALUES("H1234567A2015-03-0210:001", "2015-03-03", "2015-03-07", "H1234567A", "trainee", "Pending");
insert into leaveApp VALUES("H1234567A2015-03-0211:002", "2015-04-02", "2015-04-04", "H1234567A", "trainee", "Pending");
insert into leaveApp VALUES("H1234567A2015-03-0212:003", "2015-05-03", "2015-05-05", "H1234567A", "trainee", "Approved");

insert into leaveApp VALUES("H1234567A2015-03-0210:001", "2015-03-03", "2015-04-07", "T0234567Z", "trainer", "Approved");
insert into leaveApp VALUES("H1234567A2015-03-0211:002", "2015-04-02", "2015-04-24", "T0234567Z", "trainer", "Approved");
insert into leaveApp VALUES("H1234567A2015-03-0212:003", "2015-05-03", "2015-05-05", "T0234567Z", "trainer", "Pending");













