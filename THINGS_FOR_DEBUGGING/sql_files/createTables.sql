CREATE TABLE user (
matricNo VARCHAR(20) NOT NULL UNIQUE, 
name VARCHAR(100) NOT NULL,
personalEmail VARCHAR(100),
nuhEmail VARCHAR(100),
address TEXT,
homePhone VARCHAR(30),
handPhone VARCHAR(30),
rotation VARCHAR(100),
role VARCHAR(30) NOT NULL 
CHECK (
LOWER(role) LIKE ('trainer') OR 
LOWER(role) LIKE ('trainee') OR
LOWER(role) LIKE ('admin')),
rosterMonster INT NOT NULL 
CHECK (rosterMonster = 0 OR rosterMonster = 1), 
PRIMARY KEY (matricNo, role));

CREATE TABLE forms (
formsId TEXT NOT NULL,
name TEXT NOT NULL,
dateReleased TEXT NOT NULL,
deadline TEXT NOT NULL,
url TEXT NOT NULL UNIQUE,
type VARCHAR(20) NOT NULL 
CHECK (LOWER(type) LIKE ('online') 
OR LOWER(type) LIKE ('softcopy')),
PRIMARY KEY (formsId));

CREATE TABLE exams (
moduleId TEXT NOT NULL,
moduleName TEXT NOT NULL,
date TEXT NOT NULL,
time TEXT NOT NULL,
location TEXT,
PRIMARY KEY (moduleId));

CREATE TABLE activities (
activityId TEXT NOT NULL,
title TEXT NOT NULL,
registrationDeadlineTime TEXT NOT NULL,
registrationDeadlineDate TEXT NOT NULL,
eventDate TEXT NOT NULL,
eventStartTime TEXT NOT NULL,
eventEndTime TEXT NOT NULL,
maxVacancy INT NOT NULL,
curVacancy INT NOT NULL,
location TEXT NOT NULL,
description text NOT NULL,
link TEXT,
important INT NOT NULL 
CHECK (important = 1 OR important = 0),
PRIMARY KEY (activityId));

CREATE TABLE assessment (
assessmentId TEXT NOT NULL,
userId VARCHAR(20) NOT NULL,
role VARCHAR(30) NOT NULL 
CHECK (LOWER(role) LIKE ('trainee')),
title TEXT NOT NULL,
date TEXT NOT NULL,
grade VARCHAR(20) NOT NULL,
feedback TEXT NOT NULL,
PRIMARY KEY (assessmentId),
FOREIGN KEY (userId, role) REFERENCES user(matricNo, role) ON DELETE CASCADE);

CREATE TABLE notes (
noteId TEXT NOT NULL,
noteTitle TEXT NOT NULL,
date TEXT NOT NULL,
content TEXT NOT NULL,
userId VARCHAR(20) NOT NULL, 
role VARCHAR(30) NOT NULL,
FOREIGN KEY(userId, role) REFERENCES user(matricNo, role) ON DELETE CASCADE,
PRIMARY KEY(userId, role, noteId));

CREATE TABLE patient (
patientId VARCHAR (50) NOT NULL,
dob TEXT NOT NULL,
PRIMARY KEY (patientId));

CREATE TABLE procedureTable (
procedureId TEXT NOT NULL, 
name TEXT NOT NULL,
patientId VARCHAR (50) NOT NULL,
date TEXT,
time TEXT,
remarks TEXT,
outcome TEXT NOT NULL CHECK (
LOWER(outcome) LIKE ('good') OR 
LOWER(outcome) LIKE ('bad') OR
LOWER(outcome) LIKE ('dead') OR
LOWER(outcome) LIKE ('alive')),
PRIMARY KEY (procedureId),
FOREIGN KEY (patientId) REFERENCES patient (patientId) ON DELETE CASCADE);

CREATE TABLE workingHours (
userId VARCHAR(20) NOT NULL,
role VARCHAR(30) NOT NULL,
workingHoursId TEXT NOT NULL UNIQUE,
dayIn TEXT NOT NULL UNIQUE,
timeIn TEXT NOT NULL,
dayOut TEXT NOT NULL,
timeOut TEXT NOT NULL,
PRIMARY KEY (workingHoursId, userId, role),
FOREIGN KEY (userId, role) REFERENCES user (matricNo, role) ON DELETE CASCADE);

CREATE TABLE rosterBlockOutReq (
reqId TEXT NOT NULL,
submitDate TEXT NOT NULL,
reqDate TEXT NOT NULL,
reason TEXT NOT NULL,
userId VARCHAR(20) NOT NULL, 
role VARCHAR(30) NOT NULL,
status VARCHAR(20) NOT NULL 
CHECK (LOWER(status) LIKE('pending')
OR lower(status) LIKE ('approved')
OR LOWER(status) LIKE ('disapproved')
),
PRIMARY KEY (userId, role, reqId),
FOREIGN KEY (userId, role) REFERENCES user (matricNo, role)
ON DELETE CASCADE);

CREATE TABLE rosterAssignment (
rosterId TEXT NOT NULL, 
date TEXT NOT NULL,
PRIMARY KEY (rosterId));

CREATE TABLE leaveApp (
leaveAppId TEXT NOT NULL,
startDate TEXT NOT NULL,
endDate TEXT NOT NULL,
userId VARCHAR(20) NOT NULL,
role  VARCHAR(30) NOT NULL,
status VARCHAR(45) NOT NULL, 
CHECK (
LOWER(status) LIKE ('pending')
OR LOWER(status) LIKE ('approved')
OR LOWER(status) LIKE ('disapproved')
),
FOREIGN KEY(userId, role) REFERENCES user(matricNo, role) ON DELETE CASCADE,
PRIMARY KEY(userId, leaveAppId));

CREATE TABLE userRosterAssignment (
userId VARCHAR(20) NOT NULL, 
role VARCHAR(30) NOT NULL,
rosterId TEXT NOT NULL, 
status VARCHAR(45) NOT NULL 
CHECK(LOWER(status) LIKE ('completed')
OR LOWER(status) LIKE ('not completed')
OR LOWER(status) LIKE ('pending')),
FOREIGN KEY (userId, role) REFERENCES user (matricNo, role) ON DELETE CASCADE,
FOREIGN KEY (rosterId) REFERENCES rosterAssignment (rosterId) ON DELETE CASCADE,
PRIMARY KEY (userId, role, rosterId));

CREATE TABLE userProcedure (
userId VARCHAR(20) NOT NULL,
role VARCHAR(30) NOT NULL,
procedureId TEXT NOT NULL, 
FOREIGN KEY (userId, role) REFERENCES user (matricNo, role) ON DELETE CASCADE,
FOREIGN KEY (procedureId) REFERENCES procedureTable (procedureId) ON DELETE CASCADE,
PRIMARY KEY (userId, procedureId));


CREATE TABLE userForms (
userId VARCHAR(20) NOT NULL, 
role VARCHAR(30) NOT NULL,
formsId TEXT NOT NULL,
status VARCHAR(45) NOT NULL 
CHECK(LOWER(status) LIKE ('submitted')
OR LOWER(status) LIKE ('not submitted')),
FOREIGN KEY (userId, role) REFERENCES user(matricNo, role) ON DELETE CASCADE,
FOREIGN KEY (formsId)REFERENCES forms (formsId) ON DELETE CASCADE,
PRIMARY KEY (userId, formsId));

CREATE TABLE traineeExams (
traineeId VARCHAR(20) NOT NULL,
role VARCHAR(30) NOT NULL
CHECK (LOWER(role) LIKE ('trainee')),
moduleId TEXT NOT NULL,
seatNo VARCHAR(45) NOT NULL,
FOREIGN KEY (traineeId, role) REFERENCES user(matricNo, role) ON DELETE CASCADE,
FOREIGN KEY (moduleId) REFERENCES exams (moduleId) ON DELETE CASCADE,
PRIMARY KEY (traineeId, moduleId));

CREATE TABLE userActivities (
userId VARCHAR(20) NOT NULL,
role VARCHAR(30) NOT NULL,
activityId TEXT NOT NULL,
status VARCHAR (45) NOT NULL 
CHECK (
(LOWER(role) LIKE ('trainee') AND (LOWER(status) LIKE ('registered') OR LOWER(status) LIKE ('not registered'))) OR
(LOWER(role) LIKE ('trainer') AND (LOWER(status) LIKE ('open') OR 
LOWER(status) LIKE ('close')))),
FOREIGN KEY (userId, role) REFERENCES user (matricNo, role) ON DELETE CASCADE,
FOREIGN KEY (activityId) REFERENCES activities (activityId)  ON DELETE CASCADE,
PRIMARY KEY (userId, activityId));

CREATE TABLE trainerAssessment (
trainerId VARCHAR(20) NOT NULL, 
role VARCHAR(30) NOT NULL 
CHECK(LOWER(role) LIKE ('trainer')) , 
assessmentId TEXT NOT NULL,
FOREIGN KEY (trainerId, role) REFERENCES user (matricNo, role) ON DELETE CASCADE,
FOREIGN KEY (assessmentId) REFERENCES assessment (assessmentId) ON DELETE CASCADE,
PRIMARY KEY (trainerId, assessmentId));
