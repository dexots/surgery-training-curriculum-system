-- Trainer USERS -- 
insert into user VALUES("Ooi Oon Cheong", "Ooi Oon Cheong", "ooiooncheong@gmail.com", 
"ooiooncheong@nuhs.edu.sg", "23 Tampines Road, Singapore 463238", "+65 6473 9237", 
"+65 9483 7238", "Team Awesome", "trainer", 0);

insert into user VALUES("Jimmy Hon", "Jimmy Hon", "jimmyhon@gmail.com", 
"jimmyhon@nuhs.edu.sg", "23 Tampines Road, Singapore 463238", "+65 6473 9237", 
"+65 9483 7238", "Team Awesome", "trainer", 0);


-- Admin USERS --
insert into user VALUES("Tan Sau Kuen", "Tan Sau Kuen", "tansaukuen@gmail.com", 
"tansaukuen@nuhs.edu.sg", "23 Tampines Road, Singapore 463238", "+65 6473 9237", 
"+65 9483 7238", "Team Awesome", "trainer", 0);

insert into user VALUES("Tay Ee Leen", "Tay Ee Leen", "tayeeleen@gmail.com", 
"tayeeleen@nuhs.edu.sg", "23 Tampines Road, Singapore 463238", "+65 6473 9237", 
"+65 9483 7238", "Team Awesome", "trainer", 0);

-- Trainee USERS -- 
insert into user VALUES("Jai Ajitchandra Sule", "Jai Ajitchandra Sule", "jaiajitchandrasule@gmail.com", 
"jaiajitchandrasule@nuhs.edu.sg", "23 Tampines Road, Singapore 463238", "+65 6473 9237", 
"+65 9483 7238", "Team Awesome", "trainee", 0);

insert into user VALUES("Li Yue", "Li Yue", "liyue@gmail.com", 
"liyue@nuhs.edu.sg", "23 Tampines Road, Singapore 463238", "+65 6473 9237", 
"+65 9483 7238", "Team Awesome", "trainee", 0);

insert into user VALUES("Nieh Chih Chiang", "Nieh Chih Chiang", "niehchihchiang@gmail.com", 
"niehchihchiang@nuhs.edu.sg", "23 Tampines Road, Singapore 463238", "+65 6473 9237", 
"+65 9483 7238", "Team Awesome", "trainee", 0);

insert into user VALUES("Chang Guohao", "Chang Guohao", "changguohao@gmail.com", 
"changguohao@nuhs.edu.sg", "23 Tampines Road, Singapore 463238", "+65 6473 9237", 
"+65 9483 7238", "Team Awesome", "trainee", 0);

insert into user VALUES("Chen Jian Ye", "Chen Jian Ye", "chenjianye@gmail.com", 
"chenjianye@nuhs.edu.sg", "23 Tampines Road, Singapore 463238", "+65 6473 9237", 
"+65 9483 7238", "Team Awesome", "trainee", 0);

insert into user VALUES("Qian Qi", "Qian Qi", "qianqi@gmail.com", 
"qianqi@nuhs.edu.sg", "23 Tampines Road, Singapore 463238", "+65 6473 9237", 
"+65 9483 7238", "Team Awesome", "trainee", 0);

insert into user VALUES("Julian Nester Matigal", "Julian Nester Matigal", "juliannestermatigal@gmail.com", 
"juliannestermatigal@nuhs.edu.sg", "23 Tampines Road, Singapore 463238", "+65 6473 9237", 
"+65 9483 7238", "Team Awesome", "trainee", 0);

-- USER FORMS -- 
insert into userForms VALUES("Ooi Oon Cheong", "trainer", "A0112084U2014-10-0810:101", "Not Submitted");
insert into userForms VALUES("Ooi Oon Cheong", "trainer", "A0112084U2014-10-0810:132", "Not Submitted");
insert into userForms VALUES("Ooi Oon Cheong", "trainer", "A0112084U2014-10-0810:161", "Not Submitted");

insert into userForms VALUES("Jimmy Hon", "trainer", "A0112084U2014-10-0810:101", "Not Submitted");
insert into userForms VALUES("Jimmy Hon", "trainer", "A0112084U2014-10-0810:132", "Not Submitted");
insert into userForms VALUES("Jimmy Hon", "trainer", "A0112084U2014-10-0810:161", "Not Submitted");	

insert into userForms VALUES("Tan Sau Kuen", "trainer", "A0112084U2014-10-0810:101", "Not Submitted");
insert into userForms VALUES("Tan Sau Kuen", "trainer", "A0112084U2014-10-0810:132", "Not Submitted");
insert into userForms VALUES("Tan Sau Kuen", "trainer", "A0112084U2014-10-0810:161", "Not Submitted");		

insert into userForms VALUES("Tay Ee Leen", "trainer", "A0112084U2014-10-0810:101", "Not Submitted");
insert into userForms VALUES("Tay Ee Leen", "trainer", "A0112084U2014-10-0810:132", "Not Submitted");
insert into userForms VALUES("Tay Ee Leen", "trainer", "A0112084U2014-10-0810:161", "Not Submitted");	

insert into userForms VALUES("Jai Ajitchandra Sule", "trainee", "A0112084U2014-10-0810:101", "Not Submitted");
insert into userForms VALUES("Jai Ajitchandra Sule", "trainee", "A0112084U2014-10-0810:132", "Not Submitted");
insert into userForms VALUES("Jai Ajitchandra Sule", "trainee", "A0112084U2014-10-0810:161", "Not Submitted");

insert into userForms VALUES("Li Yue", "trainee", "A0112084U2014-10-0810:101", "Not Submitted");
insert into userForms VALUES("Li Yue", "trainee", "A0112084U2014-10-0810:132", "Not Submitted");
insert into userForms VALUES("Li Yue", "trainee", "A0112084U2014-10-0810:161", "Not Submitted");

insert into userForms VALUES("Nieh Chih Chiang", "trainee", "A0112084U2014-10-0810:101", "Not Submitted");
insert into userForms VALUES("Nieh Chih Chiang", "trainee", "A0112084U2014-10-0810:132", "Not Submitted");
insert into userForms VALUES("Nieh Chih Chiang", "trainee", "A0112084U2014-10-0810:161", "Not Submitted");

insert into userForms VALUES("Chang Guohao", "trainee", "A0112084U2014-10-0810:101", "Not Submitted");
insert into userForms VALUES("Chang Guohao", "trainee", "A0112084U2014-10-0810:132", "Not Submitted");
insert into userForms VALUES("Chang Guohao", "trainee", "A0112084U2014-10-0810:161", "Not Submitted");

insert into userForms VALUES("Chen Jian Ye", "trainee", "A0112084U2014-10-0810:101", "Not Submitted");
insert into userForms VALUES("Chen Jian Ye", "trainee", "A0112084U2014-10-0810:132", "Not Submitted");
insert into userForms VALUES("Chen Jian Ye", "trainee", "A0112084U2014-10-0810:161", "Not Submitted");

insert into userForms VALUES("Qian Qi", "trainee", "A0112084U2014-10-0810:101", "Not Submitted");
insert into userForms VALUES("Qian Qi", "trainee", "A0112084U2014-10-0810:132", "Not Submitted");
insert into userForms VALUES("Qian Qi", "trainee", "A0112084U2014-10-0810:161", "Not Submitted");	

insert into userForms VALUES("Julian Nester Matigal", "trainee", "A0112084U2014-10-0810:101", "Not Submitted");
insert into userForms VALUES("Julian Nester Matigal", "trainee", "A0112084U2014-10-0810:132", "Not Submitted");
insert into userForms VALUES("Julian Nester Matigal", "trainee", "A0112084U2014-10-0810:161", "Not Submitted");		


-- TRAINEE ACTIVITIES -- 
insert into userActivities VALUES("Jai Ajitchandra Sule", "trainee", "T0234567Z2015-01-0110:101", "Registered");
insert into userActivities VALUES("Jai Ajitchandra Sule", "trainee", "T0234567Z2015-01-0111:102", "Not Registered"); 
insert into userActivities VALUES("Jai Ajitchandra Sule", "trainee", "T0234567Z2015-01-0112:003", "Registered");
insert into userActivities VALUES("Jai Ajitchandra Sule", "trainee", "T0234567Z2015-01-0112:104", "Not Registered");
insert into userActivities VALUES("Jai Ajitchandra Sule", "trainee", "T0234567Z2015-01-0112:155", "Not Registered");
insert into userActivities VALUES("Jai Ajitchandra Sule", "trainee", "T0234567Z2015-01-0112:306", "Not Registered");
insert into userActivities VALUES("Jai Ajitchandra Sule", "trainee", "T0234567Z2015-01-0112:347", "Not Registered");
insert into userActivities VALUES("Jai Ajitchandra Sule", "trainee", "T0234567Z2015-01-0112:448", "Registered");

insert into userActivities VALUES("Li Yue", "trainee", "T0234567Z2015-01-0110:101", "Registered");
insert into userActivities VALUES("Li Yue", "trainee", "T0234567Z2015-01-0111:102", "Not Registered"); 
insert into userActivities VALUES("Li Yue", "trainee", "T0234567Z2015-01-0112:003", "Registered");
insert into userActivities VALUES("Li Yue", "trainee", "T0234567Z2015-01-0112:104", "Not Registered");
insert into userActivities VALUES("Li Yue", "trainee", "T0234567Z2015-01-0112:155", "Not Registered");
insert into userActivities VALUES("Li Yue", "trainee", "T0234567Z2015-01-0112:306", "Not Registered");
insert into userActivities VALUES("Li Yue", "trainee", "T0234567Z2015-01-0112:347", "Not Registered");
insert into userActivities VALUES("Li Yue", "trainee", "T0234567Z2015-01-0112:448", "Registered");	

insert into userActivities VALUES("Nieh Chih Chiang", "trainee", "T0234567Z2015-01-0110:101", "Registered");
insert into userActivities VALUES("Nieh Chih Chiang", "trainee", "T0234567Z2015-01-0111:102", "Not Registered"); 
insert into userActivities VALUES("Nieh Chih Chiang", "trainee", "T0234567Z2015-01-0112:003", "Registered");
insert into userActivities VALUES("Nieh Chih Chiang", "trainee", "T0234567Z2015-01-0112:104", "Not Registered");
insert into userActivities VALUES("Nieh Chih Chiang", "trainee", "T0234567Z2015-01-0112:155", "Not Registered");
insert into userActivities VALUES("Nieh Chih Chiang", "trainee", "T0234567Z2015-01-0112:306", "Not Registered");
insert into userActivities VALUES("Nieh Chih Chiang", "trainee", "T0234567Z2015-01-0112:347", "Not Registered");
insert into userActivities VALUES("Nieh Chih Chiang", "trainee", "T0234567Z2015-01-0112:448", "Registered");	

insert into userActivities VALUES("Chang Guohao", "trainee", "T0234567Z2015-01-0110:101", "Registered");
insert into userActivities VALUES("Chang Guohao", "trainee", "T0234567Z2015-01-0111:102", "Not Registered"); 
insert into userActivities VALUES("Chang Guohao", "trainee", "T0234567Z2015-01-0112:003", "Registered");
insert into userActivities VALUES("Chang Guohao", "trainee", "T0234567Z2015-01-0112:104", "Not Registered");
insert into userActivities VALUES("Chang Guohao", "trainee", "T0234567Z2015-01-0112:155", "Not Registered");
insert into userActivities VALUES("Chang Guohao", "trainee", "T0234567Z2015-01-0112:306", "Not Registered");
insert into userActivities VALUES("Chang Guohao", "trainee", "T0234567Z2015-01-0112:347", "Not Registered");
insert into userActivities VALUES("Chang Guohao", "trainee", "T0234567Z2015-01-0112:448", "Registered");	

insert into userActivities VALUES("Chen Jian Ye", "trainee", "T0234567Z2015-01-0110:101", "Registered");
insert into userActivities VALUES("Chen Jian Ye", "trainee", "T0234567Z2015-01-0111:102", "Not Registered"); 
insert into userActivities VALUES("Chen Jian Ye", "trainee", "T0234567Z2015-01-0112:003", "Registered");
insert into userActivities VALUES("Chen Jian Ye", "trainee", "T0234567Z2015-01-0112:104", "Not Registered");
insert into userActivities VALUES("Chen Jian Ye", "trainee", "T0234567Z2015-01-0112:155", "Not Registered");
insert into userActivities VALUES("Chen Jian Ye", "trainee", "T0234567Z2015-01-0112:306", "Not Registered");
insert into userActivities VALUES("Chen Jian Ye", "trainee", "T0234567Z2015-01-0112:347", "Not Registered");
insert into userActivities VALUES("Chen Jian Ye", "trainee", "T0234567Z2015-01-0112:448", "Registered");	

insert into userActivities VALUES("Qian Qi", "trainee", "T0234567Z2015-01-0110:101", "Registered");
insert into userActivities VALUES("Qian Qi", "trainee", "T0234567Z2015-01-0111:102", "Not Registered"); 
insert into userActivities VALUES("Qian Qi", "trainee", "T0234567Z2015-01-0112:003", "Registered");
insert into userActivities VALUES("Qian Qi", "trainee", "T0234567Z2015-01-0112:104", "Not Registered");
insert into userActivities VALUES("Qian Qi", "trainee", "T0234567Z2015-01-0112:155", "Not Registered");
insert into userActivities VALUES("Qian Qi", "trainee", "T0234567Z2015-01-0112:306", "Not Registered");
insert into userActivities VALUES("Qian Qi", "trainee", "T0234567Z2015-01-0112:347", "Not Registered");
insert into userActivities VALUES("Qian Qi", "trainee", "T0234567Z2015-01-0112:448", "Registered");	

insert into userActivities VALUES("Julian Nester Matigal", "trainee", "T0234567Z2015-01-0110:101", "Registered");
insert into userActivities VALUES("Julian Nester Matigal", "trainee", "T0234567Z2015-01-0111:102", "Not Registered"); 
insert into userActivities VALUES("Julian Nester Matigal", "trainee", "T0234567Z2015-01-0112:003", "Registered");
insert into userActivities VALUES("Julian Nester Matigal", "trainee", "T0234567Z2015-01-0112:104", "Not Registered");
insert into userActivities VALUES("Julian Nester Matigal", "trainee", "T0234567Z2015-01-0112:155", "Not Registered");
insert into userActivities VALUES("Julian Nester Matigal", "trainee", "T0234567Z2015-01-0112:306", "Not Registered");
insert into userActivities VALUES("Julian Nester Matigal", "trainee", "T0234567Z2015-01-0112:347", "Not Registered");
insert into userActivities VALUES("Julian Nester Matigal", "trainee", "T0234567Z2015-01-0112:448", "Registered");	


-- TRAINER ACTIVITIES -- 
insert into userActivities VALUES("Ooi Oon Cheong", "trainer", "T0234567Z2015-01-0110:101", "open");
insert into userActivities VALUES("Ooi Oon Cheong", "trainer", "T0234567Z2015-01-0111:102", "open");
insert into userActivities VALUES("Ooi Oon Cheong", "trainer", "T0234567Z2015-01-0112:003", "open");
insert into userActivities VALUES("Ooi Oon Cheong", "trainer", "T0234567Z2015-01-0112:104", "open");
insert into userActivities VALUES("Ooi Oon Cheong", "trainer", "T0234567Z2015-01-0112:155", "open");
insert into userActivities VALUES("Ooi Oon Cheong", "trainer", "T0234567Z2015-01-0112:306", "open");
insert into userActivities VALUES("Ooi Oon Cheong", "trainer", "T0234567Z2015-01-0112:347", "open");
insert into userActivities VALUES("Ooi Oon Cheong", "trainer", "T0234567Z2015-01-0112:448", "open");

insert into userActivities VALUES("Jimmy Hon", "trainer", "T0234567Z2015-01-0110:101", "open");
insert into userActivities VALUES("Jimmy Hon", "trainer", "T0234567Z2015-01-0111:102", "open");
insert into userActivities VALUES("Jimmy Hon", "trainer", "T0234567Z2015-01-0112:003", "open");
insert into userActivities VALUES("Jimmy Hon", "trainer", "T0234567Z2015-01-0112:104", "open");
insert into userActivities VALUES("Jimmy Hon", "trainer", "T0234567Z2015-01-0112:155", "open");
insert into userActivities VALUES("Jimmy Hon", "trainer", "T0234567Z2015-01-0112:306", "open");
insert into userActivities VALUES("Jimmy Hon", "trainer", "T0234567Z2015-01-0112:347", "open");
insert into userActivities VALUES("Jimmy Hon", "trainer", "T0234567Z2015-01-0112:448", "open");	

insert into userActivities VALUES("Tan Sau Kuen", "trainer", "T0234567Z2015-01-0110:101", "open");
insert into userActivities VALUES("Tan Sau Kuen", "trainer", "T0234567Z2015-01-0111:102", "open");
insert into userActivities VALUES("Tan Sau Kuen", "trainer", "T0234567Z2015-01-0112:003", "open");
insert into userActivities VALUES("Tan Sau Kuen", "trainer", "T0234567Z2015-01-0112:104", "open");
insert into userActivities VALUES("Tan Sau Kuen", "trainer", "T0234567Z2015-01-0112:155", "open");
insert into userActivities VALUES("Tan Sau Kuen", "trainer", "T0234567Z2015-01-0112:306", "open");
insert into userActivities VALUES("Tan Sau Kuen", "trainer", "T0234567Z2015-01-0112:347", "open");
insert into userActivities VALUES("Tan Sau Kuen", "trainer", "T0234567Z2015-01-0112:448", "open");	

insert into userActivities VALUES("Tay Ee Leen", "trainer", "T0234567Z2015-01-0110:101", "open");
insert into userActivities VALUES("Tay Ee Leen", "trainer", "T0234567Z2015-01-0111:102", "open");
insert into userActivities VALUES("Tay Ee Leen", "trainer", "T0234567Z2015-01-0112:003", "open");
insert into userActivities VALUES("Tay Ee Leen", "trainer", "T0234567Z2015-01-0112:104", "open");
insert into userActivities VALUES("Tay Ee Leen", "trainer", "T0234567Z2015-01-0112:155", "open");
insert into userActivities VALUES("Tay Ee Leen", "trainer", "T0234567Z2015-01-0112:306", "open");
insert into userActivities VALUES("Tay Ee Leen", "trainer", "T0234567Z2015-01-0112:347", "open");
insert into userActivities VALUES("Tay Ee Leen", "trainer", "T0234567Z2015-01-0112:448", "open");		


-- TRAINEE ROSTER ASSIGNMENT -- 
insert into userRosterAssignment VALUES("Jai Ajitchandra Sule", "trainee", "H1234567A2015-02-0711:101", "Not Completed");
insert into userRosterAssignment VALUES("Jai Ajitchandra Sule", "trainee", "H1234567A2015-02-0711:122", "Not Completed");
insert into userRosterAssignment VALUES("Jai Ajitchandra Sule", "trainee", "H1234567A2015-02-0711:133", "Not Completed");
insert into userRosterAssignment VALUES("Jai Ajitchandra Sule", "trainee", "H1234567A2015-02-0711:144", "Not Completed");

insert into userRosterAssignment VALUES("Li Yue", "trainee", "H1234567A2015-02-0711:101", "Completed");
insert into userRosterAssignment VALUES("Li Yue", "trainee", "H1234567A2015-02-0711:122", "Not Completed");
insert into userRosterAssignment VALUES("Li Yue", "trainee", "H1234567A2015-02-0711:133", "Not Completed");
insert into userRosterAssignment VALUES("Li Yue", "trainee", "H1234567A2015-02-0711:144", "Not Completed");	

insert into userRosterAssignment VALUES("Nieh Chih Chiang", "trainee", "H1234567A2015-02-0711:101", "Completed");
insert into userRosterAssignment VALUES("Nieh Chih Chiang", "trainee", "H1234567A2015-02-0711:122", "Not Completed");
insert into userRosterAssignment VALUES("Nieh Chih Chiang", "trainee", "H1234567A2015-02-0711:133", "Not Completed");
insert into userRosterAssignment VALUES("Nieh Chih Chiang", "trainee", "H1234567A2015-02-0711:144", "Not Completed");

insert into userRosterAssignment VALUES("Chang Guohao", "trainee", "H1234567A2015-02-0711:101", "Not Completed");
insert into userRosterAssignment VALUES("Chang Guohao", "trainee", "H1234567A2015-02-0711:122", "Completed");
insert into userRosterAssignment VALUES("Chang Guohao", "trainee", "H1234567A2015-02-0711:133", "Not Completed");
insert into userRosterAssignment VALUES("Chang Guohao", "trainee", "H1234567A2015-02-0711:144", "Not Completed");	

insert into userRosterAssignment VALUES("Chen Jian Ye", "trainee", "H1234567A2015-02-0711:101", "Completed");
insert into userRosterAssignment VALUES("Chen Jian Ye", "trainee", "H1234567A2015-02-0711:122", "Not Completed");
insert into userRosterAssignment VALUES("Chen Jian Ye", "trainee", "H1234567A2015-02-0711:133", "Not Completed");
insert into userRosterAssignment VALUES("Chen Jian Ye", "trainee", "H1234567A2015-02-0711:144", "Not Completed");

insert into userRosterAssignment VALUES("Julian Nester Matigal", "trainee", "H1234567A2015-02-0711:101", "Completed");
insert into userRosterAssignment VALUES("Julian Nester Matigal", "trainee", "H1234567A2015-02-0711:122", "Not Completed");
insert into userRosterAssignment VALUES("Julian Nester Matigal", "trainee", "H1234567A2015-02-0711:133", "Not Completed");
insert into userRosterAssignment VALUES("Julian Nester Matigal", "trainee", "H1234567A2015-02-0711:144", "Not Completed");	

insert into userRosterAssignment VALUES("Qian Qi", "trainee", "H1234567A2015-02-0711:101", "Completed");
insert into userRosterAssignment VALUES("Qian Qi", "trainee", "H1234567A2015-02-0711:122", "Not Completed");
insert into userRosterAssignment VALUES("Qian Qi", "trainee", "H1234567A2015-02-0711:133", "Not Completed");
insert into userRosterAssignment VALUES("Qian Qi", "trainee", "H1234567A2015-02-0711:144", "Not Completed");	

-- TRAINEER ROSTER ASSIGNMENT --
insert into userRosterAssignment VALUES("Ooi Oon Cheong", "trainer", "H1234567A2015-02-0711:101", "Not Completed");
insert into userRosterAssignment VALUES("Ooi Oon Cheong", "trainer", "H1234567A2015-02-0711:122", "Not Completed");
insert into userRosterAssignment VALUES("Ooi Oon Cheong", "trainer", "H1234567A2015-02-0711:133", "Not Completed");
insert into userRosterAssignment VALUES("Ooi Oon Cheong", "trainer", "H1234567A2015-02-0711:144", "Not Completed");

insert into userRosterAssignment VALUES("Jimmy Hon", "trainer", "H1234567A2015-02-0711:101", "Not Completed");
insert into userRosterAssignment VALUES("Jimmy Hon", "trainer", "H1234567A2015-02-0711:122", "Not Completed");
insert into userRosterAssignment VALUES("Jimmy Hon", "trainer", "H1234567A2015-02-0711:133", "Not Completed");
insert into userRosterAssignment VALUES("Jimmy Hon", "trainer", "H1234567A2015-02-0711:144", "Not Completed");

insert into userRosterAssignment VALUES("Tan Sau Kuen", "trainer", "H1234567A2015-02-0711:101", "Not Completed");
insert into userRosterAssignment VALUES("Tan Sau Kuen", "trainer", "H1234567A2015-02-0711:122", "Not Completed");
insert into userRosterAssignment VALUES("Tan Sau Kuen", "trainer", "H1234567A2015-02-0711:133", "Not Completed");
insert into userRosterAssignment VALUES("Tan Sau Kuen", "trainer", "H1234567A2015-02-0711:144", "Not Completed");

insert into userRosterAssignment VALUES("Tay Ee Leen", "trainer", "H1234567A2015-02-0711:101", "Not Completed");
insert into userRosterAssignment VALUES("Tay Ee Leen", "trainer", "H1234567A2015-02-0711:122", "Not Completed");
insert into userRosterAssignment VALUES("Tay Ee Leen", "trainer", "H1234567A2015-02-0711:133", "Not Completed");
insert into userRosterAssignment VALUES("Tay Ee Leen", "trainer", "H1234567A2015-02-0711:144", "Not Completed");	

-- ROSTER BLOCK OUT REQUEST -- 
insert into rosterBlockOutReq VALUES("H1234567A2015-02-0219:471", "2015-04-14", "2015-04-15", "Need to catch up on Sleep", "Chang Guohao", "trainee", "Approved");
insert into rosterBlockOutReq VALUES("H1234567A2015-02-0819:412", "2015-05-05", "2015-05-07", "Son's school play", "Ooi Oon Cheong", "trainer", "Pending");
insert into rosterBlockOutReq VALUES("H1234567A2015-02-0820:433", "2015-04-06", "2015-04-07", "Girlfriend's company dinner", "Chen Jian Ye", "trainee", "Pending");	


-- Trainee Exams -- 
insert into traineeExams VALUES("Jai Ajitchandra Sule", "trainee", "clf1101", 23);
insert into traineeExams VALUES("Jai Ajitchandra Sule", "trainee", "clf2015", 45);
insert into traineeExams VALUES("Jai Ajitchandra Sule", "trainee", "med1224", 123);

insert into traineeExams VALUES("Li Yue", "trainee", "clf1101", 23);
insert into traineeExams VALUES("Li Yue", "trainee", "clf2015", 45);
insert into traineeExams VALUES("Li Yue", "trainee", "med1224", 123);

insert into traineeExams VALUES("Nieh Chih Chiang", "trainee", "clf1101", 23);
insert into traineeExams VALUES("Nieh Chih Chiang", "trainee", "clf2015", 45);
insert into traineeExams VALUES("Nieh Chih Chiang", "trainee", "med1224", 123);	

insert into traineeExams VALUES("Chang Guohao", "trainee", "clf1101", 23);
insert into traineeExams VALUES("Chang Guohao", "trainee", "clf2015", 45);
insert into traineeExams VALUES("Chang Guohao", "trainee", "med1224", 123);	

insert into traineeExams VALUES("Chen Jian Ye", "trainee", "clf1101", 23);
insert into traineeExams VALUES("Chen Jian Ye", "trainee", "clf2015", 45);
insert into traineeExams VALUES("Chen Jian Ye", "trainee", "med1224", 123);

insert into traineeExams VALUES("Qian Qi", "trainee", "clf1101", 23);
insert into traineeExams VALUES("Qian Qi", "trainee", "clf2015", 45);
insert into traineeExams VALUES("Qian Qi", "trainee", "med1224", 123);

insert into traineeExams VALUES("Julian Nester Matigal", "trainee", "clf1101", 23);
insert into traineeExams VALUES("Julian Nester Matigal", "trainee", "clf2015", 45);
insert into traineeExams VALUES("Julian Nester Matigal", "trainee", "med1224", 123);	


-- TRAINEE ASSESSMENT -- 
insert into assessment VALUES("jT0236478W2015-02-2112:111", "Jai Ajitchandra Sule", "trainee", 
"Basic Medical Knowledge", "2015-02-20", "A", "Good grasp of the fundamental concepts");
insert into assessment VALUES("jT0236478W2015-02-2112:132", "Jai Ajitchandra Sule", "trainee", 
"ECG Reading Workshop", "2015-02-10", "B+", "Needs more practice");
insert into assessment VALUES("jT0236478W2015-02-2211:113", "Jai Ajitchandra Sule", "trainee", 
"Chest Tube Insertion", "2015-03-13", "B+", "Needs a more steady hand");

insert into assessment VALUES("lT0236478W2015-02-2112:111", "Li Yue", "trainee", 
"Basic Medical Knowledge", "2015-02-20", "A", "Good grasp of the fundamental concepts");
insert into assessment VALUES("lT0236478W2015-02-2112:132", "Li Yue", "trainee", 
"ECG Reading Workshop", "2015-02-10", "B+", "Needs more practice");
insert into assessment VALUES("lT0236478W2015-02-2211:113", "Li Yue", "trainee", 
"Chest Tube Insertion", "2015-03-13", "B+", "Needs a more steady hand");

insert into assessment VALUES("nT0236478W2015-02-2112:111", "Nieh Chih Chiang", "trainee", 
"Basic Medical Knowledge", "2015-02-20", "A", "Good grasp of the fundamental concepts");
insert into assessment VALUES("nT0236478W2015-02-2112:132", "Nieh Chih Chiang", "trainee", 
"ECG Reading Workshop", "2015-02-10", "B+", "Needs more practice");
insert into assessment VALUES("nT0236478W2015-02-2211:113", "Nieh Chih Chiang", "trainee", 
"Chest Tube Insertion", "2015-03-13", "B+", "Needs a more steady hand");

insert into assessment VALUES("cT0236478W2015-02-2112:111", "Chang Guohao", "trainee", 
"Basic Medical Knowledge", "2015-02-20", "A", "Good grasp of the fundamental concepts");
insert into assessment VALUES("cT0236478W2015-02-2112:132", "Chang Guohao", "trainee", 
"ECG Reading Workshop", "2015-02-10", "B+", "Needs more practice");
insert into assessment VALUES("cT0236478W2015-02-2211:113", "Chang Guohao", "trainee", 
"Chest Tube Insertion", "2015-03-13", "B+", "Needs a more steady hand");

insert into assessment VALUES("qT0236478W2015-02-2112:111", "Qian Qi", "trainee", 
"Basic Medical Knowledge", "2015-02-20", "A", "Good grasp of the fundamental concepts");
insert into assessment VALUES("qT0236478W2015-02-2112:132", "Qian Qi", "trainee", 
"ECG Reading Workshop", "2015-02-10", "B+", "Needs more practice");
insert into assessment VALUES("qT0236478W2015-02-2211:113", "Qian Qi", "trainee", 
"Chest Tube Insertion", "2015-03-13", "B+", "Needs a more steady hand");

insert into assessment VALUES("uT0236478W2015-02-2112:111", "Julian Nester Matigal", "trainee", 
"Basic Medical Knowledge", "2015-02-20", "A", "Good grasp of the fundamental concepts");
insert into assessment VALUES("uT0236478W2015-02-2112:132", "Julian Nester Matigal", "trainee", 
"ECG Reading Workshop", "2015-02-10", "B+", "Needs more practice");
insert into assessment VALUES("uT0236478W2015-02-2211:113", "Julian Nester Matigal", "trainee", 
"Chest Tube Insertion", "2015-03-13", "B+", "Needs a more steady hand");