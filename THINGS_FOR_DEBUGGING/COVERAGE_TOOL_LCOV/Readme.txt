Lcov for Windows 

The goal was to create a version of Lcov that would run on Windows without Msys.
I started with Lcov 1.10 from the LTP website. 

There are three Perl 'scripts' in Lcov - lcov.pl, geninfo.pl, and genhtml.pl - which comprise 14k+ lines of code.
I made minimal changes so that going to the next version would be easier.  Most of them are of the form:

if ($is_windows) {
    do something the Windows way
}
else {
    do something the Linux way
}

Most of these changes relate to Lcov's manipulation of directory and file names, both absolute and relative,
in Perl and in regular expressions.  Lcov makes the assumption that it is dealing with Linux names.
You have not lived until you have used the Windows directory separator, the back slash ("\"), in Perl
and in regular expressions where the back slash is also the escape character.

I changed the version number to Lcov 1.10w (Windows).

I changed the $date variable to be localtime. The date displayed now looks like this: Thu Jun 27 10:16:35 2013

LCW (Lcov for Windows) works well enough for what I need. It runs unchanged on Linux. 

LCW will read options from a config file as well as from the command line.  The config file is named lcovrc.txt
and you must tell LCW where the config file is using the --config-file command line option. For example,
--config-file=C:\temp\lcovrc.txt.

LCW, like Lcov 1.10, does not produce branch counts by default.  You can enable it by adding --rc lcov_branch_coverage=1
to the command line. Adding lcov_branch_coverage=1 to the config file does not seem to work.

The lcov.bat batch file should be run from your build directory. IOW, if your project name is MyProject, you would create
a MyProject\build directory, cd into it, and then run cmake .. from it. Cmake will put its files in the build directory
and gcc will compile from there too.

I hope you will find this useful. Feel free to improve on my Perl code. I had never used Perl until 10 days ago, and I know
it shows.


Donald MacQueen [|]

dmacq@instantiations.com

Arlington, Virgnia
12 October 2013