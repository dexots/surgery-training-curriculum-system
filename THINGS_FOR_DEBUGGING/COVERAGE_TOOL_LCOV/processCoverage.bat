echo off

REM editted by: Chan Jun Wei

REM run this in your MyProject\build directory
REM add something like --config-file=C:\temp\lcovrc.txt to use the lcovrc.txt config file
if "%1"=="" goto syntax
if "%2"=="" goto syntax
set gcov-files-dir=%1
set file-pattern=%2

REM remove --rc lcov_branch_coverage=1 from the two lines below if you do NOT want branch coverage

lcov.pl -d %gcov-files-dir% -z

lcov.pl --capture --directory %gcov-files-dir% --output-file %gcov-files-dir%\coverage.info

copy %cd%\%gcov-files-dir%\coverage.info %cd%\coverage.info


REM lcov.pl --list-full-path -e %gcov-files-dir%\coverage.info %file-pattern% -o %gcov-files-dir%\coverage-stripped.info

REM genhtml.pl %gcov-files-dir%\coverage-stripped.info --output-directory %cd%\out
genhtml.pl coverage.info --output-directory %cd%\out
REM java -jar jgenhtml.jar coverage.info --output-directory out

lcov.pl -d %gcov-files-dir% -z

goto end
:syntax
echo "usage: %0 <gcov-files-dir> \"<file-pattern>\""
:end