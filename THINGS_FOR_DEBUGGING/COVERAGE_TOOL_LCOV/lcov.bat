echo off
REM run this in your MyProject\build directory
REM add something like --config-file=C:\temp\lcovrc.txt to use the lcovrc.txt config file
if "%1"=="" goto syntax
set destinationdir=%1
set projectpath=%2
mkdir %destinationdir%

REM remove --rc lcov_branch_coverage=1 from the two lines below if you do NOT want branch coverage
lcov.pl --directory %destinationdir% --capture --output-file coverage.info

lcov.pl --list-full-path -e $coverage.info %projectpath% -o $coverage-stripped.info

genhtml.pl  coverage-stripped.info --output-directory out

goto end
:syntax
echo  --
echo   the destination directory (where the output should go) is required  
echo  __
:end